#include "StdAfx.h"
#include "GameXMLSettingAndGlobals.h"
#include <CrySystem/XML/IXml.h>
#include "Actor.h"
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include <Cry3DEngine/ITimeOfDay.h>
#include "UI/HUD/HUDCommon.h"


CGameXMLSettingAndGlobals::CGameXMLSettingAndGlobals()
{
	//CryLogAlways("CGameXMLSettingAndGlobals::CGameXMLSettingAndGlobals() constructor call");
	character_customization_path = "Scripts/Entities/Actor/CharacterCustomization.xml";
	character_spells_path = "Scripts/Entities/Actor/CharacterSpellsInfo.xml";
	character_buffs_path = "Scripts/Entities/Actor/Buffs.xml";
	character_perks_path = "Scripts/Entities/Actor/Perks.xml";
	character_skills_path = "Scripts/Entities/Actor/Skills.xml";
	character_actions_anim_path = "Scripts/Entities/Actor/CharacterActionsAnims.xml";
	items_combinations_def = "Scripts/Entities/Items/Combinations.xml";

	current_dialog_NPC = 0;
	current_opened_Chest = 0;
	current_traider = 0;
	current_traider_chest = 0;
	current_selected_Item = 0;
	current_item_to_combine = 0;
	days_left = 0;
	old_time_od = 0.0f;
	enable_character_creating_menu_at_game_start = true;
	current_selected_spell = "";
	current_selected_spell_ids = -1;
	//s_ActorSpells.reserve(65535);
	//s_pl_Quests.reserve(65535);
	//s_pl_SubQuests.reserve(65535);
}

CGameXMLSettingAndGlobals::~CGameXMLSettingAndGlobals()
{

}

void CGameXMLSettingAndGlobals::Init()
{
	IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
	if (!pxml)
		return;

	InitCharacterCustomization();
	InitPlayerChrAttachmentsBase();
	InitActorAnimActionDefs();
	InitCustomGameVars();
	InitCharacterBuffs();
	InitCharacterSpells();
	InitCharacterPerks();
	InitCharacterSkills();
}

void CGameXMLSettingAndGlobals::Serialize(TSerialize ser)
{
	ser.BeginGroup("GameSettingAndGlobals");
	ser.BeginGroup("ClrValues");
	ser.Value("days_left", days_left);
	ser.Value("chr_creating_onstart_enabled", enable_character_creating_menu_at_game_start);
	ser.EndGroup();
	ser.BeginGroup("PlayerJournal");
	
	int quest_count = 0;
	quest_count = s_pl_Quests.size();
	ser.Value("quest_count", quest_count);
	int sub_quest_count = 0;
	sub_quest_count = s_pl_SubQuests.size();
	ser.Value("sub_quest_count", sub_quest_count);
	if (ser.IsReading())
	{
		//CryLogAlways("CGameXMLSettingAndGlobals::Serialize IsReading()");
		s_pl_Quests.clear();
		for (int i = 0; i < quest_count; i++)
		{
			string questpath = ""; int id = 0; int stage = 0; int flag = 0; int last = 0; float complete_percent = 0.0f;
			string quest_sn = "";
			quest_sn.Format("Quest_%d", i);
			ser.BeginGroup(quest_sn.c_str());
			string questpath_str = "q_quest_path";
			string quest_id_str = "q_quest_id";
			string quest_stage_str = "q_quest_stage";
			string quest_flag_str = "q_quest_flag";
			string quest_last_str = "q_quest_last";
			string quest_percent_str = "q_quest_percent";
			/*questpath_str = quest_sn + questpath_str;
			quest_id_str = quest_sn + quest_id_str;
			quest_stage_str = quest_sn + quest_stage_str;
			quest_flag_str = quest_sn + quest_flag_str;
			quest_last_str = quest_sn + quest_last_str;*/
			ser.Value(questpath_str.c_str(), questpath);
			ser.Value(quest_id_str.c_str(), id);
			ser.Value(quest_stage_str.c_str(), stage);
			ser.Value(quest_flag_str.c_str(), flag);
			ser.Value(quest_last_str.c_str(), last);
			ser.Value(quest_percent_str.c_str(), complete_percent);
			if (questpath.empty())
			{
				string quest_dec_str[2];
				quest_dec_str[0] = "q_quest_dec_s";
				quest_dec_str[1] = "q_quest_dec_l";
				string quest_name_str = "q_quest_name";
				string quest_dec[2];
				quest_dec[0] = "";
				quest_dec[1] = "";
				string quest_name = "";
				ser.Value(quest_dec_str[0].c_str(), quest_dec[0]);
				ser.Value(quest_dec_str[1].c_str(), quest_dec[1]);
				ser.Value(quest_name_str.c_str(), quest_name);
				AddQuestNoXML(quest_name, quest_dec, id, stage, flag, last, complete_percent);
			}
			else
				AddQuest(questpath.c_str(), id, stage, flag, last, complete_percent);

			ser.EndGroup();

		}
		s_pl_SubQuests.clear();
		for (int i = 0; i < sub_quest_count; i++)
		{
			string questpath = ""; int id = 0; int stage = 0; int flag = 0; int last = 0;
			string quest_sn = "";
			quest_sn.Format("SubQuest_%d", i);
			ser.BeginGroup(quest_sn.c_str());
			string questpath_str = "q_quest_path";
			string quest_id_str = "q_quest_id";
			string quest_stage_str = "q_quest_stage";
			string quest_last_str = "q_quest_last";
			string quest_flag_str = "q_quest_flag";
			ser.Value(questpath_str.c_str(), questpath);
			ser.Value(quest_id_str.c_str(), id);
			ser.Value(quest_stage_str.c_str(), stage);
			ser.Value(quest_flag_str.c_str(), flag);
			ser.Value(quest_last_str.c_str(), last);
			if (questpath.empty())
			{
				string quest_dec_str[2];
				quest_dec_str[0] = "q_quest_dec_s";
				quest_dec_str[1] = "q_quest_dec_l";
				string quest_name_str = "q_quest_name";
				string quest_pr_name_str = "q_quest_parent_name";
				string quest_dec[2];
				quest_dec[0] = "";
				quest_dec[1] = "";
				string quest_name = "";
				string parent_quest_name = "";
				ser.Value(quest_dec_str[0].c_str(), quest_dec[0]);
				ser.Value(quest_dec_str[1].c_str(), quest_dec[1]);
				ser.Value(quest_name_str.c_str(), quest_name);
				ser.Value(quest_pr_name_str.c_str(), parent_quest_name);
				AddSubQuestNoXML(quest_name, parent_quest_name, quest_dec, id, stage, flag, last);
			}
			else
				AddSubQuest(questpath.c_str(), id, stage, last, flag);

			ser.EndGroup();

		}
	}
	else
	{
		//CryLogAlways("CGameXMLSettingAndGlobals::Serialize IsWriting()");
		SPlayerQuest qst;
		std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
		std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
		int count = s_pl_Quests.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			qst = *it;
			if (qst.m_questname.empty())
				continue;

			string questpath = qst.m_questpath; int id = qst.m_questid; int stage = qst.m_queststat; int flag = qst.m_questflag; int last = qst.m_lastcn; float complete_percent = qst.m_complete_percent;
			string quest_sn = "";
			quest_sn.Format("Quest_%d", i);
			ser.BeginGroup(quest_sn.c_str());
			string questpath_str = "q_quest_path";
			string quest_id_str = "q_quest_id";
			string quest_stage_str = "q_quest_stage";
			string quest_flag_str = "q_quest_flag";
			string quest_last_str = "q_quest_last";
			string quest_percent_str = "q_quest_percent";
			ser.Value(questpath_str.c_str(), questpath);
			ser.Value(quest_id_str.c_str(), id);
			ser.Value(quest_stage_str.c_str(), stage);
			ser.Value(quest_flag_str.c_str(), flag);
			ser.Value(quest_last_str.c_str(), last);
			ser.Value(quest_percent_str.c_str(), complete_percent);
			if (questpath.empty())
			{
				string quest_dec_str[2];
				quest_dec_str[0] = "q_quest_dec_s";
				quest_dec_str[1] = "q_quest_dec_l";
				string quest_name_str = "q_quest_name";
				string quest_dec[2];
				quest_dec[0] = qst.m_sdec;
				quest_dec[1] = qst.m_ldec;
				string quest_name = qst.m_questname;
				ser.Value(quest_dec_str[0].c_str(), quest_dec[0]);
				ser.Value(quest_dec_str[1].c_str(), quest_dec[1]);
				ser.Value(quest_name_str.c_str(), quest_name);
			}
			ser.EndGroup();
		}
		SPlayerSubQuest subqst;
		std::vector<SPlayerSubQuest>::iterator sub_it = s_pl_SubQuests.begin();
		std::vector<SPlayerSubQuest>::iterator sub_end = s_pl_SubQuests.end();
		count = s_pl_SubQuests.size();
		for (int i = 0; i < count, sub_it != sub_end; i++, ++sub_it)
		{
			subqst = *sub_it;
			if (subqst.m_questname.empty())
				continue;

			string questpath = subqst.m_questpath; int id = subqst.m_questid; int stage = subqst.m_queststat; int flag = subqst.m_questflag; int last = subqst.m_lastcn;
			string quest_sn = "";
			quest_sn.Format("SubQuest_%d", i);
			ser.BeginGroup(quest_sn.c_str());
			string questpath_str = "q_quest_path";
			string quest_id_str = "q_quest_id";
			string quest_stage_str = "q_quest_stage";
			string quest_flag_str = "q_quest_flag";
			string quest_last_str = "q_quest_last";
			ser.Value(questpath_str.c_str(), questpath);
			ser.Value(quest_id_str.c_str(), id);
			ser.Value(quest_stage_str.c_str(), stage);
			ser.Value(quest_flag_str.c_str(), flag);
			ser.Value(quest_last_str.c_str(), last);
			if (questpath.empty())
			{
				string quest_dec_str[2];
				quest_dec_str[0] = "q_quest_dec_s";
				quest_dec_str[1] = "q_quest_dec_l";
				string quest_name_str = "q_quest_name";
				string quest_pr_name_str = "q_quest_parent_name";
				string quest_dec[2];
				quest_dec[0] = subqst.m_sdec;
				quest_dec[1] = subqst.m_ldec;
				string quest_name = subqst.m_questname;
				string parent_quest_name = subqst.m_parentquestname;
				ser.Value(quest_dec_str[0].c_str(), quest_dec[0]);
				ser.Value(quest_dec_str[1].c_str(), quest_dec[1]);
				ser.Value(quest_name_str.c_str(), quest_name);
				ser.Value(quest_pr_name_str.c_str(), parent_quest_name);
			}
			ser.EndGroup();
		}
	}
	ser.EndGroup();
	ser.EndGroup();
}

void CGameXMLSettingAndGlobals::Update(float frametime)
{
	ITimeOfDay* pTod = gEnv->p3DEngine->GetTimeOfDay();
	if (pTod)
	{
		if (old_time_od > pTod->GetTime())
		{
			days_left += 1;
			old_time_od = 0.00000f;
		}
		else
		{
			old_time_od = pTod->GetTime();
		}
	}

	if (gEnv->IsEditor() && !GetISystem()->IsPaused())
	{
		if(enable_character_creating_menu_at_game_start)
			enable_character_creating_menu_at_game_start = false;
	}

	if (!gEnv->IsEditor()/* && !gEnv->bMultiplayer*/ && !GetISystem()->IsPaused())
	{
		if (enable_character_creating_menu_at_game_start)
		{
			if (g_pGame->GetHUDCommon())
			{
				if (g_pGame->GetHudActvTimer() <= 0.0f && !g_pGame->GetHUDCommon()->m_PostInitScreenVisible)
				{
					g_pGame->GetHUDCommon()->ShowCharacterCreatingMenu();
					enable_character_creating_menu_at_game_start = false;
				}
			}
		}
	}
}

CGameXMLSettingAndGlobals::SCharacterCustomizationData::SCharacterCustomizationData()
{
	for (int i = 0; i < 255; i++)
	{
		character_fem_hair[i] = "";
		character_fem_face[i] = "";
		character_fem_eye_lft[i] = "";
		character_fem_eye_rgt[i] = "";
		character_man_hair[i] = "";
		character_man_face[i] = "";
		character_man_eye_lft[i] = "";
		character_man_eye_rgt[i] = "";
		character_fem_hair_info[i] = "";
		character_fem_face_info[i] = "";
		character_fem_eye_info[i] = "";
		character_man_hair_info[i] = "";
		character_man_face_info[i] = "";
		character_man_eye_info[i] = "";
	}

	for (int i = 0; i < 5; i++)
	{
		character_fem_skeletion[i] = "";
		character_man_skeletion[i] = "";
	}

	for (int i = 0; i < 3; i++)
	{
		character_base_attachment_foots[i] = "";
		character_base_attachment_legs[i] = "";
		character_base_attachment_torso[i] = "";
		character_base_attachment_hands[i] = "";
		character_base_attachment_head[i] = "";
		character_base_attachment_eyes[i] = "";
	}
}

void CGameXMLSettingAndGlobals::InitCharacterCustomization()
{
	CharacterCustomization_node = GetISystem()->LoadXmlFromFile(character_customization_path.c_str());
	if (!CharacterCustomization_node)
		return;

	XmlNodeRef infopathNode = CharacterCustomization_node->findChild("info");
	if (!infopathNode)
		return;

	XmlNodeRef hairsNode = infopathNode->findChild("hairs");
	if (hairsNode)
	{
		XmlNodeRef hairsmNode = hairsNode->findChild("hairsm");
		XmlNodeRef hairsfNode = hairsNode->findChild("hairsf");

		for (int i = 1; i < g_pGameCVars->g_pl_customization_na_hairs + 1; i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;

			if (i <= 9)
				A_valstr = "hair0";
			else if (i > 9)
				A_valstr = "hair";

			string D_valstr = A_valstr + C_valstr;
			customization_data.character_fem_hair[i] = hairsfNode->getAttr(D_valstr.c_str());
			customization_data.character_man_hair[i] = hairsmNode->getAttr(D_valstr.c_str());
			string hair_info = "info";
			string D_info = D_valstr + hair_info;
			customization_data.character_fem_hair_info[i] = hairsfNode->getAttr(D_info.c_str());
			customization_data.character_man_hair_info[i] = hairsmNode->getAttr(D_info.c_str());
		}
	}
	XmlNodeRef headsNode = infopathNode->findChild("heads");
	if (headsNode)
	{
		XmlNodeRef headsfNode = headsNode->findChild("headsf");
		XmlNodeRef headsmNode = headsNode->findChild("headsm");
		for (int i = 1; i < g_pGameCVars->g_pl_customization_na_heads + 1; i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;

			if (i <= 9)
				A_valstr = "head0";
			else if (i > 9)
				A_valstr = "head";

			string D_valstr = A_valstr + C_valstr;
			customization_data.character_fem_face[i] = headsfNode->getAttr(D_valstr.c_str());
			customization_data.character_man_face[i] = headsmNode->getAttr(D_valstr.c_str());
			string head_info = "info";
			string D_info = D_valstr + head_info;
			customization_data.character_fem_face_info[i] = headsfNode->getAttr(D_info.c_str());
			customization_data.character_man_face_info[i] = headsmNode->getAttr(D_info.c_str());
		}
	}
	XmlNodeRef eyesNode = infopathNode->findChild("eyes");
	if (eyesNode)
	{
		XmlNodeRef eyesfNode = eyesNode->findChild("eyesf");
		XmlNodeRef eyesmNode = eyesNode->findChild("eyesm");
		for (int i = 1; i < g_pGameCVars->g_pl_customization_na_eyes + 1; i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;
			string S_valstr;
			string S2_valstr;
			string eye_info_h;
			string eye_info_h2;
			eye_info_h = "eyes";
			eye_info_h2 = "info";
			string D_info = eye_info_h + C_valstr + eye_info_h2;
			S_valstr = "l";
			S2_valstr = "r";
			A_valstr = "eye";
			string D_valstr = A_valstr + C_valstr + S_valstr;
			string D2_valstr = A_valstr + C_valstr + S2_valstr;
			customization_data.character_fem_eye_lft[i] = eyesfNode->getAttr(D_valstr.c_str());
			customization_data.character_fem_eye_rgt[i] = eyesfNode->getAttr(D2_valstr.c_str());
			customization_data.character_man_eye_lft[i] = eyesmNode->getAttr(D_valstr.c_str());
			customization_data.character_man_eye_rgt[i] = eyesmNode->getAttr(D2_valstr.c_str());
			customization_data.character_fem_eye_info[i] = eyesfNode->getAttr(D_info.c_str());
			customization_data.character_man_eye_info[i] = eyesmNode->getAttr(D_info.c_str());
		}
	}
	XmlNodeRef bodyNode = infopathNode->findChild("body");
	if (bodyNode)
	{
		XmlNodeRef bodyfNode = bodyNode->findChild("bodyf");
		XmlNodeRef bodymNode = bodyNode->findChild("bodym");
		for (int i = 1; i < 5; i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr = "body0";
			string D_valstr = A_valstr + C_valstr;
			customization_data.character_fem_skeletion[i] = bodyfNode->getAttr(D_valstr.c_str());
			customization_data.character_man_skeletion[i] = bodymNode->getAttr(D_valstr.c_str());
		}
	}
}

void CGameXMLSettingAndGlobals::InitPlayerChrAttachmentsBase()
{
	if (!CharacterCustomization_node)
		CharacterCustomization_node = GetISystem()->LoadXmlFromFile(character_customization_path.c_str());

	if (!CharacterCustomization_node)
		return;

	XmlNodeRef infopathNode = CharacterCustomization_node->findChild("info2");
	if (!infopathNode)
		return;

	for (int i = 0; i < 3; i++)
	{
		char B_valstr[17];
		itoa(i, B_valstr, 10);
		string C_valstr = B_valstr;
		string A_valstr = "foots";
		string A2_valstr = "legs";
		string A3_valstr = "torso";
		string A4_valstr = "hands";
		string A5_valstr = "head";
		string A6_valstr = "eye";

		string D_valstr = A_valstr + C_valstr;
		string D2_valstr = A2_valstr + C_valstr;
		string D3_valstr = A3_valstr + C_valstr;
		string D4_valstr = A4_valstr + C_valstr;
		string D5_valstr = A5_valstr + C_valstr;
		string D6_valstr = A6_valstr + C_valstr;
		customization_data.character_base_attachment_foots[i] = infopathNode->getAttr(D_valstr.c_str());
		customization_data.character_base_attachment_legs[i] = infopathNode->getAttr(D2_valstr.c_str());
		customization_data.character_base_attachment_torso[i] = infopathNode->getAttr(D3_valstr.c_str());
		customization_data.character_base_attachment_hands[i] = infopathNode->getAttr(D4_valstr.c_str());
		customization_data.character_base_attachment_head[i] = infopathNode->getAttr(D5_valstr.c_str());
		customization_data.character_base_attachment_eyes[i] = infopathNode->getAttr(D6_valstr.c_str());
	}
}

void CGameXMLSettingAndGlobals::InitActorAnimActionDefs()
{
	CharacterActionsAnims_node = GetISystem()->LoadXmlFromFile(character_actions_anim_path.c_str());
	if (!CharacterActionsAnims_node)
		return;

	XmlNodeRef infopathNode = CharacterActionsAnims_node->findChild("info");
	if (!infopathNode)
		return;

	XmlNodeRef combatActionsNode = infopathNode->findChild("CombatActions");
	if (combatActionsNode)
	{

	}

	XmlNodeRef commonActionsNode = infopathNode->findChild("CommonActions");
	if (commonActionsNode)
	{

	}
}

void CGameXMLSettingAndGlobals::InitCharacterBuffs()
{
	CharacterBuffs_node = GetISystem()->LoadXmlFromFile(character_buffs_path.c_str());
	if (!CharacterBuffs_node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("XMLSetting xml not loaded.");
		return;
	}

	
}

void CGameXMLSettingAndGlobals::InitCharacterSpells(bool preload_all_static_spells_in_mem)
{
	CharacterSpells_node = GetISystem()->LoadXmlFromFile(character_spells_path.c_str());
	if (!CharacterSpells_node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded. Character's spells file not loaded");
		return;
	}

	if (preload_all_static_spells_in_mem)
	{
		s_ActorSpells.clear();
		XmlNodeRef infopathNode = CharacterSpells_node->findChild("info");
		if (!infopathNode)
			return;

		XmlNodeRef spellsNode = infopathNode->findChild("spells");
		if (!spellsNode)
			return;

		int childs_num = spellsNode->getChildCount();
		for (int i = 0; i < childs_num; i++)
		{
			XmlNodeRef spellNode = spellsNode->getChild(i);
			if (!spellNode)
				continue;

			int ids = -1;
			spellNode->getAttr("id", ids);
			if (ids > -1)
			{
				//CryLogAlways("spell with id %i loaded from spells info file", ids);
				SActorSpell s_pcsb_spell;
				spellNode->getAttr("id", s_pcsb_spell.spell_id);
				spellNode->getAttr("effect_id", s_pcsb_spell.spell_effect_id);
				spellNode->getAttr("spell_type", s_pcsb_spell.spell_type);
				spellNode->getAttr("start_time", s_pcsb_spell.start_time);
				spellNode->getAttr("dmg", s_pcsb_spell.spell_damage);
				spellNode->getAttr("spell_chargeable", s_pcsb_spell.spell_chargeable);
				spellNode->getAttr("spell_act_scr_func", s_pcsb_spell.spell_act_scr_func[1]);
				spellNode->getAttr("spell_act_scr_func2", s_pcsb_spell.spell_act_scr_func[2]);
				spellNode->getAttr("spell_mana_cost", s_pcsb_spell.spell_mana_cost);
				spellNode->getAttr("spell_hp_cost", s_pcsb_spell.spell_hp_cost);
				spellNode->getAttr("spell_stmn_cost", s_pcsb_spell.spell_stmn_cost);
				spellNode->getAttr("spell_sub_effect_id", s_pcsb_spell.spell_sub_effect_id);
				spellNode->getAttr("spell_force", s_pcsb_spell.spell_force);
				spellNode->getAttr("spell_sub_type", s_pcsb_spell.spell_sub_type);
				spellNode->getAttr("spell_cast_delay", s_pcsb_spell.spell_cast_delay);
				spellNode->getAttr("spell_cast_time_to_can_cast", s_pcsb_spell.spell_cast_time_to_can_cast);
				spellNode->getAttr("spell_cast_max_time_to_charge", s_pcsb_spell.spell_cast_max_time_to_charge);
				spellNode->getAttr("spell_charge_max_damage_mult", s_pcsb_spell.spell_charge_max_damege_mult);
				spellNode->getAttr("spell_charge_spec_effect", s_pcsb_spell.spell_charge_spec_effect);
				spellNode->getAttr("cast_after_charge_perm", s_pcsb_spell.cast_after_charge_perm);
				spellNode->getAttr("spell_projectles_dir_offset_randomize", s_pcsb_spell.spell_projectles_dir_offset_randomize);
				spellNode->getAttr("can_cast_w_weapon", s_pcsb_spell.can_cast_w_weapon);
				spellNode->getAttr("apply_weapon_mgc_fcr_mult", s_pcsb_spell.apply_weapon_mgc_fcr_mult);
				spellNode->getAttr("can_move_while_cast", s_pcsb_spell.can_move_while_cast);
				spellNode->getAttr("spell_cast_action_startcast", s_pcsb_spell.spell_cast_action_startcast[1]);
				spellNode->getAttr("spell_cast_action_chargecast", s_pcsb_spell.spell_cast_action_chargecast[1]);
				spellNode->getAttr("spell_cast_action_pre_endcast", s_pcsb_spell.spell_cast_action_pre_endcast[1]);
				spellNode->getAttr("spell_cast_action_endcast", s_pcsb_spell.spell_cast_action_endcast[1]);
				spellNode->getAttr("spell_cast_action_startcast2", s_pcsb_spell.spell_cast_action_startcast[2]);
				spellNode->getAttr("spell_cast_action_chargecast2", s_pcsb_spell.spell_cast_action_chargecast[2]);
				spellNode->getAttr("spell_cast_action_pre_endcast2", s_pcsb_spell.spell_cast_action_pre_endcast[2]);
				spellNode->getAttr("spell_cast_action_endcast2", s_pcsb_spell.spell_cast_action_endcast[2]);
				spellNode->getAttr("spell_cast_recoil", s_pcsb_spell.spell_cast_recoil);
				spellNode->getAttr("spell_act_scr_func_arg", s_pcsb_spell.spell_act_scr_func_arg[1]);
				spellNode->getAttr("spell_act_scr_func_arg2", s_pcsb_spell.spell_act_scr_func_arg[2]);
				spellNode->getAttr("spell_name", s_pcsb_spell.spell_name);
				spellNode->getAttr("two_handed_spell", s_pcsb_spell.two_handed_spell);
				for (int i = 0; i != 3; i++)
				{
					if (i == 1)
					{
						spellNode->getAttr("icon_prim", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone", s_pcsb_spell.spell_cast_bone[i]);
					}
					else if (i == 2)
					{
						spellNode->getAttr("icon_second", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description2", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper2", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone2", s_pcsb_spell.spell_cast_bone[i]);
					}
					else if (i > 2)
					{
						spellNode->getAttr("icon_nnc", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description3", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper3", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone3", s_pcsb_spell.spell_cast_bone[i]);
					}
				}
				string spell_particle_str = "spell_particle";
				string spell_helper_str = "spell_helper";
				string spell_light_str = "spell_light";
				string spell_particle_fp_str = "spell_particle_fp";
				string spell_helper_fp_str = "spell_helper_fp";
				for (int i = 0; i < 3; i++)
				{
					for (int ic = 0; ic < 5; ic++)
					{
						char B_valstr[17];
						itoa(ic, B_valstr, 10);
						string C_valstr = B_valstr;
						string A1_valstr = spell_particle_str + C_valstr;
						string A2_valstr = spell_helper_str + C_valstr;
						string A3_valstr = spell_light_str + C_valstr;
						string A4_valstr = spell_particle_fp_str + C_valstr;
						string A5_valstr = spell_helper_fp_str + C_valstr;
						if (i == 1)
						{
							A1_valstr = string("lft_") + A1_valstr;
							A2_valstr = string("lft_") + A2_valstr;
							A3_valstr = string("lft_") + A3_valstr;
							A4_valstr = string("lft_") + A4_valstr;
							A5_valstr = string("lft_") + A5_valstr;
						}
						else if (i == 2)
						{
							A1_valstr = string("rgt_") + A1_valstr;
							A2_valstr = string("rgt_") + A2_valstr;
							A3_valstr = string("rgt_") + A3_valstr;
							A4_valstr = string("rgt_") + A4_valstr;
							A5_valstr = string("rgt_") + A5_valstr;
						}
						else
						{

						}
						spellNode->getAttr(A1_valstr.c_str(), s_pcsb_spell.spell_particles[i][ic]);
						spellNode->getAttr(A2_valstr.c_str(), s_pcsb_spell.spell_helpers[i][ic]);
						spellNode->getAttr(A3_valstr.c_str(), s_pcsb_spell.spell_lights[i][ic]);
						spellNode->getAttr(A4_valstr.c_str(), s_pcsb_spell.spell_particles_fp[i][ic]);
						spellNode->getAttr(A5_valstr.c_str(), s_pcsb_spell.spell_helpers_fp[i][ic]);
					}
				}
				string spell_projectle_str = "spell_projectle";
				string spell_projectle_pos_off_str = "spell_projectles_pos_offset";
				string spell_projectle_dir_off_str = "spell_projectles_dir_offset";
				string spell_additive_effects_ids_str = "spell_additive_effect_id";
				for (int i = 0; i < 15; i++)
				{
					char B_valstr[17];
					itoa(i, B_valstr, 10);
					string C_valstr = B_valstr;
					string A1_valstr = spell_projectle_str + C_valstr;
					string A2_valstr = spell_projectle_pos_off_str + C_valstr;
					string A3_valstr = spell_projectle_dir_off_str + C_valstr;
					string A4_valstr = spell_additive_effects_ids_str + C_valstr;
					spellNode->getAttr(A1_valstr.c_str(), s_pcsb_spell.spell_projectles[i]);
					spellNode->getAttr(A2_valstr.c_str(), s_pcsb_spell.spell_projectles_pos_offset[i]);
					spellNode->getAttr(A3_valstr.c_str(), s_pcsb_spell.spell_projectles_dir_offset[i]);
					for (int ic = 0; ic < 15; ic++)
					{
						char Bc_valstr[17];
						itoa(ic, Bc_valstr, 10);
						string Cc_valstr = Bc_valstr;
						string AC_valstr = A4_valstr + string("k") + Cc_valstr;
						spellNode->getAttr(AC_valstr.c_str(), s_pcsb_spell.spell_additive_effects_ids[i][ic]);
					}
				}
				s_ActorSpells.push_back(s_pcsb_spell);
			}
		}
	}
}

void CGameXMLSettingAndGlobals::InitCharacterPerks()
{
	CharacterPerks_node = GetISystem()->LoadXmlFromFile(character_perks_path.c_str());
	if (!CharacterPerks_node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded. Character's perks(passive skills) file not loaded");
		return;
	}
}

void CGameXMLSettingAndGlobals::InitCharacterSkills()
{
	CharacterSkills_node = GetISystem()->LoadXmlFromFile(character_skills_path.c_str());
	if (!CharacterSkills_node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded. Character's skills file not loaded");
		return;
	}
}

void CGameXMLSettingAndGlobals::InitCustomGameVars()
{
	gEnv->pConsole->ExecuteString("e_PhysProxyTriLimit = 903001");
}

bool CGameXMLSettingAndGlobals::IsCharacterCreatingVis()
{
	if (g_pGame->GetHUDCommon())
	{
		return g_pGame->GetHUDCommon()->m_CharacterCreationVisible;
	}
	return enable_character_creating_menu_at_game_start;
}

void CGameXMLSettingAndGlobals::SetDialogMainNpc(EntityId main_npc)
{
	current_dialog_NPC = main_npc;
}

EntityId CGameXMLSettingAndGlobals::GetDialogMainNpc()
{
	return current_dialog_NPC;
}

CGameXMLSettingAndGlobals::SPlayerQuest::SPlayerQuest()
{
	m_questname = "";
	m_questpath = "";
	m_sdec = "";
	m_ldec = "";
	m_queststat = 0;
	m_lastcn = 0;
	m_questflag = 0;
	m_questid = 0;
	m_complete_percent = 0.0f;
	m_quest_target_ent_id = 0;
	m_quest_target_ent_name = "";
	m_quest_target_point = Vec3(ZERO);
}

CGameXMLSettingAndGlobals::SPlayerSubQuest::SPlayerSubQuest()
{
	m_parentquestname = "";
	m_questname = "";
	m_sdec = "";
	m_ldec = "";
	m_queststat = 0;
	m_lastcn = 0;
	m_questflag = 0;
	m_questid = 0;
	m_questpath = "";
	m_quest_target_ent_id = 0;
	m_quest_target_ent_name = "";
	m_quest_target_point = Vec3(ZERO);
}

void CGameXMLSettingAndGlobals::AddQuest(const char* questpath, int id, int stage, int flag, int last, int quest_lc_st, float compl_percent)
{
	string path = GetFullQuestPath(questpath);
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(path.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Quest system xml not loaded.");
		return;
	}
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef stagesNode = node->findChild("stages");
	XmlString questname;
	XmlString questshortdec;
	XmlString questlongdec;
	paramsNode->getAttr("questname", questname);
	paramsNode->getAttr("questshortdec", questshortdec);
	paramsNode->getAttr("questlongdec", questlongdec);
	string nmn = questname;
	string qsd = questshortdec;
	string qld = questlongdec;
	char B_valstr[17];
	itoa(quest_lc_st, B_valstr, 10);
	string r_stage = string("stage") + string(B_valstr);
	XmlNodeRef stage1Node = stagesNode->findChild(r_stage.c_str());
	SPlayerQuest quest;
	quest.m_questid = id;
	quest.m_questname = nmn;
	quest.m_sdec = qsd;
	quest.m_ldec = qld;
	quest.m_questflag = flag;
	quest.m_queststat = stage;
	quest.m_lastcn = last;
	quest.m_questpath = path;
	quest.m_complete_percent = compl_percent;
	if (!IsQuestInJournal(nmn))
		s_pl_Quests.push_back(quest);
}
//-------------------------------------------------------------------------

void CGameXMLSettingAndGlobals::DelQuest(const char* questpath, int id)
{
	
}

void CGameXMLSettingAndGlobals::UpdateQuest(const char* questpath, int id, int stage, int flag, int last, int old_std, int old_flg, int old_lst, int quest_lc_st, float compl_percent)
{
	string path = GetFullQuestPath(questpath);
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(path.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Quest system xml not loaded.");
		return;
	}
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef stagesNode = node->findChild("stages");
	XmlString questname;
	XmlString questshortdec;
	XmlString questlongdec;
	paramsNode->getAttr("questname", questname);
	paramsNode->getAttr("questshortdec", questshortdec);
	paramsNode->getAttr("questlongdec", questlongdec);
	string nmn = questname;
	string qsd = questshortdec;
	string qld = questlongdec;
	char B_valstr[17];
	itoa(quest_lc_st, B_valstr, 10);
	string r_stage = string("stage") + string(B_valstr);
	XmlNodeRef stage1Node = stagesNode->findChild(r_stage.c_str());
	SPlayerQuest quest_updated;
	quest_updated.m_questid = id;
	quest_updated.m_questname = nmn;
	quest_updated.m_sdec = qsd;
	quest_updated.m_ldec = qld;
	quest_updated.m_questflag = flag;
	quest_updated.m_queststat = stage;
	quest_updated.m_lastcn = last;
	quest_updated.m_complete_percent = compl_percent;
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SPlayerQuest quest = *it;
		if (quest.m_questname == questname)
		{
			s_pl_Quests.erase(it);
			s_pl_Quests.push_back(quest_updated);
			s_pl_Quests.resize(count);
			return;
		}
	}
	s_pl_Quests.push_back(quest_updated);
	s_pl_Quests.resize(count);
}

void CGameXMLSettingAndGlobals::AddSubQuest(const char* quest, int id, int stage, int last, int flag, int quest_lc_st)
{
	string path = GetFullQuestPath(quest);
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(path.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Quest system xml not loaded.");
		return;
	}
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef stagesNode = node->findChild("stages");
	XmlString questname;
	XmlString questshortdec;
	XmlString questlongdec;
	XmlString subquestname;
	XmlString subquestshortdec;
	XmlString subquestlongdec;
	int numsubquests;
	char Br_valstr[17];
	itoa(quest_lc_st, Br_valstr, 10);
	string r_stage = string("stage") + string(Br_valstr);
	XmlNodeRef stage1Node = stagesNode->findChild(r_stage.c_str());
	XmlNodeRef qnum1Node = stage1Node->findChild("numsubquests");
	char B_valstr[17];
	itoa(id, B_valstr, 10);
	string C_valstr = B_valstr;
	string A_valstr;
	A_valstr = "quest";
	string D_valstr = A_valstr + C_valstr;
	XmlNodeRef sqNode = stage1Node->findChild(D_valstr.c_str());
	sqNode->getAttr("subquestname", subquestname);
	sqNode->getAttr("subquestshortdec", subquestshortdec);
	sqNode->getAttr("subquestlongdec", subquestlongdec);
	qnum1Node->getAttr("numsubquests", numsubquests);
	paramsNode->getAttr("questname", questname);
	paramsNode->getAttr("questshortdec", questshortdec);
	paramsNode->getAttr("questlongdec", questlongdec);
	string pnmn = questname;
	string nmn = subquestname;
	string qsd = subquestshortdec;
	string qld = subquestlongdec;
	SPlayerSubQuest subquest;
	subquest.m_questid = id;
	subquest.m_questname = nmn;
	subquest.m_sdec = qsd;
	subquest.m_ldec = qld;
	subquest.m_parentquestname = pnmn;
	subquest.m_questflag = flag;
	subquest.m_queststat = stage;
	subquest.m_lastcn = last;
	subquest.m_questpath = path;
	if(!IsSubQuestInJournal(nmn))
		s_pl_SubQuests.push_back(subquest);
}

void CGameXMLSettingAndGlobals::DelSubQuest(const char* quest, int id)
{

}

void CGameXMLSettingAndGlobals::UpdateSubQuest(const char* quest, int id, int stage, int flag, int last, int old_std, int old_flg, int old_lst, int quest_lc_st)
{
	string path = GetFullQuestPath(quest);
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(path.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Quest system xml not loaded.");
		return;
	}
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef stagesNode = node->findChild("stages");
	XmlString questname;
	XmlString questshortdec;
	XmlString questlongdec;
	XmlString subquestname;
	XmlString subquestshortdec;
	XmlString subquestlongdec;
	int numsubquests;
	char Br_valstr[17];
	itoa(quest_lc_st, Br_valstr, 10);
	string r_stage = string("stage") + string(Br_valstr);
	XmlNodeRef stage1Node = stagesNode->findChild(r_stage.c_str());
	XmlNodeRef qnum1Node = stage1Node->findChild("numsubquests");
	char B_valstr[17];
	itoa(id, B_valstr, 10);
	string C_valstr = B_valstr;
	string A_valstr;
	A_valstr = "quest";
	string D_valstr = A_valstr + C_valstr;

	XmlNodeRef sqNode = stage1Node->findChild(D_valstr.c_str());
	sqNode->getAttr("subquestname", subquestname);
	sqNode->getAttr("subquestshortdec", subquestshortdec);
	sqNode->getAttr("subquestlongdec", subquestlongdec);
	qnum1Node->getAttr("numsubquests", numsubquests);
	paramsNode->getAttr("questname", questname);
	paramsNode->getAttr("questshortdec", questshortdec);
	paramsNode->getAttr("questlongdec", questlongdec);
	string pnmn = questname;
	string nmn = subquestname;
	string qsd = subquestshortdec;
	string qld = subquestlongdec;
	SPlayerSubQuest subquest_updated;
	subquest_updated.m_questid = id;
	subquest_updated.m_questname = nmn;
	subquest_updated.m_sdec = qsd;
	subquest_updated.m_ldec = qld;
	subquest_updated.m_parentquestname = pnmn;
	subquest_updated.m_questflag = flag;
	subquest_updated.m_queststat = stage;
	subquest_updated.m_lastcn = last;
	subquest_updated.m_questpath = path;
	std::vector<SPlayerSubQuest>::iterator it = s_pl_SubQuests.begin();
	std::vector<SPlayerSubQuest>::iterator end = s_pl_SubQuests.end();
	int count = s_pl_SubQuests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SPlayerSubQuest subquest = *it;
		if (subquest.m_questname == subquestname)
		{
			s_pl_SubQuests.erase(it);
			s_pl_SubQuests.resize(count - 1);
			s_pl_SubQuests.push_back(subquest_updated);
			return;
		}
	}
}

void CGameXMLSettingAndGlobals::AddQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st, float compl_percent)
{
	SPlayerQuest quest;
	quest.m_questid = id;
	quest.m_questname = quest_name;
	quest.m_sdec = quest_dec[0];
	quest.m_ldec = quest_dec[1];
	quest.m_questflag = flag;
	quest.m_queststat = stage;
	quest.m_lastcn = last;
	quest.m_questpath = "";
	quest.m_complete_percent = compl_percent;
	if (!IsQuestInJournal(quest.m_questname))
		s_pl_Quests.push_back(quest);
}

void CGameXMLSettingAndGlobals::DelQuestNoXML(string quest_name)
{

}

void CGameXMLSettingAndGlobals::UpdateQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st, float compl_percent)
{
	SPlayerQuest quest_updated;
	quest_updated.m_questid = id;
	quest_updated.m_questname = quest_name;
	quest_updated.m_sdec = quest_dec[0];
	quest_updated.m_ldec = quest_dec[1];
	quest_updated.m_questflag = flag;
	quest_updated.m_queststat = stage;
	quest_updated.m_lastcn = last;
	quest_updated.m_complete_percent = compl_percent;
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SPlayerQuest quest = *it;
		if (quest.m_questname == quest_updated.m_questname)
		{
			s_pl_Quests.erase(it);
			s_pl_Quests.push_back(quest_updated);
			s_pl_Quests.resize(count);
			return;
		}
	}
	s_pl_Quests.push_back(quest_updated);
	s_pl_Quests.resize(count);
}

void CGameXMLSettingAndGlobals::AddSubQuestNoXML(string parent_quest_name, string quest_name, string quest_dec[2], int id, int stage, int last, int flag, int quest_lc_st)
{
	SPlayerSubQuest subquest;
	subquest.m_questid = id;
	subquest.m_questname = quest_name;
	subquest.m_sdec = quest_dec[0];
	subquest.m_ldec = quest_dec[1];
	subquest.m_parentquestname = parent_quest_name;
	subquest.m_questflag = flag;
	subquest.m_queststat = stage;
	subquest.m_lastcn = last;
	subquest.m_questpath = "";
	if (!IsSubQuestInJournal(subquest.m_questname))
		s_pl_SubQuests.push_back(subquest);
}

void CGameXMLSettingAndGlobals::DelSubQuestNoXML(string quest_name)
{

}

void CGameXMLSettingAndGlobals::UpdateSubQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st)
{
	SPlayerSubQuest subquest_updated;
	subquest_updated.m_questid = id;
	subquest_updated.m_questname = quest_name;
	subquest_updated.m_sdec = quest_dec[0];
	subquest_updated.m_ldec = quest_dec[1];
	subquest_updated.m_questflag = flag;
	subquest_updated.m_queststat = stage;
	subquest_updated.m_lastcn = last;
	std::vector<SPlayerSubQuest>::iterator it = s_pl_SubQuests.begin();
	std::vector<SPlayerSubQuest>::iterator end = s_pl_SubQuests.end();
	int count = s_pl_SubQuests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SPlayerSubQuest subquest = *it;
		if (subquest.m_questname == quest_name)
		{
			subquest_updated.m_parentquestname = subquest.m_parentquestname;
			subquest_updated.m_questpath = subquest.m_questpath;
			s_pl_SubQuests.erase(it);
			s_pl_SubQuests.resize(count - 1);
			s_pl_SubQuests.push_back(subquest_updated);
			return;
		}
	}
}

float CGameXMLSettingAndGlobals::GetQuestCompletePercent(string quest_name)
{
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questname == quest_name)
		{
			return (*it).m_complete_percent;
		}
	}
	return 0.0f;
}

void CGameXMLSettingAndGlobals::SetQuestCompletePercent(string quest_name, float percent)
{
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questname == quest_name)
		{
			(*it).m_complete_percent = percent;
			break;
		}
	}
}

float CGameXMLSettingAndGlobals::GetQuestCompletePercentById(int quest_id)
{
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questid == quest_id)
		{
			return (*it).m_complete_percent;
		}
	}
	return 0.0f;
}

void CGameXMLSettingAndGlobals::SetQuestCompletePercentById(int quest_id, float percent)
{
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questid == quest_id)
		{
			(*it).m_complete_percent = percent;
			break;
		}
	}
}

bool CGameXMLSettingAndGlobals::IsSubQuestInJournal(string quest_name)
{
	std::vector<SPlayerSubQuest>::iterator it = s_pl_SubQuests.begin();
	std::vector<SPlayerSubQuest>::iterator end = s_pl_SubQuests.end();
	int count = s_pl_SubQuests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questname == quest_name)
		{
			return true;
		}
	}
	return false;
}
bool CGameXMLSettingAndGlobals::IsQuestInJournal(string quest_name)
{
	std::vector<SPlayerQuest>::iterator it = s_pl_Quests.begin();
	std::vector<SPlayerQuest>::iterator end = s_pl_Quests.end();
	int count = s_pl_Quests.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).m_questname == quest_name)
		{
			return true;
		}
	}
	return false;
}
string CGameXMLSettingAndGlobals::GetFullQuestPath(const char * quest)
{
	string ext = PathUtil::GetExt(quest);
	if (!ext.empty())
		return quest;

	string qn = string("scripts\\GameRules\\Quests") + string("\\");
	//qn.erase(qn.size(),1);//m26
	CryLogAlways("%s%s%s", qn.c_str(), string(quest).c_str(), string(XMLEXTENSION).c_str());
	return qn + string(quest) + string(XMLEXTENSION);
}
string CGameXMLSettingAndGlobals::GetPerkScrFuncOnUp(int realId)
{
	XmlNodeRef node = CharacterPerks_node;
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Abilitys xml not loaded.");
		return string();
	}
	int real_id = realId;
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef infopathNode = node->findChild("info");
	XmlNodeRef perksNode = infopathNode->findChild("perks");
	int rl_id = -1;
	XmlString on_level_up_scr_funtion;
	int perks_number = -1;
	paramsNode->getAttr("perks_num", perks_number);
	for (int i = 0; i < perks_number; i++)
	{
		XmlNodeRef perkNode = perksNode->getChild(i);
		if (perkNode)
		{
			perkNode->getAttr("real_id", rl_id);
			if (rl_id == real_id)
			{
				perkNode->getAttr("on_level_up_scr_funtion", on_level_up_scr_funtion);
				return on_level_up_scr_funtion;
			}
		}
	}
	return string();
}
string CGameXMLSettingAndGlobals::GetPerkScrFuncOnDown(int realId)
{
	XmlNodeRef node = CharacterPerks_node;
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Abilitys xml not loaded.");
		return string();
	}
	int real_id = realId;
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef infopathNode = node->findChild("info");
	XmlNodeRef perksNode = infopathNode->findChild("perks");
	int rl_id = -1;
	XmlString on_level_down_scr_funtion;
	int perks_number = -1;
	paramsNode->getAttr("perks_num", perks_number);
	for (int i = 0; i < perks_number; i++)
	{
		XmlNodeRef perkNode = perksNode->getChild(i);
		if (perkNode)
		{
			perkNode->getAttr("real_id", rl_id);
			if (rl_id == real_id)
			{
				perkNode->getAttr("on_level_down_scr_funtion", on_level_down_scr_funtion);
				return on_level_down_scr_funtion;
			}
		}
	}
	return string();
}
//safe function to get pointer to CActor class instance
CActor* CGameXMLSettingAndGlobals::GetThisActor(EntityId entityId)
{
	static IActorSystem* pActorSys = 0;
	if (pActorSys == 0)
		pActorSys = g_pGame->GetIGameFramework()->GetIActorSystem();

	if (pActorSys != 0)
	{
		IActor* pIActor = pActorSys->GetActor(entityId);
		if (pIActor)
		{
			CActor* pActor = (CActor*)pIActor;
			return static_cast<CActor*> (pActor);
		}
	}
	return 0;
}
//safe function to get pointer to CItem class instance
CItem* CGameXMLSettingAndGlobals::GetThisItem(EntityId itm_id)
{
	static IItemSystem	*pItemSystem = 0;
	if (pItemSystem == 0)
		pItemSystem = gEnv->pGame->GetIGameFramework()->GetIItemSystem();

	if (pItemSystem != 0)
	{
		IItem* pIItem = pItemSystem->GetItem(itm_id);
		if (pIItem)
		{
			CItem* pItem = (CItem*)pIItem;
			return static_cast<CItem*> (pItem);
		}
	}
	return 0;
}

SActorBuff* CGameXMLSettingAndGlobals::GetCharacterBuffInfo(int id)
{
	//CryLogAlways("GetCharacterBuffInfo requested id - %i", id);
	SActorBuff* pActorBuff = new(SActorBuff);
	pActorBuff->buff_effect_id = 0;
	pActorBuff->buff_icon_path = "";
	pActorBuff->buff_id = 0;
	pActorBuff->buff_type = 0;
	XmlNodeRef node = CharacterBuffs_node;
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded(GetCharacterBuffInfo).");
		return pActorBuff;
	}

	//if (id <= 0)
	//	return pActorBuff;

	//id = id - 1;
	XmlNodeRef infopathNode = node->findChild("info");
	if (!infopathNode)
		return pActorBuff;

	XmlNodeRef buffsNode = infopathNode->findChild("buffs");
	if (!buffsNode)
		return pActorBuff;

	int childs_num = buffsNode->getChildCount();
	for (int i = 0; i < childs_num; i++)
	{
		XmlNodeRef buffNode = buffsNode->getChild(i);
		if (!buffNode)
			return pActorBuff;

		int ids = -1;
		buffNode->getAttr("id", ids);
		if (ids == id)
		{
			buffNode->getAttr("icon", pActorBuff->buff_icon_path);
			buffNode->getAttr("id", pActorBuff->buff_id);
			buffNode->getAttr("effect_id", pActorBuff->buff_effect_id);
			buffNode->getAttr("type", pActorBuff->buff_type);
			buffNode->getAttr("start_time", pActorBuff->start_time);
			string spec_val_name = "r_val";
			string spec_val_q_name = "r_val_q";
			for (int i = 1; i < 10; i++)
			{
				char B_valstr[17];
				itoa(i, B_valstr, 10);
				string C_valstr = B_valstr;
				string A1_valstr = spec_val_name + C_valstr;
				string A2_valstr = spec_val_q_name + C_valstr;
				buffNode->getAttr(A1_valstr.c_str(), pActorBuff->r_val[i]);
				buffNode->getAttr(A2_valstr.c_str(), pActorBuff->r_val_c[i]);
			}
			return pActorBuff;
			//CryLogAlways("GetCharacterBuffInfo id - %i effect_id - %i type - %i", pActorBuff->buff_id, pActorBuff->buff_effect_id, pActorBuff->buff_type);
		}
	}
	return pActorBuff;
}

CGameXMLSettingAndGlobals::SActorSpell CGameXMLSettingAndGlobals::GetCharacterSpellInfo(int id, bool from_xml)
{
	SActorSpell s_pcsb_spell;
	s_pcsb_spell.spell_id = -1;
	XmlNodeRef node = CharacterSpells_node;
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded. Character's spells file not loaded(GetCharacterSpellInfo).");
		return s_pcsb_spell;
	}

	if (from_xml)
	{
		XmlNodeRef infopathNode = node->findChild("info");
		if (!infopathNode)
			return s_pcsb_spell;

		XmlNodeRef spellsNode = infopathNode->findChild("spells");
		if (!spellsNode)
			return s_pcsb_spell;

		int childs_num = spellsNode->getChildCount();
		for (int i = 0; i < childs_num; i++)
		{
			XmlNodeRef spellNode = spellsNode->getChild(i);
			if (!spellNode)
				continue;

			int ids = -1;
			spellNode->getAttr("id", ids);
			if (ids == id)
			{
				spellNode->getAttr("id", s_pcsb_spell.spell_id);
				spellNode->getAttr("effect_id", s_pcsb_spell.spell_effect_id);
				spellNode->getAttr("spell_type", s_pcsb_spell.spell_type);
				spellNode->getAttr("start_time", s_pcsb_spell.start_time);
				spellNode->getAttr("dmg", s_pcsb_spell.spell_damage);
				spellNode->getAttr("spell_chargeable", s_pcsb_spell.spell_chargeable);
				spellNode->getAttr("spell_act_scr_func", s_pcsb_spell.spell_act_scr_func[1]);
				spellNode->getAttr("spell_act_scr_func2", s_pcsb_spell.spell_act_scr_func[2]);
				spellNode->getAttr("spell_mana_cost", s_pcsb_spell.spell_mana_cost);
				spellNode->getAttr("spell_hp_cost", s_pcsb_spell.spell_hp_cost);
				spellNode->getAttr("spell_stmn_cost", s_pcsb_spell.spell_stmn_cost);
				spellNode->getAttr("spell_sub_effect_id", s_pcsb_spell.spell_sub_effect_id);
				spellNode->getAttr("spell_force", s_pcsb_spell.spell_force);
				spellNode->getAttr("spell_sub_type", s_pcsb_spell.spell_sub_type);
				spellNode->getAttr("spell_cast_delay", s_pcsb_spell.spell_cast_delay);
				spellNode->getAttr("spell_cast_time_to_can_cast", s_pcsb_spell.spell_cast_time_to_can_cast);
				spellNode->getAttr("spell_cast_max_time_to_charge", s_pcsb_spell.spell_cast_max_time_to_charge);
				spellNode->getAttr("spell_charge_max_damage_mult", s_pcsb_spell.spell_charge_max_damege_mult);
				spellNode->getAttr("spell_charge_spec_effect", s_pcsb_spell.spell_charge_spec_effect);
				spellNode->getAttr("cast_after_charge_perm", s_pcsb_spell.cast_after_charge_perm);
				spellNode->getAttr("spell_projectles_dir_offset_randomize", s_pcsb_spell.spell_projectles_dir_offset_randomize);
				spellNode->getAttr("can_cast_w_weapon", s_pcsb_spell.can_cast_w_weapon);
				spellNode->getAttr("apply_weapon_mgc_fcr_mult", s_pcsb_spell.apply_weapon_mgc_fcr_mult);
				spellNode->getAttr("can_move_while_cast", s_pcsb_spell.can_move_while_cast);
				spellNode->getAttr("spell_cast_action_startcast", s_pcsb_spell.spell_cast_action_startcast[1]);
				spellNode->getAttr("spell_cast_action_chargecast", s_pcsb_spell.spell_cast_action_chargecast[1]);
				spellNode->getAttr("spell_cast_action_pre_endcast", s_pcsb_spell.spell_cast_action_pre_endcast[1]);
				spellNode->getAttr("spell_cast_action_endcast", s_pcsb_spell.spell_cast_action_endcast[1]);
				spellNode->getAttr("spell_cast_action_startcast2", s_pcsb_spell.spell_cast_action_startcast[2]);
				spellNode->getAttr("spell_cast_action_chargecast2", s_pcsb_spell.spell_cast_action_chargecast[2]);
				spellNode->getAttr("spell_cast_action_pre_endcast2", s_pcsb_spell.spell_cast_action_pre_endcast[2]);
				spellNode->getAttr("spell_cast_action_endcast2", s_pcsb_spell.spell_cast_action_endcast[2]);
				spellNode->getAttr("spell_cast_recoil", s_pcsb_spell.spell_cast_recoil);
				spellNode->getAttr("spell_act_scr_func_arg", s_pcsb_spell.spell_act_scr_func_arg[1]);
				spellNode->getAttr("spell_act_scr_func_arg2", s_pcsb_spell.spell_act_scr_func_arg[2]);
				spellNode->getAttr("spell_name", s_pcsb_spell.spell_name);
				spellNode->getAttr("two_handed_spell", s_pcsb_spell.two_handed_spell);
				for (int i = 0; i != 3; i++)
				{
					if (i == 1)
					{
						spellNode->getAttr("icon_prim", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone", s_pcsb_spell.spell_cast_bone[i]);
					}
					else if (i == 2)
					{
						spellNode->getAttr("icon_second", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description2", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper2", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone2", s_pcsb_spell.spell_cast_bone[i]);
					}
					else if (i > 2)
					{
						spellNode->getAttr("icon_nnc", s_pcsb_spell.spell_icon_path[i]);
						spellNode->getAttr("spell_description3", s_pcsb_spell.spell_description[i]);
						spellNode->getAttr("spell_cast_helper3", s_pcsb_spell.spell_cast_helper[i]);
						spellNode->getAttr("spell_cast_bone3", s_pcsb_spell.spell_cast_bone[i]);
					}
				}
				string spell_particle_str = "spell_particle";
				string spell_helper_str = "spell_helper";
				string spell_light_str = "spell_light";
				string spell_particle_fp_str = "spell_particle_fp";
				string spell_helper_fp_str = "spell_helper_fp";
				for (int i = 0; i < 3; i++)
				{
					for (int ic = 0; ic < 5; ic++)
					{
						char B_valstr[17];
						itoa(ic, B_valstr, 10);
						string C_valstr = B_valstr;
						string A1_valstr = spell_particle_str + C_valstr;
						string A2_valstr = spell_helper_str + C_valstr;
						string A3_valstr = spell_light_str + C_valstr;
						string A4_valstr = spell_particle_fp_str + C_valstr;
						string A5_valstr = spell_helper_fp_str + C_valstr;
						if (i == 1)
						{
							A1_valstr = string("lft_") + A1_valstr;
							A2_valstr = string("lft_") + A2_valstr;
							A3_valstr = string("lft_") + A3_valstr;
							A4_valstr = string("lft_") + A4_valstr;
							A5_valstr = string("lft_") + A5_valstr;
						}
						else if (i == 2)
						{
							A1_valstr = string("rgt_") + A1_valstr;
							A2_valstr = string("rgt_") + A2_valstr;
							A3_valstr = string("rgt_") + A3_valstr;
							A4_valstr = string("rgt_") + A4_valstr;
							A5_valstr = string("rgt_") + A5_valstr;
						}
						else
						{

						}
						spellNode->getAttr(A1_valstr.c_str(), s_pcsb_spell.spell_particles[i][ic]);
						spellNode->getAttr(A2_valstr.c_str(), s_pcsb_spell.spell_helpers[i][ic]);
						spellNode->getAttr(A3_valstr.c_str(), s_pcsb_spell.spell_lights[i][ic]);
						spellNode->getAttr(A4_valstr.c_str(), s_pcsb_spell.spell_particles_fp[i][ic]);
						spellNode->getAttr(A5_valstr.c_str(), s_pcsb_spell.spell_helpers_fp[i][ic]);
					}
				}
				string spell_projectle_str = "spell_projectle";
				string spell_projectle_pos_off_str = "spell_projectles_pos_offset";
				string spell_projectle_dir_off_str = "spell_projectles_dir_offset";
				string spell_additive_effects_ids_str = "spell_additive_effect_id";
				for (int i = 0; i < 15; i++)
				{
					char B_valstr[17];
					itoa(i, B_valstr, 10);
					string C_valstr = B_valstr;
					string A1_valstr = spell_projectle_str + C_valstr;
					string A2_valstr = spell_projectle_pos_off_str + C_valstr;
					string A3_valstr = spell_projectle_dir_off_str + C_valstr;
					string A4_valstr = spell_additive_effects_ids_str + C_valstr;
					spellNode->getAttr(A1_valstr.c_str(), s_pcsb_spell.spell_projectles[i]);
					spellNode->getAttr(A2_valstr.c_str(), s_pcsb_spell.spell_projectles_pos_offset[i]);
					spellNode->getAttr(A3_valstr.c_str(), s_pcsb_spell.spell_projectles_dir_offset[i]);
					for (int ic = 0; ic < 15; ic++)
					{
						char Bc_valstr[17];
						itoa(ic, Bc_valstr, 10);
						string Cc_valstr = Bc_valstr;
						string AC_valstr = A4_valstr + string("k") + Cc_valstr;
						spellNode->getAttr(AC_valstr.c_str(), s_pcsb_spell.spell_additive_effects_ids[i][ic]);
					}
				}
				break;
			}
		}
	}
	else
	{
		std::vector<SActorSpell>::iterator it = s_ActorSpells.begin();
		std::vector<SActorSpell>::iterator end = s_ActorSpells.end();
		int count = s_ActorSpells.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			SActorSpell s_act_cur_spell = *it;
			if (s_act_cur_spell.spell_id == id)
			{
				s_pcsb_spell = s_act_cur_spell;
				break;
			}
		}
	}

	return s_pcsb_spell;
}

Vec3 CGameXMLSettingAndGlobals::GetCurrentPlayerPos()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return Vec3(ZERO);

	return pActor->GetEntity()->GetPos();
}

Quat CGameXMLSettingAndGlobals::GetCurrentPlayerRot()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return Quat(IDENTITY);

	return pActor->GetEntity()->GetRotation();
}

CChest * CGameXMLSettingAndGlobals::GetThisContainer(EntityId container_id)
{
	CChest *pChest = static_cast<CChest*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(container_id, "Chest"));
	if (pChest)
	{
		return static_cast<CChest*>(pChest);
	}
	return nullptr;
}

void CGameXMLSettingAndGlobals::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}
