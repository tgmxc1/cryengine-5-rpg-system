
/*

#include "StdAfx.h"
#include "MeleeAlt.h"
#include "Game.h"
#include "Item.h"
#include "Weapon.h"
#include "GameRules.h"
#include "Player.h"
#include "BulletTime.h"
#include <IEntitySystem.h>
#include "IMaterialEffects.h"
#include "GameCVars.h"
#include "PlayerInput.h"


#include "IRenderer.h"
#include "IRenderAuxGeom.h"	




//------------------------------------------------------------------------
CMeleeAlt::CMeleeAlt()
{
	m_noImpulse = false;
	m_animspeed_returned = true;
}

//------------------------------------------------------------------------
CMeleeAlt::~CMeleeAlt()
{
}

const float STRENGTH_MULT = 0.018f;

void CMeleeAlt::Init(IWeapon *pWeapon, const struct IItemParamsNode *params)
{
	m_pWeapon = static_cast<CWeapon *>(pWeapon);

	if (params)
		ResetParams(params);

	m_attacking = false;
	m_attacked = false;
	m_delayTimer=0.0f;
	m_durationTimer=0.0f;
	m_ignoredEntity = 0;
	m_meleeScale = 1.0f;
	m_hold_timer = 0.0f;
	m_atkn=false;
	m_pulling=false;
	m_atking=false;
	m_holdedf = false;
	m_holdedb = false;
	m_holdedl = false;
	m_holdedr = false;
	m_updtimer=0;
	m_hitedEntites.clear();
	num_hitedEntites=0;
	m_crt_hit_dmg_mult=1;
	m_updtimer2=0;
	
}

//------------------------------------------------------------------------
void CMeleeAlt::Update(float frameTime, uint frameId)
{
	FUNCTION_PROFILER( GetISystem(), PROFILE_GAME );

	bool requireUpdate = false;
		CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());

		if(!pOwner || pOwner->GetHealth()<=0)
		return;
		CPlayer *pPlayer = (CPlayer *)pOwner;
		
			if(m_updtimer>0.1f)
			{
			m_updtimer-=frameTime;
			if (m_updtimer<=0.1f)
				m_updtimer=0.0f;
			}
			
			

			if(m_updtimer>0.02)
			{
				CActor *pActor = m_pWeapon->GetOwnerActor();
					
					
	
					float strength = 1.0f; 
				
							float dmgmult = 0.1f;
							strength = dmgmult;
				strength = strength * (0.2f + 0.4f * STRENGTH_MULT);
					IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);
					if(str > pActor->GetActorStrength())
					{
						strength *= str;
					}
					else if(str < pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
					else if(str == pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
					strength = GetOwnerStrength();
					IEntityRenderProxy* pRenderProxy = (IEntityRenderProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
		AABB bbox;
		pRenderProxy->GetWorldBounds(bbox);
		IPhysicalWorld *pWorld = gEnv->pSystem->GetIPhysicalWorld();
		IPhysicalEntity **ppColliders;
		Vec3 dir = bbox.max;
		Vec3 pos = bbox.min;
		
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
		if(pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers()==0)
		{
			string helperstart = "weapon";
			string helperend = "weapon_term";
			Vec3 position(0,0,0);
			Vec3 positionl(0,0,0);
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(position);
			
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			IEntity *pOwner = m_pWeapon->GetOwner();
			IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
			IEntity *pHeldObject = NULL;

			if(m_ignoredEntity)
				pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
			PerformRayTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), strength, false);
			PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), strength, true);
			m_updtimer2-=frameTime;
			if (m_updtimer2<=0.0f)
			{
				m_updtimer2=0.2f;
			PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), strength, false);
			}

		}
		else
		{
			IEntity *pOwner = m_pWeapon->GetOwner();
			IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
			IEntity *pHeldObject = NULL;

			if(m_ignoredEntity)
				pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);

			
			PerformRayTest(pos, dir, strength, false);
			PerformRayTest2(pos, dir, strength, true);
			m_updtimer2-=frameTime;
			if (m_updtimer2<=0.0f)
			{
				m_updtimer2=0.2f;
				PerformRayTest2(pos, dir, strength, false);
			}
		}
			}


				if(m_durationTimer>0)
				{
			
				}
				else
				{
			
				}


				if(m_delayTimer>0.07)
				{
			
			ICharacterInstance *pOwnerCharacter = pPlayer->GetEntity()->GetCharacter(0);
			pOwnerCharacter->SetAnimationSpeed(m_meleeparams.animspdmult);
			m_animspeed_returned = false;
				}
				else if(m_delayTimer>=0.01f && m_delayTimer<=0.07f)
				{
			
			ICharacterInstance *pOwnerCharacter = pPlayer->GetEntity()->GetCharacter(0);
			pOwnerCharacter->SetAnimationSpeed(1);
			m_animspeed_returned = true;
			
				}
				else if(!m_animspeed_returned && m_delayTimer<=0)
				{
					ICharacterInstance *pCharacter = pOwner->GetEntity()->GetCharacter(0);
					pCharacter->SetAnimationSpeed(1);
					m_animspeed_returned = true;
				}
	
	if (m_attacking)
	{
		requireUpdate = true;
		if (!m_pulling && !m_atking && !m_atkn)
		{
			if (m_hold_timer>0.0f)
			{


				m_hold_timer-=frameTime;
				if (m_hold_timer<=0.0f)
					m_hold_timer=0.0f;
			}
		}

	
		if (m_delayTimer>0.0f)
		{


			m_delayTimer-=frameTime;
			if (m_delayTimer<=0.0f)
				m_delayTimer=0.0f;
		}
		else
		{
			

			if (!m_attacked)
			{
			
				
			}
		}
		if (m_atking && m_atk_time<=0.0f)
		{
		}
		else if (m_atkn && m_atk_time<=0.0f)
		{
			m_pWeapon->SetBusy(false);
			
			

			m_attacking = false;
			m_atking = false;
			m_atkn = false;
		}

		m_atk_time -= frameTime;
		if (m_atk_time<0.0f)
			m_atk_time=0.0f;


	}
	
	if (requireUpdate)
		m_pWeapon->RequireUpdate(eIUS_FireMode);
}

//------------------------------------------------------------------------
void CMeleeAlt::Release()
{
	delete this;
}

//------------------------------------------------------------------------
void CMeleeAlt::ResetParams(const struct IItemParamsNode *params)
{
	const IItemParamsNode *melee = params?params->GetChild("melee"):0;
	const IItemParamsNode *actions = params?params->GetChild("actions"):0;

	m_meleeparams.Reset(melee);
	m_meleeactions.Reset(actions);
}

//------------------------------------------------------------------------
void CMeleeAlt::PatchParams(const struct IItemParamsNode *patch)
{
	const IItemParamsNode *melee = patch->GetChild("melee");
	const IItemParamsNode *actions = patch->GetChild("actions");

	m_meleeparams.Reset(melee, false);
	m_meleeactions.Reset(actions, false);
}

//------------------------------------------------------------------------
void CMeleeAlt::Activate(bool activate)
{
	m_hold_timer=0.0f;

	m_atkn=false;
	m_pulling=false;
	m_atking=false;
	m_attacking = m_noImpulse = false;
	m_delayTimer=0.0f;
	m_durationTimer=0.0f;
}

//------------------------------------------------------------------------
bool CMeleeAlt::CanFire(bool considerAmmo) const
{
	return !m_attacking && m_delayTimer <= 0 && m_durationTimer <= 0;
}

//------------------------------------------------------------------------
struct CMeleeAlt::StartAttackingAction
{
	StartAttackingAction(CMeleeAlt *_meleealt): pmeleealt(_meleealt) {};
	CMeleeAlt *pmeleealt;

	void execute(CItem *_this)
	{
		pmeleealt->m_pulling = false;
		CActor* pOwner = static_cast<CActor *>(_this->GetOwnerActor());
		CPlayer *pPlayer = (CPlayer *)pOwner;
		CPlayerInput *pPlayerInput = static_cast<CPlayerInput *>(pPlayer->GetPlayerInput());
		if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Forward)
		{
		pmeleealt->m_pWeapon->PlayAction(pmeleealt->m_meleeactions.holdf, 0, true, CItem::eIPAF_Default|CItem::eIPAF_NoBlend);
		pmeleealt->m_holdedf = true;
		pmeleealt->m_holdedb = false;
		pmeleealt->m_holdedl = false;
		pmeleealt->m_holdedr = false;
		}
		else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Back)
		{
		pmeleealt->m_pWeapon->PlayAction(pmeleealt->m_meleeactions.holdb, 0, true, CItem::eIPAF_Default|CItem::eIPAF_NoBlend);
		pmeleealt->m_holdedf = false;
		pmeleealt->m_holdedb = true;
		pmeleealt->m_holdedl = false;
		pmeleealt->m_holdedr = false;
		}
		else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Left)
		{
		pmeleealt->m_pWeapon->PlayAction(pmeleealt->m_meleeactions.holdl, 0, true, CItem::eIPAF_Default|CItem::eIPAF_NoBlend);
		pmeleealt->m_holdedf = false;
		pmeleealt->m_holdedb = false;
		pmeleealt->m_holdedl = true;
		pmeleealt->m_holdedr = false;
		}
		else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Right)
		{
		pmeleealt->m_pWeapon->PlayAction(pmeleealt->m_meleeactions.holdr, 0, true, CItem::eIPAF_Default|CItem::eIPAF_NoBlend);
		pmeleealt->m_holdedf = false;
		pmeleealt->m_holdedb = false;
		pmeleealt->m_holdedl = false;
		pmeleealt->m_holdedr = true;
		}
		else
		{
		pmeleealt->m_pWeapon->PlayAction(pmeleealt->m_meleeactions.hold, 0, true, CItem::eIPAF_Default|CItem::eIPAF_NoBlend);
		pmeleealt->m_holdedf = false;
		pmeleealt->m_holdedb = false;
		pmeleealt->m_holdedl = false;
		pmeleealt->m_holdedr = false;
		}
	}
};

struct CMeleeAlt::StopAttackingAction
{
	CMeleeAlt *_this;
	StopAttackingAction(CMeleeAlt *melee): _this(melee) {};
	//CMeleeAlt *pmeleealt;
	void execute(CItem *pItem)
	{
		_this->m_atkn = false;
		_this->m_attacking = false;
		_this->m_attacked = false;
		_this->m_atking = false;

		_this->m_delayTimer = 0.0f;
		_this->m_durationTimer = 0.0f;
		_this->m_hold_timer = 0.0f;
		_this->m_atk_time=0.0f;
		pItem->SetBusy(false);
		

	}
};

void CMeleeAlt::StartFire()
{
	if (CanFire(true) && !m_attacking && !m_atking && !m_pulling)
	{
		
		m_attacking = true;
		m_pulling = true;
		m_atking = false;
		m_atkn = false;
			
		m_attacked = false;
		

	

		
		m_pWeapon->SetBusy(true);
		m_pWeapon->PlayAction(m_meleeactions.pull);
		m_pWeapon->GetScheduler()->TimerAction(m_pWeapon->GetCurrentAnimationTime(CItem::eIGS_FirstPerson)+1, CSchedulerAction<StartAttackingAction>::Create(this), false);
		m_pWeapon->SetDefaultIdleAnimation(CItem::eIGS_FirstPerson, m_meleeactions.hold);
		
		m_hold_timer=m_meleeparams.hold_duration;

		
		

	m_pWeapon->RequestStartMeleeAttack(m_pWeapon->GetMeleeFireMode()==this);
	m_pWeapon->RequireUpdate(eIUS_FireMode);
	

	}
}

//------------------------------------------------------------------------
void CMeleeAlt::StopFire()
{

	if (m_attacking && !m_atking && !m_atkn)
	{
		num_hitedEntites=0;
		m_hitedEntites.clear();
		m_updtimer = m_meleeparams.ray_test_time;
		DoAttack();
		m_pWeapon->RequestStopFire();

		m_pWeapon->RequireUpdate(eIUS_FireMode);

	}
	
	
}

//------------------------------------------------------------------------
void CMeleeAlt::NetStartFire()
{
	
}

//------------------------------------------------------------------------
void CMeleeAlt::NetStopFire()
{
}

//------------------------------------------------------------------------
void CMeleeAlt::NetShoot(const Vec3 &hit, int ph)
{
}

//------------------------------------------------------------------------
void CMeleeAlt::DoAttack()
{


	m_atk_time = m_meleeparams.delay;
	CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	m_atking = true;
	m_atkn = false;


	bool isClient = pOwner?pOwner->IsClient():false;

	if (g_pGameCVars->bt_end_melee && isClient)
		g_pGame->GetBulletTime()->Activate(false);


	float speedOverride = -1.0f;

	if(CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor()))
	{
		CPlayer *pPlayer = (CPlayer *)pOwner;
			if (pPlayer->GetStamina() > 15.0f)
				speedOverride = 1.5f;
				speedOverride=g_pGameCVars->g_playerItemAtkSpeed;
			if (pPlayer->GetStamina() < 15.0f)
				speedOverride = 0.8f;

		
		pPlayer->PlaySound(CPlayer::ESound_Melee);
	}

	CPlayer *pPlayer = (CPlayer *)pOwner;
	CPlayerInput *pPlayerInput = static_cast<CPlayerInput *>(pPlayer->GetPlayerInput());


	if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Forward || m_holdedf)
	m_pWeapon->PlayAction(m_meleeactions.attackf.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Back || m_holdedb)
	m_pWeapon->PlayAction(m_meleeactions.attackb.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Left || m_holdedl)
	m_pWeapon->PlayAction(m_meleeactions.attackl.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	else if(pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Right || m_holdedr)
	m_pWeapon->PlayAction(m_meleeactions.attackr.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	else
	m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);

	m_beginPos = m_pWeapon->GetSlotHelperPos(CItem::eIGS_FirstPerson, m_meleeparams.helper.c_str(), true); 


	m_pWeapon->GetScheduler()->TimerAction(m_pWeapon->GetCurrentAnimationTime(CItem::eIGS_FirstPerson), CSchedulerAction<StopAttackingAction>::Create(this), true);
	
	m_delayTimer = m_meleeparams.delay;

	


	m_durationTimer = m_meleeparams.duration;

	

					m_attacked = true;

				CActor *pActor = m_pWeapon->GetOwnerActor();
				if(!pActor)
					return;

				Vec3 pos(ZERO);
				Vec3 dir(ZERO);
				IMovementController * pMC = pActor->GetMovementController();
				if (!pMC)
					return;

				float strength = 1.0f; 
							float dmgmult = 0.1f;
							strength = dmgmult;
				strength = strength * (0.2f + 0.4f * gEnv->bClient*STRENGTH_MULT);
					IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);
					if(str > pActor->GetActorStrength())
					{
						strength *= str;
					}
					else if(str < pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
					else if(str == pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
			

				SMovementState info;
				pMC->GetMovementState(info);
				pos = info.eyePosition;
				dir = info.eyeDirection;
			
						ApplyCameraShake(false);

				m_ignoredEntity = 0;
				m_meleeScale = 1.0f;
				
				m_pWeapon->RequestMeleeAttack(m_pWeapon->GetMeleeFireMode()==this, pos, dir, m_pWeapon->GetShootSeqN());

				m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());

	m_pWeapon->SetDefaultIdleAnimation(CItem::eIGS_FirstPerson, g_pItemStrings->idle);


}
//------------------------------------------------------------------------
void CMeleeAlt::NetShootEx(const Vec3 &pos, const Vec3 &dir, const Vec3 &vel, const Vec3 &hit, float extra, int ph)
{
	float strength=GetOwnerStrength();

	PerformRayTest(pos, dir, strength, false);
		PerformCylinderTest(pos, dir, strength, true);
			ApplyCameraShake(true);

	m_ignoredEntity = 0;
	m_meleeScale = 1.0f;
}

//------------------------------------------------------------------------
const char *CMeleeAlt::GetType() const
{
	return "MeleeAlt";
}

//------------------------------------------------------------------------
int CMeleeAlt::GetDamage(float distance) const
{
	// NOTE: in multiplayer m_meleeScale == 1.0,
	// however this is not a problem since we cannot pick up objects in that scenario
			if(num_hitedEntites==0)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
}

//------------------------------------------------------------------------
const char *CMeleeAlt::GetDamageType() const
{
	return m_meleeparams.hit_type.c_str();
}


//------------------------------------------------------------------------
bool CMeleeAlt::PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{


	

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;

	 IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200,0,0,128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	
	ray_hit hit;
	int n; 
	if(pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_meleeparams.range/2), ent_living|ent_sleeping_rigid|ent_rigid|ent_independent,
			rwi_colltype_any|rwi_stop_at_pierceable,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
	}
	else if(!pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_meleeparams.range/2), ent_living|ent_sleeping_rigid|ent_rigid|ent_independent,
			rwi_colltype_any|rwi_stop_at_pierceable,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		
	}

	//===================OffHand melee (also in PerformCylincerTest)===================
	if(m_ignoredEntity && (n>0))
	{
		if(IEntity* pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity))
		{
			IPhysicalEntity *pHeldObjectPhysics = pHeldObject->GetPhysics();
			if(pHeldObjectPhysics==hit.pCollider)
				return false;
		}
	}
	//=================================================================================

	if (n>0)
	{
		Impulse(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
		Hit(&hit, dir, strength, remote);
	}

	return n>0;
}

bool CMeleeAlt::PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{


	

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;

	 IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200,0,0,128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	
	ray_hit hit;
	int n; 
	if(pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_meleeparams.range/2), ent_static|ent_terrain|ent_water,
			rwi_colltype_any|rwi_stop_at_pierceable,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
	}
	else if(!pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_meleeparams.range/2),  ent_static|ent_terrain|ent_water,
			rwi_colltype_any|rwi_stop_at_pierceable,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		
	}

	

	if (n>0)
	{
		if(remote)
		{
			Impulse2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
		}
		else
		{
			Hit2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength, remote);
		}
		
	}

	return n>0;
}

void CMeleeAlt::Hit2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote)
{
	
		IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();

		TMFXEffectId effectId = pMaterialEffects->GetEffectId("melee", surfaceIdx);
		if (effectId != InvalidEffectId)
		{
			SMFXRunTimeEffectParams params;
			params.pos = pt;
			params.playflags = MFX_PLAY_ALL | MFX_DISABLE_DELAY;
			params.soundSemantic = eSoundSemantic_Player_Foley;
			pMaterialEffects->ExecuteEffect(effectId, params);
		}


	ApplyCameraShake(true);
	
	m_pWeapon->PlayAction(m_meleeactions.hit.c_str());
}

//------------------------------------------------------------------------
bool CMeleeAlt::PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
	IEntity *pHeldObject = NULL;

	if(m_ignoredEntity)
		pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
	primitives::cylinder cyl;
	cyl.r = 0.25f;
	cyl.axis = dir;
	cyl.hh = m_meleeparams.range/2.0f;
	cyl.center = pos + dir.normalized()*cyl.hh;
	
	float n = 0.0f;
	geom_contact *contacts;
	intersection_params params;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = false;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO), 
		ent_rigid|ent_sleeping_rigid|ent_independent|ent_static|ent_terrain|ent_water, &contacts, 0,
		geom_colltype0|geom_colltype_foliage|geom_colltype_player, &params, 0, 0, &pIgnore, pIgnore?1:0);

	int ret = (int)n;

	float closestdSq = 9999.0f;
	geom_contact *closestc = 0;
	geom_contact *currentc = contacts;

	for (int i=0; i<ret; i++)
	{
		geom_contact *contact = currentc;
		if (contact)
		{
			IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
			if (pCollider)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				if (pEntity)
				{
					if ((pEntity == pOwner)||(pHeldObject && (pEntity == pHeldObject)))
					{
						++currentc;
						continue;
					}
				}

				float distSq = (pos-currentc->pt).len2();
				if (distSq < closestdSq)
				{
					closestdSq = distSq;
					closestc = contact;
				}
			}
		}
		++currentc;
	}

	if (ret)
	{
		WriteLockCond lockColl(*params.plock, 0);
		lockColl.SetActive(1);
	}

  
	if (closestc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);

		Hit(closestc, dir, strength, remote);
		Impulse(closestc->pt, dir, closestc->n, pCollider, closestc->iPrim[1], 0, closestc->id[1], strength);
	}

	return closestc!=0;
}

//------------------------------------------------------------------------
void CMeleeAlt::Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote)
{
	
	IEntity *pTarget = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);



	std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
	std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
	int count = m_hitedEntites.size();
				
	for(int i = 0; i < count, it!=end; i++ , ++it)
	{
		if(pTarget->GetId() == (*it))
			return;
	}

	CActor *pActor = m_pWeapon->GetOwnerActor();

	

	int rnd_value1 = cry_rand() % 100+1;

	if(m_pWeapon->GetWeaponType()==1)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_swords_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_swords_ml;
	}
	else if(m_pWeapon->GetWeaponType()==2)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_axes_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_axes_ml;
	}
	else if(m_pWeapon->GetWeaponType()==3)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	else if(m_pWeapon->GetWeaponType()==4)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thswords_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thswords_ml;
	}
	else if(m_pWeapon->GetWeaponType()==5)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thaxes_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thaxes_ml;
	}
	else if(m_pWeapon->GetWeaponType()==6)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	else if(m_pWeapon->GetWeaponType()==7)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_poles_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_poles_ml;
	}
	else if(m_pWeapon->GetWeaponType()==8)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thpoles_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thpoles_ml;
	}
	else
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	
	if (pActor && pActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer *pPlayer = (CPlayer *)pActor;
		if (pPlayer && pPlayer->GetNanoSuit())
		{
			if (pPlayer->GetEntity() && pPlayer->GetEntity()->GetAI())
			{
				SAIEVENT AIevent;
				AIevent.targetId = pTarget ? pTarget->GetId() : 0;
				
				pPlayer->GetEntity()->GetAI()->Event(AIEVENT_PLAYER_STUNT_PUNCH, &AIevent);
			}
		}
	}

	bool ok = true;
	if(pTarget)
	{
		CActor* pCAITarget = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
		if(pCAITarget && (pCAITarget->m_blockactivenoshield))
		{


					float behindTargetAngleDegrees = 175.0f;
					Vec3 v3PlayerToTarget = pActor->GetEntity()->GetWorldPos() - pTarget->GetWorldPos();

					Vec2 vPlayerToTarget(v3PlayerToTarget);
					float cosineBehindTargetHalfAngleRadians = cos(DEG2RAD(behindTargetAngleDegrees/2.0f));
					Vec2 vBehindTargetDir = -Vec2(pCAITarget->GetAnimatedCharacter()->GetAnimLocation().GetColumn1()).GetNormalizedSafe();
					float dot = vPlayerToTarget.GetNormalizedSafe() * vBehindTargetDir;
					if (dot <= cosineBehindTargetHalfAngleRadians)
					{
						CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
					
						pCAITarget->StartSpecialAction(3,0.3f,true,"combat_oh_block_hit_01",0);

						if(pActor->IsPlayer())
							pClient->StartSpecialAction(3,1.5f,true,"combat_bidl_01",0);
						else
							pActor->StartSpecialAction(3,1.5f,true,"combat_bidl_01",0);
					
						if(pCAITarget->GetStamina()>5)
						{
							pCAITarget->SetStamina(pCAITarget->GetStamina()-(GetDamage()*pCAITarget->stamina_cons_block_ml));
							return;
						}
						else
						{
							pCAITarget->StartSpecialAction(3,1.9f,true,"combat_oh_stagger_01",0);
						}
					}
					
						
		}
		else if(pCAITarget &&  pCAITarget->m_blockactiveshield)
		{
					float behindTargetAngleDegrees = 175.0f;
					Vec3 v3PlayerToTarget = pActor->GetEntity()->GetWorldPos() - pTarget->GetWorldPos();

					Vec2 vPlayerToTarget(v3PlayerToTarget);
					float cosineBehindTargetHalfAngleRadians = cos(DEG2RAD(behindTargetAngleDegrees/2.0f));
					Vec2 vBehindTargetDir = -Vec2(pCAITarget->GetAnimatedCharacter()->GetAnimLocation().GetColumn1()).GetNormalizedSafe();
					float dot = vPlayerToTarget.GetNormalizedSafe() * vBehindTargetDir;
					if (dot <= cosineBehindTargetHalfAngleRadians)
					{
						CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
					
						pCAITarget->StartSpecialAction(3,0.5f,true,"combat_block_shield_hit_01",0);

						if(pActor->IsPlayer())
							pClient->StartSpecialAction(3,1.5f,true,"combat_bidl_01",0);
						else
							pActor->StartSpecialAction(3,1.5f,true,"combat_bidl_01",0);
					
						if(pCAITarget->GetStamina()>5)
						{
							pCAITarget->SetStamina(pCAITarget->GetStamina()-(GetDamage()*pCAITarget->stamina_cons_block_ml));
							return;
						}
						else
						{
							pCAITarget->StartSpecialAction(3,1.9f,true,"combat_oh_stagger_01",0);
							return;
						}
					}
		}

		if(!gEnv->bMultiplayer && pActor && pActor->IsPlayer())
		{
			IActor* pAITarget = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
			if(pAITarget && pTarget->GetAI() && pTarget->GetAI()->IsHostile(pActor->GetEntity()->GetAI(),false))
			{
				
				CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
				CActor  *pActor  = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if(pActor->GetHealth() > 0)
				{
					int lvl = pActor->GetLevel();
					
					pClient->SetLevelXp(pClient->GetLevelXp() + lvl * m_meleeparams.damage * 0.03);
					

				}

				
			}
		}

		if(ok)
		{
			num_hitedEntites+=1;
			CGameRules *pGameRules = g_pGame->GetGameRules();


			if(pActor->IsPlayer() && m_crt_hit_dmg_mult>1)
			{
				g_pGame->GetHUD()->TextMessage("Critical Hit!");
			}

			int damage = m_meleeparams.damage;
			if(pActor && !pActor->IsPlayer())
				damage = m_meleeparams.damageAI;

			HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
				m_pWeapon->GetFireModeIdx(GetName()), 0.0f, pGameRules->GetHitMaterialIdFromSurfaceId(surfaceIdx), partId,
				pGameRules->GetHitTypeId(m_meleeparams.hit_type.c_str()), pt, dir, normal);

			info.remote = remote;
			if (!remote)
				info.seq=m_pWeapon->GenerateShootSeqN();
			info.damage = m_meleeparams.damage;

			if (m_pWeapon->GetForcedHitMaterial() != -1)
				info.material=pGameRules->GetHitMaterialIdFromSurfaceId(m_pWeapon->GetForcedHitMaterial());

			pGameRules->ClientHit(info);
			m_hitedEntites.push_back(pTarget->GetId());
			m_crt_hit_dmg_mult=1.0f;
			
			
		}
	}

	
	if(ok)
	{
		IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();

		TMFXEffectId effectId = pMaterialEffects->GetEffectId("melee", surfaceIdx);
		if (effectId != InvalidEffectId)
		{
			SMFXRunTimeEffectParams params;
			params.pos = pt;
			params.playflags = MFX_PLAY_ALL | MFX_DISABLE_DELAY;
			params.soundSemantic = eSoundSemantic_Player_Foley;
			pMaterialEffects->ExecuteEffect(effectId, params);
		}
	}

	ApplyCameraShake(true);
	
	m_pWeapon->PlayAction(m_meleeactions.hit.c_str());
}

//------------------------------------------------------------------------
void CMeleeAlt::Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale)
{
	if(m_noImpulse)
	{
		m_noImpulse = false;
		return;
	}

	if (pCollider && m_meleeparams.impulse>0.001f)
	{
		IEntity *pEntityPhy = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
		if(pEntityPhy)
		{
			std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
			std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
			int count = m_hitedEntites.size();
				
			for(int i = 0; i < count, it!=end; i++ , ++it)
			{
				if(pEntityPhy->GetId() == (*it))
				return;
			}
			
		}
	
		bool strengthMode = true;
		CPlayer *pPlayer = (CPlayer *)m_pWeapon->GetOwnerActor();
		if(pPlayer)
		{
			if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
			{
			
				if (gEnv->bClient)
					strengthMode = true;
			}
		}

		pe_status_dynamics dyn;

		if (!pCollider->GetStatus(&dyn))
		{
			if(strengthMode)
				impulseScale *= 3.0f;
		}
		else
			impulseScale *= clamp((dyn.mass * 0.01f), 1.0f, 15.0f);
		
		
		IEntity * pEntity = (IEntity*) pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);

		if(gEnv->bMultiplayer && pEntity)
		{
			if(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()) == NULL)
				impulseScale *= 0.33f;
		}

		if(pEntity)
		{
			{
				IEntityPhysicalProxy* pPhysicsProxy = (IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
				CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());

				if (pActor)
				{
					SActorStats* pAS = pActor->GetActorStats();
					if (pAS && pAS->isRagDoll)
					{
						
						impulseScale = 1.0f; 
					}
				}

				
				pPhysicsProxy->AddImpulse(partId, pt, dir*m_meleeparams.impulse*impulseScale*m_meleeScale, true, 1.f);
			}
		}
		else
		{
			pe_action_impulse ai;
			ai.partid = partId;
			ai.ipart = ipart;
			ai.point = pt;
			ai.iApplyTime = 0;
			ai.impulse = dir*(m_meleeparams.impulse*impulseScale*m_meleeScale);
			pCollider->Action(&ai);
		}

		ISurfaceTypeManager *pSurfaceTypeManager = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager();
		int invId = pSurfaceTypeManager->GetSurfaceTypeByName("mat_invulnerable")->GetId();

		
		pe_action_register_coll_event collision;

		collision.collMass = 0.005f;
		collision.partid[1] = partId;

		
		collision.partid[0] = 1;
		collision.idmat[1] = surfaceIdx;
		collision.idmat[0] = invId;
		collision.n = normal;
		collision.pt = pt;
	
		Vec3	v = dir;
		float speed = cry_sqrtf(4000.0f/(80.0f*0.5f)); 

	
		IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);
		if (pBrush)
		{
			CActor *pActor = m_pWeapon->GetOwnerActor();
			if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
			{
				CPlayer *pPlayer = (CPlayer *)pActor;
				if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
				{
					if (gEnv->bClient)
						speed =0.003f;
				}
			}
		}

		collision.vSelf = (v.normalized()*speed*m_meleeScale);
		collision.v = Vec3(0,0,0);
		collision.pCollider = pCollider;

		IEntity *pOwner = m_pWeapon->GetOwner();
		if (pOwner && pOwner->GetCharacter(0) && pOwner->GetCharacter(0)->GetISkeletonPose()->GetCharacterPhysics())
		{
			if (ISkeletonPose *pSkeletonPose=pOwner->GetCharacter(0)->GetISkeletonPose())
			{
				if (pSkeletonPose && pSkeletonPose->GetCharacterPhysics())
					pSkeletonPose->GetCharacterPhysics()->Action(&collision);
			}

		}
	}
}

//------------------------------------------------------------------------
void CMeleeAlt::Impulse2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale)
{
	if(m_noImpulse)
	{
		m_noImpulse = false;
		return;
	}
	
	if (pCollider && m_meleeparams.impulse>0.001f)
	{
		
		bool strengthMode = true;

		pe_status_dynamics dyn;

		
				impulseScale *= 3.0f;
	
		
			pe_action_impulse ai;
			ai.partid = partId;
			ai.ipart = ipart;
			ai.point = pt;
			ai.iApplyTime = 0;
			ai.impulse = dir*(m_meleeparams.impulse*impulseScale*m_meleeScale);
			pCollider->Action(&ai);
		

		ISurfaceTypeManager *pSurfaceTypeManager = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager();
		int invId = pSurfaceTypeManager->GetSurfaceTypeByName("mat_invulnerable")->GetId();

		// create a physical collision to break trees
		pe_action_register_coll_event collision;
		//pe_action_awake caaa;

		//caaa.bAwake = true;
		//pCollider->Action(&caaa);

		collision.collMass = 0.005f; // this is actually ignored
		collision.partid[1] = partId;
		//collision.

		// collisions involving partId<-1 are to be ignored by game's damage calculations
		// usually created articially to make stuff break.
		collision.partid[0] = -2;
		collision.idmat[1] = surfaceIdx;
		collision.idmat[0] = invId;
		collision.n = normal;
		collision.pt = pt;
	
		// scar bullet
		// m = 0.0125
		// v = 800
		// energy: 4000
		// in this case the mass of the active collider is a player part
		// so we must solve for v given the same energy as a scar bullet
		Vec3	v = dir;
		float speed = cry_sqrtf(4000.0f/(80.0f*0.5f)); // 80.0f is the mass of the player

		// [marco] Check if an object. Should take lots of time to break stuff if not in nanosuit strength mode;
		// and still creates a very low impulse for stuff that might depend on receiving an impulse.
		IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);
		if (pBrush)
		{
			
		}
		

		collision.vSelf = (v.normalized()*speed*m_meleeScale);
		collision.v = v;
		collision.pCollider = pCollider;
		

		IEntity *pOwner = m_pWeapon->GetOwner();
		
		if (pOwner && pOwner->GetCharacter(0) && pOwner->GetCharacter(0)->GetISkeletonPose()->GetCharacterPhysics())
		{
			if (ISkeletonPose *pSkeletonPose=pOwner->GetCharacter(0)->GetISkeletonPose())
			{
				if (pSkeletonPose && pSkeletonPose->GetCharacterPhysics())
					pSkeletonPose->GetCharacterPhysics()->Action(&collision);
			}

		}
	}
}
//------------------------------------------------------------------------
void CMeleeAlt::Hit(geom_contact *contact, const Vec3 &dir, float damageScale, bool remote)
{
	CActor *pOwner = m_pWeapon->GetOwnerActor();
	if (!pOwner)
		return;

	Vec3 view(0.0f, 1.0f, 0.0f);

	if (IMovementController *pMC = pOwner->GetMovementController())
	{
		SMovementState state;
		pMC->GetMovementState(state);
		view = state.weaponPosition;
	}

	
	bool backface = dir.Dot(contact->n)>0;
	bool away = dir.Dot(view.normalized())>0; 

	Vec3 normal=contact->n;
	Vec3 ndir=dir;

	if (backface)
	{
		if (away)
			normal = -normal;
		else
			ndir = -dir;
	}
	else
	{
		if (!away)
		{
			ndir = -dir;
			normal = -normal;
		}
	}

	IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->id[0]);

	Hit(contact->pt, ndir, normal, pCollider, contact->id[1], 0, contact->id[1], damageScale, remote);
}

//------------------------------------------------------------------------
void CMeleeAlt::Hit(ray_hit *hit, const Vec3 &dir, float damageScale, bool remote)
{
	Hit(hit->pt, dir, hit->n, hit->pCollider, hit->partid, hit->ipart, hit->surface_idx, damageScale, remote);
}

//-----------------------------------------------------------------------
void CMeleeAlt::ApplyCameraShake(bool hit)
{
	
	if(m_pWeapon->GetOwnerActor() && m_pWeapon->GetOwnerActor()->IsClient())
	{
		if(CScreenEffects* pScreenEffects = m_pWeapon->GetOwnerActor()->GetScreenEffects())
		{
			float rotateTime;
			if(!hit)
			{
				rotateTime = g_pGameCVars->hr_rotateTime*1.25f;
				pScreenEffects->CamShake(Vec3(rotateTime*0.5f,rotateTime*0.5f,rotateTime*0.25f), Vec3(0, 0.3f * g_pGameCVars->hr_rotateFactor,0), rotateTime, rotateTime,1);
			}
			else
			{
				rotateTime = g_pGameCVars->hr_rotateTime*2.0f;
				pScreenEffects->CamShake(Vec3(rotateTime,rotateTime,rotateTime*0.5f), Vec3(0, 0.5f * g_pGameCVars->hr_rotateFactor,0), rotateTime, rotateTime,2);
			}
		}
	}
}

float CMeleeAlt::GetOwnerStrength() const
{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if(!pActor)
		return 1.0f;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return 1.0f;

	float strength = 1.0f; 
							float dmgmult = 0.1f;
							strength = dmgmult;
				strength = strength * (0.2f + 0.4f * gEnv->bClient*STRENGTH_MULT);
				
				IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);


				
					if(str > pActor->GetActorStrength())
					{
						strength *= str;
					}
					else if(str < pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
					else if(str == pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
				

	return (0.2f+(strength/10));
}

void CMeleeAlt::GetMemoryStatistics( ICrySizer * s )
{
	s->Add(*this);
	s->Add(m_name);
	m_meleeparams.GetMemoryStatistics(s);
	m_meleeactions.GetMemoryStatistics(s);
}   


*/
