// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

//
////////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"

#include "ICryMannequin.h"

#include <Mannequin/Serialization.h>

#include "Player.h"
#include "Actor.h"
#include "Melee.h"
#include "ActorActionsNew.h"

#include <CryAISystem/ITargetTrackManager.h>
#include <CryAISystem/IAIActor.h>

#include "ActorImpulseHandler.h"

#include "MeleeCollisionHelper.h"

struct SProceduralClipEventParams
	: public IProceduralParams
{
	virtual void Serialize(Serialization::IArchive& ar) override
	{
		ar(eventName, "EventName", "Event Name");
	}

	virtual void GetExtraDebugInfo(StringWrapper& extraInfoOut) const override
	{
		extraInfoOut = eventName.c_str();
	}

	SProcDataCRC eventName;
};

struct SFovSetupParams : public IProceduralParams
{
	SFovSetupParams()
		: smoothTime(1.0f)
		, fovValue(1.0f)
		, nearFovValue(1.0f)
		, smoothTimeEnd(1.0f)
	{
	}

	virtual void Serialize(Serialization::IArchive& ar) override
	{
		ar(smoothTime, "SmoothTime", "Time");
		ar(fovValue, "fovValue", "Fov Value");
		ar(nearFovValue, "NearFovValue", "Fov nearplane Value");
		ar(smoothTimeEnd, "SmoothTimeEnd", "Smooth Time to returning normal fov");
	}

	
	float    smoothTime;
	float    fovValue;
	float    nearFovValue;
	float    smoothTimeEnd;
};

struct SCameraShakeParams : public IProceduralParams
{
	SCameraShakeParams()
		: angle(15.0f)
		, shift(0.3f)
		, frequency(0.4f)
		, duration(0.5f)
	{
	}

	virtual void Serialize(Serialization::IArchive& ar) override
	{
		ar(angle, "angle", "angle");
		ar(shift, "shift", "shift");
		ar(frequency, "frequency", "frequency");
		ar(duration, "duration", "duration");
	}


	float    angle;
	float    shift;
	float    frequency;
	float    duration;
};

struct SPrcMeleeAttackParams : public IProceduralParams
{
	SPrcMeleeAttackParams()
	{
		StartPointName = "melee_attack_helper_01";
		EndPointName = "melee_attack_helper_02";
		StartPointName2 = "melee_attack_helper_03";
		EndPointName2 = "melee_attack_helper_04";
		IntersectionType = 1;
		Damage = 10.0f;
		CheckDamageMultiplers = 1;
		DamageType = "Melee";
		HitFXType = "melee";
		UseWeaponModel = 1;
		IntersectionRange = 1.0f;
		EnableWallHitStop = 0;
		WallHitStopRange = 0.0f;
		IgnoreBlock = 0;
		ImpulseValue = 1.0f;
		HitsToStatic = 1;
		AnimationPausing = 0;
		PrimitiveIntersectionScale = 1.0f;
	}

	virtual void Serialize(Serialization::IArchive& ar) override
	{
		ar(StartPointName, "StartPointName", "StartPointName");
		ar(EndPointName, "EndPointName", "EndPointName");
		ar(StartPointName2, "StartPointName2", "StartPointName2");
		ar(EndPointName2, "EndPointName2", "EndPointName2");
		ar(IntersectionType, "IntersectionType", "IntersectionType");
		ar(Damage, "Damage", "Damage");
		ar(CheckDamageMultiplers, "CheckDamageMultiplers", "CheckDamageMultiplers");
		ar(DamageType, "DamageType", "DamageType");
		ar(UseWeaponModel, "UseWeaponModel", "UseWeaponModel");
		ar(IntersectionRange, "IntersectionRange", "IntersectionRange");
		ar(EnableWallHitStop, "EnableWallHitStop", "EnableWallHitStop");
		ar(WallHitStopRange, "WallHitStopRange", "WallHitStopRange");
		ar(IgnoreBlock, "IgnoreBlock", "IgnoreBlock");
		ar(ImpulseValue, "ImpulseValue", "ImpulseValue");
		ar(HitsToStatic, "HitsToStatic", "HitsToStatic");
		ar(AnimationPausing, "AnimationPausing", "AnimationPausing");
		ar(PrimitiveIntersectionScale, "PrimitiveIntersectionScale", "PrimitiveIntersectionScale");
		ar(HitFXType, "HitFXType", "HitFXType");
		/*
		ar(StartPointName, "StartPointName", "start point name for intersection");
		ar(EndPointName, "EndPointName", "end point name for intersection");
		ar(StartPointName2, "StartPointName2", "next start point name for intersection");
		ar(EndPointName2, "EndPointName2", "next end point name for intersection");
		ar(IntersectionType, "IntersectionType", "Type of the melee damage zone(0 == simple intersection from character StartPointName to current target; 1 == standart intersection from start to and and ets..)");
		ar(Damage, "Damage", "Damage of melee attack");
		ar(CheckDamageMultiplers, "CheckDamageMultiplers", "Apply melee damage various multiplers like character strength");
		ar(DamageType, "DamageType", "Damage type of this melee attack");
		ar(UseWeaponModel, "UseWeaponModel", "Use current weapon model file for intersection tests");
		ar(IntersectionRange, "IntersectionRange", "Range of selected intersection type");
		ar(EnableWallHitStop, "EnableWallHitStop", "Close wall hit stop");
		ar(WallHitStopRange, "WallHitStopRange", "Hit stop on close wall range");
		ar(IgnoreBlock, "IgnoreBlock", "Hit to block ignoring");
		ar(ImpulseValue, "ImpulseValue", "Melee Impulse To Hited Objects");
		ar(HitsToStatic, "HitsToStatic", "Hits to static objects, terrain and water");
		ar(AnimationPausing, "AnimationPausing", "Enable animation invisible pauses, it can help to do normal physics intersection with very short animations or at low game framerate");
		ar(PrimitiveIntersectionScale, "PrimitiveIntersectionScale", "Primitive intersection scale, used with IntersectionType >= 2");
		*/
	}

	string    StartPointName;
	string    EndPointName;
	string    StartPointName2;
	string    EndPointName2;
	int    IntersectionType;
	float    Damage;
	int    CheckDamageMultiplers;
	string    DamageType;
	string    HitFXType;
	int    UseWeaponModel;
	float    IntersectionRange;
	int    EnableWallHitStop;
	float    WallHitStopRange;
	int    IgnoreBlock;
	float    ImpulseValue;
	int    HitsToStatic;
	int    AnimationPausing;
	float    PrimitiveIntersectionScale;
};

class CProceduralClipEventNvc : public TProceduralClip<SProceduralClipEventParams>
{
public:
	CProceduralClipEventNvc()
	{
	}

	virtual void OnEnter(float blendTime, float duration, const SProceduralClipEventParams &params)
	{
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote() || !pActor->IsPlayer())
				return;
		}
		CryLogAlways("CProceduralClipEventNvc OnEnter, event name = %s", params.eventName.c_str());
		if (!strcmp(params.eventName.c_str(), "playerCameraAnimControlled"))
		{
			g_pGameCVars->g_test_cam_anim_controlled = 11.1f;
			CryLogAlways("CProceduralClipEventNvc OnEnter, playerCameraAnimControlled called");
		}
		else if (!strcmp(params.eventName.c_str(), "playerCameraStandartControlled"))
		{
			if (pActor)
			{
				CPlayer *pPlayer = (CPlayer *)pActor;
				g_pGameCVars->g_test_cam_anim_controlled = 0.0f;
				if (pPlayer)
					pPlayer->SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
			}
		}
		//SendActionEvent(params.eventName.crc);
	}

	virtual void OnExit(float blendTime) {}

	virtual void Update(float timePassed) {}

};

class CProceduralClipConsoleCommandCall : public TProceduralClip<SProceduralClipEventParams>
{
public:
	CProceduralClipConsoleCommandCall()
	{
	}

	virtual void OnEnter(float blendTime, float duration, const SProceduralClipEventParams &params)
	{
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote() || !pActor->IsPlayer())
				return;
		}

		if(gEnv->pConsole)
			gEnv->pConsole->ExecuteString(params.eventName.c_str(), true);
	}

	virtual void OnExit(float blendTime) {}

	virtual void Update(float timePassed) {}

};

class CProceduralClipCameraAnimControlled : public TProceduralClip<SProceduralClipEventParams>
{
public:
	CProceduralClipCameraAnimControlled()
	{
	}

	virtual void OnEnter(float blendTime, float duration, const SProceduralClipEventParams &params)
	{
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote() || !pActor->IsPlayer())
				return;

			g_pGameCVars->g_test_cam_anim_controlled = 11.1f;
		}
	}

	virtual void OnExit(float blendTime) 
	{
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote() || !pActor->IsPlayer())
				return;

			CPlayer *pPlayer = (CPlayer *)pActor;
			g_pGameCVars->g_test_cam_anim_controlled = 0.0f;
			if(pPlayer)
				pPlayer->SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
		}
	}

	virtual void Update(float timePassed) {}

};

class CProceduralClipCameraShake : public TProceduralClip<SCameraShakeParams>
{
public:
	CProceduralClipCameraShake()
	{
	}

	virtual void OnEnter(float blendTime, float duration, const SCameraShakeParams &params)
	{
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote() || !pActor->IsPlayer())
				return;
		}

		CPlayer *pPlayer = (CPlayer*)pActor;
		if (pPlayer)
		{
			pPlayer->CameraShake(params.angle, params.shift, params.duration, params.frequency, Vec3(ZERO), 36);
		}
	}

	virtual void OnExit(float blendTime) {}

	virtual void Update(float timePassed) {}

};

class CProceduralClipSetupFov : public TProceduralClip<SFovSetupParams>
{
public:
	CProceduralClipSetupFov()
	{
		default_fov = 0.0f;
		current_fov = 0.0f;
		requested_fov = 0.0f;
		requested_fov_near = 0.0f;
		cur_time = 0.0f;
		cm_time = 0.0f;
		end_time = 0.0f;
		cr_time = 0.0f;
		current_near_fov_mlt = 1.0f;
		pPlayer = NULL;
	}

	virtual void OnEnter(float blendTime, float duration, const SFovSetupParams &params)
	{
		pPlayer = NULL;
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (pActor)
		{
			if (pActor->IsRemote())
				return;
		}
		default_fov = 0.0f;
		current_fov = 0.0f;
		requested_fov = 0.0f;
		requested_fov_near = 0.0f;
		cur_time = 0.0f;
		cm_time = 0.0f;
		end_time = 0.0f;
		cr_time = 0.0f;
		current_near_fov_mlt = 1.0f;
		pPlayer = NULL;
		IActor *pClientActor = gEnv->pGame->GetIGameFramework()->GetClientActor();
		if (pClientActor)
		{
			pPlayer = (CPlayer *)pClientActor;
			if (pPlayer)
			{
				default_fov = 1.0;
				current_fov = pPlayer->GetActorParams().viewFoVScale;
				requested_fov = params.fovValue;
				cm_time = params.smoothTime;
				requested_fov_near = params.nearFovValue;
				end_time = params.smoothTimeEnd;
				cr_time = duration / 2.0f;
				pPlayer->return_to_default_cam_fov_scl = 0;
				pPlayer->return_to_default_cam_fov_scl2 = 0;
				ICVar* r_drawnearfovDef = gEnv->pConsole->GetCVar("cl_near_fov_default_value");
				ICVar* r_drawnearfov = gEnv->pConsole->GetCVar("r_DrawNearFoV");
				float cn_fov_n = 1.0f;
				if (r_drawnearfovDef && r_drawnearfov)
				{
					cn_fov_n = r_drawnearfov->GetFVal() / r_drawnearfovDef->GetFVal();
				}
				current_near_fov_mlt = cn_fov_n;
			}
		}
	}

	virtual void OnExit(float blendTime) 
	{
		if (pPlayer)
		{
			//pPlayer->GetActorParams().viewFoVScale = default_fov;
			pPlayer->return_to_default_cam_fov_scl = 1;
			pPlayer->return_to_default_cam_fov_scl2 = 1;
			pPlayer->cam_fov_scl_retr_spd = end_time;
		}
	}

	virtual void Update(float timePassed) 
	{
		if (pPlayer)
		{
			//float lrp_valx1 = cur_time / cm_time;
			//if (cur_time < cr_time)
			//{
			if (current_fov != requested_fov)
			{
				if (default_fov > requested_fov)
				{
					current_fov -= timePassed / cm_time;//LERP(default_fov, requested_fov, lrp_valx1);
					if (current_fov < requested_fov)
						current_fov = requested_fov;

				}
				else
				{
					current_fov += timePassed / cm_time;
					if (current_fov > requested_fov)
						current_fov = requested_fov;
				}
				ICVar* r_drawnearfovDef = gEnv->pConsole->GetCVar("cl_near_fov_default_value");
				ICVar* r_drawnearfov = gEnv->pConsole->GetCVar("r_DrawNearFoV");
				if (r_drawnearfovDef && r_drawnearfov)
				{
					if (1.0 > requested_fov_near)
					{
						current_near_fov_mlt -= timePassed / cm_time;//LERP(default_fov, requested_fov, lrp_valx1);
						if (current_near_fov_mlt < requested_fov_near)
							current_near_fov_mlt = requested_fov_near;

					}
					else
					{
						current_near_fov_mlt += timePassed / cm_time;
						if (current_near_fov_mlt > requested_fov_near)
							current_near_fov_mlt = requested_fov_near;
					}
					r_drawnearfov->Set(r_drawnearfovDef->GetFVal()*current_near_fov_mlt);
				}
				pPlayer->GetActorParams().viewFoVScale = current_fov;
				cur_time += timePassed;
			}
			//}
			/*else
			{
				if (default_fov > requested_fov)
				{
					current_fov += timePassed / cm_time;//LERP(current_fov, default_fov, lrp_valx1);
					if (current_fov > default_fov)
						current_fov = default_fov;
				}
				else
				{
					current_fov -= timePassed / cm_time;
					if (current_fov < default_fov)
						current_fov = default_fov;
				}

				if (current_fov < default_fov)
					current_fov = default_fov;

				pPlayer->GetActorParams().viewFoVScale = current_fov;
				cur_time += timePassed;
			}*/
			//CryLogAlways("player fs = current_fov = %f", current_fov);
			//CryLogAlways("CProceduralClipSetupFov timePassed = %f", timePassed);
		}
	}
	float requested_fov;
	float requested_fov_near;
	float current_fov;
	float current_near_fov_mlt;
	float default_fov;
	float cm_time;
	float end_time;
	float cr_time;
	float cur_time;
	CPlayer *pPlayer;
};

class CProceduralMeleeAttack : public TProceduralClip<SPrcMeleeAttackParams>
{
public:
	CProceduralMeleeAttack()
	{
		for (int i = 0; i < 2; i++)
		{
			pos_initial[i] = Vec3(ZERO);
			pos_start_old[i] = Vec3(ZERO);
			pos_end_old[i] = Vec3(ZERO);
			pos_start_nv[i] = Vec3(ZERO);
			pos_end_nv[i] = Vec3(ZERO);
			normalized_hit_direction[i] = Vec3(ZERO);
			pos_start[i] = Vec3(ZERO);
			pos_end[i] = Vec3(ZERO);
			num_mtl_eff_spwn[i] = 0;
		}
		m_updtimer = 0.0f;
		m_updtimer2 = 0.0f;
		m_updtimer3 = 0.0f;
		m_clp_lght = 0.0f;
		pause_state = 0;
		num_ents_to_ignore = 0;
		pAttacker = NULL;
	}

	virtual void OnEnter(float blendTime, float duration, const SPrcMeleeAttackParams &params)
	{
		pAttacker = NULL;
		MeleeParams_SS = params;
		if(!m_entity)
			return;

		m_updtimer = 0.0f;
		CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_entity->GetId());
		if (!pActor)
			return;

		if (pActor)
		{
			if (pActor->IsRemote())
				return;
		}

		//CryLogAlways("CProceduralMeleeAttack OnEnter called");

		m_clp_lght = duration;
		pAttacker = pActor;
		if (MeleeParams_SS.IntersectionType == 0)
		{
			CWeapon * pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
			if (pWpn)
			{
				pWpn->RequireUpdate(eIUS_General);
				pWpn->SpecialMeleeRequestDelayedSimpleAttack(blendTime, GetMeleeDamage(), 0.2f, MeleeParams_SS.UseWeaponModel, MeleeParams_SS.IntersectionRange);
				if (MeleeParams_SS.AnimationPausing > 0)
					pWpn->SetItemFlag(CItem::eIF_BlockActions, true);
			}
		}
		else
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction && pAttacker)
				num_ents_to_ignore = nwAction->GetIgnoredPhyObjects(pAttacker->GetEntityId(), idnoredEnts);

			if (!MeleeParams_SS.StartPointName.empty())
			{
				StartPhysicalIntersectionSequenceOne();
			}

			if (!MeleeParams_SS.StartPointName2.empty())
			{
				StartPhysicalIntersectionSequenceTwo();
			}

			if (MeleeParams_SS.AnimationPausing == 1)
			{
				m_scope->IncrementTime(-0.2f);
				pause_state = 1;
			}
		}
	}

	virtual void OnExit(float blendTime)
	{
		if (pAttacker)
		{
			for (int i = 0; i < 2; i++)
			{
				pos_initial[i] = Vec3(ZERO);
				pos_start_old[i] = Vec3(ZERO);
				pos_end_old[i] = Vec3(ZERO);
				pos_start_nv[i] = Vec3(ZERO);
				pos_end_nv[i] = Vec3(ZERO);
				normalized_hit_direction[i] = Vec3(ZERO);
				pos_start[i] = Vec3(ZERO);
				pos_end[i] = Vec3(ZERO);
				num_mtl_eff_spwn[i] = 0;
			}
			m_updtimer = 0.0f;
			m_clp_lght = 0.0f;
			pAttacker = NULL;
		}
	}

	virtual void Update(float timePassed)
	{
		if (pAttacker && pAttacker->GetHealth() > 0)
		{
			if (MeleeParams_SS.IntersectionType > 0)
			{
				if (g_pGameCVars->g_enable_fl_limit_for_melee_system > 0)
				{
					if (timePassed > g_pGameCVars->g_ft_limit_val_for_melee)
						timePassed = g_pGameCVars->g_ft_limit_val_for_melee;
				}
				m_updtimer += timePassed;
				m_updtimer2 += timePassed;
				if (!MeleeParams_SS.StartPointName.empty())
				{
					if (!pos_start_nv[0].IsZero())
					{
						pos_start_old[0] = pos_start_nv[0];
						pos_end_old[0] = pos_end_nv[0];
						if (pos_initial[0].IsZero())
							pos_initial[0] = pos_start_old[0];
					}
					StartPhysicalIntersectionSequenceOne();
				}

				if (!MeleeParams_SS.StartPointName2.empty())
				{
					if (!pos_start_nv[1].IsZero())
					{
						pos_start_old[1] = pos_start_nv[1];
						pos_end_old[1] = pos_end_nv[1];
						if (pos_initial[1].IsZero())
							pos_initial[1] = pos_start_old[1];
					}
					StartPhysicalIntersectionSequenceTwo();
				}

				if (MeleeParams_SS.AnimationPausing == 1 && pause_state > 0)
				{
					m_scope->IncrementTime(-0.2f);
					pause_state = 0;
				}
				else if (MeleeParams_SS.AnimationPausing == 1 && pause_state == 0)
				{
					m_scope->IncrementTime(0.2f);
					pause_state = 1;
				}

				if (g_pGameCVars->g_melee_system_clear_hit_targets_after_time > 0)
				{
					m_updtimer3 += timePassed;
					if (m_updtimer3 >= g_pGameCVars->g_melee_system_time_to_clear_hit_targets)
					{
						m_hitedEntites.clear();
						m_updtimer3 = 0.0f;
					}
				}

				//CryLogAlways("CProceduralMeleeAttack Update called");
			}
		}
	}

	virtual void StartPhysicalIntersectionSequenceOne()
	{
		CActor* pOwner = pAttacker;
		if (pOwner)
		{
			string helperstart = MeleeParams_SS.StartPointName.c_str();
			string helperend = MeleeParams_SS.EndPointName.c_str();
			Vec3 position(0, 0, 0);
			Vec3 positionl(0, 0, 0);
			if (MeleeParams_SS.UseWeaponModel > 0)
			{
				CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
				CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
				CItem *m_pWeapon = pAttacker->GetItem(pAttacker->GetCurrentItemId());
				if (!m_pWeapon || (pAttacker->GetCurrentItemId() == pAttacker->GetNoWeaponId()))
					m_pWeapon = pAttacker->GetItem(pAttacker->GetAnyItemInLeftHand());
				
				if (!m_pWeapon)
					return;

				IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
				if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					positionl = pStatObj->GetHelperPos(helperstart.c_str());
					positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
					position = pStatObj->GetHelperPos(helperend.c_str());
					position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
					m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
					m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
					if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
					{
						if (pCurrentWeapon)
						{
							position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend, true);
							positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart, true);
						}
						PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 0);
						if (MeleeParams_SS.HitsToStatic > 0)
							PerformPhyTest2(positionl, (position - positionl), 0);

						pos_start[0] = positionl;
						pos_end[0] = position - positionl;
						pos_end_nv[0] = position;
					}
					else
					{
						PerformPhyTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), 
							(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), Vec3(ZERO), 0);
						if (MeleeParams_SS.HitsToStatic > 0)
							PerformPhyTest2(positionl, (position - positionl), 0);

						pos_start[0] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
						pos_end[0] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
						pos_end_nv[0] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
					}

					if (!pos_start_old[0].IsZero() && !pos_end_nv[0].IsZero())
					{
						Vec3 nv_pos_center = (pos_end_nv[0] + pos_start_nv[0]) * 0.5000f;
						Vec3 nv_dir_xc_fa = (pos_start_old[0] - nv_pos_center).normalized() * 0.1f;
						nv_pos_center = pos_start_old[0] + (nv_dir_xc_fa * 1.5f);
						normalized_hit_direction[0] = nv_pos_center - pos_start_old[0];
						normalized_hit_direction[0].normalize();
					}
					pos_start_nv[0] = pos_start[0];
					AdditivePhyTestStart(pos_start_old[0], pos_start_nv[0], pos_end_old[0], pos_end_nv[0], 0);
				}
				else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					int slot = 0;
					if (pAttackingPlayer->IsThirdPerson())
						slot = 1;

					position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
					positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
					PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 0);
					if (MeleeParams_SS.HitsToStatic > 0)
						PerformPhyTest2(positionl, (position - positionl), 0);

					pos_start[0] = positionl;
					pos_end[0] = position - positionl;
					pos_end_nv[0] = position;
					if (!pos_start_old[0].IsZero() && !pos_end_nv[0].IsZero())
					{
						Vec3 nv_pos_center = (pos_end_nv[0] + pos_start_nv[0]) * 0.5000f;
						Vec3 nv_dir_xc_fa = (pos_start_old[0] - nv_pos_center).normalized() * 0.1f;
						nv_pos_center = pos_start_old[0] + (nv_dir_xc_fa * 1.5f);
						normalized_hit_direction[0] = pos_start_old[0] - nv_pos_center;
						normalized_hit_direction[0].normalize();
					}
					pos_start_nv[0] = pos_start[0];
					AdditivePhyTestStart(pos_start_old[0], pos_start_nv[0], pos_end_old[0], pos_end_nv[0], 0);
				}
				else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
				{

				}
			}
			else
			{
				positionl = pOwner->GetBonePosition(helperstart.c_str());
				position = pOwner->GetBonePosition(helperend.c_str());
				PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 0);
				if (MeleeParams_SS.HitsToStatic > 0)
					PerformPhyTest2(positionl, (position - positionl), 0);

				pos_start[0] = positionl;
				pos_end[0] = position - positionl;
				pos_end_nv[0] = position;
				if (!pos_start_old[0].IsZero() && !pos_end_nv[0].IsZero())
				{
					Vec3 nv_pos_center = (pos_end_nv[0] + pos_start_nv[0]) * 0.5000f;
					Vec3 nv_dir_xc_fa = (pos_start_old[0] - nv_pos_center).normalized() * 0.1f;
					nv_pos_center = pos_start_old[0] + (nv_dir_xc_fa * 1.5f);
					normalized_hit_direction[0] = nv_pos_center - pos_start_old[0];
					normalized_hit_direction[0].normalize();
				}
				pos_start_nv[0] = pos_start[0];
				AdditivePhyTestStart(pos_start_old[0], pos_start_nv[0], pos_end_old[0], pos_end_nv[0], 0);
			}
		}
	}

	virtual void StartPhysicalIntersectionSequenceTwo()
	{
		CActor* pOwner = pAttacker;
		if (pOwner)
		{
			string helperstart = MeleeParams_SS.StartPointName2.c_str();
			string helperend = MeleeParams_SS.EndPointName2.c_str();
			Vec3 position(0, 0, 0);
			Vec3 positionl(0, 0, 0);
			if (MeleeParams_SS.UseWeaponModel > 0)
			{
				CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
				CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
				CItem *m_pWeapon = pAttacker->GetItem(pAttacker->GetCurrentItemId());
				if (!m_pWeapon || (pAttacker->GetCurrentItemId() == pAttacker->GetNoWeaponId()))
					m_pWeapon = pAttacker->GetItem(pAttacker->GetAnyItemInLeftHand());

				if (!m_pWeapon)
					return;

				IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
				if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					positionl = pStatObj->GetHelperPos(helperstart.c_str());
					positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
					position = pStatObj->GetHelperPos(helperend.c_str());
					position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
					m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
					m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
					if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
					{
						if (pCurrentWeapon)
						{
							position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend, true);
							positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart, true);
						}
						PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 1);
						if (MeleeParams_SS.HitsToStatic > 0)
							PerformPhyTest2(positionl, (position - positionl), 1);

						pos_start[1] = positionl;
						pos_end[1] = position - positionl;
						pos_end_nv[1] = position;
					}
					else
					{
						PerformPhyTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl),
							(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), Vec3(ZERO), 1);
						if (MeleeParams_SS.HitsToStatic > 0)
							PerformPhyTest2(positionl, (position - positionl), 1);

						pos_start[1] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
						pos_end[1] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
						pos_end_nv[1] = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
					}

					if (!pos_start_old[1].IsZero() && !pos_end_nv[1].IsZero())
					{
						Vec3 nv_pos_center = (pos_end_nv[1] + pos_start_nv[1]) * 0.5000f;
						Vec3 nv_dir_xc_fa = (pos_start_old[1] - nv_pos_center).normalized() * 0.1f;
						nv_pos_center = pos_start_old[1] + (nv_dir_xc_fa * 1.5f);
						normalized_hit_direction[1] = nv_pos_center - pos_start_old[1];
						normalized_hit_direction[1].normalize();
					}
					pos_start_nv[1] = pos_start[1];
					AdditivePhyTestStart(pos_start_old[1], pos_start_nv[1], pos_end_old[1], pos_end_nv[1], 1);
				}
				else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					int slot = 0;
					if (pAttackingPlayer->IsThirdPerson())
						slot = 1;

					position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
					positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
					PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 1);
					if (MeleeParams_SS.HitsToStatic > 0)
						PerformPhyTest2(positionl, (position - positionl), 1);

					pos_start[1] = positionl;
					pos_end[1] = position - positionl;
					pos_end_nv[1] = position;
					if (!pos_start_old[1].IsZero() && !pos_end_nv[1].IsZero())
					{
						Vec3 nv_pos_center = (pos_end_nv[1] + pos_start_nv[1]) * 0.5000f;
						Vec3 nv_dir_xc_fa = (pos_start_old[1] - nv_pos_center).normalized() * 0.1f;
						nv_pos_center = pos_start_old[1] + (nv_dir_xc_fa * 1.5f);
						normalized_hit_direction[1] = nv_pos_center - pos_start_old[1];
						normalized_hit_direction[1].normalize();
					}
					pos_start_nv[1] = pos_start[1];
					AdditivePhyTestStart(pos_start_old[1], pos_start_nv[1], pos_end_old[1], pos_end_nv[1], 1);
				}
				else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
				{

				}
			}
			else
			{
				positionl = pOwner->GetBonePosition(helperstart.c_str());
				position = pOwner->GetBonePosition(helperend.c_str());
				PerformPhyTest(positionl, (position - positionl), Vec3(ZERO), 1);
				if (MeleeParams_SS.HitsToStatic > 0)
					PerformPhyTest2(positionl, (position - positionl), 1);

				pos_start[1] = positionl;
				pos_end[1] = position - positionl;
				pos_end_nv[1] = position;
				if (!pos_start_old[1].IsZero() && !pos_end_nv[1].IsZero())
				{
					Vec3 nv_pos_center = (pos_end_nv[1] + pos_start_nv[1]) * 0.5000f;
					Vec3 nv_dir_xc_fa = (pos_start_old[1] - nv_pos_center).normalized() * 0.1f;
					nv_pos_center = pos_start_old[1] + (nv_dir_xc_fa * 1.5f);
					normalized_hit_direction[1] = nv_pos_center - pos_start_old[1];
					normalized_hit_direction[1].normalize();
				}
				pos_start_nv[1] = pos_start[1];
				AdditivePhyTestStart(pos_start_old[1], pos_start_nv[1], pos_end_old[1], pos_end_nv[1], 1);
			}
		}
	}

	virtual void AdditivePhyTestStart(Vec3 pos_start_old_cx, Vec3 pos_start_nv_cx, Vec3 pos_end_old_cx, Vec3 pos_end_nv_cx, const int helper_xc)
	{
		if (g_pGameCVars->g_melee_system_adtr_type <= 0)
			return;

		if(MeleeParams_SS.HitsToStatic > 0)
			AdditivePhyTestStart2(pos_start_old_cx, pos_start_nv_cx, pos_end_old_cx, pos_end_nv_cx, helper_xc);

		if (pos_start_nv_cx != Vec3(ZERO) && pos_start_old_cx != Vec3(ZERO))
		{
			if (pos_start_nv_cx != pos_start_old_cx)
			{
				float dist_of_2points = 0.0f;
				float dist_of_2points_orig = 0.0f;
				dist_of_2points = pos_end_nv_cx.GetDistance(pos_end_old_cx);
				dist_of_2points_orig = dist_of_2points;
				dist_of_2points = dist_of_2points / g_pGameCVars->g_melee_system_rcdr_coof;
				int num_rays = (int)dist_of_2points;
				if (num_rays >= MAX_ADDITIVE_RAYS)
					num_rays = MAX_ADDITIVE_RAYS;

				if (g_pGameCVars->g_melee_system_adtr_type == 1)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + (pos_start_old_cx - pos_start_nv_cx) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + (pos_end_old_cx - pos_end_nv_cx) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 2)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalized() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalized() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 3)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 4)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 5)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit3 = pos_start_old_cx + (pos_end_old_cx - pos_start_old_cx) * mult_vv;
							Vec3 new_posit4 = pos_start_nv_cx + (pos_end_nv_cx - pos_start_nv_cx) * mult_vv;
							PerformPhyTest(new_posit3, (new_posit4 - new_posit3), new_posit4, helper_xc);
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 6)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit3 = pos_start_old_cx + (pos_end_old_cx - pos_start_old_cx) * mult_vv;
							Vec3 new_posit4 = pos_start_nv_cx + (pos_end_nv_cx - pos_start_nv_cx) * mult_vv;
							PerformPhyTest(new_posit3, (new_posit4 - new_posit3), new_posit4, helper_xc);
							Vec3 new_posit1 = pos_start_nv_cx + (pos_start_old_cx - pos_start_nv_cx) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + (pos_end_old_cx - pos_end_nv_cx) * mult_vv;
							PerformPhyTest(new_posit1, (new_posit2 - new_posit1), new_posit2, helper_xc);
						}
					}
				}
			}
		}
	}

	virtual void AdditivePhyTestStart2(Vec3 pos_start_old_cx, Vec3 pos_start_nv_cx, Vec3 pos_end_old_cx, Vec3 pos_end_nv_cx, const int helper_xc)
	{
		if (g_pGameCVars->g_melee_system_adtr_type <= 0)
			return;

		if (pos_start_nv_cx != Vec3(ZERO) && pos_start_old_cx != Vec3(ZERO))
		{
			if (pos_start_nv_cx != pos_start_old_cx)
			{
				float dist_of_2points = 0.0f;
				float dist_of_2points_orig = 0.0f;
				dist_of_2points = pos_end_nv_cx.GetDistance(pos_end_old_cx);
				dist_of_2points_orig = dist_of_2points;
				dist_of_2points = dist_of_2points / (g_pGameCVars->g_melee_system_rcdr_coof * 2);
				int num_rays = (int)dist_of_2points;
				if (num_rays >= MAX_ADDITIVE_RAYS / 2)
					num_rays = MAX_ADDITIVE_RAYS / 2;

				if (g_pGameCVars->g_melee_system_adtr_type == 1)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + (pos_start_old_cx - pos_start_nv_cx) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + (pos_end_old_cx - pos_end_nv_cx) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 2)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalized() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalized() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 3)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 4)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 5)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit3 = pos_start_old_cx + (pos_end_old_cx - pos_start_old_cx) * mult_vv;
							Vec3 new_posit4 = pos_start_nv_cx + (pos_end_nv_cx - pos_start_nv_cx) * mult_vv;
							PerformPhyTest2(new_posit3, (new_posit4 - new_posit3), helper_xc);
							Vec3 new_posit1 = pos_start_nv_cx + ((pos_start_old_cx - pos_start_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + ((pos_end_old_cx - pos_end_nv_cx).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
				else if (g_pGameCVars->g_melee_system_adtr_type == 6)
				{
					float coof = 1.0f / num_rays;
					for (int i = 0; i < num_rays; i++)
					{
						if (i > 0 && i != num_rays)
						{
							float mult_vv = i * coof;
							Vec3 new_posit3 = pos_start_old_cx + (pos_end_old_cx - pos_start_old_cx) * mult_vv;
							Vec3 new_posit4 = pos_start_nv_cx + (pos_end_nv_cx - pos_start_nv_cx) * mult_vv;
							PerformPhyTest2(new_posit3, (new_posit4 - new_posit3), helper_xc);
							Vec3 new_posit1 = pos_start_nv_cx + (pos_start_old_cx - pos_start_nv_cx) * mult_vv;
							Vec3 new_posit2 = pos_end_nv_cx + (pos_end_old_cx - pos_end_nv_cx) * mult_vv;
							PerformPhyTest2(new_posit1, (new_posit2 - new_posit1), helper_xc);
						}
					}
				}
			}
		}
	}

	virtual bool PerformPhyTest(const Vec3 &pos, const Vec3 &dir, const Vec3 &orig, const int helper_xc)
	{
		IPhysicalEntity *pEnts_to_ignore[6] = { 0, 0, 0, 0, 0, 0 };
		for (size_t i = 0; i < static_cast<size_t>(idnoredEnts.size()); ++i)
		{
			if (i > 5)
			{
				num_ents_to_ignore = 6;
				break;
			}
			//debug
			//IEntity* pEnt = gEnv->pEntitySystem->GetEntityFromPhysics(idnoredEnts[i]);
			//if (pEnt)
			//{
				//CryLogAlways("PerformPhyTest PhysIgnLst ent name == %s", pEnt->GetName());
			//}
			pEnts_to_ignore[i] = idnoredEnts[i];
		}

		if (!pAttacker || pAttacker->GetHealth() <= 0)
			return false;

		//debug
		//if (pEnts_to_ignore[0] == pAttacker->GetEntity()->GetPhysics())
		//	CryLogAlways("PerformPhyTest pEnts_to_ignore[0] == pAttacker->GetEntity()->GetPhysics()");

		if (MeleeParams_SS.IntersectionType == 2)
		{
			return PerformPhyPrimitiveTest(pos, dir, 1, helper_xc);
		}
		else if (MeleeParams_SS.IntersectionType == 3)
		{
			PerformPhyPrimitiveTest(pos, dir, 1, helper_xc);
		}

		ray_hit hit;
		int n;
		
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(MeleeParams_SS.IntersectionRange / 2), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (n>0)
		{
			IPhysicalEntity *pCollider = hit.pCollider;
			if (pCollider)
			{
				IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
				if (pCollidedEntity)
				{
					if (normalized_hit_direction[helper_xc].IsZeroFast())
						normalized_hit_direction[helper_xc] = hit.n;

					int hitTypeID = Hit(hit.pt, dir, normalized_hit_direction[helper_xc], hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, helper_xc);
					Impulse(hit.pt, dir, normalized_hit_direction[helper_xc], hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
				}
			}
		}
		return n>0;
	}

	bool PerformPhyTest2(const Vec3 &pos, const Vec3 &dir, const int helper_xc)
	{
		if (!pAttacker || pAttacker->GetHealth() <= 0)
			return false;

		if (MeleeParams_SS.IntersectionType == 2)
		{
			return PerformPhyPrimitiveTest2(pos, dir, 1, helper_xc);
		}
		else if (MeleeParams_SS.IntersectionType == 3)
		{
			PerformPhyPrimitiveTest2(pos, dir, 1, helper_xc);
		}

		ray_hit hit;
		int n;
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(MeleeParams_SS.IntersectionRange / 2), ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, NULL, 0);
		
		int hitTypeID = CGameRules::EHitType::Melee;
		if (n>0)
		{
			IPhysicalEntity *pCollider = hit.pCollider;
			IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
			EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
			if (normalized_hit_direction[helper_xc].IsZeroFast())
				normalized_hit_direction[helper_xc] = hit.n;

			//CryLogAlways("PerformPhyTest2");
			Impulse(hit.pt, dir, normalized_hit_direction[helper_xc], hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			Hit(hit.pt, dir, normalized_hit_direction[helper_xc], hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, helper_xc);
		}

		return n>0;
	}

	virtual bool PerformPhyPrimitiveTest2(const Vec3 &pos, const Vec3 &dir, int prim_type, const int helper_xc)
	{
		geom_contact *closestc = 0;
		if (prim_type == 1)
		{
			primitives::cylinder cyl;
			cyl.r = 0.25f * MeleeParams_SS.PrimitiveIntersectionScale;
			cyl.axis = dir;
			cyl.hh = MeleeParams_SS.IntersectionRange*0.5f;
			cyl.center = pos + dir*cyl.hh;

			geom_contact *contacts;
			intersection_params params;
			params.bStopAtFirstTri = false;
			params.bNoBorder = true;
			params.bNoAreaContacts = true;
			float n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO),
				ent_static | ent_terrain | ent_water, &contacts, 0,
				rwi_colltype_any | rwi_stop_at_pierceable, &params, 0, 0, NULL, 0);

			int ret = (int)n;

			float closestdSq = 9999.0f;
			geom_contact *currentc = contacts;

			for (int i = 0; i < ret; i++)
			{
				geom_contact *contact = currentc;
				if (contact)
				{
					IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
					if (pCollider)
					{
						IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
						const float distSq = (pos - currentc->pt).len2();
						if (distSq < closestdSq)
						{
							closestdSq = distSq;
							closestc = contact;
						}
					}
				}
				++currentc;
			}

			if (closestc)
			{
				IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);
				IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
				EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
				if (normalized_hit_direction[helper_xc].IsZeroFast())
					normalized_hit_direction[helper_xc] = closestc->n;

				int hitTypeID = CGameRules::EHitType::Melee;
				Impulse(closestc->pt, dir, normalized_hit_direction[helper_xc], pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], hitTypeID, closestc->iPrim[1]);
				Hit(closestc->pt, dir, normalized_hit_direction[helper_xc], pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], helper_xc);
			}
		}
		return closestc != 0;
	}

	virtual bool PerformPhyPrimitiveTest(const Vec3 &pos, const Vec3 &dir, int prim_type, const int helper_xc)
	{
		IPhysicalEntity *pEnts_to_ignore[6] = { 0, 0, 0, 0, 0, 0 };
		for (size_t i = 0; i < static_cast<size_t>(idnoredEnts.size()); ++i)
		{
			if (i > 5)
			{
				num_ents_to_ignore = 6;
				break;
			}
			pEnts_to_ignore[i] = idnoredEnts[i];
		}
		geom_contact *closestc = 0;
		if (prim_type == 1)
		{
			primitives::cylinder cyl;
			cyl.r = 0.25f * MeleeParams_SS.PrimitiveIntersectionScale;
			cyl.axis = dir;
			cyl.hh = MeleeParams_SS.IntersectionRange*0.5f;
			cyl.center = pos + dir*cyl.hh;

			geom_contact *contacts;
			intersection_params params;
			params.bStopAtFirstTri = false;
			params.bNoBorder = true;
			params.bNoAreaContacts = true;
			float n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO),
				ent_living | ent_sleeping_rigid | ent_rigid | ent_independent, &contacts, 0,
				rwi_colltype_any | rwi_stop_at_pierceable/*geom_colltype_foliage | geom_colltype_player*/,
				&params, 0, 0, pEnts_to_ignore, num_ents_to_ignore);

			int ret = (int)n;

			float closestdSq = 9999.0f;
			geom_contact *currentc = contacts;

			for (int i = 0; i < ret; i++)
			{
				geom_contact *contact = currentc;
				if (contact)
				{
					IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
					if (pCollider)
					{
						for (int i = 0; i < num_ents_to_ignore; ++i)
						{
							if (pCollider == pEnts_to_ignore[i])
							{
								++currentc;
								continue;
							}
						}
						IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
						if (pEntity)
						{
							if (pEntity->GetId() == pAttacker->GetEntityId())
							{
								++currentc;
								continue;
							}
						}
						const float distSq = (pos - currentc->pt).len2();
						if (distSq < closestdSq)
						{
							closestdSq = distSq;
							closestc = contact;
						}
					}
				}
				++currentc;
			}

			if (closestc)
			{
				IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);
				IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
				EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
				if (normalized_hit_direction[helper_xc].IsZeroFast())
					normalized_hit_direction[helper_xc] = closestc->n;

				int hitTypeID = Hit(closestc->pt, dir, normalized_hit_direction[helper_xc], pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], helper_xc);
				Impulse(closestc->pt, dir, normalized_hit_direction[helper_xc], pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], hitTypeID, closestc->iPrim[1]);
			}
		}
		return closestc != 0;
	}

	virtual float GetMeleeDamage()
	{
		if (MeleeParams_SS.CheckDamageMultiplers > 0)
		{
			if(!pAttacker)
				return MeleeParams_SS.Damage;

			float strength = 0.1f * (0.2f + 0.4f * 0.018f);
			strength *= pAttacker->GetActorStrength();
			strength = 0.2f + (strength / 10);

			CItem *m_pWeapon = pAttacker->GetItem(pAttacker->GetCurrentItemId());
			if (!m_pWeapon)
				return MeleeParams_SS.Damage * strength;

			return (((MeleeParams_SS.Damage + m_pWeapon->GetDmgAdded())*strength))*GetWpnCrtAtk();
		}
		else
		{
			return MeleeParams_SS.Damage;
		}
	}

	virtual float GetWpnCrtAtk()
	{
		if (!pAttacker)
			return 1.0f;

		CItem *m_pWeapon = pAttacker->GetItem(pAttacker->GetCurrentItemId());
		if (!m_pWeapon)
			return 1.0f;

		float m_crt_hit_dmg_mult = 1.0f;
		int rnd_value1 = rand() % 100 + 1;

		if (m_pWeapon->GetWeaponType() == 1)
		{
			if (rnd_value1 > 0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_swords_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_swords_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 2)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_axes_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_axes_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 3)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 4)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_thswords_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_thswords_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 5)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_thaxes_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_thaxes_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 6)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 7)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_poles_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_poles_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else if (m_pWeapon->GetWeaponType() == 8)
		{
			if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml*pAttacker->crtchance_thpoles_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml*pAttacker->crtdmg_thpoles_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		else
		{
			if (rnd_value1>0 && rnd_value1 < m_pWeapon->GetCriticalHitBaseChance()*pAttacker->crtchance_common_ml)
			{
				m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pAttacker->crtdmg_common_ml;
				return m_crt_hit_dmg_mult;
			}
		}
		return 1.0f;
	}

	virtual int Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, const int helper_xc)
	{
		int hitTypeID = 0;
		if (!pAttacker)
			return 1;

		int m_hitTypeID = CGameRules::EHitType::Melee;
		if (!MeleeParams_SS.DamageType.empty())
		{
			CGameRules *pGameRules = g_pGame->GetGameRules();
			if (pGameRules)
			{
				int h_t_id = pGameRules->GetHitTypeId(MeleeParams_SS.DamageType.c_str());
				if (h_t_id > 0)
					m_hitTypeID = h_t_id;
			}
		}

		IEntity *pTargetEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
		if (pTargetEnt)
		{

			std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
			std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
			int count = m_hitedEntites.size();

			for (int i = 0; i < count, it != end; i++, ++it)
			{
				if (pTargetEnt->GetId() == (*it))
				{
					return 1;
				}
			}
		}
		CActor *pOwnerActor = pAttacker;
		if (pOwnerActor)
		{
			IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
			IEntity* pTarget = pTargetActor ? pTargetActor->GetEntity() : gEnv->pEntitySystem->GetEntity(collidedEntityId);
			IEntity* pOwnerEntity = pOwnerActor->GetEntity();
			IAIObject* pOwnerAI = pOwnerEntity->GetAI();

			float damageScale = 1.0f;
			bool silentHit = false;
			bool effects_from_wpn_added = false;
			if (pTargetActor)
			{
				CActor *pActor = static_cast<CActor*>(pTargetActor);
				if (pActor)
				{
					if (pActor->GetHealth() <= 0)
					{

					}
				}
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					//add effect in its need
					effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), pt, normal);
					//check block
					if (!nwAction->TestHitBlock(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
					{
						if (MeleeParams_SS.IgnoreBlock == 0)
						{
							return 1;
						}
					}
				}
				IAnimatedCharacter* pTargetAC = pTargetActor->GetAnimatedCharacter();
				IAnimatedCharacter* pOwnerAC = pOwnerActor->GetAnimatedCharacter();

				if (pTargetAC && pOwnerAC)
				{
					Vec3 targetFacing(pTargetAC->GetAnimLocation().GetColumn1());
					Vec3 ownerFacing(pOwnerAC->GetAnimLocation().GetColumn1());
					float ownerFacingDot = ownerFacing.Dot(targetFacing);
					float fromBehindDot = cos_tpl(DEG2RAD(g_pGameCVars->pl_melee.angle_limit_from_behind));

					if (ownerFacingDot > fromBehindDot)
					{
						damageScale *= g_pGameCVars->pl_melee.damage_multiplier_from_behind;
					}
				}
			}
			else
			{
				CItem* pItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
				if (pItemTarget)
				{
					if (pItemTarget->GetOwnerActor())
					{
						CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
						if (nwAction)
						{
							CGameRules *pGameRules = g_pGame->GetGameRules();
							if (pGameRules)
							{
								float damage = GetMeleeDamage();
								if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
								{
									float dist_xc = pt.GetDistance(pos_start[0]);
									if (dist_xc > 2.0f)
										dist_xc = 2.0f;

									float dist_dmg_coof = damage * 0.05f;
									dist_xc *= 0.3f;
									//bigger distance == more damage;
									damage = damage - dist_dmg_coof / dist_xc;
								}

								if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
								{
									if (pOwnerActor)
									{
										float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
										if (vel_pwr > 5.0f)
											vel_pwr = 5.0f;

										float vel_dmg_coof = damage * 0.32f;
										vel_pwr *= 0.3f;
										//bigger velocity == more damage;
										damage = damage + (vel_dmg_coof * vel_pwr);
									}
								}
								hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
								HitInfo info(pAttacker->GetEntityId(), pTarget->GetId(), pAttacker->GetCurrentItemId(),
									damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
								pGameRules->ClientHit(info);
								m_hitedEntites.push_back(pTarget->GetId());
							}
							//add effect in its need
							effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId());
							/*if (effects_from_wpn_added)
							{
							pItemTarget->GetOwnerActor()->SetLastDamagePosAndNormal(pt, normal);
							}*/
							//check block
							if (!nwAction->TestHitBlock(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
							{
								if (MeleeParams_SS.IgnoreBlock == 0)
								{
									return 1;
								}
							}
							else
							{
								if (pGameRules && pItemTarget->GetOwnerActor())
								{
									float damage = GetMeleeDamage();
									if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
									{
										float dist_xc = pt.GetDistance(pos_start[0]);
										if (dist_xc > 2.0f)
											dist_xc = 2.0f;

										float dist_dmg_coof = damage * 0.05f;
										dist_xc *= 0.3f;
										//bigger distance == more damage;
										damage = damage - dist_dmg_coof / dist_xc;
									}

									if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
									{
										if (pOwnerActor)
										{
											float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
											if (vel_pwr > 5.0f)
												vel_pwr = 5.0f;

											float vel_dmg_coof = damage * 0.32f;
											vel_pwr *= 0.3f;
											//bigger velocity == more damage;
											damage = damage + (vel_dmg_coof * vel_pwr);
										}
									}
									hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
									HitInfo info(pAttacker->GetEntityId(), pItemTarget->GetOwnerActor()->GetEntityId(), pAttacker->GetCurrentItemId(),
										damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
									pGameRules->ClientHit(info);
									m_hitedEntites.push_back(pItemTarget->GetOwnerActor()->GetEntityId());
								}

								if (MeleeParams_SS.IgnoreBlock == 0)
								{
									return 1;
								}
							}
						}
					}
				}
			}


			// Send target stimuli
			if (!gEnv->bMultiplayer)
			{
				IAISystem *pAISystem = gEnv->pAISystem;
				ITargetTrackManager *pTargetTrackManager = pAISystem ? pAISystem->GetTargetTrackManager() : NULL;
				if (pTargetTrackManager && pOwnerAI)
				{
					IAIObject *pTargetAI = pTarget ? pTarget->GetAI() : NULL;
					if (pTargetAI)
					{
						const tAIObjectID aiOwnerId = pOwnerAI->GetAIObjectID();
						const tAIObjectID aiTargetId = pTargetAI->GetAIObjectID();

						TargetTrackHelpers::SStimulusEvent eventInfo;
						eventInfo.vPos = pt;
						eventInfo.eStimulusType = TargetTrackHelpers::eEST_Generic;
						eventInfo.eTargetThreat = AITHREAT_AGGRESSIVE;
						pTargetTrackManager->HandleStimulusEventForAgent(aiTargetId, aiOwnerId, "MeleeHit", eventInfo);
						pTargetTrackManager->HandleStimulusEventInRange(aiOwnerId, "MeleeHitNear", eventInfo, 5.0f);
					}
				}
			}

			//Check if is a friendly hit, in that case FX and Hit will be skipped
			bool isFriendlyHit = (pOwnerEntity && pTarget) ? IsFriendlyHit(pOwnerEntity, pTarget) : false;
			if (g_pGameCVars->i_enable_frenldly_hits > 0)
				isFriendlyHit = false;

			if (!isFriendlyHit)
			{
				CPlayer * pAttackerPlayer = pOwnerActor->IsPlayer() ? static_cast<CPlayer*>(pOwnerActor) : NULL;
				float damage = GetMeleeDamage();
				//Generate Hit
				if (pTarget)
				{
					int bl_tp = 0;
					SmartScriptTable props;
					IScriptTable* pScriptTable = pTarget->GetScriptTable();
					if (pScriptTable)
					{
						if (pScriptTable && pScriptTable->GetValue("Properties", props))
						{
							CScriptSetGetChain prop(props);
							prop.GetValue("blood_type", bl_tp);
						}
					}
					CGameRules *pGameRules = g_pGame->GetGameRules();
					if (pGameRules)
					{
						hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
						HitInfo info(pAttacker->GetEntityId(), pTarget->GetId(), pAttacker->GetCurrentItemId(),
							damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
						info.remote = false;
						pGameRules->ClientHit(info);
						m_hitedEntites.push_back(pTarget->GetId());
					}
					if (pAttackerPlayer && pAttackerPlayer->IsClient())
					{
						CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
						if (nwAction_x)
						{
							nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, true);
						}
						const Vec3 posOffset = (pt - pTarget->GetWorldPos());
						SMeleeHitParams params;
						params.m_boostedMelee = false;
						params.m_hitNormal = normal;
						params.m_hitOffset = posOffset;
						params.m_surfaceIdx = surfaceIdx;
						params.m_targetId = pTarget->GetId();
						pAttackerPlayer->OnMeleeHit(params);
					}
					else
					{
						PlayHitMaterialEffect(pt, normal, surfaceIdx);
					}
					CActor *pActor = static_cast<CActor*>(pTargetActor);
					if (pActor)
					{
						CPlayer * pTargetPlayer = pActor->IsPlayer() ? static_cast<CPlayer*>(pActor) : NULL;
						if (pTargetPlayer)
						{
							pTargetPlayer->OnMeleeDamage();
						}
					}
				}
				else
				{
					//CryLogAlways("CProceduralMeleeAttack hit no target");
					//PlayHitMaterialEffect(pt, normal, surfaceIdx);
					bool can_spwn_eff_mt = false;
					if (m_updtimer2 > cry_random(0.05f, 0.3f) && num_mtl_eff_spwn[helper_xc] < 4)
					{
						m_updtimer2 = 0.0f;
						can_spwn_eff_mt = true;
						num_mtl_eff_spwn[helper_xc] += 1;
					}
					//Play Material FX
					/*if ((m_updtimer <= m_clp_lght * 0.8f) && num_mtl_eff_spwn[helper_xc] == 0)
					{
						can_spwn_eff_mt = true;
						num_mtl_eff_spwn[helper_xc] += 1;
					}
					else if ((m_updtimer <= m_clp_lght * 0.6f) && num_mtl_eff_spwn[helper_xc] == 1)
					{
						can_spwn_eff_mt = true;
						num_mtl_eff_spwn[helper_xc] += 1;
					}
					else if ((m_updtimer <= m_clp_lght * 0.4f) && num_mtl_eff_spwn[helper_xc] == 2)
					{
						can_spwn_eff_mt = true;
						num_mtl_eff_spwn[helper_xc] += 1;
					}
					else if ((m_updtimer <= m_clp_lght * 0.2f) && num_mtl_eff_spwn[helper_xc] == 3)
					{
						can_spwn_eff_mt = true;
						num_mtl_eff_spwn[helper_xc] += 1;
					}*/

					if (can_spwn_eff_mt)
					{
						CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
						if (nwAction_x)
						{
							if (pAttackerPlayer && pAttackerPlayer->IsClient())
							{
								nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
							}
						}
						PlayHitMaterialEffect(pt, normal, surfaceIdx);
					}

					if (g_pGameCVars->g_melee_sys_enable_close_waills_reaction > 0)
					{
						if (CheckCollisionWD(pt, surfaceIdx, helper_xc))
						{
							CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
							if (nwAction_x)
							{
								if (pAttackerPlayer && pAttackerPlayer->IsClient())
								{
									nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
								}
							}
							PlayHitMaterialEffect(pt, normal, surfaceIdx);
							return 1;
						}
					}
				}
			}
		}
		return hitTypeID;
	}

	virtual bool IsFriendlyHit(IEntity* pShooter, IEntity* pTarget)
	{
		if (gEnv->bMultiplayer)
		{
			if (g_pGame->GetGameRules()->GetFriendlyFireRatio() <= 0.f)
			{
				CActor* pTargetActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
				if (pTargetActor)
				{
					return pTargetActor->IsFriendlyEntity(pShooter->GetId()) != 0;
				}
			}
		}
		else
		{
			IActor* pAITarget = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
			if (pAITarget && pTarget->GetAI() && !pTarget->GetAI()->IsHostile(pShooter->GetAI(), false))
			{
				return true;
			}
		}

		return false;
	}

	virtual void PlayHitMaterialEffect(const Vec3 &position, const Vec3 &normal, int surfaceIdx)
	{
		//Play Material FX
		const char* meleeFXType = MeleeParams_SS.HitFXType;
		if (MeleeParams_SS.UseWeaponModel > 0)
		{
			if (pAttacker)
			{
				CItem *m_pWeapon = pAttacker->GetItem(pAttacker->GetCurrentItemId());
				if (m_pWeapon)
				{
					meleeFXType = m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str();
				}
			}
		}

		if (string(meleeFXType).empty())
		{
			meleeFXType = "melee";
		}

		IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();
		Vec3 nv_normal = normal;
		if (surfaceIdx == 141)
		{
			if (nv_normal.z < 0.0f)
			{
				nv_normal.z = 0.2f;
			}
		}
		TMFXEffectId effectId = pMaterialEffects->GetEffectId(meleeFXType, surfaceIdx);
		if (effectId != InvalidEffectId)
		{
			SMFXRunTimeEffectParams params;
			params.pos = position;
			params.normal = nv_normal;
			params.playflags = eMFXPF_All | eMFXPF_Disable_Delay;
			pMaterialEffects->ExecuteEffect(effectId, params);
		}
	}

	virtual bool CheckCollisionWD(Vec3 position, int surfaceIdx, const int helper_xc)
	{
		if (surfaceIdx == 141 || !pAttacker)
		{
			return false;
		}
		float dist = pos_start_nv[helper_xc].GetDistance(position);
		if (MeleeParams_SS.EnableWallHitStop > 0)
		{
			float mx_val = MeleeParams_SS.WallHitStopRange;
			if (mx_val <= 0.0f)
				mx_val = g_pGameCVars->g_melee_sys_wail_max_dist;

			if (dist < mx_val && mx_val > 0.0f)
			{
				if (pAttacker->GetItem(pAttacker->GetCurrentItemId()))
				{
					const ItemString &meleeAction = "melee_attack_close_hit_on_wail";
					FragmentID fragmentId = pAttacker->GetItem(pAttacker->GetCurrentItemId())->GetFragmentID(meleeAction.c_str());
					pAttacker->GetItem(pAttacker->GetCurrentItemId())->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
				}
				return true;
			}
		}
		return false;
	}

	virtual void Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, int hitTypeID, int iPrim)
	{
		if (pCollider && MeleeParams_SS.ImpulseValue>0.001f)
		{
			CActor* pOwnerActor = pAttacker;
			if (!pOwnerActor)
				return;

			const SPlayerMelee& meleeCVars = g_pGameCVars->pl_melee;
			float impulse = MeleeParams_SS.ImpulseValue;
			bool aiShooter = pOwnerActor ? !pOwnerActor->IsPlayer() : true;
			bool delayImpulse = false;
			float impulseScale = 1.0f;
			//[kirill] add impulse to phys proxy - to make sure it's applied to cylinder as well (not only skeleton) - so that entity gets pushed
			// if no pEntity - do it old way
			IEntity * pEntity = gEnv->pEntitySystem->GetEntity(collidedEntityId);
			IGameFramework* pGameFramework = g_pGame->GetIGameFramework();
			CActor* pTargetActor = static_cast<CActor*>(pGameFramework->GetIActorSystem()->GetActor(collidedEntityId));
			if (pEntity && pTargetActor)
			{
				//If it's an entity, use the specific impulses if needed, and apply to physics proxy
				if (meleeCVars.impulses_enable == SPlayerMelee::ei_Disabled)
				{
					impulse = 0.0f;
				}
			}

			float speed = sqrt_tpl(4000.0f / (80.0f*0.5f));
			if (surfaceIdx == 141)
			{
				impulse = 1.0f;
				impulseScale = 1.0f;
				speed = 0.3f;
			}

			const float fScaledImpulse = impulse * impulseScale;
			if (fScaledImpulse > 0.0f)
			{
				if (!delayImpulse)
				{
					m_collisionHelper.Impulse(pCollider, pt, dir * fScaledImpulse, partId, ipart, hitTypeID);
				}
				else
				{
					//Force up impulse, to make the enemy fly a bit
					Vec3 newDir = (dir.z < 0.0f) ? Vec3(dir.x, dir.y, 0.1f) : dir;
					newDir.Normalize();
					newDir.x *= fScaledImpulse;
					newDir.y *= fScaledImpulse;
					newDir.z *= impulse;

					if (pTargetActor)
					{
						pe_action_impulse imp;
						imp.iApplyTime = 0;
						imp.impulse = newDir;
						//imp.ipart = ipart;
						imp.partid = partId;
						imp.point = pt;
						pTargetActor->GetImpulseHander()->SetOnRagdollPhysicalizedImpulse(imp);
					}
				}
			}

			if (IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC))
			{
				speed = 0.003f;
			}
			//CryLogAlways("CProceduralMeleeAttack impusle");
			m_collisionHelper.GenerateArtificialCollision(pAttacker->GetEntity(), pCollider, pt, normal, dir * speed, partId, ipart, surfaceIdx, iPrim);
		}
	}
	Vec3 pos_initial[2];
	Vec3 pos_start_old[2];
	Vec3 pos_end_old[2];
	Vec3 pos_start_nv[2];
	Vec3 pos_end_nv[2];
	Vec3 pos_start[2];
	Vec3 pos_end[2];
	Vec3 normalized_hit_direction[2];
	SPrcMeleeAttackParams MeleeParams_SS;
	std::vector<EntityId> m_hitedEntites;
	float m_updtimer;
	float m_updtimer2;
	float m_clp_lght;
	float m_updtimer3 = 0.0f;
	int num_mtl_eff_spwn[2];
	int pause_state;
	int num_ents_to_ignore;
	CActorActionsNew::PhysIgnLst idnoredEnts;
	CMeleeCollisionHelper m_collisionHelper;
	CActor *pAttacker;
};

REGISTER_PROCEDURAL_CLIP(CProceduralClipEventNvc, "EventNvc");
REGISTER_PROCEDURAL_CLIP(CProceduralClipSetupFov, "SetupFovMultipler");
REGISTER_PROCEDURAL_CLIP(CProceduralMeleeAttack, "ProceduralMeleeAttack");
REGISTER_PROCEDURAL_CLIP(CProceduralClipConsoleCommandCall, "ConsoleCommandCall");
REGISTER_PROCEDURAL_CLIP(CProceduralClipCameraShake, "ProceduralCameraShake");
REGISTER_PROCEDURAL_CLIP(CProceduralClipCameraAnimControlled, "CameraAnimControlled");
