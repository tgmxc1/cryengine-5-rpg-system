// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
 -------------------------------------------------------------------------
  $Id$
  $DateTime$
  
 -------------------------------------------------------------------------
  History:
  - 7:10:2004   14:48 : Created by Márcio Martins
												taken over by Filippo De Luca

*************************************************************************/
#include "StdAfx.h"
#include <CryString/StringUtils.h>
#include "Game.h"
#include "GameCVars.h"
#include "GamePhysicsSettings.h"
#include "Actor.h"
#include "ScriptBind_Actor.h"
#include <CryNetwork/ISerialize.h>
#include <CryGame/GameUtils.h>
#include <CryAnimation/ICryAnimation.h>
#include <CryGame/IGameTokens.h>
#include <IItemSystem.h>
#include <IInteractor.h>
#include "Item.h"
#include "Weapon.h"
#include "WeaponSharedParams.h"
#include "WeaponSystem.h"
#include "Player.h"
#include "GameRules.h"
#include "Battlechatter.h"
#include <CryAction/IMaterialEffects.h>
#include "IVehicleSystem.h"
#include <CryAISystem/IAgent.h>
#include "IPlayerInput.h"
#include "Utility/StringUtils.h"
#include "Utility/DesignerWarning.h"
#include "UI/UIManager.h"
#include "UI/HUD/HUDEventDispatcher.h"
#include "UI/HUD/HUDEventWrapper.h"
#include <CryAnimation/IFacialAnimation.h>
#include "ScreenEffects.h"
#include "LagOMeter.h"
#include "TacticalManager.h"
#include "EquipmentLoadout.h"
#include "ActorImpulseHandler.h"
#include <CrySystem/ISystem.h>
#include <CrySystem/Profilers/IStatoscope.h>
//xml необходим
#include <CrySystem/XML/IXml.h>
#include "UI/HUD/HUDCommon.h"
#include "ActorActionsNew.h"
#include "Bow.h"
#include "Crossbow.h"
#include "PlayerCharacterCreatingSys.h"

#include <CryNetwork/INetwork.h>
#include "AI/GameAISystem.h"
#include "AI/GameAIEnv.h"
#include "Utility/CryWatch.h"

#include "SkillKill.h"
#include "RecordingSystem.h"
#include "ActorManager.h"
#include "GameCodeCoverage/GameCodeCoverageTracker.h"

#include "GameCache.h"

#include <CryGame/IGameStatistics.h>
#include <CryAISystem/IAIActor.h>

#include "BodyManagerCVars.h"

#include "BodyManager.h"
#include "EntityUtility/EntityEffectsCloak.h"
#include "FireMode.h"

#include "GameRulesModules/IGameRulesSpawningModule.h"
#include "Network/Lobby/GameLobby.h"

#include "EntityUtility/EntityScriptCalls.h"

#include "GameRulesModules/IGameRulesPlayerSetupModule.h"
#include "GameRulesModules/IGameRulesSpectatorModule.h"

#include "ProceduralContextRagdoll.h"
#include "AnimActionBlendFromRagdoll.h"

#include "Projectile.h"

#include "PlayerInput.h"



IItemSystem *CActor::m_pItemSystem=0;
IGameFramework	*CActor::m_pGameFramework=0;
IGameplayRecorder	*CActor::m_pGameplayRecorder=0;

#if !defined(DEMO_BUILD_RPG_SS)
	#define DEBUG_ACTOR_STATE
#endif

#if !defined(DEMO_BUILD_RPG_SS)
	#define STANCE_DEBUG
#endif


SStanceInfo CActor::m_defaultStance;

#define PHYSICS_COUNTER_MAX		4

#define kNumFramesUntilDisplayNullStanceWarning   200

AUTOENUM_BUILDNAMEARRAY(s_BONE_ID_NAME, ActorBoneList);

static void DoNotDeleteThisPointer(void* pPointer) {}

const char CActor::DEFAULT_ENTITY_CLASS_NAME[] = "Default";

//------------------------------------------------------------------------
// "W" stands for "world"
void SIKLimb::SetWPos(IEntity *pOwner,const Vec3 &pos,const Vec3 &normal,float blend,float recover,int requestID)
{
  assert(!_isnan(pos.len2()));
  assert(!_isnan(normal.len2()));
  assert(pos.len()<25000.f);

	// NOTE Dez 13, 2006: <pvl> request ID's work like priorities - if
	// the new request has an ID lower than the one currently being performed,
	// nothing happens. 
	if (requestID<blendID)
		return;

	goalWPos = pos;
	goalNormal = normal;

	if (requestID!=blendID)
	{
		blendTime = blend;
		invBlendTimeMax = 1.0f / blend;
		blendID = requestID;
	}
	else if (blendTime<0.001f)
	{
		blendTime = 0.0011f;
	}

	recoverTime = recoverTimeMax = recover;
}

void SIKLimb::SetLimb(int slot,const char *limbName,int rootID,int midID,int endID,int iFlags)
{
	rootBoneID = rootID;
	endBoneID = endID;
	middleBoneID = midID;

	cry_strcpy(name, limbName);

	blendID = -1;

	flags = iFlags;

	characterSlot = slot;
}

void SIKLimb::Update(IEntity *pOwner,float frameTime)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		CActor *PPAct = pGameGlobals->GetThisActor(pOwner->GetId());
		if (PPAct && PPAct->block_updates)
			return;
	}
	

	assert(pOwner);

	ICharacterInstance *pCharacter = pOwner->GetCharacter(characterSlot);
	if (!pCharacter)
		return;

	// pvl: the correction for translation is to be removed once character offsets are redone
//	lAnimPos = pCharacter->GetISkeleton()->GetAbsJointByID(endBoneID).t - pOwner->GetSlotLocalTM (characterSlot, false).GetTranslation ();

	Vec3 vRootBone = pCharacter->GetISkeletonPose()->GetAbsJointByID(0).t; // - pOwner->GetSlotLocalTM (characterSlot, false).GetTranslation ();
	vRootBone.z=0;
	lAnimPos = pCharacter->GetISkeletonPose()->GetAbsJointByID(endBoneID).t-vRootBone;// - pOwner->GetSlotLocalTM (characterSlot, false).GetTranslation ();
  
  assert(!_isnan(lAnimPos.len2()));

	bool setLimbPos(true);
	Vec3 finalPos=Vec3(ZERO);

	if (blendTime>0.001f)
	{
		Vec3 limbWPos = currentWPos;
		finalPos = goalWPos;

		blendTime = (float)__fsel(-fabsf(blendTime - 0.0011f), 0.0f, blendTime);

		finalPos -= (finalPos - limbWPos) * min(blendTime * invBlendTimeMax,1.0f);
		currentWPos = finalPos;

		blendTime -= frameTime;
	}
	else if (recoverTime>0.001f)
	{
		Vec3 limbWPos = currentWPos;
		finalPos = pOwner->GetSlotWorldTM(characterSlot) * lAnimPos;

		finalPos -= (finalPos - limbWPos) * min(recoverTime / recoverTimeMax,1.0f);
		currentWPos = finalPos;
		goalNormal.zero();

		recoverTime -= frameTime;
		
		if (recoverTime<0.001f)
			blendID = -1;
	}
	else
	{
		currentWPos = pOwner->GetSlotWorldTM(characterSlot) * lAnimPos;
		setLimbPos = false;
	}

  assert(!_isnan(finalPos.len2()));
  assert(!_isnan(goalNormal.len2()));

	if (setLimbPos)
	{
		if (flags & IKLIMB_RIGHTHAND)
			pCharacter->GetISkeletonPose()->SetHumanLimbIK(finalPos,"RgtArm01"); //SetRArmIK(finalPos);
		else if (flags & IKLIMB_LEFTHAND)
			pCharacter->GetISkeletonPose()->SetHumanLimbIK(finalPos,"LftArm01");  //SetLArmIK(finalPos);
	}
}

//--------------------
IVehicle *SLinkStats::GetLinkedVehicle()
{
	if (!linkID)
		return NULL;
	else
	{
		IVehicle *pVehicle = NULL;
		if(g_pGame && g_pGame->GetIGameFramework() && g_pGame->GetIGameFramework()->GetIVehicleSystem())
			pVehicle = g_pGame->GetIGameFramework()->GetIVehicleSystem()->GetVehicle(linkID);
		//if the vehicle doesnt exist and this is supposed to be a vehicle linking forget about it.
		if (!pVehicle && flags & LINKED_VEHICLE)
			UnLink();

		return pVehicle;
	}
}

void SLinkStats::Serialize(TSerialize ser)
{
	assert(ser.GetSerializationTarget() != eST_Network);

	ser.BeginGroup("PlayerLinkStats");

	//when reading, reset the structure first.
	if (ser.IsReading())
		*this = SLinkStats();

	ser.Value("linkID", linkID);
	ser.Value("flags", flags);

	ser.EndGroup();
}
//--------------------

SActorAnimationEvents CActor::s_animationEventsTable;

void SActorAnimationEvents::Init()
{
	if (!m_initialized)
	{
		m_soundId = CCrc32::ComputeLowercase("sound");
		m_plugginTriggerId = CCrc32::ComputeLowercase("PluginTrigger");
		m_footstepSignalId = CCrc32::ComputeLowercase("footstep");
		m_foleySignalId = CCrc32::ComputeLowercase("foley");
		m_groundEffectId = CCrc32::ComputeLowercase("groundEffect");

		m_swimmingStrokeId = CCrc32::ComputeLowercase("swimmingStroke");
		m_footStepImpulseId = CCrc32::ComputeLowercase("footstep_impulse");
		m_forceFeedbackId = CCrc32::ComputeLowercase("ForceFeedback");
		m_grabObjectId = CCrc32::ComputeLowercase("FlagGrab");
		m_stowId = CCrc32::ComputeLowercase("Stow");
		m_weaponLeftHandId = CCrc32::ComputeLowercase("leftHand");
		m_weaponRightHandId = CCrc32::ComputeLowercase("rightHand");

		m_deathReactionEndId = CCrc32::ComputeLowercase("DeathReactionEnd");
		m_reactionOnCollision = CCrc32::ComputeLowercase("ReactionOnCollision");
		m_forbidReactionsId = CCrc32::ComputeLowercase("ForbidReactions");
		m_ragdollStartId = CCrc32::ComputeLowercase( "RagdollStart");

		m_deathBlow = CCrc32::ComputeLowercase("DeathBlow");
		m_killId = CCrc32::ComputeLowercase("Kill");

		m_startFire = CCrc32::ComputeLowercase("StartFire");
		m_stopFire = CCrc32::ComputeLowercase("StopFire");

		m_shootGrenade = CCrc32::ComputeLowercase("ShootGrenade");

		m_meleeHitId = CCrc32::ComputeLowercase("MeleeHit");
		m_meleeStartDamagePhase = CCrc32::ComputeLowercase("MeleeStartDamagePhase");
		m_meleeEndDamagePhase = CCrc32::ComputeLowercase("MeleeEndDamagePhase");
		m_detachEnvironmentalWeapon = CCrc32::ComputeLowercase("DetachEnvironmentalWeapon");
		m_stealthMeleeDeath = CCrc32::ComputeLowercase("StealthMeleeDeath");

		m_endReboundAnim = CCrc32::ComputeLowercase("EndReboundAnim");
	}

	m_initialized = true;
}

//------------------------------------------------------------------------
CActor::CActor()
: m_pAnimatedCharacter(0)
, m_isClient(false)
, m_isPlayer(false)
, m_isMigrating(false)
, m_pMovementController(0)
, m_stance(STANCE_NULL)
, m_desiredStance(STANCE_NULL)
,	m_teamId(0)
, m_pInventory(0)
, m_cloakLayerActive(false)
, m_registeredInAutoAimMng(false)
, m_registeredAnimationDBAs(false)
, m_lastUnCloakTime(0.0f)
, m_IsImmuneToForbiddenZone(false)
, m_bAllowHitImpulses(true)
, m_spectateSwitchTime(0.f)
, m_fAwaitingServerUseResponse(0.f)
, m_DefaultBodyDamageProfileId(INVALID_BODYDAMAGEPROFILEID)
, m_OverrideBodyDamageProfileId(INVALID_BODYDAMAGEPROFILEID)
, m_bAwaitingServerUseResponse(false)
, m_shouldPlayHitReactions(true)
, m_pendingDropEntityId(0)
, m_lastNetItemId(0)

//-------------
, m_strength(1.0f)
, m_agility(10.0f)
, m_willpower(10.0f)
, m_endurance(10.0f)
, m_personality(10.0f)
, m_intelligence(10.0f)
, m_level(1)
, m_gender(0)
, m_gold(0)
, m_dialogstage(1)
, m_magicka(100.0f)
, m_maxMagicka(0)
, m_stamina(100.0f)
, m_maxStamina(0)

, st_run_drain_base(-1.0f)
, st_run_drain_timing(0)

, maxhp_ml(1)
, maxmp_ml(1)
, maxst_ml(1)

, maxhp_mod(0)
, maxmp_mod(0)
, maxst_mod(0)

, num_buffs(0)
, num_perks(0)

, m_blockactiveshield(false)
, m_blockactivenoshield(false)

, str_mod(0)
, agl_mod(0)
, int_mod(0)
, will_mod(0)
, endr_mod(0)
, pers_mod(0)

, bows_ml(1)
, swords_ml(1)
, thswords_ml(1)
, poles_ml(1)
, thpoles_ml(1)
, axes_ml(1)
, thaxes_ml(1)

, hpreg_ml(1)
, mpreg_ml(1)
, streg_ml(1)

, hpreg_base(0)
, mpreg_base(0.5)
, streg_base(0.5)

, hpreg_timing(1)
, mpreg_timing(1)
, streg_timing(0.4f)

, crtchance_common_ml(1)
, crtdmg_common_ml(1)

, crtchance_bows_ml(1)
, crtdmg_bows_ml(1)

, crtchance_swords_ml(1)
, crtdmg_swords_ml(1)

, crtchance_thswords_ml(1)
, crtdmg_thswords_ml(1)

, crtchance_poles_ml(1)
, crtdmg_poles_ml(1)

, crtchance_thpoles_ml(1)
, crtdmg_thpoles_ml(1)

, crtchance_axes_ml(1)
, crtdmg_axes_ml(1)

, crtchance_thaxes_ml(1)
, crtdmg_thaxes_ml(1)

, selfspells_mpcons_common_ml(1)
, spells_mpcons_common_ml(1)
, selfspells_pwr_common_ml(1)
, spells_pwr_common_ml(1)

, stamina_cons_jump_base(20)
, stamina_cons_run_base(3.5f)
, stamina_cons_atk_base(15)
, stamina_cons_jump_ml(1)
, stamina_cons_run_ml(1)
, stamina_cons_atk_ml(1)
, stamina_cons_block_ml(1)

, stamina_cons_ml_overheat(2)
, stamina_cons_ml_frost(3)

, magicka_cons_ml_overheat(3)
, magicka_cons_ml_frost(0.7)


, m_wpnselecteditem(0)
, m_wear_boots_id(0)
, m_wear_arms_id(0)
, m_wear_helm_id(0)
, m_wear_curr_id(0)
, m_wear_pants_id(0)
, m_wear_shield_id(0)
, m_wear_torch_id(0)
, m_wear_ammoarrows_id(0)
, m_wpn_left_hand_id(0)


, m_equipmentswitchboots(false)
, m_equipmentswitchlegs(false)
, m_equipmentswitchchest(false)
, m_equipmentswitcharms(false)
, m_equipmentswitchhead(false)
, m_equipmentswitchshield(false)
, m_equipmentswitchtorch(false)

, m_boots_num_parts(0)
, m_llegs_num_parts(0)
, m_chest_num_parts(0)
, m_arms_num_parts(0)


, m_hpRegenRate(0)
, m_mpRegenRate(0)
, m_stRegenRate(0)
, m_hpTime(0)
, m_hpAcc(0)
, m_mpAcc(0)
, m_stAcc(0)
, m_run_culldown(3)
, m_run_multipler(1)
, m_stmnTime(0)
, m_staminaRegenRate(0)
, m_stealth_mode(false)
, m_relaxed_mode(false)
, item_on_back(0)
, m_special_action_anim_playing_time(0.0f)
, m_special_action_id(0)
, m_levelxp(0.0f)
, m_xptonextlevel(200.0f)
, inventory_bag_max_slots(30)
, m_jump_timer(0.0f)
, additional_pack_added(false)
, additional_pack2_added(false)
, additional_pack_add_request(false)
, additional_pack2_add_request(false)
, additional_pack_equipped(false)
, additional_pack2_equipped(false)
, equipping_timer(0.5f)
, m_process_falling_timer_c1(0.0f)
, m_process_falling_timer_c2(0.0f)
, m_process_falling_current_vector(ZERO)
, m_process_falling_controll_vector(ZERO)
, m_process_falling_controll_start_rot(ZERO)
, m_process_falling_controll_dinamic_rot(ZERO)
, last_damage_hit_pos(ZERO)
, last_damage_hit_normal(ZERO)
, Current_target_dir(ZERO)
, m_falling_max_speed_add(3.0f)
, m_falling_max_speed_multipler(3.0f)
, m_falling_base_speed(0.5f)
, m_falling_vector_z_add(0.08f)
, m_falling_vector_max_vel_xyz(10.0f)
, delayed_physicalization_request(0.0f)
, delayed_add_packs_request(0.0f)
, old_veloc_z(0.0f)
, old_gravity_z(0.0f)
, blood_loose_effect_counter(0)
, imd_equip_at_next(false)
, block_updates(false)
, start_update_timer_xc(0.0f)
, fdir_pos_for_pl_rv_mode(ZERO)
, m_TimerDelayedReequipPhyReqItems(NULL)
//------------------------------------------------------------------------
, base_mass_val(0.0f)
, inventory_mass_val(0.0f)
, attack_timer_smpl(0.0f)
, alt_aim_pose_old_angle_horiz(0.0f)
, alt_aim_pose_old_angle_vert(0.0f)
, m_chrmodel("")
, m_chrmodelstr("")
, m_underwear01("")
, m_underwear02("")
, m_pppp("")
, m_headmodel("")
, m_eye_l("")
, m_eye_r("")
, m_hairmodel("")
, m_lowerbody_model("")
, m_upperbody_model("")
, m_hands_model("")
, m_foots_model("")
, m_complex_attachments_timer(0.5f)
, lazy_update_timer(0.0f)
//---------------


{
	//CryLogAlways("CActor::CActor() - constructor call");
	m_currentPhysProfile=CActor::GetDefaultProfile(eEA_Physics);

	m_pImpulseHandler.reset(new CActorImpulseHandler(*this));
	CRY_ASSERT(m_pImpulseHandler != NULL);

	m_timeImpulseRecover = 0.0f;
	m_airResistance = 0.0f;
	m_airControl = 1.0f;
	m_inertia = 10.0f;
	m_inertiaAccel = 11.0f;
	m_netLastSelectablePickedUp = 0;
	m_enableSwitchingItems = true;
	m_enableIronSights = true;
	m_enablePickupItems = true;
	m_pLegsCollider[0]=m_pLegsCollider[1]=m_pLegsFrame = 0;
	m_iboneLeg[0]=m_iboneLeg[1] = 0;
	m_bLegActive[0]=m_bLegActive[1] = 0;
	m_items_to_equip_pack1.clear();
	m_items_to_equip_pack2.clear();
	m_spells_ids_for_serializing.clear();
	m_actor_items_to_imd_equip.clear();
	m_to_target_quaternion.CreateIdentity();
	ClearActorSB(true);
	m_complex_attachments_to_attach.clear();
	m_complex_attachments_to_attach.resize(0);
	//----create sa
	m_pActorSpellsActions = new CActorSpellsActions;
	//[AlexMcC:22.03.10] CActors aren't owned by shared_ptrs, so we use boost's "weak without shared" technique:
	// http://www.boost.org/doc/libs/1_42_0/libs/smart_ptr/sp_techniques.html#weak_without_shared
	m_pThis.reset(this, DoNotDeleteThisPointer);

	m_netPhysCounter = 0;
	memset(m_boneTrans, 0, sizeof(m_boneTrans));

#ifndef DEMO_BUILD_RPG_SS
	m_tryToChangeStanceCounter = 0;
#endif
	//CryLogAlways("CActor::CActor() - constructor end call");
}

//------------------------------------------------------------------------
CActor::~CActor()
{
	CRY_ASSERT(g_pGame);
	PREFAST_ASSUME(g_pGame);

	GetGameObject()->SetMovementController(NULL);
	SAFE_RELEASE(m_pMovementController);

	if (m_pInventory)
	{
		if (IItem* item = GetCurrentItem())
		{
			if (item->IsUsed())
				item->StopUse(GetEntityId());
		}

		if (gEnv->bServer)
			m_pInventory->Destroy();

		GetGameObject()->ReleaseExtension("Inventory");
	}
	//----delete sa
	if (m_pActorSpellsActions)
	{
		m_actor_spells_book.clear();
		m_actor_spells_book.resize(0);
		delete m_pActorSpellsActions;
	}

	if (!m_sLipSyncExtensionType.empty())
	{
		GetGameObject()->ReleaseExtension(m_sLipSyncExtensionType.c_str());
	}

	if (m_pAnimatedCharacter)
	{
		GetGameObject()->ReleaseExtension("AnimatedCharacter");
		GetGameObject()->DeactivateExtension("AnimatedCharacter");
	}
	GetGameObject()->ReleaseView( this );
	GetGameObject()->ReleaseProfileManager( this );

	if(g_pGame && g_pGame->GetIGameFramework() && g_pGame->GetIGameFramework()->GetIActorSystem())
		g_pGame->GetIGameFramework()->GetIActorSystem()->RemoveActor( GetEntityId() );

	ICVar* pEnableAI = gEnv->pConsole->GetCVar("sv_AISystem");
	if(!gEnv->bMultiplayer || (pEnableAI && pEnableAI->GetIVal()))
	{
		g_pGame->GetGameAISystem()->LeaveAllModules(GetEntityId());
	}

	UnRegisterInAutoAimManager();
	UnRegisterDBAGroups();
	ReleaseLegsColliders();
	CActorManager::GetActorManager()->ActorRemoved(this);
}

//------------------------------------------------------------------------
void CActor::PhysicalizeBodyDamage()
{
	CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
	CRY_ASSERT(pBodyDamageManager);

	const TBodyDamageProfileId bodyDamageProfileId = GetCurrentBodyDamageProfileId();
	if ((bodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID) && GetEntity()->GetCharacter(0))
	{
		pBodyDamageManager->PhysicalizePlayer(bodyDamageProfileId, *GetEntity());
	}
}

//------------------------------------------------------------------------
bool CActor::Init( IGameObject * pGameObject )
{
	//CryLogAlways("CActor::Init call");
	SetGameObject(pGameObject);

	if (!GetGameObject()->CaptureView(this))
		return false;
	if (!GetGameObject()->CaptureProfileManager(this))
		return false;

	m_isClient = (g_pGame->GetClientActorId() == GetEntityId());

	IEntity *pEntity = GetEntity();
	IEntityClass *pEntityClass = pEntity->GetClass();

	for (int i = 0; i < 65535; i++)
	{
		perks[i] = false;
		buffs[i] = false;
		buffs_time[i] = 0;
		perks_level[i] = 0;
	}

	for (int i = 0; i < 100; i++)
	{
		spec_action_timers_out_combo[i] = 0.0f;
		spec_action_time_to_recharge_combo[i] = 0.0f;
		spec_action_counter_in_combo[i] = 0;
		spec_action_counter_max_in_combo[i] = 0;
	}

	for (int i = 0; i < MAX_DAMAGE_RESIST_TYPES; i++)
	{
		damage_resists[i] = 0.0f;
	}

	for (int i = 0; i < MAX_QUICK_EQUIPMENT_SLOTS; i++)
	{
		AcQuickSlot slot_q;
		slot_q.obj_in_slot_id = 0;
		slot_q.slot_id = 0;
		slot_q.slot_type = 0;
		m_quick_eq_slots[i] = slot_q;
	}

	for (int i = 0; i < 15; i++)
	{
		m_special_recoil_action[i] = 0.0f;
	}

	if (m_pActorSpellsActions)
	{
		m_pActorSpellsActions->SetUser(this);
	}

	m_isPlayer = (pEntityClass == gEnv->pEntitySystem->GetClassRegistry()->FindClass("Player"));

	g_pGame->GetGameCache().CacheActorClass(pEntityClass, pEntity->GetScriptTable());

	m_pMovementController = CreateMovementController();
	GetGameObject()->SetMovementController(m_pMovementController);

	g_pGame->GetIGameFramework()->GetIActorSystem()->AddActor( GetEntityId(), this );

	g_pGame->GetActorScriptBind()->AttachTo(this);
	m_pAnimatedCharacter = static_cast<IAnimatedCharacter*>(pGameObject->AcquireExtension("AnimatedCharacter"));
	if (m_pAnimatedCharacter)
	{
		ResetAnimationState();
	}

	m_pInventory = static_cast<IInventory*>(pGameObject->AcquireExtension("Inventory"));

	if (!m_pGameFramework)
	{
		m_pGameFramework = g_pGame->GetIGameFramework();
		m_pGameplayRecorder = g_pGame->GetIGameFramework()->GetIGameplayRecorder();
		m_pItemSystem = m_pGameFramework->GetIItemSystem();
	}

	if (!GetGameObject()->BindToNetwork())
		return false;

	uint32 uNewFlags = (ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO);
	if(GetChannelId() == 0 && pEntityClass != gEnv->pEntitySystem->GetClassRegistry()->FindClass("DummyPlayer"))
	{
		uNewFlags |= ENTITY_FLAG_TRIGGER_AREAS;
	}

	pEntity->SetFlags(pEntity->GetFlags() | uNewFlags);

	m_damageEffectController.Init(this);

	g_pGame->GetTacticalManager()->AddEntity(GetEntityId(), CTacticalManager::eTacticalEntity_Unit);

	s_animationEventsTable.Init();
	m_telemetry.SetOwner(this);

	// Need to setup multiplayer lua script before reviving the player
	// Note: This is done before we cache any data, so the Lua cache will contain the properties we override here
	if (gEnv->bMultiplayer)
	{
		IScriptTable *pEntityScript = pEntity->GetScriptTable();
		if (pEntityScript)
		{
			HSCRIPTFUNCTION setIsMultiplayerFunc(NULL);
			if (pEntityScript->GetValue("SetIsMultiplayer", setIsMultiplayerFunc))
			{
				Script::Call(gEnv->pScriptSystem, setIsMultiplayerFunc, pEntityScript);
				gEnv->pScriptSystem->ReleaseFunc(setIsMultiplayerFunc);
			}
		}
	}

	g_pGame->GetGameCache().CacheActorInstance(pEntity->GetId(), pEntity->GetScriptTable());

	PrepareLuaCache();

	GenerateBlendRagdollTags();

	for (int i = 0; i < eAESlots_Number; i++)
	{
		equipped_cngtie[i] = GetCurrentEquippedItemId(i);
		equipped_cngtie_names[i] = "";
		m_wear_acc[i] = 0;
	}

	return true;
}

void CActor::Release()
{
	delete this;
}

//----------------------------------------------------------------------
void CActor::PostInit( IGameObject * pGameObject )
{
	GetGameObject()->EnablePrePhysicsUpdate( gEnv->bMultiplayer ? ePPU_Always : ePPU_WhenAIActivated );

	pGameObject->EnableUpdateSlot( this, 0 );	
	pGameObject->EnablePostUpdates( this );

	if (m_teamId)
	{
		CGameRules *pGameRules = g_pGame->GetGameRules();
		pGameRules->ClDoSetTeam(m_teamId, GetEntityId());
	}

	if (additional_pack_add_request)
	{
		AddAdditionalEquip();
	}

	if (additional_pack2_add_request)
	{
		AddAdditionalEquip2();
	}
	RequestForDelayedPhysicaliseActor();
}

//------------------------------------------------------------------------
bool CActor::ReloadExtension( IGameObject *pGameObject, const SEntitySpawnParams &params )
{
	CRY_ASSERT(GetGameObject() == pGameObject);

	ResetGameObject();

	if (!GetGameObject()->CaptureView(this))
		return false;
	if (!GetGameObject()->CaptureProfileManager(this))
		return false;

	if (!GetGameObject()->BindToNetwork())
		return false;

	//--- If we are in a vehicle then we should exit it as we've been torn down
	IVehicle *pVehicle = GetLinkedVehicle();
	if (pVehicle)
	{
		IVehicleSeat *pVehicleSeat = pVehicle->GetSeatForPassenger(params.prevId);
		if (pVehicleSeat)
		{
			pVehicleSeat->Exit(false, true);
		}
	}
	CRY_ASSERT(GetLinkedVehicle() == NULL);

	g_pGame->GetIGameFramework()->GetIActorSystem()->RemoveActor(params.prevId);
	g_pGame->GetIGameFramework()->GetIActorSystem()->AddActor(GetEntityId(), this);

	g_pGame->GetTacticalManager()->RemoveEntity(params.prevId, CTacticalManager::eTacticalEntity_Unit);
	g_pGame->GetTacticalManager()->AddEntity(GetEntityId(), CTacticalManager::eTacticalEntity_Unit);

	ICVar* pEnableAI = gEnv->pConsole->GetCVar("sv_AISystem");
	if(!gEnv->bMultiplayer || (pEnableAI && pEnableAI->GetIVal()))
	{
		g_pGame->GetGameAISystem()->LeaveAllModules(params.prevId);
	}

	InvalidateCurrentModelName();
	SetAspectProfile(eEA_Physics, eAP_NotPhysicalized);

	PrepareLuaCache();

	return true;
}

//----------------------------------------------------------------------
void CActor::PostReloadExtension( IGameObject *pGameObject, const SEntitySpawnParams &params )
{
	CRY_ASSERT(GetGameObject() == pGameObject);

	pGameObject->SetMovementController(m_pMovementController);
	m_pMovementController->Reset();

	g_pGame->GetActorScriptBind()->AttachTo(this);

	ResetAnimationState();

	GetGameObject()->EnablePrePhysicsUpdate( gEnv->bMultiplayer ? ePPU_Always : ePPU_WhenAIActivated );

	GetEntity()->SetFlags(GetEntity()->GetFlags() |
		(ENTITY_FLAG_CUSTOM_VIEWDIST_RATIO | ENTITY_FLAG_TRIGGER_AREAS));

	if (m_registeredInAutoAimMng)
	{
		g_pGame->GetAutoAimManager().UnregisterAutoaimTarget(params.prevId);
		m_registeredInAutoAimMng = false;
	}
	if (m_registeredAnimationDBAs)
	{
		g_pGame->GetGameCache().RemoveDBAUser(params.prevId);
		m_registeredAnimationDBAs = false;
	}

	RegisterInAutoAimManager();
	RegisterDBAGroups();
}

//----------------------------------------------------------------------
void CActor::RebindScript()
{
	IEntity* pEntity = GetEntity();
	IEntityScriptProxy* pScript = static_cast<IEntityScriptProxy*>( pEntity->GetProxy( ENTITY_PROXY_SCRIPT ) );

	g_pGame->GetActorScriptBind()->AttachTo(this);

	SEntitySpawnParams params;
	params.prevId = GetEntityId();
	pScript->Init( pEntity, params );

	CGameCache &gameCache = g_pGame->GetGameCache();
	gameCache.RefreshActorInstance(GetEntityId(), GetEntity()->GetScriptTable());

	PrepareLuaCache();
}

//----------------------------------------------------------------------
bool CActor::GetEntityPoolSignature( TSerialize signature )
{
	signature.BeginGroup("Actor");
	signature.EndGroup();
	return true;
}

//----------------------------------------------------------------------
void CActor::PrepareLuaCache()
{
	const CGameCache &gameCache = g_pGame->GetGameCache();
	IEntityClass *pClass = GetEntity()->GetClass();

	m_LuaCache_PhysicsParams.reset(gameCache.GetActorPhysicsParams(pClass));
	m_LuaCache_GameParams.reset(gameCache.GetActorGameParams(pClass));
	m_LuaCache_Properties.reset(gameCache.GetActorProperties(GetEntityId()));
}

//----------------------------------------------------------------------
void CActor::HideAllAttachments(bool isHiding)
{

	if (IItem *pCurrentItem = GetCurrentItem())
	{
		CItem *pItem = static_cast<CItem*>(pCurrentItem);
		const bool doHide = !pItem->IsMounted();
		if(doHide)
			pCurrentItem->GetEntity()->Hide(isHiding);
	}

	//This is only for AI, in MP players don't have back attachments
	if (!IsPlayer())
	{
		int totalItems = m_pInventory->GetCount();
		for (int i=0; i<totalItems; i++)
		{
			EntityId entId = m_pInventory->GetItem(i);
			CItem *item = (CItem *)m_pItemSystem->GetItem(entId);
			if (item && item->IsAttachedToBack())
			{
				item->Hide(isHiding);
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::InitClient(int channelId)
{
	if (m_health.IsDead() && !GetSpectatorMode())
		GetGameObject()->InvokeRMI(ClSimpleKill(), NoParams(), eRMI_ToClientChannel, channelId);
}

//------------------------------------------------------------------------
void CActor::Revive( EReasonForRevive reasonForRevive )
{
	if (reasonForRevive == kRFR_FromInit)
		g_pGame->GetGameRules()->OnRevive(this);

	if(gEnv->bServer)
		m_damageEffectController.OnRevive();

	for (uint32 i=0; i<BONE_ID_NUM; i++)
	{
		m_boneTrans[i].SetIdentity();
		m_boneIDs[i] = -1;
	}

	CancelScheduledSwitch();

	if(IsClient())
	{
		// Stop force feedback
		IForceFeedbackSystem* pForceFeedbackSystem = gEnv->pGame->GetIGameFramework()->GetIForceFeedbackSystem();
		if(pForceFeedbackSystem)
		{
			pForceFeedbackSystem->StopAllEffects();
		}
	}
	else
	{
		g_pGame->GetBodyDamageManager()->ResetInstance(*GetEntity(), m_bodyDestructionInstance);
	}

	//Reset animated character before setting the model (could break shadow char setup)
	if (m_pAnimatedCharacter)
		m_pAnimatedCharacter->ResetState();

	bool hasChangedModel = false;
	if (m_chrmodelstr.empty() || !gEnv->bMultiplayer)
		hasChangedModel = SetActorModel(); // set the model before physicalizing
	else
	{
		//hasChangedModel = SetActorModel(m_chrmodelstr, true);
		//if(!IsPlayer())
		//hasChangedModel = SetActorModel();

		if (gEnv->bMultiplayer)
		{
			/*Net_CheckActorBodyAndAllAttachments();
			if(IsPlayer())
				Net_CheckActorBodyAndAllAttachments(5);*/

			/*if (gEnv->bMultiplayer)
			{
				if (gEnv->bServer)
					GetGameObject()->InvokeRMI(CActor::ClAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToRemoteClients);
				else
				{
					GetGameObject()->InvokeRMI(CActor::SvAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToServer);
				}
			}*/
			hasChangedModel = SetActorModel();
		}
		else
		{
			hasChangedModel = SetActorModel(m_chrmodelstr, true);
		}
	}

	if(!gEnv->bMultiplayer || hasChangedModel)
	{
		ReadDataFromXML();
	}

	m_stance = STANCE_NULL;
	m_desiredStance = STANCE_NULL;

	if(reasonForRevive != kRFR_StartSpectating)
	{
		//if(gEnv->bMultiplayer)
		//{
			Physicalize();
		//}

		if (gEnv->bServer)
		{
			GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Alive);
		}
	}

	// Set the actor game parameters after model is loaded and physicalized
	InitGameParams();

  if (IPhysicalEntity* pPhysics = GetEntity()->GetPhysics())
  {
    pe_action_move actionMove;    
    actionMove.dir.zero();
    actionMove.iJump = 1;

		pe_action_set_velocity actionVel;
		actionVel.v.zero();
		actionVel.w.zero();
    
    pPhysics->Action(&actionMove);
		pPhysics->Action(&actionVel);
  }

	if (m_pMovementController)
		m_pMovementController->Reset();

	m_linkStats = SLinkStats();

	if (ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0))
		pCharacter->EnableProceduralFacialAnimation(GetMaxHealth() > 0);

	if(!IsPlayer())
	{
		const char* const szTeamName = g_pGameCVars->sv_aiTeamName->GetString();
		if(szTeamName && (*szTeamName) != 0)
		{
			IGameRulesSystem *pIGameRulesSystem = g_pGame->GetIGameFramework()->GetIGameRulesSystem();
			CRY_ASSERT(pIGameRulesSystem);

			if (CGameRules *pGameRules=static_cast<CGameRules*>(pIGameRulesSystem->GetCurrentGameRules()))
			{
				int teamId = pGameRules->GetTeamId(szTeamName);
				if(!teamId)
				{
					teamId = pGameRules->CreateTeam(szTeamName);
					CRY_ASSERT(teamId);
				}

				// Team assignment is propagated from server to client,
				// so we should only set a unit's team on the server
				if(gEnv->bServer)
					pGameRules->SetTeam(teamId, GetEntityId());
			}
		}
	}

	//Only from scripts, when AI has already set its properties
	const bool registerForAutoAimDuringRevival = (reasonForRevive == CActor::kRFR_ScriptBind) || IsPlayer();
	if (registerForAutoAimDuringRevival)
	{
		RegisterInAutoAimManager();
		RegisterDBAGroups();
	}

	if(reasonForRevive == kRFR_StartSpectating)
	{
		if (ICharacterInstance *pCharacter=GetEntity()->GetCharacter(0))
				pCharacter->GetISkeletonPose()->DestroyCharacterPhysics(1);
		m_pAnimatedCharacter->ForceRefreshPhysicalColliderMode();
		m_pAnimatedCharacter->RequestPhysicalColliderMode( eColliderMode_Spectator, eColliderModeLayer_Game, "Actor::Revive");
	}

	if (reasonForRevive == kRFR_FromInit)
	{
		CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
		CRY_ASSERT(pBodyDamageManager);
		m_DefaultBodyDamageProfileId = pBodyDamageManager->GetBodyDamage(*GetEntity());

		pBodyDamageManager->GetBodyDestructibility(*GetEntity(), m_bodyDestructionInstance);
	}

	UpdateAutoDisablePhys(false);

	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	IFacialInstance* pFacialInstance = (pCharacter) ? pCharacter->GetFacialInstance() : 0;
	if (pFacialInstance)
	{
		pFacialInstance->StopAllSequencesAndChannels();
	}

	if(reasonForRevive == kRFR_Spawn)
	{
		// ensure the AI part gets enabled in case this is a re-spawn
		// (this is to make sure that Players on a dedicated server expose themselves to the AISystem again once they respawn)
		if(IAIObject *pAI = GetEntity()->GetAI())
		{
			pAI->Event(AIEVENT_ENABLE, NULL);
		}
	}
}

void CActor::UpdateAutoDisablePhys(bool bRagdoll)
{
	EAutoDisablePhysicsMode adpm = eADPM_Never;
	if (bRagdoll)
		adpm = eADPM_Never;
	else if (gEnv->bMultiplayer)
		adpm = eADPM_Never;
	else if (IsClient())
		adpm = eADPM_Never;
	else
		adpm = eADPM_WhenAIDeactivated;

	GetGameObject()->SetAutoDisablePhysicsMode(adpm);
}

//------------------------------------------------------------------------
bool CActor::LoadPhysicsParams(SmartScriptTable pEntityTable, const char* szEntityClassName, SEntityPhysicalizeParams &outPhysicsParams, 
							   pe_player_dimensions &outPlayerDim, pe_player_dynamics &outPlayerDyn)
{
	assert((bool)pEntityTable);

	bool bResult = false;

	if (pEntityTable)
	{
		SmartScriptTable physicsParams;
		if (pEntityTable->GetValue("physicsParams", physicsParams))
		{
			CScriptSetGetChain physicsTableChain(physicsParams);

			outPhysicsParams.nSlot = 0;
			outPhysicsParams.type = PE_LIVING;

			//Separate player's mass from AI, for testing
			string massParamName = string("mass") + "_" + string(szEntityClassName);
			if (!physicsTableChain.GetValue(massParamName.c_str(), outPhysicsParams.mass))
				physicsTableChain.GetValue("mass", outPhysicsParams.mass);
			physicsTableChain.GetValue("density", outPhysicsParams.density);
			physicsTableChain.GetValue("flags", outPhysicsParams.nFlagsOR);
			physicsTableChain.GetValue("partid", outPhysicsParams.nAttachToPart);
			physicsTableChain.GetValue("stiffness_scale", outPhysicsParams.fStiffnessScale);

			SmartScriptTable livingTab;
			if (physicsTableChain.GetValue("Living", livingTab))
			{
				CScriptSetGetChain livingTableChain(livingTab);

				// Player Dimensions
				livingTableChain.GetValue("height", outPlayerDim.heightCollider);
				livingTableChain.GetValue("size", outPlayerDim.sizeCollider);
				//livingTableChain.GetValue("height_eye", outPlayerDim.heightEye);
				livingTableChain.GetValue("height_pivot", outPlayerDim.heightPivot);
				livingTableChain.GetValue("use_capsule", outPlayerDim.bUseCapsule);

				// As part of a previous fix, these should be kept at 0
				//	See changelist #144179
				//	"!B 34147 -fixed sometimes players clipping through geometry while unfreezing"
				outPlayerDim.headRadius = 0.0f;
				outPlayerDim.heightEye = 0.0f;

				// Player Dynamics.
				livingTableChain.GetValue("inertia", outPlayerDyn.kInertia);
				livingTableChain.GetValue("k_air_control", outPlayerDyn.kAirControl);
				livingTableChain.GetValue("inertiaAccel", outPlayerDyn.kInertiaAccel);
				livingTableChain.GetValue("air_resistance", outPlayerDyn.kAirResistance);
				livingTableChain.GetValue("gravity", outPlayerDyn.gravity.z);

				//Separate player's mass from AI, for testing
				if (!physicsTableChain.GetValue(massParamName.c_str(), outPlayerDyn.mass))
					physicsTableChain.GetValue("mass",outPlayerDyn.mass);

				livingTableChain.GetValue("min_slide_angle", outPlayerDyn.minSlideAngle);
				livingTableChain.GetValue("max_climb_angle", outPlayerDyn.maxClimbAngle);
				livingTableChain.GetValue("max_jump_angle", outPlayerDyn.maxJumpAngle);
				livingTableChain.GetValue("min_fall_angle", outPlayerDyn.minFallAngle);
				livingTableChain.GetValue("max_vel_ground", outPlayerDyn.maxVelGround);
				livingTableChain.GetValue("timeImpulseRecover", outPlayerDyn.timeImpulseRecover);

				const char *colliderMat = 0;
				if (livingTableChain.GetValue("colliderMat", colliderMat) && colliderMat && colliderMat[0])
				{
					if (ISurfaceType *pSurfaceType=gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeByName(colliderMat))
						outPlayerDyn.surface_idx = pSurfaceType->GetId();
				}
			}

			bResult = true;
		}
	}

	return bResult;
}

//------------------------------------------------------------------------
void CActor::Physicalize(EStance stance)
{
	IEntity *pEntity = GetEntity();

	bool bHidden = pEntity->IsHidden();
	if (bHidden)
		pEntity->Hide(false);

	bool bHasPhysicsParams = true;
	SEntityPhysicalizeParams pp;

	// Physics params structs contain floats with invalid data (marked as "unused" see MARK_UNUSED macro on physinterface.h) 
	// by design that will generate a FPE when used in floating-point operations (like assignments).
	// Default copy-constructing the physic params here generates a bitwise copy of the struct, which avoid any FP-operation 
	// and FPE. 
	pe_player_dimensions playerDim;
	pe_player_dynamics playerDyn; 
	if (m_LuaCache_PhysicsParams)
	{
		memcpy(&playerDim, &m_LuaCache_PhysicsParams->playerDim, sizeof(playerDim));
		memcpy(&playerDyn, &m_LuaCache_PhysicsParams->playerDyn, sizeof(playerDyn));
		pp = m_LuaCache_PhysicsParams->params;
	}
	else
	{
		IEntityClass *pClass = pEntity->GetClass();

		// Run-time loading
		if (CGameCache::IsLuaCacheEnabled())
		{
			GameWarning("[Game Cache] Warning: Loading physics params for entity class \'%s\' at run-time!", pClass->GetName());
		}

		bHasPhysicsParams = LoadPhysicsParams(pEntity->GetScriptTable(), pClass->GetName(), pp, playerDim, playerDyn);
	}

	if (bHasPhysicsParams)
	{
		if (base_mass_val > 0.0f && inventory_mass_val > 0.0f)
		{
			playerDyn.mass = playerDyn.mass + inventory_mass_val;
			pp.mass = pp.mass + inventory_mass_val;
		}

		pp.pPlayerDimensions = &playerDim;
		pp.pPlayerDynamics = &playerDyn;

		// Enable the post step callback in physics.
		if( m_pAnimatedCharacter )
		{
			pp.nFlagsOR = pef_log_poststep;
		}

		// Multiply mass
		if (m_LuaCache_Properties)
		{
			pp.mass *= m_LuaCache_Properties->fPhysicMassMult;
		}

		// Player Dimensions
		if (STANCE_NULL != stance)
		{
			const SStanceInfo *sInfo = GetStanceInfo(stance);
			playerDim.heightCollider = sInfo->heightCollider;
			playerDim.sizeCollider = sInfo->size;
			playerDim.heightPivot = sInfo->heightPivot;
			playerDim.maxUnproj = max(0.0f,sInfo->heightPivot);
			playerDim.bUseCapsule = sInfo->useCapsule;
		}

		if(!is_unused(playerDyn.timeImpulseRecover))
			m_timeImpulseRecover = playerDyn.timeImpulseRecover;
		else
			m_timeImpulseRecover = 0.0f;

		if(!is_unused(playerDyn.kAirResistance))
			m_airResistance = playerDyn.kAirResistance;
		else
			m_airResistance = 0.0f;

		if(!is_unused(playerDyn.kAirControl))
			m_airControl = playerDyn.kAirControl;
		else
			m_airControl = 1.0f;

		if(!is_unused(playerDyn.kInertia))
			m_inertia = playerDyn.kInertia;
		else
			m_inertia = 10.0f; //Same value as scripts default

		if(!is_unused(playerDyn.kInertiaAccel))
			m_inertiaAccel = playerDyn.kInertiaAccel;
		else
			m_inertiaAccel = 11.0f; //Same value as scripts default

		if (pEntity->GetPhysics())
		{
			Ang3 rot(pEntity->GetWorldAngles());
			pEntity->SetRotation(Quat::CreateRotationZ(rot.z));

			SEntityPhysicalizeParams nop;
			nop.type = PE_NONE;
			pEntity->Physicalize(nop);
		}

		pEntity->Physicalize(pp);
	}
	else
	{
		GameWarning("Failed to physicalize actor \'%s\'", pEntity->GetName());
	}

	PhysicalizeLocalPlayerAdditionalParts();

#if ENABLE_GAME_CODE_COVERAGE || ENABLE_SHARED_CODE_COVERAGE
	if (!IsPlayer())
	{
		CCCPOINT(Actor_PhysicalizeNPC);
	}
	else if (gEnv->pGame->GetIGameFramework()->GetClientActor() == NULL)
	{
		CCCPOINT(Actor_PhysicalizePlayerWhileNoClient);
	}
	else if (gEnv->pGame->GetIGameFramework()->GetClientActor() == this)
	{
		CCCPOINT(Actor_PhysicalizeLocalPlayer);
	}
	else
	{
		CCCPOINT(Actor_PhysicalizeOtherPlayer);
	}
#endif

	//the finish physicalization
	PostPhysicalize();

	CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
	CRY_ASSERT(pBodyDamageManager);
	m_DefaultBodyDamageProfileId = pBodyDamageManager->GetBodyDamage(*GetEntity());

	PhysicalizeBodyDamage();

	pBodyDamageManager->GetBodyDestructibility(*GetEntity(), m_bodyDestructionInstance);

	if (bHidden)
		pEntity->Hide(true);
}

//------------------------------------------------------------------------
bool CActor::LoadFileModelInfo(SmartScriptTable pEntityTable, SmartScriptTable pProperties, SActorFileModelInfo &outFileModelInfo)
{
	assert((bool)pEntityTable);
	assert((bool)pProperties);

	bool bResult = false;

	if (pProperties)
	{
		CScriptSetGetChain propertiesTableChain(pProperties);

		const char* szTemp = 0;
		if (propertiesTableChain.GetValue("fileModel", szTemp))
			outFileModelInfo.sFileName = szTemp;
		if (propertiesTableChain.GetValue("clientFileModel", szTemp))
			outFileModelInfo.sClientFileName = szTemp;
		if (propertiesTableChain.GetValue("shadowFileModel", szTemp))
			outFileModelInfo.sShadowFileName = szTemp;

		propertiesTableChain.GetValue("nModelVariations", outFileModelInfo.nModelVariations);
		propertiesTableChain.GetValue("bUseFacialFrameRateLimiting", outFileModelInfo.bUseFacialFrameRateLimiting);

		// Load IK limbs
		SmartScriptTable pIKLimbs;
		if (pEntityTable && pEntityTable->GetValue("IKLimbs", pIKLimbs))
		{
			const int count = pIKLimbs->Count();
			outFileModelInfo.IKLimbInfo.clear();
			outFileModelInfo.IKLimbInfo.reserve(count);
			for (int limbIndex = 1; limbIndex <= count; ++limbIndex)
			{
				SmartScriptTable pIKLimb;
				if (pIKLimbs->GetAt(limbIndex, pIKLimb))
				{
					SActorIKLimbInfo limbInfo;

					pIKLimb->GetAt(1, limbInfo.characterSlot);
					if (pIKLimb->GetAt(2, szTemp))
						limbInfo.sLimbName = szTemp;
					if (pIKLimb->GetAt(3, szTemp))
						limbInfo.sRootBone = szTemp;
					if (pIKLimb->GetAt(4, szTemp))
						limbInfo.sMidBone = szTemp;
					if (pIKLimb->GetAt(5, szTemp))
						limbInfo.sEndBone = szTemp;
					pIKLimb->GetAt(6, limbInfo.flags);

					outFileModelInfo.IKLimbInfo.push_back(limbInfo);
				}
			}
		}

		bResult = true;
	}

	return bResult;
}

//------------------------------------------------------------------------
bool CActor::SetActorModel(const char* modelName, bool not_from_lua)
{
	if (gEnv->bMultiplayer)
	{
		/*string old_file = PathUtil::GetFileName(m_currModel);
		string new_file = PathUtil::GetFileName(modelName);
		string start_file = "player_invisible";
		if (!m_currModel.empty() && old_file != start_file && new_file == start_file)
		{
			modelName = m_currModel;
		}*/
	}
	bool hasChanged = false;
	if (g_pGameCVars->g_setActorModelFromLua == 0 || not_from_lua)
	{
		hasChanged = SetActorModelInternal(modelName);
	}
	else
	{
		hasChanged = SetActorModelFromScript();
	}

	IAnimatedCharacter *pAnimatedCharacter = GetAnimatedCharacter();
	if (pAnimatedCharacter)
	{
		pAnimatedCharacter->UpdateCharacterPtrs();
	}

	return hasChanged;
}

bool CActor::Net_SetActorModel(const char * modelName, bool not_from_lua)
{
	//if (!IsPlayer())
	//	return false;

	//if (!IsRemote())
	//	return false;

	//return false;

	bool hasChanged = false;
	if (g_pGameCVars->g_setActorModelFromLua == 0 || not_from_lua)
	{
		hasChanged = SetActorModelInternal(modelName);
	}
	else
	{
		hasChanged = SetActorModelFromScript();
	}

	IAnimatedCharacter *pAnimatedCharacter = GetAnimatedCharacter();
	if (pAnimatedCharacter)
	{
		pAnimatedCharacter->UpdateCharacterPtrs();
	}

	CPlayer *pPl = (CPlayer *)this;
	if (pPl)
	{
		pPl->Physicalize();
		pPl->PostPhysicalize();
	}

	return hasChanged;
}

void CActor::Net_UpdateCharacterCustomozationInfo(int gender, int head, int hair, int eyes, string name)
{
	if (!IsPlayer())
		return;

	//if (!IsRemote())
	//	return;

	CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
	if (!p_PlCrSys)
		return;

	SetGender(gender);
	p_PlCrSys->SetEyes(eyes);
	p_PlCrSys->SetFace(head);
	p_PlCrSys->SetHair(hair);
	CPlayer *pPl = (CPlayer *)this;
	if(pPl && IsPlayer())
		pPl->m_player_name = name;
}

//------------------------------------------------------------------------
void CActor::UpdateActorModel()
{
	// Update cache to reflect the current state of the Lua properties
	if (m_LuaCache_Properties)
	{
		IScriptTable *pEntityTable = GetEntity()->GetScriptTable();
		SmartScriptTable pProperties;
		if (pEntityTable && pEntityTable->GetValue("Properties", pProperties))
		{
			LoadFileModelInfo(pEntityTable, pProperties, m_LuaCache_Properties->fileModelInfo);
		}
	}
}

//------------------------------------------------------------------------
bool CActor::FullyUpdateActorModel()
{
	UpdateActorModel();

	const bool hasChangedModel = SetActorModel();
	if (hasChangedModel)
	{
		ReadDataFromXML();

		// Re-physicalize the actor
		Physicalize();

		CItem *pItem = static_cast<CItem*>(GetCurrentItem());
		if (pItem && pItem->IsAttachedToHand())
		{
			pItem->AttachToHand(false);		// Need to remove first otherwise the following attach will be ignored
			pItem->AttachToHand(true);
		}

		return true;
	}
	return false;
}

void CActor::UpdateActorModelFTO()
{
	ReadDataFromXML();

	// Re-physicalize the actor
	Physicalize();

	CItem *pItem = static_cast<CItem*>(GetCurrentItem());
	if (pItem && pItem->IsAttachedToHand())
	{
		pItem->AttachToHand(false);		// Need to remove first otherwise the following attach will be ignored
		pItem->AttachToHand(true);
	}
}


// Use this function, for example, when the entity slot is somehow reset.
// By invalidating the current model, it will be properly reloaded from
// a save-game.
void CActor::InvalidateCurrentModelName()
{
	m_currModel.clear();
	m_currShadowModel.clear();
}

void CActor::OnItemUPD(int state)
{
	if (state > 0)
	{
		if (IsPlayer())
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				CPlayer *pPl = (CPlayer *)this;
				if (pPl)
				{
					if (!pPl->IsThirdPerson())
					{
						nwAction->DelayedUpdatePlShadowChr();
					}
				}
			}
		}
	}
}


//------------------------------------------------------------------------
bool CActor::SetActorModelFromScript()
{
	bool hasChangedModel = false;

	if (IScriptTable* pScriptTable = GetEntity()->GetScriptTable())
	{
		HSCRIPTFUNCTION pfnSetActorModel = 0;
		if (pScriptTable->GetValue("SetActorModel", pfnSetActorModel))
		{
			Script::CallReturn(gEnv->pScriptSystem, pfnSetActorModel, pScriptTable, IsClient(), hasChangedModel);
			gEnv->pScriptSystem->ReleaseFunc(pfnSetActorModel);
		}
	}

	return hasChangedModel;
}

//------------------------------------------------------------------------
bool CActor::SetActorModelInternal(const char* modelName)
{
	bool hasChangedModel = false;

	const bool bIsClient = IsClient();

	if (m_LuaCache_Properties && !modelName)
	{
		hasChangedModel = SetActorModelInternal(m_LuaCache_Properties->fileModelInfo);
	}
	else
	{
		IEntity *pEntity = GetEntity();
		IEntityClass *pClass = pEntity->GetClass();

		// Run-time loading
		if (CGameCache::IsLuaCacheEnabled())
		{
			GameWarning("[Game Cache] Warning: Loading file model info for entity class \'%s\' at run-time!", pClass->GetName());
		}

		SActorFileModelInfo fileModelInfo;
		IScriptTable *pEntityTable = pEntity->GetScriptTable();
		SmartScriptTable pProperties;
		if (pEntityTable && pEntityTable->GetValue("Properties", pProperties) &&
			LoadFileModelInfo(pEntityTable, pProperties, fileModelInfo))
		{
			if(modelName)
			{
				fileModelInfo.sFileName = modelName;
				fileModelInfo.sClientFileName = modelName;
				fileModelInfo.sShadowFileName = modelName;
			}
			hasChangedModel = SetActorModelInternal(fileModelInfo);
		}
		else
		{
			GameWarning("Failed to load file model info for actor \'%s\'", pEntity->GetName());
		}
	}

	return hasChangedModel;
}

//------------------------------------------------------------------------
bool CActor::SetActorModelInternal(const SActorFileModelInfo &fileModelInfo)
{
	bool hasChangedModel = false;

	IEntity *pEntity = GetEntity();
	const bool bIsClient = IsClient();

	// Get correct file for model
	const string& sFileModel = (bIsClient ? fileModelInfo.sClientFileName : fileModelInfo.sFileName);

	CGameCache::TCachedModelName modelVariationFileName;
	CGameCache::GenerateModelVariation(sFileModel, modelVariationFileName, GetEntity()->GetScriptTable(), fileModelInfo.nModelVariations, -1);

	if (modelVariationFileName.empty())
	{
		// Just create a render proxy for it
		pEntity->CreateProxy(ENTITY_PROXY_RENDER);
	}
	else if (strcmpi(m_currModel.c_str(), modelVariationFileName.c_str()) != 0)
	{
		hasChangedModel = true;
		m_currModel = modelVariationFileName.c_str();

		bool bShouldHide = (pEntity->GetCharacter(0) != NULL) && ((pEntity->GetSlotFlags(0) & ENTITY_SLOT_RENDER) == 0);
		pEntity->LoadCharacter(0, modelVariationFileName.c_str());
		if (bShouldHide)
		{
			const uint32 flags = (pEntity->GetSlotFlags(0) & ~ENTITY_SLOT_RENDER);
			pEntity->SetSlotFlags(0, flags);
		}

		// Set IK limbs
		SActorFileModelInfo::TIKLimbInfoVec::const_iterator itLimb = fileModelInfo.IKLimbInfo.begin();
		SActorFileModelInfo::TIKLimbInfoVec::const_iterator itLimbEnd = fileModelInfo.IKLimbInfo.end();
		for (; itLimb != itLimbEnd; ++itLimb)
		{
			const SActorIKLimbInfo &limbInfo = *itLimb;
			CreateIKLimb(limbInfo);
		}

		ICharacterInstance *pCharacter = pEntity->GetCharacter(0);
		if (pCharacter)
		{
			// Set the character to not be force updated
			ISkeletonPose *pSkeletonPose = pCharacter->GetISkeletonPose();
			if (pSkeletonPose)
			{
				pSkeletonPose->SetForceSkeletonUpdate(0);
			}

			IFacialInstance *pFacialInstance = pCharacter->GetFacialInstance();
			if (pFacialInstance)
			{
				pFacialInstance->SetUseFrameRateLimiting(fileModelInfo.bUseFacialFrameRateLimiting);
			}

			// Create the attachments
			IAttachmentManager* pAttachmentManager = pCharacter->GetIAttachmentManager();
			GetOrCreateAttachment(pAttachmentManager, "weapon_bone", "right_item_attachment");
			GetOrCreateAttachment(pAttachmentManager, "weapon_bone", "weapon");
			GetOrCreateAttachment(pAttachmentManager, "alt_weapon_bone01", "left_item_attachment");
			GetOrCreateAttachment(pAttachmentManager, "weapon_bone", "laser_attachment");

			// Let script create any extra attachments
			EntityScripts::CallScriptFunction(GetEntity(), GetEntity()->GetScriptTable(), "CreateAttachments");
		}
	}

	if (bIsClient && !fileModelInfo.sShadowFileName.empty() && 0 != m_currShadowModel.compareNoCase(fileModelInfo.sShadowFileName))
	{
		m_currShadowModel = fileModelInfo.sShadowFileName;
		pEntity->LoadCharacter(5, fileModelInfo.sShadowFileName.c_str());
	}

	return hasChangedModel;
}

//------------------------------------------------------------------------
IAttachment* CActor::GetOrCreateAttachment(IAttachmentManager *pAttachmentManager, const char *boneName, const char *attachmentName)
{
	assert(pAttachmentManager);

	IAttachment* pIAttachment = pAttachmentManager->GetInterfaceByName(attachmentName);
	if (!pIAttachment)
	{
		pIAttachment = pAttachmentManager->CreateAttachment(attachmentName, CA_BONE, boneName);
	}

	return pIAttachment;
}

//------------------------------------------------------------------------
void CActor::PostPhysicalize()
{
	//force the physical proxy to be rebuilt
	m_stance = STANCE_NULL;
	EStance stance =  m_desiredStance != STANCE_NULL ? m_desiredStance : m_params.defaultStance;
	SetStance(stance);

	UpdateStance();

	// [*DavidR | 1/Feb/2010]
	// Disable physical impulses created by explosions. We want to have control over impulses on projectiles in game code
	if (ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0))
	{
		if (IPhysicalEntity* pCharacterPhysics = pCharacter->GetISkeletonPose()->GetCharacterPhysics())
		{
			pe_params_part colltype;
			colltype.flagsColliderAND = ~geom_colltype_explosion;
			pCharacterPhysics->SetParams(&colltype);
		}
	}

	//set player lod always
//	if (IsPlayer())
	{
		IEntityRenderProxy *pRenderProxy = static_cast<IEntityRenderProxy *>(GetEntity()->GetProxy(ENTITY_PROXY_RENDER));

		if (pRenderProxy)
		{
			IRenderNode *pRenderNode = pRenderProxy->GetRenderNode();

			if (pRenderNode)
			{
				if (IsPlayer())
					pRenderNode->SetViewDistRatio(g_pGameCVars->g_actorViewDistRatio);
				pRenderNode->SetLodRatio(g_pGameCVars->g_playerLodRatio);  
			}
		}
	}

	//CryLogAlways("CActor::PostPhysicalize: %s, inertia: %f, inertiaAccel: %f", GetEntity()->GetName(), m_inertia, m_inertiaAccel);

	if (m_pAnimatedCharacter)
	{
		SAnimatedCharacterParams params = m_pAnimatedCharacter->GetParams();
		params.timeImpulseRecover = GetTimeImpulseRecover();
		m_pAnimatedCharacter->SetParams(params);

		m_pAnimatedCharacter->ResetInertiaCache();
	}
}

//------------------------------------------------------------------------
void CActor::ShutDown()
{
	m_bodyDestructionInstance.DeleteMikeAttachmentEntity();
}

//------------------------------------------------------------------------
bool CActor::IsFallen() const
{
	return GetActorStats()->isInBlendRagdoll;
}

bool CActor::IsDead() const
{
	return( m_health.IsDead() );
}

void CActor::Fall(Vec3 hitPos)
{
	HitInfo hitInfo;
	hitInfo.pos	= hitPos;
	Fall(hitInfo);
}

void CActor::Fall(const HitInfo& hitInfo)
{
	if(!m_pAnimatedCharacter)
		return;

	if(!m_pAnimatedCharacter->GetActionController())
		return;

	if (!GetActorStats())
		return;

	CRY_ASSERT( m_pAnimatedCharacter->GetActionController() );

	if( GetActorStats()->isInBlendRagdoll )
		return;

	GetActorStats()->isInBlendRagdoll = true;

	m_pAnimatedCharacter->GetActionController()->Queue( *new CAnimActionBlendFromRagdollSleep(PP_HitReaction, *this, hitInfo, m_blendRagdollParams.m_blendInTagState, m_blendRagdollParams.m_blendOutTagState) );
}

//------------------------------------------------------------------------
void CActor::OnFall(const HitInfo& hitInfo)
{
	//we don't want noDeath (tutorial) AIs to loose their weapon, since we don't have pickup animations yet
	bool	dropWeapon(true);
	bool  hasDamageTable = false;

	IAISystem *pAISystem=gEnv->pAISystem;
	if (pAISystem)
	{
		if(GetEntity() && GetEntity()->GetAI())
		{	SmartScriptTable props;
		SmartScriptTable propsDamage;

			IAIActor* pAIActor = CastToIAIActorSafe(GetEntity()->GetAI());
			if(pAIActor)
			{
				IAISignalExtraData *pEData = pAISystem->CreateSignalExtraData();	// no leak - this will be deleted inside SendAnonymousSignal
				pEData->point = Vec3(0,0,0);

				pAIActor->SetSignal(1,"OnFallAndPlay",0,pEData);
			}
		}
	}

	// TODO: Does this do anything anymore ?
	CreateScriptEvent("sleep", 0);

	// Stop using mounted items before ragdollizing
	CItem* pCurrentItem = static_cast<CItem*>(GetCurrentItem());
	if(pCurrentItem && pCurrentItem->IsMounted() && pCurrentItem->IsUsed())
		pCurrentItem->StopUse(GetEntityId());

	//Do we want this for the player? (Sure not for AI)
	if(IsPlayer() && dropWeapon)
	{
		//DropItem(GetCurrentItemId(), 1.0f, false);
		if (GetCurrentItemId(false))
			HolsterItem(true);
	}

	// stop shooting
	if ( EntityId currentItem = GetCurrentItemId(true) )
		if ( CWeapon* pWeapon = GetWeapon(currentItem) )
			pWeapon->StopFire();

	//add some twist
	if (!IsClient() && hitInfo.pos.len())
	{
		if(IPhysicalEntity *pPE = GetEntity()->GetPhysics())
		{
			pe_action_impulse imp;
			if( hitInfo.partId != -1 )
			{
				const float scale = 1000.f;

				imp.partid = hitInfo.partId;
				imp.iApplyTime = 0;
				imp.point = hitInfo.pos;
				imp.impulse = scale*hitInfo.dir;
			}

			// push the impulse on to the queue.
			m_pImpulseHandler->SetOnRagdollPhysicalizedImpulse(imp);
		}
	}
}

//------------------------------------------------------------------------
void CActor::KnockDown(float backwardsImpulse)
{

}

//------------------------------------------------------------------------
void CActor::SetLookAtTargetId(EntityId targetIdsp, float interpolationTime){}

//------------------------------------------------------------------------
void CActor::SetForceLookAtTargetId(EntityId targetId, float interpolationTime){}

//------------------------------------------------------------------------
void CActor::StandUp()
{
	if ( m_health.IsDead() )
	{
		GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Ragdoll);
	}
	else
	{
		if ( GetLinkedVehicle() && GetAnimationGraphState() )
			GetAnimationGraphState()->Hurry();
		else if ( m_currentPhysProfile == eAP_Sleep )
			GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Alive);
	}
}

//------------------------------------------------------
void CActor::OnSetStance(EStance desiredStance)
{
	CRY_ASSERT_TRACE(desiredStance >= 0 && desiredStance < STANCE_LAST, ("%s '%s' desired stance %d is out of range 0-%d", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), desiredStance, STANCE_LAST - 1));

#if ENABLE_GAME_CODE_COVERAGE || ENABLE_SHARED_CODE_COVERAGE
	if (m_desiredStance != desiredStance)
	{
		switch (m_desiredStance)
		{
			case STANCE_STAND:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStopStand);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStopStand);
			break;

			case STANCE_CROUCH:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStopCrouch);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStopCrouch);
			break;

			case STANCE_SWIM:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStopSwim);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStopSwim);
			break;
		}

		switch (desiredStance)
		{
			case STANCE_STAND:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStartStand);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStartStand);
			break;

			case STANCE_CROUCH:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStartCrouch);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStartCrouch);
			break;

			case STANCE_SWIM:
			CCCPOINT_IF(m_isClient,  ActorStance_LocalActorStartSwim);
			CCCPOINT_IF(!m_isClient, ActorStance_OtherActorStartSwim);
			break;
		}
	}
#endif

	m_desiredStance = desiredStance;
}

void CActor::SetStance(EStance desiredStance)
{
	OnSetStance( desiredStance );
}

//------------------------------------------------------
void CActor::OnStanceChanged(EStance newStance, EStance oldStance)
{
	if(!gEnv->bMultiplayer)
	{
		EntityScripts::CallScriptFunction(GetEntity(), GetEntity()->GetScriptTable(), "OnStanceChanged", newStance, oldStance);
	}

	if (IsClient())
	{
		CItem* pCurrentItem = GetItem( GetCurrentItemId( false ) );
		if (pCurrentItem != NULL)
		{
			pCurrentItem->OnOwnerStanceChanged( newStance );
		}
	}
}

//------------------------------------------------------
IEntity *CActor::LinkToVehicle(EntityId vehicleId) 
{
	// did this link actually change, or are we just re-linking?
	bool changed=((m_linkStats.linkID!=vehicleId)||gEnv->pSystem->IsSerializingFile())?true:false;

	m_linkStats = SLinkStats(vehicleId,SLinkStats::LINKED_VEHICLE);
	
	IVehicle *pVehicle = m_linkStats.GetLinkedVehicle();
	IEntity *pLinked = pVehicle?pVehicle->GetEntity():NULL;

	IVehicleSeat* pSeat = pVehicle?pVehicle->GetSeatForPassenger(GetEntityId()):NULL;
	if(pSeat && pSeat->IsRemoteControlled())
	{
		return pLinked;
	}
  
	if (m_pAnimatedCharacter)
	{
		bool enabled = pLinked?true:false;
		
		if(gEnv->bServer)
		{
			if(enabled)
			{
				if (changed)
					GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Linked);
			}
			else if(IPhysicalEntity *pPhys = GetEntity()->GetPhysics())
			{
				pe_type type = pPhys->GetType();
				if(type == PE_LIVING)
					GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Alive);
				else if(type == PE_ARTICULATED)
					GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Ragdoll);
			}
		}

		// if the player is hidden when entering a vehicle, the collider mode
		//	change will be ignored (caused problems in Convoy due to cutscene)
		assert(!IsPlayer() || !GetEntity()->IsHidden());

		m_pAnimatedCharacter->ForceRefreshPhysicalColliderMode();
		m_pAnimatedCharacter->RequestPhysicalColliderMode(enabled ? eColliderMode_Disabled : eColliderMode_Undefined, eColliderModeLayer_Game, "Actor::LinkToVehicle");
	}
  
  if (pLinked)  
    pLinked->AttachChild(GetEntity(), ENTITY_XFORM_USER|IEntity::ATTACHMENT_KEEP_TRANSFORMATION);
  else
    GetEntity()->DetachThis(IEntity::ATTACHMENT_KEEP_TRANSFORMATION,/*ENTITY_XFORM_USER*/0);
  
	return pLinked;
}

IEntity *CActor::LinkToEntity(EntityId entityId, bool bKeepTransformOnDetach) 
{
	m_linkStats = SLinkStats(entityId,SLinkStats::LINKED_FREELOOK);

	IEntity *pLinked = m_linkStats.GetLinked();

	if (m_pAnimatedCharacter)
	{
		bool enabled = pLinked?true:false;

		m_pAnimatedCharacter->ForceRefreshPhysicalColliderMode();
		m_pAnimatedCharacter->RequestPhysicalColliderMode(enabled ? eColliderMode_Disabled : eColliderMode_Undefined, eColliderModeLayer_Game, "Actor::LinkToEntity");
	}

  if (pLinked)
    pLinked->AttachChild(GetEntity(), 0);
  else
		GetEntity()->DetachThis(bKeepTransformOnDetach ? IEntity::ATTACHMENT_KEEP_TRANSFORMATION : 0, bKeepTransformOnDetach ? ENTITY_XFORM_USER : 0);

	return pLinked;
}

void CActor::ProcessEvent(SEntityEvent& event)
{
	switch (event.event)
	{
	case ENTITY_EVENT_HIDE:
		{
			g_pGame->GetTacticalManager()->RemoveEntity(GetEntityId(), CTacticalManager::eTacticalEntity_Unit);
			if(!gEnv->bMultiplayer)
			{
				SHUDEvent hudevent(eHUDEvent_RemoveEntity);
				hudevent.AddData(SHUDEventData((int)GetEntityId()));
				CHUDEventDispatcher::CallEvent(hudevent);
			}

		} // no break, because the ENTITY_EVENT_INVISIBLE should be executed as well
	case ENTITY_EVENT_INVISIBLE:
		{
			HideAllAttachments(true);
		}	
		break;
	case ENTITY_EVENT_COLLISION:
		{
								  // CryLogAlways("CActor::ProcessEvent ENTITY_EVENT_COLLISION");
								  // EventPhysCollision *pCollision = (EventPhysCollision *)(event.nParam[0]);
								  // OnCollisionEvent(pCollision);
		}
		break;
	case ENTITY_EVENT_UNHIDE:
		{
			g_pGame->GetTacticalManager()->AddEntity(GetEntityId(), CTacticalManager::eTacticalEntity_Unit);
			SHUDEvent hudevent(eHUDEvent_AddEntity);
			hudevent.AddData(SHUDEventData((int)GetEntityId()));
			CHUDEventDispatcher::CallEvent(hudevent);

		} // no break, because the ENTITY_EVENT_VISIBLE should be executed as well
	case ENTITY_EVENT_VISIBLE:
		{
			if(gEnv->bMultiplayer && IsClient())
			{
				ICharacterInstance *pCharacter = GetEntity()->GetCharacter(0);
				if (pCharacter)
				{
					ISkeletonPose *pSkeletonPose = pCharacter->GetISkeletonPose();
					if (pSkeletonPose)
					{
						pSkeletonPose->SetForceSkeletonUpdate(2);
					}
				}
			}

			HideAllAttachments(false);
		}	
		break;
  case ENTITY_EVENT_RESET:
    Reset(event.nParam[0]==1);
    break;
	case ENTITY_EVENT_ANIM_EVENT:
		{
			const AnimEventInstance* pAnimEvent = reinterpret_cast<const AnimEventInstance*>(event.nParam[0]);
			ICharacterInstance* pCharacter = reinterpret_cast<ICharacterInstance*>(event.nParam[1]);
			if (pAnimEvent && pCharacter)
			{
				AnimationEvent(pCharacter, *pAnimEvent);
			}
		}
		break;
	case ENTITY_EVENT_RETURNING_TO_POOL:
		{
			// Set all the Item's Action Controllers to NULL as they will be removed in this event in the AnimatedCharacter and we can't leave hanging ptrs.
			ClearItemActionControllers();
		}
		break;
	case ENTITY_EVENT_DONE:
		{
			// Set all the Item's Action Controllers to NULL as they will be removed in this event in the AnimatedCharacter and we can't leave hanging ptrs.
			ClearItemActionControllers();

			if (IsClient())
			{
				BecomeRemotePlayer();
			}
		}
		break;
	case ENTITY_EVENT_TIMER:
		{
			if (event.nParam[0] == ITEM_SWITCH_TIMER_ID)
			{
				SActorStats* pActorStats = GetActorStats();
				assert(pActorStats);

				if (pActorStats->exchangeItemStats.switchingToItemID != 0)
				{
					SelectItem(pActorStats->exchangeItemStats.switchingToItemID, pActorStats->exchangeItemStats.keepHistory, false);
					pActorStats->exchangeItemStats.switchingToItemID = 0;
				}
				else
				{
					HolsterItem(true);
				}
			}
			else if( event.nParam[0] == ITEM_SWITCH_THIS_FRAME )
			{
				GetActorStats()->exchangeItemStats.switchThisFrame = true;
			}
			else if ( event.nParam[0] == RECYCLE_AI_ACTOR_TIMER_ID )
			{
				AttemptToRecycleAIActor();
			}
			else if (event.nParam[0] == ON_RAGDOLLIZED_GRAVITY_CHECK_TIMER_ID)
			{
				if (old_gravity_z != 0.0f)
				{
					if (GetActorStats()->isRagDoll)
					{
						CryLogAlways("ActorIsRagdolized");
						SetGravityZ(old_gravity_z);

					}
				}

				if (GetActorStats()->isRagDoll)
				{
					if (m_pAnimatedCharacter)
						m_pAnimatedCharacter->DisableAICollider();
				}
			}
		}
		break;
	case ENTITY_EVENT_PREPHYSICSUPDATE:
		{
			PrefetchLine(m_boneTrans, 0);	PrefetchLine(m_boneTrans, 128);	PrefetchLine(m_boneTrans, 256);	PrefetchLine(m_boneTrans, 384);
			COMPILE_TIME_ASSERT(sizeof(m_boneTrans) > 384);

			ICharacterInstance *pCharacter = GetEntity()->GetCharacter(0);
			ISkeletonPose *pSkelPose = pCharacter ? pCharacter->GetISkeletonPose() : NULL;
			if (pSkelPose)
			{
				QuatT * __restrict pBoneTrans = m_boneTrans;
				for (uint32 i=0; i<BONE_ID_NUM; i++)
				{
					int boneID = GetBoneID(i);
					if (boneID >= 0)
					{
						pBoneTrans[i] = pSkelPose->GetAbsJointByID(boneID);
					}
				}
			}

			SActorStats* pActorStats = GetActorStats();
			if( pActorStats->exchangeItemStats.switchThisFrame )
			{
				pActorStats->exchangeItemStats.switchThisFrame = false;

				SEntityEvent eventTimer;
				eventTimer.event = ENTITY_EVENT_TIMER;
				eventTimer.nParam[0] = ITEM_SWITCH_TIMER_ID;
				ProcessEvent( eventTimer );
			}

			break;
		}
	case ENTITY_EVENT_INIT:
		{
			Revive(kRFR_FromInit);
			break;
		}
	case ENTITY_EVENT_RELOAD_SCRIPT:
		{
			CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
			assert(pBodyDamageManager);

			pBodyDamageManager->ReloadBodyDamage(*this);
			break;
		}
	case ENTITY_EVENT_ADD_TO_RADAR:
		{
			SHUDEvent hudevent(eHUDEvent_AddEntity);
			hudevent.AddData(SHUDEventData(static_cast<int>(GetEntityId())));
			CHUDEventDispatcher::CallEvent(hudevent);
			break;
		}
	case ENTITY_EVENT_REMOVE_FROM_RADAR:
		{
			SHUDEvent hudevent(eHUDEvent_RemoveEntity);
			hudevent.AddData(SHUDEventData(static_cast<int>(GetEntityId())));
			CHUDEventDispatcher::CallEvent(hudevent);
			break;
		}
  }  
}

void CActor::BecomeRemotePlayer()
{
	CGameRules *pGameRules = g_pGame->GetGameRules();
	if (!pGameRules || pGameRules->IsRealActor(GetEntityId()))
	{
		gEnv->pCryPak->DisableRuntimeFileAccess (false);
	}
	m_isClient = false;
}

bool CActor::BecomeAggressiveToAgent(EntityId agentID)
{
	// return false to disallow it happening
	return true;
}

bool CActor::IsRemote() const
{
	static IEntityClass* sDummyPlayerClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("DummyPlayer");
	const bool isAI = !IsPlayer();
	IEntityClass* myClass = GetEntity()->GetClass();

	return gEnv->bMultiplayer && !IsClient() && !(gEnv->bServer && (myClass == sDummyPlayerClass || isAI));
}

void CActor::AddLocalHitImpulse(const SHitImpulse& hitImpulse)
{
	if(m_bAllowHitImpulses)
	{
		m_pImpulseHandler->AddLocalHitImpulse(hitImpulse);
	}
}

void CActor::Update(SEntityUpdateContext& ctx, int slot)
{
	if (block_updates)
		return;

#ifdef DEBUG_ACTOR_STATE
	if(g_pGameCVars && g_pGameCVars->g_displayDbgText_actorState)
	{
		IItem* currentItem = GetCurrentItem();
		WATCH_ACTOR_STATE("(team %d, health %8.2f/%8.2f%s%s%s) is (%s) stance=%s", m_teamId, m_health.GetHealth(), GetMaxHealth(), GetEntity()->IsHidden() ? ", $6HIDDEN$o" : "", GetActorStats()->isRagDoll ? ", $5RAGDOLL$o" : "", "", currentItem ? currentItem->GetEntity()->GetClass()->GetName() : "no item", GetStanceInfo(GetStance())->name);
	}

	if (CBodyManagerCVars::IsBodyDestructionDebugEnabled())
	{
		g_pGame->GetBodyDamageManager()->DebugBodyDestructionInstance(*GetEntity(), m_bodyDestructionInstance);
	}
#endif

	//CryLogAlways("CActor::Update called");

	m_damageEffectController.UpdateEffects(ctx.fFrameTime);

	// Death is now handled by PlayerStateDead (stephenn).
	// Only update stance for alive characters. Dead characters never request any stance changes
	// but if a different stance is requested for whatever reason (currently it happens after QL)
	// and the animation graph has different death animations for different stances (like for the
	// Hunter currently) then some other death animation may play again to better match the state.
	UpdateStance();

	if(gEnv->bServer)
	{
		m_telemetry.Update();
	}

	UpdateBodyDestruction(ctx.fFrameTime);

	UpdateServerResponseTimeOut(ctx.fFrameTime);

	UpdateLegsColliders();

	if (m_pActorSpellsActions)
	{
		m_pActorSpellsActions->Update(ctx.fFrameTime);
	}

	if (g_pGameCVars->g_actors_stats_debug_uphead_enable > 0)
	{
		IRenderer* pRenderer = gEnv->pRenderer;
		if (pRenderer)
		{
			Matrix34 matWorld = this->GetEntity()->GetWorldTM();
			Vec3 vWorldPos = matWorld.GetTranslation();
			vWorldPos.z += 2.1f;
			pRenderer->DrawLabel(vWorldPos, 1, "%i health", (int)this->GetHealth());
			vWorldPos.z += 0.2f;
			pRenderer->DrawLabel(vWorldPos, 1, "%i mass", (int)GetActorPhysics().mass);
			vWorldPos.z += 0.6f;
			pRenderer->DrawLabel(vWorldPos, 1, "%i stamina", (int)this->GetStamina());
			vWorldPos.z += 0.2f;
			pRenderer->DrawLabel(vWorldPos, 1, "%i armor rating", (int)this->GetArmor());
			if (g_pGameCVars->g_enable_debug_actors_dialog_stage > 0)
			{
				vWorldPos.z += 0.2f;
				pRenderer->DrawLabel(vWorldPos, 2, "%i dialog stage", GetDialogStage());
			}

			if (g_pGameCVars->g_enable_debug_items_in_actors_inventories > 0)
			{
				if (m_pInventory)
				{
					vWorldPos.z += 0.2f;
					pRenderer->DrawLabel(vWorldPos, 1, "%i of inventory items", m_pInventory->GetCount());
					vWorldPos.z += 0.2f;
					Vec3 old_psz = vWorldPos;
					int bugged_slots = 0;
					for (int i = 0; i < m_pInventory->GetCount(); i++)
					{
						CItem *pItemiiii = GetItem(m_pInventory->GetItem(i));
						if (pItemiiii)
						{
							if (pItemiiii->GetEquipped() > 0)
							{
								vWorldPos.z += 0.2f;
								pRenderer->DrawLabel(vWorldPos, 1, "item_in_inv(equipped) %s", pItemiiii->GetEntity()->GetName());
							}
							else
							{
								vWorldPos.z += 0.2f;
								pRenderer->DrawLabel(vWorldPos, 1, "item_in_inv %s", pItemiiii->GetEntity()->GetName());
							}
						}
						else
						{
							bugged_slots++;
						}
					}
					ColorF color(1, 0.1f, 0.1f, 1);
					pRenderer->DrawLabelEx(old_psz, 0.8f, (float*)&color, true, true, "%i of bugged slots in inventory array", bugged_slots);
				}
			}
		}
	}
}

void CActor::UpdateBodyDestruction(float frameTime)
{
	m_bodyDestructionInstance.Update(frameTime);
}

void CActor::ReadDataFromXML(bool isReloading/* = false*/)
{
	const IItemParamsNode* pEntityClassParamsNode = GetEntityClassParamsNode();
	if (!IsPoolEntity())
	{
		m_pImpulseHandler->ReadXmlData(pEntityClassParamsNode);
	}
};

bool CActor::UpdateStance()
{
	if (m_stance != m_desiredStance)
	{
		// If character is animated, postpone stance change until state transition is finished (i.e. in steady state).
		if ((m_pAnimatedCharacter != NULL) && m_pAnimatedCharacter->InStanceTransition())
			return false;

		if (!TrySetStance(m_desiredStance))
		{
#ifdef STANCE_DEBUG
			// Only time this should be allowed is when we're already in a valid state or we have no physics... otherwise warn user!
			// (Temporarily only warn about players, although really NPCs shouldn't be left in STANCE_NULL either.)
			if (m_stance == STANCE_NULL && GetEntity()->GetPhysics() && IsPlayer())
			{
				const Vec3 pos = GetEntity()->GetWorldPos();

				switch (m_tryToChangeStanceCounter)
				{
					case kNumFramesUntilDisplayNullStanceWarning + 1:
					break;

					case kNumFramesUntilDisplayNullStanceWarning:
					DesignerWarning(false, "%s '%s' (%g %g %g) can't change stance from %d '%s' to %d '%s' (tried and failed for %u consecutive frames). Possibly a spawn-point intersecting with level geometry?", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), pos.x, pos.y, pos.z, (int) m_stance, GetStanceName(m_stance), (int) m_desiredStance, GetStanceName(m_desiredStance), m_tryToChangeStanceCounter);
					++ m_tryToChangeStanceCounter;
					break;

					default:
					++ m_tryToChangeStanceCounter;
					CryLog("%s '%s' (%g %g %g) can't change stance from %d '%s' to %d '%s' [Attempt %u]", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), pos.x, pos.y, pos.z, (int) m_stance, GetStanceName(m_stance), (int) m_desiredStance, GetStanceName(m_desiredStance), m_tryToChangeStanceCounter);
					break;
				}
			}
#endif
			return false;
		}

		EStance newStance = m_desiredStance;
		EStance oldStance = m_stance;
		m_stance = newStance;

#ifdef STANCE_DEBUG
		if (m_tryToChangeStanceCounter)
		{
			const Vec3 pos = GetEntity()->GetWorldPos();
			CryLog("%s '%s' (%g %g %g) has finally changed stance from %d '%s' to %d '%s'", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), pos.x, pos.y, pos.z, (int) oldStance, GetStanceName(oldStance), (int) newStance, GetStanceName(m_desiredStance));
			m_tryToChangeStanceCounter = 0;
		}
#endif

		OnStanceChanged(newStance, oldStance);

		// Request new animation stance.
		// AnimatedCharacter has it's own understanding of stance, which might be in conflict.
		// Ideally the stance should be maintained only in one place. Currently the Actor (gameplay) rules.
		if (m_pAnimatedCharacter != NULL)
			m_pAnimatedCharacter->RequestStance(m_stance, GetStanceInfo(m_stance)->name);

		IPhysicalEntity *pPhysEnt = GetEntity()->GetPhysics();
		if (pPhysEnt != NULL)
		{
			pe_action_awake aa;
			aa.bAwake = 1;
			pPhysEnt->Action(&aa);
		}
	}

	return true;
}

//------------------------------------------------------

bool CActor::TrySetStance(EStance stance)
{
	IPhysicalEntity *pPhysEnt = GetEntity()->GetPhysics();
	int result = 0;
	if (pPhysEnt)
	{
		const SStanceInfo *sInfo = GetStanceInfo(stance);
#ifdef STANCE_DEBUG
		if (sInfo == &m_defaultStance)
		{
			CryLogAlways("%s trying to set undefined stance (%d).\nPlease update the stances tables in the Lua script accordingly.", 
				GetEntity()->GetName(), stance);
		}
#endif

		// don't changes to an invalid stance.
		if( stricmp( sInfo->name, "null" ) == 0 )
		{
			return false;
		}

		pe_player_dimensions playerDim;
		playerDim.heightEye = 0.0f;
		playerDim.heightCollider = sInfo->heightCollider;
		playerDim.sizeCollider = sInfo->size;
		playerDim.heightPivot = sInfo->heightPivot;
		if( m_stance != STANCE_NULL || stance == STANCE_CROUCH )
		{
			playerDim.maxUnproj = max(0.25f, sInfo->heightPivot);
		}
		else
		{
			// If we're coming from a NULL stance, try and be a bit for aggressive at
			// unprojecting from collisions.
			// fixes some last minute s/l bugs on Crysis2.
			playerDim.maxUnproj = max( sInfo->size.x, sInfo->heightPivot );
			playerDim.maxUnproj = max( 0.35f, playerDim.maxUnproj );
			playerDim.dirUnproj = ZERO;		
		}
		playerDim.bUseCapsule = sInfo->useCapsule;
		playerDim.groundContactEps = sInfo->groundContactEps;

		result = pPhysEnt->SetParams(&playerDim);
	}

	return (result != 0);
}

//------------------------------------------------------------------------
float CActor::GetSpeedMultiplier(SActorParams::ESpeedMultiplierReason reason)
{
	float fResult = 1.0f;

	assert(reason >= 0 && reason < SActorParams::eSMR_COUNT);
	if (reason >= 0 && reason < SActorParams::eSMR_COUNT)
		fResult = m_params.speedMultiplier[reason];

	return fResult;
}

//------------------------------------------------------------------------
void CActor::SetSpeedMultipler(SActorParams::ESpeedMultiplierReason reason, float fSpeedMult)
{
	assert(reason >= 0 && reason < SActorParams::eSMR_COUNT);
	if (reason >= 0 && reason < SActorParams::eSMR_COUNT)
		m_params.speedMultiplier[reason] = fSpeedMult;
}

//------------------------------------------------------------------------
void CActor::MultSpeedMultiplier(SActorParams::ESpeedMultiplierReason reason, float fSpeedMult)
{
	assert(reason >= 0 && reason < SActorParams::eSMR_COUNT);
	if (reason >= 0 && reason < SActorParams::eSMR_COUNT)
		m_params.speedMultiplier[reason] *= fSpeedMult;
}

//------------------------------------------------------------------------
void CActor::SetStats(SmartScriptTable &rTable)
{
	SActorStats *pStats = GetActorStats();
	if (pStats)
	{
		rTable->GetValue("inFiring",pStats->inFiring);
	}
}

//------------------------------------------------------------------------
void CActor::SetCloakLayer(bool set, eFadeRules config /*= eAllowFades*/)
{
	if ( !set && m_cloakLayerActive == true )
	{
		m_lastUnCloakTime = gEnv->pTimer->GetCurrTime();
	}

	m_cloakLayerActive = set;

	// new cloak effect
	const EntityId entityId = GetEntityId();
	const bool bCloakFadeByDistance = EntityEffects::Cloak::DoesCloakFadeByDistance(entityId);
	const uint8 cloakColorChannel = EntityEffects::Cloak::GetCloakColorChannel(entityId);
	const bool bIgnoreCloakRefractionColor = EntityEffects::Cloak::IgnoreRefractionColor(entityId);
	const bool bFade = (config == eAllowFades ? true : false);

	EntityEffects::Cloak::CloakEntity(entityId, set, bFade, GetCloakBlendSpeedScale(), bCloakFadeByDistance, cloakColorChannel, bIgnoreCloakRefractionColor);

	CloakSyncAttachments(true);

	// Cloak mounted item
	// (Mounted item's are seperate entities whilst mounted, not attachments)
	CItem* pItem = static_cast<CItem *>(GetCurrentItem());
	if(pItem && pItem->IsMounted())
	{
		CloakSyncEntity(pItem->GetEntityId(),true);
	}
}

//------------------------------------------------------------------------
void CActor::CloakSyncAttachments(bool bFade)
{
	IEntity *pEntity = GetEntity();

	// take care of the attachments on the back
	if (ICharacterInstance *pOwnerCharacter = pEntity->GetCharacter(0))
	{
		if (IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager())
		{
			const uint32 attachmentCount = pAttachmentManager->GetAttachmentCount();
			for (uint32 attachmentIndex = 0; attachmentIndex < attachmentCount; ++attachmentIndex)
			{
				if (IAttachment* pAttachment = pAttachmentManager->GetInterfaceByIndex(attachmentIndex))
				{
					CloakSyncAttachment(pAttachment, bFade);
				}
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::CloakSyncAttachment(IAttachment* pAttachment, bool bFade)
{
	assert(pAttachment);

	if(IAttachmentObject *pAO = pAttachment->GetIAttachmentObject())
	{
		if(pAO->GetAttachmentType()==IAttachmentObject::eAttachment_Entity)
		{
			CEntityAttachment* pEA = static_cast<CEntityAttachment*>(pAO);
			if (CItem* pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pEA->GetEntityId())))
			{
				//Ensure that the entity has the right flags set, though hidden, so that if we unhide it the flags are correct.
				//	Only do the fade if the object is visible.
				pItem->CloakSync(bFade && !pItem->GetEntity()->IsHidden());
			}
			else
			{
				CloakSyncEntity(pEA->GetEntityId(), bFade);
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::CloakSyncEntity(EntityId entityId, bool bFade)
{
	//Don't cloak held objects/NPCs
	const bool isPickAndThrowEntity = (GetGrabbedEntityId() == entityId) && !gEnv->bMultiplayer;
	
	if (!isPickAndThrowEntity)
	{
		IEntityRenderProxy *pOwnerRP = (IEntityRenderProxy*)GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
		if (pOwnerRP)
		{
			const uint8 ownerMask = pOwnerRP->GetMaterialLayersMask();
			const bool isCloaked = (ownerMask&MTL_LAYER_CLOAK) != 0;
			const bool bCloakFadeByDistance = pOwnerRP->DoesCloakFadeByDistance();
			const uint8 cloakColorChannel = pOwnerRP->GetCloakColorChannel();
			const bool bIgnoreCloakRefractionColor = pOwnerRP->DoesIgnoreCloakRefractionColor();

			EntityEffects::Cloak::CloakEntity(entityId, isCloaked, bFade, GetCloakBlendSpeedScale(), bCloakFadeByDistance, cloakColorChannel, bIgnoreCloakRefractionColor);

			if(CRecordingSystem *pRecordingSystem = g_pGame->GetRecordingSystem())
			{
				pRecordingSystem->OnObjectCloakSync( entityId, GetEntityId(), isCloaked, bFade);
			}
		}
	}
}

//------------------------------------------------------------------------
const float CActor::GetCloakBlendSpeedScale()
{
	return g_pGameCVars->g_cloakBlendSpeedScale;
}

//------------------------------------------------------------------------
void CActor::OnAction(const ActionId& actionId, int activationMode, float value)
{
	if (!gEnv->IsEditing())
	{
		if(!GetActorStats()->bAttemptingStealthKill)
		{
			IItem *pItem = GetCurrentItem();
			if (pItem)
				pItem->OnAction(GetGameObject()->GetEntityId(), actionId, activationMode, value);
		}
	}
}

//------------------------------------------------------------------------
void CActor::CreateScriptEvent(const char *event,float value,const char *str)
{
	EntityScripts::CallScriptFunction(GetEntity(), GetEntity()->GetScriptTable(), "ScriptEvent", event, value, str);
}

bool CActor::CreateCodeEvent(SmartScriptTable &rTable)
{
	return false;
}

void CActor::AnimationEvent(ICharacterInstance *pCharacter, const AnimEventInstance &event)
{
	EntityScripts::CallScriptFunction(GetEntity(), GetEntity()->GetScriptTable(), "AnimationEvent", event.m_EventName,  (float)atof(event.m_CustomParameter));
}


// In:	threshold angle, in degrees, that is needed before turning is even considered (>= 0.0f).
// In:	the current angle deviation needs to be over the turnThresholdAngle for longer than this time before the character turns (>= 0.0f).
//
void CActor::SetTurnAnimationParams(const float turnThresholdAngle, const float turnThresholdTime)
{			
	assert(turnThresholdTime >= 0.0f);
	assert(turnThresholdAngle >= 0.0f);

	m_params.AITurnParams.minimumAngle = DEG2RAD(turnThresholdAngle);
	m_params.AITurnParams.maximumDelay = turnThresholdTime;
}


void CActor::RequestFacialExpression(const char* pExpressionName /* = NULL */, f32* sequenceLength) 
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	IFacialInstance* pFacialInstance = (pCharacter ? pCharacter->GetFacialInstance() : 0);
	if (pFacialInstance)
	{
		IFacialAnimSequence* pSequence = pFacialInstance->LoadSequence(pExpressionName);
		pFacialInstance->PlaySequence(pSequence, eFacialSequenceLayer_AIExpression);
		if (pSequence)
		{
			if (sequenceLength)
			{
				Range r = pSequence->GetTimeRange();
				*sequenceLength = r.end - r.start;
			}
		}
		else if (pExpressionName != NULL)
		{
			// Try with a effector channel instead. This behavior would have cleaned any expression
			// on the face (with the previous PlaySequence(NULL...))
			IFacialModel* pFacialModel = pFacialInstance->GetFacialModel();
			IFacialEffectorsLibrary* pLibrary = pFacialModel ? pFacialModel->GetLibrary() : NULL;
			if (pLibrary)
			{
				IFacialEffector* pEffector = pLibrary->Find(pExpressionName);
				if (pEffector)
					pFacialInstance->StartEffectorChannel(pEffector, 1.0f, 0.1f);
			}
		}
	}
}

void CActor::PrecacheFacialExpression(const char* pExpressionName)
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	IFacialInstance* pFacialInstance = (pCharacter ? pCharacter->GetFacialInstance() : 0);
	if (pFacialInstance)
		pFacialInstance->PrecacheFacialExpression(pExpressionName);
}

void CActor::PostSerialize()
{
	if (ICharacterInstance *pCharacter = GetEntity()->GetCharacter(0))
	{
		if (ISkeletonPose *pSkelPose = pCharacter->GetISkeletonPose())
		{
			pSkelPose->SetForceSkeletonUpdate(2);
		}
	}

	if (GetHealth() <= 0.0f)
	{
		Fall();
	}

	m_telemetry.PostSerialize();
}

void CActor::SetChannelId(uint16 id)
{
}

void CActor::SetHealth(float health)
{
	if (GetEntityId() == g_pGame->GetClientActorId())
	{
		if (g_pGameCVars->g_pl_disable_health_consumption > 0)
		{
			if (health < GetHealth())
				return;
		}
	}

	if (health <= 0.0f)
	{
		if (IsGod() > 0) // handled in CPlayer
			return;

		//TODO: Remove this on _RELEASE?
		if (IsClient() == false)
		{
			if (gEnv->pAISystem && GetEntity()->GetAI())
				gEnv->pAISystem->DebugReportDeath(GetEntity()->GetAI());
		}

		health = 0.0f;
	}

	if (gEnv->bServer && g_pGameCVars->sv_pacifist && health < m_health.GetHealth())
		return;
	
	float prevHealth = m_health.GetHealth();

#ifdef DEMO_BUILD_RPG_SS
	m_health.SetHealth( min(health, m_health.GetHealthMax()) );
#else
	float fNewHealth = min(health, m_health.GetHealthMax());
	if (!IsPlayer())
		fNewHealth = (float)__fsel(g_pGameCVars->g_instantKillDamageThreshold, __fsel(g_pGameCVars->g_instantKillDamageThreshold - (prevHealth - fNewHealth), fNewHealth, 0.0f), fNewHealth);
	m_health.SetHealth( (float)__fsel(g_pGameCVars->g_maximumDamage, max(m_health.GetHealth() - g_pGameCVars->g_maximumDamage, fNewHealth), fNewHealth) );
#endif

	if (m_health.GetHealth()!=prevHealth && m_health.GetHealth()<=0.0f)
	{
		DeathIncome();
		IItem *pItem = GetCurrentItem();
		IWeapon *pWeapon = pItem ? pItem->GetIWeapon() : NULL;

		if (pWeapon)
		{
			//CryLogAlways("TryTodrop111bow111item");
			//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItem->GetEntityId(), "Bow"));
			CBow *pBow = static_cast<CBow*>(pItem);
			if (pBow)
			{
				//CryLogAlways("TryTodrop111bow111item");
				pBow->ForcedStopFire();
			}
			else
			{
				pWeapon->StopFire();
			}
		}
		//request disable colliders on death
		m_pAnimatedCharacter->DisableAICollider(); //RequestPhysicalColliderMode(eColliderMode_Disabled, eColliderModeLayer_Game, "Actor::OnDeath");
		if (old_gravity_z != 0.0f)
		{
			if (old_gravity_z != 0.0f)
			{
				if (old_gravity_z > 0.0f)
					old_gravity_z = -old_gravity_z;
				else
					old_gravity_z = old_gravity_z - (old_gravity_z * 2);
				//CryLogAlways("Actor have setuped gravity z, CActor::SetHealth 0 called");
				m_pAnimatedCharacter->SetRagdollizedZGravity(old_gravity_z);
			}
			//GetEntity()->SetTimer(ON_RAGDOLLIZED_GRAVITY_CHECK_TIMER_ID, 100);
		}
	}
	//m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_HealthChanged, 0, m_health, 0));
}

void CActor::DamageInfo(EntityId shooterID, EntityId weaponID, IEntityClass *pProjectileClass, float damage, int damageType, const Vec3 hitDirection)
{
}


void InitializeHitRecoilGameEffect()
{
}


void CActor::SetMaxHealth( float maxHealth )
{
	if (gEnv->bMultiplayer && IsPlayer())
	{
#ifndef DEMO_BUILD_RPG_SS
		//const int oldMaxHealth = maxHealth;
#endif

		const float fmaxh = maxHealth;
		maxHealth = max(1.0f, floorf(fmaxh * g_pGameCVars->g_maxHealthMultiplier));

#ifndef DEMO_BUILD_RPG_SS
		//CryLog ("CActor::SetMaxHealth() - Scaling max health %d for %s '%s' by %.3f, giving %d", oldMaxHealth, GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), g_pGameCVars->g_maxHealthMultiplier, maxHealth);
#endif
	}

	assert (maxHealth > 0.0f);
	m_health.SetHealthMax( maxHealth );

	if (m_health.GetHealth() > 0.0f)
	{
		SetHealth(maxHealth);
	}
}

void CActor::Kill()
{
	if(IsClient())
	{
		// Clear force feedback
		IForceFeedbackSystem* pForceFeedbackSystem = gEnv->pGame->GetIGameFramework()->GetIForceFeedbackSystem();
		if(pForceFeedbackSystem)
		{
			pForceFeedbackSystem->StopAllEffects();
		}
	}

	if (!m_health.IsDead())
		SetHealth(0.0f);

	IEntity *pEntity = GetEntity();
	if (IAIObject *pAI = pEntity->GetAI())
	{
		pAI->Event(AIEVENT_DISABLE, NULL);
	}

	SetCloakLayer(false);
	
	if (IVehicle* pVehicle = GetLinkedVehicle())
	{
		if (IVehicleSeat* pVehicleSeat = pVehicle->GetSeatForPassenger(GetEntityId()))
			pVehicleSeat->OnPassengerDeath();
	}

	g_pGame->GetTacticalManager()->RemoveEntity(GetEntityId(), CTacticalManager::eTacticalEntity_Unit);
	if(!gEnv->bMultiplayer)
	{
		SHUDEvent hudevent(eHUDEvent_RemoveEntity);
		hudevent.AddData(SHUDEventData((int)GetEntityId()));
		CHUDEventDispatcher::CallEvent(hudevent);
	}

	RequestFacialExpression("Exp_Dead");

}

void CActor::DeathIncome()
{
	if (GetInventory() && GetGold() > 0)
	{
		IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		if (!pItemSystem)
			return;

		Matrix34 mxt_nl = Matrix34::CreateIdentity();
		mxt_nl.SetTranslation(Vec3(0, 0, cry_random(-1000.0f, -50.0f)));
		EntityId ids = pItemSystem->CreateItemSimple(mxt_nl, "Gold");
		if (ids > 0)
		{
			m_pInventory->AddItem(ids);
			CItem *pItem = static_cast<CItem*>(pItemSystem->GetItem(ids));
			if (pItem)
			{
				pItem->Physicalize(false, false);
				pItem->HideItem(true);
				pItem->SetItemQuanity(GetGold());
			}
		}
	}

	if (IsPlayer())
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->CloseAnyInteractiveUIOpened();
		}
	}

	if (IsRemote() && gEnv->bMultiplayer)
	{
		CPlayer *pPlayer = static_cast<CPlayer*>(this);
		if (pPlayer)
		{
			pPlayer->m_player_name = "";
			m_chrmodelstr = "";
			ResetActorVars();
			pPlayer->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_RPG_INFO, 0.0f);
		}
	}
}

//------------------------------------------------------------------------
bool CActor::LoadDynamicAimPoseElement(CScriptSetGetChain& gameParamsTableChain, const char* szName, string& output)
{
	const char* pose;
	pose = 0;
	if (gameParamsTableChain.GetValue(szName, pose))
		output = pose;
	return !output.empty();
}


//------------------------------------------------------------------------
bool CActor::LoadGameParams(SmartScriptTable pEntityTable, SActorGameParams &outGameParams)
{
	assert((bool)pEntityTable);

	bool bResult = false;

	SmartScriptTable pGameParams;
	if (pEntityTable && pEntityTable->GetValue("gameParams", pGameParams))
	{
		SmartScriptTable tempTable;

		if (pGameParams->GetValue("stance", tempTable))
		{
			IScriptTable::Iterator iter = tempTable->BeginIteration();
			int stance;

			while (tempTable->MoveNext(iter))
			{
				SmartScriptTable stanceTable;

				if (iter.value.CopyTo(stanceTable))
				{
					if (stanceTable->GetValue("stanceId",stance))
					{
						if (stance == STANCE_NULL)
							break;

						if (stance > STANCE_NULL && stance < STANCE_LAST)
						{
							SStanceInfo &sInfo = outGameParams.stances[stance];

							CScriptSetGetChain stanceTableChain(stanceTable);
							stanceTableChain.GetValue("normalSpeed",sInfo.normalSpeed);
							stanceTableChain.GetValue("maxSpeed",sInfo.maxSpeed);
							stanceTableChain.GetValue("heightCollider",sInfo.heightCollider);
							stanceTableChain.GetValue("heightPivot",sInfo.heightPivot);
							stanceTableChain.GetValue("groundContactEps",sInfo.groundContactEps);
							stanceTableChain.GetValue("size",sInfo.size);
							stanceTableChain.GetValue("modelOffset",sInfo.modelOffset);

							stanceTableChain.GetValue("viewOffset",sInfo.viewOffset);
							sInfo.leanLeftViewOffset = sInfo.leanRightViewOffset = sInfo.viewOffset;
							stanceTableChain.GetValue("leanLeftViewOffset",sInfo.leanLeftViewOffset);
							stanceTableChain.GetValue("leanRightViewOffset",sInfo.leanRightViewOffset);
							sInfo.whileLeanedLeftViewOffset = sInfo.whileLeanedRightViewOffset = sInfo.viewOffset;
							stanceTableChain.GetValue("whileLeanedLeftViewOffset", sInfo.whileLeanedLeftViewOffset);
							stanceTableChain.GetValue("whileLeanedRightViewOffset", sInfo.whileLeanedRightViewOffset);

							sInfo.peekOverViewOffset = sInfo.viewOffset;
							stanceTableChain.GetValue("peekOverViewOffset", sInfo.peekOverViewOffset);
							sInfo.peekOverWeaponOffset = sInfo.weaponOffset;
							stanceTableChain.GetValue("peekOverWeaponOffset", sInfo.peekOverWeaponOffset);

							stanceTableChain.GetValue("viewDownYMod",sInfo.viewDownYMod);

							stanceTableChain.GetValue("weaponOffset",sInfo.weaponOffset);
								sInfo.leanLeftWeaponOffset = sInfo.leanRightWeaponOffset = sInfo.weaponOffset;
							stanceTableChain.GetValue("leanLeftWeaponOffset",sInfo.leanLeftWeaponOffset);
								stanceTableChain.GetValue("leanRightWeaponOffset",sInfo.leanRightWeaponOffset);
							sInfo.whileLeanedLeftWeaponOffset = sInfo.whileLeanedRightWeaponOffset = sInfo.weaponOffset;
							stanceTableChain.GetValue("whileLeanedLeftWeaponOffset", sInfo.whileLeanedLeftWeaponOffset);
							stanceTableChain.GetValue("whileLeanedRightWeaponOffset", sInfo.whileLeanedRightWeaponOffset);

							stanceTableChain.GetValue("useCapsule",sInfo.useCapsule);

							const char *name = 0;
							if (stanceTableChain.GetValue("name",name))
							{
								cry_strcpy(sInfo.name, name);
							}
						}
					}
				}
			}

			tempTable->EndIteration(iter);
		}

		if (pGameParams->GetValue("boneIDs", tempTable))
		{
			for (uint32 i=0; i<BONE_ID_NUM; i++)
			{
				const char *name = 0;
				if (tempTable->GetValue(s_BONE_ID_NAME[i], name))
				{
					outGameParams.boneNames[i] = name;
				}
			}
		}

		SActorParams &actorParams = outGameParams.actorParams;
		CScriptSetGetChain gameParamsTableChain(pGameParams);
		gameParamsTableChain.GetValue("meeleHitRagdollImpulseScale", actorParams.meeleHitRagdollImpulseScale);

		if (gameParamsTableChain.GetValue("aimFOV", actorParams.aimFOVRadians))
			actorParams.aimFOVRadians = DEG2RAD(actorParams.aimFOVRadians);

		if (gameParamsTableChain.GetValue("lookFOV", actorParams.lookFOVRadians))
			actorParams.lookFOVRadians = DEG2RAD(actorParams.lookFOVRadians);

		if (gameParamsTableChain.GetValue("lookInVehicleFOV", actorParams.lookInVehicleFOVRadians))
			actorParams.lookInVehicleFOVRadians = DEG2RAD(actorParams.lookInVehicleFOVRadians);
		else
			actorParams.lookInVehicleFOVRadians = actorParams.lookFOVRadians;

		gameParamsTableChain.GetValue("allowLookAimStrafing", actorParams.allowLookAimStrafing);
		gameParamsTableChain.GetValue("cornerSmoother", actorParams.cornerSmoother);

		if (gameParamsTableChain.GetValue("maxLookAimAngle", actorParams.maxLookAimAngleRadians))
			actorParams.maxLookAimAngleRadians = DEG2RAD(actorParams.maxLookAimAngleRadians);

		gameParamsTableChain.GetValue("sprintMultiplier", actorParams.sprintMultiplier);
		gameParamsTableChain.GetValue("crouchMultiplier", actorParams.crouchMultiplier);
		gameParamsTableChain.GetValue("strafeMultiplier", actorParams.strafeMultiplier);
		gameParamsTableChain.GetValue("backwardMultiplier", actorParams.backwardMultiplier);

		gameParamsTableChain.GetValue("jumpHeight", actorParams.jumpHeight);

		gameParamsTableChain.GetValue("leanShift", actorParams.leanShift);
		gameParamsTableChain.GetValue("leanAngle", actorParams.leanAngle);

		gameParamsTableChain.GetValue("speedMultiplier", actorParams.internalSpeedMult);

		// View-related

		Vec3 limitDir(ZERO);
		float limitH = 0.f, limitV = 0.f;

		gameParamsTableChain.GetValue("viewLimitDir", limitDir);
		gameParamsTableChain.GetValue("viewLimitYaw", limitH);
		gameParamsTableChain.GetValue("viewLimitPitch", limitV);
		gameParamsTableChain.GetValue("viewFoVScale", actorParams.viewFoVScale);

		actorParams.viewLimits.SetViewLimit(limitDir, limitH, limitV, 0.f, 0.f, SViewLimitParams::eVLS_None);

		gameParamsTableChain.GetValue("canUseComplexLookIK", actorParams.canUseComplexLookIK);

		const char* lookAtSimpleHeadBoneName = 0;
		if (gameParamsTableChain.GetValue("lookAtSimpleHeadBone", lookAtSimpleHeadBoneName))
		{
			actorParams.lookAtSimpleHeadBoneName = lookAtSimpleHeadBoneName;
		}

		gameParamsTableChain.GetValue("aimIKLayer", actorParams.aimIKLayer);
		gameParamsTableChain.GetValue("lookIKLayer", actorParams.lookIKLayer);

		gameParamsTableChain.GetValue("aimIKFadeDuration", actorParams.aimIKFadeDuration);
		gameParamsTableChain.GetValue("proceduralLeaningFactor", actorParams.proceduralLeaningFactor);

		actorParams.useDynamicAimPoses  = LoadDynamicAimPoseElement(gameParamsTableChain, "idleLeftArmAimPose", actorParams.idleDynamicAimPose.leftArmAimPose);
		actorParams.useDynamicAimPoses |= LoadDynamicAimPoseElement(gameParamsTableChain, "idleRightArmAimPose", actorParams.idleDynamicAimPose.rightArmAimPose);
		actorParams.useDynamicAimPoses |= LoadDynamicAimPoseElement(gameParamsTableChain, "idleBothArmsAimPose", actorParams.idleDynamicAimPose.bothArmsAimPose);
		actorParams.useDynamicAimPoses |= LoadDynamicAimPoseElement(gameParamsTableChain, "runLeftArmAimPose", actorParams.runDynamicAimPose.leftArmAimPose);
		actorParams.useDynamicAimPoses |= LoadDynamicAimPoseElement(gameParamsTableChain, "runRightArmAimPose", actorParams.runDynamicAimPose.rightArmAimPose);
		actorParams.useDynamicAimPoses |= LoadDynamicAimPoseElement(gameParamsTableChain, "runBothArmsAimPose", actorParams.runDynamicAimPose.bothArmsAimPose);

		float bothArmsAimFOV = 0.0f;
		if (gameParamsTableChain.GetValue("bothArmsAimFOV", bothArmsAimFOV))
		{
			actorParams.bothArmsAimHalfFOV = DEG2RAD(bothArmsAimFOV) * 0.5f;
		}

		gameParamsTableChain.GetValue("bothArmsAimPitchFactor", actorParams.bothArmsAimPitchFactor);

		{
			float minimumAngleDegrees;
			if (gameParamsTableChain.GetValue("turnThresholdAngle", minimumAngleDegrees))
			{
				actorParams.AITurnParams.minimumAngle = max(DEG2RAD(minimumAngleDegrees), 0.0f);
			}

			gameParamsTableChain.GetValue("turnThresholdTime", actorParams.AITurnParams.maximumDelay);

			float minimumAngleForTurnWithoutDelayDegrees;
			if (gameParamsTableChain.GetValue("minimumAngleForTurnWithoutDelay", minimumAngleForTurnWithoutDelayDegrees))
			{
				actorParams.AITurnParams.minimumAngleForTurnWithoutDelay = max(DEG2RAD(minimumAngleForTurnWithoutDelayDegrees), actorParams.AITurnParams.minimumAngle);
			}
		}

		gameParamsTableChain.GetValue("stepThresholdDistance", actorParams.stepThresholdDistance);
		gameParamsTableChain.GetValue("stepThresholdTime", actorParams.stepThresholdTime);
		int defStance;
		if (gameParamsTableChain.GetValue("defaultStance", defStance))
		{
			assert((defStance > STANCE_NULL) && (defStance < STANCE_LAST));
			actorParams.defaultStance = (EStance)defStance;
		}

		if (gameParamsTableChain.GetValue("maxDeltaAngleRateNormal", actorParams.maxDeltaAngleRateNormal))
		{
			actorParams.maxDeltaAngleRateNormal = DEG2RAD(actorParams.maxDeltaAngleRateNormal);
		}
		if (gameParamsTableChain.GetValue("maxDeltaAngleRateAnimTarget", actorParams.maxDeltaAngleRateAnimTarget))
		{
			actorParams.maxDeltaAngleRateAnimTarget = DEG2RAD(actorParams.maxDeltaAngleRateAnimTarget);
		}
		if (gameParamsTableChain.GetValue("maxDeltaAngleMultiplayer", actorParams.maxDeltaAngleMultiplayer))
		{
			actorParams.maxDeltaAngleMultiplayer = DEG2RAD(actorParams.maxDeltaAngleMultiplayer);
		}
		if (gameParamsTableChain.GetValue("maxDeltaAngleRateJukeTurn", actorParams.maxDeltaAngleRateJukeTurn))
		{
			actorParams.maxDeltaAngleRateJukeTurn = DEG2RAD(actorParams.maxDeltaAngleRateJukeTurn);
		}
		gameParamsTableChain.GetValue("smoothedZTurning", actorParams.smoothedZTurning);

		// Physic-related
		SmartScriptTable physicsParams;
		if(pEntityTable->GetValue("physicsParams", physicsParams))
		{
			physicsParams->GetValue("fallNPlayStiffness_scale", actorParams.fallNPlayStiffness_scale);
		}

		//DBAs for character
		SmartScriptTable characterDBAs;
		if (pGameParams->GetValue("characterDBAs", characterDBAs))
		{
			const int tableElements = characterDBAs->Count();
			for (int i = 1; i <= tableElements; ++i)
			{
				if(characterDBAs->GetAtType(i) == svtString)
				{
					const char* dbaGroupName = NULL;
					characterDBAs->GetAt(i, dbaGroupName);

					CRY_ASSERT(dbaGroupName);

					outGameParams.animationDBAs.push_back(string(dbaGroupName));
				}
			}
		}

		bResult = true;
	}

	SmartScriptTable propertiesTable;
	if (pEntityTable && pEntityTable->GetValue("Properties", propertiesTable))
	{
		// End-of-C3 workaround for the fact that a property that should be editable is in the gameparams table
		bool overrideAllowLookAimStrafing;
		if (propertiesTable->GetValue("bOverrideAllowLookAimStrafing", overrideAllowLookAimStrafing))
		{
			outGameParams.actorParams.allowLookAimStrafing = overrideAllowLookAimStrafing;
		}

		SmartScriptTable characterSoundsTable;
		if (propertiesTable->GetValue("CharacterSounds", characterSoundsTable))
		{
			SActorParams &actorParams = outGameParams.actorParams;

			CScriptSetGetChain characterSoundsChain(characterSoundsTable);

			// Footstep Effect Name
			const char* footstepName = 0;
			if (characterSoundsChain.GetValue("footstepEffect", footstepName) &&
				footstepName && footstepName[0])
			{
				actorParams.footstepEffectName = footstepName;
			}

			const char* remoteFootstepName = 0;
			if (characterSoundsChain.GetValue("remoteFootstepEffect", remoteFootstepName) &&
				remoteFootstepName && remoteFootstepName[0])
			{
				actorParams.remoteFootstepEffectName = remoteFootstepName;
			}

			// Foley Effect Name
			const char* foleyName = 0;
			if (characterSoundsChain.GetValue("foleyEffect", foleyName) &&
				foleyName && foleyName[0])
			{
				actorParams.foleyEffectName = foleyName;
			}
			characterSoundsChain.GetValue("bFootstepGearEffect", actorParams.footstepGearEffect);

			const char* footstepIndGearAudioSignal_Walk = 0;
			if (characterSoundsChain.GetValue("footstepIndGearAudioSignal_Walk", footstepIndGearAudioSignal_Walk) &&
				footstepIndGearAudioSignal_Walk && footstepIndGearAudioSignal_Walk[0])
			{
				actorParams.footstepIndGearAudioSignal_Walk = footstepIndGearAudioSignal_Walk;
			}

			const char* footstepIndGearAudioSignal_Run = 0;
			if (characterSoundsChain.GetValue("footstepIndGearAudioSignal_Run", footstepIndGearAudioSignal_Run) &&
				footstepIndGearAudioSignal_Run && footstepIndGearAudioSignal_Run[0])
			{
				actorParams.footstepIndGearAudioSignal_Run = footstepIndGearAudioSignal_Run;
			}
		}
	}

	return bResult;
}

//------------------------------------------------------------------------
void CActor::InitGameParams()
{
	IEntity *pEntity = GetEntity();

	if (m_LuaCache_GameParams)
	{
		const bool reloadCharacterSounds = !gEnv->bMultiplayer && !IsPlayer();
		InitGameParams(m_LuaCache_GameParams->gameParams, reloadCharacterSounds);
	}
	else
	{
		IEntityClass *pClass = pEntity->GetClass();

		// Run-time loading
		if (CGameCache::IsLuaCacheEnabled())
		{
			GameWarning("[Game Cache] Warning: Loading game params for entity class \'%s\' at run-time!", pClass->GetName());
		}

		const bool reloadCharacterSounds = false;
		SActorGameParams gameParams;
		if (LoadGameParams(pEntity->GetScriptTable(), gameParams))
		{
			InitGameParams(gameParams, reloadCharacterSounds);
		}
		else
		{
			GameWarning("Failed to load game params for actor \'%s\'", pEntity->GetName());
		}
	}
}

//------------------------------------------------------------------------
void CActor::SetStanceMaxSpeed(uint32 stance, float fValue)
{
	if ((stance < STANCE_LAST))
	{
		m_stances[stance].maxSpeed = fValue;
	}
}

//------------------------------------------------------------------------
void CActor::InitGameParams(const SActorGameParams &gameParams, const bool reloadCharacterSounds)
{
	// Copy stances over
	for (uint32 stance = 0; stance < STANCE_LAST; ++stance)
	{
		m_stances[stance] = gameParams.stances[stance];
	}

	// Create bone Ids
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(0);
	ISkeletonPose *pSkelPose = pCharacter ? pCharacter->GetISkeletonPose() : NULL;
	if (pSkelPose)
	{
		IDefaultSkeleton& rIDefaultSkeleton = pCharacter->GetIDefaultSkeleton();
		for (uint32 boneId = 0; boneId < BONE_ID_NUM; ++boneId)
		{
			const string &sName = gameParams.boneNames[boneId];
			if (!sName.empty())
			{
				m_boneIDs[boneId] = rIDefaultSkeleton.GetJointIDByName(sName.c_str());
				if (m_boneIDs[boneId] < 0)
				{
					GameWarning("CActor %s - %s not a valid joint for BoneID %s in character %s", GetEntity()->GetName(), sName.c_str(), s_BONE_ID_NAME[boneId], pCharacter->GetFilePath());
				}
			}
		}
	}

	// Copy params over
	m_params = gameParams.actorParams;

	// Workaround: Need to override sound properties because these are per instance
	// TODO: Move sound values into properties instance cache (needs some more work and changes) 
	if(reloadCharacterSounds)
	{
		IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
		SmartScriptTable propertiesTable;
		if((pScriptTable != NULL) && pScriptTable->GetValue("Properties", propertiesTable))
		{
			assert(propertiesTable.GetPtr() != NULL);

			SmartScriptTable characterSoundsTable;
			if (propertiesTable->GetValue("CharacterSounds", characterSoundsTable))
			{
				CScriptSetGetChain characterSoundsChain(characterSoundsTable);

				// Footstep Effect Name
				const char* footstepName = "";
				if (characterSoundsChain.GetValue("footstepEffect", footstepName))
				{
					m_params.footstepEffectName = footstepName;
				}

				const char* remoteFootstepName = "";
				if (characterSoundsChain.GetValue("remoteFootstepEffect", remoteFootstepName))
				{
					m_params.remoteFootstepEffectName = remoteFootstepName;
				}

				// Foley Effect Name
				const char* foleyName = "";
				if (characterSoundsChain.GetValue("foleyEffect", foleyName))
				{
					m_params.foleyEffectName = foleyName;
				}

				characterSoundsChain.GetValue("bFootstepGearEffect", m_params.footstepGearEffect);

				const char* footstepIndGearAudioSignal_Walk = "";
				if (characterSoundsChain.GetValue("footstepIndGearAudioSignal_Walk", footstepIndGearAudioSignal_Walk))
				{
					m_params.footstepIndGearAudioSignal_Walk = footstepIndGearAudioSignal_Walk;
				}

				const char* footstepIndGearAudioSignal_Run = "";
				if (characterSoundsChain.GetValue("footstepIndGearAudioSignal_Run", footstepIndGearAudioSignal_Run))
				{
					m_params.footstepIndGearAudioSignal_Run = footstepIndGearAudioSignal_Run;
				}
			}
		}
		
	}

	SetSpeedMultipler(SActorParams::eSMR_Internal, m_params.internalSpeedMult);
}

//------------------------------------------------------------------------
void CActor::SetParamsFromLua(SmartScriptTable &rTable)
{
	
}

bool CActor::IsClient() const
{
	return m_isClient;
}

bool CActor::IsPlayer() const
{
	return m_isPlayer;
}


bool CActor::SetAspectProfile( EEntityAspects aspect, uint8 profile )
{
	bool res(false);

	if (aspect == eEA_Physics)
	{
		if (!gEnv->bMultiplayer && m_currentPhysProfile==profile && !gEnv->pSystem->IsSerializingFile()) //rephysicalize when loading savegame
			return true;

		switch (profile)
		{
		case eAP_NotPhysicalized:
			{
				SEntityPhysicalizeParams params;
				params.type = PE_NONE;
				GetEntity()->Physicalize(params);
			}
			res=true;
			break;
		case eAP_Spectator:
		case eAP_Alive:
			{
				if(!gEnv->bMultiplayer)
				{
					// if we were asleep, we just want to wakeup
					if (profile==eAP_Alive && (m_currentPhysProfile==eAP_Sleep || m_currentPhysProfile==eAP_Ragdoll))
					{
						ICharacterInstance *pCharacter=GetEntity()->GetCharacter(0);
						if (pCharacter && pCharacter->GetISkeletonAnim())
						{
							IPhysicalEntity *pPhysicalEntity=0;
							Matrix34 delta(IDENTITY);

							if (m_currentPhysProfile==eAP_Sleep)
							{
								QuatTS location(GetEntity()->GetWorldTM());
								pCharacter->GetISkeletonPose()->BlendFromRagdoll(location, pPhysicalEntity, false);
								delta = Matrix34(location);

								if (pPhysicalEntity)
								{
									pe_player_dynamics pd;
									pd.gravity.Set(0.0f, 0.0f, -1.8f);
									pPhysicalEntity->SetParams(&pd);

									pe_params_flags pf;
									pf.flagsOR = pef_ignore_areas;
									pPhysicalEntity->SetParams(&pf);

									pe_params_foreign_data pfd;
									pfd.iForeignData = PHYS_FOREIGN_ID_ENTITY;
									pfd.pForeignData = GetEntity();
									for (int iAuxPhys = -1; ; ++iAuxPhys)
									{
										if (IPhysicalEntity* pent = pCharacter->GetISkeletonPose()->GetCharacterPhysics(iAuxPhys))
										{
											pent->SetParams(&pfd);
										}
										else
										{
											break;
										}
									}

									IEntityPhysicalProxy *pPhysicsProxy=static_cast<IEntityPhysicalProxy *>(GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS));
									if (pPhysicsProxy)
									{
										GetEntity()->SetWorldTM(delta);
										pPhysicsProxy->AssignPhysicalEntity(pPhysicalEntity);
									}
								}
							}
							else
							{
								CRY_ASSERT(m_currentPhysProfile==eAP_Ragdoll);

								pCharacter->GetISkeletonPose()->SetDefaultPose();
								Physicalize(STANCE_NULL);
							}

							assert(m_pAnimatedCharacter);
							m_pAnimatedCharacter->ForceRefreshPhysicalColliderMode();

							PhysicalizeBodyDamage();
						}
					}
					else
					{
						Physicalize(STANCE_NULL);
					}
				}
			}
			res=true;
			break;
		case eAP_Linked:
			// make sure we are alive, for when we transition from ragdoll to linked...
			if (!GetEntity()->GetPhysics() || GetEntity()->GetPhysics()->GetType()!=PE_LIVING)
				Physicalize();
			res=true;
			break;
		case eAP_Sleep:
			RagDollize(true);
			res=true;
			break;
		case eAP_Ragdoll:
			// killed while sleeping?
			RagDollize(false);
			res=true;
			break;
		}

		m_currentPhysProfile = profile;
	}

	return res;
}

bool CActor::GetRagdollContext( CProceduralContextRagdoll** ppRagdollContext ) const
{
	CRY_ASSERT( ppRagdollContext );

	IActionController* piActionController = m_pAnimatedCharacter->GetActionController();
	if( !gEnv->bMultiplayer && piActionController )
	{
		IProceduralContext* piProcContext = piActionController->FindOrCreateProceduralContext( PROCEDURAL_CONTEXT_RAGDOLL_NAME );
		if( piProcContext )
		{
			*ppRagdollContext = static_cast<CProceduralContextRagdoll*> (piProcContext);
			return true;
		}
	}
	return false;
}

bool CActor::AllowPhysicsUpdate(uint8 newCounter) const
{
	return AllowPhysicsUpdate(newCounter, m_netPhysCounter);
}

bool CActor::AllowPhysicsUpdate(uint8 newCounter, uint8 oldCounter)
{
	return (newCounter == oldCounter);
}

bool CActor::NetSerialize( TSerialize ser, EEntityAspects aspect, uint8 profile, int pflags )
{
	NET_PROFILE_SCOPE("Actor NetSerialize", ser.IsReading());
	m_damageEffectController.NetSerialiseEffects(ser, aspect);

	if (aspect == eEA_Physics)
	{
		pe_type type = PE_NONE;
		switch (profile)
		{
		case eAP_NotPhysicalized:
			type = PE_NONE;
			break;
		case eAP_Spectator:
			type = PE_LIVING;
			break;
		case eAP_Alive:
			type = PE_LIVING;
			break;
		case eAP_Sleep:
		case eAP_Ragdoll:
			type = PE_NONE;
			break;
		case eAP_Linked: 	//if actor is attached to a vehicle - don't serialize actor physics additionally
			return true;
			break;
		default:
			return false;
		}

		if (type == PE_NONE)
		{
			return true;
		}

		NET_PROFILE_SCOPE("Physics", ser.IsReading());

		IEntityPhysicalProxy * pEPP = (IEntityPhysicalProxy *) GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS);
		if (ser.IsWriting())
		{
			if (!pEPP || !pEPP->GetPhysicalEntity() || pEPP->GetPhysicalEntity()->GetType() != type)
			{
				if(type!=PE_LIVING)
				{
					gEnv->pPhysicalWorld->SerializeGarbageTypedSnapshot( ser, type, 0 );
				}
				return true;
			}
		}
		else if (!pEPP)
		{
			return false;
		}

		if(type!=PE_LIVING)
		{
			pEPP->SerializeTyped( ser, type, pflags );
		}
	}

	return true;
}


void CActor::ProceduralRecoil( float duration, float kinematicImpact, float kickIn/*=0.8f*/, int arms/*=0*/ )
{
	if(m_pAnimatedCharacter)
	{
		EAnimatedCharacterArms animCharArms;
		switch(arms)
		{
		case 1: animCharArms = eACA_RightArm; break;
		case 2: animCharArms = eACA_LeftArm; break;
		default: animCharArms = eACA_BothArms;
		}
		m_pAnimatedCharacter->TriggerRecoil(duration, kinematicImpact, kickIn, animCharArms);
	}
}

void CActor::HandleEvent( const SGameObjectEvent& event )
{
	switch( event.event )
	{
	case eCGE_OnShoot:
		{
			SActorStats *pStats = GetActorStats();
			if (pStats)
				pStats->inFiring = 10.0f;
		}
		break;
	case eCGE_Ragdollize:
		{
			const bool bSleep = event.paramAsBool;
			if( gEnv->bServer )
			{
				const EActorPhysicalization actorPhys = bSleep ? eAP_Sleep : eAP_Ragdoll;
				GetGameObject()->SetAspectProfile(eEA_Physics, actorPhys);
			}
			else
			{
				RagDollize( bSleep );
			}

			if (m_pAnimatedCharacter)
				m_pAnimatedCharacter->DisableAICollider();
		}
		break;
	case eGFE_OnCollision:
	{
							 EventPhysCollision *physCollision = reinterpret_cast<EventPhysCollision *>(event.ptr);
							 OnCollisionEvent(physCollision);
	}
		break;
	case eGFE_EnableBlendRagdoll:
		{
			const HitInfo& hitInfo = *static_cast<const HitInfo*>(event.ptr);

			OnFall(hitInfo);
			if (m_pAnimatedCharacter)
				m_pAnimatedCharacter->DisableAICollider();
		}
		break;
	case eGFE_DisableBlendRagdoll:
		StandUp();
		if(m_pAnimatedCharacter && !IsDead())
			m_pAnimatedCharacter->EnableAICollider(0.30f);

		break;
	case eCGE_EnablePhysicalCollider:
		{
			assert(m_pAnimatedCharacter);
			m_pAnimatedCharacter->RequestPhysicalColliderMode(eColliderMode_Undefined, eColliderModeLayer_Game, "Actor::HandleEvent");
			if (m_pAnimatedCharacter && !IsDead())
				m_pAnimatedCharacter->EnableAICollider(0.30f);
		}
		break;
	case eCGE_DisablePhysicalCollider:
		{
			assert(m_pAnimatedCharacter);
			m_pAnimatedCharacter->RequestPhysicalColliderMode(eColliderMode_Disabled, eColliderModeLayer_Game, "Actor::HandleEvent");
			m_pAnimatedCharacter->DisableAICollider();
		}
		break;
	case eGFE_BecomeLocalPlayer:
		{
			CryLog ("%s '%s' is becoming local actor", GetEntity()->GetClass()->GetName(), GetEntity()->GetName());
			INDENT_LOG_DURING_SCOPE();


			m_isClient = true;

			SetupLocalPlayer();
		}
		break;
	case eGFE_RagdollPhysicalized:
		{
			// Let the actor impulse handler know 
			if(m_pImpulseHandler)
			{
				m_pImpulseHandler->OnRagdollPhysicalized(); 
			}

			if (old_gravity_z != 0.0f)
			{
				if (old_gravity_z > 0.0f)
					old_gravity_z = -old_gravity_z;
				else
					old_gravity_z = old_gravity_z - (old_gravity_z * 2);

				CryLogAlways("Actor have setuped gravity z, eCGE_Ragdollize");
				SetGravityZ(old_gravity_z);
			}

			if (m_pAnimatedCharacter)
				m_pAnimatedCharacter->DisableAICollider();
		}
		break;
	}
}

void CActor::ResetAnimationState()
{
	SetStance(STANCE_RELAXED);
}

void CActor::QueueAnimationState( const char * state )
{
	if (m_pAnimatedCharacter)
		m_pAnimatedCharacter->PushForcedState( state );
}


Vec3 CActor::GetLocalEyePos() const
{
	if (HasBoneID(BONE_CAMERA))
	{
		Vec3 camera = GetBoneTransform(BONE_CAMERA).t;
		if (camera.IsValid())
			return camera;
	}

	if (HasBoneID(BONE_EYE_R) && HasBoneID(BONE_EYE_L))
	{
		Vec3 reye = GetBoneTransform(BONE_EYE_R).t;
		Vec3 leye = GetBoneTransform(BONE_EYE_L).t;
		if (reye.IsValid() && leye.IsValid())
			return (reye+leye)*0.5f;
	}

	return GetStanceInfo(m_stance)->viewOffset;
}

QuatT CActor::GetCameraTran() const
{
	if (HasBoneID(BONE_CAMERA))
	{
		QuatT camera = GetBoneTransform(BONE_CAMERA);
		CRY_ASSERT(camera.IsValid());

		return camera;
	}

	CRY_ASSERT(GetSpectatorState() == eASS_SpectatorMode);
	QuatT returnQuatT;
	returnQuatT.SetIdentity();
	return returnQuatT;
}

QuatT CActor::GetHUDTran() const
{
	if (HasBoneID(BONE_HUD))
	{
		QuatT hud = GetBoneTransform(BONE_HUD);
		CRY_ASSERT(hud.IsValid());

		return hud;
	}

	return GetCameraTran();
}

bool CActor::CheckInventoryRestrictions(const char *itemClassName)
{
	if (g_pGameCVars->g_inventoryNoLimits != 0)
		return true;

	if (m_pInventory)
		return m_pInventory->IsAvailableSlotForItemClass(itemClassName);


	return false;
}

//IK stuff
void CActor::SetIKPos(const char *pLimbName, const Vec3& goalPos, int priority)
{
	int limbID = GetIKLimbIndex(pLimbName);
	if (limbID > -1)
	{
		Vec3 pos(goalPos);
		m_IKLimbs[limbID].SetWPos(GetEntity(),pos,ZERO,0.5f,0.5f,priority);
	}
}

void CActor::CreateIKLimb(const SActorIKLimbInfo &limbInfo)
{
	for (size_t i=0;i<m_IKLimbs.size();++i)
	{
		if (!limbInfo.sLimbName.compare(m_IKLimbs[i].name))
			return;
	}

	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(limbInfo.characterSlot);
	if (pCharacter)
	{
		SIKLimb newLimb;
		newLimb.SetLimb(limbInfo.characterSlot, 
			limbInfo.sLimbName.c_str(), 
			pCharacter->GetIDefaultSkeleton().GetJointIDByName(limbInfo.sRootBone.c_str()), 
			pCharacter->GetIDefaultSkeleton().GetJointIDByName(limbInfo.sMidBone.c_str()), 
			pCharacter->GetIDefaultSkeleton().GetJointIDByName(limbInfo.sEndBone.c_str()), 
			limbInfo.flags);

		if (newLimb.endBoneID>-1 && newLimb.rootBoneID>-1)
			m_IKLimbs.push_back(newLimb);
	}
}

int CActor::GetIKLimbIndex(const char *limbName)
{
	for (size_t i=0;i<m_IKLimbs.size();++i)
		if (!strcmp(limbName,m_IKLimbs[i].name))
			return i;

	return -1;
}

void CActor::ProcessIKLimbs(float frameTime)
{
	IEntity *pEntity = GetEntity();

	//first thing: restore the original animation pose if there was some IK
	//FIXME: it could get some optimization.
	for (size_t i=0;i<m_IKLimbs.size();++i)
		m_IKLimbs[i].Update(pEntity,frameTime);
}

IAnimationGraphState * CActor::GetAnimationGraphState()
{
	if (m_pAnimatedCharacter)
		return m_pAnimatedCharacter->GetAnimationGraphState();
	else
		return NULL;
}

void CActor::SetFacialAlertnessLevel(int alertness)
{
	if (m_pAnimatedCharacter)
		m_pAnimatedCharacter->SetFacialAlertnessLevel(alertness);
}

//------------------------------------------------------------------------
void CActor::SwitchToWeaponWithAccessoryFireMode()
{
	if (!m_pInventory)
		return;

	if (m_pInventory->GetCount() < 1)
		return;

	int startSlot = -1;
	EntityId currentItemId;

	currentItemId = m_pInventory->GetCurrentItem();
	
	if (currentItemId)
	{
		startSlot = m_pInventory->FindItem(currentItemId);
	}

	int currentSlot = startSlot;
	int skip = m_pInventory->GetCount(); // maximum number of interactions
	
	while(skip)
	{
		int slot = currentSlot + 1;

		if (slot >= m_pInventory->GetCount())
			slot = 0;

		if (startSlot == slot)
			return;

		EntityId itemId = m_pInventory->GetItem(slot);
		CWeapon* pWeapon = GetWeapon(itemId);
		
		if (pWeapon && pWeapon->CanSelect())
		{
			int numFiremodes = pWeapon->GetNumOfFireModes();
			int accessoryEnabledMode = -1;

			for(int i = 0; i < numFiremodes && accessoryEnabledMode < 0; i++)
			{
				CFireMode* pFiremode = static_cast<CFireMode*>(pWeapon->GetFireMode(i));

				if(pFiremode && pFiremode->IsEnabledByAccessory())
				{
					accessoryEnabledMode = i;
				}
			}

			if(accessoryEnabledMode >= 0)
			{
				if(pWeapon->GetCurrentFireMode() != accessoryEnabledMode)
				{
					static_cast<CWeapon*>(pWeapon)->RequestFireMode(accessoryEnabledMode);
				}
				
				ScheduleItemSwitch(itemId, true, eICT_Primary);

				const EWeaponSwitchSpecialParam specialParam = eWSSP_None;
				const EItemCategoryType category = eICT_Primary;

				SHUDEventWrapper::OnPrepareItemSelected(itemId, category, specialParam );

				return;
			}
		}

		currentSlot = slot;
		--skip;
	}
}

//------------------------------------------------------------------------
void CActor::SelectNextItem(int direction, bool keepHistory, int category)
{
	if (!m_pInventory)
		return;

	if (m_pInventory->GetCount() < 1)
		return;

	int startSlot = -1;
	int delta = direction;
	EntityId currentItemId;
	
	SActorStats* pActorStats = GetActorStats();
	if (!pActorStats)
		return;

	// If in progress of switching, use the item in progress as current item to allow fast switching
	bool bCurrentItemIsSwitchedItem = false;
	if (pActorStats->exchangeItemStats.switchingToItemID != 0)
	{
		IItem* pSwitchingItem = GetItem(pActorStats->exchangeItemStats.switchingToItemID);
		if (pSwitchingItem != NULL)
		{
			IEntityClass* pEntityClass = pSwitchingItem->GetEntity()->GetClass();
			CRY_ASSERT(pEntityClass != NULL);
			const char* switchingItemCategory = m_pItemSystem->GetItemCategory(pEntityClass->GetName());
			int switchingItemCategoryType = GetItemCategoryType(switchingItemCategory);
			if (switchingItemCategoryType & eICT_Primary || switchingItemCategoryType & eICT_Secondary)
			{
				currentItemId = pActorStats->exchangeItemStats.switchingToItemID;
				bCurrentItemIsSwitchedItem = true;
			}
		}

		if (bCurrentItemIsSwitchedItem == false)
		{
			currentItemId = m_pInventory->GetCurrentItem();
		}
	}
	else
	{
		currentItemId = m_pInventory->GetCurrentItem();
	}

	IItem* pCurrentItem = m_pItemSystem->GetItem(currentItemId);

	bool currWeaponExplosive = false; 

	IInventory::EInventorySlots inventorySlot = IInventory::eInventorySlot_Last;
	
	if (pCurrentItem)
	{
		if (const char* const currentWeaponCat = m_pItemSystem->GetItemCategory(pCurrentItem->GetEntity()->GetClass()->GetName()))
		{
			inventorySlot = m_pInventory->GetSlotForItemCategory(currentWeaponCat);
			currWeaponExplosive = (inventorySlot == IInventory::eInventorySlot_Explosives) || (inventorySlot == IInventory::eInventorySlot_Grenades);
		}
	}

	if(	inventorySlot != IInventory::eInventorySlot_Last 
			&& inventorySlot != IInventory::eInventorySlot_Weapon 
			&& (category & eICT_Primary || category & eICT_Secondary)
			&& !(category & eICT_Explosive || category & eICT_Grenade))
	{
		EntityId lastSelectedItem = m_pInventory->GetLastSelectedInSlot(IInventory::eInventorySlot_Weapon);
		startSlot = m_pInventory->FindItem(lastSelectedItem) - delta;
	}
	else if (currentItemId)
	{
		startSlot = m_pInventory->FindItem(currentItemId);
	}

	EntityId itemId = ComputeNextItem(startSlot, category, delta, keepHistory, pCurrentItem, currWeaponExplosive);

	if(itemId)
	{
		if(IsClient())
		{
			EWeaponSwitchSpecialParam specialParam = eWSSP_None;
			if(category == eICT_None)
			{
				specialParam = (direction>0) ? eWSSP_Next : eWSSP_Prev;
			}

			SHUDEventWrapper::OnPrepareItemSelected(itemId, (EItemCategoryType)category, specialParam );
		}

		ScheduleItemSwitch(itemId, keepHistory, category);
	}
}

//------------------------------------------------------------------------
EntityId CActor::ComputeNextItem(const int startSlot, const int category, const int delta, bool& inOutKeepHistory, IItem* pCurrentItem, const bool currWeaponExplosive) const
{
	int slot = startSlot;
	int skip = m_pInventory->GetCount(); // maximum number of interactions
	
	while(skip)
	{
		slot += delta;

		if (slot<0)
			slot = m_pInventory->GetCount()-1;
		else if (slot >= m_pInventory->GetCount())
			slot = 0;

		EntityId itemId = m_pInventory->GetItem(slot);
		IItem *pItem = m_pItemSystem->GetItem(itemId);
		CRY_ASSERT(pItem);
		if (pItem)
		{
			const char* name = pItem->GetEntity()->GetClass()->GetName();
			const char *nextWeaponCatStr = m_pItemSystem->GetItemCategory(name);
			int nextWeaponCat = GetItemCategoryType(nextWeaponCatStr);
			bool isDiferentItemFromCurrent = pCurrentItem ? pItem->GetEntity()->GetClass() != pCurrentItem->GetEntity()->GetClass() : true;
			if (pItem->CanSelect() && (category == eICT_None || category & nextWeaponCat) && isDiferentItemFromCurrent)
			{
				CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(GetEntityId()));
				if (pActor)
				{
					if (!pActor->EquipItem(itemId))
					{
						--skip;
						continue;
					}
				}

				IInventory::EInventorySlots nextWeaponSlot = nextWeaponCat != eICT_None ? m_pInventory->GetSlotForItemCategory(nextWeaponCatStr) : IInventory::eInventorySlot_Last;
				if (currWeaponExplosive && ((nextWeaponSlot == IInventory::eInventorySlot_Explosives) || (nextWeaponSlot == IInventory::eInventorySlot_Grenades)))
				{
					//don't keep the history when switching from explosive to an explosive as the switch back after wants to go to the last primary/secondary weapon
					inOutKeepHistory = false;
				}
				return itemId;

				}
		}

		--skip;
	}
	return 0;
}

//------------------------------------------------------------------------
CItem *CActor::GetItem(EntityId itemId) const
{
	return static_cast<CItem*>(m_pItemSystem->GetItem(itemId));
}

//------------------------------------------------------------------------
CItem *CActor::GetItemByClass(IEntityClass* pClass) const
{
	if (m_pInventory)
	{
		return static_cast<CItem *>(m_pItemSystem->GetItem(m_pInventory->GetItemByClass(pClass)));
	}

	return 0;
}

//------------------------------------------------------------------------
CWeapon *CActor::GetWeapon(EntityId itemId) const
{
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(itemId));
	if (pItem)
		return static_cast<CWeapon *>(pItem->GetIWeapon());

	return 0;
}

//------------------------------------------------------------------------
CWeapon *CActor::GetWeaponByClass(IEntityClass* pClass) const
{
	CItem* pItem = GetItemByClass(pClass);
	if (pItem)
		return static_cast<CWeapon *>(pItem->GetIWeapon());
	return 0;
}

//------------------------------------------------------------------------
void CActor::SelectLastItem(bool keepHistory, bool forceNext /* = false */)
{
	if (!m_pInventory)
		return;

	EntityId itemId = m_pInventory->GetLastItem();
	IItem *pItem = m_pItemSystem->GetItem(itemId);

	if (pItem && itemId != GetCurrentItemId(false))
	{
		IWeapon* pWeapon = pItem->GetIWeapon();

		if(pWeapon && pWeapon->OutOfAmmo(false))
		{
			SelectWeaponWithAmmo(itemId, keepHistory);
		}
		else
		{
			ScheduleItemSwitch(pItem->GetEntityId(), keepHistory);
		}
	}
	else if(forceNext)
	{
		SelectNextItem(1,keepHistory,eICT_Primary|eICT_Secondary); //Prevent binoculars to get stuck under certain circumstances
	}
}

//-----------------------------------------------------------------------
void CActor::SelectWeaponWithAmmo(EntityId currentOutOfAmmoId, bool keepHistory)
{
	EntityId bestOutOfAmmoId = 0;
	EntityId secondaryWithAmmoId = 0;

	if (m_pInventory)
	{
		ItemString primaryCat = "primary";
		ItemString secondaryCat = "secondary";

		int numItems = m_pInventory->GetCount();
		EntityId currentItemId = m_pInventory->GetCurrentItem();

		for(int i = 0; i < numItems; i++)
		{
			EntityId itemId = m_pInventory->GetItem(i);

			if(itemId != currentItemId)
			{
				IItem* pItem = m_pItemSystem->GetItem(itemId);
				CRY_ASSERT(pItem);

				if(pItem && pItem->CanSelect())
				{
					const char* name = pItem->GetEntity()->GetClass()->GetName();
					ItemString category = m_pItemSystem->GetItemCategory(name);

					if(category == primaryCat || category == secondaryCat)
					{
						IWeapon* pWeapon = pItem->GetIWeapon();

						if(pWeapon && !pWeapon->OutOfAmmo(false))
						{
							if(category == primaryCat)
							{
								ScheduleItemSwitch(itemId, keepHistory);
								return;
							}
							else if(!secondaryWithAmmoId)
							{
								secondaryWithAmmoId = itemId;
							}
						}
						else
						{
							if((category == primaryCat) && (!bestOutOfAmmoId || currentOutOfAmmoId == itemId))
							{
								bestOutOfAmmoId = itemId;
							}
						}
					}
				}
			}
		}
	}

	EntityId useId = secondaryWithAmmoId;
	
	if(!useId)
	{
		useId = bestOutOfAmmoId ? bestOutOfAmmoId : currentOutOfAmmoId;
	}

	ScheduleItemSwitch(useId, keepHistory);
}

void CActor::ClearItemActionControllers()
{
	if(IItem* pCurrentItem = GetCurrentItem(true))
	{
		pCurrentItem->SetCurrentActionController(NULL);
	}

	if(SActorStats* pStats = GetActorStats())
	{
		if(IItem* pMountedGun = m_pItemSystem->GetItem(pStats->mountedWeaponID))
		{
			pMountedGun->SetCurrentActionController(NULL);
		}
	}

	if(m_pInventory)
	{
		const int numItems = m_pInventory->GetCount();
		for(int i = 0; i<numItems; i++)
		{
			if(IItem* pItem = m_pItemSystem->GetItem(m_pInventory->GetItem(i)))
			{
				pItem->SetCurrentActionController(NULL);
			}
		}

		// This could be a weapon NOT in your inventory list.
		if(IItem* pHolsteredItem = m_pItemSystem->GetItem(m_pInventory->GetHolsteredItem()))
		{
			pHolsteredItem->SetCurrentActionController(NULL);
		}

		// This could be a weapon NOT in your inventory list.
		if(IItem* pCurrentInvItem = m_pItemSystem->GetItem(m_pInventory->GetCurrentItem()))
		{
			pCurrentInvItem->SetCurrentActionController(NULL);
		}
	}
}

void CActor::AttemptToRecycleAIActor()
{
	const bool removalAllowed = EntityScripts::CallScriptFunction( GetEntity(), GetEntity()->GetScriptTable(), "AIWaveAllowsRemoval" );

	if (removalAllowed)
	{
		if (gEnv->IsEditor())
		{
			EntityScripts::CallScriptFunction( GetEntity(), GetEntity()->GetScriptTable(), "ShutDown" );
		}
		else if (GetEntity()->IsFromPool())
		{
			gEnv->pEntitySystem->GetIEntityPoolManager()->ReturnToPool( GetEntityId() );
		}
		else
		{
			SetHealth( 0.0f );
			IEntityPhysicalProxy *pPhysicsProxy = (IEntityPhysicalProxy*)GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS);
			if (pPhysicsProxy)
			{
				SEntityPhysicalizeParams params;
				params.type = PE_NONE;
				pPhysicsProxy->Physicalize(params);
			}
			gEnv->pEntitySystem->RemoveEntity( GetEntityId() );
		}
	}
	else
	{
		// Attempt later again
		GetEntity()->SetTimer( RECYCLE_AI_ACTOR_TIMER_ID, 2000 );
	}
}

//-----------------------------------------------------------------------
bool CActor::ScheduleItemSwitch(EntityId itemId, bool keepHistory, int category, bool forceFastSelect)
{
	//CryLogAlways(" CActor::ScheduleItemSwitch called");
	//-----Temp fix for relaxed mode------------------------------
	if (GetRelaxedMod())
	{
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Weapon);
		if (itemId == GetCurrentItemId())
		{
			if (itemId == GetNoWeaponId())
			{
				//CryLogAlways(" CActor::ScheduleItemSwitch f2");
				CItem *pItemiii = GetItem(ide);
				if (pItemiii)
					pItemiii->SetEquipped(0);

				CItem *pItemii = GetItem(itemId);
				pItemii->SetEquipped(1);
				SetEquippedItemId(eAESlot_Weapon, itemId);
			}
			return false;
		}

		if (itemId != GetNoWeaponId())
		{
			if (IsItemInActorInventory(itemId))
			{
				if (ide == itemId)
				{
					CItem *pItemiii = GetItem(ide);
					if (pItemiii)
						pItemiii->SetEquipped(0);

					CItem *pItemii = GetItem(GetNoWeaponId());
					pItemii->SetEquipped(1);
					SetEquippedItemId(eAESlot_Weapon, GetNoWeaponId());
					return false;
				}
				//CryLogAlways(" CActor::ScheduleItemSwitch f3");
				CItem *pItemiii = GetItem(ide);
				if (pItemiii)
					pItemiii->SetEquipped(0);

				CItem *pItemii = GetItem(itemId);
				pItemii->SetEquipped(1);
				SetEquippedItemId(eAESlot_Weapon, itemId);
			}
			return false;
		}
	}
	//------------------------------------------------------------
	CItem* pCurrentItem = NULL;

	SActorStats* pActorStats = GetActorStats();
	if (!pActorStats)
	{
		return false;
	}

	// If already switching, current item is the item being switched to
	if (pActorStats->exchangeItemStats.switchingToItemID != 0)
	{
		// Only allow fast switching for primary and secondary weapons
		// current item is checked in CPlayerInput::OnAction
		if (category & eICT_Primary || category & eICT_Secondary)
		{
			pCurrentItem = GetItem(pActorStats->exchangeItemStats.switchingToItemID);
			if (pCurrentItem != NULL)
			{
				IEntityClass* pEntityClass = pCurrentItem->GetEntity()->GetClass();
				CRY_ASSERT(pEntityClass != NULL);
				const char* switchingItemCategory = m_pItemSystem->GetItemCategory(pEntityClass->GetName());
				int switchingItemCategoryType = GetItemCategoryType(switchingItemCategory);
				if (switchingItemCategoryType & eICT_Primary || switchingItemCategoryType & eICT_Secondary)
				{
					// Possible item switch timer already set up if just started deselecting, disable it from firing
					CancelScheduledSwitch();

					SelectItem(itemId, keepHistory, true);

					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		pCurrentItem = static_cast<CItem*>(GetCurrentItem());
	}

	bool fastDeselect = forceFastSelect;
	const bool isMounted = (pCurrentItem && (pCurrentItem->IsMounted() || pCurrentItem->IsRippingOrRippedOff()));

	if (pCurrentItem && pCurrentItem->HasFastSelect(itemId))
	{
		fastDeselect = true;
	}
	
	if (!isMounted && (gEnv->IsEditing() == false))
	{
		//If it's the same item we have, just ignore it
		if (pCurrentItem && pCurrentItem->GetEntityId() == itemId)
		{
			return false;
		}

		//If already switching, ignore
		if (pActorStats->exchangeItemStats.switchingToItemID != 0)
		{
			return false;
		}

		IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
		if (pClass && GetRelaxedMod())
		{
			if (m_pInventory)
			{
				EntityId itemId_no_wpn = m_pInventory->GetItemByClass(pClass);
				if (itemId_no_wpn != itemId)
					fastDeselect = true;
			}
		}

		uint32 deselectDelay = pCurrentItem ? pCurrentItem->StartDeselection(fastDeselect) : 0;

		if(deselectDelay > 0)
		{
			pActorStats->exchangeItemStats.switchingToItemID = itemId;
			pActorStats->exchangeItemStats.keepHistory = keepHistory;

			GetEntity()->SetTimer(ITEM_SWITCH_THIS_FRAME, deselectDelay);

			return true;
		}
	}
	
	SelectItem(itemId, keepHistory, false);

	return false;
}

//------------------------------------------------------------------------
void CActor::SelectItemByName(const char *name, bool keepHistory, bool forceFastSelect)
{
	if (!m_pInventory)
		return;

	if (m_pInventory->GetCount() < 1)
		return;

	IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(name);
	EntityId itemId = m_pInventory->GetItemByClass(pClass);
	IItem *pItem = m_pItemSystem->GetItem(itemId);

	if (pItem)
	{
		ScheduleItemSwitch(itemId, keepHistory, 0, forceFastSelect);
	}
}

//------------------------------------------------------------------------
void CActor::SelectItem(EntityId itemId, bool keepHistory, bool forceSelect)
{
	if (IItem * pItem = m_pItemSystem->GetItem(itemId))
	{
		if (!m_pInventory)
			return;

		if (m_pInventory->GetCount() < 1)
			return;

		if (m_pInventory->FindItem(itemId) < 0)
		{
			GameWarning("Trying to select an item which is not in %s's inventory!", GetEntity()->GetName());
			return;
		}

		if (pItem->GetEntityId() == m_pInventory->GetCurrentItem() && !forceSelect)	
			return;

		if(pItem->GetEntityId() == m_pInventory->GetHolsteredItem()) //unholster selected weapon
			m_pInventory->HolsterItem(false);

		/*if (IsPlayer() && GetRelaxedMod())
		{
			CPlayer *pPl = (CPlayer *)this;
			if (pPl)
			{
				CPlayerInput *pPlayerInput = reinterpret_cast<CPlayerInput *>(pPl->GetPlayerInput());
				if (pPlayerInput)
				{
					if (pPlayerInput->m_rel_mode_timer == 0.0f)
					{
						if(pPlayerInput->m_old_sel_itmn != itemId)
							pPlayerInput->m_old_sel_itmn = itemId;
						else
							pPlayerInput->m_old_sel_itmn = GetNoWeaponId();

						CHUDCommon* pHud = g_pGame->GetHUDCommon();
						if (pHud)
						{
							if (pHud->Inv_loaded)
							{
								pHud->UpdateInventory(1);
								pHud->delayed_update_player_doll_timer -= 1.0f;
							}
						}
						return;
					}
				}
			}
		}*/

		if (IsPlayer() && GetRelaxedMod())
		{
			m_pItemSystem->SetActorItem(this, pItem->GetEntityId(), keepHistory);
			CHUDCommon* pHud = g_pGame->GetHUDCommon();
			if (pHud)
			{
				if (pHud->Inv_loaded)
				{
					pHud->UpdateInventory(1);
					pHud->delayed_update_player_doll_timer -= 1.0f;
				}
			}
			return;
		}
			
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Weapon);
		CItem *pItemiii = GetItem(ide);
		if (pItemiii)
			pItemiii->SetEquipped(0);

		CItem *pItemii = GetItem(itemId);
		pItemii->SetEquipped(1);
		int id = itemId;
		SetEquippedItemId(eAESlot_Weapon, id);
		m_pItemSystem->SetActorItem(this, pItem->GetEntityId(), keepHistory);

		if (IsPlayer())
		{
			CHUDCommon* pHud = g_pGame->GetHUDCommon();
			if (pHud)
			{
				if (pHud->Inv_loaded)
				{
					pHud->UpdateInventory(1);
					pHud->delayed_update_player_doll_timer -= 1.0f;
				}
			}
			return;
		}
		//}

		SendPhyAndAiUpdateEvent();

		if (gEnv->bMultiplayer && !IsRemote())
		{
			//CPlayer *pPl = (CPlayer *)this;
			//if (pPl)
				//CHANGED_NETWORK_STATE(pPl, CPlayer::ASPECT_RPG_INFO);
		}

		CItem *pItemiiiy = GetItem(itemId);
		CItem *pItemiiiiy = GetItem(this->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows));
		if (pItemiiiy && pItemiiiy->GetWeaponType() == 9 && pItemiiiiy)
		{
			int idr = pItemiiiy->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItemiiiy->GetIWeapon()->GetFireMode(idr);
			if (pFmd && pItemiiiiy->GetEquipped() == 1 && pItemiiiiy->GetItemQuanity()>0)
			{
				pFmd->SetAmmoType(pItemiiiiy->GetItemAmmoType());
				pFmd->Activate(false);
				pFmd->Activate(true);
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::DeSelectItem(EntityId itemId, bool keepHistory)
{
	CryLogAlways(" CActor::DeSelectItem called");
	SendPhyAndAiUpdateEvent();
	if (IItem * pItem = m_pItemSystem->GetItem(itemId))
	{
		IInventory *pInventory = GetInventory();
		if (!pInventory)
			return;

		if (pInventory->GetCount() < 1)
			return;

		if (pInventory->FindItem(itemId) < 0)
		{
			return;
		}

		if (pItem->GetEntityId() == pInventory->GetHolsteredItem()) //unholster selected weapon
			pInventory->HolsterItem(false);



		//m_pItemSystem->SetActorItem(this, "Fists");
		CItem *pItemii = GetItem(itemId);
		
		pItemii->SetEquipped(0);

		IEntityClass *pNoWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
		EntityId noWeaponId = GetInventory()->GetItemByClass(pNoWeaponClass);
		if (pItemii->GetEntityId() == noWeaponId && GetRelaxedMod())
		{
			//pItemii->SetEquipped(0);
			//this->SetEquippedItemId(eAESlot_Weapon, noWeaponId);
			//return;
		}

		//SelectItem(noWeaponId, false, true);
		ScheduleItemSwitch(noWeaponId, true);

		CItem *pItemiii = GetItem(itemId);
		CItem *pItemiiii = GetItem(this->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows));
		if (pItemiii && pItemiii->GetWeaponType() == 9)
		{
			int idr = pItemiii->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItemiii->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				pFmd->SetAmmoType("noarrow");
			}
		}
	}
}
bool CActor::IsItemInActorInventory(EntityId itemId)
{
	IInventory *pInventory = GetInventory();
	if (!pInventory)
		return false;

	CItem *pItem = GetItem(itemId);
	if (!pItem)
		return false;

	if (pInventory->FindItem(itemId) > -1)
		return true;

	return false;
}
//------------------------------------------------------------------------
EntityId CActor::GetLeftHandObject() const
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (pCharacter)
	{
		IAttachmentManager* pAttachMan = pCharacter->GetIAttachmentManager();
		IAttachment* pLeftAttachment = pAttachMan->GetInterfaceByName(g_pItemStrings->left_item_attachment.c_str());
		if (pLeftAttachment && pLeftAttachment->GetIAttachmentObject() && (pLeftAttachment->GetIAttachmentObject()->GetAttachmentType() == IAttachmentObject::eAttachment_Entity))
		{
			CEntityAttachment *pEntityAttachment = (CEntityAttachment *) pLeftAttachment->GetIAttachmentObject();
			return pEntityAttachment->GetEntityId();
		}
	}

	return 0;
}

void CActor::HideLeftHandObject(bool inHide)
{
	EntityId leftHandEnt = GetLeftHandObject();
	if (leftHandEnt)
	{
		IEntity *pEnt = gEnv->pEntitySystem->GetEntity(leftHandEnt);
		if (pEnt)
		{
			pEnt->Hide(inHide);
		}
	}
}

//------------------------------------------------------------------------
void CActor::HolsterItem(bool holster, bool playSelect, float selectSpeedBias, bool hideLeftHandObject)
{
	if (!m_pInventory)
		return;

	if(holster)
	{
		if(hideLeftHandObject)
		{	
			if(!GetGrabbedEntityId())
			{
				HideLeftHandObject(true);
			}
			else
			{
				HolsterItem(true,true,1.0f,false);
			}
		}
	}
	else
	{
		HideLeftHandObject(false);
	}

	if(holster)
	{
		SActorStats* pActorStats = GetActorStats();

		//if interrupting a scheduled switch select the new item immediately to ensure correct selection upon unholster 
		if (pActorStats && pActorStats->exchangeItemStats.switchingToItemID != 0)
		{
			IEntity* pSwitchEntity = gEnv->pEntitySystem->GetEntity(pActorStats->exchangeItemStats.switchingToItemID);

			if(pSwitchEntity && pSwitchEntity->GetClass() != CItem::sBinocularsClass)
			{
				SelectItem(pActorStats->exchangeItemStats.switchingToItemID, pActorStats->exchangeItemStats.keepHistory, false);
			}

			CancelScheduledSwitch();
		}
	}

	if (!holster)
	{
		CItem* pHolsteredItem = GetItem(m_pInventory->GetHolsteredItem());
		if (pHolsteredItem)
			pHolsteredItem->Unholstering(playSelect, selectSpeedBias);
	};

	m_pInventory->HolsterItem(holster);

	if(holster && (GetEntityId() == g_pGame->GetIGameFramework()->GetClientActorId()))
	{
		SHUDEventWrapper::FireModeChanged(NULL, 0);
	}

	if (!holster)
	{
		const EntityId item_to_reselect = GetCurrentEquippedItemId(eAESlot_Weapon);
		IEntityClass *pNoWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
		EntityId noWeaponId = GetInventory()->GetItemByClass(pNoWeaponClass);
		if (item_to_reselect == noWeaponId)
			return;

		if (GetCurrentItemId() == noWeaponId)
			return;

		SelectItem(noWeaponId, false, true);
		ScheduleItemSwitch(item_to_reselect, false);
	}
}

//------------------------------------------------------------------------
bool CActor::UseItem(EntityId itemId)
{
	if (m_health.IsDead())
		return false;

	CItem *pItem=GetItem(itemId);
	bool canBeUsed = pItem != NULL;
	
	CryLog ("UseItem: %s %s wants to use %s %s %s", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), pItem ? pItem->GetEntity()->GetClass()->GetName() : "NULL", pItem ? pItem->GetEntity()->GetName() : "item", canBeUsed ? "which is fine!" : "but it's not allowed!");

	if (!canBeUsed)
		return false;

	CancelScheduledSwitch();

	if (gEnv->bServer || (pItem->GetEntity()->GetFlags()&(ENTITY_FLAG_CLIENT_ONLY|ENTITY_FLAG_SERVER_ONLY)))
		pItem->Use(GetEntityId());
	else
	{
		if(!m_bAwaitingServerUseResponse)
		{
			GetGameObject()->InvokeRMI(SvRequestUseItem(), ItemIdParam(itemId), eRMI_ToServer);
			SetStillWaitingOnServerUseResponse(true);
		}
	}

	return true;
}

//------------------------------------------------------------------------
bool CActor::PickUpItem(EntityId itemId, bool sound, bool select)
{
	CItem *pItem = static_cast<CItem*>(m_pItemSystem->GetItem(itemId));
	if (!pItem/* || m_health.IsDead()*/)
		return false;

	if (pItem->GetOwner() || pItem->GetEquipped()>0 || pItem->IsSelected())
		return false;

	if (gEnv->bServer || (pItem->GetEntity()->GetFlags()&(ENTITY_FLAG_CLIENT_ONLY|ENTITY_FLAG_SERVER_ONLY)))
	{
		pItem->PickUp(GetEntityId(), sound, select);
		m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemPickedUp, 0, 0, (void *)(EXPAND_PTR)pItem->GetEntityId()));
	}
	else 
	{
		if(!m_bAwaitingServerUseResponse)
		{
			GetGameObject()->InvokeRMI(SvRequestPickUpItem(), ItemIdParam(itemId, false, select), eRMI_ToServer);
			SetStillWaitingOnServerUseResponse(true);
		}
	}

	return true;
}

//------------------------------------------------------------------------
bool CActor::PickUpItemAmmo(EntityId itemId)
{
	CItem *pItem = static_cast<CItem*>(m_pItemSystem->GetItem(itemId));
	if (!pItem || m_health.IsDead())
		return false;

	if (gEnv->bServer || (pItem->GetEntity()->GetFlags()&(ENTITY_FLAG_CLIENT_ONLY|ENTITY_FLAG_SERVER_ONLY)))
	{
		pItem->PickUpAmmo(GetEntityId());

		CWeapon* pCurrWeapon = GetWeapon(GetCurrentItemId(false));
		if(pCurrWeapon && pCurrWeapon->OutOfAmmo(false) && pCurrWeapon->CanReload())
		{
			pCurrWeapon->Reload();
		}

		//m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemPickedUp, 0, 0, (void *)pItem->GetEntityId()));
	}
	else
	{
		if(!m_bAwaitingServerUseResponse)
		{
			GetGameObject()->InvokeRMI(SvRequestPickUpItem(), ItemIdParam(itemId, true), eRMI_ToServer);
			SetStillWaitingOnServerUseResponse(true);
		}
	}

	return true;
}

//------------------------------------------------------------------------
bool CActor::DropItem(EntityId itemId, float impulseScale, bool selectNext, bool bydeath)
{
	CItem *pItem = static_cast<CItem*>(m_pItemSystem->GetItem(itemId));
	if (!pItem)
		return false;

	EntityId wpn_ide = GetCurrentEquippedItemId(1);
	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	CItem* pItem_wpn = static_cast<CItem*>(pItemSystem->GetItem(wpn_ide));
	if (pItem_wpn)
	{
		if (pItem_wpn->GetEntity()->GetId() == pItem->GetEntity()->GetId())
		{
			SetEquippedItemId(eAESlot_Weapon, 0);
		}

		if (pItem_wpn->GetWeaponType() == 9)
		{
			int idr = pItem_wpn->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItem_wpn->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				pFmd->SetAmmoType("noarrow");
			}
		}
	}

	if (this->IsPlayer())
	{
		
	}

	if (pItem->GetEquipped() == 1)
	{
		EquipItem(pItem->GetEntityId());
		pItem->SetEquipped(0);
		CItem *pItemiii = GetItem(this->GetCurrentEquippedItemId(eAESlot_Weapon));
		CItem *pItemiiii = GetItem(itemId);
		if (pItemiii && pItemiii->GetWeaponType() == 9)
		{
			int idr = pItemiii->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItemiii->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				this->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
				pFmd->SetAmmoType("noarrow");
			}
		}
		if (pItem->GetEntityId() == GetCurrentEquippedItemId(eAESlot_Weapon_Left))
		{
			EquipWeaponToLeftHand(pItem->GetEntityId(), false);
		}
	}


	//Fix editor reseting issue
	//Player dies - Don't drop weapon
	//pItem->ShouldNotDrop() is only true when leaving the game mode into the editor (see EVENT_RESET in Item.cpp)
	if(IsClient() && gEnv->IsEditor() && (m_health.IsDead()||pItem->ShouldNotDrop()))
	{
		return false;
	}

	bool bOK = false;
	if (pItem->CanDrop())
	{
		bool performCloakFade = IsCloaked();

		if (gEnv->bServer)
		{
			pItem->Drop(impulseScale, selectNext, bydeath);

			if (!bydeath)
			{
				// send game event
				// check also if the dropped item was actually the slave (akimbo guns)
				m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)(itemId)));
			}
		}
		else
		{
			m_pendingDropEntityId = itemId;
			GetGameObject()->InvokeRMI(SvRequestDropItem(), DropItemParams(itemId, selectNext, bydeath), eRMI_ToServer);
		}

		if (performCloakFade)
		{
			pItem->CloakEnable(false, true);
		}

		bOK = true;
	}
	return bOK;
}

void CActor::ServerExchangeItem(CItem* pCurrentItem, CItem* pNewItem)
{
	if (pCurrentItem && pNewItem)
	{
		if (pNewItem->GetOwnerId() != 0)
		{
			CryLog("%s tried to exchange %s for %s, but the new item already has an owner %d", GetEntity()->GetName(), pCurrentItem->GetEntity()->GetName(), pNewItem->GetEntity()->GetName(), pNewItem->GetOwnerId());
			return;
		}

		IItemSystem* pItemSystem = gEnv->pGame->GetIGameFramework()->GetIItemSystem();
		CWeapon* pCurrentWeapon = static_cast<CWeapon*>(pCurrentItem->GetIWeapon());
		CWeapon* pNewWeapon = static_cast<CWeapon*>(pNewItem->GetIWeapon());
		IInventory* pInventory = GetInventory();

		DropItem(pCurrentItem->GetEntityId(), 1.0f, false, false);
		PickUpItem(pNewItem->GetEntityId(), true, true);

		if (pCurrentWeapon && pNewWeapon)
		{
			const CWeaponSharedParams* pCurrentWeaponParams = pCurrentWeapon->GetWeaponSharedParams();
			const CWeaponSharedParams* pNewWeaponParams = pNewWeapon->GetWeaponSharedParams();
			for (size_t i = 0; i < pCurrentWeaponParams->ammoParams.ammo.size(); ++i)
			{
				IEntityClass* pAmmoType = pCurrentWeaponParams->ammoParams.ammo[i].pAmmoClass;
				int bonusAmmo = pCurrentWeapon->GetBonusAmmoCount(pAmmoType);

				for (size_t j = 0; j < pNewWeaponParams->ammoParams.ammo.size(); ++j)
				{
					if (pNewWeaponParams->ammoParams.ammo[j].pAmmoClass != pAmmoType)
						continue;

					int currentCapacity = pInventory->GetAmmoCapacity(pAmmoType);
					int currentAmmo = pInventory->GetAmmoCount(pAmmoType);
					int newIventoryAmmount = min(currentAmmo + bonusAmmo, currentCapacity);
					int newDropAmmount = max((currentAmmo + bonusAmmo) - currentCapacity, 0);
					pInventory->SetAmmoCount(pAmmoType, newIventoryAmmount);
					pCurrentWeapon->SetBonusAmmoCount(pAmmoType, newDropAmmount);

					break;
				}
			}
		}
	}
}

//---------------------------------------------------------------------
void CActor::ExchangeItem(CItem* pCurrentItem, CItem* pNewItem)
{
	m_telemetry.OnExchangeItem(pCurrentItem, pNewItem);
	if(gEnv->bServer)
	{
		ServerExchangeItem(pCurrentItem, pNewItem);
	}
	else if(!m_bAwaitingServerUseResponse)
	{
		GetGameObject()->InvokeRMI(SvRequestExchangeItem(), ExchangeItemParams(pCurrentItem->GetEntityId(), pNewItem->GetEntityId()), eRMI_ToServer);
		SetStillWaitingOnServerUseResponse(true);
	}
}

//---------------------------------------------------------------------
void CActor::DropAttachedItems()
{
	//CryLogAlways("DropAttachedItems() called");
	bool drop_only_current_wpn = false;
	if (g_pGameCVars->g_dead_actors_dropallitems_after_death < 1)
		drop_only_current_wpn = true;
	//Drop weapons attached to the back
	//CryLogAlways("DropAttachedItems() called2");
	if (gEnv->bServer)
	{
		//CryLogAlways("DropAttachedItems() called3");
		if (drop_only_current_wpn)
		{
			//CryLogAlways("DropAttachedItems() called4");
			EntityId itemId = m_pInventory->GetCurrentItem();
			IItem *pItemc = GetCurrentItem();
			CItem* pItem = (CItem *)m_pItemSystem->GetItem(itemId);
			if (pItem)
			{
				//CryLogAlways("TryTodropMainitem");
				//temportary fix for bows(have no physics)
				//CryLogAlways("TryTodrop111bow111item");
				if (pItemc)
				{
					//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItemc->GetEntityId(), "Bow"));
					CBow *pBow = static_cast<CBow*>(pItemc);
					if (pBow && pItem->GetWeaponType() == 9)
					{
						//CryLogAlways("TryTodrop111bow111item");
						pBow->ForcedStopFire();
						return;
					}
				}
				//-----------------------------------------

				pItem->OnOwnerDeactivated();
				if (pItem->CanDrop())
				{
					pItem->Drop(1.0f, false, true);
					m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)itemId));
				}
			}
			//CryLogAlways("CActor::DropAttachedItems() drop_only_current_wpn CALLED");
			CItem* pItem_left = (CItem *)GetItem(GetAnyItemInLeftHand());
			if (pItem_left)
			{
				//CryLogAlways("CActor::DropAttachedItems() pItem_left is have");
				if (pItem_left->GetOwner())
				{
					//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner()");
					if (pItem_left->GetOwner()->GetId() == GetEntityId())
					{
						//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner() nrm");
						if (pItem_left->CanDrop())
						{
							//CryLogAlways("CActor::DropAttachedItems() pItem_left drop");
							DropItem(itemId, 0.1f, false, true);
							DropItem(pItem_left->GetEntityId());
						}
						pItem_left->OnOwnerDeactivated();
						m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)pItem_left->GetEntityId()));
					}
				}
			}
			else // bugbugbug
			{
				int totalItems = m_pInventory->GetCount();
				for (int i = 0; i < totalItems; i++)
				{
					itemId = m_pInventory->GetItem(i);
					pItem_left = (CItem *)m_pItemSystem->GetItem(itemId);
					if (pItem_left)
					{
						if (pItem_left->GetEquipped() > 0)
						{
							if (pItem_left->GetShield() > 0)
							{
								if (pItem_left->GetOwner())
								{
									//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner()");
									if (pItem_left->GetOwner()->GetId() == GetEntityId())
									{
										//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner() nrm");
										if (pItem_left->CanDrop())
										{
											//CryLogAlways("CActor::DropAttachedItems() pItem_left drop");
											DropItem(itemId, 0.1f, false, true);
										}
										pItem_left->OnOwnerDeactivated();
										m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)itemId));
										continue;
									}
								}
							}
							else if (pItem_left->GetItemType() == 4)
							{
								if (pItem_left->GetOwner())
								{
									//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner()");
									if (pItem_left->GetOwner()->GetId() == GetEntityId())
									{
										//CryLogAlways("CActor::DropAttachedItems() pItem_left GetOwner() nrm");
										if (pItem_left->CanDrop())
										{
											//CryLogAlways("CActor::DropAttachedItems() pItem_left drop");
											DropItem(itemId, 0.1f, false, true);
										}
										pItem_left->OnOwnerDeactivated();
										m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)itemId));
										continue;
									}
								}
							}
						}
					}
				}
			}
			return;
		}
		const int maxNumItems = 16;
		EntityId itemsTodrop[maxNumItems];
		int numItems = 0;
		int totalItems = m_pInventory->GetCount();

		for (int i=0; i < totalItems; i++)
		{
			EntityId itemId = m_pInventory->GetItem(i);
			CItem* pItem = (CItem *)m_pItemSystem->GetItem(itemId);
			if (pItem)
			{
				pItem->OnOwnerDeactivated();

				if (pItem->CanDrop())
				{
					itemsTodrop[numItems++] = itemId;
					if (numItems >= maxNumItems)
						break;
				}
			}
		}

		for (int i = 0; i < numItems; i++)
		{
			EntityId entId = itemsTodrop[i];
			CItem* item = (CItem *)m_pItemSystem->GetItem(entId);
			item->Drop(1.0f,false,true);
			m_pGameplayRecorder->Event(GetEntity(), GameplayEvent(eGE_ItemDropped, 0, 0, (void *)(EXPAND_PTR)entId));
		}
	}
}

//------------------------------------------------------------------------
IItem *CActor::GetCurrentItem(bool includeVehicle/*=false*/) const
{
  if (EntityId itemId = GetCurrentItemId(includeVehicle))
	  return m_pItemSystem->GetItem(itemId);

  return 0;
}

//------------------------------------------------------------------------
EntityId CActor::GetCurrentItemId(bool includeVehicle/*=false*/) const
{
	if (includeVehicle)
	{
		if (IVehicle* pVehicle = GetLinkedVehicle())
		{
			if (EntityId itemId = pVehicle->GetCurrentWeaponId(GetEntityId()))
				return itemId;
		}
	}
  
	if (m_pInventory)
		return m_pInventory->GetCurrentItem();

	return 0;
}

//------------------------------------------------------------------------
IItem *CActor::GetHolsteredItem() const
{
	if (m_pInventory)
	{
		return m_pItemSystem->GetItem(m_pInventory->GetHolsteredItem());
	}

	return NULL;
}

//------------------------------------------------------------------------
EntityId CActor::GetHolsteredItemId() const
{
	if (m_pInventory)
	{
		return m_pInventory->GetHolsteredItem();
	}

	return 0;
}

//------------------------------------------------------------------------
IInventory* CActor::GetInventory() const
{
	return m_pInventory;
}

//------------------------------------------------------------------------
EntityId CActor::NetGetCurrentItem() const
{
	if (m_pInventory)
		return m_pInventory->GetCurrentItem();

	return 0;
}

//------------------------------------------------------------------------
void CActor::NetSetCurrentItem(EntityId id, bool forceDeselect)
{	
	if (id == 0 || forceDeselect)
	{
		if(!GetGrabbedEntityId())
		{
			HolsterItem(true);
		}
		else
		{
			HolsterItem(true,true,1.0f,false); 
		}
	}
	else
	{
		if (GetHolsteredItemId() == id)
		{
			HolsterItem(false);
		}
		else
		{
			const CWeapon* pWeapon = GetWeapon(id);
			const bool force = id == GetCurrentItemId(false) && pWeapon && pWeapon->IsDeselecting();

			static const IEntityClass* pPickAndThrowWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("PickAndThrowWeapon");
			const bool keepHistory = pWeapon && pWeapon->GetEntity()->GetClass() == pPickAndThrowWeaponClass;

			SelectItem(id, keepHistory, force);
		}
	}
}

//------------------------------------------------------------------------
EntityId CActor::NetGetScheduledItem() const
{
	return GetActorStats()->exchangeItemStats.switchingToItemID;
}

//------------------------------------------------------------------------
void CActor::NetSetScheduledItem(EntityId id)
{	
	
	SActorStats* pStats = GetActorStats();

	if(id && id != pStats->exchangeItemStats.switchingToItemID)
	{
		CItem* pCurrentItem = static_cast<CItem*>(GetCurrentItem());
		if(pCurrentItem)
		{
			bool fastDeselect = pCurrentItem->HasFastSelect(id);
			pCurrentItem->StartDeselection(fastDeselect);
		}	
	}

	pStats->exchangeItemStats.switchingToItemID = id;
}


void CActor::PostUpdate(float frameTime)
{
	//actors postupdating optimization & ets...
	if (start_update_timer_xc < 50.0f)
	{
		if(frameTime < 0.05f)
			start_update_timer_xc += frameTime;
		else
			start_update_timer_xc += 0.05f;

		if (GetEntity())
		{
			if (GetEntity()->IsHidden())
				return;
		}
	}

	//if(gEnv->bMultiplayer && IsRemote())
	//	CryLogAlways("CActor::PostUpdate called in multiplayer mode by remote actor");

	CGameRules *pGameRules = g_pGame->GetGameRules();
	//five relative seconds wait before start visbility tests
	if (start_update_timer_xc > 5.0f)
	{
		if (lazy_update_timer == 0.0f)
		{
			lazy_update_timer_next_ft = 1.0f + cry_random(0.0f, 0.33f);
			if (!block_updates)
			{
				if (GetGameObject()->IsProbablyDistant())
				{
					if (pGameRules)
					{
						if (pGameRules->GetCurrentGameTime() > 15.0f)
						{
							if (!IsPlayer())
							{
								if (g_pGameCVars->e_actors_updating_optimization > 0)
									block_updates = true;
							}
						}
					}
				}
				else
				{
					SendPhyAndAiUpdateEvent();
				}
			}
		}
		lazy_update_timer += frameTime;
		if (lazy_update_timer > lazy_update_timer_next_ft)
		{
			lazy_update_timer = 0.0f;
			if (block_updates)
			{
				if (GetGameObject()->IsProbablyVisible())
				{
					if (g_pGameCVars->e_actors_updating_optimization > 0)
						block_updates = false;
				}
			}
		}

		if (block_updates || block_updates_cn)
			return;
	}
	//temp fix-------------------------------------------------------------------------
	if (GetEntity())
	{
		if (GetEntity()->IsHidden())
			return;
	}
	//---------------------------------------------------------------------------------
	//------------------------------------------------------------------
	//----temportary game equipment system fix. remove it//don't touch this!!!
	if (!gEnv->IsEditor())
	{
		if (additional_pack_add_request)
		{
			AddAdditionalEquip();
		}

		if (additional_pack2_add_request)
		{
			AddAdditionalEquip2();
		}
	}
	//----------------------------------------------------
	//---------------New gravity update-------------------
	if (old_gravity_z != 0.0f)
	{
		SetGravityZ(old_gravity_z);
		if (GetActorStats()->isRagDoll)
		{
			
		}
	}
	//reloading of all skin attachments---------------------
	if (update_all_skin_attachment_at_next_frame > 0)
	{
		UpdateAllSkinAttachments();
	}
	//--------------------------------------------
	//update quivers on character-----------------
	if (update_quiver_attachment > 0)
	{
		update_quiver_attachment -= 1;
		if (update_quiver_attachment <= 0)
		{
			update_quiver_attachment = 0;
			UpdateQuivers();
		}
	}
	//update complex attachments and ets... 
	UpdateSkinAttachmentsAndEts(frameTime);
	//---------------------------------------------------------------------------------------------------------------------------------------------------------
	UpdateNonPlayerActorsFallingEvents(frameTime);
	//---------------------------------------------------------------------------------------------------------------------------------------------------------
	CItem *pItem_current = static_cast<CItem*>(GetCurrentItem());
	if (!pItem_current && !IsDead())
	{
		IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
		if (pClass && m_pInventory)
		{
			EntityId itemId_no_wpn = m_pInventory->GetItemByClass(pClass);
			if (itemId_no_wpn > 0)
			{
				if (m_pItemSystem)
					m_pItemSystem->SetActorItem(this, itemId_no_wpn, false);
			}
		}
	}
	//----------Special actions system update------------------------------------------------------------------------------------------------------------------
	UpdateSpecialActions(frameTime);
	//----------Update Additional Equipment Packs processes----------------------------------------------------------------------------------------------------
	UpdateAdditionalEquipmentPacksAdding(frameTime, 1);
	//---------Update new aim and aimpose system---------------------------------------------------------------------------------------------------------------
	UpdateAimProcess(frameTime);
	//---------Update actor target-----------------------------------------------------------------------------------------------------------------------------
	UpdateTargeting(frameTime);
	//----Delayed actor physicalization------------------------------------------------------------------------------------------------------------------------
	if (delayed_physicalization_request > 0.0f)
	{
		delayed_physicalization_request -= frameTime;
		if (delayed_physicalization_request <= 0.0f)
		{
			Physicalize();
			delayed_physicalization_request = 0.0f;
		}
	}
	//----------Update Additional Equipment Packs processes----------------------------------------------------------------------------------------------------
	UpdateAdditionalEquipmentPacksAdding(frameTime, 2);
	//update actor timed effects
	UpdateBuffs(frameTime);
	//call update to actor update script function
	UpdateActorScriptFnc(frameTime);
}

bool CActor::IsInMercyTime() const
{
	if (m_isPlayer == false)
	{
		return false;
	}
	else
	{
		bool isInMercyTime = false;
		const IAIObject* pAiObject = GetEntity()->GetAI();
		if (pAiObject != NULL)
		{
			const IAIActor* pAiActor = pAiObject->CastToIAIActor();
			CRY_ASSERT(pAiActor);
			isInMercyTime = pAiActor->IsLowHealthPauseActive();
		}

		return isInMercyTime;
	}
}


float CActor::GetBodyDamageMultiplier(const HitInfo &hitInfo) const
{
	float fMultiplier = 1.0f;

	const TBodyDamageProfileId bodyDamageProfileId = GetCurrentBodyDamageProfileId();
	if (bodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID)
	{
		CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
		CRY_ASSERT(pBodyDamageManager);

		fMultiplier = pBodyDamageManager->GetDamageMultiplier(bodyDamageProfileId, *GetEntity(), hitInfo);
	}

	return fMultiplier;
}

float CActor::GetBodyExplosionDamageMultiplier(const HitInfo &hitInfo) const
{
	float fMultiplier = 1.0f;

	const TBodyDamageProfileId bodyDamageProfileId = GetCurrentBodyDamageProfileId();
	if (bodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID)
	{
		CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
		CRY_ASSERT(pBodyDamageManager);

		fMultiplier = pBodyDamageManager->GetExplosionDamageMultiplier(bodyDamageProfileId, *GetEntity(), hitInfo);
	}

	return fMultiplier;
}

// ===========================================================================
//	Get the body damage part flags via a part ID.
//
//	In:		The part ID.
//	In:		The material ID.
//
//	Returns:	The corresponding body damage part flags or 0 if it could not 
//				be obtained (e.g.: the part ID was invalid).
//
uint32 CActor::GetBodyDamagePartFlags(const int partID, const int materialID) const
{
	const TBodyDamageProfileId bodyDamageProfileId = GetCurrentBodyDamageProfileId();
	if (bodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID)
	{
		CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
		CRY_ASSERT(pBodyDamageManager != NULL);

		HitInfo hitInfo;
		hitInfo.partId = partID;
		hitInfo.material = materialID;
		return pBodyDamageManager->GetPartFlags(bodyDamageProfileId, *GetEntity(), hitInfo);
	}

	return 0;
}


// ===========================================================================
// Get a body damage profile ID for a combination of body damage file names.
//
//  In:     The body damage file name (NULL will ignore).
//  In:     The body damage parts file name (NULL will ignore).
//
//  Returns:    A default exit code (in Lua: A body damage profile ID or
//              -1 on error).
//
TBodyDamageProfileId CActor::GetBodyDamageProfileID(
	const char* bodyDamageFileName, const char* bodyDamagePartsFileName) const
{
	CBodyDamageManager *bodyDamageManager = g_pGame->GetBodyDamageManager();
	CRY_ASSERT(bodyDamageManager != NULL);

	IEntity* entity = GetEntity();
	assert(entity != NULL);
	return bodyDamageManager->GetBodyDamage(*entity, bodyDamageFileName, bodyDamagePartsFileName);
}


// ===========================================================================
// Override the body damage profile ID.
//
//  In:     The ID of the damage profile that should become active or
//          INVALID_BODYDAMAGEPROFILEID if the default one should be used).
//
void CActor::OverrideBodyDamageProfileID(const TBodyDamageProfileId profileID)
{
	CBodyDamageManager *bodyDamageManager = g_pGame->GetBodyDamageManager();
	CRY_ASSERT(bodyDamageManager != NULL);

	if (m_OverrideBodyDamageProfileId != profileID)
	{
		m_OverrideBodyDamageProfileId = profileID;
		PhysicalizeBodyDamage();
	}
}

bool CActor::IsHeadShot(const HitInfo &hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_Headshot);
}

bool CActor::IsHelmetShot(const HitInfo & hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_Helmet);
}

bool CActor::IsGroinShot(const HitInfo &hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_Groin);
}

bool CActor::IsFootShot(const HitInfo &hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_Foot);
}

bool CActor::IsKneeShot(const HitInfo &hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_Knee);
}

bool CActor::IsWeakSpotShot(const HitInfo &hitInfo) const
{
	return IsBodyDamageFlag(hitInfo, eBodyDamage_PID_WeakSpot);
}


bool CActor::IsBodyDamageFlag(const HitInfo &hitInfo, const EBodyDamagePIDFlags flag) const
{
	bool bResult = false;

	const TBodyDamageProfileId bodyDamageProfileId = GetCurrentBodyDamageProfileId();
	if (bodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID)
	{
		CBodyDamageManager *pBodyDamageManager = g_pGame->GetBodyDamageManager();
		assert(pBodyDamageManager);

		bResult = ((pBodyDamageManager->GetPartFlags( bodyDamageProfileId, *GetEntity(), hitInfo ) & flag) == flag);
	}

	return bResult;
}

void CActor::ProcessDestructiblesHit( const HitInfo& hitInfo, const float previousHealth, const float newHealth )
{
	if (m_bodyDestructionInstance.GetProfileId() != INVALID_BODYDAMAGEPROFILEID)
	{
		g_pGame->GetBodyDamageManager()->ProcessDestructiblesHit(*GetEntity(), m_bodyDestructionInstance, hitInfo, previousHealth, newHealth);
	}
}

void CActor::ProcessDestructiblesOnExplosion( const HitInfo& hitInfo, const float previousHealth, const float newHealth )
{
	if (m_bodyDestructionInstance.GetProfileId() != INVALID_BODYDAMAGEPROFILEID)
	{
		g_pGame->GetBodyDamageManager()->ProcessDestructiblesOnExplosion(*GetEntity(), m_bodyDestructionInstance, hitInfo, previousHealth, newHealth);
	}
}

//------------------------------------------------------------------------

void CActor::SerializeLevelToLevel( TSerialize &ser )
{
	m_pInventory->SerializeInventoryForLevelChange(ser);
	if(ser.IsReading() && IsClient())
	{
		SHUDEvent	hudEvent_initLocalPlayer(eHUDEvent_OnInitLocalPlayer);
		hudEvent_initLocalPlayer.AddData(static_cast<int>(GetEntityId()));
		CHUDEventDispatcher::CallEvent(hudEvent_initLocalPlayer);
	}
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, SvRequestDropItem)
{
	IItem *pItem = GetItem(params.itemId);
	if (!pItem)
	{
		GameWarning("[gamenet] Failed to drop item. Item not found!");
		return false;
	}

	//CryLogAlways("%s::SvRequestDropItem(%s)", GetEntity()->GetName(), pItem->GetEntity()->GetName());

	pItem->Drop(1.0f, params.selectNext, params.byDeath);

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, SvRequestPickUpItem)
{
	bool bProcessedRequest = false;
	CItem *pItem = static_cast<CItem*>(GetItem(params.itemId));

	if (!pItem)
	{
		// this may occur if the item has been deleted but the client has not yet been informed
		GameWarning("[gamenet] Failed to pickup item. Item not found!");
		bProcessedRequest = true;
	}

	if(!bProcessedRequest)
	{
		if (m_health.IsDead())
		{
			bProcessedRequest = true;
		}

		if(!bProcessedRequest)
		{
			EntityId ownerId = pItem->GetOwnerId();
			if (!ownerId)
			{
				if (params.pickOnlyAmmo)
				{
					pItem->PickUpAmmo(GetEntityId());

					CWeapon* pCurrWeapon = GetWeapon(GetCurrentItemId(false));
					if(pCurrWeapon && pCurrWeapon->OutOfAmmo(false) && pCurrWeapon->CanReload())
					{
						pCurrWeapon->Reload();
						pCurrWeapon->RequestReload(); //Required to propagate out to clients
					}
				}
				else
				{	
					pItem->PickUp(GetEntityId(), true, params.select);
				}
			}
		}
	}

	// Tell the client their use request has now been processed
	if(!IsClient())
	{
		GetGameObject()->InvokeRMI(CActor::ClUseRequestProcessed(), CActor::NoParams(), eRMI_ToClientChannel, GetChannelId());
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, SvRequestExchangeItem)
{
	CItem* pDropItem = GetItem(params.dropItemId);
	CItem* pPickupItem = GetItem(params.pickUpItemId);

	ServerExchangeItem(pDropItem, pPickupItem);

	// Tell the client their use request has now been processed
	if(!IsClient())
	{
		GetGameObject()->InvokeRMI(CActor::ClUseRequestProcessed(), CActor::NoParams(), eRMI_ToClientChannel, GetChannelId());
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, SvRequestUseItem)
{
	UseItem(params.itemId);

	// Tell the client their use request has now been processed
	if(!IsClient())
	{
		GetGameObject()->InvokeRMI(CActor::ClUseRequestProcessed(), CActor::NoParams(), eRMI_ToClientChannel, GetChannelId());
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClUseRequestProcessed)
{
	CRY_ASSERT(m_bAwaitingServerUseResponse);

	SetStillWaitingOnServerUseResponse(false);

	return true;
}

//------------------------------------------------------------------------
CItem * CActor::StartRevive(int teamId)
{
	// stop using any mounted weapons before reviving
	CItem *pItem=static_cast<CItem *>(GetCurrentItem());
	if (pItem)
	{
		if (pItem->IsMounted())
		{
			pItem->StopUse(GetEntityId());
			pItem=0;
		}
	}

	if (!IsMigrating())
	{
		SetHealth(GetMaxHealth());
	}

	m_teamId = teamId;

	return pItem;
}

//------------------------------------------------------------------------
void CActor::FinishRevive(CItem * pItem)
{
	// This will cover the case when the ClPickup RMI comes in before we're revived
	if (m_netLastSelectablePickedUp)
	{
		pItem=static_cast<CItem *>(m_pItemSystem->GetItem(m_netLastSelectablePickedUp));
	}

	m_netLastSelectablePickedUp=0;

	if (pItem)
	{
		bool soundEnabled=pItem->IsSoundEnabled();
		pItem->EnableSound(false);
		pItem->Select(false);
		pItem->EnableSound(soundEnabled);

		m_pItemSystem->SetActorItem(this, (EntityId)0);
		SelectItem(pItem->GetEntityId(), true, false);
	}

	if (IsClient())
	{
		SupressViewBlending(); // no view blending when respawning // CActor::Revive resets it.

		if (gEnv->bMultiplayer)
		{
			if (CEquipmentLoadout *pEquipmentLoadout = g_pGame->GetEquipmentLoadout())
			{
				pEquipmentLoadout->ClAssignLastSentEquipmentLoadout(this);
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::NetReviveAt(const Vec3 &pos, const Quat &rot, int teamId, uint8 modelIndex)
{
	CryLog("CActor::NetReviveAt: %s", GetEntity()->GetName());
	INDENT_LOG_DURING_SCOPE();

#if ENABLE_STATOSCOPE
	if(gEnv->pStatoscope)
	{
		CryFixedStringT<128> buffer;
		buffer.Format("CActor::NetReviveAt: %s", GetEntity()->GetName());
		gEnv->pStatoscope->AddUserMarker("Actor", buffer.c_str());
	}
#endif // ENABLE_STATOSCOPE

	if (IVehicle *pVehicle=GetLinkedVehicle())
	{
		if (IVehicleSeat *pSeat=pVehicle->GetSeatForPassenger(GetEntityId()))
			pSeat->Exit(false);
	}

	CItem * currentItem = StartRevive(teamId);

	CGameRules * pGameRules = g_pGame->GetGameRules();

	pGameRules->OnRevive(this);

	GetEntity()->SetWorldTM(Matrix34::Create(Vec3(1,1,1), rot, pos));

	SetModelIndex(modelIndex);
	Revive(kRFR_Spawn);

	BATTLECHATTER(BC_Spawn, GetEntityId());

	pGameRules->EntityRevived_NotifyListeners(GetEntityId());

	if(gEnv->bServer)
	{
		m_netPhysCounter = (m_netPhysCounter + 1)&(PHYSICS_COUNTER_MAX - 1);
	}

	FinishRevive(currentItem);

	if (IsClient())
	{
		g_pGame->GetGameRules()->OnLocalPlayerRevived();
	}

	CActorManager::GetActorManager()->ActorRevived(this);
}

//------------------------------------------------------------------------
void CActor::NetReviveInVehicle(EntityId vehicleId, int seatId, int teamId)
{
	CItem * currentItem = StartRevive(teamId);
	
	g_pGame->GetGameRules()->OnReviveInVehicle(this, vehicleId, seatId, m_teamId);

	Revive(kRFR_Spawn);

	// fix our physicalization, since it's need for some vehicle stuff, and it will be set correctly before the end of the frame
	// make sure we are alive, for when we transition from ragdoll to linked...
	if (!GetEntity()->GetPhysics() || GetEntity()->GetPhysics()->GetType()!=PE_LIVING)
		Physicalize();

	IVehicle *pVehicle=m_pGameFramework->GetIVehicleSystem()->GetVehicle(vehicleId);
	assert(pVehicle);
	if(pVehicle)
	{
		IVehicleSeat *pSeat=pVehicle->GetSeatById(seatId);
		if (pSeat && (!pSeat->GetPassenger() || pSeat->GetPassenger()==GetEntityId()))
			pSeat->Enter(GetEntityId(), false);
	}

	FinishRevive(currentItem);
}

void CActor::FillHitInfoFromKillParams(const CActor::KillParams& killParams, HitInfo &hitInfo) const
{
	hitInfo.type = killParams.hit_type;
	hitInfo.damage = killParams.damage;
	hitInfo.partId = killParams.hit_joint;
	hitInfo.dir = killParams.dir;
	hitInfo.material = killParams.material;
	hitInfo.projectileId = killParams.projectileId;
	hitInfo.shooterId = killParams.shooterId;
	hitInfo.weaponClassId = killParams.weaponClassId;
	hitInfo.projectileClassId = killParams.projectileClassId;
	hitInfo.targetId = killParams.targetId;
	hitInfo.weaponId = killParams.weaponId;
	hitInfo.penetrationCount = killParams.penetration;
	hitInfo.hitViaProxy = killParams.killViaProxy;
	hitInfo.impulseScale			= killParams.impulseScale; 
	hitInfo.forceLocalKill = killParams.forceLocalKill; 

	// Get some needed parameters on the HitInfo structure
	// This means no DeathReaction has been processed
	char projectileClassName[128];
	if (g_pGame->GetIGameFramework()->GetNetworkSafeClassName(projectileClassName, sizeof(projectileClassName), killParams.projectileClassId))
	{
		IEntityClass* pProjectileClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(projectileClassName);
		const SAmmoParams *pAmmoParams = g_pGame->GetWeaponSystem()->GetAmmoParams(pProjectileClass);
		hitInfo.bulletType = pAmmoParams ? pAmmoParams->bulletType : 1;
	}

	ICharacterInstance* pCharacter = NULL;
	if ((hitInfo.partId != uint16(-1)) && (pCharacter = GetEntity()->GetCharacter(0)))
	{
		const int FIRST_ATTACHMENT_PARTID = 1000;

		// perhaps it's an attachment?
		if (hitInfo.partId < FIRST_ATTACHMENT_PARTID)
		{
			hitInfo.pos = GetEntity()->GetWorldTM().TransformPoint(pCharacter->GetISkeletonPose()->GetAbsJointByID(hitInfo.partId).t);
		}
		else
		{
			IAttachmentManager* pAttchmentManager = pCharacter->GetIAttachmentManager();
			IAttachment* pAttachment = pAttchmentManager->GetInterfaceByIndex(hitInfo.partId - FIRST_ATTACHMENT_PARTID);
			if (pAttachment)
			{
				hitInfo.pos = pAttachment->GetAttWorldAbsolute().t;
			}
		}
	}
}

//------------------------------------------------------------------------
void CActor::NetKill(const KillParams &killParams)
{
	HitInfo hitInfo;
	FillHitInfoFromKillParams(killParams, hitInfo);

#if ENABLE_STATOSCOPE
	if(gEnv->pStatoscope)
	{
		const char* shooter = "unknown";
		const char* target = "unknown";
		if (gEnv->pEntitySystem)
		{
			IEntity* pShooter = gEnv->pEntitySystem->GetEntity(killParams.shooterId);
			if (pShooter)
			{
				shooter = pShooter->GetName();
			}
			IEntity* pTarget = gEnv->pEntitySystem->GetEntity(killParams.targetId);
			if (pTarget)
			{
				target = pTarget->GetName();
			}
		}

		CryFixedStringT<128> buffer;
		buffer.Format("CActor::NetKill: %s killed %s", shooter, target);
		gEnv->pStatoscope->AddUserMarker("Actor", buffer.c_str());
		CryLog("%s", buffer.c_str());
	}
#endif // ENABLE_STATOSCOPE

	if (killParams.ragdoll)
	{
		CProceduralContextRagdoll* pRagdollContext = NULL;
		if( GetRagdollContext( &pRagdollContext ) )
		{
			pRagdollContext->SetEntityTarget( killParams.targetId );
		}

		GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Ragdoll);

		// If we have no specific impulse override fallback to using old default impulse method
		if(killParams.impulseScale <= 0.0f && !gEnv->bMultiplayer && (killParams.hit_type == CGameRules::EHitType::Collision) )
		{
			pe_action_impulse impulse;
			impulse.partid = hitInfo.partId;
			impulse.point = hitInfo.pos;
			impulse.impulse = 1000.f*hitInfo.dir;
			m_pImpulseHandler->SetOnRagdollPhysicalizedImpulse(impulse);
		}
	}

	g_pGame->GetGameRules()->OnKill(this, hitInfo, killParams.winningKill, killParams.firstKill, killParams.bulletTimeReplay);

	m_netLastSelectablePickedUp=0;

	Kill();

	// Once killed, if override impulse specified and if we werent the killer(who has already applied an impulse), force ragdoll and apply.
	// killParams.ragdoll will only be true if no hit death reaction is currently active. 
	if(killParams.ragdoll && killParams.impulseScale > 0.0f && (hitInfo.shooterId != gEnv->pGame->GetIGameFramework()->GetClientActorId()))
	{
		ForceRagdollizeAndApplyImpulse(hitInfo); 
	}

	UnRegisterInAutoAimManager();
	UnRegisterDBAGroups();

	g_pGame->GetGameRules()->OnKillMessage(GetEntityId(), killParams.shooterId);
}

//------------------------------------------------------------------------
void CActor::ForceRagdollizeAndApplyImpulse(const HitInfo& hitInfo)
{
	IEntity* pEntity = GetEntity(); 
	if(IsDead() && pEntity && hitInfo.partId)
	{
		pe_action_impulse peImpulse;
		peImpulse.impulse    = (hitInfo.dir * hitInfo.impulseScale);
		peImpulse.point      = hitInfo.pos;
		peImpulse.partid     = hitInfo.partId;
		peImpulse.iApplyTime = 0; // Apply immediately

		RagDollize(false);
		IPhysicalEntity* pPhysics = GetEntity()->GetPhysics();
		m_pImpulseHandler->SetOnRagdollPhysicalizedImpulse( peImpulse );

		CRecordingSystem *pRecordingSystem = g_pGame->GetRecordingSystem();
		if (pRecordingSystem)
		{
			pRecordingSystem->OnForcedRagdollAndImpulse( pEntity->GetId(), peImpulse.impulse, hitInfo.pos, hitInfo.partId, 0 );
		}
	}
}

//------------------------------------------------------------------------
void CActor::NetSimpleKill()
{
	CryLog("CActor::NetSimpleKill() '%s", GetEntity()->GetName());
	if (gEnv->bMultiplayer && IsPlayer())
	{
		if (!IsThirdPerson() && g_pGameCVars->pl_switchTPOnKill)
		{
			ToggleThirdPerson();
		}
	}

	Kill();
}

//-----------------------------------------------------------------------
bool CActor::CanRagDollize() const
{
	const SActorStats *pStats = GetActorStats();
	if(!gEnv->bMultiplayer && !m_isClient && pStats && pStats->isGrabbed)
		return false;

	return true;

}
//------------------------------------------------------------------------
void CActor::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
	GetInternalMemoryUsage(s);
	
}

void CActor::GetInternalMemoryUsage(ICrySizer * s) const
{
	s->AddObject(m_pInventory);
	s->AddObject(m_pAnimatedCharacter);	
	s->AddObject(m_pMovementController);
	s->AddObject(m_pActorSpellsActions);
	s->AddContainer(m_IKLimbs);	
	s->AddContainer(m_actor_spells_book);
	//s->AddContainer(Current_actor_spells);

}
//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClRevive)
{
	IGameRulesSpawningModule *pSpawningModule = g_pGame->GetGameRules()->GetSpawningModule();
	EntityId spawnEntity = pSpawningModule->GetNthSpawnLocation(params.spawnPointIdx); 
	IEntity *pSpawnPoint = gEnv->pEntitySystem->GetEntity(spawnEntity);
	
	if (pSpawnPoint == NULL)
	{
		// If we don't know about this entity, something has gone wrong with the spawn point indexing.
		// Returning false will cause a disconnect but it's better than a crash!
		CryLog("CActor::ClRevive() Unable to find spawn point index %d (num known spawns=%d), disconnecting", params.spawnPointIdx, pSpawningModule->GetSpawnLocationCount());
		return false;
	}

	pSpawningModule->SetLastSpawn(GetEntityId(), pSpawnPoint->GetId());

	Vec3 pos = pSpawnPoint->GetWorldPos();
	Quat rot = Quat::CreateRotationXYZ(pSpawnPoint->GetWorldAngles());

	CryLog("CActor::ClRevive(), Actor: '%s' - Spawning at EntityID: %d, '%s', class '%s', position: %.2f %.2f %.2f\n",
					GetEntity()->GetName(), pSpawnPoint->GetId(), pSpawnPoint->GetName(), pSpawnPoint->GetClass()->GetName(), pos.x, pos.y, pos.z);

	NetReviveAt(pos, rot, params.teamId, params.modelIndex);

	IScriptTable *pEntityScript = pSpawnPoint->GetScriptTable();
	if (pEntityScript)
	{
		HSCRIPTFUNCTION spawnedFunc(NULL);
		if (GetEntity()->GetScriptTable() && pEntityScript->GetValue("Spawned", spawnedFunc))
		{
			Script::Call(gEnv->pScriptSystem, spawnedFunc, pEntityScript, GetEntity()->GetScriptTable());
		}
	}
	
	m_netPhysCounter = params.physCounter;

	if (IsClient())
	{
		if (g_pGame->GetRecordingSystem())
		{
			g_pGame->GetRecordingSystem()->StartRecording();
		}
				
		g_pGame->GetUI()->ActivateDefaultState();

		if (GetSpectatorState()==CActor::eASS_None)
		{
			SetSpectatorState(CActor::eASS_Ingame);
		}

		if(IPhysicalEntity* pPhysics = GetEntity()->GetPhysics())
		{
			pe_action_awake paa;
			paa.bAwake = 1;
			pPhysics->Action(&paa);

			pe_player_dynamics pd;
			pd.gravity.z = -9.81f;
			pPhysics->SetParams(&pd);
		}
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClKill)
{
#if USE_LAGOMETER
	CLagOMeter* pLagOMeter = g_pGame->GetLagOMeter();
	if (pLagOMeter)
	{
		pLagOMeter->OnClientReceivedKill(params);
	}
#endif

	if (params.itemIdToDrop)
	{
		CItem *pItem=GetItem(params.itemIdToDrop);
		if (pItem)
		{
			pItem->Drop(1.f, false, true);
		}
	}

	NetKill(params);

	SkillKill::SetFirstBlood(params.firstKill);

	if (CGameRules *pGameRules=g_pGame->GetGameRules())
	{
		CryLog("CActor::ClKill - %d '%s' killed by %d '%s' (damage=%f type=%s)", params.targetId, GetEntity()->GetName(), params.shooterId, pGameRules->GetEntityName(params.shooterId), params.damage, pGameRules->GetHitType(params.hit_type));

		HitInfo dummyHitInfo;
		dummyHitInfo.shooterId = params.shooterId;
		dummyHitInfo.targetId = params.targetId;
		dummyHitInfo.weaponId = params.weaponId;
		dummyHitInfo.projectileId = params.projectileId;
		dummyHitInfo.weaponClassId = params.weaponClassId;
		dummyHitInfo.damage = params.damage;
		dummyHitInfo.material = params.material;
		dummyHitInfo.type = params.hit_type;
		dummyHitInfo.partId = params.hit_joint;
		dummyHitInfo.projectileClassId = params.projectileClassId;
		dummyHitInfo.penetrationCount = params.penetration;
		dummyHitInfo.dir = params.dir;
		dummyHitInfo.weaponId = params.weaponId;
		dummyHitInfo.impulseScale  = params.impulseScale;

		if(IEntity * pKilledEntity = gEnv->pEntitySystem->GetEntity(dummyHitInfo.targetId))
		{
			dummyHitInfo.pos = pKilledEntity->GetWorldPos();
		}
		
		pGameRules->OnEntityKilled(dummyHitInfo);
	}
	else
	{
		GameWarning("CActor::ClKill - %d '%s' killed while no game rules exist!", GetEntityId(), GetEntity()->GetName());
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClKillFPCamData)
{
	CRecordingSystem *crs = g_pGame->GetRecordingSystem();
	CRY_ASSERT(crs);
	if (crs)
	{
		crs->ClProcessKillCamData(this, params);
	}
	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, SvKillFPCamData)
{
	CRecordingSystem *crs = g_pGame->GetRecordingSystem();
	CRY_ASSERT(crs);
	if (crs)
	{
		crs->SvProcessKillCamData(this, params);
	}

	return true;
}
//RPG System fuctions sync for multiplayer mode
IMPLEMENT_RMI(CActor, SvEquipItem)
{
	GetGameObject()->InvokeRMI(CActor::ClEquipItem(), CActor::EquipItemParams(params.item_to_equip, params.need_equip, params.slot), eRMI_ToOtherClients, GetChannelId());
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		CItem *pItem = GetItem(params.item_to_equip);
		if (pItem)
		{
			if (params.need_equip && pItem->GetEquipped() > 0)
			{
				CryLogAlways("CActor::ClEquipItem called, pItem->GetEquipped() > 0 and need_equip == true");
				return true;
			}
		}
		if (Net_EquipItem(params.item_to_equip))
		{
			if (pItem && pItem->CanSelect())
			{
				if (params.need_equip && params.slot == eAESlot_Weapon_Left)
				{
					if (params.item_to_equip != GetCurrentEquippedItemId(eAESlot_Weapon_Left))
						EquipWeaponToLeftHand(params.item_to_equip, true);
				}

				if (params.item_to_equip == GetCurrentEquippedItemId(eAESlot_Weapon_Left))
				{
					if (!params.need_equip)
						EquipWeaponToLeftHand(params.item_to_equip, false);
				}
				else
				{
					if (params.slot == eAESlot_Weapon)
					{
						CItem *pItemii = pItem;
						if (!pItemii->IsSelected() && params.need_equip)
						{
							ScheduleItemSwitch(params.item_to_equip, true);
							m_lastNetItemId = params.item_to_equip;
						}
						else if (!params.need_equip)
						{
							DeSelectItem(params.item_to_equip, true);
						}
					}
				}
			}
		}
	}
	return true;
}

IMPLEMENT_RMI(CActor, ClEquipItem)
{
	CItem *pItem = GetItem(params.item_to_equip);
	if (pItem)
	{
		if (params.need_equip && pItem->GetEquipped() > 0)
		{
			CryLogAlways("CActor::ClEquipItem called, pItem->GetEquipped() > 0 and need_equip == true");
			return true;
		}
	}
	if (Net_EquipItem(params.item_to_equip))
	{
		if (pItem && pItem->CanSelect())
		{
			if (params.need_equip && params.slot == eAESlot_Weapon_Left)
			{
				if(params.item_to_equip != GetCurrentEquippedItemId(eAESlot_Weapon_Left))
					EquipWeaponToLeftHand(params.item_to_equip, true);
			}

			if (params.item_to_equip == GetCurrentEquippedItemId(eAESlot_Weapon_Left))
			{
				if(!params.need_equip)
					EquipWeaponToLeftHand(params.item_to_equip, false);
			}
			else
			{
				if (params.slot == eAESlot_Weapon)
				{
					CItem *pItemii = pItem;
					if (!pItemii->IsSelected() && params.need_equip)
					{
						ScheduleItemSwitch(params.item_to_equip, true);
						m_lastNetItemId = params.item_to_equip;
					}
					else if (!params.need_equip)
					{
						DeSelectItem(params.item_to_equip, true);
					}
				}
			}
		}
	}
	return true;
}

IMPLEMENT_RMI(CActor, SvAddBuff)
{
	GetGameObject()->InvokeRMI(CActor::ClAddBuff(), CActor::AcEffectParams(params.buff_id, params.buff_time, params.eff_id), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClAddBuff)
{
	Net_AddBuff(params.buff_id, params.buff_time, params.eff_id);
	return true;
}

IMPLEMENT_RMI(CActor, SvDelBuff)
{
	GetGameObject()->InvokeRMI(CActor::ClDelBuff(), CActor::AcEffectParams(params.buff_id, params.buff_time, params.eff_id), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClDelBuff)
{
	Net_DelBuff(params.buff_id);
	return true;
}

IMPLEMENT_RMI(CActor, SvUpdateActorBodyAndAttachments)
{
	Net_CheckActorBodyAndAllAttachments();
	return true;
}

IMPLEMENT_RMI(CActor, ClUpdateActorBodyAndAttachments)
{
	Net_CheckActorBodyAndAllAttachments();
	return true;
}
//additional equipment packs for multiplayer
IMPLEMENT_RMI(CActor, SvAddAdditionalEquip)
{
	GetGameObject()->InvokeRMI(CActor::ClAddAdditionalEquip(), NoParams(), eRMI_ToOtherClients, GetChannelId());
	return true;
}
IMPLEMENT_RMI(CActor, ClAddAdditionalEquip)
{
	AddAdditionalEquip();
	return true;
}
IMPLEMENT_RMI(CActor, SvAddAdditionalEquip2)
{
	GetGameObject()->InvokeRMI(CActor::ClAddAdditionalEquip2(), NoParams(), eRMI_ToOtherClients, GetChannelId());
	return true;
}
IMPLEMENT_RMI(CActor, ClAddAdditionalEquip2)
{
	AddAdditionalEquip2();
	return true;
}

IMPLEMENT_RMI(CActor, SvAddItemWParams)
{
	AddFromCharXMLItemParams ItemXc(params.item_class, params.item_quantity, params.selected);
	CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(
		gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(this, ItemXc.item_class,
			false, false, false)));
	if (curItem)
	{
		curItem->SetItemQuanity(ItemXc.item_quantity);
		if (ItemXc.selected)
		{
			if (curItem->CanSelect())
				SelectItem(curItem->GetEntityId(), false, false);

			EquipItem(curItem->GetEntityId());
			//GetGameObject()->InvokeRMI(CActor::ClEquipItem(), CActor::EquipItemParams(curItem->GetEntityId()), eRMI_ToClientChannel, GetChannelId());
		}
	}
	return true;
}

IMPLEMENT_RMI(CActor, SVCastSpell)
{
	GetGameObject()->InvokeRMI(CActor::ClCastSpell(), CActor::SNetCastSlotPrm(params.cast_slot, params.forced_slot), eRMI_ToOtherClients, GetChannelId());
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		if (m_pActorSpellsActions)
		{
			m_pActorSpellsActions->StartCast(params.cast_slot, params.forced_slot);
		}
	}
	return true;
}

IMPLEMENT_RMI(CActor, ClCastSpell)
{
	if (m_pActorSpellsActions)
	{
		//CryLogAlways("ClCastSpell called");
		//int spell_ids = GetSpellFromCurrentSpellsForUseSlot(params.cast_slot).spell_id;
		//CryLogAlways("spell_ids == %i", spell_ids);
		m_pActorSpellsActions->StartCast(params.cast_slot, params.forced_slot);
	}
	return true;
}

IMPLEMENT_RMI(CActor, SVEndCastSpell)
{
	GetGameObject()->InvokeRMI(CActor::ClEndCastSpell(), CActor::SNetEndCastSlotPrm(params.cast_slot, params.forced), eRMI_ToOtherClients, GetChannelId());
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		if (m_pActorSpellsActions)
		{
			m_pActorSpellsActions->RequestToPreEndCast(params.cast_slot, params.forced);
		}
	}
	return true;
}

IMPLEMENT_RMI(CActor, ClEndCastSpell)
{
	if (m_pActorSpellsActions)
	{
		m_pActorSpellsActions->RequestToPreEndCast(params.cast_slot, params.forced);
	}
	return true;
}

IMPLEMENT_RMI(CActor, SVUdateMgcSlotsInfo)
{
	GetGameObject()->InvokeRMI(CActor::ClUdateMgcSlotsInfo(), CActor::SNetMgcSlots(params.mgc_slot1, params.mgc_slot2), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClUdateMgcSlotsInfo)
{
	NET_AddSpellToCurrentSpellsForUseSlot(params.mgc_slot1, 1);
	NET_AddSpellToCurrentSpellsForUseSlot(params.mgc_slot2, 2);
	return true;
}

IMPLEMENT_RMI(CActor, SvSetBlockState)
{
	GetGameObject()->InvokeRMI(CActor::ClSetBlockState(), CActor::AcBlcAction(params.block_enable, params.block_type), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClSetBlockState)
{
	if (params.block_type == 1)
		m_blockactiveshield = params.block_enable;
	else if (params.block_type == 2)
		m_blockactivenoshield = params.block_enable;
	else if (params.block_type == 3)
	{
		m_blockactiveshield = params.block_enable;
		m_blockactivenoshield = params.block_enable;
	}
	return true;
}

//***********************************************************************************************
IMPLEMENT_RMI(CActor, SVPlayMQAction)
{
	GetGameObject()->InvokeRMI(CActor::ClPlayMQAction(), CActor::SNetPlayMQActionOnCurrentItem(params.tag_id, params.is_left_item), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClPlayMQAction)
{
	//CryLogAlways("ClPlayMQAction called s1");
	CItem *pItem = GetItem(m_lastNetItemId);
	if (pItem)
	{
		//CryLogAlways("ClPlayMQAction called s2, anim tag == %i, weapon name == %s", params.tag_id, pItem->GetEntity()->GetName());
		pItem->PlayAction(params.tag_id, 0, false, CItem::eIPAF_Default);
	}
	return true;
}

//***********************************************************************************************
IMPLEMENT_RMI(CActor, SVPlayMQAction2)
{
	GetGameObject()->InvokeRMI(CActor::ClPlayMQAction2(), CActor::SNetPlayMQActionOnCurrentItemWName(params.tag_name, params.is_left_item), eRMI_ToOtherClients, GetChannelId());
	return true;
}

IMPLEMENT_RMI(CActor, ClPlayMQAction2)
{
	//CryLogAlways("ClPlayMQAction2 called s1");
	CItem *pItem = GetItem(m_lastNetItemId);
	if (pItem)
	{
		const ItemString &MQAction = params.tag_name.c_str();
		FragmentID fragmentId = pItem->GetFragmentID(MQAction.c_str());
		//CryLogAlways("ClPlayMQAction2 called s2, anim fragmentId == %i, weapon name == %s, action name == %s", fragmentId, pItem->GetEntity()->GetName(), params.tag_name);
		IAction* pAction = pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//CryLogAlways("ClPlayMQAction2 called s3, action name == %s", pAction->GetName());
	}
	return true;
}

//***********************************************************************************************
IMPLEMENT_RMI(CActor, SvUpdateRPGInfo)
{
	CActor::SNvRPGInfoPack pRPGInfoPz;
	pRPGInfoPz.chrmodelstr = params.chrmodelstr;
	pRPGInfoPz.gender = params.gender;
	pRPGInfoPz.player_name = params.player_name;
	pRPGInfoPz.pleyestype = params.pleyestype;
	pRPGInfoPz.plhairtype = params.plhairtype;
	pRPGInfoPz.plheadtype = params.plheadtype;
	pRPGInfoPz.relaxed_mode = params.relaxed_mode;
	pRPGInfoPz.stealth_mode = params.stealth_mode;
	GetGameObject()->InvokeRMI(CActor::ClUpdateRPGInfo(), pRPGInfoPz, eRMI_ToOtherClients, GetChannelId());
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		m_gender = params.gender;
		m_chrmodelstr = params.chrmodelstr;
		CPlayer* pPlayer = (CPlayer *)this;
		if (pPlayer)
		{
			pPlayer->m_player_name = params.player_name;
		}
		m_plheadtype = params.plheadtype;
		m_plhairtype = params.plhairtype;
		m_pleyestype = params.pleyestype;
		m_stealth_mode = params.relaxed_mode;
		m_relaxed_mode = params.stealth_mode;
	}
	return true;
}

IMPLEMENT_RMI(CActor, ClUpdateRPGInfo)
{
	m_gender = params.gender;
	m_chrmodelstr = params.chrmodelstr;
	CPlayer* pPlayer = (CPlayer *)this;
	if (pPlayer)
	{
		pPlayer->m_player_name = params.player_name;
	}
	m_plheadtype = params.plheadtype;
	m_plhairtype = params.plhairtype;
	m_pleyestype = params.pleyestype;
	m_stealth_mode = params.relaxed_mode;
	m_relaxed_mode = params.stealth_mode;
	rload_pl_model_timer_after_ps = 0.001f;
	serialization_reinit_customization_setting_on_character = 0.01f;
	return true;
}


//-------------------------------------------------------------------------
//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClSimpleKill)
{
	NetSimpleKill();

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClMoveTo)
{
#ifndef DEMO_BUILD_RPG_SS
	Vec3 worldPos = GetEntity()->GetWorldPos();
	CryLog("ClMoveTo: %s '%s' moving from (%.2f %.2f %.2f) to (%.2f %.2f %.2f) while state=%s", GetEntity()->GetClass()->GetName(), GetEntity()->GetName(), worldPos.x, worldPos.y, worldPos.z, params.pos.x, params.pos.y, params.pos.z, GetActorStats()->isRagDoll ? ", ragdoll" : "");
#endif

	//teleport
	GetEntity()->SetWorldTM(Matrix34::Create(Vec3(1,1,1), params.rot, params.pos));
	OnTeleported();

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClPickUp)
{
	if (CItem *pItem = GetItem(params.itemId))
	{
		//CryLog("%s::ClPickUp %s", GetEntity()->GetName(), pItem->GetEntity()->GetName());

		if(params.pickOnlyAmmo)
		{
			pItem->PickUpAmmo(GetEntityId());
		}
		else
		{
			pItem->PickUp(GetEntityId(), params.sound, params.select);

			const char *displayName = pItem->GetDisplayName();

			if (params.select)
				m_netLastSelectablePickedUp=params.itemId;
		}
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClClearInventory)
{
	//CryLog("%s::ClClearInventory", GetEntity()->GetName());

	g_pGame->GetGameRules()->ClearInventory(this);

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClDrop)
{
	if (params.itemId == m_pendingDropEntityId)
	{
		m_pendingDropEntityId = 0;
	}

	CItem *pItem=GetItem(params.itemId);
	if (pItem)
	{
		pItem->Drop(1.0f, params.selectNext, params.byDeath);
	}
	else if(params.selectNext)
	{
		SelectNextItem(1, false);
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClStartUse)
{
	CItem *pItem=GetItem(params.itemId);
	if (pItem)
		pItem->StartUse(GetEntityId()); 

	if(IsClient())
	{
		SetStillWaitingOnServerUseResponse(false);
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClStopUse)
{
	CItem *pItem=GetItem(params.itemId);
	if (pItem)
	{
		pItem->StopUse(GetEntityId());

		//This is to guard against a scenario where we StartUse/StopUse item A, resulting in us swapping to
		//	item B in our inventory. Current item is a NetSerialize, StartUse/StopUse is an RMI, so they can arrive
		//	out of order. If they do and the StartUse/StopUse arrive after the NetSerialize, the user is left
		//	without a weapon. This uses the last NetSerialized weapon as the next item to select, if it was not
		//	the weapon we are currently stopping using (e.g. it's out of order) - Rich S
		if(!IsClient() && m_lastNetItemId != params.itemId)
			SelectItem(m_lastNetItemId, false, true);
	}

	return true;
}

//------------------------------------------------------------------------
IMPLEMENT_RMI(CActor, ClAssignWeaponAttachments)
{
	IGameFramework *pGameFramework = g_pGame->GetIGameFramework();
	IItemSystem *pItemSystem = pGameFramework->GetIItemSystem();
	int numAttachments = params.m_attachments.size();

	if( IsClient() )
	{
		if( CEquipmentLoadout* pLoadout = g_pGame->GetEquipmentLoadout() )
		{
			pLoadout->SpawnedWithLoadout( params.m_loadoutIdx );
		}
	}

	CryLog("%s::ClAssignWeaponAttachments numAttachments:%d", GetEntity()->GetName(), numAttachments);
	INDENT_LOG_DURING_SCOPE();

	for(int i=0; i < numAttachments; i++)
	{
		const AttachmentsParams::SWeaponAttachment &data = params.m_attachments[i];
		char className[256];
		if(pGameFramework->GetNetworkSafeClassName(className, sizeof(className), data.m_classId))
		{
			pItemSystem->GiveItem(this, className, false, data.m_default, true, NULL, EEntityFlags(ENTITY_FLAG_CLIENT_ONLY));
		}
	}

	return true;
}

//------------------------------------------------------------------------
const IItemParamsNode* CActor::GetActorParamsNode() const
{
	return g_pGame->GetIGameFramework()->GetIActorSystem()->GetActorParams(GetActorClassName());
}

//------------------------------------------------------------------------
const IItemParamsNode* CActor::GetEntityClassParamsNode() const
{
	return g_pGame->GetIGameFramework()->GetIActorSystem()->GetActorParams(GetEntityClassName());
}

//------------------------------------------------------------------------
bool CActor::IsPoolEntity() const
{
	// [*DavidR | 26/Oct/2010] ToDo: Find a fastest, better way (is getting the bookmarked's entity entityPoolId and the
	// default entity class from there and comparing with the current entity class worth it? probably equally slow)
	return (strcmp(GetEntity()->GetName(), "PoolEntity") == 0);
}

//------------------------------------------------------------------------
void CActor::DumpActorInfo()
{
  IEntity* pEntity = GetEntity();

  CryLog("ActorInfo for %s", pEntity->GetName());
  CryLog("=====================================");
  
  Vec3 entPos(pEntity->GetWorldPos());
  CryLog("Entity Pos: %.f %.f %.f", entPos.x, entPos.y, entPos.z);
  CryLog("Active: %i", pEntity->IsActive());
  CryLog("Hidden: %i", pEntity->IsHidden());
  CryLog("Invisible: %i", pEntity->IsInvisible());  
  CryLog("Profile: %i", m_currentPhysProfile);
  CryLog("Health: %8.2f", m_health.GetHealth());  
  
  if (IPhysicalEntity* pPhysics = pEntity->GetPhysics())
  { 
    CryLog("Physics type: %i", pPhysics->GetType());
    
    pe_status_pos physicsPos;
    if (pPhysics->GetStatus(&physicsPos))
    {
      CryLog("Physics pos: %.f %.f %.f", physicsPos.pos.x, physicsPos.pos.y, physicsPos.pos.z);
    }

    pe_status_dynamics dyn;
    if (pPhysics->GetStatus(&dyn))
    {   
      CryLog("Mass: %.1f", dyn.mass);
      CryLog("Vel: %.2f %.2f %.2f", dyn.v.x, dyn.v.y, dyn.v.z);
    } 
  }  

  if (IVehicle* pVehicle = GetLinkedVehicle())
  {
    CryLog("Vehicle: %s (destroyed: %i)", pVehicle->GetEntity()->GetName(), pVehicle->IsDestroyed());
    
    IVehicleSeat* pSeat = pVehicle->GetSeatForPassenger(GetEntityId());
    CryLog("Seat %i", pSeat ? pSeat->GetSeatId() : 0);
  }

  if (IItem* pItem = GetCurrentItem())
  {
    CryLog("Item: %s", pItem->GetEntity()->GetName());
  }


  CryLog("=====================================");
}

//
//-----------------------------------------------------------------------------
Vec3 CActor::GetWeaponOffsetWithLean(EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets) const
{
	//for player just do it the old way - from stance info
	if(IsPlayer())
	{
		return GetStanceInfo(stance)->GetWeaponOffsetWithLean(lean, peekOver);
	}
	else
	{
		return GetWeaponOffsetWithLeanForAI(GetWeapon(GetCurrentItemId()), stance, lean, peekOver, eyeOffset, useWhileLeanedOffsets);
	}
}

//-----------------------------------------------------------------------------
Vec3 CActor::GetWeaponOffsetWithLean(CWeapon* pCurrentWeapon, EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets) const
{
	//for player just do it the old way - from stance info
	if(IsPlayer())
	{
		return GetStanceInfo(stance)->GetWeaponOffsetWithLean(lean, peekOver);
	}
	else
	{
		return GetWeaponOffsetWithLeanForAI(pCurrentWeapon, stance, lean, peekOver, eyeOffset, useWhileLeanedOffsets);
	}
}

//-----------------------------------------------------------------------------
Vec3 CActor::GetWeaponOffsetWithLeanForAI(CWeapon* pCurrentWeapon, EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets) const
{
	if(pCurrentWeapon)
	{
		if(pCurrentWeapon->AIUseEyeOffset())
			return eyeOffset;

		Vec3	overrideOffset;
		if(pCurrentWeapon->AIUseOverrideOffset(stance, lean, peekOver, overrideOffset))
			return overrideOffset;
	}

	return GetStanceInfo(stance)->GetWeaponOffsetWithLean(lean, peekOver, useWhileLeanedOffsets);
}

//-------------------------------------------------------------------
//This function is called from Equipment Manager only for the client actor
void CActor::NotifyInventoryAmmoChange(IEntityClass* pAmmoClass, int amount)
{
	if(!pAmmoClass)
		return;
}

bool CActor::IsFriendlyEntity(EntityId entityId, bool bUsingAIIgnorePlayer/* = true*/) const
{
	IEntity *pEntity = gEnv->pEntitySystem->GetEntity(entityId);

	//Faster to not do an early out on entityId == actor->entityId
	if (!pEntity)
	{
		return true;
	}

	CGameRules *pGameRules = g_pGame->GetGameRules();
	if (!gEnv->bMultiplayer)
	{
		if (pEntity->GetAI())
		{
			return (!pEntity->GetAI()->IsHostile(GetEntity()->GetAI(), bUsingAIIgnorePlayer));
		}
	}
	else
	{
		if(pGameRules)
		{
			if (pGameRules->GetTeamCount()>=2)
			{
				int iMyTeam=pGameRules->GetTeam(GetEntityId());
				int iClientTeam = pGameRules->GetTeam(entityId);
				if (iClientTeam == 0 || iMyTeam == 0)
				{
					return true;	// entity or actor isn't on a team, hence friendly
				}
				else
				{
					return (iClientTeam==iMyTeam);
				}
			}
			else
			{
				return entityId == GetEntityId();
			}
		}

	}
	return false;
}

void CActor::AddViewAngleOffsetForFrame( const Ang3 &offset )
{

}

//------------------------------------------------------------------------
void CActor::EnableSwitchingItems( bool enable )
{
	m_enableSwitchingItems = enable;
}

//------------------------------------------------------------------------
void CActor::EnableIronSights( bool enable )
{
	m_enableIronSights = enable;
}

//************************************************************************

void CActor::SetGold(int gold)
{
	m_gold = gold;
}

void CActor::SetGender(int gender)
{
	m_gender = gender;
}

void CActor::SetStamina(int stamina)
{
	if (g_pGameCVars->g_stamina_sys_enable_consumption_for_ai_actors == 0)
	{
		if (!IsPlayer())
			return;
	}

	if (GetEntityId() == g_pGame->GetClientActorId())
	{
		if (g_pGameCVars->g_pl_disable_stamina_consumption > 0)
		{
			if (stamina < GetStamina())
				return;
		}
	}

	int prevStamina = (int)m_stamina;
	if (prevStamina > stamina)
	{
		streg_paused_time = 3.0f;
	}
	m_stamina = float(min(stamina, GetMaxStamina()));
}

void CActor::SetMagicka(int magicka)
{
	if (GetEntityId() == g_pGame->GetClientActorId())
	{
		if (g_pGameCVars->g_pl_disable_magicka_consumption > 0)
		{
			if (magicka < GetMagicka())
				return;
		}
	}

	int prevMagicka = (int)m_magicka;
	m_magicka = float(min(magicka, GetMaxMagicka()));
}

void CActor::SetStrength(float strength)
{
	
	m_strength = strength;
}

void CActor::SetAgility(float agility)
{
	m_agility = agility;
}

void CActor::SetWillpower(float willpower)
{
	m_willpower = willpower;
}

void CActor::SetEndurance(float endurance)
{
	m_endurance = endurance;
}

void CActor::SetPersonality(float personality)
{
	m_personality = personality;
}

void CActor::SetIntelligence(float intelligence)
{
	m_intelligence = intelligence;
}

void CActor::SetLevel(float level)
{
	m_level = level;
}

void CActor::SetArmor(float armor)
{
	m_armor = armor;
	
}

void CActor::SetDialogStage(int stage)
{
	m_dialogstage = stage;
}

void CActor::SetMaxStamina(int maxStamina)
{
	m_maxStamina = maxStamina;
	SetStamina(maxStamina);
}

void CActor::SetMaxMagicka(int maxMagicka)
{
	m_maxMagicka = maxMagicka;
	SetMagicka(maxMagicka);
}

int CActor::GetGender() const
{
	return m_gender;
}
//-----------
int CActor::GetGold() const
{
	return m_gold;
}

//************************************************************************

//------------------------------------------------------------------------
void CActor::EnablePickingUpItems( bool enable )
{
	m_enablePickupItems = enable;
}

void CActor::UpdateMountedGunController(bool forceIKUpdate)
{
}


//------------------------------------------------------------------------
bool CActor::LoadAutoAimParams(SmartScriptTable pEntityTable, SAutoaimTargetRegisterParams &outAutoAimParams)
{
	assert((bool)pEntityTable);

	bool bResult = false;

	SmartScriptTable pGameParams;
	if (pEntityTable && pEntityTable->GetValue("gameParams", pGameParams))
	{
		SmartScriptTable pAutoAimParams;
		if (pGameParams->GetValue("autoAimTargetParams", pAutoAimParams))
		{
			CScriptSetGetChain chainParams(pAutoAimParams);

			int primaryTargetId = -1, secondaryTargetId = -1, physicsTargetId = -1;
			chainParams.GetValue("primaryTargetBone", primaryTargetId);
			chainParams.GetValue("physicsTargetBone", physicsTargetId);
			chainParams.GetValue("secondaryTargetBone", secondaryTargetId);
			chainParams.GetValue("fallbackOffset", outAutoAimParams.fallbackOffset);
			chainParams.GetValue("innerRadius", outAutoAimParams.innerRadius);
			chainParams.GetValue("outerRadius", outAutoAimParams.outerRadius);
			chainParams.GetValue("snapRadius", outAutoAimParams.snapRadius);
			chainParams.GetValue("snapRadiusTagged", outAutoAimParams.snapRadiusTagged);

			outAutoAimParams.primaryBoneId = primaryTargetId;
			outAutoAimParams.physicsBoneId = physicsTargetId;
			outAutoAimParams.secondaryBoneId = secondaryTargetId;

			bResult = true;
		}
	}

	return bResult;
}

//------------------------------------------------------------------------
void CActor::RegisterInAutoAimManager()
{
	if (m_registeredInAutoAimMng)
		return;

	if (gEnv->IsEditing())
		return;

	if (m_isClient || m_health.IsDead())
		return;

	if (m_LuaCache_GameParams)
	{
		RegisterInAutoAimManager(m_LuaCache_GameParams->autoAimParams);
	}
	else
	{
		IEntity *pEntity = GetEntity();
		IEntityClass *pClass = pEntity->GetClass();

		// Run-time loading
		if (CGameCache::IsLuaCacheEnabled())
		{
			GameWarning("[Game Cache] Warning: Loading auto aim params for entity class \'%s\' at run-time!", pClass->GetName());
		}

		SAutoaimTargetRegisterParams autoAimParams;
		if (LoadAutoAimParams(pEntity->GetScriptTable(), autoAimParams))
		{
			RegisterInAutoAimManager(autoAimParams);
		}
		else
		{
			GameWarning("Failed to load auto aim params for actor \'%s\'", pEntity->GetName());
		}
	}
}

//------------------------------------------------------------------------
void CActor::RegisterInAutoAimManager(const SAutoaimTargetRegisterParams &autoAimParams)
{
	m_registeredInAutoAimMng = ShouldRegisterAsAutoAimTarget() && g_pGame->GetAutoAimManager().RegisterAutoaimTargetActor(*this, autoAimParams);
}

//------------------------------------------------------------------------
void CActor::UnRegisterInAutoAimManager()
{
	if (!m_registeredInAutoAimMng)
		return;

	g_pGame->GetAutoAimManager().UnregisterAutoaimTarget(GetEntityId());

	m_registeredInAutoAimMng = false;
}

//------------------------------------------------------------------------
void CActor::RegisterDBAGroups()
{
	if (m_registeredAnimationDBAs)
		return;

	if (m_health.IsDead() || GetEntity()->IsHidden())
		return;
	
	if (m_LuaCache_GameParams)
	{
		m_registeredAnimationDBAs = g_pGame->GetGameCache().PrepareDBAsFor(GetEntityId(), m_LuaCache_GameParams->gameParams.animationDBAs);
	}
}

//------------------------------------------------------------------------
void CActor::UnRegisterDBAGroups()
{
	if (!m_registeredAnimationDBAs)
		return;

	//The local player should keep assets always loaded
	if (IsClient())
		return;

	g_pGame->GetGameCache().RemoveDBAUser(GetEntityId());
	m_registeredAnimationDBAs = false;
}

//------------------------------------------------------------------------
void CActor::OnAIProxyEnabled( bool enabled )
{
	if (enabled)
	{
		RegisterInAutoAimManager();
		RegisterDBAGroups();
		NotifyInventoryAboutOwnerActivation();
	}
	else
	{
		UnRegisterInAutoAimManager();
		UnRegisterDBAGroups();
		NotifyInventoryAboutOwnerDeactivation();
	}
}

void CActor::OnReused(IEntity *pEntity, SEntitySpawnParams &params)
{
	// set the overridden model, not the one provided by the entity class
	UpdateActorModel();

	// have the overridden model show up
	SetActorModelInternal();
}

void CActor::StartInteractiveAction( EntityId entityId, int interactionIndex )
{
	
}

void CActor::StartInteractiveActionByName( const char* interaction, bool bUpdateVisibility, float actionSpeed /*= 1.0f*/ )
{

}

void CActor::EndInteractiveAction( EntityId entityId )
{

}

void CActor::LockInteractor(EntityId lockId, bool lock)
{
	SmartScriptTable locker(gEnv->pScriptSystem);
	locker->SetValue("locker", ScriptHandle(lockId));
	locker->SetValue("lockId", ScriptHandle( lock ? lockId : 0));
	locker->SetValue("lockIdx", lock ? 1 : 0);
	GetGameObject()->SetExtensionParams("Interactor", locker);
}

void CActor::AddHeatPulse( const float intensity, const float time )
{

}

void CActor::SetGrabbedByPlayer( IEntity* pPlayerEntity, bool grabbed )
{
	SActorStats* pActorStats = GetActorStats();
	if ((pActorStats != NULL) && (pActorStats->isGrabbed != grabbed))
	{
		if (grabbed)
		{
			IAIObject* pAIObject = GetEntity()->GetAI();
			if (pAIObject && gEnv->pAISystem)
			{
				IAIActor* pAIActor = CastToIAIActorSafe(pAIObject);
				if(pAIActor)
				{
					IAISignalExtraData *pSData = gEnv->pAISystem->CreateSignalExtraData();	
					pSData->point = Vec3(0,0,0);
					pAIActor->SetSignal(1, "OnGrabbedByPlayer", pPlayerEntity, pSData);
				}
				pAIObject->Event(AIEVENT_DISABLE,0);
			}

			if (m_currentPhysProfile == eAP_Sleep)
				GetGameObject()->SetAspectProfile(eEA_Physics, eAP_Alive);
			m_pAnimatedCharacter->SetInGrabbedState(true); 
		}
		else
		{
			IAIObject* pAIObject = GetEntity()->GetAI();
			if (pAIObject)
			{
				pAIObject->Event(AIEVENT_ENABLE, 0);
			}

			//Force an animation update if needed here
			SEntityEvent xFormEvent;
			xFormEvent.event = ENTITY_EVENT_XFORM;
			xFormEvent.nParam[0] = ENTITY_XFORM_ROT|ENTITY_XFORM_POS;
			m_pAnimatedCharacter->ProcessEvent(xFormEvent);

			m_pAnimatedCharacter->SetInGrabbedState(false);
		}

		pActorStats->isGrabbed = grabbed;
	}
}

void CActor::Reset( bool toGame )
{
	if (m_bodyDestructionInstance.GetProfileId() != INVALID_BODYDESTRUCTIBILITYPROFILEID)
	{
		g_pGame->GetBodyDamageManager()->ResetInstance(*GetEntity(), m_bodyDestructionInstance);
	}

	GenerateBlendRagdollTags();
	ReleaseLegsColliders();
	RequestForDelayedAddEquipPacks();
}

const char * CActor::GetShadowFileModel()
{
	if (m_LuaCache_Properties)
	{
		return m_LuaCache_Properties->fileModelInfo.sShadowFileName.c_str();
	}
	else
	{
		// If we haven't cached the value, return our normal character model
		IEntity *pEntity = GetEntity();
		return pEntity->GetCharacter(0)->GetFilePath();
	}
}
void CActor::ReloadBodyDestruction()
{
	if (m_bodyDestructionInstance.GetProfileId() != INVALID_BODYDESTRUCTIBILITYPROFILEID)
	{
		g_pGame->GetBodyDamageManager()->GetBodyDestructibility(*GetEntity(), m_bodyDestructionInstance);
	}
}

void CActor::GenerateBlendRagdollTags()
{
	SmartScriptTable props;
	IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
	string additional_equipment_path = "";
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		CScriptSetGetChain prop(props);
		prop.GetValue("manequen_path", additional_equipment_path);
	}

	if (additional_equipment_path.empty())
	{
		additional_equipment_path = "Animations/Mannequin/ADB/";
	}
	additional_equipment_path = additional_equipment_path + string("blendRagdollTags.xml");
	IMannequin &mannequinSys = gEnv->pGame->GetIGameFramework()->GetMannequinInterface();
	const CTagDefinition* pTagDefinition = mannequinSys.GetAnimationDatabaseManager().FindTagDef(additional_equipment_path.c_str());

	if( pTagDefinition )
	{
		m_blendRagdollParams.m_blendInTagState = TAG_STATE_EMPTY;
		m_blendRagdollParams.m_blendOutTagState = TAG_STATE_EMPTY;

		{
			const TagID tagID = pTagDefinition->Find( "blendIn" );
			pTagDefinition->Set( m_blendRagdollParams.m_blendInTagState, tagID, true );
		}

		{
			const TagID tagID = pTagDefinition->Find( "blendOut" );
			pTagDefinition->Set( m_blendRagdollParams.m_blendOutTagState, tagID, true );
		}

		{
			const TagID tagID = pTagDefinition->Find( "standup" );
			pTagDefinition->Set( m_blendRagdollParams.m_blendOutTagState, tagID, true );
			pTagDefinition->Set( m_blendRagdollParams.m_blendInTagState, tagID, true );
		}

		{
			const TagID tagID = pTagDefinition->Find( "ragdoll" );
			pTagDefinition->Set( m_blendRagdollParams.m_blendInTagState, tagID, true );
		}
	}
}

void CActor::PhysicalizeLocalPlayerAdditionalParts()
{
	if (!IsClient())
		return;

	IPhysicalEntity* pPhysicalEntity = GetEntity()->GetPhysics();

	CRY_ASSERT(pPhysicalEntity);
	if (!pPhysicalEntity)
		return;

	const bool wasHidden = GetEntity()->IsHidden();

	//Make sure to un-hide before trying to do anything with physics
	if (wasHidden)
		GetEntity()->Hide(false);

	//Note: Main cylinder/capsule is part 100
	const int vegetationBendingPartId = 101;

	//Add extra capsule for vegetation bending
	//Offset slightly to the front so we get good/visible bending for the camera distance 
	{
		primitives::capsule prim;
		prim.axis.Set(0,0,1);
		prim.center.zero(); prim.r = 0.4f; prim.hh = 0.2f;

		IGeometry *pPrimGeom = gEnv->pPhysicalWorld->GetGeomManager()->CreatePrimitive(primitives::capsule::type, &prim);
		phys_geometry *pGeom = gEnv->pPhysicalWorld->GetGeomManager()->RegisterGeometry(pPrimGeom, 0);

		pe_geomparams gp;
		gp.pos = Vec3(0.0f,0.2f,0.7f);
		gp.flags = geom_colltype_foliage;
		gp.flagsCollider = 0;
		pGeom->nRefCount = 0;
		pPrimGeom->Release();

		pPhysicalEntity->AddGeometry(pGeom, &gp, vegetationBendingPartId);
	}

	//Restore hidden state if needed
	if (wasHidden)
		GetEntity()->Hide(true);

}
//------------------------------------------------------------------------
void CActor::ImmuneToForbiddenZone(const bool immune)
{
	const bool bPrev = m_IsImmuneToForbiddenZone;
	m_IsImmuneToForbiddenZone = immune;
	if(bPrev!=immune && IsClient())
	{
		if(CGameRules* pGameRules = g_pGame->GetGameRules())
		{
			pGameRules->CallOnForbiddenAreas(immune?"OnLocalPlayerImmunityOn":"OnLocalPlayerImmunityOff");
		}
	}
}

const bool CActor::ImmuneToForbiddenZone() const
{
	return m_IsImmuneToForbiddenZone; 
}

//------------------------------------------------------------------------
EntityId CActor::SimpleFindItemIdInCategory(const char *category) const
{
	EntityId retItem=0;

	if(IInventory* pInventory = GetInventory())
	{
		IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
		int categoryType = GetItemCategoryType(category);	
		int numItems = pInventory->GetCount();

		for(int i = 0; i < numItems; i++)
		{
			EntityId itemId = pInventory->GetItem(i);

			IItem* pItem = pItemSystem->GetItem(itemId);

			if(pItem && pItem->CanSelect())
			{
				const char* itemCategory = pItemSystem->GetItemCategory(pItem->GetEntity()->GetClass()->GetName());
				int itemCategoryType = GetItemCategoryType(itemCategory);
				if (itemCategoryType & categoryType)
				{
					retItem = itemId;
					break;
				}
			}
		}
	}

	return retItem;
}

void CActor::NotifyInventoryAboutOwnerActivation()
{
	if (!IsClient())
	{
		IInventory* pInventory = GetInventory();
		if (pInventory)
		{
			IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
			const int itemCount = pInventory->GetCount();
			for ( int i = 0; i < itemCount; ++i )
			{
				CItem* pItem = static_cast<CItem *>(pItemSystem->GetItem(pInventory->GetItem(i)));
				if (pItem)
				{
					pItem->OnOwnerActivated();
				}
			}
		}
	}
}

void CActor::NotifyInventoryAboutOwnerDeactivation()
{
	if (!IsClient())
	{
		IInventory* pInventory = GetInventory();
		if (pInventory)
		{
			IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
			const int itemCount = pInventory->GetCount();
			for ( int i = 0; i < itemCount; ++i )
			{
				CItem* pItem = static_cast<CItem *>(pItemSystem->GetItem(pInventory->GetItem(i)));
				if (pItem)
				{
					pItem->OnOwnerDeactivated();
				}
			}
		}
	}
}

void CActor::SetTag(TagID tagId, bool enable)
{
	if(IActionController* pActionController = GetAnimatedCharacter()->GetActionController())
	{
		SAnimationContext &animContext = pActionController->GetContext();

		animContext.state.Set(tagId, enable);
	}
}

void SActorPhysics::Serialize(TSerialize ser, EEntityAspects aspects)
{
	assert( ser.GetSerializationTarget() != eST_Network );
	ser.BeginGroup("PlayerStats");

	if (ser.GetSerializationTarget() != eST_Network)
	{
		//when reading, reset the structure first.
		if (ser.IsReading())
			*this = SActorPhysics();

		ser.Value("gravity", gravity);
		ser.Value("velocity", velocity);
		ser.Value("velocityUnconstrained", velocityUnconstrained);
		ser.Value("groundNormal", groundNormal);
	}

	ser.EndGroup();
}
void CActor::SetTagByCRC(uint32 tagId, bool enable)
{
	if(IActionController* pActionController = GetAnimatedCharacter()->GetActionController())
	{
		SAnimationContext &animContext = pActionController->GetContext();

		animContext.state.SetByCRC(tagId, enable);
	}
}

void CActor::OnSpectateModeStatusChanged( bool spectate )
{
	if(CGameLobby* pGameLobby = g_pGame->GetGameLobby())
	{
		const int channelId = GetGameObject()->GetChannelId();
		//Update spectator status

		if(IsClient())
		{
			SetupLocalPlayer();

			pGameLobby->SetLocalSpectatorStatus(spectate);
		}

		if(gEnv->bServer)
		{
			CGameRules* pGameRules = g_pGame->GetGameRules();
			if(!spectate)
			{
				CRY_ASSERT_MESSAGE( pGameRules->GetTeam(GetEntityId()) == 0, "CActor::SetSpectateStatus - Trying to add ex-spectator to a team but they already have a team" );
				pGameRules->GetPlayerSetupModule()->OnActorJoinedFromSpectate(this, channelId);
			}
			else
			{
				if(pGameRules->IsTeamGame())
				{
					pGameRules->SetTeam(0, GetEntityId());
				}
			}
		}
	}

	GetEntity()->Hide(spectate);
}

void CActor::SetupLocalPlayer()
{
	IEntity *pEntity = GetEntity();

	if(GetSpectatorState() != eASS_SpectatorMode)
	{
		pEntity->SetFlags(pEntity->GetFlags() | ENTITY_FLAG_TRIGGER_AREAS);
		// Invalidate the matrix in order to force an update through the area manager
		pEntity->InvalidateTM(ENTITY_XFORM_POS);

		GetGameObject()->EnablePrePhysicsUpdate( ePPU_Always );

		// always update client's character
		if (ICharacterInstance * pCharacter = pEntity->GetCharacter(0))
			pCharacter->SetFlags(pCharacter->GetFlags() | CS_FLAG_UPDATE_ALWAYS);

		// We get this after we've been revived, so start recording now.
		if (g_pGame->GetRecordingSystem() && (g_pGame->GetHostMigrationState() == CGame::eHMS_NotMigrating))
		{
			g_pGame->GetRecordingSystem()->StartRecording();
		}

		GetGameObject()->AttachDistanceChecker();
	}
	else
	{
		pEntity->SetFlags(pEntity->GetFlags() & ~ENTITY_FLAG_TRIGGER_AREAS);

		GetGameObject()->EnablePrePhysicsUpdate( ePPU_Never );

		if (ICharacterInstance * pCharacter = pEntity->GetCharacter(0))
			pCharacter->SetFlags(pCharacter->GetFlags() & ~CS_FLAG_UPDATE_ALWAYS);
	}
}

void CActor::RequestChangeSpectatorStatus( bool spectate )
{
	CRY_ASSERT_MESSAGE(IsClient(), "CActor::RequestChangeSpectatorStatus is being called on a non-client player");

	if(!CanSwitchSpectatorStatus())
	{
		return;
	}

	EActorSpectatorState desiredState = spectate ? eASS_SpectatorMode : eASS_ForcedEquipmentChange;

	if(desiredState != GetSpectatorState())
	{
		if(GetSpectatorState() != eASS_None)
		{
			m_spectateSwitchTime = gEnv->pTimer->GetFrameStartTime().GetSeconds();
		}

		if(gEnv->bServer)
		{
			IGameRulesSpectatorModule *pSpectatorModule = g_pGame->GetGameRules()->GetSpectatorModule();
			if( pSpectatorModule )
			{
				pSpectatorModule->ChangeSpectatorMode( this, spectate ? eASM_Follow : CActor::eASM_Fixed, 0, false );
				SetSpectatorState(desiredState);
			}
		}
		else
		{
			CGameRules::ServerSpectatorParams params;
			params.entityId = GetEntityId();
			params.state = desiredState;
			params.mode = spectate ? eASM_Follow : CActor::eASM_Fixed;

			g_pGame->GetGameRules()->GetGameObject()->InvokeRMI(CGameRules::SvSetSpectatorState(), params, eRMI_ToServer);
		}
	}
}

bool CActor::CanSwitchSpectatorStatus() const
{
	if(g_pGameCVars->g_allowSpectators == 0 || m_spectateSwitchTime > 0.f && gEnv->pTimer->GetFrameStartTime().GetSeconds() - m_spectateSwitchTime < g_pGameCVars->g_spectatorOnlySwitchCooldown)
	{
		return false;
	}

	const CGameRules* pGameRules = g_pGame->GetGameRules();
	const int numberOfSpectators = pGameRules->GetSpectatorCount();
	const int numberOfPlayers = pGameRules->GetPlayerCountClient() - numberOfSpectators;
	if(GetSpectatorState() != eASS_SpectatorMode && ((numberOfPlayers <= 2 || numberOfSpectators >= SPECTATE_MAX_ALLOWED) || pGameRules->GetTeamPlayerCount(pGameRules->GetTeam(GetEntityId())) == 1))
	{
		return false;
	}

	return true;
}

IComponent::ComponentEventPriority CActor::GetEventPriority( const int eventID ) const
{
	switch( eventID )
	{
	case ENTITY_EVENT_PREPHYSICSUPDATE:
		return( ENTITY_PROXY_LAST - ENTITY_PROXY_USER + EEntityEventPriority_Actor + (m_isClient ? EEntityEventPriority_Client : 0) );
	}

	return IGameObjectExtension::GetEventPriority( eventID );
}

void CActor::OnHostMigrationCompleted()
{
	// If we had a request out there in the wild... unlikely to get a response now!
	SetStillWaitingOnServerUseResponse(false);

	CItem *pItem = (CItem*) GetCurrentItem();
	if (pItem)
	{
		CWeapon *pWeapon = (CWeapon*) pItem->GetIWeapon();
		if (pWeapon)
		{
			pWeapon->OnHostMigrationCompleted();
		}
	}
}

void CActor::SetStillWaitingOnServerUseResponse( bool waiting )
{
#ifndef DEMO_BUILD_RPG_SS
	if(m_bAwaitingServerUseResponse && waiting)
	{
		CryFatalError("CActor::SetStillWaitingOnServerUseResponse - Two systems are triggering to wait for a server response at the same time.");
	}
#endif
	m_fAwaitingServerUseResponse = 0.f;
	m_bAwaitingServerUseResponse = waiting;
}

void CActor::UpdateServerResponseTimeOut( const float frameTime )
{
	// Fallback for Server Use Response
	if(m_bAwaitingServerUseResponse)
	{
		m_fAwaitingServerUseResponse += frameTime;
		if(m_fAwaitingServerUseResponse>10.f)
		{
			SetStillWaitingOnServerUseResponse(false);
		}
	}
}


void CActor::UpdateLegsColliders()
{
	pe_status_living sl;
	pe_player_dynamics pd;
	if (g_pGameCVars->pl_legs_colliders_dist<=0 || !GetEntity()->GetPhysics() || !GetEntity()->GetPhysics()->GetStatus(&sl) || !GetEntity()->GetPhysics()->GetParams(&pd) || !pd.bActive)
	{
		ReleaseLegsColliders();
		return;
	}
	Vec3 pos = GetEntity()->GetPos();
	Quat q = GetEntity()->GetRotation();
	IPhysicalEntity *pPhysicalEntity = GetEntity()->GetPhysics();

	if (ICharacterInstance *pChar = GetEntity()->GetCharacter(0))
		if (IPhysicalEntity *pCharPhys = pChar->GetISkeletonPose()->GetCharacterPhysics())
			if (!m_pLegsCollider[0])
			{
				pe_params_pos pp; pp.iSimClass=2;
				m_pLegsFrame = gEnv->pPhysicalWorld->CreatePhysicalEntity(PE_ARTICULATED,&pp);
				pe_simulation_params simp; simp.collTypes = 0;
				m_pLegsFrame->SetParams(&simp);
				m_pLegsIgnoredCollider = 0;
				pe_params_part ppart; ppart.ipart=0;	
				if (pCharPhys->GetParams(&ppart))
				{
					pe_params_articulated_body pab; 
					pab.bGrounded=1; pab.pHost=pPhysicalEntity;	
					m_pLegsFrame->SetParams(&pab);
					pe_articgeomparams agp;
					agp.flags=agp.flagsCollider = 0; agp.idbody=0;
					m_pLegsFrame->AddGeometry(ppart.pPhysGeom,&agp);
					pe_params_flags pf; pf.flagsAND = ~pef_traceable;
					int bFrameQueued=m_pLegsFrame->SetParams(&pf)-1, bLegsQueued=0;
					pe_geomparams gp; 
					gp.flags = geom_colltype_solid & ~(geom_colltype_player|geom_colltype8);
					gp.flagsCollider = geom_colltype0;
					gp.mass = 5;
					gp.scale = g_pGameCVars->pl_legs_colliders_scale;
					simp.collTypes = ent_sleeping_rigid|ent_rigid;

					char boneName[] = "Bip01 L Calf";
					for(int i=0; i<2; i++,boneName[6]='R')
						if ((ppart.partid=pChar->GetIDefaultSkeleton().GetJointIDByName(boneName))>0 && (ppart.ipart=-1,pCharPhys->GetParams(&ppart)))
						{
							QuatT qbone = pChar->GetISkeletonPose()->GetAbsJointByID(m_iboneLeg[i]=ppart.partid);
							pp.pos = pos + q*qbone.t;	pp.q = q*qbone.q;
							m_pLegsCollider[i] = gEnv->pPhysicalWorld->CreatePhysicalEntity(PE_RIGID,&pp);
							m_pLegsCollider[i]->AddGeometry(ppart.pPhysGeom,&gp,ppart.partid);
							pe_action_add_constraint aac;
							aac.flags = local_frames|constraint_no_tears|constraint_ignore_buddy|constraint_no_rotation;
							aac.pt[0].zero(); aac.qframe[0].SetIdentity();
							aac.pt[1] = q*qbone.t; aac.qframe[1] = q*qbone.q;
							aac.maxPullForce = aac.maxBendTorque = 5000.0f;
							aac.pBuddy = m_pLegsFrame; aac.id = 100;
							m_pLegsCollider[i]->Action(&aac,-bFrameQueued>>31);
							bLegsQueued += m_pLegsCollider[i]->SetParams(&simp)-1;
							m_ptSample[i].zero();
							m_bLegActive[i] = 1;
						}
					if (m_pLegsCollider[0] && m_pLegsCollider[1])
					{
						pe_action_add_constraint aac;
						aac.flags = constraint_inactive|constraint_ignore_buddy;
						aac.pBuddy = m_pLegsCollider[1]; aac.pt[0].zero();
						m_pLegsCollider[0]->Action(&aac,-bLegsQueued>>31);
					}
				}
			}	else for(int i=0;i<2;i++)	if (m_pLegsCollider[i])
			{
				QuatT qbone = pChar->GetISkeletonPose()->GetAbsJointByID(m_iboneLeg[i]);
				int bActive = isneg((gEnv->pSystem->GetViewCamera().GetPosition()-pos).len2()-sqr(g_pGameCVars->pl_legs_colliders_dist));
				pe_action_awake aa; aa.bAwake = bActive;
				if (bActive!=m_bLegActive[i])
				{
					pe_params_flags pf; 
					pf.flagsOR=pef_traceable&-bActive; pf.flagsAND=~(pef_traceable&~-bActive);
					m_pLegsCollider[i]->SetParams(&pf);
					pf.flagsOR=pef_disabled&~-bActive; pf.flagsAND=~(pef_disabled&-bActive);
					m_pLegsFrame->SetParams(&pf);
					if (bActive)
					{
						pe_params_pos pp; pp.pos = pos;
						m_pLegsFrame->SetParams(&pp);
						pp.pos = pos + q*qbone.t;	pp.q = q*qbone.q;
						m_pLegsCollider[i]->SetParams(&pp);
					}	else
					{
						m_pLegsCollider[i]->Action(&aa);
						pe_action_update_constraint auc; auc.idConstraint=101; auc.bRemove=1;
						m_pLegsCollider[i]->Action(&auc);	m_pLegsIgnoredCollider=0;
					}
					m_bLegActive[i] = bActive;
				}
				if (bActive)
				{
					pe_action_update_constraint auc;
					auc.idConstraint = 100;	auc.flags = local_frames;
					auc.pt[1] = q*qbone.t; 
					auc.qframe[1] = q*qbone.q;
					m_pLegsCollider[i]->Action(&auc);
					Vec3 ptSample = auc.qframe[1]*Vec3(0,0,1)+auc.pt[1]+pos;
					if ((ptSample-m_ptSample[i]).len2()>sqr(0.005f))
					{
						m_pLegsCollider[i]->Action(&aa); m_ptSample[i] = ptSample;
					}
					if (m_pLegsIgnoredCollider!=sl.pGroundCollider)
					{
						auc.idConstraint=101; auc.bRemove=1;
						m_pLegsCollider[i]->Action(&auc);	
						if (sl.pGroundCollider)
						{
							pe_action_add_constraint aac; aac.id=101; aac.pt[0].zero();
							aac.flags = constraint_ignore_buddy|constraint_inactive;
							aac.pBuddy = sl.pGroundCollider;
							m_pLegsCollider[i]->Action(&aac);
						}
						if (i==1)
							m_pLegsIgnoredCollider = sl.pGroundCollider;
					}
				}
			}
}

void CActor::ReleaseLegsColliders()
{
	for(int i=0;i<2;i++) if (m_pLegsCollider[i])
		gEnv->pPhysicalWorld->DestroyPhysicalEntity(m_pLegsCollider[i]);
	if (m_pLegsFrame)
		gEnv->pPhysicalWorld->DestroyPhysicalEntity(m_pLegsFrame);	
	m_pLegsFrame=m_pLegsCollider[0]=m_pLegsCollider[1] = 0;
}

void CActor::AcquireOrReleaseLipSyncExtension()
{
	// get rid of a possibly acquired lip-sync extension
	if (!m_sLipSyncExtensionType.empty())
	{
		GetGameObject()->ReleaseExtension(m_sLipSyncExtensionType.c_str());
		m_sLipSyncExtensionType = "";
	}

	IGameObject* pGameObject = GetGameObject();
	IEntity* pEntity = pGameObject->GetEntity();

	// attempt to acquire the lip-sync extension: check for holding a "LipSync" script table and if so add the LipSync extension (the specific LipSyncProvider will load its settings from this table)
	if (IScriptTable* pScriptTable = pEntity->GetScriptTable())
	{
		SmartScriptTable pPropertiesTable;
		if (pScriptTable->GetValue("Properties", pPropertiesTable))
		{
			SmartScriptTable pLipSyncTable;
			if (pPropertiesTable->GetValue("LipSync", pLipSyncTable))
			{
				bool enabled = false;
				if (pLipSyncTable->GetValue("bEnabled", enabled) && enabled)
				{
					const char* type = NULL;
					if (pLipSyncTable->GetValue("esLipSyncType", type))
					{
						if (pGameObject->AcquireExtension(type) != NULL)
						{
							m_sLipSyncExtensionType = type;
						}
					}
				}
			}
		}
	}
}

//--------------------------------------------------------------------
void CActor::CreateSkinAttachment(int characterSlot, const char *attachmentName, const char *attachmentPth, int spec_fl)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);
	if (!pCharacter)
	{
		return;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if(!pIAttachmentManager)
		return;

	bool is_normal_attachment = true;
	if (0 == stricmp(PathUtil::GetExt(attachmentPth), "xml"))
	{
		if (spec_fl == 0)
		{
			//CryLogAlways("CActor::CreateSkinAttachment ComplexAttachments request");
			SComplexAttachments CA_attach;
			CA_attach.ca_nmn = attachmentName;
			CA_attach.ca_pth = attachmentPth;

			std::vector<SComplexAttachments>::const_iterator it = m_complex_attachments_to_attach.begin();
			std::vector<SComplexAttachments>::const_iterator end = m_complex_attachments_to_attach.end();
			int count = m_complex_attachments_to_attach.size();
			for (int i = 0; i < count, it != end; i++, it++)
			{
				if ((*it).ca_pth == CA_attach.ca_pth)
				{
					//CryLogAlways("CActor::CreateSkinAttachment ComplexAttachments request returned");
					return;
				}
			}
			m_complex_attachments_to_attach.push_back(CA_attach);
			if (gEnv->pTimer->GetFrameRate() <= 1.0f)
			{
				m_complex_attachments_timer = 0.9f;
			}
			else if (gEnv->pTimer->GetFrameRate() <= 5.0f)
			{
				m_complex_attachments_timer = 0.7f;
			}
			else if (gEnv->pTimer->GetFrameRate() <= 10.0f)
			{
				m_complex_attachments_timer = 0.5f;
			}
			else if (gEnv->pTimer->GetFrameRate() <= 20.0f)
			{
				m_complex_attachments_timer = 0.25f;
			}
			else if (gEnv->pTimer->GetFrameRate() <= 40.0f)
			{
				m_complex_attachments_timer = 0.125f;
			}
			else
			{
				m_complex_attachments_timer = 0.05f;
			}
			//CryLogAlways("CActor::CreateSkinAttachment ComplexAttachments request sucess");
			return;
		}

		ISkeletonPose *pSkeletonPose = pCharacter->GetISkeletonPose();
		if (pSkeletonPose)
		{
			pSkeletonPose->DestroyCharacterPhysics(0);
		}
		uint32 plk_lk = pIAttachmentManager->LoadAttachmentList(attachmentPth, false);
		IEntity *pEntity = GetEntity();
		pCharacter = GetEntity()->GetCharacter(characterSlot);
		is_normal_attachment = false;
		//if (spec_fl == 2)
			update_all_skin_attachment_at_next_frame = 2;
		//else if (spec_fl == 1)
			//plk_lk = 1;

		//if (plk_lk == 2)
		{
			//CryLogAlways("CreateSkinAttachment plk_lk == 2");
			IEntityClass *pClass = pEntity->GetClass();
			SActorFileModelInfo fileModelInfo;
			IScriptTable *pEntityTable = pEntity->GetScriptTable();
			SmartScriptTable pProperties;
			if (pEntityTable && pEntityTable->GetValue("Properties", pProperties) &&
				LoadFileModelInfo(pEntityTable, pProperties, fileModelInfo))
			{
				if (!m_currModel.empty())
				{
					fileModelInfo.sFileName = m_currModel;
					fileModelInfo.sClientFileName = m_currModel;
					fileModelInfo.sShadowFileName = m_currModel;
				}
			}
			SActorFileModelInfo::TIKLimbInfoVec::const_iterator itLimb = fileModelInfo.IKLimbInfo.begin();
			SActorFileModelInfo::TIKLimbInfoVec::const_iterator itLimbEnd = fileModelInfo.IKLimbInfo.end();
			for (; itLimb != itLimbEnd; ++itLimb)
			{
				const SActorIKLimbInfo &limbInfo = *itLimb;
				CreateIKLimb(limbInfo);
			}

			if (pCharacter)
			{
				ISkeletonPose *pSkeletonPose = pCharacter->GetISkeletonPose();
				if (pSkeletonPose)
				{
					//pSkeletonPose->DestroyCharacterPhysics(0);
					pSkeletonPose->SetForceSkeletonUpdate(0);
				}
				// Set the character to not be force updated
				IFacialInstance *pFacialInstance = pCharacter->GetFacialInstance();
				if (pFacialInstance)
				{
					pFacialInstance->SetUseFrameRateLimiting(fileModelInfo.bUseFacialFrameRateLimiting);
					pFacialInstance->StopAllSequencesAndChannels();
				}
			}
			Physicalize();
			CPlayer* pPlayer = static_cast<CPlayer*>(this);
			if (pPlayer)
			{

				pPlayer->Physicalize();
				pPlayer->PostPhysicalize();
				if (pPlayer->IsPlayer())
				{
					pPlayer->m_torsoAimIK.Reset();
					if (!pPlayer->IsThirdPerson())
						pPlayer->m_torsoAimIK.Enable();
				}
				pPlayer->m_lookAim.Reset();
			}
			for (uint32 i = 0; i < BONE_ID_NUM; i++)
			{
				m_boneTrans[i].SetIdentity();
				m_boneIDs[i] = -1;
			}
			SActorGameParams gameParams;
			if (m_LuaCache_GameParams)
			{
				gameParams = m_LuaCache_GameParams->gameParams;
			}
			else
			{
				LoadGameParams(pEntity->GetScriptTable(), gameParams);
			}
			ISkeletonPose *pSkelPose = pCharacter ? pCharacter->GetISkeletonPose() : NULL;
			if (pSkelPose)
			{
				IDefaultSkeleton& rIDefaultSkeleton = pCharacter->GetIDefaultSkeleton();
				for (uint32 boneId = 0; boneId < BONE_ID_NUM; ++boneId)
				{
					const string &sName = gameParams.boneNames[boneId];
					if (!sName.empty())
					{
						m_boneIDs[boneId] = rIDefaultSkeleton.GetJointIDByName(sName.c_str());
					}
				}
			}

			IAnimatedCharacter *pAnimatedCharacter = GetAnimatedCharacter();
			if (pAnimatedCharacter)
			{
				pAnimatedCharacter->UpdateCharacterPtrs();
			}
		}
	}

	if (is_normal_attachment)
	{
		IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_SKIN, 0, false);
			if (attachmentPth != NULL)
			{
				ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(attachmentPth, 0);
				if (pSkin)
				{
					IAttachmentSkin* pSkinAttach = pIAttachment->GetIAttachmentSkin();
					if (pSkinAttach)
					{
						CSKINAttachment* pSkinAttachment = new CSKINAttachment();
						pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
						pIAttachment->AddBinding(pSkinAttachment, pSkin, 0);
						pIAttachment->HideAttachment(0);
					}
				}
			}
		}
		else
		{
			if (attachmentPth != NULL)
			{
				if (IAttachmentSkin* pSkinAttach = pIAttachment->GetIAttachmentSkin())
				{
					if (pSkinAttach->GetISkin())
					{
						if (0 == stricmp(pSkinAttach->GetISkin()->GetModelFilePath(), attachmentPth))
						{
							pIAttachment->HideAttachment(0);
							return;
						}
					}

					if (ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(attachmentPth, 0))
					{
						pIAttachment->ClearBinding();
						CSKINAttachment* pSkinAttachment = new CSKINAttachment();
						pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
						pIAttachment->AddBinding(pSkinAttachment, pSkin, 0);
						pIAttachment->HideAttachment(0);
					}
				}
			}
		}
	}

	if (this->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(this);
		if (!pPlayer)
			return;

		ICharacterInstance *pCharacter_shadow = pPlayer->GetShadowCharacter();
		if (!pCharacter_shadow)
		{
			return;
		}
		IAttachmentManager* pIAttachmentManager_shadow = pCharacter_shadow->GetIAttachmentManager();
		if(!pIAttachmentManager_shadow)
			return;

		if (0 == stricmp(PathUtil::GetExt(attachmentPth), "xml"))
		{
			pIAttachmentManager_shadow->LoadAttachmentList(attachmentPth, false);
			is_normal_attachment = false;
		}

		if (is_normal_attachment)
		{
			IAttachment* pIAttachment_shadow = pIAttachmentManager_shadow->GetInterfaceByName(attachmentName);
			if (!pIAttachment_shadow)
			{
				pIAttachment_shadow = pIAttachmentManager_shadow->CreateAttachment(attachmentName, CA_SKIN, 0, false);
				if (attachmentPth != NULL)
				{
					ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(attachmentPth, 0);
					if (pSkin)
					{
						IAttachmentSkin* pSkinAttach = pIAttachment_shadow->GetIAttachmentSkin();
						if (pSkinAttach)
						{
							CSKINAttachment* pSkinAttachment = new CSKINAttachment();
							pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
							pIAttachment_shadow->AddBinding(pSkinAttachment, pSkin, 0);
							pIAttachment_shadow->HideAttachment(0);
							pIAttachment_shadow->HideAttachment(1);
						}
					}
				}
			}
			else
			{
				if (attachmentPth != NULL)
				{
					if (IAttachmentSkin* pSkinAttach = pIAttachment_shadow->GetIAttachmentSkin())
					{
						if (pSkinAttach->GetISkin())
						{
							if (0 == stricmp(pSkinAttach->GetISkin()->GetModelFilePath(), attachmentPth))
							{
								pIAttachment_shadow->HideAttachment(0);
								pIAttachment_shadow->HideAttachment(1);
								return;
							}
						}
						if (ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(attachmentPth, 0))
						{
							pIAttachment_shadow->ClearBinding();
							CSKINAttachment* pSkinAttachment = new CSKINAttachment();
							pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
							pIAttachment_shadow->AddBinding(pSkinAttachment, pSkin, 0);
							pIAttachment_shadow->HideAttachment(0);
							pIAttachment_shadow->HideAttachment(1);
						}
					}
				}
			}
		}
	}
}

void CActor::CreateBoneAttachment(int characterSlot, const char *attachmentName, const char *attachmentPth, const char *BoneName, bool setDefPose, bool ent, EntityId id, bool skel)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	if (0 == stricmp(PathUtil::GetExt(attachmentPth), "chr"))
	{
		skel = true;
	}

	if (0 == stricmp(PathUtil::GetExt(attachmentPth), "cdf"))
	{
		skel = true;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);

	if (skel)
	{
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
			if (attachmentPth != NULL)
			{
				if (ICharacterInstance *charInst = gEnv->pCharacterManager->CreateInstance(attachmentPth))
				{
					CSKELAttachment *pChrAttachment = new CSKELAttachment();
					pChrAttachment->m_pCharInstance = charInst;
					pIAttachment->AddBinding(pChrAttachment);
					pIAttachment->AlignJointAttachment();
				}
			}
		}
		else
		{
			if (attachmentPth != NULL)
			{
				if (ICharacterInstance *charInst = gEnv->pCharacterManager->CreateInstance(attachmentPth))
				{
					pIAttachment->ClearBinding();
					CSKELAttachment *pChrAttachment = new CSKELAttachment();
					pChrAttachment->m_pCharInstance = charInst;
					pIAttachment->AddBinding(pChrAttachment);
				}
			}
		}
		return;
	}

	if (!ent)
	{
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
			if (attachmentPth != NULL)
			{
				if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(attachmentPth))
				{
					CCGFAttachment *pCGFAttachment = new CCGFAttachment();
					pCGFAttachment->pObj = pStatObj;
					pIAttachment->AddBinding(pCGFAttachment);
					pIAttachment->AlignJointAttachment();
				}
			}
		}
		else
		{
			if (attachmentPth != NULL)
			{
				if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(attachmentPth))
				{
					pIAttachment->ClearBinding();
					CCGFAttachment *pCGFAttachment = new CCGFAttachment();
					pCGFAttachment->pObj = pStatObj;
					pIAttachment->AddBinding(pCGFAttachment);
				}
			}
		}
	}
	else
	{
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
			if (IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id))
			{
				CEntityAttachment* pEntityAttachment = new CEntityAttachment();
				pEntityAttachment->SetEntityId(id);
				pIAttachment->AddBinding(pEntityAttachment);
				pIAttachment->AlignJointAttachment();
			}
		}
		else
		{
			if (IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id))
			{
				pIAttachment->ClearBinding();
				CEntityAttachment* pEntityAttachment = new CEntityAttachment();
				pEntityAttachment->SetEntityId(id);
				pIAttachment->AddBinding(pEntityAttachment);
			}
		}
	}

	if (this->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(this);
		if (!pPlayer)
			return;

		ICharacterInstance *pCharacter_shadow = pPlayer->GetShadowCharacter();
		if (!pCharacter_shadow)
		{
			return;
		}
		IAttachmentManager* pIAttachmentManager_shadow = pCharacter_shadow->GetIAttachmentManager();
		IAttachment* pIAttachment_shadow = pIAttachmentManager_shadow->GetInterfaceByName(attachmentName);
		if (skel)
		{
			if (!pIAttachment_shadow)
			{
				pIAttachment_shadow = pIAttachmentManager_shadow->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
				if (attachmentPth != NULL)
				{
					if (ICharacterInstance *charInst = gEnv->pCharacterManager->CreateInstance(attachmentPth))
					{
						CSKELAttachment *pChrAttachment = new CSKELAttachment();
						pChrAttachment->m_pCharInstance = charInst;
						pIAttachment_shadow->AddBinding(pChrAttachment);
						pIAttachment_shadow->AlignJointAttachment();
					}
				}
			}
			else
			{
				if (attachmentPth != NULL)
				{
					if (ICharacterInstance *charInst = gEnv->pCharacterManager->CreateInstance(attachmentPth))
					{
						pIAttachment_shadow->ClearBinding();
						CSKELAttachment *pChrAttachment = new CSKELAttachment();
						pChrAttachment->m_pCharInstance = charInst;
						pIAttachment_shadow->AddBinding(pChrAttachment);
					}
				}
			}
			return;
		}

		if (!ent)
		{
			if (!pIAttachment_shadow)
			{
				pIAttachment_shadow = pIAttachmentManager_shadow->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
				if (attachmentPth != NULL)
				{
					if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(attachmentPth))
					{
						CCGFAttachment *pCGFAttachment = new CCGFAttachment();
						pCGFAttachment->pObj = pStatObj;
						pIAttachment_shadow->AddBinding(pCGFAttachment);
						pIAttachment_shadow->AlignJointAttachment();
					}
				}
			}
			else
			{
				if (attachmentPth != NULL)
				{
					if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(attachmentPth))
					{
						pIAttachment_shadow->ClearBinding();
						CCGFAttachment *pCGFAttachment = new CCGFAttachment();
						pCGFAttachment->pObj = pStatObj;
						pIAttachment_shadow->AddBinding(pCGFAttachment);
					}
				}
			}
		}
		else
		{
			if (!pIAttachment_shadow)
			{
				pIAttachment_shadow = pIAttachmentManager_shadow->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
				if (IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id))
				{
					CEntityAttachment* pEntityAttachment = new CEntityAttachment();
					pEntityAttachment->SetEntityId(id);
					pIAttachment_shadow->AddBinding(pEntityAttachment);
					pIAttachment_shadow->AlignJointAttachment();
				}
			}
			else
			{
				if (IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id))
				{
					pIAttachment_shadow->ClearBinding();
					CEntityAttachment* pEntityAttachment = new CEntityAttachment();
					pEntityAttachment->SetEntityId(id);
					pIAttachment_shadow->AddBinding(pEntityAttachment);
				}
			}
		}
	}
}

void CActor::DeleteBoneAttachment(int characterSlot, const char *attachmentName, bool clear_only)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);

	if (!pIAttachment)
	{
		return;
	}
	else
	{
		if (clear_only)
		{
			pIAttachment->ClearBinding();
		}
		else
		{
			pIAttachmentManager->RemoveAttachmentByName(attachmentName);
		}
	}

	if (this->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(this);
		if (!pPlayer)
			return;

		ICharacterInstance *pCharacter_shadow = pPlayer->GetShadowCharacter();
		if (!pCharacter_shadow)
		{
			return;
		}
		IAttachmentManager* pIAttachmentManager_shadow = pCharacter_shadow->GetIAttachmentManager();
		IAttachment* pIAttachment_shadow = pIAttachmentManager_shadow->GetInterfaceByName(attachmentName);
		if (!pIAttachment_shadow)
		{
			return;
		}
		else
		{
			if (clear_only)
			{
				pIAttachment_shadow->ClearBinding();
			}
			else
			{
				pIAttachmentManager_shadow->RemoveAttachmentByName(attachmentName);
			}
		}
	}
}

void CActor::DeleteSkinAttachment(int characterSlot, const char *attachmentName, bool clear_only, const char *attachmentPth)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);
	if (!pCharacter)
	{
		return;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if (!pIAttachmentManager)
		return;

	bool is_normal_attachment = true;
	if (attachmentPth)
	{
		if (0 == stricmp(PathUtil::GetExt(attachmentPth), "xml"))
		{
			SComplexAttachments CA_attach;
			CA_attach.ca_nmn = attachmentName;
			CA_attach.ca_pth = attachmentPth;
			std::vector<SComplexAttachments>::const_iterator it = m_complex_attachments_to_attach.begin();
			std::vector<SComplexAttachments>::const_iterator end = m_complex_attachments_to_attach.end();
			int count = m_complex_attachments_to_attach.size();
			for (int i = 0; i < count, it != end; i++, it++)
			{
				if ((*it).ca_pth == CA_attach.ca_pth)
				{
					m_complex_attachments_to_attach.erase(it);
					m_complex_attachments_to_attach.resize(count - 1);
					return;
				}
			}
			pIAttachmentManager->LoadAttachmentList(attachmentPth, false, true);
			is_normal_attachment = false;
		}
	}

	if (is_normal_attachment)
	{
		IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
		if (!pIAttachment)
		{
			return;
		}
		else
		{
			if (clear_only)
			{
				pIAttachment->ClearBinding(CA_SkipSkelRecreation);
			}
			else
			{
				pIAttachmentManager->RemoveAttachmentByName(attachmentName);
			}
		}
	}

	if (this->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(this);
		if (!pPlayer)
			return;

		ICharacterInstance *pCharacter_shadow = pPlayer->GetShadowCharacter();
		if (!pCharacter_shadow)
		{
			return;
		}
		IAttachmentManager* pIAttachmentManager_shadow = pCharacter_shadow->GetIAttachmentManager();
		if (!pIAttachmentManager_shadow)
			return;

		if (attachmentPth)
		{
			if (0 == stricmp(PathUtil::GetExt(attachmentPth), "xml"))
			{
				pIAttachmentManager_shadow->LoadAttachmentList(attachmentPth, false, true);
				is_normal_attachment = false;
			}
		}

		if (is_normal_attachment)
		{
			IAttachment* pIAttachment_shadow = pIAttachmentManager_shadow->GetInterfaceByName(attachmentName);
			if (!pIAttachment_shadow)
			{
				return;
			}
			else
			{
				if (clear_only)
				{
					pIAttachment_shadow->ClearBinding();
				}
				else
				{
					pIAttachmentManager_shadow->RemoveAttachmentByName(attachmentName);
				}
			}
		}
	}
}

void CActor::DeleteLightAttachment(int characterSlot, const char *attachmentName, bool clear_only)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);

	if (!pIAttachment)
	{
		return;
	}
	else
	{
		if (clear_only)
		{
			pIAttachment->ClearBinding();
		}
		else
		{
			pIAttachmentManager->RemoveAttachmentByName(attachmentName);
		}
	}
}

void CActor::DeleteParticleAttachment(int characterSlot, const char *attachmentName, bool clear_only)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);

	if (!pIAttachment)
	{
		return;
	}
	else
	{
		if (clear_only)
		{
			pIAttachment->ClearBinding();
		}
		else
		{
			pIAttachmentManager->RemoveAttachmentByName(attachmentName);
		}
	}
}

void CActor::CreateLightAttachment(int characterSlot, const char *attachmentName, const char *BoneName, bool setDefPose, ColorF col, float rad, float intense, bool shadows)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
	CDLight light;
	col = col*0.4;
	light.SetLightColor(col);
	light.m_Flags = DLF_LM;
	light.m_fAttenuationBulbSize = 0.005f;
	light.m_fShadowUpdateMinRadius = 10;
	light.m_nShadowUpdateRatio = 1;
	light.m_fRadius = rad;
	light.m_nLightStyle = 0;
	light.m_nAnimSpeed = 0;
	light.m_fAreaWidth = 5.0f;
	light.m_fAreaHeight = 5.0f;
	light.m_nLightPhase = 1;
	light.m_fBaseRadius = rad*0.01f;
	light.m_SpecMult = 1.0f;
	light.m_ProjMatrix.SetIdentity();
	light.m_ObjMatrix.SetIdentity();
	light.m_sName = "ChrLgt";
	light.m_fHDRDynamic = -9.5f;
	light.SetShadowBiasParams(1, 1);
	light.m_nAttenFalloffMax = 1;
	if (shadows)
		light.m_Flags |= DLF_CASTSHADOW_MAPS;

	light.m_Flags |= DLF_POINT;

	if (!pIAttachment)
	{
		pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
		CLightAttachment *pLightAttachment = new CLightAttachment();
		pLightAttachment->LoadLight(light);
		pIAttachment->AddBinding(pLightAttachment);
		pIAttachment->AlignJointAttachment();
	}
	else
	{
		pIAttachment->ClearBinding();
		CLightAttachment *pLightAttachment = new CLightAttachment();
		pLightAttachment->LoadLight(light);
		pIAttachment->AddBinding(pLightAttachment);
	}

}

void CActor::CreateParticleAttachment(int characterSlot, const char *attachmentName, const char *BoneName, const char *ParticleEffectName, bool setDefPose)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);

	if (!pCharacter)
	{
		return;
	}

	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);

	if (!pIAttachment)
	{
		pIAttachment = pIAttachmentManager->CreateAttachment(attachmentName, CA_BONE, BoneName, setDefPose);
		IParticleEffect *pParticleEffect = gEnv->pParticleManager->FindEffect(ParticleEffectName);
		CEffectAttachment *pEffectAttachment = new CEffectAttachment(pParticleEffect, Vec3(0, 0, 0), Vec3(0, 1, 0), 1);
		pIAttachment->AddBinding(pEffectAttachment);
		pIAttachment->AlignJointAttachment();
		pIAttachment->UpdateAttModelRelative();
	}
	else
	{
		pIAttachment->ClearBinding();
		IParticleEffect *pParticleEffect = gEnv->pParticleManager->FindEffect(ParticleEffectName);
		CEffectAttachment *pEffectAttachment = new CEffectAttachment(pParticleEffect, Vec3(0, 0, 0), Vec3(0, 1, 0), 1);
		pIAttachment->AddBinding(pEffectAttachment);
		//pIAttachment->AlignJointAttachment();
		pIAttachment->UpdateAttModelRelative();
	}
}

bool CActor::IsAttachmentOnCharacter(int characterSlot, const char *attachmentName)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);
	if (!pCharacter)
	{
		return false;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if (!pIAttachmentManager)
	{
		return false;
	}
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
	if (pIAttachment)
		return true;
	else
		return false;
}

ICharacterInstance *CActor::GetAttachmentObjectCharacterInstance(int characterSlot, const char *attachmentName)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);
	if (!pCharacter)
	{
		return NULL;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if (!pIAttachmentManager)
	{
		return NULL;
	}
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
	if (!pIAttachment)
	{
		return NULL;
	}

	if (!pIAttachment->GetIAttachmentObject())
	{
		return NULL;
	}
	return pIAttachment->GetIAttachmentObject()->GetICharacterInstance();
}
//--------------------------------------------------------------------

const char* CActor::GetActorArmorPartModelPath(int part_id, int model_slot)
{
	if (part_id == 1)
	{
		if (model_slot == 0)
		{
			return m_armboots.c_str();
		}
		else if (model_slot == 1)
		{
			return m_armbootsa.c_str();
		}
		else if (model_slot == 2)
		{
			return m_armbootsb.c_str();
		}
		else if (model_slot == 3)
		{
			return m_armbootsc.c_str();
		}
		else if (model_slot == 4)
		{
			return m_armbootsd.c_str();
		}
		else if (model_slot == 5)
		{
			return m_armbootsf.c_str();
		}
		else if (model_slot == 6)
		{
			return m_armbootsj.c_str();
		}
	}
	else if (part_id == 2)
	{
		if (model_slot == 0)
		{
			return m_armlegs.c_str();
		}
		else if (model_slot == 1)
		{
			return m_armlegsa.c_str();
		}
		else if (model_slot == 2)
		{
			return m_armlegsb.c_str();
		}
		else if (model_slot == 3)
		{
			return m_armlegsc.c_str();
		}
		else if (model_slot == 4)
		{
			return m_armlegsd.c_str();
		}
		else if (model_slot == 5)
		{
			return m_armlegsf.c_str();
		}
		else if (model_slot == 6)
		{
			return m_armlegsj.c_str();
		}
	}
	else if (part_id == 3)
	{
		if (model_slot == 0)
		{
			return m_armchest.c_str();
		}
		else if (model_slot == 1)
		{
			return m_armchesta.c_str();
		}
		else if (model_slot == 2)
		{
			return m_armchestb.c_str();
		}
		else if (model_slot == 3)
		{
			return m_armchestc.c_str();
		}
		else if (model_slot == 4)
		{
			return m_armchestd.c_str();
		}
		else if (model_slot == 5)
		{
			return m_armchestf.c_str();
		}
		else if (model_slot == 6)
		{
			return m_armchestj.c_str();
		}
	}
	else if (part_id == 4)
	{
		if (model_slot == 0)
		{
			return m_armarms.c_str();
		}
		else if (model_slot == 1)
		{
			return m_armarmsa.c_str();
		}
		else if (model_slot == 2)
		{
			return m_armarmsb.c_str();
		}
		else if (model_slot == 3)
		{
			return m_armarmsc.c_str();
		}
		else if (model_slot == 4)
		{
			return m_armarmsd.c_str();
		}
		else if (model_slot == 5)
		{
			return m_armarmsf.c_str();
		}
		else if (model_slot == 6)
		{
			return m_armarmsj.c_str();
		}
	}
	else if (part_id == 5)
	{
		return m_armhelmet.c_str();
	}
	else if (part_id == 6)
	{
		return m_armshield.c_str();
	}
	else if (part_id == 7)
	{
		return m_armtorch.c_str();
	}
	return 0;
}
const char* CActor::GetActorBodyPartModelPath(int part_id)
{
	if (part_id == 1)
	{
		return m_headmodel.c_str();
	}
	else if (part_id == 2)
	{
		return m_upperbody_model.c_str();
	}
	else if (part_id == 3)
	{
		return m_lowerbody_model.c_str();
	}
	else if (part_id == 4)
	{
		return m_hands_model.c_str();
	}
	else if (part_id == 5)
	{
		return m_foots_model.c_str();
	}
	else if (part_id == 6)
	{
		return m_hairmodel.c_str();
	}
	else if (part_id == 7)
	{
		return m_pppp.c_str();
	}
	else if (part_id == 8)
	{
		return m_underwear01.c_str();
	}
	else if (part_id == 9)
	{
		return m_underwear02.c_str();
	}
	else if (part_id == 10)
	{
		return m_eye_l.c_str();
	}
	else if (part_id == 11)
	{
		return m_eye_r.c_str();
	}
	return 0;
}
void CActor::SetActorArmorPartModelPath(int part_id, int model_slot, const char* model_path)
{
	if (part_id == 1)
	{
		if (model_slot == 0)
		{
			m_armboots = model_path;
		}
		else if (model_slot == 1)
		{
			m_armbootsa = model_path;
		}
		else if (model_slot == 2)
		{
			m_armbootsb = model_path;
		}
		else if (model_slot == 3)
		{
			m_armbootsc = model_path;
		}
		else if (model_slot == 4)
		{
			m_armbootsd = model_path;
		}
		else if (model_slot == 5)
		{
			m_armbootsf = model_path;
		}
		else if (model_slot == 6)
		{
			m_armbootsj = model_path;
		}
	}
	else if (part_id == 2)
	{
		if (model_slot == 0)
		{
			m_armlegs = model_path;
		}
		else if (model_slot == 1)
		{
			m_armlegsa = model_path;
		}
		else if (model_slot == 2)
		{
			m_armlegsb = model_path;
		}
		else if (model_slot == 3)
		{
			m_armlegsc = model_path;
		}
		else if (model_slot == 4)
		{
			m_armlegsd = model_path;
		}
		else if (model_slot == 5)
		{
			m_armlegsf = model_path;
		}
		else if (model_slot == 6)
		{
			m_armlegsj = model_path;
		}
	}
	else if (part_id == 3)
	{
		if (model_slot == 0)
		{
			m_armchest = model_path;
		}
		else if (model_slot == 1)
		{
			m_armchesta = model_path;
		}
		else if (model_slot == 2)
		{
			m_armchestb = model_path;
		}
		else if (model_slot == 3)
		{
			m_armchestc = model_path;
		}
		else if (model_slot == 4)
		{
			m_armchestd = model_path;
		}
		else if (model_slot == 5)
		{
			m_armchestf = model_path;
		}
		else if (model_slot == 6)
		{
			m_armchestj = model_path;
		}
	}
	else if (part_id == 4)
	{
		if (model_slot == 0)
		{
			m_armarms = model_path;
		}
		else if (model_slot == 1)
		{
			m_armarmsa = model_path;
		}
		else if (model_slot == 2)
		{
			m_armarmsb = model_path;
		}
		else if (model_slot == 3)
		{
			m_armarmsc = model_path;
		}
		else if (model_slot == 4)
		{
			m_armarmsd = model_path;
		}
		else if (model_slot == 5)
		{
			m_armarmsf = model_path;
		}
		else if (model_slot == 6)
		{
			m_armarmsj = model_path;
		}
	}
	else if (part_id == 5)
	{
		m_armhelmet = model_path;
	}
	else if (part_id == 6)
	{
		m_armshield = model_path;
	}
	else if (part_id == 7)
	{
		m_armtorch = model_path;
	}
}
void CActor::SetActorBodyPartModelPath(int part_id, const char* model_path)
{
	if (part_id == 1)
	{
		m_headmodel = model_path;
	}
	else if (part_id == 2)
	{
		m_upperbody_model = model_path;
	}
	else if (part_id == 3)
	{
		m_lowerbody_model = model_path;
	}
	else if (part_id == 4)
	{
		m_hands_model = model_path;
	}
	else if (part_id == 5)
	{
		m_foots_model = model_path;
	}
	else if (part_id == 6)
	{
		m_hairmodel = model_path;
	}
	else if (part_id == 7)
	{
		m_pppp = model_path;
	}
	else if (part_id == 8)
	{
		m_underwear01 = model_path;
	}
	else if (part_id == 9)
	{
		m_underwear02 = model_path;
	}
	else if (part_id == 10)
	{
		m_eye_l = model_path;
	}
	else if (part_id == 11)
	{
		m_eye_r = model_path;
	}
}
EntityId CActor::GetCurrentEquippedItemId(int id)
{
	if (id == eAESlot_Weapon)
	{
		return m_wpnselecteditem;
	}
	else if (id == eAESlot_Boots)
	{
		return m_wear_boots_id;
	}
	else if (id == eAESlot_Arms)
	{
		return m_wear_arms_id;
	}
	else if (id == eAESlot_Helmet)
	{
		return m_wear_helm_id;
	}
	else if (id == eAESlot_Torso)
	{
		return m_wear_curr_id;
	}
	else if (id == eAESlot_Pants)
	{
		return m_wear_pants_id;
	}
	else if (id == eAESlot_Shield)
	{
		return m_wear_shield_id;
	}
	else if (id == eAESlot_Torch)
	{
		return m_wear_torch_id;
	}
	else if (id == eAESlot_Ammo_Arrows)
	{
		return m_wear_ammoarrows_id;
	}
	else if (id == eAESlot_Weapon_Left)
	{
		return m_wpn_left_hand_id;
	}
	else if (id == eAESlot_Torso_Decor_1)
	{
		return m_wear_acc[eAESlot_Torso_Decor_1];
	}
	else if (id == eAESlot_Torso_Decor_2)
	{
		return m_wear_acc[eAESlot_Torso_Decor_2];
	}
	return 0;
}
void CActor::SetEquippedItemId(int id, EntityId item_id)
{
	if (id == eAESlot_Weapon)
	{
		m_wpnselecteditem = item_id;
	}
	else if (id == eAESlot_Boots)
	{
		m_wear_boots_id = item_id;
	}
	else if (id == eAESlot_Arms)
	{
		m_wear_arms_id = item_id;
	}
	else if (id == eAESlot_Helmet)
	{
		m_wear_helm_id = item_id;
	}
	else if (id == eAESlot_Torso)
	{
		m_wear_curr_id = item_id;
	}
	else if (id == eAESlot_Pants)
	{
		m_wear_pants_id = item_id;
	}
	else if (id == eAESlot_Shield)
	{
		m_wear_shield_id = item_id;
	}
	else if (id == eAESlot_Torch)
	{
		m_wear_torch_id = item_id;
	}
	else if (id == eAESlot_Ammo_Arrows)
	{
		m_wear_ammoarrows_id = item_id;
	}
	else if (id == eAESlot_Weapon_Left)
	{
		m_wpn_left_hand_id = item_id;
	}
	else if (id == eAESlot_Torso_Decor_1)
	{
		m_wear_acc[eAESlot_Torso_Decor_1] = item_id;
	}
	else if (id == eAESlot_Torso_Decor_2)
	{
		m_wear_acc[eAESlot_Torso_Decor_2] = item_id;
	}
}
void CActor::SetEquippedState(int state_id, bool state)
{
	if (state_id == eAESlot_Boots)
	{
		m_equipmentswitchboots = state;
	}
	else if (state_id == eAESlot_Pants)
	{
		m_equipmentswitchlegs = state;
	}
	else if (state_id == eAESlot_Torso)
	{
		m_equipmentswitchchest = state;
	}
	else if (state_id == eAESlot_Arms)
	{
		m_equipmentswitcharms = state;
	}
	else if (state_id == eAESlot_Helmet)
	{
		m_equipmentswitchhead = state;
	}
	else if (state_id == eAESlot_Shield)
	{
		m_equipmentswitchshield = state;
	}
	else if (state_id == eAESlot_Torch)
	{
		m_equipmentswitchtorch = state;
	}
}
bool CActor::GetEquippedState(int state_id)
{
	if (state_id == eAESlot_Boots)
	{
		return m_equipmentswitchboots;
	}
	else if (state_id == eAESlot_Pants)
	{
		return m_equipmentswitchlegs;
	}
	else if (state_id == eAESlot_Torso)
	{
		return m_equipmentswitchchest;
	}
	else if (state_id == eAESlot_Arms)
	{
		return m_equipmentswitcharms;
	}
	else if (state_id == eAESlot_Helmet)
	{
		return m_equipmentswitchhead;
	}
	else if (state_id == eAESlot_Shield)
	{
		return m_equipmentswitchshield;
	}
	else if (state_id == eAESlot_Torch)
	{
		return m_equipmentswitchtorch;
	}
	return false;
}
void CActor::SetEquippedArmorNumParts(int part_id, int part_num)
{
	if (part_id == eAESlot_Boots)
	{
		m_boots_num_parts = part_num;
	}
	else if (part_id == eAESlot_Pants)
	{
		m_llegs_num_parts = part_num;
	}
	else if (part_id == eAESlot_Torso)
	{
		m_chest_num_parts = part_num;
	}
	else if (part_id == eAESlot_Arms)
	{
		m_arms_num_parts = part_num;
	}
}
int CActor::GetEquippedArmorNumParts(int part_id)
{
	if (part_id == eAESlot_Boots)
	{
		return m_boots_num_parts;
	}
	else if (part_id == eAESlot_Pants)
	{
		return m_llegs_num_parts;
	}
	else if (part_id == eAESlot_Torso)
	{
		return m_chest_num_parts;
	}
	else if (part_id == eAESlot_Arms)
	{
		return m_arms_num_parts;
	}
	return 0;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
void CActor::AddAdditionalEquip()
{
	if (!additional_pack_add_request)
	{
		additional_pack_add_request = true;
		return;
	}

	if (!additional_pack_added)
	{
		//CryLogAlways("Try to add additional pack");
		SmartScriptTable props;
		IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		int dsgsdtgr;
		string additional_equipment_path;
		CScriptSetGetChain prop(props);
		prop.GetValue("add_equip_pack_path", additional_equipment_path);
		prop.GetValue("add_equip_pack_wear", dsgsdtgr);
		if (additional_equipment_path.empty())
		{
			additional_pack_added = true;
			return;
		}

		if (dsgsdtgr == 0)
		{
			this->AddAdditionalEquipNotWear();
			return;
		}

		/*IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
		if (!pxml)
			return;*/

		CryLogAlways("additional_equipment_path = %s", additional_equipment_path);
		string m_currenteqpack = additional_equipment_path;
		XmlNodeRef node = GetISystem()->LoadXmlFromFile(m_currenteqpack.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("AdditionalEquip xml not loaded.");
			return;
		}
		IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		IInventory *pInventory = this->GetInventory();
		XmlString itemname;
		int numitems;
		XmlNodeRef paramsNode = node->findChild("params");
		paramsNode->getAttr("numitems", numitems);
		for (int i = 0; i<numitems + 1; ++i)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;
			if (i <= 9)
				A_valstr = "item0";
			else if (i>9)
				A_valstr = "item";

			string D_valstr = A_valstr + C_valstr;
			if (paramsNode->getAttr(D_valstr.c_str(), itemname))
			{
				//gEnv->pGame->
				IActor *pActor = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(this->GetEntityId());
				EntityId ids = pItemSystem->GiveItem(pActor, itemname, false, false, false/*, "", EEntityFlags(ENTITY_FLAG_UNREMOVABLE | ENTITY_FLAG_PROCEDURAL)*/);
				//char LCT_valstr[17];
				//itoa(ids, LCT_valstr, 10);
				//CryLogAlways(LCT_valstr);
				//IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(itemname.c_str());
				//EntityId itemId = pInventory->GetItemByClass(pClass);
				IItem *pItem = pItemSystem->GetItem(ids);
				if (pItem)
				{
					//pItem->Drop(0, false, false);
					//pItem->PickUp(this->GetEntityId(), false, false, false);
					//this->EquipItem(pItem->GetEntityId());
					m_items_to_equip_pack1.push_back(pItem->GetEntityId());
				}
			}
		}
		additional_pack_added = true;
		return;
	}
}

void CActor::AddAdditionalEquipNotWear()
{
	if (!additional_pack_added)
	{
		SmartScriptTable props;
		IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		string additional_equipment_path;
		CScriptSetGetChain prop(props);
		prop.GetValue("add_equip_pack_path", additional_equipment_path);
		/*IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
		if (!pxml)
			return;*/

		if (additional_equipment_path.empty())
		{
			additional_pack_added = true;
			return;
		}

		CryLogAlways("additional_equipment_path = %s", additional_equipment_path);
		string m_currenteqpack = additional_equipment_path;
		XmlNodeRef node = GetISystem()->LoadXmlFromFile(m_currenteqpack.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("AdditionalEquip xml not loaded.");
			return;
		}
		IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		IInventory *pInventory = this->GetInventory();

		XmlString itemname;
		int numitems;
		XmlNodeRef paramsNode = node->findChild("params");
		paramsNode->getAttr("numitems", numitems);

		for (int i = 0; i<numitems + 1; ++i)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;

			if (i <= 9)
				A_valstr = "item0";
			else if (i>9)
				A_valstr = "item";

			string D_valstr = A_valstr + C_valstr;

			if (paramsNode->getAttr(D_valstr.c_str(), itemname))
			{
				pItemSystem->GiveItem(this, itemname, false, false, false);
			}
		}
		additional_pack_added = true;
		return;
	}
}

void CActor::AddAdditionalEquip2()
{
	if (!additional_pack2_add_request)
	{
		additional_pack2_add_request = true;
		return;
	}

	if (!additional_pack2_added)
	{
		//CryLogAlways("Try to add additional pack2");
		SmartScriptTable props;
		IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		int dsgsdtgr;
		string additional_equipment_path;
		CScriptSetGetChain prop(props);
		prop.GetValue("add_equip_pack_path2", additional_equipment_path);
		prop.GetValue("add_equip_pack_wear2", dsgsdtgr);

		if (dsgsdtgr == 0)
		{
			this->AddAdditionalEquipNotWear();
			return;
		}

		/*IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
		if (!pxml)
			return;*/

		if (additional_equipment_path.empty())
		{
			additional_pack2_added = true;
			return;
		}

		CryLogAlways("additional_equipment_path = %s", additional_equipment_path);
		string m_currenteqpack = additional_equipment_path;
		XmlNodeRef node = GetISystem()->LoadXmlFromFile(m_currenteqpack.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("AdditionalEquip xml not loaded.");
			return;
		}
		IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		IInventory *pInventory = this->GetInventory();
		XmlString itemname;
		int numitems;
		XmlNodeRef paramsNode = node->findChild("params");
		paramsNode->getAttr("numitems", numitems);
		for (int i = 0; i<numitems + 1; ++i)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;
			if (i <= 9)
				A_valstr = "item0";
			else if (i>9)
				A_valstr = "item";

			string D_valstr = A_valstr + C_valstr;
			if (paramsNode->getAttr(D_valstr.c_str(), itemname))
			{
				IActor *pActor = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(this->GetEntityId());
				EntityId ids = pItemSystem->GiveItem(pActor, itemname, false, false, false);
				/*IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(itemname.c_str());
				EntityId itemId = pInventory->GetItemByClass(pClass);
				IItem *pItem = pItemSystem->GetItem(itemId);*/
				IItem *pItem = pItemSystem->GetItem(ids);
				if (pItem)
				{
					//CryLogAlways("item added ti stack from equipment pack 2");
					//this->EquipItem(pItem->GetEntityId());
					m_items_to_equip_pack2.push_back(pItem->GetEntityId());
				}
			}
		}
		additional_pack2_added = true;
		return;
	}
}

void CActor::AddAdditionalEquipNotWear2()
{
	if (!additional_pack2_added)
	{
		SmartScriptTable props;
		IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		string additional_equipment_path;
		CScriptSetGetChain prop(props);
		prop.GetValue("add_equip_pack_path2", additional_equipment_path);
		/*IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
		if (!pxml)
			return;*/
		if (additional_equipment_path.empty())
		{
			additional_pack2_added = true;
			return;
		}

		CryLogAlways("additional_equipment_path = %s", additional_equipment_path);
		string m_currenteqpack = additional_equipment_path;
		XmlNodeRef node = GetISystem()->LoadXmlFromFile(m_currenteqpack.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("AdditionalEquip xml not loaded.");
			return;
		}
		IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		IInventory *pInventory = this->GetInventory();

		XmlString itemname;
		int numitems;
		XmlNodeRef paramsNode = node->findChild("params");
		paramsNode->getAttr("numitems", numitems);

		for (int i = 0; i<numitems + 1; ++i)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A_valstr;

			if (i <= 9)
				A_valstr = "item0";
			else if (i>9)
				A_valstr = "item";

			string D_valstr = A_valstr + C_valstr;

			if (paramsNode->getAttr(D_valstr.c_str(), itemname))
			{
				pItemSystem->GiveItem(this, itemname, false, false, false);
			}
		}
		additional_pack2_added = true;
		return;
	}
}

void CActor::AddAdditionalEquipPermoment(const char *eqpack, bool equip)
{
	//CryLogAlways("AddAdditionalEquipPermoment eqpack = %s", eqpack);
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(eqpack);
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("AdditionalEquip xml not loaded.");
		return;
	}

	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	IInventory *pInventory = this->GetInventory();

	XmlString itemname;
	int numitems;
	XmlNodeRef paramsNode = node->findChild("params");
	paramsNode->getAttr("numitems", numitems);

	for (int i = 0; i<numitems; ++i)
	{
		char B_valstr[17];
		itoa(i, B_valstr, 10);
		string C_valstr = B_valstr;
		string A_valstr;

		if (i <= 9)
			A_valstr = "item0";
		else if (i>9)
			A_valstr = "item";

		string D_valstr = A_valstr + C_valstr;

		if (paramsNode->getAttr(D_valstr.c_str(), itemname))
		{
			EntityId ccItemId = pItemSystem->GiveItem(this, itemname, false, false, false);
			if (equip)
			{
				CItem *pItem = GetItem(ccItemId);
				if (pItem)
				{
					EquipItem(ccItemId);
				}
			}
		}
	}
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
bool CActor::EquipItem(EntityId itemId)
{
	if (IsRemote())
	{
		return Net_EquipItem(itemId);
	}

	CItem *pItem = GetItem(itemId);
	if (!pItem)
		return false;

	pItem->ClearItemVisuals();
	bool imd_equip = false;
	CHUDCommon* pHudComm = g_pGame->GetHUDCommon();
	EntityId item_equ_or_deq = 0;
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return false;

	if (pItem->GetGenderRestruction()>0)
	{
		if (this->GetGender() == 0 && pItem->GetGenderRestruction() == 1)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item due gender restruction( this item only for female)");

			return false;
		}
		else if (this->GetGender() == 1 && pItem->GetGenderRestruction() == 2)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item due gender restruction( this item only for male)");

			return false;
		}
	}

	if (pItem && pItem->IsWpnTwoHanded())
	{
		if (this->IsPlayer() && !IsRemote() && (GetEquippedState(eAESlot_Shield) == true || GetEquippedState(eAESlot_Torch) == true))
		{
			pHudComm->StatMsg("You can not equip this item with busy left hand");

			this->DeSelectItem(pItem->GetEntityId(), true);

			return false;
		}
	}

	if (pItem->GetArmorType() == 1)
	{
		if (GetEquippedState(eAESlot_Boots) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Boots, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Boots) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Boots);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Boots, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 2)
	{
		if (GetEquippedState(eAESlot_Pants) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Pants, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Pants) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Pants);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Pants, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 3)
	{
		if (GetEquippedState(eAESlot_Torso) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Torso) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Torso, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 4)
	{
		if (GetEquippedState(eAESlot_Arms) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Arms, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Arms) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Arms);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Arms, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 5)
	{
		if (GetEquippedState(eAESlot_Helmet) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Helmet, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Helmet) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Helmet);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Helmet, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 6)
	{
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso_Decor_1);
		CItem *pItemiii = GetItem(ide);
		if (!pItemiii)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_1);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso_Decor_1, id);
			item_equ_or_deq = itemId;
		}
		else if (pItemiii && pItemiii->GetEquipped()>0)
		{
			nwAction->DeEquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_1);
			pItem->SetEquipped(0);
			pItemiii->SetEquipped(0);
			item_equ_or_deq = pItemiii->GetEntityId();
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 7)
	{
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso_Decor_2);
		CItem *pItemiii = GetItem(ide);
		if (!pItemiii)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_2);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso_Decor_2, id);
			item_equ_or_deq = itemId;
		}
		else if (pItemiii && pItemiii->GetEquipped()>0)
		{
			nwAction->DeEquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_2);
			pItem->SetEquipped(0);
			pItemiii->SetEquipped(0);
			item_equ_or_deq = pItemiii->GetEntityId();
			imd_equip = true;
		}
	}
	else if (pItem->GetShield() == 1)
	{
		//CryLogAlways("TryEquipShield");
		if (GetEquippedState(eAESlot_Shield) == false)
		{

			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}

			if (GetEquippedState(eAESlot_Torch) == true)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item slot is busy");

				return false;
			}

			CItem *pItm_wpn = static_cast<CItem*>(this->GetCurrentItem());
			if (pItm_wpn && pItm_wpn->IsWpnTwoHanded())
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item with equipped two-handed weapon");

				return false;
			}

			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			SetEquippedItemId(eAESlot_Shield, itemId);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Shield) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Shield);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Shield, 0);

			imd_equip = true;
		}
	}
	else if (pItem->GetItemType() == 3)
	{
		if (pItem->IsDestroyed())
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		nwAction->UseItemCommon(this->GetEntityId(), itemId);
		item_equ_or_deq = itemId;
	}
	else if (pItem->GetItemType() == 4)
	{
		if (GetEquippedState(eAESlot_Torch) == false)
		{

			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}

			if (GetEquippedState(eAESlot_Shield) == true)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item slot is busy");

				return false;
			}

			CItem *pItm_wpn = static_cast<CItem*>(this->GetCurrentItem());
			
			if (pItm_wpn && pItm_wpn->IsWpnTwoHanded())
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item with equipped two-handed weapon");

				return false;
			}

			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torch, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Torch) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Torch);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				item_equ_or_deq = pItemiii->GetEntityId();
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), ide);
				pItemiii->SetEquipped(0);
			}
			SetEquippedItemId(eAESlot_Torch, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetItemType() == 5)//spell books
	{
		if (pItem->IsDestroyed())
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		nwAction->UseItemCommon(this->GetEntityId(), itemId);
		item_equ_or_deq = itemId;
	}
	else if (pItem->GetItemType() == 9)//Arrows
	{
		if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		CItem *pItemWp = GetItem(this->GetCurrentEquippedItemId(eAESlot_Weapon));
		if (GetCurrentEquippedItemId(eAESlot_Ammo_Arrows) <= 0)
		{
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Ammo_Arrows, id);
			item_equ_or_deq = itemId;
			if (pItemWp && pItemWp->GetWeaponType() == 9)
			{
				int idr = pItemWp->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = pItemWp->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					pFmd->SetAmmoType(pItem->GetItemAmmoType());
					pFmd->Activate(false);
					pFmd->Activate(true);
				}
			}
		}
		else if (GetCurrentEquippedItemId(eAESlot_Ammo_Arrows) > 0)
		{
			CBow *pBow = static_cast<CBow*>(GetWeapon(GetCurrentItemId()));
			if (pBow)
			{
				pBow->ForcedStopFire();
			}
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Ammo_Arrows);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				item_equ_or_deq = pItemiii->GetEntityId();
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), ide);
				pItemiii->SetEquipped(0);
			}
			SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
			imd_equip = true;

			if (pItemWp && pItemWp->GetWeaponType() == 9)
			{
				int idr = pItemWp->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = pItemWp->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					pFmd->SetAmmoType("noarrow");
				}
			}
		}
		/*CItem *pItemiii = GetItem(this->GetCurrentEquippedItemId(eAESlot_Weapon));
		CItem *pItemiiii = GetItem(this->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows));
		if (pItemiii && pItemiii->GetWeaponType() == 9 && !pItemiiii)
		{
			
			int idr = pItemiii->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItemiii->GetIWeapon()->GetFireMode(idr);
			if (pFmd && pItem->GetEquipped() == 0 && pItem->GetItemQuanity()>0)
			{
				this->SetEquippedItemId(eAESlot_Ammo_Arrows, pItem->GetEntity()->GetId());
				pFmd->SetAmmoType(pItem->GetItemAmmoType());
				pFmd->Activate(false);
				pFmd->Activate(true);
				pItem->SetEquipped(1);
				nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
				item_equ_or_deq = itemId;

			}
			else if (pFmd && pItem->GetEquipped() == 1 && pItem->GetItemQuanity()>0)
			{
				this->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
				pItem->SetEquipped(0);
				pFmd->SetAmmoType("noarrow");
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
				item_equ_or_deq = itemId;
			}
			
		}
		else if (pItemiii && pItemiii->GetWeaponType() == 9 && pItemiiii)
		{
			CBow *pBow = static_cast<CBow*>(pItemiii);
			if (pBow)
			{
				if (pItemiii->GetItemAmmoType() != pItemiiii->GetItemAmmoType())
				{
					if (pBow->arrow_loaded)
					{
						if (this->IsPlayer() && !IsRemote())
							pHudComm->StatMsg("You can not equip this item. Please unload arrow on your bow");

						return false;
					}
				}
			}
			int idr = pItemiii->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItemiii->GetIWeapon()->GetFireMode(idr);
			if (pFmd && pItem->GetEquipped() == 1 && pItem->GetItemQuanity()>0)
			{
				this->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
				pItem->SetEquipped(0);
				pFmd->SetAmmoType("noarrow");
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), pItemiiii->GetEntityId());
				imd_equip = true;
				item_equ_or_deq = pItemiiii->GetEntityId();
			}	
		}
		else if (pItem->GetEquipped() == 0 && !pItemiiii)
		{
			this->SetEquippedItemId(eAESlot_Ammo_Arrows, pItem->GetEntity()->GetId());
			pItem->SetEquipped(1);
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			item_equ_or_deq = itemId;
		}
		else
		{
			this->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
			pItem->SetEquipped(0);
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			imd_equip = true;
			item_equ_or_deq = itemId;
		}*/
	}
	else if (pItem->GetItemType() == 10)//Bolts
	{
		if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		CItem *pItemiiii = GetItem(GetCurrentEquippedItemId(eAESlot_Ammo_Bolts));
		if (!pItemiiii)
		{
			nwAction->EquipArmorOrCloth(GetEntityId(), itemId);
			pItem->SetEquipped(1);
			SetEquippedItemId(eAESlot_Ammo_Bolts, itemId);
			item_equ_or_deq = itemId;
		}
		else
		{
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Ammo_Bolts);
			nwAction->DeEquipArmorOrCloth(GetEntityId(), ide);
			item_equ_or_deq = pItemiiii->GetEntityId();
			pItemiiii->SetEquipped(0);
			SetEquippedItemId(eAESlot_Ammo_Bolts, 0);
			imd_equip = true;
			CWeapon *pWpn = GetWeapon(GetCurrentItemId());
			if (pWpn && pWpn->GetWeaponType() == 11)
			{
				CCrossBow *pCrossBow = static_cast<CCrossBow*>(pWpn);
				if (pCrossBow)
				{
					pCrossBow->ResetLoadedState();
				}
			}
		}
	}

	if (CItem *lpItem = GetItem(item_equ_or_deq))
	{
		if (lpItem->GetEquipped() == 0)
		{
			lpItem->TryRunOnEquipLuaFunc(false);
		}
		else
		{
			lpItem->TryRunOnEquipLuaFunc(true);
		}
	}

	if (g_pGameCVars->g_equipment_system_immediately_equip_enable == 1)
	{
		if (imd_equip)
		{
			if (item_equ_or_deq != itemId)
			{
				imd_equip_at_next = true;
				m_actor_items_to_imd_equip.push_back(itemId);
			}
		}
	}

	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
			GetGameObject()->InvokeRMI(CActor::ClEquipItem(), CActor::EquipItemParams(itemId, (pItem->GetEquipped() > 0), 0), eRMI_ToRemoteClients);
		else
		{
			GetGameObject()->InvokeRMI(CActor::SvEquipItem(), CActor::EquipItemParams(itemId, (pItem->GetEquipped() > 0), 0), eRMI_ToServer);
		}
	}

	SendPhyAndAiUpdateEvent();
	return true;
}

bool CActor::Net_EquipItem(EntityId itemId)
{
	CryLogAlways("CActor::Net_EquipItem");
	CItem *pItem = GetItem(itemId);
	if (!pItem)
	{
		CryLogAlways("CActor::Net_EquipItem have no item!");
		return false;
	}

	if (pItem->GetOwnerId() != GetEntityId())
	{
		CryLogAlways("CActor::Net_EquipItem item owner is not this actor!");
		return false;
	}

	pItem->ClearItemVisuals();
	bool imd_equip = false;
	CHUDCommon* pHudComm = g_pGame->GetHUDCommon();
	EntityId item_equ_or_deq = 0;
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return false;

	if (pItem->GetGenderRestruction()>0)
	{
		if (this->GetGender() == 0 && pItem->GetGenderRestruction() == 1)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item due gender restruction( this item only for female)");

			return false;
		}
		else if (this->GetGender() == 1 && pItem->GetGenderRestruction() == 2)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item due gender restruction( this item only for male)");

			return false;
		}
	}

	if (pItem && pItem->IsWpnTwoHanded())
	{
		if (this->IsPlayer() && !IsRemote() && (GetEquippedState(eAESlot_Shield) == true || GetEquippedState(eAESlot_Torch) == true))
		{
			pHudComm->StatMsg("You can not equip this item with busy left hand");

			this->DeSelectItem(pItem->GetEntityId(), true);

			return false;
		}
	}

	if (pItem->GetArmorType() == 1)
	{
		if (GetEquippedState(eAESlot_Boots) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Boots, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Boots) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Boots);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Boots, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 2)
	{
		if (GetEquippedState(eAESlot_Pants) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Pants, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Pants) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Pants);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Pants, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 3)
	{
		if (GetEquippedState(eAESlot_Torso) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Torso) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Torso, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 4)
	{
		if (GetEquippedState(eAESlot_Arms) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Arms, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Arms) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Arms);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Arms, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 5)
	{
		if (GetEquippedState(eAESlot_Helmet) == false)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Helmet, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Helmet) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Helmet);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Helmet, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 6)
	{
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso_Decor_1);
		CItem *pItemiii = GetItem(ide);
		if (!pItemiii)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_1);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso_Decor_1, id);
			item_equ_or_deq = itemId;
		}
		else if (pItemiii && pItemiii->GetEquipped()>0)
		{
			nwAction->DeEquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_1);
			pItem->SetEquipped(0);
			pItemiii->SetEquipped(0);
			item_equ_or_deq = pItemiii->GetEntityId();
			imd_equip = true;
		}
	}
	else if (pItem->GetArmorType() == 7)
	{
		EntityId ide = GetCurrentEquippedItemId(eAESlot_Torso_Decor_2);
		CItem *pItemiii = GetItem(ide);
		if (!pItemiii)
		{
			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}
			nwAction->EquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_2);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torso_Decor_2, id);
			item_equ_or_deq = itemId;
		}
		else if (pItemiii && pItemiii->GetEquipped()>0)
		{
			nwAction->DeEquipDecorationItem(this->GetEntityId(), itemId, eAESlot_Torso_Decor_2);
			pItem->SetEquipped(0);
			pItemiii->SetEquipped(0);
			item_equ_or_deq = pItemiii->GetEntityId();
			imd_equip = true;
		}
	}
	else if (pItem->GetShield() == 1)
	{
		//CryLogAlways("TryEquipShield");
		if (GetEquippedState(eAESlot_Shield) == false)
		{

			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}

			if (GetEquippedState(eAESlot_Torch) == true)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item slot is busy");

				return false;
			}

			CItem *pItm_wpn = static_cast<CItem*>(this->GetCurrentItem());
			if (pItm_wpn && pItm_wpn->IsWpnTwoHanded())
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item with equipped two-handed weapon");

				return false;
			}

			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			SetEquippedItemId(eAESlot_Shield, itemId);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Shield) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Shield);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				pItemiii->SetEquipped(0);
				item_equ_or_deq = pItemiii->GetEntityId();
			}
			SetEquippedItemId(eAESlot_Shield, 0);

			imd_equip = true;
		}
	}
	else if (pItem->GetItemType() == 3)
	{
		if (pItem->IsDestroyed())
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		nwAction->UseItemCommon(this->GetEntityId(), itemId);
		item_equ_or_deq = itemId;
	}
	else if (pItem->GetItemType() == 4)
	{
		if (GetEquippedState(eAESlot_Torch) == false)
		{

			if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item is broken");

				return false;
			}

			if (GetEquippedState(eAESlot_Shield) == true)
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item. Item slot is busy");

				return false;
			}

			CItem *pItm_wpn = static_cast<CItem*>(this->GetCurrentItem());

			if (pItm_wpn && pItm_wpn->IsWpnTwoHanded())
			{
				if (this->IsPlayer() && !IsRemote())
					pHudComm->StatMsg("You can not equip this item with equipped two-handed weapon");

				return false;
			}

			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Torch, id);
			item_equ_or_deq = itemId;
		}
		else if (GetEquippedState(eAESlot_Torch) == true)
		{
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Torch);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				item_equ_or_deq = pItemiii->GetEntityId();
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), ide);
				pItemiii->SetEquipped(0);
			}
			SetEquippedItemId(eAESlot_Torch, 0);
			imd_equip = true;
		}
	}
	else if (pItem->GetItemType() == 5)//spell books
	{
		if (pItem->IsDestroyed())
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		nwAction->UseItemCommon(this->GetEntityId(), itemId);
		item_equ_or_deq = itemId;
	}
	else if (pItem->GetItemType() == 9)//Arrows
	{
		if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		CItem *pItemWp = GetItem(this->GetCurrentEquippedItemId(eAESlot_Weapon));
		if (GetCurrentEquippedItemId(eAESlot_Ammo_Arrows) <= 0)
		{
			nwAction->EquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(1);
			int id = itemId;
			SetEquippedItemId(eAESlot_Ammo_Arrows, id);
			item_equ_or_deq = itemId;
			if (pItemWp && pItemWp->GetWeaponType() == 9)
			{
				int idr = pItemWp->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = pItemWp->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					pFmd->SetAmmoType(pItem->GetItemAmmoType());
					pFmd->Activate(false);
					pFmd->Activate(true);
				}
			}
		}
		else if (GetCurrentEquippedItemId(eAESlot_Ammo_Arrows) > 0)
		{
			CBow *pBow = static_cast<CBow*>(GetWeapon(GetCurrentItemId()));
			if (pBow)
			{
				pBow->ForcedStopFire();
			}
			nwAction->DeEquipArmorOrCloth(this->GetEntityId(), itemId);
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Ammo_Arrows);
			CItem *pItemiii = GetItem(ide);
			if (pItemiii)
			{
				item_equ_or_deq = pItemiii->GetEntityId();
				nwAction->DeEquipArmorOrCloth(this->GetEntityId(), ide);
				pItemiii->SetEquipped(0);
			}
			SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
			imd_equip = true;

			if (pItemWp && pItemWp->GetWeaponType() == 9)
			{
				int idr = pItemWp->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = pItemWp->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					pFmd->SetAmmoType("noarrow");
				}
			}
		}
	}
	else if (pItem->GetItemType() == 10)//Bolts
	{
		if (pItem->IsDestroyed() && pItem->GetEquipped() == 0)
		{
			if (this->IsPlayer() && !IsRemote())
				pHudComm->StatMsg("You can not equip this item. Item is broken");

			return false;
		}
		CItem *pItemiiii = GetItem(GetCurrentEquippedItemId(eAESlot_Ammo_Bolts));
		if (!pItemiiii)
		{
			nwAction->EquipArmorOrCloth(GetEntityId(), itemId);
			pItem->SetEquipped(1);
			SetEquippedItemId(eAESlot_Ammo_Bolts, itemId);
			item_equ_or_deq = itemId;
		}
		else
		{
			pItem->SetEquipped(0);
			EntityId ide = GetCurrentEquippedItemId(eAESlot_Ammo_Bolts);
			nwAction->DeEquipArmorOrCloth(GetEntityId(), ide);
			item_equ_or_deq = pItemiiii->GetEntityId();
			pItemiiii->SetEquipped(0);
			SetEquippedItemId(eAESlot_Ammo_Bolts, 0);
			imd_equip = true;
			CWeapon *pWpn = GetWeapon(GetCurrentItemId());
			if (pWpn && pWpn->GetWeaponType() == 11)
			{
				CCrossBow *pCrossBow = static_cast<CCrossBow*>(pWpn);
				if (pCrossBow)
				{
					pCrossBow->ResetLoadedState();
				}
			}
		}
	}

	if (CItem *lpItem = GetItem(item_equ_or_deq))
	{
		if (lpItem->GetEquipped() == 0)
		{
			lpItem->TryRunOnEquipLuaFunc(false);
		}
		else
		{
			lpItem->TryRunOnEquipLuaFunc(true);
		}
	}

	if (g_pGameCVars->g_equipment_system_immediately_equip_enable == 1)
	{
		if (imd_equip)
		{
			if (item_equ_or_deq != itemId)
			{
				imd_equip_at_next = true;
				m_actor_items_to_imd_equip.push_back(itemId);
			}
		}
	}

	SendPhyAndAiUpdateEvent();
	return true;
}

float CActor::GetActorStrength() const
{
	float strength = (float)this->GetStrength();
	return strength;
}

void CActor::SetLevelXp(int levelxp)
{
	m_levelxp = (float)levelxp;
}

void CActor::SetXpToNextLevel(int xptonextlevel)
{
	m_xptonextlevel = (float)xptonextlevel;
}

void CActor::SetHeadType(int plheadtype)
{
	m_plheadtype = plheadtype;
}

void CActor::SetHairType(int plhairtype)
{
	m_plhairtype = plhairtype;
}

void CActor::SetEyesType(int pleyestype)
{
	m_pleyestype = pleyestype;
}



void CActor::AddBuff(int id, float time, int eff_id)
{
	if(IsRemote())
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	SActorBuff* pActorBuff = NULL;
	pActorBuff = pGameGlobals->GetCharacterBuffInfo(id);
	if (!pActorBuff)
		return;

	if (time <= 0.0f)
		time = pActorBuff->start_time;

	std::vector<SActorTimedEffectsFtg>::iterator it = m_timed_effects_buffs.begin();
	std::vector<SActorTimedEffectsFtg>::iterator end = m_timed_effects_buffs.end();
	int count = m_timed_effects_buffs.size();
	int request_to_clear_unstackable_buff[3];
	request_to_clear_unstackable_buff[1] = 0;
	request_to_clear_unstackable_buff[2] = 0;
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SActorTimedEffectsFtg effect = *it;
		if (effect.effect_id == pActorBuff->buff_effect_id && effect.buff_id != pActorBuff->buff_id)
		{
			if (effect.effect_id > 0)
			{
				request_to_clear_unstackable_buff[1] = 7;
				request_to_clear_unstackable_buff[2] = effect.buff_id;
				break;
			}
		}

		if (effect.buff_id == pActorBuff->buff_id)
		{
			if (time > 0.0f)
			{
				if (GetBuffTime(effect.buff_id) > pActorBuff->start_time)
					time = GetBuffTime(effect.buff_id);
			}

			if (IsPlayer() && !IsRemote())
			{
				CHUDCommon* pHud = g_pGame->GetHUDCommon();
				if (pHud && time > 0.0f)
				{
					pHud->RemoveBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_type);
					pHud->AddBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_icon_path.c_str(), (int)time, pActorBuff->buff_type);
				}
			}
			this->ActivateBuff(pActorBuff->buff_id, time, true, pActorBuff->buff_effect_id);
			//---------- special effects for baff from lua or//////////////////////
			int actor_id = GetEntityId();
			int buff_id = pActorBuff->buff_id;
			for (int i = 1; i < 10; i++)
			{
				if (pActorBuff->r_val[i].empty())
					continue;

				if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
				{
					IScriptSystem* pSS = pActScript->GetScriptSystem();
					if (pActScript->GetValueType(pActorBuff->r_val[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val[i].c_str()))
					{
						pSS->PushFuncParam(pActScript);
						pSS->PushFuncParam(ScriptHandle(actor_id));
						pSS->PushFuncParam(buff_id);
						pSS->EndCall();
					}
				}
			}
			//----------------------------------------------------------------------
			delete(pActorBuff);

			if (gEnv->bMultiplayer)
			{
				if (gEnv->bServer)
					GetGameObject()->InvokeRMI(CActor::ClAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToRemoteClients);
				else
				{
					GetGameObject()->InvokeRMI(CActor::SvAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToServer);
				}
			}

			return;
		}
	}

	if (request_to_clear_unstackable_buff[1] > 0)
	{
		std::vector<SActorTimedEffectsFtg>::iterator it = m_timed_effects_buffs.begin();
		std::vector<SActorTimedEffectsFtg>::iterator end = m_timed_effects_buffs.end();
		int count = m_timed_effects_buffs.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			SActorTimedEffectsFtg effect = *it;
			if (effect.buff_id == request_to_clear_unstackable_buff[2])
			{
				this->ActivateBuff(effect.buff_id, 0.0f, false);
				m_timed_effects_buffs.erase(it);
				m_timed_effects_buffs.resize(count - 1);
				break;
			}
		}
	}

	if (IsPlayer() && !IsRemote())
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (pHud && time > 0.0f)
		{
			pHud->AddBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_icon_path.c_str(), (int)time, pActorBuff->buff_type);
		}
	}
	//---------- special effects for baff from lua or//////////////////////
	int actor_id = GetEntityId();
	int buff_id = pActorBuff->buff_id;
	for (int i = 1; i < 10; i++)
	{
		if (pActorBuff->r_val[i].empty())
			continue;

		if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
		{
			IScriptSystem* pSS = pActScript->GetScriptSystem();
			if (pActScript->GetValueType(pActorBuff->r_val[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val[i].c_str()))
			{
				pSS->PushFuncParam(pActScript);
				pSS->PushFuncParam(ScriptHandle(actor_id));
				pSS->PushFuncParam(buff_id);
				pSS->EndCall();
			}
		}

	}
	//----------------------------------------------------------------------
	this->ActivateBuff(pActorBuff->buff_id, time, true, pActorBuff->buff_effect_id);
	delete(pActorBuff);

	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
			GetGameObject()->InvokeRMI(CActor::ClAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToRemoteClients);
		else
		{
			GetGameObject()->InvokeRMI(CActor::SvAddBuff(), CActor::AcEffectParams(id, time, eff_id), eRMI_ToServer);
		}
	}
}

void CActor::DelBuff(int id)
{
	if (IsRemote())
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	SActorBuff* pActorBuff = NULL;
	pActorBuff = pGameGlobals->GetCharacterBuffInfo(id);
	if (!pActorBuff)
		return;

	if (IsPlayer() && !IsRemote())
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (pHud)
		{
			pHud->RemoveBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_type);
		}
	}
	//---------- special effects for baff from lua or//////////////////////
	int actor_id = GetEntityId();
	int buff_id = pActorBuff->buff_id;
	for (int i = 1; i < 10; i++)
	{
		if (pActorBuff->r_val_c[i].empty())
			continue;

		if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
		{
			IScriptSystem* pSS = pActScript->GetScriptSystem();
			if (pActScript->GetValueType(pActorBuff->r_val_c[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val_c[i].c_str()))
			{
				pSS->PushFuncParam(pActScript);
				pSS->PushFuncParam(ScriptHandle(actor_id));
				pSS->PushFuncParam(buff_id);
				pSS->EndCall();
			}
		}

	}
	//----------------------------------------------------------------------
	delete(pActorBuff);

	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
			GetGameObject()->InvokeRMI(CActor::ClDelBuff(), CActor::AcEffectParams(id, 0.0f, 0), eRMI_ToRemoteClients);
		else
		{
			GetGameObject()->InvokeRMI(CActor::SvDelBuff(), CActor::AcEffectParams(id, 0.0f, 0), eRMI_ToServer);
		}
	}
}

void CActor::Net_AddBuff(int id, float time, int eff_id)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	SActorBuff* pActorBuff = NULL;
	pActorBuff = pGameGlobals->GetCharacterBuffInfo(id);
	if (!pActorBuff)
		return;

	if (time <= 0.0f)
		time = pActorBuff->start_time;

	std::vector<SActorTimedEffectsFtg>::iterator it = m_timed_effects_buffs.begin();
	std::vector<SActorTimedEffectsFtg>::iterator end = m_timed_effects_buffs.end();
	int count = m_timed_effects_buffs.size();
	int request_to_clear_unstackable_buff[3];
	request_to_clear_unstackable_buff[1] = 0;
	request_to_clear_unstackable_buff[2] = 0;
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SActorTimedEffectsFtg effect = *it;
		if (effect.effect_id == pActorBuff->buff_effect_id && effect.buff_id != pActorBuff->buff_id)
		{
			if (effect.effect_id > 0)
			{
				request_to_clear_unstackable_buff[1] = 7;
				request_to_clear_unstackable_buff[2] = effect.buff_id;
				break;
			}
		}

		if (effect.buff_id == pActorBuff->buff_id)
		{
			if (time > 0.0f)
			{
				if (GetBuffTime(effect.buff_id) > pActorBuff->start_time)
					time = GetBuffTime(effect.buff_id);
			}

			if (IsPlayer() && !IsRemote())
			{
				CHUDCommon* pHud = g_pGame->GetHUDCommon();
				if (pHud && time > 0.0f)
				{
					pHud->RemoveBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_type);
					pHud->AddBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_icon_path.c_str(), (int)time, pActorBuff->buff_type);
				}
			}
			this->ActivateBuff(pActorBuff->buff_id, time, true, pActorBuff->buff_effect_id);
			//---------- special effects for baff from lua or//////////////////////
			int actor_id = GetEntityId();
			int buff_id = pActorBuff->buff_id;
			for (int i = 1; i < 10; i++)
			{
				if (pActorBuff->r_val[i].empty())
					continue;

				if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
				{
					IScriptSystem* pSS = pActScript->GetScriptSystem();
					if (pActScript->GetValueType(pActorBuff->r_val[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val[i].c_str()))
					{
						pSS->PushFuncParam(pActScript);
						pSS->PushFuncParam(ScriptHandle(actor_id));
						pSS->PushFuncParam(buff_id);
						pSS->EndCall();
					}
				}
			}
			//----------------------------------------------------------------------
			delete(pActorBuff);
			return;
		}
	}

	if (request_to_clear_unstackable_buff[1] > 0)
	{
		std::vector<SActorTimedEffectsFtg>::iterator it = m_timed_effects_buffs.begin();
		std::vector<SActorTimedEffectsFtg>::iterator end = m_timed_effects_buffs.end();
		int count = m_timed_effects_buffs.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			SActorTimedEffectsFtg effect = *it;
			if (effect.buff_id == request_to_clear_unstackable_buff[2])
			{
				this->ActivateBuff(effect.buff_id, 0.0f, false);
				m_timed_effects_buffs.erase(it);
				m_timed_effects_buffs.resize(count - 1);
				break;
			}
		}
	}

	if (IsPlayer() && !IsRemote())
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (pHud && time > 0.0f)
		{
			pHud->AddBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_icon_path.c_str(), (int)time, pActorBuff->buff_type);
		}
	}
	//---------- special effects for baff from lua or//////////////////////
	int actor_id = GetEntityId();
	int buff_id = pActorBuff->buff_id;
	for (int i = 1; i < 10; i++)
	{
		if (pActorBuff->r_val[i].empty())
			continue;

		if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
		{
			IScriptSystem* pSS = pActScript->GetScriptSystem();
			if (pActScript->GetValueType(pActorBuff->r_val[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val[i].c_str()))
			{
				pSS->PushFuncParam(pActScript);
				pSS->PushFuncParam(ScriptHandle(actor_id));
				pSS->PushFuncParam(buff_id);
				pSS->EndCall();
			}
		}

	}
	//----------------------------------------------------------------------
	this->ActivateBuff(pActorBuff->buff_id, time, true, pActorBuff->buff_effect_id);
	delete(pActorBuff);
}

void CActor::Net_DelBuff(int id)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	SActorBuff* pActorBuff = NULL;
	pActorBuff = pGameGlobals->GetCharacterBuffInfo(id);
	if (!pActorBuff)
		return;

	if (IsPlayer() && !IsRemote())
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (pHud)
		{
			pHud->RemoveBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_type);
		}
	}
	//---------- special effects for baff from lua or//////////////////////
	int actor_id = GetEntityId();
	int buff_id = pActorBuff->buff_id;
	for (int i = 1; i < 10; i++)
	{
		if (pActorBuff->r_val_c[i].empty())
			continue;

		if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
		{
			IScriptSystem* pSS = pActScript->GetScriptSystem();
			if (pActScript->GetValueType(pActorBuff->r_val_c[i].c_str()) == svtFunction && pSS->BeginCall(pActScript, pActorBuff->r_val_c[i].c_str()))
			{
				pSS->PushFuncParam(pActScript);
				pSS->PushFuncParam(ScriptHandle(actor_id));
				pSS->PushFuncParam(buff_id);
				pSS->EndCall();
			}
		}

	}
	//----------------------------------------------------------------------
	delete(pActorBuff);
}

bool CActor::GetBuffActive(int id)
{
	if (id > -1 && id <= 65534)
		return buffs[id];
	else
		return false;
}

float CActor::GetBuffTime(int id)
{
	if (id > -1 && id <= 65534)
		return buffs_time[id];
	else
		return 0.0f;
}

void CActor::AddBuffTime(int id, float time)
{
	if (id > -1 && id <= 65534)
	{
		buffs_time[id] += time;
	}
}

void CActor::SendPhyAndAiUpdateEvent()
{
	if (GetEntity()/* && !IsPlayer()*/)
	{
		SEntityEvent evt(ENTITY_EVENT_ENABLE_PHYSICS);
		evt.nParam[0] = true;
		GetEntity()->SendEvent(evt);
	}
}

void CActor::ActivateBuff(int id, float time, bool active, int eff_id)
{
	if (active == true)
	{
		bool act = false;

		if (id == 1 && !buffs[id])
		{
			hpreg_base += 1;
			act = true;
		}
		else if (id == 2 && !buffs[id])
		{
			mpreg_base += 2;
			act = true;
		}
		else if (id == 3 && !buffs[id])
		{
			streg_base += 3;
			act = true;
		}
		else if (id == 4 && !buffs[id])
		{
			//maxhp_ml += 0.15f;
			act = true;
		}
		else if (id == 5 && !buffs[id])
		{
			hpreg_base -= 2.5;
			act = true;
		}
		else if (!buffs[id])
			act = true;

		if (act)
		{
			SActorTimedEffectsFtg effect;
			effect.buff_id = id;
			effect.buff_time = time;
			if (eff_id > 0)
				effect.effect_id = eff_id;

			m_timed_effects_buffs.push_back(effect);
			//m_buffs.push_back(id);
		}

	}
	else
	{
		if (id == 1 && buffs[id])
		{
			hpreg_base -= 1;
		}
		else if (id == 2 && buffs[id])
		{
			mpreg_base -= 2;
		}
		else if (id == 3 && buffs[id])
		{
			streg_base -= 3;
		}
		else if (id == 4 && buffs[id])
		{
			maxhp_ml -= 0.15f;
		}
		else if (id == 5 && buffs[id])
		{
			hpreg_base += 2.5;
		}

		DelBuff(id);
	}
	buffs_time[id] = time;
	buffs[id] = active;
}

void CActor::ActivatePerk(int id, bool active, int level)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	bool perk_is_activated = false;
	int perk_old_level = 0;
	int perks_count = m_passive_effects.size();
	if (perks_count > 0)
	{
		std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
		std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
		for (int i = 0; i < perks_count, it != end; i++, ++it)
		{
			SActorPassiveEffectRK effect = *it;
			if (effect.perk_id == id)
			{
				perk_is_activated = true;
				perk_old_level = effect.perk_lvl;
				break;
			}
		}
	}
	SActorPassiveEffectRK new_effect;
	new_effect.perk_id = id;
	new_effect.perk_lvl = level;
	bool level_chanded = false;
	if (perk_is_activated)
	{
		level_chanded = (level != perk_old_level);
	}
	bool need_delete_perk = false;
	if (perk_is_activated && level <= 0)
	{
		need_delete_perk = true;
	}

	if (level_chanded)
	{
		if (pGameGlobals)
		{
			string old_lvl_arg = "";
			old_lvl_arg.Format("%i", perk_old_level);
			if (level > perk_old_level)
			{
				RequestToRunLuaFncFromActorScriptInstance(pGameGlobals->GetPerkScrFuncOnUp(id), old_lvl_arg);
			}
			else if (level == 0)
			{
				RequestToRunLuaFncFromActorScriptInstance(pGameGlobals->GetPerkScrFuncOnDown(id), old_lvl_arg);
			}
			else
			{
				RequestToRunLuaFncFromActorScriptInstance(pGameGlobals->GetPerkScrFuncOnDown(id), old_lvl_arg);
			}
		}
	}

	if (level == 0 && !level_chanded)
	{
		if(pGameGlobals)
			RequestToRunLuaFncFromActorScriptInstance(pGameGlobals->GetPerkScrFuncOnDown(id), "");
	}

	if (id == 0)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
				inventory_bag_max_slots += 10;
		}
		else if (level == 0)
		{
			if (inventory_bag_max_slots>10)
				inventory_bag_max_slots -= 10;
		}
		else if (level == 2)
		{
			inventory_bag_max_slots += 10;
		}
		else if (level == 3)
		{
			inventory_bag_max_slots += 10;
		}
		else if (level == 4)
		{
			inventory_bag_max_slots += 10;
		}
	}
	if (id == 1)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
				this->maxst_ml += 0.1f;
		}
		else if (level == 0)
		{
			this->maxst_ml -= 0.1f;
		}
	}
	if (id == 2)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
				this->streg_timing -= 0.2f;
		}
		else if (level == 0)
		{
			this->streg_timing += 0.2f;
		}
	}
	if (id == 3)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
				this->stamina_cons_jump_ml -= 0.2f;
		}
		else if (level == 0)
		{
			this->stamina_cons_jump_ml += 0.2f;
		}
	}
	if (id == 4)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
				this->maxhp_ml += 0.05f;
		}
		else if (level == 0)
		{
			this->maxhp_ml -= 0.05f;
		}
	}
	if (id == 5)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
			{
				this->will_mod += 1;
				this->endr_mod += 1;
			}
		}
		else if (level == 0)
		{
			this->will_mod -= 1;
			this->endr_mod -= 1;
		}
		else if (level == 2)
		{
			this->will_mod += 1;
			this->endr_mod += 1;
		}
		else if (level == 3)
		{
			this->will_mod += 1;
			this->endr_mod += 1;
		}
	}

	if (id == 12)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
			{
				this->maxhp_ml -= 0.2f;
				this->maxmp_ml += 0.3f;
				this->maxst_ml -= 0.15f;
			}
		}
		else if (level == 0)
		{
			this->maxhp_ml += 0.2f;
			this->maxmp_ml -= 0.3f;
			this->maxst_ml += 0.15f;
		}
	}
	if (id == 13)
	{
		if (level == 1)
		{
			if (perk_is_activated == false && active == true)
			{
				this->selfspells_mpcons_common_ml -= 0.1f;
			}
		}
		else if (level == 0)
		{
			this->selfspells_mpcons_common_ml += 0.1f;
		}
	}
	//perks[id] = active;
	//perks_level[id] = level;

	if (!active)
	{
		if (perk_is_activated)
		{
			std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
			std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
			for (int i = 0; i < perks_count, it != end; i++, ++it)
			{
				SActorPassiveEffectRK effect = *it;
				if (effect.perk_id == id)
				{
					m_passive_effects.erase(it);
					m_passive_effects.resize(perks_count - 1);
					break;
				}
			}
		}
	}
	else
	{
		if (need_delete_perk)
		{
			std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
			std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
			for (int i = 0; i < perks_count, it != end; i++, ++it)
			{
				SActorPassiveEffectRK effect = *it;
				if (effect.perk_id == id)
				{
					m_passive_effects.erase(it);
					m_passive_effects.resize(perks_count - 1);
					break;
				}
			}
		}
		else
		{
			if (level_chanded)
			{
				std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
				std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
				for (int i = 0; i < perks_count, it != end; i++, ++it)
				{
					SActorPassiveEffectRK effect = *it;
					if (effect.perk_id == id)
					{
						m_passive_effects.erase(it);
						m_passive_effects.resize(perks_count - 1);
						break;
					}
				}
			}
			m_passive_effects.push_back(new_effect);
		}
	}
}

bool CActor::IsPerkActive(int id)
{
	bool perk_is_activated = false;
	int perks_count = m_passive_effects.size();
	if (perks_count > 0)
	{
		std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
		std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
		for (int i = 0; i < perks_count, it != end; i++, ++it)
		{
			SActorPassiveEffectRK effect = *it;
			if (effect.perk_id == id)
			{
				perk_is_activated = true;
				break;
			}
		}
	}
	return perk_is_activated;
}

int CActor::GetPerkLevel(int id)
{
	int perk_level = 0;
	int perks_count = m_passive_effects.size();
	if (perks_count > 0)
	{
		std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects.begin();
		std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects.end();
		for (int i = 0; i < perks_count, it != end; i++, ++it)
		{
			SActorPassiveEffectRK effect = *it;
			if (effect.perk_id == id)
			{
				perk_level = effect.perk_lvl;
				break;
			}
		}
	}
	return perk_level;
}

void CActor::ClearActorPerks(bool full)
{
	std::vector<SActorPassiveEffectRK> m_passive_effects_vc2;
	m_passive_effects_vc2.clear();
	m_passive_effects_vc2 = m_passive_effects;
	std::vector<SActorPassiveEffectRK>::iterator it = m_passive_effects_vc2.begin();
	std::vector<SActorPassiveEffectRK>::iterator end = m_passive_effects_vc2.end();
	int perks_count = m_passive_effects_vc2.size();
	for (int i = 0; i < perks_count, it != end; i++, ++it)
	{
		for (int ic = 0; ic < (*it).perk_lvl; ic++)
		{
			ActivatePerk((*it).perk_id, false, 0);
		}
	}
	m_passive_effects.clear();
	m_passive_effects.resize(0);
	m_passive_effects_vc2.clear();
	m_passive_effects_vc2.resize(0);
}

void CActor::UpdateBuffs(float frametime)
{
	if (IsRemote())
		return;

	std::vector<SActorTimedEffectsFtg>::iterator it = m_timed_effects_buffs.begin();
	std::vector<SActorTimedEffectsFtg>::iterator end = m_timed_effects_buffs.end();
	int count = m_timed_effects_buffs.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SActorTimedEffectsFtg effect = *it;
		if (buffs_time[effect.buff_id] <= 0.0f)
		{
			this->ActivateBuff(effect.buff_id, 0.0f, false);
			m_timed_effects_buffs.erase(it);
			m_timed_effects_buffs.resize(count - 1);
			break;
		}

		if (buffs_time[effect.buff_id] > 0.0f)
		{
			buffs_time[effect.buff_id] -= frametime;
			if (buffs_time[effect.buff_id] < 0.0f)
				buffs_time[effect.buff_id] = 0.0f;
		}
	}
}

bool CActor::ActCanJump()
{
	if (GetActorStats()->inAir > 0.01f || GetActorStats()->onGround < 0.1f)
		return false;

	if (IsSwimming() | IsHeadUnderWater())
		return false;

	if (m_jump_timer != 0.0f)
		return false;

	if(g_pGameCVars->g_stamina_sys_can_jump_if_low_stmn_val > 0)
		return true;

	if (GetStamina() < stamina_cons_jump_base*stamina_cons_jump_ml)
		return false;
	else
		return true;
}

void CActor::ActJump()
{
	if (m_jump_timer == 0.0f)
	{
		if (!IsRemote())
			SetStamina(GetStamina() - (int)(stamina_cons_jump_base*stamina_cons_jump_ml));

		m_jump_timer = 1.0f;
	}
}

void CActor::SetRelaxedMod(bool relaxed_mode)
{
	m_relaxed_mode = relaxed_mode;
	if (!relaxed_mode)
	{
		//this->GetEntity()->GetCharacter(0)->GetISkeletonAnim()->StopAnimationInLayer(3, 1);
		//m_wounded_anim_playing = false;
	}
}

void CActor::SetStealthMod(bool stealth_mode)
{
	m_stealth_mode = stealth_mode;
}

void CActor::SetItemOnBackId(EntityId ItemId)
{
	item_on_back = ItemId;
}

void CActor::SetSAPlayTime(float p_time)
{
	m_special_action_anim_playing_time = p_time;
}

void CActor::SetSACurrent(int id)
{
	m_special_action_id = id;
}

void CActor::AddXperiencePoints(float xp_points)
{
	SetLevelXp(GetLevelXp() + (int)xp_points);
	int lvlxp = GetLevelXp();
	int lvlxpnext = GetXpToNextLevel();
	int lvl = GetLevel();
	int num_cn = lvlxp / lvlxpnext;
	if (GetLevelXp() >= GetXpToNextLevel() && num_cn >= 1 && num_cn < 2)
	{
		ChekForLevelUP(lvl, lvlxp);
		return;
	}

	if (GetLevelXp() >= GetXpToNextLevel())
	{
		for (int i = 0; i < num_cn; i++)
		{
			if (GetLevelXp() >= GetXpToNextLevel() * 1)
			{
				ChekForLevelUP(GetLevel(), GetLevelXp());
			}
			else
			{
				break;
			}
		}
	}
}

void CActor::ChekForLevelUP(int lvl, int lvlxp)
{
	SetLevel(lvl+1.0f);
	g_pGameCVars->g_leveluppointsstat += g_pGameCVars->g_levelup_points_stat_per_lvl;
	g_pGameCVars->g_levelupabilitypoints += g_pGameCVars->g_levelup_ability_points_per_lvl;
	float next_xp = GetXpToNextLevel() * g_pGameCVars->g_levelup_multipler_for_xp_points_to_next_lvl;
	SetXpToNextLevel((int)next_xp);
}

void CActor::SetMaxInvSlots(int slots)
{
	int gl_inv_cap = gEnv->pConsole->GetCVar("i_inventory_capacity")->GetIVal();
	if (gl_inv_cap > slots)
		inventory_bag_max_slots = slots;
}

int CActor::GetNumNOItemsInInv()
{
	IInventory *pInventory = this->GetInventory();
	if (!pInventory)
		return 0;

	int num = 0;
	for (int i = 0; i < pInventory->GetCount(); ++i)
	{
		CItem *pItem = GetItem(pInventory->GetItem(i));
		if (pItem && pItem->GetItemType() == 81)
		{
			num++;
		}
	}
	return num;
}

void CActor::SetSpecRecoilAct(int id, float time)
{
	m_special_recoil_action[id] = time;
}

float CActor::GetSpecRecoilAct(int id)
{
	return m_special_recoil_action[id];
}

void CActor::EquipWeaponToLeftHand(EntityId itemId, bool select)
{
	if (GetItem(GetCurrentItemId()))
	{
		if (GetItem(GetCurrentItemId())->IsWpnTwoHanded())
			return;
	}

	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return;

	if (!select)
	{
		EntityId itemId_cur = GetCurrentEquippedItemId(eAESlot_Weapon_Left);
		if (itemId_cur > 0)
		{
			CItem *pItem_cur = GetItem(itemId_cur);
			if (pItem_cur)
			{
				pItem_cur->SetHand(0);
				nwAction->SimpleAttachItemToHand(GetEntityId(), itemId_cur, false, 1);
				pItem_cur->SetEquipped(0);
			}
		}
		SetEquippedItemId(eAESlot_Weapon_Left, 0);
		return;
	}

	CItem *pItem = GetItem(itemId);
	if (!pItem)
		return;

	if(pItem->IsWpnTwoHanded())
		return;

	EntityId itemId_lft = GetAnyItemInLeftHand();
	if (itemId_lft > 0)
	{
		CItem *pItem_lft = GetItem(itemId_lft);
		if (pItem_lft)
		{
			if (itemId_lft != GetCurrentEquippedItemId(eAESlot_Weapon_Left))
			{
				if (pItem_lft->GetEquipped() == 1)
				{
					EquipItem(itemId_lft);
				}
			}
		}
	}

	EntityId itemId_cur = GetCurrentEquippedItemId(eAESlot_Weapon_Left);
	if (itemId_cur > 0)
	{
		CItem *pItem_cur = GetItem(itemId_cur);
		if (pItem_cur)
		{
			pItem_cur->SetHand(0);
			nwAction->SimpleAttachItemToHand(GetEntityId(), itemId_cur, false, 1);
			pItem_cur->SetEquipped(0);
		}
	}
	pItem->SetHand(1);
	nwAction->SimpleAttachItemToHand(GetEntityId(), itemId, true, 1);
	SetEquippedItemId(eAESlot_Weapon_Left, itemId);
	pItem->SetEquipped(1);
}

void CActor::RequestToAddItemWAttCRC(const char *item_base_id, uint32 base_crc, int number, EntityId att_ent)
{
	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	if (!pItemSystem)
		return;

	IActor *pActor = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(this->GetEntityId());
	for (int i = 0; i < number; ++i)
	{
		CProjectile* pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(att_ent);
		if (pProjectile)
		{
			string base_item_cl = pProjectile->GetBaseItem().c_str();
			EntityId it_id = pItemSystem->CrItemWOPick(pActor, base_item_cl.c_str());
			CItem *pItem_cur = GetItem(it_id);
			if (pItem_cur)
			{
				if (!pItem_cur->IsStackable())
				{
					pItem_cur->m_ThisSpec2AttachmentCrC = base_crc;
					if (att_ent > 0)
					{
						pItem_cur->m_ThisSpec2AttachmentEntId = att_ent;
					}
				}
				else
				{
					pItem_cur->m_ThisSpec2AttachmentCrC = base_crc;
					if (att_ent > 0)
					{
						pItem_cur->m_ThisSpec2AttachmentEntId = att_ent;
					}
				}
				pItem_cur->PickUp(GetEntityId(), false, false);
			}
		}
		else
		{
			EntityId it_id = pItemSystem->CrItemWOPick(pActor, item_base_id);
			CItem *pItem_cur = GetItem(it_id);
			if (pItem_cur)
			{
				if (!pItem_cur->IsStackable())
				{
					pItem_cur->m_ThisSpec2AttachmentCrC = base_crc;
					if (att_ent > 0)
					{
						pItem_cur->m_ThisSpec2AttachmentEntId = att_ent;
					}
				}
				else
				{
					pItem_cur->m_ThisSpec2AttachmentCrC = base_crc;
					if (att_ent > 0)
					{
						pItem_cur->m_ThisSpec2AttachmentEntId = att_ent;
					}
				}
				pItem_cur->PickUp(GetEntityId(), false, false);
			}
		}
	}

	if (!IsThirdPerson() && IsPlayer())
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(att_ent);
		if (pEntity)
		{
			if(g_pGameCVars->g_pl_draw_arrows_on_body_in_fp == 2)
			{
				pEntity->SetSlotFlags(0, ENTITY_SLOT_RENDER | ENTITY_SLOT_RENDER_NEAREST);
				pEntity->SetSlotFlags(1, ENTITY_SLOT_RENDER | ENTITY_SLOT_RENDER_NEAREST);
			}
			else if (g_pGameCVars->g_pl_draw_arrows_on_body_in_fp == 1)
			{

			}
			else if (g_pGameCVars->g_pl_draw_arrows_on_body_in_fp == 0)
			{
				pEntity->SetSlotFlags(0, 0);
				pEntity->SetSlotFlags(1, 0);
			}
		}
	}
}

string CActor::GetCurrentModelPth()
{
	return m_currModel;
}

void CActor::UpdateNewStats(float frameTime)
{
	//----------------------------------------------------
	if (mpreg_base > 0.0f)
	{
		mpreg_ft += frameTime * g_pGameCVars->g_mgc_sys_reg_speed;
		if (mpreg_ft > mpreg_timing)
		{
			mpreg_ft = 0.0f;
			m_magicka += mpreg_base*mpreg_ml;
		}
	}

	if (hpreg_base > 0.0f)
	{
		hpreg_ft += frameTime;
		if (hpreg_ft > hpreg_timing)
		{
			hpreg_ft = 0.0f;
			float mf_health = GetHealth();
			mf_health += hpreg_base*hpreg_ml;
			SetHealth(mf_health);
		}
	}

	if (streg_base > 0.0f)
	{
		if (streg_paused_time > 0.0f)
			streg_paused_time -= frameTime;

		if (streg_paused_time <= 0.0f)
		{
			streg_ft += frameTime * g_pGameCVars->g_stamina_sys_reg_speed;
			if (streg_ft > streg_timing)
			{
				streg_ft = 0.0f;
				m_stamina += streg_base*streg_ml;
			}
		}
	}
	//-----------------------------------------------------

	m_jump_timer -= frameTime;
	if (m_jump_timer < 0.0f)
	{
		m_jump_timer = 0.0f;
	}

	m_hpTime -= frameTime;
	if (m_hpTime < 0.0f)
	{
		m_hpTime = 0.0f;
		if (GetHealth() != GetMaxHealth())
		{
			float healthInc = m_hpAcc + m_hpRegenRate / 10 * 1;
			int healthIncInt = (int32)healthInc;
			m_hpAcc = healthInc - healthIncInt;
			int32 cn_hp = (int32)(GetHealth() + healthIncInt);
			int32 mcn_hp = (int32)GetMaxHealth();
			int newHealth = min(mcn_hp, (int32)cn_hp);
			if (GetHealth() != newHealth)
			{
				if (!IsRemote())
					SetHealth((float)newHealth);
			}
		}

		if (GetStamina() != GetMaxStamina())
		{
			float staminaInc = m_stAcc + m_stRegenRate / 10 * 1;
			int staminaIncInt = (int32)staminaInc;
			m_stAcc = staminaInc - staminaIncInt;
			int newStamina = min(GetMaxStamina(), (int32)(GetStamina() + staminaIncInt));
			if (GetStamina() != newStamina)
			{
				if (!IsRemote())
					SetStamina(newStamina);
			}
		}

		if (GetMagicka() != GetMaxMagicka())
		{
			float magickaInc = m_mpAcc + m_mpRegenRate / 10 * 1;
			int magickaIncInt = (int32)magickaInc;
			m_mpAcc = magickaInc - magickaIncInt;
			int newMagicka = min(GetMaxMagicka(), (int32)(GetMagicka() + magickaIncInt));
			if (GetMagicka() != newMagicka)
			{
				if (!IsRemote())
					SetMagicka(newMagicka);
			}
		}
	}

	m_run_culldown -= frameTime;
	if (m_run_culldown < 0.0f)
	{
		m_run_culldown = 0.0f;
	}

	if (IsSprinting())
	{
		//m_run_culldown;
		float staminaInc = m_stAcc + st_run_drain_base / 10 * 1;
		int staminaIncInt = (int32)staminaInc;
		m_stAcc = staminaInc - staminaIncInt;
		int newStamina = min(GetMaxStamina(), (int32)(GetStamina() + staminaIncInt));
		if (GetStamina() != newStamina)
		{
			if (!IsRemote())
				SetStamina(newStamina);
		}

		if (GetStamina() < 6 && m_run_culldown <= 0.0f)
			m_run_culldown += 6;
	}

	if (!IsPlayer())
	{
		m_stmnTime -= frameTime;
		if (m_stmnTime < 0.0f && !IsSprinting() && m_run_culldown <= 0.7f)
		{
			m_stmnTime = 0.0f;
			m_stmnTime += 2.0f;
			int32 currentStamina = GetStamina();
			int32 maxStamina(GetMaxStamina());
			float regenStamina = 0.0f;
			float regenTimeStamina = 20.0f;
			regenTimeStamina = 50.0f;
			regenStamina = maxStamina / max(0.01f, regenTimeStamina);
			m_staminaRegenRate = regenStamina;

			if (currentStamina < maxStamina)
			{
				m_staminaRegenRate = maxStamina / max(0.01f, 100.0f);
			}

			float staminaInc = m_staminaRegenRate;
			int staminaIncInt = (int32)staminaInc;

			int newStamina = min(maxStamina, (int32)(currentStamina + staminaIncInt + 1));
			if (currentStamina != newStamina)
			{
				if (!IsRemote())
					SetStamina(newStamina);
			}
		}
	}
}

void CActor::UpdateActorScriptFnc(float frameTime)
{
	update_actor_script_time += frameTime;
	if (update_actor_script_time >= update_actor_script_timing)
	{
		update_actor_script_timing = cry_random(0.7f, 1.0f);
		update_actor_script_time = 0.0f;
		if (IScriptTable* pActScript = GetEntity()->GetScriptTable())
		{
			IScriptSystem* pSS = pActScript->GetScriptSystem();
			if (pSS)
			{
				if (pActScript->GetValueType("SpecialUpdateActor") == svtFunction && pSS->BeginCall(pActScript, "SpecialUpdateActor"))
				{
					pSS->PushFuncParam(pActScript);
					pSS->EndCall();
				}
			}
		}
	}
}

void CActor::UpdateAimProcess(float frameTime)
{
	int chr_slot_to_aim_simulate = 0;
	if (IsDead())
		return;

	if (IsPlayer() && g_pGameCVars->ca_aim_pose_player_alternative < 1)
		return;
	else if (!IsPlayer() && g_pGameCVars->ca_aim_pose_ai_alternative < 1)
		return;

	if (IsPlayer() && g_pGameCVars->g_test_cam_anim_controlled > 0.0f)
		return;

	if (!m_poseModifier_aim2)
		::CryCreateClassInstance("AnimationPoseModifier_OperatorQueue", m_poseModifier_aim2);
	if (!m_poseModifier_aim2)
		return;

	if (IsPlayer())
	{
		CPlayer *pPlayer = static_cast<CPlayer*>(this);
		if (pPlayer && pPlayer->IsOnLadder())
			return;

		if (pPlayer && pPlayer->IsOnLedge())
			return;
	}

	if (!IsThirdPerson())
	{
		if (IsPlayer())
		{
			CPlayer *pPlayer = static_cast<CPlayer*>(this);
			if (pPlayer)
			{
				chr_slot_to_aim_simulate = pPlayer->GetShadowCharacterSlot();
				/*ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
				if (!pCharacter)
					return;

				int headJoint = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Head");
				SMovementState info;
				m_pMovementController->GetMovementState(info);
				Quat x_aim_rot = Quat::CreateRotationVDir(info.fireDirection);
				Ang3 lds = Ang3(x_aim_rot);
				Quat desiredOrientation = Quat::CreateRotationXYZ(Ang3(lds.x*0.6f, 0, 0));
				m_poseModifier_aim2->PushOrientation(headJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
				pCharacter->GetISkeletonAnim()->PushPoseModifier(g_pGameCVars->ca_aim_pose_alternative_aim_layer, cryinterface_cast<IAnimationPoseModifier>(m_poseModifier_aim2), "IKTorsoAimActor");*/
			}
			else
				return;
		}
		else
			return;
	}

	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(chr_slot_to_aim_simulate);
	if (!pCharacter)
		return;

	if (GetActorStats()->isRagDoll)
		return;

	//if (m_pImpulseHandler->m_impulseDuration_public > 0.0f)
	//	return;

	m_poseModifier_aim2->Clear();
	SMovementState info;
	m_pMovementController->GetMovementState(info);
	int aimJoint = -1;
	int aimJoint2 = -1;
	int aimJoint3 = -1;
	int headJoint = -1;
	int neckJoint = -1;
	aimJoint = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Spine2");
	aimJoint2 = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Spine1");
	aimJoint3 = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Spine");
	headJoint = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Head");
	neckJoint = pCharacter->GetIDefaultSkeleton().GetJointIDByName("Bip01 Neck");
	Vec3 targ_pos(0, 0, 0);
	Matrix34 worldTM;
	Vec3 targetWorldPosition(0, 0, 0);
	Vec3 targetLocalPosition(0, 0, 0);
	CItem *pItem = GetItem(GetCurrentItemId());
	IAIActor *pActII = CastToIAIActorSafe(GetEntity()->GetAI());
	if (pActII)
	{
		if (pActII->GetAttentionTarget())
		{
			targ_pos = pActII->GetAttentionTarget()->GetPos();
			targetWorldPosition = targ_pos;
			targetLocalPosition = targetWorldPosition;
			targetLocalPosition.z = targetLocalPosition.z - g_pGameCVars->ca_aim_pose_ai_alternative_target_z_m;
			if (pActII->GetAttentionTarget()->GetEntity())
			{
				bool up_z_for_simple_target = false;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals)
				{
					CActor *pActor = pGameGlobals->GetThisActor(pActII->GetAttentionTarget()->GetEntity()->GetId());
					if (pActor)
					{
						up_z_for_simple_target = false;
					}
					else
					{
						up_z_for_simple_target = true;
					}
				}
				targetLocalPosition = pActII->GetAttentionTarget()->GetEntity()->GetWorldPos();
				float dist_to_target = targetLocalPosition.GetDistance(GetEntity()->GetWorldPos());
				//if(GetBonePosition("Bip01 spine"))
				if (dist_to_target < 1.0f && up_z_for_simple_target)
				{
					targetLocalPosition.z = targetLocalPosition.z + 1.4f;
				}
			}

			if (pItem && pItem->GetWeaponType() == 9)
			{
				Vec3 rh_pos = GetEntity()->GetPos();
				Vec3 rt_pos = targetLocalPosition;
				Vec3 rt_pos_wzh = Vec3(targetLocalPosition.x, targetLocalPosition.y, rh_pos.z);
				float dist_b_ht = rh_pos.GetDistance(rt_pos_wzh);
				if (dist_b_ht < 100.0f && dist_b_ht > 5.0f)
				{
					if (rt_pos.z > rh_pos.z)
					{
						float rzn_ht_z = rt_pos.z - rh_pos.z;
						if (rzn_ht_z > 0)
						{
							float coof_cnd = ((rzn_ht_z * dist_b_ht) * (0.01f * g_pGameCVars->ca_aim_pose_ai_alternative_target_z_m_for_range_w_bow));
							targetLocalPosition.z = targetLocalPosition.z + coof_cnd;
						}
					}
				}
			}
		}
	}
	else
	{
		//CryLogAlways("no AI Ext");
	}
	SActorFrameMovementParams frameMovementParams;
	if (m_pMovementController)
	{
		m_pMovementController->Update(0.0f, frameMovementParams);
	}
	if (IsPlayer())
	{
		if (g_pGameCVars->g_playerRotViewMode == 0)
		{
			//fdir_pos_for_pl_rv_mode = info.fireDirection;
		}
		else if (g_pGameCVars->g_playerRotViewMode == 1)
		{
			//info.fireDirection = fdir_pos_for_pl_rv_mode;
		}
	}
	Vec3 aimTarget = GetEntity()->GetPos() + info.fireDirection * 5.0f;
	if (g_pGameCVars->g_ai_actor_use_aim_target_info_fr_mvs_to_targeting > 0)
	{
		targetLocalPosition = frameMovementParams.aimTarget;
	}
	//if (frameMovementParams.aimTarget.IsZero())
	//	CryLogAlways("no target pos for aim");
	Vec3 aimTarget2h(0, 0, 0);
	Vec3 aimTarget2pos(0, 0, 0);
	if (!IsPlayer())
	{
		if (targetLocalPosition.IsZero())
			return;

		aimTarget = targetLocalPosition - GetEntity()->GetPos();
		aimTarget2h = Vec3(targetLocalPosition.x, targetLocalPosition.y, GetEntity()->GetPos().z);
		aimTarget2pos = aimTarget2h;
		aimTarget2h = aimTarget2h - GetEntity()->GetPos();
	}
	else
	{
		targetLocalPosition = aimTarget;
		aimTarget2h = Vec3(aimTarget.x, aimTarget.y, GetEntity()->GetPos().z);
		aimTarget2pos = aimTarget2h;
		aimTarget2h = aimTarget2h - GetEntity()->GetPos();
		aimTarget = aimTarget - GetEntity()->GetPos();
	}
	Quat desiredOrientation = desiredOrientation.CreateIdentity();
	QuatT bone_1orient = pCharacter->GetISkeletonPose()->GetAbsJointByID(aimJoint);
	float root1 = bone_1orient.q.GetFwdY();
	Vec3 orig_direct = GetEntity()->GetPos() + (GetEntity()->GetRotation() * Vec3(0, 1, 0)) * 5.0f;
	float root2 = GetEntity()->GetRotation().GetRotZ();

	IRenderer* pRenderer = gEnv->pRenderer;
	if (pRenderer && g_pGameCVars->ca_aim_pose_alternative_debug > 0)
	{
		IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
		if (pAuxGeom)
		{
			pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
			ColorB colNormal(25, 150, 15, 128);
			ColorB colNormalEnd(150, 15, 250, 128);
			ColorB colNormal2(250, 15, 150, 128);
			ColorB colNormalEnd2(150, 150, 25, 128);
			pAuxGeom->DrawLine(GetEntity()->GetPos(), colNormal, aimTarget2pos, colNormalEnd, 5);
			pAuxGeom->DrawLine(GetEntity()->GetPos(), colNormal2, targetLocalPosition, colNormalEnd2, 5);
			pAuxGeom->DrawLine(GetEntity()->GetPos(), colNormal2, orig_direct, colNormalEnd2, 5);
		}
	}
	orig_direct.z = GetEntity()->GetPos().z;
	orig_direct = orig_direct - GetEntity()->GetPos();
	aimTarget2h.normalize();
	orig_direct.normalize();
	aimTarget.normalize();
	float fDot = aimTarget2h * orig_direct;
	fDot = clamp_tpl(fDot, -1.0f, 1.0f);
	float angle = acos_tpl(fDot);
	if (((orig_direct.x * aimTarget2h.y) - (orig_direct.y * aimTarget2h.x)) < 0)
		angle = -angle;

	Quat x_aim_rot = Quat::CreateRotationVDir(aimTarget);
	Ang3 lds = Ang3(x_aim_rot);

	float fDot2 = aimTarget * aimTarget2h;
	fDot2 = clamp_tpl(fDot2, -1.0f, 1.0f);
	float angle2 = acos_tpl(fDot2);
	if (aimTarget.z < aimTarget2h.z)
		angle2 = -angle2;
	//compensation system------------------------------------------------------------------------------------------
	float fps_coof = frameTime;
	if (!IsThirdPerson() && IsPlayer())
		fps_coof = fps_coof + (0.02f * g_pGameCVars->ca_aim_pose_alternative_speed_mlt_for_pl_shadow_chr);
	else
		fps_coof = fps_coof + (0.02f * g_pGameCVars->ca_aim_pose_alternative_speed_mlt);

	if (fps_coof > 1.0f)
		fps_coof = 1.0f;

	float npm_pl_val_ang = angle;
	if (npm_pl_val_ang < 0)
		npm_pl_val_ang = npm_pl_val_ang - (npm_pl_val_ang * 2.0f);

	float npm_pl_val_old_ang = alt_aim_pose_old_angle_horiz;
	if (npm_pl_val_old_ang < 0)
		npm_pl_val_old_ang = npm_pl_val_old_ang - (npm_pl_val_old_ang * 2.0f);

	float rzn_numb = npm_pl_val_ang - npm_pl_val_old_ang;
	if (rzn_numb < 0)
		rzn_numb = rzn_numb - (rzn_numb * 2.0f);

	float frct = rzn_numb * fps_coof;
	if (alt_aim_pose_old_angle_horiz != angle)
	{
		if (angle >= 0)
		{
			if (alt_aim_pose_old_angle_horiz > angle)
			{
				if (alt_aim_pose_old_angle_horiz > 0)
					angle = alt_aim_pose_old_angle_horiz - frct;
			}
			else
			{
				if (alt_aim_pose_old_angle_horiz > 0)
					angle = alt_aim_pose_old_angle_horiz + frct;
				else
					angle = alt_aim_pose_old_angle_horiz + frct;
			}
		}
		else if (angle < 0)
		{
			if (alt_aim_pose_old_angle_horiz > angle)
			{
				if (alt_aim_pose_old_angle_horiz > 0)
					angle = alt_aim_pose_old_angle_horiz - frct;
				else
					angle = alt_aim_pose_old_angle_horiz - frct;
			}
			else
			{
				if (alt_aim_pose_old_angle_horiz < 0)
					angle = alt_aim_pose_old_angle_horiz + frct;
			}
		}
	}
	//vert
	npm_pl_val_ang = angle2;
	if (npm_pl_val_ang < 0)
		npm_pl_val_ang = npm_pl_val_ang - (npm_pl_val_ang * 2.0f);

	npm_pl_val_old_ang = alt_aim_pose_old_angle_vert;
	if (npm_pl_val_old_ang < 0)
		npm_pl_val_old_ang = npm_pl_val_old_ang - (npm_pl_val_old_ang * 2.0f);

	rzn_numb = npm_pl_val_ang - npm_pl_val_old_ang;
	if (rzn_numb < 0)
		rzn_numb = rzn_numb - (rzn_numb * 2.0f);

	frct = rzn_numb * fps_coof;
	if (alt_aim_pose_old_angle_vert != angle2)
	{
		if (angle2 >= 0)
		{
			if (alt_aim_pose_old_angle_vert > angle2)
			{
				if (alt_aim_pose_old_angle_vert > 0)
					angle2 = alt_aim_pose_old_angle_vert - frct;
			}
			else
			{
				if (alt_aim_pose_old_angle_vert > 0)
					angle2 = alt_aim_pose_old_angle_vert + frct;
				else
					angle2 = alt_aim_pose_old_angle_vert + frct;
			}
		}
		else if (angle2 < 0)
		{
			if (alt_aim_pose_old_angle_vert > angle2)
			{
				if (alt_aim_pose_old_angle_vert > 0)
					angle2 = alt_aim_pose_old_angle_vert - frct;
				else
					angle2 = alt_aim_pose_old_angle_vert - frct;
			}
			else
			{
				if (alt_aim_pose_old_angle_vert < 0)
					angle2 = alt_aim_pose_old_angle_vert + frct;
			}
		}
	}
	//---------------------------------base aim limits--------------------------------------------------------------
	float limit_horiz = g_pGameCVars->ca_aim_pose_alternative_limit_base_horizontal / 120.0f;
	float limit_vert = g_pGameCVars->ca_aim_pose_alternative_limit_base_vertical / 120.0f;
	//aim limits from current item setting-------------------------------------------
	if (g_pGameCVars->ca_aim_pose_alternative_limit_hv_from_wpn_setting > 0)
	{
		if (pItem)
		{
			limit_horiz = pItem->GetAimAltLimitsHoriz() / 120.0f;
			limit_vert = pItem->GetAimAltLimitsVert() / 120.0f;
		}
	}
	float angle_horiz_max = limit_horiz;
	float angle_vert_max = limit_vert;
	//-------------check limits------------------------------
	if (angle2 > 0 && angle2 > angle_vert_max)
		angle2 = angle_vert_max;
	else if (angle2 < 0 && angle2 < -angle_vert_max)
		angle2 = -angle_vert_max;
	//-------------------horizontal-------------------------------
	if (angle > 0 && angle > angle_horiz_max)
		angle = angle_horiz_max;
	else if (angle < 0 && angle < -angle_horiz_max)
		angle = -angle_horiz_max;
	//------------------set old value to next frame compensation--------------
	alt_aim_pose_old_angle_horiz = angle;
	alt_aim_pose_old_angle_vert = angle2;
	//------------------------------------------------------------------------
	//only head&neck force rotate in relaxed mode

	if (limit_horiz <= 0.0f && limit_vert <= 0.0f)
	{
		if (g_pGameCVars->g_ai_actor_rotate_to_target_permoment > 0 && !IsPlayer())
		{
			if (pActII)
			{
				pActII->SetBodyTargetDir(aimTarget);
			}
		}

		if (!IsPlayer())
		{
			Current_target_dir = aimTarget;
			if (targetLocalPosition.IsZero())
				Current_target_dir = Vec3(ZERO);
		}
		return;
	}
	m_to_target_quaternion = Quat::CreateRotationXYZ(Ang3(angle2, lds.y, angle));
	if (!GetRelaxedMod())
	{
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.2f, lds.y*0.2f, angle*0.2f));
		m_poseModifier_aim2->PushOrientation(aimJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.2f, lds.y*0.2f, angle*0.2f));
		m_poseModifier_aim2->PushOrientation(aimJoint3, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.25f, lds.y*0.25f, angle*0.25f));
		m_poseModifier_aim2->PushOrientation(aimJoint2, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.35f, lds.y*0.35f, angle*0.35f));
		m_poseModifier_aim2->PushOrientation(neckJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		pCharacter->GetISkeletonAnim()->PushPoseModifier(g_pGameCVars->ca_aim_pose_alternative_aim_layer, cryinterface_cast<IAnimationPoseModifier>(m_poseModifier_aim2), "IKTorsoAimActor");
	}
	else
	{
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.6f, lds.y*0.6f, angle*0.6f));
		m_poseModifier_aim2->PushOrientation(headJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.1f, lds.y*0.1f, angle*0.1f));
		m_poseModifier_aim2->PushOrientation(neckJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.1f, lds.y*0.1f, angle*0.1f));
		m_poseModifier_aim2->PushOrientation(aimJoint, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.1f, lds.y*0.1f, angle*0.1f));
		m_poseModifier_aim2->PushOrientation(aimJoint2, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		desiredOrientation = Quat::CreateRotationXYZ(Ang3(angle2*0.1f, lds.y*0.1f, angle*0.1f));
		m_poseModifier_aim2->PushOrientation(aimJoint3, IAnimationOperatorQueue::eOp_Additive, desiredOrientation);
		pCharacter->GetISkeletonAnim()->PushPoseModifier(g_pGameCVars->ca_aim_pose_alternative_aim_layer, cryinterface_cast<IAnimationPoseModifier>(m_poseModifier_aim2), "IKTorsoAimActor");
	}

	if (pRenderer && g_pGameCVars->ca_aim_pose_alternative_debug > 0)
	{
		Matrix34 matWorld = GetEntity()->GetWorldTM();
		Vec3 vWorldPos = matWorld.GetTranslation();
		vWorldPos.z += 2.4f;
		pRenderer->DrawLabel(vWorldPos, 1, "%f angle_horiz", angle);
		vWorldPos.z += 0.2f;
		pRenderer->DrawLabel(vWorldPos, 1, "%f angle_vert", angle2);
	}

	if (g_pGameCVars->g_ai_actor_rotate_to_target_permoment > 0 && !IsPlayer())
	{
		if (pActII)
		{
			pActII->SetBodyTargetDir(aimTarget);
		}
	}

	if (!IsPlayer())
	{
		Current_target_dir = aimTarget;
		if (targetLocalPosition.IsZero())
			Current_target_dir = Vec3(ZERO);
	}
}

void CActor::DelayedReequipPhyReqItems(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_TimerDelayedReequipPhyReqItems)
	{
		std::vector<EntityId>::const_iterator it = m_delayed_left_hand_phy_wpns_equip.begin();
		std::vector<EntityId>::const_iterator end = m_delayed_left_hand_phy_wpns_equip.end();
		int count = m_delayed_left_hand_phy_wpns_equip.size();
		for (int i = 0; i < count, it != end; i++, it++)
		{
			CItem *pItem = GetItem(*it);
			if (pItem)
			{
				if (pItem->CanSelect())
				{
					SelectItem(pItem->GetEntityId(), false, true);
				}
				else
				{
					EquipItem(pItem->GetEntityId());
					//------Just epic fix............//
					EquipItem(pItem->GetEntityId());
					EquipItem(pItem->GetEntityId());
					//--Need to be deleted in future---
				}
			}
		}
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerDelayedReequipPhyReqItems);
		m_TimerDelayedReequipPhyReqItems = NULL;
	}
}

CActor::AcQuickSlot CActor::GetQuickSlotInfo(int slot)
{
	for (int i = 0; i < MAX_QUICK_EQUIPMENT_SLOTS; i++)
	{
		if (m_quick_eq_slots[i].slot_id == slot)
			return m_quick_eq_slots[i];
	}
	return AcQuickSlot();
}

void CActor::SetQuickSlotInfo(int slot, AcQuickSlot info)
{
	AcQuickSlot nv_slot = GetQuickSlotInfo(slot);
	if (nv_slot.slot_type == info.slot_type)
	{
		if (nv_slot.obj_in_slot_id == info.obj_in_slot_id)
			return;
	}
	int real_slot = slot;
	int virt_slot = slot - 1;
	if (virt_slot < 0)
		virt_slot = 0;

	if (virt_slot > (MAX_QUICK_EQUIPMENT_SLOTS - 1))
		virt_slot = MAX_QUICK_EQUIPMENT_SLOTS - 1;

	for (int i = 0; i < MAX_QUICK_EQUIPMENT_SLOTS; i++)
	{
		if (m_quick_eq_slots[i].slot_id == real_slot)
		{
			m_quick_eq_slots[i] = info;
			//CryLogAlways("CActor::SetQuickSlotInfo cv1");
			return;
		}
		else if (m_quick_eq_slots[i].slot_type == info.slot_type)
		{
			if (m_quick_eq_slots[i].obj_in_slot_id == info.obj_in_slot_id)
			{
				RemoveQuickSlotInfo(m_quick_eq_slots[i].slot_id);
				//CryLogAlways("CActor::SetQuickSlotInfo cv2");
				break;
			}
		}
	}
	m_quick_eq_slots[virt_slot] = info;
}

void CActor::RemoveQuickSlotInfo(int slot)
{
	for (int i = 0; i < MAX_QUICK_EQUIPMENT_SLOTS; i++)
	{
		if (m_quick_eq_slots[i].slot_id == slot)
		{
			m_quick_eq_slots[i] = AcQuickSlot();
			return;
		}
	}
}

void CActor::UseQuickSlot(int slot)
{
	AcQuickSlot current_slot = AcQuickSlot();
	for (int i = 0; i < MAX_QUICK_EQUIPMENT_SLOTS; i++)
	{
		if (m_quick_eq_slots[i].slot_id == slot)
		{
			current_slot = m_quick_eq_slots[i];
			break;
		}
	}

	if (current_slot.slot_type == 0)
		return;

	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return;

	if (current_slot.slot_type == eQSType_Item)
	{
		EntityId item_id = (EntityId)current_slot.obj_in_slot_id;
		if (item_id <= 0)
			return;

		CItem *pItemInSlot = GetItem(item_id);
		if(!pItemInSlot)
			return;

		nwAction->QuickSlotItemPressed(item_id, GetEntityId());
		pItemInSlot = GetItem(item_id);
		if (pItemInSlot)
		{
			if (GetInventory())
			{
				if (GetInventory()->FindItem(item_id) == -1)
				{
					RemoveQuickSlotInfo(slot);
					return;
				}
			}
		}
		else if(pItemInSlot && (pItemInSlot->GetOwnerId() != GetEntityId()))
		{
			RemoveQuickSlotInfo(slot);
			return;
		}
		else
		{
			RemoveQuickSlotInfo(slot);
			return;
		}
	}
	else if(current_slot.slot_type == eQSType_Spell)
	{
		CGameXMLSettingAndGlobals::SActorSpell act_spell = GetSpellInfoFromActorSB(current_slot.obj_in_slot_id);
		if(act_spell.spell_id == -1)
			return;

		nwAction->SpellPressed(string(act_spell.spell_name.c_str()));
	}
	else if (current_slot.slot_type == eQSType_Ability)
	{

	}
}

void CActor::CreateBloodLooseEffect()
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (!pCharacter)
		return;

	if (GetLastDamagePos().IsZero())
		return;

	if (blood_loose_effect_counter > 65535)
		return;

	Vec3 pReturnPos = GetLastDamagePos();
	int jointId = FindNearestBone(pReturnPos);
	if (IAttachment* pAttachment = pCharacter->GetIAttachmentManager()->GetInterfaceByPhysId(jointId))
	{
		if (!pReturnPos.IsZero())
		{
			pReturnPos = pAttachment->GetAttWorldAbsolute().t;
		}
		jointId = pAttachment->GetJointID();
	}
	pReturnPos = GetEntity()->GetWorldTM().TransformPoint(pCharacter->GetISkeletonPose()->GetAbsJointByID(jointId).t);

	IDefaultSkeleton& rIDefaultSkeleton = pCharacter->GetIDefaultSkeleton();
	ISkeletonPose* pSkeleton = pCharacter->GetISkeletonPose();
	const char* boneName = rIDefaultSkeleton.GetJointNameByID(jointId);
	const QuatT jointWorld = QuatT(GetEntity()->GetWorldTM()) * pSkeleton->GetAbsJointByID(jointId);
	QuatT loc;
	loc.q = GetEntity()->GetRotation();
	loc.t = GetLastDamagePos() + (loc.q.GetColumn1() * 0.1f);
	Matrix34 worldToLocalTM = GetEntity()->GetWorldTM().GetInverted();
	//GetEntity()->SetWorldTM(Matrix34(loc));
	loc = jointWorld.GetInverted() * loc;
	Vec3 m_stuckPos = loc.t;
	//Vec3 point = worldToLocalTM.TransformPoint(GetLastDamagePos());
	Vec3 point = worldToLocalTM.TransformPoint(jointWorld.t);
	//Quat m_stuckRot = loc.q;
	char attachName[17] = "";
	sprintf(attachName, "blood_loose_eff_%d", blood_loose_effect_counter);
	IAttachment* pCharacterAttachment = NULL;
	pCharacterAttachment = pCharacter->GetIAttachmentManager()->CreateAttachment(attachName, CA_BONE, boneName, false);
	if (pCharacterAttachment)
	{
		//CryLogAlways("blood loose effect point x- %f y- %f z- %f", point.x, point.y, point.z);
		string bl_particle = "";
		SmartScriptTable props;
		IScriptTable* pScriptTable = this->GetEntity()->GetScriptTable();
		if (pScriptTable)
		{
			if (pScriptTable && pScriptTable->GetValue("Properties", props))
			{
				CScriptSetGetChain prop(props);
				prop.GetValue("bloodlooseeffectname", bl_particle);
			}
		}

		if (bl_particle.empty())
			bl_particle = "misc.blood_fx.blood_loose_effect";

		IParticleEffect *pParticleEffect = gEnv->pParticleManager->FindEffect(bl_particle.c_str());
		CEffectAttachment *pEffectAttachment = new CEffectAttachment(pParticleEffect, point, GetLastDamageNormal(), 1);
		pCharacterAttachment->AddBinding(pEffectAttachment);
		//pCharacterAttachment->AlignJointAttachment();
		//pCharacterAttachment->UpdateAttModelRelative();
		blood_loose_effect_counter++;
	}
}

int CActor::GetBloodLooseEffectNumder()
{
	return blood_loose_effect_counter;
}

void CActor::SetBloodLooseEffectNumder(int num)
{
	blood_loose_effect_counter = num;
}

void CActor::ClearBloodLooseEffects()
{
	for (int i = 0; i < (blood_loose_effect_counter + 1); i++)
	{
		char attachName[17] = "";
		sprintf(attachName, "blood_loose_eff_%d", i);
		DeleteParticleAttachment(0, attachName, false);
	}
	blood_loose_effect_counter = 0;
}

void CActor::ResetActorVars()
{
	st_run_drain_base=-1.0f; st_run_drain_timing=0;	maxhp_ml=1;	maxmp_ml=1;	maxst_ml=1;	maxhp_mod=0; maxmp_mod=0; maxst_mod=0; num_buffs=0;	num_perks=0; m_blockactiveshield = false; m_blockactivenoshield = false;
	str_mod = 0; agl_mod = 0; int_mod = 0; will_mod = 0; endr_mod = 0; pers_mod = 0; bows_ml = 1; swords_ml = 1; thswords_ml = 1; poles_ml = 1;	thpoles_ml = 1;	axes_ml = 1; thaxes_ml = 1;	hpreg_ml = 1; mpreg_ml = 1;
	streg_ml = 1; hpreg_base = 0; mpreg_base = 0.5;	streg_base = 0.5; hpreg_timing = 1;	mpreg_timing = 1;	streg_timing = 0.4f; crtchance_common_ml = 1; crtdmg_common_ml = 1; crtchance_bows_ml = 1;	crtdmg_bows_ml = 1;
	crtchance_swords_ml = 1;	crtdmg_swords_ml = 1;	crtchance_thswords_ml = 1;	crtdmg_thswords_ml = 1;	crtchance_poles_ml = 1;	crtdmg_poles_ml = 1;	crtchance_thpoles_ml = 1;	crtdmg_thpoles_ml=1;	crtchance_axes_ml=1;
	crtdmg_axes_ml=1;	crtchance_thaxes_ml=1;	crtdmg_thaxes_ml=1;	selfspells_mpcons_common_ml=1;	spells_mpcons_common_ml=1;	selfspells_pwr_common_ml=1;	spells_pwr_common_ml=1;	stamina_cons_jump_base=20;	stamina_cons_run_base=3.5f;
	stamina_cons_atk_base=15;	stamina_cons_jump_ml=1;	stamina_cons_run_ml=1;	stamina_cons_atk_ml=1;	stamina_cons_block_ml=1;	stamina_cons_ml_overheat=2;	stamina_cons_ml_frost=3;	magicka_cons_ml_overheat=3;
	magicka_cons_ml_frost=0.7;	m_wpnselecteditem=0;	m_wear_boots_id=0;	m_wear_arms_id=0;	m_wear_helm_id=0;	m_wear_curr_id=0;	m_wear_pants_id=0;	m_wear_shield_id=0;	m_wear_torch_id=0;	m_wear_ammoarrows_id=0;
	m_wpn_left_hand_id = 0;	m_equipmentswitchboots = false;	m_equipmentswitchlegs = false;	m_equipmentswitchchest = false;	m_equipmentswitcharms = false;	m_equipmentswitchhead = false;	m_equipmentswitchshield = false;
	m_equipmentswitchtorch = false;	m_boots_num_parts = 0;	m_llegs_num_parts = 0;	m_chest_num_parts = 0;	m_arms_num_parts = 0;	m_hpRegenRate = 0;	m_mpRegenRate = 0;	m_stRegenRate = 0;	m_hpTime = 0;	m_hpAcc = 0;
	m_mpAcc = 0;	m_stAcc = 0;	m_run_culldown = 3;	m_run_multipler = 1;	m_stmnTime = 0;	m_staminaRegenRate = 0;	m_stealth_mode = false;	m_relaxed_mode = false;	item_on_back = 0;	m_special_action_anim_playing_time = 0.0f;
	m_special_action_id = 0;	m_levelxp = 0.0f;	m_xptonextlevel = 200.0f;	inventory_bag_max_slots = 30;	m_jump_timer = 0.0f;	additional_pack_added = false;	additional_pack2_added = false;	additional_pack_add_request = false;
	additional_pack2_add_request = false;	additional_pack_equipped = false;	additional_pack2_equipped = false;	equipping_timer = 0.5f;	m_process_falling_timer_c1 = 0.0f;	m_process_falling_timer_c2 = 0.0f;
	m_process_falling_current_vector = Vec3(ZERO);	m_process_falling_controll_vector = Vec3(ZERO);	m_process_falling_controll_start_rot = Quat(ZERO);	m_process_falling_controll_dinamic_rot = Quat(ZERO);	Current_target_dir = Vec3(ZERO);
	m_falling_max_speed_add = 3.0f;	m_falling_max_speed_multipler = 3.0f;	m_falling_base_speed = 0.5f;	m_falling_vector_z_add = 0.08f;	m_falling_vector_max_vel_xyz = 10.0f;

	for (int i = 0; i < 65535; i++)
	{
		perks[i] = false;
		buffs[i] = false;
		buffs_time[i] = 0;
		perks_level[i] = 0;
	}

	for (int i = 0; i < 15; i++)
	{
		m_special_recoil_action[i] = 0.0f;
	}

	m_items_to_equip_pack1.clear();
	m_items_to_equip_pack2.clear();
	SetArmor(0);
	start_update_timer_xc = 0.0f;
	//CryLogAlways("CActor::ResetActorVars");
}

void CActor::OnCollisionEvent(EventPhysCollision *physCollision)
{
	if (!IsPlayer())
	{
		if (physCollision)
		{
			IPhysicalEntity* pHitByPhysicalEntity1 = physCollision->pEntity[1];
			IPhysicalEntity* pHitByPhysicalEntity0 = physCollision->pEntity[0];
			if (pHitByPhysicalEntity1 && pHitByPhysicalEntity0)
			{
				if (pHitByPhysicalEntity1->GetType() == PE_PARTICLE)
					return;

				if (pHitByPhysicalEntity0->GetType() == PE_PARTICLE)
					return;
			}

			if (g_pGameCVars->g_ai_actor_fall_damage_enable > 0)
			{
				//CryLogAlways("CActor::OnCollisionEvent  velocity z = %f", GetActorPhysics().velocity.z);
				if (GetActorPhysics().velocity.z < g_pGameCVars->g_ai_actor_fall_z_veloc_min_to_add_damage)
				{
					//CryLogAlways("CActor::OnCollisionEvent");
					CGameRules *pGameRules = g_pGame->GetGameRules();
					if (pGameRules)
					{
						if (!IsDead())
						{
							float dmg_val = GetActorPhysics().velocity.z * (GetActorPhysics().velocity.z + GetActorPhysics().velocity.z);
							dmg_val *= g_pGameCVars->g_ai_actor_fall_damage_multipler;
							HitInfo info(GetEntityId(), GetEntityId(), 0,
								dmg_val, 0.0f, physCollision->idmat[1], physCollision->partid[1],
								CGameRules::EHitType::Fall, physCollision->pt, physCollision->n, physCollision->n);
							pGameRules->ClientHit(info);

							if (GetActorPhysics().velocity.z < g_pGameCVars->g_ai_actor_fall_z_veloc_max_to_add_damage)
							{
								dmg_val += 65535;
								pGameRules->ClientHit(info);
							}
						}
					}
				}
			}
		}
	}
}

void CActor::RequestToRunLuaFncFromActorScriptInstance(const char* func, const char* arg, bool fnc_from_gamerules)
{
	IEntity* pEvtEnt = GetEntity();
	if (fnc_from_gamerules)
		pEvtEnt = gEnv->pGame->GetIGameFramework()->GetIGameRulesSystem()->GetCurrentGameRulesEntity();

	if (!pEvtEnt)
		return;

	if (IScriptTable* pActScript = pEvtEnt->GetScriptTable())
	{
		IScriptSystem* pSS = pActScript->GetScriptSystem();
		if (pActScript->GetValueType(func) == svtFunction && pSS->BeginCall(pActScript, func))
		{
			pSS->PushFuncParam(pActScript);
			if (fnc_from_gamerules)
			{
				pSS->PushFuncParam(ScriptHandle(GetEntityId()));
				//pSS->PushFuncParam(arg);
			}
			pSS->PushFuncParam(arg);
			pSS->EndCall();
		}
	}
}

void CActor::RequestForDelayedPhysicaliseActor()
{
	delayed_physicalization_request = 0.5f;
}

void CActor::RequestForDelayedAddEquipPacks()
{
	delayed_add_packs_request = 0.1f;
}

void CActor::RequestToDeequipAllItemsFromEquipPacks()
{
	if (additional_pack_added && additional_pack_equipped)
	{
		std::list<EntityId>::const_iterator it = m_items_to_equip_pack1.begin();
		std::list<EntityId>::const_iterator end = m_items_to_equip_pack1.end();
		int count = m_items_to_equip_pack1.size();
		for (int i = 0; i < count, it != end; i++, it++)
		{
			CItem *pItem = GetItem(*it);
			if (pItem && pItem->GetEquipped() > 0)
			{
				if (pItem->GetOwnerId() == GetEntityId())
					EquipItem(*it);
			}
		}
		additional_pack_equipped = false;
	}

	if (additional_pack2_added && additional_pack2_equipped)
	{
		std::list<EntityId>::const_iterator it = m_items_to_equip_pack2.begin();
		std::list<EntityId>::const_iterator end = m_items_to_equip_pack2.end();
		int count = m_items_to_equip_pack2.size();
		for (int i = 0; i < count, it != end; i++, it++)
		{
			CItem *pItem = GetItem(*it);
			if (pItem && pItem->GetEquipped() > 0)
			{
				if (pItem->GetOwnerId() == GetEntityId())
					EquipItem(*it);
			}
		}
		additional_pack2_equipped = false;
	}
	//CryLogAlways("CActor::RequestToDeequipAllItemsFromEquipPacks");
}

void CActor::RequestToDeequipAllEquippedItems()
{
	for (int i = 0; i < eAESlots_Number; i++)
	{
		CItem *pItem_to_deequip = GetItem(GetCurrentEquippedItemId(i));
		if (pItem_to_deequip)
		{
			if (pItem_to_deequip->GetEquipped() > 0)
			{
				if (pItem_to_deequip->GetOwnerId() == GetEntityId())
				{
					if (i == eAESlot_Weapon_Left)
					{
						EquipWeaponToLeftHand(pItem_to_deequip->GetEntityId(), false);
						pItem_to_deequip->SetEquipped(0);
						continue;
					}
					//CryLogAlways("CActor::RequestToDeequipAllEquippedItems called, sequence 5");
					if (pItem_to_deequip->IsSelected())
					{
						//CryLogAlways("CActor::RequestToDeequipAllEquippedItems called, sequence 6");
						//DeSelectItem(pItem_to_deequip->GetEntityId(), false);
						EntityId noWeaponId = GetNoWeaponId();
						if (noWeaponId != pItem_to_deequip->GetEntityId())
							SelectItem(noWeaponId, false, true);
					}
					else
					{
						//CryLogAlways("CActor::RequestToDeequipAllEquippedItems called, sequence 7");
						EquipItem(pItem_to_deequip->GetEntityId());
						pItem_to_deequip->SetEquipped(0);
					}
				}
			}
		}
		else if (i == 1 && !pItem_to_deequip)
		{
			pItem_to_deequip = GetItem(GetCurrentItemId());
			if (pItem_to_deequip)
			{
				EntityId noWeaponId = GetNoWeaponId();
				if (noWeaponId != pItem_to_deequip->GetEntityId())
					SelectItem(noWeaponId, false, true);
			}
		}
	}

	if (GetInventory())
	{
		for (int i = 0; i < GetInventory()->GetCount(); i++)
		{
			EntityId id = GetInventory()->GetItem(i);
			if (id > 0)
			{
				CItem *pItem_to_deequip = GetItem(id);
				if (pItem_to_deequip)
				{
					pItem_to_deequip->ClearItemVisuals();
				}
			}
		}
	}
	/*if (GetInventory())
	{
		for (int i = 0; i < GetInventory()->GetCount(); i++)
		{
			EntityId id = GetInventory()->GetItem(i);
			if (id>0)
			{
				CItem *pItem_to_deequip = GetItem(id);
				if (pItem_to_deequip)
				{
					if (pItem_to_deequip->GetEquipped() > 0 && !pItem_to_deequip->IsItemIgnoredByHud())
					{
						if (pItem_to_deequip->GetOwnerId() == GetEntityId())
						{
							if (pItem_to_deequip->IsSelected())
							{
								EntityId noWeaponId = GetNoWeaponId();
								if (noWeaponId != pItem_to_deequip->GetEntityId())
									SelectItem(noWeaponId, false, true);
							}
							else
								EquipItem(pItem_to_deequip->GetEntityId());
						}
					}
				}
			}
		}
	}*/
}

void CActor::RequestToReequipUsedItemsOnPSerialize()
{
	if (IsRemote())
	{
		NET_RequestToReequipUsedItemsOnPSerialize();
		return;
	}
	m_delayed_left_hand_phy_wpns_equip.clear();
	m_delayed_left_hand_phy_wpns_equip.resize(0);
	if (IsPlayer())
	{
		for (int i = 0; i < eAESlots_Number; i++)
		{
			CItem *pItem_to_deequip = GetItem(equipped_cngtie[i]);
			if (pItem_to_deequip)
			{
				//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize have pItem_to_deequip");
				pItem_to_deequip->SetEquipped(0);
				if (pItem_to_deequip->GetEquipped() == 0)
				{
					if (pItem_to_deequip->GetOwnerId() == GetEntityId())
					{
						if (i == eAESlot_Shield || i == eAESlot_Torch)
						{
							m_delayed_left_hand_phy_wpns_equip.push_back(equipped_cngtie[i]);
							continue;
						}
						//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize item to equip = %s", pItem_to_deequip->GetEntity()->GetName());
						if(i == eAESlot_Weapon_Left)
						{ 
							EquipWeaponToLeftHand(equipped_cngtie[i]);
							continue;
						}
						if (pItem_to_deequip->CanSelect())
						{
							if(!GetRelaxedMod())
								SelectItem(pItem_to_deequip->GetEntityId(), false, true);
						}
						else
							EquipItem(pItem_to_deequip->GetEntityId());
					}
				}
			}
			else if (i == 1 && !pItem_to_deequip)
			{
				EntityId noWeaponId = 0;
				noWeaponId = GetNoWeaponId();
				if (noWeaponId > 0)
				{
					SelectItem(noWeaponId, false, true);
					//EquipItem(noWeaponId);
				}
			}
		}
	}
	else
	{
		//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 1");
		for (int i = 0; i < eAESlots_Number; i++)
		{
			//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 2");
			CItem *pItem_to_deequip = GetItem(equipped_cngtie[i]);
			if (pItem_to_deequip)
			{
				pItem_to_deequip->SetEquipped(0);
				//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 3");
				if (pItem_to_deequip->GetEquipped() == 0)
				{
					//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 4");
					if (pItem_to_deequip->GetOwnerId() == GetEntityId())
					{
						if (i == eAESlot_Shield || i == eAESlot_Torch)
						{
							m_delayed_left_hand_phy_wpns_equip.push_back(equipped_cngtie[i]);
							continue;
						}

						if (i == eAESlot_Weapon_Left)
						{
							EquipWeaponToLeftHand(equipped_cngtie[i]);
							continue;
						}
						//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 5");
						if (pItem_to_deequip->CanSelect())
						{
							//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 6");
							if (!GetRelaxedMod())
								SelectItem(pItem_to_deequip->GetEntityId(), false, true);
						}
						else
						{
							//CryLogAlways("CActor::RequestToReequipUsedItemsOnPSerialize called on npc, sequence 7");
							EquipItem(pItem_to_deequip->GetEntityId());
						}
					}
				}
			}
			else if (i == 1 && !pItem_to_deequip)
			{
				EntityId noWeaponId = 0;
				noWeaponId = GetNoWeaponId();
				if (noWeaponId > 0)
				{
					SelectItem(noWeaponId, false, true);
					//EquipItem(noWeaponId);
				}
			}
			else
			{
				if (!equipped_cngtie_names[i].empty())
				{
					if (GetInventory())
					{
						IEntityClass *pItmClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(equipped_cngtie_names[i].c_str());
						if (pItmClass)
						{
							EntityId itmId = 0;
							itmId = GetInventory()->GetItemByClass(pItmClass);
							if (itmId > 0)
							{
								CItem *pItem = GetItem(itmId);
								if (pItem)
								{
									pItem->SetEquipped(0);
									if (pItem->GetEquipped() == 0)
									{
										if (pItem->GetOwnerId() == GetEntityId())
										{
											if (i == eAESlot_Shield || i == eAESlot_Torch)
											{
												m_delayed_left_hand_phy_wpns_equip.push_back(itmId);
												continue;
											}

											if (i == eAESlot_Weapon_Left)
											{
												EquipWeaponToLeftHand(itmId);
												continue;
											}

											if (pItem->CanSelect())
											{
												if (!GetRelaxedMod())
													SelectItem(pItem->GetEntityId(), false, true);
											}
											else
												EquipItem(pItem->GetEntityId());
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if (m_delayed_left_hand_phy_wpns_equip.size() > 0)
	{
		if (!m_TimerDelayedReequipPhyReqItems)
			m_TimerDelayedReequipPhyReqItems = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.13f), false, functor(*this, &CActor::DelayedReequipPhyReqItems), NULL);
	}
}

void CActor::NET_RequestToReequipUsedItemsOnPSerialize()
{
	CryLogAlways("CActor::NET_RequestToReequipUsedItemsOnPSerialize()");
	//if (IsPlayer())
	//{
		for (int i = 0; i < eAESlots_Number; i++)
		{
			if (GetCurrentEquippedItemId(i) == equipped_cngtie[i])
				continue;

			CItem *pItem_to_deequip = GetItem(equipped_cngtie[i]);
			if (pItem_to_deequip)
			{
				if(pItem_to_deequip->GetEquipped() > 0)
					CryLogAlways("CActor::NET_RequestToReequipUsedItemsOnPSerialize item already equipped");

				CryLogAlways("CActor::NET_RequestToReequipUsedItemsOnPSerialize have pItem_to_deequip");
				//pItem_to_deequip->SetEquipped(0);
				if (pItem_to_deequip->GetEquipped() == 0)
				{
					if (pItem_to_deequip->GetOwnerId() == GetEntityId())
					{
						if (i == eAESlot_Weapon_Left)
						{
							EquipWeaponToLeftHand(pItem_to_deequip->GetEntityId());
							continue;
						}
						CryLogAlways("CActor::NET_RequestToReequipUsedItemsOnPSerialize item to equip = %s", pItem_to_deequip->GetEntity()->GetName());
						if (pItem_to_deequip->CanSelect())
						{
							
						}
						else
							Net_EquipItem(pItem_to_deequip->GetEntityId());
					}
				}
			}
		}
	//}
}

void CActor::RequestToReloadActorModelOrReinitAttachments(bool clear, bool serializing_event)
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (!pCharacter)
		return;

	if (IsPlayer() && serializing_event)
	{
		if (!m_chrmodelstr.empty())
		{
			SetActorModel(m_chrmodelstr.c_str(), true);
			UpdateActorModelFTO();
		}
		else
		{
			CryLogAlways("m_chrmodelstr is empty!!!!!");
		}
	}

	if (IsPlayer())
		return;

	if (!clear)
	{
		IAttachmentManager *pAttachmentManager = pCharacter->GetIAttachmentManager();
		if (!pAttachmentManager)
			return;
	}
	else
	{
		if (gEnv->IsEditor())
		{
			m_currModel = "null";
			SetActorModel(pCharacter->GetFilePath());
			FullyUpdateActorModel();
		}
	}
	//CryLogAlways("CActor::RequestToReloadActorModelOrReinitAttachments");
}

void CActor::AddSpellToActorSB(int id, bool from_xml)
{
	if (IsSpellInActorSB(id))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CGameXMLSettingAndGlobals::SActorSpell act_spell = pGameGlobals->GetCharacterSpellInfo(id, from_xml);
	if (act_spell.spell_id > -1)
		m_actor_spells_book.push_back(act_spell);
}

CGameXMLSettingAndGlobals::SActorSpell CActor::GetSpellInfoFromActorSB(int id)
{
	CGameXMLSettingAndGlobals::SActorSpell act_spell;
	act_spell.spell_id = -1;
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = m_actor_spells_book.end();
	int count = m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		act_spell = *it;
		if (act_spell.spell_id == id)
		{
			break;
		}
	}
	return act_spell;
}

bool CActor::IsSpellInActorSB(int id)
{
	CGameXMLSettingAndGlobals::SActorSpell act_spell;
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = m_actor_spells_book.end();
	int count = m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		act_spell = *it;
		if (act_spell.spell_id == id)
		{
			return true;
		}
	}
	return false;
}

void CActor::AddSpellToCurrentSpellsForUseSlot(int id, int slot)
{
	if (slot >= MAX_USABLE_SPELLS_SLOTS)
		return;

	CGameXMLSettingAndGlobals::SActorSpell act_spell = GetSpellInfoFromActorSB(id);
	if (act_spell.spell_id > -1)
	{
		Current_actor_spells[slot] = act_spell;
	}
	else//spell for now not in SB, adding it imidiately!!!
	{
		AddSpellToActorSB(id);
		act_spell = GetSpellInfoFromActorSB(id);
		if (act_spell.spell_id > -1)
		{
			Current_actor_spells[slot] = act_spell;
		}
	}

	if (gEnv->bMultiplayer)
	{
		if (IsPlayer())
		{
			CPlayer *pPlayer = static_cast<CPlayer*>(this);
			if (pPlayer)
			{
				SNetMgcSlots mgc_slt_inf;
				mgc_slt_inf.mgc_slot1 = GetSpellFromCurrentSpellsForUseSlot(1).spell_id;
				mgc_slt_inf.mgc_slot2 = GetSpellFromCurrentSpellsForUseSlot(2).spell_id;

				if (gEnv->bServer)
				{
					GetGameObject()->InvokeRMI(CActor::ClUdateMgcSlotsInfo(), mgc_slt_inf, eRMI_ToRemoteClients);
				}
				else
				{
					GetGameObject()->InvokeRMI(CActor::SVUdateMgcSlotsInfo(), mgc_slt_inf, eRMI_ToServer);
				}
			}
		}
	}
}

void CActor::NET_AddSpellToCurrentSpellsForUseSlot(int id, int slot)
{
	if (slot >= MAX_USABLE_SPELLS_SLOTS)
		return;

	CGameXMLSettingAndGlobals::SActorSpell act_spell = GetSpellInfoFromActorSB(id);
	if (act_spell.spell_id > -1)
	{
		Current_actor_spells[slot] = act_spell;
	}
	else//spell for now not in SB, adding it imidiately!!!
	{
		AddSpellToActorSB(id);
		act_spell = GetSpellInfoFromActorSB(id);
		if (act_spell.spell_id > -1)
		{
			Current_actor_spells[slot] = act_spell;
		}
	}
}

void CActor::DelSpellFromCurrentSpellsForUseSlot(int id, int slot)
{
	if (slot >= MAX_USABLE_SPELLS_SLOTS)
		return;

	CGameXMLSettingAndGlobals::SActorSpell act_spell = GetSpellInfoFromActorSB(id);
	if (act_spell.spell_id > -1)
	{
		if (Current_actor_spells[slot].spell_id == act_spell.spell_id)
		{
			CGameXMLSettingAndGlobals::SActorSpell act_spell_null;
			act_spell_null.spell_id = -1;
			Current_actor_spells[slot] = act_spell_null;
		}
	}

	if (gEnv->bMultiplayer)
	{
		if (IsPlayer())
		{
			CPlayer *pPlayer = static_cast<CPlayer*>(this);
			if (pPlayer)
			{
				SNetMgcSlots mgc_slt_inf;
				mgc_slt_inf.mgc_slot1 = GetSpellFromCurrentSpellsForUseSlot(1).spell_id;
				mgc_slt_inf.mgc_slot2 = GetSpellFromCurrentSpellsForUseSlot(2).spell_id;

				if (gEnv->bServer)
				{
					GetGameObject()->InvokeRMI(CActor::ClUdateMgcSlotsInfo(), mgc_slt_inf, eRMI_ToRemoteClients);
				}
				else
				{
					GetGameObject()->InvokeRMI(CActor::SVUdateMgcSlotsInfo(), mgc_slt_inf, eRMI_ToServer);
				}
			}
		}
	}
}

void CActor::ClearActorSB(bool full)
{
	m_actor_spells_book.clear();
	m_actor_spells_book.resize(0);
	if (full)
	{
		CGameXMLSettingAndGlobals::SActorSpell act_spell;
		act_spell.spell_id = -1;
		for (int i = 0; i < MAX_USABLE_SPELLS_SLOTS; i++)
		{
			Current_actor_spells[i] = act_spell;
		}
	}
}

CGameXMLSettingAndGlobals::SActorSpell CActor::GetSpellFromCurrentSpellsForUseSlot(int slot)
{
	CGameXMLSettingAndGlobals::SActorSpell act_spell;
	act_spell.spell_id = -1;
	if (slot >= MAX_USABLE_SPELLS_SLOTS)
		return act_spell;

	return Current_actor_spells[slot];
}

int CActor::GetSpellIdFromCurrentSpellsForUseSlot(int slot)
{
	if (slot >= MAX_USABLE_SPELLS_SLOTS)
		return -1;

	return Current_actor_spells[slot].spell_id;
}

float CActor::GetResistForDamageType(int type)
{
	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return 0.0f;

	return damage_resists[type];
}

void CActor::SetResistForDamageType(int type, float resist_val)
{
	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] = resist_val;
}

void CActor::AddResistForDamageType(int type, float resist_val)
{
	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] += resist_val;
}

void CActor::DelResistForDamageType(int type, float resist_val)
{
	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] -= resist_val;
}

float CActor::GetResistForDamageTypeByName(const char *name)
{
	int type = 0;
	if (g_pGame->GetGameRules())
	{
		type = g_pGame->GetGameRules()->GetHitTypeId(name);
	}

	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return 0.0f;

	return damage_resists[type];
}

void CActor::SetResistForDamageTypeByName(const char *name, float resist_val)
{
	int type = 0;
	if (g_pGame->GetGameRules())
	{
		type = g_pGame->GetGameRules()->GetHitTypeId(name);
	}

	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] = resist_val;
}

void CActor::AddResistForDamageTypeByName(const char *name, float resist_val)
{
	int type = 0;
	if (g_pGame->GetGameRules())
	{
		type = g_pGame->GetGameRules()->GetHitTypeId(name);
	}

	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] += resist_val;
}

void CActor::DelResistForDamageTypeByName(const char *name, float resist_val)
{
	int type = 0;
	if (g_pGame->GetGameRules())
	{
		type = g_pGame->GetGameRules()->GetHitTypeId(name);
	}

	if (type < 0 || type > MAX_DAMAGE_RESIST_TYPES)
		return;

	damage_resists[type] -= resist_val;
}

EntityId CActor::GetAnyItemInLeftHand()
{
	CItem *pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetCurrentEquippedItemId(eAESlot_Weapon_Left)));
	if (!pItem_lft)
		pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetCurrentEquippedItemId(eAESlot_Shield)));

	if (!pItem_lft)
		pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetCurrentEquippedItemId(eAESlot_Torch)));

	if (pItem_lft)
		return pItem_lft->GetEntityId();
	else
		return 0;
}

EntityId CActor::GetAnyItemInLeftHandCC() const
{
	CItem *pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_wpn_left_hand_id));
	if (!pItem_lft)
		pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_wear_shield_id));

	if (!pItem_lft)
		pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_wear_torch_id));

	if (pItem_lft)
		return pItem_lft->GetEntityId();
	else
		return NULL;
}

EntityId CActor::GetNoWeaponId()
{
	EntityId noWeaponId = 0;
	IEntityClass *pNoWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
	if (pNoWeaponClass)
	{
		if (GetInventory())
		{
			noWeaponId = GetInventory()->GetItemByClass(pNoWeaponClass);
			if (noWeaponId > 0)
				return noWeaponId;
		}
	}

	if (GetInventory() && !gEnv->bMultiplayer)
	{
		bool bHasNoWeapon = GetInventory()->GetCountOfClass("NoWeapon") > 0;
		bool bHasPcarWeapon = GetInventory()->GetCountOfClass("PickAndThrowWeapon") > 0;
		if (!bHasNoWeapon)
		{
			noWeaponId = gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(this, "NoWeapon", false, false, false);
		}
		if (!bHasPcarWeapon)
		{
			gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(this, "PickAndThrowWeapon", false, false, false);
		}
	}

	return noWeaponId;
}

bool CActor::CanDoAnyActionInCastStateWithSWeapon(EntityId selected_wpn_id)
{
	if(selected_wpn_id == 0)
		return true;

	if(!m_pActorSpellsActions)
		return true;

	if (!m_lft_hand_blc && !m_rgt_hand_blc)
	{
		return true;
	}
	else
	{
		CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current_ot;
		for (int i = 0; i < MAX_PER_CAST_SLOTS; i++)
		{
			if (m_pActorSpellsActions->current_cast_slots[i] <= -1)
				continue;

			pUser_Spell_Current_ot.spell_id = -1;
			pUser_Spell_Current_ot = GetSpellFromCurrentSpellsForUseSlot(m_pActorSpellsActions->current_cast_slots[i]);
			if (pUser_Spell_Current_ot.spell_id > 0)
			{
				if(pUser_Spell_Current_ot.two_handed_spell)
					return false;

				if (selected_wpn_id == GetNoWeaponId())
				{
					return true;
				}
				else if (selected_wpn_id == GetAnyItemInLeftHand())
				{
					if(m_lft_hand_blc)
						return false;
					else if (m_lft_hand_blc && m_rgt_hand_blc)
						return false;
					else
						return true;
				}
				else if (selected_wpn_id == GetCurrentItemId())
				{
					if (m_rgt_hand_blc)
						return false;
					else if (m_lft_hand_blc && m_rgt_hand_blc)
						return false;
					else
						return true;
				}
			}
		}
	}
	return true;
}

int32 CActor::FindNearestBone(Vec3 pos, float distance)
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (!pCharacter)
		return -333;

	IDefaultSkeleton& rIDefaultSkeleton = pCharacter->GetIDefaultSkeleton();
	float old_bone_distance = 10000.0f;
	int32 current_nearest_bone = -333;
	const int joint_count = rIDefaultSkeleton.GetJointCount();
	for (int i = 0; i < joint_count; i++)
	{
		string joint_name = rIDefaultSkeleton.GetJointNameByID(i);
		if (joint_name.empty())
			continue;

		//CryLogAlways(joint_name.c_str());
		ISkeletonPose* pSkeleton = pCharacter->GetISkeletonPose();
		QuatT jointWorld = pSkeleton->GetAbsJointByID(i);
		Vec3 JointPos = jointWorld.t;
		Vec3 vBonePos = GetEntity()->GetSlotWorldTM(0) * JointPos;
		if (distance > 0.0f)
		{
			float this_dist = pos.GetDistance(vBonePos);
			if (this_dist <= distance)
			{
				current_nearest_bone = (int32)i;
				break;
			}
		}
		else
		{
			float this_dist = pos.GetDistance(vBonePos);
			if (this_dist < old_bone_distance)
			{
				old_bone_distance = this_dist;
				current_nearest_bone = (int32)i;
			}
			else
			{
				continue;
			}
		}
	}
	return current_nearest_bone;
}

Vec3 CActor::GetBonePosition(const char* bone_name, bool local)
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (!pCharacter)
		return Vec3(ZERO);

	int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(bone_name);
	if (id == -1)
	{
		gEnv->pLog->Log("ERROR: CActor::GetBonePosition: Bone not found: %s", bone_name);
		return Vec3(ZERO);
	}

	Vec3 JointPos = pCharacter->GetISkeletonPose()->GetAbsJointByID(id).t;
	Vec3 vBonePos(ZERO);
	if (!local)
		vBonePos = GetEntity()->GetSlotWorldTM(0) * JointPos;
	else
		vBonePos = JointPos;

	return vBonePos;
}

Quat CActor::GetBoneRotation(const char* bone_name)
{
	ICharacterInstance* pCharacter = GetEntity()->GetCharacter(0);
	if (!pCharacter)
		return Quat(IDENTITY);

	int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(bone_name);
	if (id == -1)
	{
		gEnv->pLog->Log("ERROR: CActor::GetBoneRotation: Bone not found: %s", bone_name);
		return Quat(IDENTITY);
	}
	Quat rootn = pCharacter->GetISkeletonPose()->GetAbsJointByID(id).q;
	return rootn;
}

void CActor::SetLastDamagePosAndNormal(Vec3 pos, Vec3 normal)
{
	last_damage_hit_pos = pos;
	last_damage_hit_normal = normal;
}

void CActor::SetGravityZ(float gr_z)
{
	if (IPhysicalEntity* pPhysics = GetEntity()->GetPhysics())
	{
		pe_action_awake paa;
		paa.bAwake = 1;
		pPhysics->Action(&paa);

		pe_player_dynamics pd;
		pd.gravity.z = gr_z;
		pPhysics->SetParams(&pd);
		old_gravity_z = gr_z;
		if (m_pAnimatedCharacter)
		{
			m_pAnimatedCharacter->SetRagdollizedZGravity(old_gravity_z);
			if (GetActorStats()->isRagDoll)
			{
				//CryLogAlways("ActorIsRagdolized");
				m_pAnimatedCharacter->SetRagdollizedZGravity(old_gravity_z);
			}
		}
		pe_simulation_params sp;
		pPhysics->GetParams(&sp);
		sp.gravity.z = old_gravity_z;
		sp.gravityFreefall.z = old_gravity_z;
		pPhysics->SetParams(&sp);
		m_actorPhysics.gravity.z = old_gravity_z;
		if (IsDead())
		{
			pe_action_impulse ai;
			ai.impulse = Vec3(0, 0, 3.0f);
			ai.ipart = -1;
			ai.point = GetEntity()->GetPos();
			ai.iApplyTime = 0;
			m_pImpulseHandler->SetOnRagdollPhysicalizedImpulse(ai);
		}
	}
}

void CActor::SetInertiaAndInertiaAccel(float inertia, float inertiaAcc)
{
	m_inertia = inertia;
	m_inertiaAccel = inertiaAcc;
}

bool CActor::IsActorVelocityMoreThan(int velType, float velValue)
{
	if (velType == 0)
	{
		Vec2 horiz_vel = Vec2(GetActorPhysics().velocity.x, GetActorPhysics().velocity.y);
		return horiz_vel.GetLength() > velValue;
	}
	else if (velType == 1)
	{
		return GetActorPhysics().velocity.z > velValue;
	}
	else if (velType == 2)
	{
		return GetActorPhysics().velocity.GetLength() > velValue;
	}
	return false;
}

int CActor::GetRealItemsCountInInventory()
{
	int count = 0;
	if (m_pInventory)
	{
		int inv_count = m_pInventory->GetCount();
		for (int i = 0; i < inv_count; i++)
		{
			EntityId id = m_pInventory->GetItem(i);
			CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			if (curItem)
			{
				count++;
			}
		}
	}
	return count;
}

bool CActor::CanDoAnyActionWNeededTwoHands()
{
	if (m_rgt_hand_blc || m_lft_hand_blc)
	{
		return false;
	}
	CItem *pItem_lft = GetItem(GetAnyItemInLeftHand());
	if (pItem_lft && pItem_lft->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
	{
		//CryLogAlways("CActor::CanDoAnyActionWNeededTwoHands() return false bt pItem_lft &eIF_BlockActions");
		return false;
	}
	CItem *pItem_rgt = GetItem(GetCurrentItemId());
	if (pItem_rgt && pItem_rgt->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
	{
		//CryLogAlways("CActor::CanDoAnyActionWNeededTwoHands() return false bt pItem_rgt &eIF_BlockActions");
		return false;
	}
	return true;
}

string CActor::GetActorName()
{
	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		pScriptTable->GetValue("Properties", props);
		CScriptSetGetChain prop(props);
		string charactername = "";
		prop.GetValue("charactername", charactername);
		if (!charactername.empty())
			return charactername;
		else
			return GetEntity()->GetName();
	}
	return GetEntity()->GetName();
}

void CActor::CheckActorBodyPartsAfterPSerialize(int chr_slot)
{
	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[0].empty())
		{
			base_att_eye_right = pGameGlobals->customization_data.character_base_attachment_eyes[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[1].empty())
		{
			base_att_eye_left = pGameGlobals->customization_data.character_base_attachment_eyes[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[0].empty())
		{
			base_att_head = pGameGlobals->customization_data.character_base_attachment_head[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[1].empty())
		{
			base_att_hair = pGameGlobals->customization_data.character_base_attachment_head[1];
		}
	}

	ICharacterInstance *pOwnerCharacter = GetEntity()->GetCharacter(chr_slot);
	if (!pOwnerCharacter)
		return;

	IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
	if (pAttachmentManager)
		return;

	IAttachment *pAttachmentheadface = pAttachmentManager->GetInterfaceByName(base_att_head.c_str());
	IAttachment *pAttachment_eyergt = pAttachmentManager->GetInterfaceByName(base_att_eye_right.c_str());
	IAttachment *pAttachment_eyelft = pAttachmentManager->GetInterfaceByName(base_att_eye_left.c_str());
	IAttachment *pHairAttachment = pAttachmentManager->GetInterfaceByName(base_att_hair.c_str());
	IAttachment *pAttachmentfoots = pAttachmentManager->GetInterfaceByName(base_att_foots.c_str());
	IAttachment *pAttachmentUnderwearlow = pAttachmentManager->GetInterfaceByName(base_att_lowerUdw.c_str());
	IAttachment *pAttachmentLowerbody = pAttachmentManager->GetInterfaceByName(base_att_lowerBd.c_str());
	IAttachment *pAttachmentLowerbodypp = pAttachmentManager->GetInterfaceByName(base_att_upperCnp.c_str());
	IAttachment *pAttachmentUnderwearupper = pAttachmentManager->GetInterfaceByName(base_att_upperUdw.c_str());
	IAttachment *pAttachmentupperbody = pAttachmentManager->GetInterfaceByName(base_att_upperBd.c_str());
	IAttachment *pAttachmenthands = pAttachmentManager->GetInterfaceByName(base_att_hands.c_str());
	if (pAttachmentheadface)
	{
		if (!m_headmodel.empty())
		{
			if (pAttachmentheadface->GetIAttachmentSkin())
			{
				if (pAttachmentheadface->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentheadface->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_headmodel)
					{
						CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
				}
			}
			else
			{
				if (pAttachmentheadface->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachment_eyergt)
	{
		if (!m_eye_r.empty())
		{
			if (pAttachment_eyergt->GetIAttachmentSkin())
			{
				if (pAttachment_eyergt->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyergt->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_r)
					{
						CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
				}
			}
			else
			{
				if (pAttachment_eyergt->GetIAttachmentObject())
				{
					if (pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_r)
						{
							CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
					}
				}
			}
		}
	}
	if (pAttachment_eyelft)
	{
		if (!m_eye_l.empty())
		{
			if (pAttachment_eyelft->GetIAttachmentSkin())
			{
				if (pAttachment_eyelft->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyelft->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_l)
					{
						CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
				}
			}
			else
			{
				if (pAttachment_eyelft->GetIAttachmentObject())
				{
					if (pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_l)
						{
							CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
					}
				}
			}
		}
	}
	if (pHairAttachment)
	{
		if (!m_hairmodel.empty())
		{
			if (pHairAttachment->GetIAttachmentSkin())
			{
				if (pHairAttachment->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pHairAttachment->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hairmodel)
					{
						CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
				}
			}
			else
			{
				if (pHairAttachment->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentfoots)
	{
		if (!m_foots_model.empty())
		{
			if (pAttachmentfoots->GetIAttachmentSkin())
			{
				if (pAttachmentfoots->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentfoots->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_foots_model)
					{
						CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
				}
			}
			else
			{
				if (pAttachmentfoots->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearlow)
	{
		if (!m_underwear01.empty())
		{
			if (pAttachmentUnderwearlow->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear01)
					{
						CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearlow->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbody)
	{
		if (!m_lowerbody_model.empty())
		{
			if (pAttachmentLowerbody->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_lowerbody_model)
					{
						CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbodypp)
	{
		if (!m_pppp.empty())
		{
			if (pAttachmentLowerbodypp->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_pppp)
					{
						CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbodypp->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearupper)
	{
		if (!m_underwear02.empty())
		{
			if (pAttachmentUnderwearupper->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear02)
					{
						CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearupper->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentupperbody)
	{
		if (!m_upperbody_model.empty())
		{
			if (pAttachmentupperbody->GetIAttachmentSkin())
			{
				if (pAttachmentupperbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentupperbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_upperbody_model)
					{
						CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentupperbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmenthands)
	{
		if (!m_hands_model.empty())
		{
			if (pAttachmenthands->GetIAttachmentSkin())
			{
				if (pAttachmenthands->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmenthands->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hands_model)
					{
						CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
				}
			}
			else
			{
				if (pAttachmenthands->GetIAttachmentObject())
				{

				}
			}
		}
	}
	const int number_arm_atts = 9;
	string attachment_arm_cf[number_arm_atts];
	attachment_arm_cf[0] = "armorboots";
	attachment_arm_cf[1] = "armorlegs";
	attachment_arm_cf[2] = "armorchest";
	attachment_arm_cf[3] = "armorarms";
	attachment_arm_cf[4] = "armorhead";
	attachment_arm_cf[5] = "arrow_quiver";
	attachment_arm_cf[6] = "torch";
	attachment_arm_cf[7] = "armorshield";
	attachment_arm_cf[8] = "bolts_quiver";
	int att_num = pAttachmentManager->GetAttachmentCount();
	for (int i = 0; i < att_num+1; i++)
	{
		IAttachment *pAttacments_player = pAttachmentManager->GetInterfaceByIndex(i);
		if (pAttacments_player)
		{
			if (pAttachmentheadface)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentheadface->GetNameCRC())
					continue;
			}
			if (pAttachment_eyergt)
			{
				if (pAttacments_player->GetNameCRC() == pAttachment_eyergt->GetNameCRC())
					continue;
			}
			if (pAttachment_eyelft)
			{
				if (pAttacments_player->GetNameCRC() == pAttachment_eyelft->GetNameCRC())
					continue;
			}
			if (pHairAttachment)
			{
				if (pAttacments_player->GetNameCRC() == pHairAttachment->GetNameCRC())
					continue;
			}
			if (pAttachmentfoots)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentfoots->GetNameCRC())
					continue;
			}
			if (pAttachmentUnderwearlow)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentUnderwearlow->GetNameCRC())
					continue;
			}
			if (pAttachmentLowerbody)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentLowerbody->GetNameCRC())
					continue;
			}
			if (pAttachmentLowerbodypp)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentLowerbodypp->GetNameCRC())
					continue;
			}
			if (pAttachmentUnderwearupper)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentUnderwearupper->GetNameCRC())
					continue;
			}
			if (pAttachmentupperbody)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmentupperbody->GetNameCRC())
					continue;
			}
			if (pAttachmenthands)
			{
				if (pAttacments_player->GetNameCRC() == pAttachmenthands->GetNameCRC())
					continue;
			}

			if (!pAttacments_player->GetIAttachmentObject()	&& (pAttacments_player->GetType() == CA_BONE || pAttacments_player->GetType() == CA_FACE))
			{
				continue;
			}
			if (!pAttacments_player->GetIAttachmentSkin() && (pAttacments_player->GetType() == CA_SKIN || pAttacments_player->GetType() == CA_VCLOTH))
			{
				continue;
			}
			string att_to_del_name = pAttacments_player->GetName();
			for (int ic = 0; ic < att_to_del_name.size(); ic++)
			{
				bool break_and_delete_att = false;
				for (int icc = 0; icc < number_arm_atts; icc++)
				{
					string this_rp = att_to_del_name.substr(ic, attachment_arm_cf[icc].size());
					if (this_rp == attachment_arm_cf[icc])
					{
						if (!break_and_delete_att)
						{
							break_and_delete_att = true;
							break;
						}
					}
				}

				if (break_and_delete_att)
				{
					pAttacments_player->ClearBinding();
					break;
				}
			}			
		}
	}
}

void CActor::CheckActorBodyAndAllAttachments(int chr_slot)
{
	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[0].empty())
		{
			base_att_eye_right = pGameGlobals->customization_data.character_base_attachment_eyes[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[1].empty())
		{
			base_att_eye_left = pGameGlobals->customization_data.character_base_attachment_eyes[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[0].empty())
		{
			base_att_head = pGameGlobals->customization_data.character_base_attachment_head[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[1].empty())
		{
			base_att_hair = pGameGlobals->customization_data.character_base_attachment_head[1];
		}
	}

	ICharacterInstance *pOwnerCharacter = GetEntity()->GetCharacter(chr_slot);
	if (!pOwnerCharacter)
		return;

	IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
	if (pAttachmentManager)
		return;

	IAttachment *pAttachmentheadface = pAttachmentManager->GetInterfaceByName(base_att_head.c_str());
	IAttachment *pAttachment_eyergt = pAttachmentManager->GetInterfaceByName(base_att_eye_right.c_str());
	IAttachment *pAttachment_eyelft = pAttachmentManager->GetInterfaceByName(base_att_eye_left.c_str());
	IAttachment *pHairAttachment = pAttachmentManager->GetInterfaceByName(base_att_hair.c_str());
	IAttachment *pAttachmentfoots = pAttachmentManager->GetInterfaceByName(base_att_foots.c_str());
	IAttachment *pAttachmentUnderwearlow = pAttachmentManager->GetInterfaceByName(base_att_lowerUdw.c_str());
	IAttachment *pAttachmentLowerbody = pAttachmentManager->GetInterfaceByName(base_att_lowerBd.c_str());
	IAttachment *pAttachmentLowerbodypp = pAttachmentManager->GetInterfaceByName(base_att_upperCnp.c_str());
	IAttachment *pAttachmentUnderwearupper = pAttachmentManager->GetInterfaceByName(base_att_upperUdw.c_str());
	IAttachment *pAttachmentupperbody = pAttachmentManager->GetInterfaceByName(base_att_upperBd.c_str());
	IAttachment *pAttachmenthands = pAttachmentManager->GetInterfaceByName(base_att_hands.c_str());
	if (pAttachmentheadface)
	{
		if (!m_headmodel.empty())
		{
			if (pAttachmentheadface->GetIAttachmentSkin())
			{
				if (pAttachmentheadface->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentheadface->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_headmodel)
					{
						CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
				}
			}
			else
			{
				if (pAttachmentheadface->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachment_eyergt)
	{
		if (!m_eye_r.empty())
		{
			if (pAttachment_eyergt->GetIAttachmentSkin())
			{
				if (pAttachment_eyergt->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyergt->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_r)
					{
						CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
				}
			}
			else
			{
				if (pAttachment_eyergt->GetIAttachmentObject())
				{
					if (pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_r)
						{
							CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
					}
				}
			}
		}
	}
	if (pAttachment_eyelft)
	{
		if (!m_eye_l.empty())
		{
			if (pAttachment_eyelft->GetIAttachmentSkin())
			{
				if (pAttachment_eyelft->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyelft->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_l)
					{
						CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
				}
			}
			else
			{
				if (pAttachment_eyelft->GetIAttachmentObject())
				{
					if (pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_l)
						{
							CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
					}
				}
			}
		}
	}
	if (pHairAttachment)
	{
		if (!m_hairmodel.empty())
		{
			if (pHairAttachment->GetIAttachmentSkin())
			{
				if (pHairAttachment->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pHairAttachment->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hairmodel)
					{
						CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
				}
			}
			else
			{
				if (pHairAttachment->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentfoots)
	{
		if (!m_foots_model.empty())
		{
			if (pAttachmentfoots->GetIAttachmentSkin())
			{
				if (pAttachmentfoots->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentfoots->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_foots_model && !current_mdl_path.empty())
					{

						CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
					}

					if (GetEquippedState(eAESlot_Boots) == true)
					{
						pAttachmentfoots->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Boots) == false)
						CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
				}
			}
			else
			{
				if (pAttachmentfoots->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearlow)
	{
		if (!m_underwear01.empty())
		{
			if (pAttachmentUnderwearlow->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear01 && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentUnderwearlow->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearlow->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbody)
	{
		if (!m_lowerbody_model.empty())
		{
			if (pAttachmentLowerbody->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_lowerbody_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentLowerbody->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbodypp)
	{
		if (!m_pppp.empty())
		{
			if (pAttachmentLowerbodypp->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_pppp && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentLowerbodypp->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbodypp->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearupper)
	{
		if (!m_underwear02.empty())
		{
			if (pAttachmentUnderwearupper->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear02 && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
					}

					if (GetEquippedState(eAESlot_Torso) == true)
					{
						pAttachmentUnderwearupper->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Torso) == false)
						CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearupper->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentupperbody)
	{
		if (!m_upperbody_model.empty())
		{
			if (pAttachmentupperbody->GetIAttachmentSkin())
			{
				if (pAttachmentupperbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentupperbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_upperbody_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
					}

					if (GetEquippedState(eAESlot_Torso) == true)
					{
						pAttachmentupperbody->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Torso) == false)
						CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentupperbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmenthands)
	{
		if (!m_hands_model.empty())
		{
			if (pAttachmenthands->GetIAttachmentSkin())
			{
				if (pAttachmenthands->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmenthands->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hands_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
					}

					if (GetEquippedState(eAESlot_Arms) == true)
					{
						pAttachmenthands->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Arms) == false)
						CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
				}
			}
			else
			{
				if (pAttachmenthands->GetIAttachmentObject())
				{

				}
			}
		}
	}
}

void CActor::Net_CheckActorBodyAndAllAttachments(int chr_slot)
{
	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[0].empty())
		{
			base_att_eye_right = pGameGlobals->customization_data.character_base_attachment_eyes[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[1].empty())
		{
			base_att_eye_left = pGameGlobals->customization_data.character_base_attachment_eyes[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[0].empty())
		{
			base_att_head = pGameGlobals->customization_data.character_base_attachment_head[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[1].empty())
		{
			base_att_hair = pGameGlobals->customization_data.character_base_attachment_head[1];
		}
	}

	ICharacterInstance *pOwnerCharacter = GetEntity()->GetCharacter(chr_slot);
	if (!pOwnerCharacter)
		return;

	IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
	if (pAttachmentManager)
		return;

	IAttachment *pAttachmentheadface = pAttachmentManager->GetInterfaceByName(base_att_head.c_str());
	IAttachment *pAttachment_eyergt = pAttachmentManager->GetInterfaceByName(base_att_eye_right.c_str());
	IAttachment *pAttachment_eyelft = pAttachmentManager->GetInterfaceByName(base_att_eye_left.c_str());
	IAttachment *pHairAttachment = pAttachmentManager->GetInterfaceByName(base_att_hair.c_str());
	IAttachment *pAttachmentfoots = pAttachmentManager->GetInterfaceByName(base_att_foots.c_str());
	IAttachment *pAttachmentUnderwearlow = pAttachmentManager->GetInterfaceByName(base_att_lowerUdw.c_str());
	IAttachment *pAttachmentLowerbody = pAttachmentManager->GetInterfaceByName(base_att_lowerBd.c_str());
	IAttachment *pAttachmentLowerbodypp = pAttachmentManager->GetInterfaceByName(base_att_upperCnp.c_str());
	IAttachment *pAttachmentUnderwearupper = pAttachmentManager->GetInterfaceByName(base_att_upperUdw.c_str());
	IAttachment *pAttachmentupperbody = pAttachmentManager->GetInterfaceByName(base_att_upperBd.c_str());
	IAttachment *pAttachmenthands = pAttachmentManager->GetInterfaceByName(base_att_hands.c_str());
	if (pAttachmentheadface)
	{
		if (!m_headmodel.empty())
		{
			if (pAttachmentheadface->GetIAttachmentSkin())
			{
				if (pAttachmentheadface->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentheadface->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_headmodel)
					{
						CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_head.c_str(), m_headmodel.c_str());
				}
			}
			else
			{
				if (pAttachmentheadface->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachment_eyergt)
	{
		if (!m_eye_r.empty())
		{
			if (pAttachment_eyergt->GetIAttachmentSkin())
			{
				if (pAttachment_eyergt->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyergt->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_r)
					{
						CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str());
				}
			}
			else
			{
				if (pAttachment_eyergt->GetIAttachmentObject())
				{
					if (pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyergt->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_r)
						{
							CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_right.c_str(), m_eye_r.c_str(), "eye_right_bone", true, false);
					}
				}
			}
		}
	}
	if (pAttachment_eyelft)
	{
		if (!m_eye_l.empty())
		{
			if (pAttachment_eyelft->GetIAttachmentSkin())
			{
				if (pAttachment_eyelft->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachment_eyelft->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_eye_l)
					{
						CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str());
				}
			}
			else
			{
				if (pAttachment_eyelft->GetIAttachmentObject())
				{
					if (pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj())
					{
						string current_mdl_path = pAttachment_eyelft->GetIAttachmentObject()->GetIStatObj()->GetFilePath();
						if (current_mdl_path != m_eye_l)
						{
							CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
						}
					}
					else
					{
						CreateBoneAttachment(0, base_att_eye_left.c_str(), m_eye_l.c_str(), "eye_left_bone", true, false);
					}
				}
			}
		}
	}
	if (pHairAttachment)
	{
		if (!m_hairmodel.empty())
		{
			if (pHairAttachment->GetIAttachmentSkin())
			{
				if (pHairAttachment->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pHairAttachment->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hairmodel)
					{
						CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
					}
				}
				else
				{
					CreateSkinAttachment(0, base_att_hair.c_str(), m_hairmodel.c_str());
				}
			}
			else
			{
				if (pHairAttachment->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentfoots)
	{
		if (!m_foots_model.empty())
		{
			if (pAttachmentfoots->GetIAttachmentSkin())
			{
				if (pAttachmentfoots->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentfoots->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_foots_model && !current_mdl_path.empty())
					{

						CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
					}

					if (GetEquippedState(eAESlot_Boots) == true)
					{
						pAttachmentfoots->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Boots) == false)
						CreateSkinAttachment(0, base_att_foots.c_str(), m_foots_model.c_str());
				}
			}
			else
			{
				if (pAttachmentfoots->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearlow)
	{
		if (!m_underwear01.empty())
		{
			if (pAttachmentUnderwearlow->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearlow->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear01 && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentUnderwearlow->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_lowerUdw.c_str(), m_underwear01.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearlow->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbody)
	{
		if (!m_lowerbody_model.empty())
		{
			if (pAttachmentLowerbody->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_lowerbody_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentLowerbody->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_lowerBd.c_str(), m_lowerbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentLowerbodypp)
	{
		if (!m_pppp.empty())
		{
			if (pAttachmentLowerbodypp->GetIAttachmentSkin())
			{
				if (pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentLowerbodypp->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_pppp && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
					}

					if (GetEquippedState(eAESlot_Pants) == true)
					{
						pAttachmentLowerbodypp->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Pants) == false)
						CreateSkinAttachment(0, base_att_upperCnp.c_str(), m_pppp.c_str());
				}
			}
			else
			{
				if (pAttachmentLowerbodypp->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentUnderwearupper)
	{
		if (!m_underwear02.empty())
		{
			if (pAttachmentUnderwearupper->GetIAttachmentSkin())
			{
				if (pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentUnderwearupper->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_underwear02 && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
					}

					if (GetEquippedState(eAESlot_Torso) == true)
					{
						pAttachmentUnderwearupper->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Torso) == false)
						CreateSkinAttachment(0, base_att_upperUdw.c_str(), m_underwear02.c_str());
				}
			}
			else
			{
				if (pAttachmentUnderwearupper->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmentupperbody)
	{
		if (!m_upperbody_model.empty())
		{
			if (pAttachmentupperbody->GetIAttachmentSkin())
			{
				if (pAttachmentupperbody->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmentupperbody->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_upperbody_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
					}

					if (GetEquippedState(eAESlot_Torso) == true)
					{
						pAttachmentupperbody->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Torso) == false)
						CreateSkinAttachment(0, base_att_upperBd.c_str(), m_upperbody_model.c_str());
				}
			}
			else
			{
				if (pAttachmentupperbody->GetIAttachmentObject())
				{

				}
			}
		}
	}
	if (pAttachmenthands)
	{
		if (!m_hands_model.empty())
		{
			if (pAttachmenthands->GetIAttachmentSkin())
			{
				if (pAttachmenthands->GetIAttachmentSkin()->GetISkin())
				{
					string current_mdl_path = pAttachmenthands->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
					if (current_mdl_path != m_hands_model && !current_mdl_path.empty())
					{
						CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
					}

					if (GetEquippedState(eAESlot_Arms) == true)
					{
						pAttachmenthands->ClearBinding();
					}
				}
				else
				{
					if (GetEquippedState(eAESlot_Arms) == false)
						CreateSkinAttachment(0, base_att_hands.c_str(), m_hands_model.c_str());
				}
			}
			else
			{
				if (pAttachmenthands->GetIAttachmentObject())
				{

				}
			}
		}
	}
}

void CActor::UpdateAllSkinAttachments(int chr_slot)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(chr_slot);
	if (!pCharacter)
	{
		return;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if (!pIAttachmentManager)
		return;

	if (update_all_skin_attachment_at_next_frame == 0)
		return;

	if (!GetGameObject()->IsProbablyVisible() || GetEntity()->IsHidden())
		return;

	CCamera& cam = GetISystem()->GetViewCamera();
	float dist_to_cam = cam.GetPosition().GetDistance(GetEntity()->GetWorldPos());
	if(dist_to_cam > 50.0f)
		return;

	//if(!IsPlayer())
	//	CryLogAlways("CActor::UpdateAllSkinAttachments called, not player, update_all_skin_attachment_at_next_frame == %i, dist_to_cam == %f", update_all_skin_attachment_at_next_frame, dist_to_cam);

	if (update_all_skin_attachment_at_next_frame == 1)
	{
		std::vector<SDelayedAttachments>::iterator it = s_SDelayedAttachments.begin();
		std::vector<SDelayedAttachments>::iterator end = s_SDelayedAttachments.end();
		int count = s_SDelayedAttachments.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			SDelayedAttachments s_act_cur_att = *it;
			CreateSkinAttachment(chr_slot, s_act_cur_att.attachment_name.c_str(), s_act_cur_att.attachment_binding_path.c_str());
		}
		s_SDelayedAttachments.clear();
	}
	else if (update_all_skin_attachment_at_next_frame == 2)
	{
		s_SDelayedAttachments.clear();
		const int att_num = pIAttachmentManager->GetAttachmentCount();
		for (int i = 0; i < att_num; i++)
		{
			IAttachment *pAttacments_nrm = pIAttachmentManager->GetInterfaceByIndex(i);
			if (pAttacments_nrm)
			{
				//CryLogAlways("LoadAttachmentList seq 3");
				if (pAttacments_nrm->GetType() != CA_SKIN)
					continue;

				SDelayedAttachments att;
				if (pAttacments_nrm->GetIAttachmentSkin() && pAttacments_nrm->GetIAttachmentSkin()->GetISkin())
				{
					att.attachment_binding_path = pAttacments_nrm->GetIAttachmentSkin()->GetISkin()->GetModelFilePath();
				}
				att.attachment_name = pAttacments_nrm->GetName();
				s_SDelayedAttachments.push_back(att);
				DeleteSkinAttachment(chr_slot, pAttacments_nrm->GetName(), true);

			}
		}
	}

	if (update_all_skin_attachment_at_next_frame == 1)
		update_all_skin_attachment_at_next_frame = 0;

	if (update_all_skin_attachment_at_next_frame == 2)
		update_all_skin_attachment_at_next_frame = 1;
}

void CActor::HideOrUnhideAttachment(int characterSlot, const char *attachmentName, bool hide)
{
	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(characterSlot);
	if (!pCharacter)
	{
		return;
	}
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	if (!pIAttachmentManager)
	{
		return;
	}
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(attachmentName);
	if (pIAttachment)
	{
		pIAttachment->HideAttachment(hide);
	}
}

void CActor::UpdateQuivers()
{
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (nwAction)
	{
		nwAction->UpdateQuiverAttachment(GetEntityId(), GetCurrentEquippedItemId(eAESlot_Ammo_Arrows));
		nwAction->UpdateQuiverAttachment(GetEntityId(), GetCurrentEquippedItemId(eAESlot_Ammo_Bolts));
	}
}

void CActor::UpdateSkinAttachmentsAndEts(float frameTime)
{
	if (GetEntity()->IsHidden())
		return;
	//-------------One operation on character skin & model file per frame----------------------------------------------------------------------------------
	//-------------Delayed operation with character skin---------------------------------------------------------------------------------------------------
	bool r_model_valc[3];
	r_model_valc[0] = false;
	r_model_valc[1] = false;
	r_model_valc[2] = false;
	//--------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//model reload & update****************************************************************************************************
	if (rload_pl_model_timer_after_ps > 0.0f)
	{
		if (gEnv->pTimer->GetFrameTime() > 0.02f)
			rload_pl_model_timer_after_ps -= 0.02f;
		else
			rload_pl_model_timer_after_ps -= gEnv->pTimer->GetFrameTime();

		if (rload_pl_model_timer_after_ps <= 0.0f)
		{
			RequestToReloadActorModelOrReinitAttachments(false, true);
			rload_pl_model_timer_after_ps = 0.0f;
			serialization_reinit_customization_setting_on_character = 0.01f;
			reequip_timer_cn1 = 0.1f;
			r_model_valc[0] = true;
		}
	}
	//*****************************************************************************************************************************
	//Character Model player customization setting reload & update*****************************************************************
	if (serialization_reinit_customization_setting_on_character > 0.0f && !r_model_valc[0])
	{
		if (gEnv->pTimer->GetFrameTime() > 0.02f)
			serialization_reinit_customization_setting_on_character -= 0.02f;
		else
			serialization_reinit_customization_setting_on_character -= gEnv->pTimer->GetFrameTime();

		if (serialization_reinit_customization_setting_on_character <= 0.0f)
		{
			if (rload_pl_model_timer_after_ps == 0.0f)
			{
				CPlayerCharacterCreatingSys* p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
				if (p_PlCrSys && IsPlayer() && !IsRemote())
				{
					p_PlCrSys->ReLoadPlayerCharacterCsetting();
					r_model_valc[1] = true;
				}

				if (p_PlCrSys && IsRemote())
				{
					p_PlCrSys->ReLoadRemotePlayerCharacterCsetting(GetEntityId());
					r_model_valc[1] = true;
				}
				serialization_reinit_customization_setting_on_character = 0.0f;
			}
		}
	}
	//********************************************************************************************************************************************
	//----------------------Request reequip old equipped items on player character after post serialization event*********************************
	if (reequip_timer_cn1 > 0.0f && !r_model_valc[1] && !r_model_valc[0])
	{
		if (gEnv->pTimer->GetFrameTime() > 0.02f)
			reequip_timer_cn1 -= 0.02f;
		else
			reequip_timer_cn1 -= gEnv->pTimer->GetFrameTime();

		if (reequip_timer_cn1 <= 0.0f)
		{
			if (serialization_reinit_customization_setting_on_character == 0.0f)
			{
				RequestToReequipUsedItemsOnPSerialize();
				reequip_timer_cn1 = 0.0f;
				r_model_valc[2] = true;
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					if(IsPlayer())
						nwAction->DelayedUpdatePlShadowChr(0.7f);
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//Equipment system upgrade, request automatically equip new items at next frame after deequipping old items in slots only-------------------------------
	if (imd_equip_at_next)
	{
		if (!r_model_valc[2])
		{
			std::vector<EntityId> m_actor_items_to_imd_equip_llc = m_actor_items_to_imd_equip;
			std::vector<EntityId>::const_iterator it = m_actor_items_to_imd_equip_llc.begin();
			std::vector<EntityId>::const_iterator end = m_actor_items_to_imd_equip_llc.end();
			int count = m_actor_items_to_imd_equip_llc.size();
			for (int i = 0; i < count, it != end; i++, it++)
			{
				EquipItem(*it);
			}
			m_actor_items_to_imd_equip.clear();
			m_actor_items_to_imd_equip.resize(0);
			m_actor_items_to_imd_equip_llc.clear();
			m_actor_items_to_imd_equip_llc.resize(0);
			imd_equip_at_next = false;
			if (g_pGame->GetHUDCommon())
				g_pGame->GetHUDCommon()->UpdateInventory(10.0f);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//complex attachments(lists of variable attachments) fixes-----------------------------------------------------------------------------
	if (m_complex_attachments_timer > 0.0f)
	{
		if (gEnv->pTimer->GetFrameTime() > 0.01f)
			m_complex_attachments_timer -= 0.01f;
		else
			m_complex_attachments_timer -= gEnv->pTimer->GetFrameTime();
	}

	if (m_complex_attachments_timer <= 0.0f && !m_complex_attachments_to_attach.empty())
	{
		if (reequip_timer_cn1 == 0.0f && rload_pl_model_timer_after_ps == 0.0f && !imd_equip_at_next &&
			serialization_reinit_customization_setting_on_character == 0.0f
			&& update_all_skin_attachment_at_next_frame <= 0 && !r_model_valc[2])
		{
			std::vector<SComplexAttachments>::const_iterator it = m_complex_attachments_to_attach.begin();
			std::vector<SComplexAttachments>::const_iterator end = m_complex_attachments_to_attach.end();
			int count = m_complex_attachments_to_attach.size();
			for (int i = 0; i < count, it != end; i++, it++)
			{
				if (!(*it).ca_pth.empty())
				{
					if (i == (count - 1))
					{
						CreateSkinAttachment(0, (*it).ca_nmn, (*it).ca_pth, 2);
						m_complex_attachments_timer = 0.1f;
					}
					else
					{
						CreateSkinAttachment(0, (*it).ca_nmn, (*it).ca_pth, 1);
						m_complex_attachments_timer = 0.1f;
					}
				}
				m_complex_attachments_to_attach.erase(it);
				m_complex_attachments_to_attach.resize(count - 1);
				break;//one per frame
			}
		}
	}
}

void CActor::UpdateAdditionalEquipmentPacksAdding(float frameTime, int mode)
{
	if (mode == 1)
	{
		if (equipping_timer > 0.0f)
		{
			float ft_time_ct = frameTime;
			if (ft_time_ct > 0.05f)
				ft_time_ct = 0.05f;

			equipping_timer -= ft_time_ct;
			if (equipping_timer < 0.1f && equipping_timer > 0.0f)
			{

				if (additional_pack_added && !additional_pack_equipped)
				{
					std::list<EntityId>::const_iterator it = m_items_to_equip_pack1.begin();
					std::list<EntityId>::const_iterator end = m_items_to_equip_pack1.end();
					int count = m_items_to_equip_pack1.size();
					for (int i = 0; i < count, it != end; i++, it++)
					{
						CItem *pItem = GetItem(*it);
						if (pItem)
						{
							if (GetInventory() && (GetInventory()->FindItem(pItem->GetEntityId()) >= 0))
								EquipItem(*it);
						}
					}
					additional_pack_equipped = true;
					m_items_to_equip_pack1.clear();
					m_items_to_equip_pack1.resize(0);
				}

				if (additional_pack2_added && !additional_pack2_equipped)
				{
					std::list<EntityId>::const_iterator it = m_items_to_equip_pack2.begin();
					std::list<EntityId>::const_iterator end = m_items_to_equip_pack2.end();
					int count = m_items_to_equip_pack2.size();
					for (int i = 0; i < count, it != end; i++, it++)
					{
						CItem *pItem = GetItem(*it);
						if (pItem)
						{
							if (GetInventory() && (GetInventory()->FindItem(pItem->GetEntityId()) >= 0))
								EquipItem(*it);
						}
					}
					additional_pack2_equipped = true;
					m_items_to_equip_pack2.clear();
					m_items_to_equip_pack2.resize(0);
				}
				equipping_timer = 0.0f;
			}
		}
	}
	else if (mode == 2)
	{
		if (delayed_add_packs_request)
		{
			delayed_add_packs_request -= frameTime;
			if (delayed_add_packs_request <= 0.0f)
			{
				if (additional_pack_add_request)
				{
					AddAdditionalEquip();
				}

				if (additional_pack2_add_request)
				{
					AddAdditionalEquip2();
				}
				delayed_add_packs_request = 0.0f;
			}
		}
	}
}

void CActor::UpdateSpecialActions(float frameTime)
{
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return;

	if (IsPlayer())
	{
		for (int i = 0; i < 100; i++)
		{
			if (spec_action_timers_out_combo[i] > 0.0f)
			{
				spec_action_timers_out_combo[i] -= gEnv->pTimer->GetFrameTime();
				if (spec_action_timers_out_combo[i] <= 0.0f)
				{
					spec_action_timers_out_combo[i] = 0.0f;

					if (i == nwAction->eSpec_act_combat_dodge_attack_rq)
					{
						nwAction->AttackStartAfterDodge(GetEntityId());
					}
					else if (i == nwAction->eSpec_act_combat_dodge_attack_lft)
					{
						nwAction->AttackStartAfterDodge(GetEntityId(), "_lft");
					}
					else if (i == nwAction->eSpec_act_combat_dodge_attack_rgt)
					{
						nwAction->AttackStartAfterDodge(GetEntityId(), "_rgt");
					}
					else if (i == nwAction->eSpec_act_combat_dodge_attack_bwd)
					{
						nwAction->AttackStartAfterDodge(GetEntityId(), "_bwd");
					}
					else if (i == nwAction->eSpec_act_combat_dodge_attack_fwd)
					{
						nwAction->AttackStartAfterDodge(GetEntityId(), "_fwd");
					}
				}
			}

			if (spec_action_time_to_recharge_combo[i] > 0.0f)
			{
				spec_action_time_to_recharge_combo[i] -= gEnv->pTimer->GetFrameTime();
				if (spec_action_time_to_recharge_combo[i] <= 0.0f)
				{
					spec_action_time_to_recharge_combo[i] = 0.0f;
					spec_action_counter_in_combo[i] = 0;
					spec_action_counter_max_in_combo[i] = 0;
				}
			}
		}
	}

	if (m_special_action_anim_playing_time < 0.0f)
	{
		if (m_special_action_id > nwAction->eSpec_act_none)
		{
			if ((m_special_action_id == nwAction->eSpec_act_combat_block_safe_no_shield) || (m_special_action_id == nwAction->eSpec_act_combat_block_safe_with_shield))
			{
				nwAction->MeleeBlockReplayAnim(GetEntityId(), GetCurrentItemId());
			}

			if (m_special_action_id != nwAction->eSpec_act_combat_recoil)
				m_special_action_id = nwAction->eSpec_act_none;

			if (m_special_action_id == nwAction->eSpec_act_combat_recoil)
			{
				if (m_special_recoil_action[nwAction->eARMode_recoil_dodge] > 0.0f && GetActorStats()->inAir < 0.01f)
					m_special_recoil_action[nwAction->eARMode_recoil_dodge] -= frameTime;

				if (m_special_recoil_action[nwAction->eARMode_recoil_dodge] < 0.0f)
				{
					m_special_action_id = nwAction->eSpec_act_none;
					m_special_recoil_action[nwAction->eARMode_recoil_dodge] = 0.0f;
				}

				if (m_special_recoil_action[nwAction->eARMode_recoil_fly] > 0.0f)
					m_special_recoil_action[nwAction->eARMode_recoil_fly] -= frameTime;

				if (m_special_recoil_action[nwAction->eARMode_recoil_fly] < 0.0f)
				{
					m_special_action_id = nwAction->eSpec_act_none;
					m_special_recoil_action[nwAction->eARMode_recoil_fly] = 0.0f;
				}
			}
		}
		if (m_special_action_id != nwAction->eSpec_act_combat_recoil)
			m_special_action_anim_playing_time = 0.0f;
	}
	else if (m_special_action_anim_playing_time > 0.0f)
	{
		if (m_special_action_anim_playing_time > 0.0f)
		{
			m_special_action_anim_playing_time -= frameTime;
			if (m_special_action_anim_playing_time <= 0.0f)
				m_special_action_anim_playing_time = -0.1f;
		}
		if (m_special_action_id == nwAction->eSpec_act_combat_recoil)
		{
			m_special_recoil_action[nwAction->eARMode_recoil_dodge] = m_special_action_anim_playing_time;
			m_special_action_anim_playing_time = -0.1f;
		}
		else if (m_special_action_id == nwAction->eSpec_act_combat_falling)
		{
			m_special_action_anim_playing_time = -0.1f;
			m_process_falling_timer_c1 = 0.0f;
		}
	}
}

void CActor::UpdateTargeting(float frameTime)
{
	if (attack_timer_smpl > 0.0f)
	{
		attack_timer_smpl -= frameTime;
		if (attack_timer_smpl <= 0.0f)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				Vec3 orig_direct = GetEntity()->GetPos() + (GetEntity()->GetRotation() * Vec3(0, 1, 0)) * 5.0f;
				orig_direct.z = GetEntity()->GetPos().z;
				orig_direct = orig_direct - GetEntity()->GetPos();
				orig_direct.normalize();
				Vec3 aim_direct = Current_target_dir;
				aim_direct.z = orig_direct.z;
				if (!Current_target_dir.IsZero())
				{
					bool can_stop_fire = true;
					float fDot = aim_direct * orig_direct;
					fDot = clamp_tpl(fDot, -1.0f, 1.0f);
					float angle = acos_tpl(fDot);
					if (((orig_direct.x * aim_direct.y) - (orig_direct.y * aim_direct.x)) < 0)
						angle = -angle;

					float limit_horiz = g_pGameCVars->ca_aim_pose_alternative_limit_base_horizontal / 120.0f;

					if (angle > 0 && angle > limit_horiz)
						can_stop_fire = false;
					else if (angle < 0 && angle < -limit_horiz)
						can_stop_fire = false;

					if (can_stop_fire)
						nwAction->StopWeaponAttackSimple(GetEntityId());
					else
						attack_timer_smpl += frameTime*1.5f;
				}
			}
		}
	}
}

void CActor::UpdateNonPlayerActorsFallingEvents(float frameTime)
{
	CGameRules *pGameRules = g_pGame->GetGameRules();
	bool in_falling = GetActorPhysics().velocity.z < -0.5f;
	if (!in_falling && !IsPlayer())
	{
		if (g_pGameCVars->g_ai_actor_fall_damage_enable > 0)
		{
			if (old_veloc_z != 0.0f)
			{
				if (old_veloc_z < g_pGameCVars->g_ai_actor_fall_z_veloc_min_to_add_damage)
				{
					if (pGameRules)
					{
						if (!IsDead())
						{
							float dmg_val = old_veloc_z * (old_veloc_z + old_veloc_z);
							dmg_val *= g_pGameCVars->g_ai_actor_fall_damage_multipler;
							HitInfo info(GetEntityId(), GetEntityId(), 0,
								dmg_val, 0.0f, 0, 0,
								CGameRules::EHitType::Fall, GetEntity()->GetPos(), Vec3(0, 0, -1), Vec3(0, 0, -1));
							pGameRules->ClientHit(info);

							if (old_veloc_z < g_pGameCVars->g_ai_actor_fall_z_veloc_max_to_add_damage)
							{
								dmg_val += 65535;
								pGameRules->ClientHit(info);
							}
						}
					}
					old_veloc_z = 0.0f;
				}
			}
			old_veloc_z = GetActorPhysics().velocity.z;
		}
	}
	else
	{
		if (GetActorPhysics().velocity.z < old_veloc_z)
			old_veloc_z = GetActorPhysics().velocity.z;
	}
}

void CActor::SetBlockActive(int blc_type, bool active)
{
	if (blc_type == 1)
		m_blockactiveshield = active;
	else if (blc_type == 2)
		m_blockactivenoshield = active;
	else if (blc_type == 3)
	{
		m_blockactiveshield = active;
		m_blockactivenoshield = active;
	}

	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
			GetGameObject()->InvokeRMI(CActor::ClSetBlockState(), CActor::AcBlcAction(active, blc_type), eRMI_ToRemoteClients);
		else
		{
			GetGameObject()->InvokeRMI(CActor::SvSetBlockState(), CActor::AcBlcAction(active, blc_type), eRMI_ToServer);
		}
	}
}
