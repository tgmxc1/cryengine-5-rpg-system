#pragma once

#ifndef __BOW_H__
#define __BOW_H__

#include "Weapon.h"
#include "FireModeParams.h"

class CBow : public CWeapon
{
	typedef CWeapon BaseClass;
public:

	CBow();
	~CBow();

	struct ActionRequestParamX
	{
		ActionRequestParamX() : actorId(0), actionName(""), activationMode(0), activationValue(0.0f) {};
		ActionRequestParamX(EntityId actor, string name, int aM, float aV) : actorId(actor), actionName(name), activationMode(aM), activationValue(aV) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("actorId", actorId, 'eid');
			ser.Value("actionNm", actionName);
			ser.Value("activationMode", activationMode, 'ui8');
			ser.Value("activationValue", activationValue, 'vHRd');
		}
		EntityId actorId;
		string actionName;
		int activationMode;
		float activationValue;
	};

	virtual bool Init(IGameObject * pGameObject);
	virtual void Update(SEntityUpdateContext &ctx, int slot);
	virtual void OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value);
	virtual void StartFire();
	virtual void StartFire(const SProjectileLaunchParams& launchParams);
	virtual void StopFire();
	virtual void OnSelected(bool selected);
	virtual void OnEnterFirstPerson();
	virtual void OnEnterThirdPerson();

	DECLARE_SERVER_RMI_NOATTACH(SVOnActionXBow, ActionRequestParamX, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClOnActionXBow, ActionRequestParamX, eNRT_ReliableOrdered);

	virtual void NetXcAction(const ActionRequestParamX &params);

	void ForcedStopFire();
	void OnHitDeathReaction();
	void ReinitFiremode();
	void CreateArrowAttachment();
	void DeleteArrowAttachment();
	void StartHoldAnim();
	void EndHoldAnim();
	void EndHoldAnimByCancel();
	void ProcessReloadArrow();
	void AttachArrowToHand();
	void DetachArrowFromHand();
	void GetOrPutArrowToQuiver(bool get);
	void ChekMainArrowItem();
	void ProcessCancelAction();
	float m_arrow_hold_time;
	float m_stamina_cons_time;
	float m_arrow_load_time;
	float m_arrow_load_time_sequence_1;
	float m_arrow_load_time_sequence_2;
	float m_arrow_shot_recovery_time;
	float m_arrow_hold_time_old;
	float m_hold_time_min_to_shot;
	float m_hold_time_max_to_vel;
	float m_close_combat_damage;
	bool shot;
	bool hold_started;
	bool anim_hold_started;
	bool arrow_loaded;
	bool process_arrow_load_started;
	bool shot_after_reload;
	bool arrow_attached_to_hand;
	bool arrow_attached_to_bow;
private:
	SBowParams pBowParams;
};
#endif