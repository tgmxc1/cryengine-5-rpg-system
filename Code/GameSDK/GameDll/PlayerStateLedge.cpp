// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
-------------------------------------------------------------------------
$Id$
$DateTime$
Description: Implements the Player ledge state

-------------------------------------------------------------------------
History:
- 9.9.10: Created by Stephen M. North

*************************************************************************/
#include <StdAfx.h>
#include "PlayerStateLedge.h"
#include "GameCVars.h"
#include "Player.h"
#include "Weapon.h"
#include "StatsRecordingMgr.h"
#include "PersistantStats.h"
#include "IAnimatedCharacter.h"
#include "PlayerInput.h"
#include "GameActions.h"

#include "PlayerStateUtil.h"

#include "Environment/LedgeManager.h"

#include "PlayerAnimation.h"
#include "Utility/CryWatch.h"
#include "Melee.h"
#include "MeleeTimedEAAS.h"
#include "ActorActionsNew.h"
#include "PlayerCamera.h"

static const float s_PlayerMax2DPhysicsVelocity = 9.f;


class CActionLedgeGrab : public TPlayerAction
{
public:

	DEFINE_ACTION("LedgeGrab");

	CActionLedgeGrab(CPlayerStateLedge * ledgeState, CPlayer &player, const QuatT &ledgeLoc, FragmentID fragmentID, SLedgeTransitionData::EOnLedgeTransition transition, bool endCrouched, bool comingFromOnGround, bool comingFromSprint, bool isnormalledge = false)
		: TPlayerAction(PP_PlayerActionUrgent, fragmentID),
		m_ledgeState(ledgeState),
		m_player(player),
		orig_loc(ledgeLoc),
		m_targetViewDirTime(0.2f),
		m_transitionType(transition),
		m_endCrouched(endCrouched),
		m_comingFromOnGround(comingFromOnGround),
		m_comingFromSprint(comingFromSprint),
		m_isnormalledge(isnormalledge),
		m_haveUnHolsteredWeapon(false),
		m_action_special_timer(0.0f),
		view_setuped(false)
	{
		SetParam("TargetPos", ledgeLoc);
		//PlayerMannequin.fragmentIDs.ledgeGrab
		Ang3 viewAng;
		viewAng.SetAnglesXYZ( ledgeLoc.q );
		viewAng.y = 0.0f;
		m_targetViewDir = Quat::CreateRotationXYZ( viewAng );
	}

	void InitialiseWithParams(const char * directionText)
	{
		const CTagDefinition* pFragTagDef = m_context->controllerDef.GetFragmentTagDef(m_fragmentID);
		if (directionText)
		{
			pFragTagDef->Set(m_fragTags, pFragTagDef->Find(directionText), true);
		}
	}

	void ExitWOAnimDis()
	{
		TPlayerAction::Exit();

		IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();

		if (pAnimChar)
		{
			pAnimChar->SetMovementControlMethods(eMCM_Entity, eMCM_Entity);
			pAnimChar->ForceRefreshPhysicalColliderMode();
			pAnimChar->RequestPhysicalColliderMode(eColliderMode_Undefined, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
		}

		

		m_player.PartialAnimationControlled(false, PlayerCameraAnimationSettings());

		/*SPlayerStats *pPlayerStats = m_player.GetActorStats();
		pPlayerStats->forceSTAP = SPlayerStats::eFS_None;
		pPlayerStats->bDisableTranslationPinning = false;
		m_player.SetCanTurnBody(true);
		g_pGameCVars->g_disable_ik_torso_upd = 0;
		m_player.UpdateVisibility();
		m_player.GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Ledge);*/
	}

	void ExitFull()
	{
		TPlayerAction::Exit();
		m_player.m_torsoAimIK.Enable(true);
		//m_player.UpdateVisibility();
		IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();

		if (pAnimChar)
		{
			pAnimChar->SetMovementControlMethods(eMCM_Entity, eMCM_Entity);
			pAnimChar->ForceRefreshPhysicalColliderMode();
			pAnimChar->RequestPhysicalColliderMode(eColliderMode_Undefined, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
		}

		/*if (!m_haveUnHolsteredWeapon)
		{
			m_player.GetHolsteredItemId() == m_player.GetCurrentItemId();
			m_haveUnHolsteredWeapon = true;
			m_player.HolsterItem_NoNetwork(false, true, 0.5f, false);
		}*/

		if (m_player.GetHolsteredItemId() == m_player.GetCurrentItemId())
		{
			m_player.HolsterItem_NoNetwork(false, true, 0.5f, false);
		}

		//m_player.PartialAnimationControlled(false, PlayerCameraAnimationSettings());

		//SPlayerStats *pPlayerStats = m_player.GetActorStats();
		//pPlayerStats->forceSTAP = SPlayerStats::eFS_None;
		//pPlayerStats->bDisableTranslationPinning = false;
		m_player.StateMachineHandleEventMovement(PLAYER_EVENT_LEDGE_ANIM_FINISHED);
		//m_player.BlendPartialCameraAnim(0.0f, 0.1f);
		//m_player.SetCanTurnBody(true);
		g_pGameCVars->g_disable_ik_torso_upd = 0;
		m_player.GetPlayerInput()->CallAllCancelHandlers();
		CPlayerInput* pPlayerInput = static_cast<CPlayerInput*>(m_player.GetPlayerInput());
		if (pPlayerInput)
		{
			pPlayerInput->Reset();
		}
		//m_player.UpdateVisibility();
		m_player.GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Ledge);

		m_player.SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
	}

	virtual void OnInitialise()
	{
		if (m_isnormalledge)
		{
			const CTagDefinition *fragTagDef = m_context->controllerDef.GetFragmentTagDef(m_fragmentID);
			if (fragTagDef)
			{
				const SMannequinPlayerParams::Fragments::SledgeGrab& ledgeGrabFragment = PlayerMannequin.fragments.ledgeGrab;

				// TODO - remove transition type and change over to just using the ledge's m_ledgeFlagBitfield this 
				// would simplify everything and remove a lot of the dependency on Player_Params.xml

				switch (m_transitionType)
				{
				case SLedgeTransitionData::eOLT_VaultOnto:
				case SLedgeTransitionData::eOLT_HighVaultOnto:
				{
					const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.up;
					fragTagDef->Set(m_fragTags, vaultID, true);
					break;
				}
				case SLedgeTransitionData::eOLT_VaultOver:
				case SLedgeTransitionData::eOLT_VaultOverIntoFall: // this flag is now deprecated
				case SLedgeTransitionData::eOLT_HighVaultOver:
				case SLedgeTransitionData::eOLT_HighVaultOverIntoFall: // this flag probably shouldn't be needed either in that case
				{
					const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.over;
					fragTagDef->Set(m_fragTags, vaultID, true);
					break;
				}
				case SLedgeTransitionData::eOLT_QuickLedgeGrab:
				{
					const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.quick;
					fragTagDef->Set(m_fragTags, vaultID, true);
					break;
				}
				// perhaps set ledge off of midair and falling here. But ledge should be the default in data
				}

				switch (m_transitionType)
				{
				case SLedgeTransitionData::eOLT_HighVaultOverIntoFall:
				case SLedgeTransitionData::eOLT_HighVaultOnto:
				case SLedgeTransitionData::eOLT_HighVaultOver:
				{
					const TagID highID = ledgeGrabFragment.fragmentTagIDs.high;
					fragTagDef->Set(m_fragTags, highID, true);
					//intentionally no break to fall through
				}

				case SLedgeTransitionData::eOLT_VaultOverIntoFall:
				case SLedgeTransitionData::eOLT_VaultOnto:
				case SLedgeTransitionData::eOLT_VaultOver:
				{
					const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.vault;
					fragTagDef->Set(m_fragTags, vaultID, true);
					SPlayerStats *pPlayerStats = m_player.GetActorStats();
					pPlayerStats->forceSTAP = SPlayerStats::eFS_On;	// force STAP on for vaults
					pPlayerStats->bDisableTranslationPinning = true; // disables translation pinning for vaults (stops 3P legs from cutting through camera)

					break;
				}
				}

				if (m_endCrouched)
				{
					const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.endCrouched;
					fragTagDef->Set(m_fragTags, vaultID, true);
				}

				//--- Disabling this for now whilst position adjusted anims with trimmed start-times don't work
				//--- Once this is resolved we'll re-enable.
				//if (m_comingFromOnGround)
				//{
				const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.floor;
				fragTagDef->Set(m_fragTags, vaultID, true);
				//}
				//else
				//{
				//	const TagID vaultID = ledgeGrabFragment.fragmentTagIDs.fall;
				//	fragTagDef->Set(m_fragTags, vaultID, true);
				//}

				//was sprinting
				if (m_comingFromSprint)
				{
					const TagID floorSprintID = ledgeGrabFragment.fragmentTagIDs.floorSprint;
					fragTagDef->Set(m_fragTags, floorSprintID, true);
				}
			}
		}
	}

	virtual void Enter()
	{
		if (!m_isnormalledge)
		{
			TPlayerAction::Enter();
			g_pGameCVars->g_disable_ik_torso_upd = 1;
			m_action_special_timer = g_pGameCVars->g_ledge_movement_normal_time_to_block_event_to_event;
			IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();
			FragmentID fragmentID_nv;
			IActionController *pActionController = pAnimChar->GetActionController();
			if (pActionController)
			{
				SAnimationContext &animContext = pActionController->GetContext();
				fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge");
				if (m_fragmentID == fragmentID_nv)
				{
					m_action_special_timer = g_pGameCVars->g_ledge_movement_normal_time_to_block_after_event;
				}
			}
			//m_player.BlendPartialCameraAnim(0.3f, 0.1f);
			//m_player.SetCanTurnBody(false);

			if (pAnimChar)
			{
				pAnimChar->SetMovementControlMethods(eMCM_Animation, eMCM_Animation);
				pAnimChar->RequestPhysicalColliderMode(eColliderMode_Disabled, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
				//m_player.SetCanTurnBody(false);
			}
			switch (m_transitionType)
			{
			case SLedgeTransitionData::eOLT_VaultOverIntoFall:
			case SLedgeTransitionData::eOLT_VaultOnto:
			case SLedgeTransitionData::eOLT_VaultOver:
			case SLedgeTransitionData::eOLT_HighVaultOverIntoFall:
			case SLedgeTransitionData::eOLT_HighVaultOnto:
			case SLedgeTransitionData::eOLT_HighVaultOver:
				break;
			default:
			{
				PlayerCameraAnimationSettings cameraAnimationSettings;
				cameraAnimationSettings.positionFactor = 1.0f;
				cameraAnimationSettings.rotationFactor = 0.0f;
				m_player.PartialAnimationControlled(true, cameraAnimationSettings);
			}
			break;
			}
			//CryLogAlways("m_ledgeState->SetMostRecentlyEnteredAction");
			m_ledgeState->SetMostRecentlyEnteredAction(this);
			m_player.m_torsoAimIK.Disable(true);
			float verticalUpViewLimit = 45.0f;
			float horizontalViewLimit = 65.0f;
			Vec3 dir_to_test(0, 1, 0);
			dir_to_test = m_player.GetEntity()->GetRotation()*dir_to_test;
			m_player.GetActorParams().viewLimits.SetViewLimit(dir_to_test.normalized(), DEG2RAD(horizontalViewLimit), DEG2RAD(verticalUpViewLimit),
				0.0f, 0.0f, SViewLimitParams::eVLS_Ledge);

			m_player.UpdateVisibility();
			/*float t = m_activeTime / m_targetViewDirTime;
			t = min(t, 1.0f);
			Quat rotNew;
			rotNew.SetSlerp(m_player.GetViewRotation(), m_targetViewDir, t);
			m_player.SetViewRotation( rotNew );*/

			if (m_player.GetHolsteredItemId() != m_player.GetCurrentItemId())
			{
				m_player.HolsterItem_NoNetwork(true, false, 0.5f, false);
			}
		}
		else
		{
			TPlayerAction::Enter();

			IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();

			if (pAnimChar)
			{
				pAnimChar->SetMovementControlMethods(eMCM_Animation, eMCM_Animation);
				pAnimChar->RequestPhysicalColliderMode(eColliderMode_Disabled, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
			}
			switch (m_transitionType)
			{
			case SLedgeTransitionData::eOLT_VaultOverIntoFall:
			case SLedgeTransitionData::eOLT_VaultOnto:
			case SLedgeTransitionData::eOLT_VaultOver:
			case SLedgeTransitionData::eOLT_HighVaultOverIntoFall:
			case SLedgeTransitionData::eOLT_HighVaultOnto:
			case SLedgeTransitionData::eOLT_HighVaultOver:
				break;
			default:
			{
				PlayerCameraAnimationSettings cameraAnimationSettings;
				cameraAnimationSettings.positionFactor = 1.0f;
				cameraAnimationSettings.rotationFactor = g_pGameCVars->pl_ledgeClamber.cameraBlendWeight;
				m_player.PartialAnimationControlled(true, cameraAnimationSettings);
			}
			break;
			}
		}
	}
	
	virtual void Enter2()
	{
		TPlayerAction::Enter();
		g_pGameCVars->g_disable_ik_torso_upd = 1;
		m_action_special_timer = g_pGameCVars->g_ledge_movement_normal_time_to_block_event_to_event;
		IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();
		FragmentID fragmentID_nv;
		IActionController *pActionController = pAnimChar->GetActionController();
		if (pActionController)
		{
			SAnimationContext &animContext = pActionController->GetContext();
			fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge");
			if (m_fragmentID == fragmentID_nv)
			{
				m_action_special_timer = g_pGameCVars->g_ledge_movement_normal_time_to_block_event_to_event;
			}
		}
		if (pAnimChar)
		{
			pAnimChar->SetMovementControlMethods(eMCM_Animation, eMCM_Animation);
			pAnimChar->RequestPhysicalColliderMode(eColliderMode_Disabled, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
		}
		switch (m_transitionType)
		{
		case SLedgeTransitionData::eOLT_VaultOverIntoFall:
		case SLedgeTransitionData::eOLT_VaultOnto:
		case SLedgeTransitionData::eOLT_VaultOver:
		case SLedgeTransitionData::eOLT_HighVaultOverIntoFall:
		case SLedgeTransitionData::eOLT_HighVaultOnto:
		case SLedgeTransitionData::eOLT_HighVaultOver:
			break;
		default:
		{
			PlayerCameraAnimationSettings cameraAnimationSettings;
			cameraAnimationSettings.positionFactor = 1.0f;
			cameraAnimationSettings.rotationFactor = 0.25f;
			m_player.PartialAnimationControlled(true, cameraAnimationSettings);
		}
		break;
		}
		m_player.m_torsoAimIK.Disable(true);
		float verticalUpViewLimit = 45.0f;
		float horizontalViewLimit = 65.0f;
		Vec3 dir_to_test(0, 1, 0);
		dir_to_test = m_player.GetEntity()->GetRotation()*dir_to_test;
		m_player.GetActorParams().viewLimits.SetViewLimit(dir_to_test.normalized(), DEG2RAD(horizontalViewLimit), DEG2RAD(verticalUpViewLimit),
			0.0f, 0.0f, SViewLimitParams::eVLS_Ledge);

		m_player.UpdateVisibility();
		if (m_player.GetHolsteredItemId() != m_player.GetCurrentItemId())
		{
			m_player.SelectItem(m_player.GetNoWeaponId(), false, true);
			m_player.HolsterItem_NoNetwork(true, false, 0.5f, false);
		}
	}

	virtual void Exit()
	{
		if (!m_isnormalledge)
		{
			TPlayerAction::Exit();
			//m_player.UpdateVisibility();
			//m_player.m_torsoAimIK.Enable(true);
			IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();

			if (pAnimChar)
			{
				pAnimChar->SetMovementControlMethods(eMCM_Entity, eMCM_Entity);
				pAnimChar->ForceRefreshPhysicalColliderMode();
				pAnimChar->RequestPhysicalColliderMode(eColliderMode_Undefined, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
			}

			if (m_player.GetHolsteredItemId() == m_player.GetCurrentItemId())
			{
				m_player.HolsterItem_NoNetwork(false, true, 0.5f, false);
			}

			m_player.PartialAnimationControlled(false, PlayerCameraAnimationSettings());


			//SPlayerStats *pPlayerStats = m_player.GetActorStats();
			//pPlayerStats->forceSTAP = SPlayerStats::eFS_None;
			//pPlayerStats->bDisableTranslationPinning=false;
			m_player.GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Ledge);
			//m_player.BlendPartialCameraAnim(0.0f, 0.1f);
			//m_player.SetCanTurnBody(true);
			g_pGameCVars->g_disable_ik_torso_upd = 0;
			m_player.GetPlayerInput()->CallAllCancelHandlers();
			CPlayerInput* pPlayerInput = static_cast<CPlayerInput*>(m_player.GetPlayerInput());
			if (pPlayerInput)
			{
				pPlayerInput->Reset();
			}
			m_player.SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
		}
		else
		{
			TPlayerAction::Exit();
			IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter();
			if (pAnimChar)
			{
				pAnimChar->SetMovementControlMethods(eMCM_Entity, eMCM_Entity);
				pAnimChar->ForceRefreshPhysicalColliderMode();
				pAnimChar->RequestPhysicalColliderMode(eColliderMode_Undefined, eColliderModeLayer_Game, "CActionLedgeGrab::Enter()");
			}

			if (!m_haveUnHolsteredWeapon)
			{
				m_haveUnHolsteredWeapon = true;
				m_player.HolsterItem_NoNetwork(false, true, 0.5f);
			}
			m_player.PartialAnimationControlled(false, PlayerCameraAnimationSettings());
			SPlayerStats *pPlayerStats = m_player.GetActorStats();
			pPlayerStats->forceSTAP = SPlayerStats::eFS_None;
			pPlayerStats->bDisableTranslationPinning = false;
			m_player.StateMachineHandleEventMovement(PLAYER_EVENT_LEDGE_ANIM_FINISHED);
			m_player.SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
		}
	}

	virtual EStatus Update(float timePassed)
	{
		TPlayerAction::Update(timePassed);
		if (m_action_special_timer > 0.0f)
			m_action_special_timer -= timePassed;

		if (m_action_special_timer > 0.5f && m_action_special_timer < 0.6f && !view_setuped)
		{
			DelayedSetViewLimits();
		}

		if (m_isnormalledge)
		{
			//--- Update view direction
			float t = m_activeTime / m_targetViewDirTime;
			t = min(t, 1.0f);
			Quat rotNew;
			rotNew.SetSlerp(m_player.GetViewRotation(), m_targetViewDir, t);
			m_player.SetViewRotation(rotNew);

			if (!m_player.IsThirdPerson())
			{
				if (m_activeTime > 0.5f && !m_haveUnHolsteredWeapon)
				{
					m_haveUnHolsteredWeapon = true;
					m_player.HolsterItem_NoNetwork(false, true, 0.5f);
				}
			}
		}

		return m_eStatus;
	}

	virtual bool SpecialTimerEnded()
	{
		return m_action_special_timer <= 0.0f;
	}

	virtual void DelayedSetViewLimits()
	{
		if (!TPlayerAction::IsStarted())
			return;

		float verticalUpViewLimit = 45.0f;
		float horizontalViewLimit = 65.0f;
		Vec3 dir_to_test(0, 1, 0);
		dir_to_test = m_player.GetEntity()->GetRotation()*dir_to_test;
		m_player.GetActorParams().viewLimits.SetViewLimit(dir_to_test.normalized(), DEG2RAD(horizontalViewLimit), DEG2RAD(verticalUpViewLimit),
			0.0f, 0.0f, SViewLimitParams::eVLS_Ledge);

		view_setuped = true;
		if (m_ledgeState)
		{
			orig_loc = m_ledgeState->CalculateLedgeOffsetLocation3(m_player.GetEntity()->GetWorldTM(), Vec3(ZERO));
			SetParam("TargetPos", orig_loc);
		}
	}

private:

	CPlayer& m_player;
	Quat     m_targetViewDir;
	float		 m_targetViewDirTime;
	CPlayerStateLedge * m_ledgeState;
	SLedgeTransitionData::EOnLedgeTransition m_transitionType;
	bool		 m_endCrouched;
	bool		 m_comingFromOnGround;
	bool		 m_comingFromSprint;
	bool		 m_haveUnHolsteredWeapon;
	bool		 m_isnormalledge;
	float		 m_action_special_timer;
	bool view_setuped;
public:
	QuatT orig_loc;
};

class CActionLedgeRight : public CActionLedgeGrab
{
public:
	DEFINE_ACTION("LedgeRight");

	CActionLedgeRight(CPlayerStateLedge * ledgeState, CPlayer &player, QuatT &ledgeLoc, FragmentID fragmentID, SLedgeTransitionData::EOnLedgeTransition animType, bool endCrouched, bool comingFromOnGround, bool comingFromSprint) :
		CActionLedgeGrab(ledgeState, player, ledgeLoc, fragmentID, animType, endCrouched, comingFromOnGround, comingFromSprint)
	{
	}

	virtual void OnInitialise()
	{
		/*switch (m_animType)
		{
		case CPlayerStateLadder::kLadderAnimType_atBottom:           { InitialiseWithParams("up", NULL);				return; }
		case CPlayerStateLadder::kLadderAnimType_atTopLeftFoot:      { InitialiseWithParams("down", "left");		return; }
		case CPlayerStateLadder::kLadderAnimType_atTopRightFoot:     { InitialiseWithParams("down", "right");	return; }
		}*/
		//CActionLedgeGrab::InitialiseWithParams("right");
		CActionLedgeGrab::OnInitialise();
	}

	virtual void Exit()
	{
		/*if (IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter())
		{
			Quat animQuat = pAnimChar->GetAnimLocation().q;

			m_player.SetViewRotation(animQuat);
		}*/
		CActionLedgeGrab::ExitWOAnimDis();
	}
};

class CActionLedgeUpClumbingToPlace : public CActionLedgeGrab
{
public:
	DEFINE_ACTION("LedgeUpClumbingToPlace");

	CActionLedgeUpClumbingToPlace(CPlayerStateLedge * ledgeState, CPlayer &player, QuatT &ledgeLoc, FragmentID fragmentID, SLedgeTransitionData::EOnLedgeTransition animType, bool endCrouched, bool comingFromOnGround, bool comingFromSprint) :
		CActionLedgeGrab(ledgeState, player, ledgeLoc, fragmentID, animType, endCrouched, comingFromOnGround, comingFromSprint)
	{
	}

	virtual void OnInitialise()
	{
		CActionLedgeGrab::OnInitialise();
	}

	virtual void Exit()
	{
		CActionLedgeGrab::ExitFull();
	}
};

class CActionExitFromCurrentLedge : public CActionLedgeGrab
{
public:
	DEFINE_ACTION("ExitFromCurrentLedge");

	CActionExitFromCurrentLedge(CPlayerStateLedge * ledgeState, CPlayer &player, QuatT &ledgeLoc, FragmentID fragmentID, SLedgeTransitionData::EOnLedgeTransition animType, bool endCrouched, bool comingFromOnGround, bool comingFromSprint) :
		CActionLedgeGrab(ledgeState, player, ledgeLoc, fragmentID, animType, endCrouched, comingFromOnGround, comingFromSprint)
	{
	}

	virtual void OnInitialise()
	{
		CActionLedgeGrab::OnInitialise();
	}

	virtual void Enter()
	{
		CActionLedgeGrab::Enter2();
	}

	virtual void Exit()
	{
		CActionLedgeGrab::ExitFull();
	}
};

class CActionLedgeLeft : public CActionLedgeGrab
{
public:
	DEFINE_ACTION("LedgeLeft");

	CActionLedgeLeft(CPlayerStateLedge * ledgeState, CPlayer &player, QuatT &ledgeLoc, FragmentID fragmentID, SLedgeTransitionData::EOnLedgeTransition animType, bool endCrouched, bool comingFromOnGround, bool comingFromSprint) :
		CActionLedgeGrab(ledgeState, player, ledgeLoc, fragmentID, animType, endCrouched, comingFromOnGround, comingFromSprint)
	{
	}

	virtual void OnInitialise()
	{
		/*switch (m_animType)
		{
		case CPlayerStateLadder::kLadderAnimType_atBottom:           { InitialiseWithParams("down", NULL);		return; }
		case CPlayerStateLadder::kLadderAnimType_atTopLeftFoot:      { InitialiseWithParams("up", "left");		return; }
		case CPlayerStateLadder::kLadderAnimType_atTopRightFoot:     { InitialiseWithParams("up", "right");	return; }
		}*/
		//CActionLedgeGrab::InitialiseWithParams("left");
		CActionLedgeGrab::OnInitialise();
	}

	virtual void Exit()
	{
		/*if (IAnimatedCharacter* pAnimChar = m_player.GetAnimatedCharacter())
		{
		Quat animQuat = pAnimChar->GetAnimLocation().q;

		m_player.SetViewRotation(animQuat);
		}*/
		CActionLedgeGrab::ExitWOAnimDis();
	}
};

CPlayerStateLedge::SLedgeGrabbingParams CPlayerStateLedge::s_ledgeGrabbingParams;

CPlayerStateLedge::CPlayerStateLedge()
	: m_onLedge(false)
	, m_ledgeSpeedMultiplier(1.0f)
	, m_lastTimeOnLedge(0.0f)
	, m_exitVelocity(ZERO)
	, m_ledgePreviousPosition(ZERO)
	, m_ledgePreviousPositionDiff(ZERO)
	, m_ledgePreviousRotation(ZERO)
	,m_ledgePreviousPosition2(ZERO)
	, m_enterFromGround(false)
	, m_enterFromSprint(false)
	,m_mostRecentlyEnteredAction(NULL)
	,dist_from_ledge_start_point(0.0f)
{
}

CPlayerStateLedge::~CPlayerStateLedge()
{
}

bool CPlayerStateLedge::CanGrabOntoLedge( const CPlayer& player )
{
	if (player.IsClient())
	{
		const float kAirTime[3] = { 0.5f, 0.0f, 0.0f };
		const float inAirTime = player.CPlayer::GetActorStats()->inAir;
		const bool canGrabFromAir   = (inAirTime >= kAirTime[player.IsJumping()]);
		const SPlayerStats& playerStats = *player.GetActorStats();

		IItem *pCurrentItem = player.GetCurrentItem();
		if (pCurrentItem)
		{
			IWeapon *pIWeapon = pCurrentItem->GetIWeapon();
			if (pIWeapon)
			{
				CWeapon *pWeapon = static_cast<CWeapon*>(pIWeapon);
				CMelee *pMelee = pWeapon->GetMelee();
				if (pMelee)
				{
					if (pMelee->IsAttacking())
					{
						return false;		// can't ledge grab when we're currently melee-ing
					}
				}
			}
		}
		//if (!(canGrabFromAir && (playerStats.pickAndThrowEntity == 0) && (player.IsMovementRestricted() == false)))
			//CryLogAlways("Can not GrabOntoLedge");

		return canGrabFromAir && (playerStats.pickAndThrowEntity == 0) && (player.IsMovementRestricted() == false);
	}

	return false;
}

void CPlayerStateLedge::OnEnter(CPlayer& player, const SStateEventLedge& ledgeEvent, const char *optional_action)
{
	
	const SLedgeTransitionData::EOnLedgeTransition ledgeTransition = ledgeEvent.GetLedgeTransition();
	const LedgeId nearestGrabbableLedgeId(ledgeEvent.GetLedgeId());
	const bool isClient = player.IsClient();

	const bool from_ground = player.GetActorPhysics().velocity.z >= 0.0f;
	// Record 'LedgeGrab' telemetry and game stats.
	if(ledgeTransition != SLedgeTransitionData::eOLT_None)
	{
		CStatsRecordingMgr::TryTrackEvent(&player, eGSE_LedgeGrab);
		if(isClient)
		{
			g_pGame->GetPersistantStats()->IncrementClientStats(EIPS_LedgeGrabs);
		}
	}

	m_onLedge = true;
	m_postSerializeLedgeTransition = ledgeTransition;

	if( IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter() )
	{
		//////////////////////////////////////////////////////////////////////////
		// Current weapon and input setup
		IItem* pCurrentItem = player.GetCurrentItem();
		CWeapon* pCurrentWeapon =  (pCurrentItem != NULL) ? static_cast<CWeapon*>(pCurrentItem->GetIWeapon()) : NULL;

		if(pCurrentWeapon != NULL)
		{
			pCurrentWeapon->FumbleGrenade();
			pCurrentWeapon->CancelCharge();
		}

		m_ledgeId = nearestGrabbableLedgeId;
		const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById(nearestGrabbableLedgeId);
		CRY_ASSERT( ledgeInfo.IsValid() );

		const bool useVault = ledgeInfo.AreAnyFlagsSet( kLedgeFlag_useVault|kLedgeFlag_useHighVault );
		const bool is_climbable_lg = ledgeInfo.AreAnyFlagsSet(kledgeFlag_climbableLedge);
		if (!useVault)
		{
			player.SelectItem(player.GetNoWeaponId(), false, true);
			player.HolsterItem_NoNetwork(true);

			IPlayerInput* pPlayerInput = player.GetPlayerInput();
			if ((pPlayerInput != NULL) && (pPlayerInput->GetType() == IPlayerInput::PLAYER_INPUT))
			{
				static_cast<CPlayerInput*>(pPlayerInput)->ForceStopSprinting();
			}

			if (isClient)
			{
				g_pGameActions->FilterLedgeGrab()->Enable(true);
			}

			if(ledgeInfo.AreFlagsSet( kLedgeFlag_endCrouched ))
			{
				player.OnSetStance( STANCE_CROUCH );
			}
		}
		else
		{
			if (pCurrentWeapon != NULL)
			{
				pCurrentWeapon->ClearDelayedFireTimeout();
			}

			if (isClient)
			{
				g_pGameActions->FilterVault()->Enable(true);
			}
		}

		//////////////////////////////////////////////////////////////////////////
		/// Figure out speed based on factors like ledge type
		m_ledgeSpeedMultiplier = GetLedgeGrabSpeed( player );

		SLedgeTransitionData::EOnLedgeTransition transition = ledgeTransition;
		if ((transition > SLedgeTransitionData::eOLT_None) && (transition < SLedgeTransitionData::eOLT_MaxTransitions))
		{
			const SLedgeBlendingParams& ledgeGrabParams = GetLedgeGrabbingParams().m_grabTransitionsParams[transition];

#if 0
			if(transition == SLedgeTransitionData::eOLT_VaultOver || transition == SLedgeTransitionData::eOLT_VaultOnto)
			{
				const float vel2D = player.GetActorPhysics().velocity.GetLength2D();
				m_exitVelocity.Set(0.f, vel2D, ledgeGrabParams.m_vExitVelocity.z); 

				const float animSpeedMult = (float)__fsel( -vel2D, g_pGameCVars->g_vaultMinAnimationSpeed, max(g_pGameCVars->g_vaultMinAnimationSpeed, vel2D / s_PlayerMax2DPhysicsVelocity));

				m_ledgeSpeedMultiplier *= animSpeedMult;
			}
			else
#endif
			{
				m_exitVelocity = ledgeGrabParams.m_vExitVelocity;
			}

			StartLedgeBlending(player,ledgeGrabParams);
		}

		
		m_ledgePreviousPosition = ledgeInfo.GetPosition();
		m_ledgePreviousRotation.SetRotationVDir( ledgeInfo.GetFacingDirection() );

		//////////////////////////////////////////////////////////////////////////
		/// Notify script side of ledge entity
		IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity( ledgeInfo.GetEntityId() );
		if (pLedgeEntity != NULL )
		{
			EntityScripts::CallScriptFunction( pLedgeEntity, pLedgeEntity->GetScriptTable(), "StartUse", ScriptHandle(player.GetEntity()) );
		}

		//////////////////////////////////////////////////////////////////////////
		/// Queue Mannequin action
		const bool endCrouched = ledgeInfo.AreFlagsSet( kLedgeFlag_endCrouched );
		const bool comingFromOnGround = ledgeEvent.GetComingFromOnGround();
		const bool comingFromSprint = ledgeEvent.GetComingFromSprint();

		m_enterFromGround = comingFromOnGround;
		m_enterFromSprint = comingFromSprint;
		//CryLogAlways("new CActionLedgeGrab");
		FragmentID fragmentID_nv;
		IActionController *pActionController = pAnimChar->GetActionController();
		if (pActionController)
		{
			SAnimationContext &animContext = pActionController->GetContext();
			string opt_act = optional_action;
			if (opt_act.empty())
				fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge");
			else
				fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find(optional_action);

			if (opt_act.empty() && !from_ground)
			{
				fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge_air_gr");
			}
		}

		if (!is_climbable_lg)
		{
			fragmentID_nv = PlayerMannequin.fragmentIDs.ledgeGrab;
		}
		dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
		CActionLedgeGrab *pLedgeGrabAction = new CActionLedgeGrab(this, player, m_ledgeBlending.m_qtTargetLocation, fragmentID_nv, transition, endCrouched, comingFromOnGround, comingFromSprint, !is_climbable_lg);
		pLedgeGrabAction->SetSpeedBias(m_ledgeSpeedMultiplier);
		player.GetAnimatedCharacter()->GetActionController()->Queue(*pLedgeGrabAction);

		if (!player.IsRemote())
		{
			player.HasClimbedLedge( nearestGrabbableLedgeId, comingFromOnGround, comingFromSprint );
		}
	}
}



void CPlayerStateLedge::OnPrePhysicsUpdate( CPlayer& player, const SActorFrameMovementParams& movement, float frameTime )
{
	const bool gameIsPaused = (frameTime == 0.0f);

	IF_UNLIKELY ( gameIsPaused )
	{
		return;
	}

	player.GetMoveRequest().type = eCMT_Fly;

	Vec3 posDiff(ZERO);
	const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById( m_ledgeId );
	if ( ledgeInfo.IsValid() )
	{
		/*int nWhyFlags = ENTITY_XFORM_POS | ENTITY_XFORM_ROT;
		IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity(ledgeInfo.GetEntityId());
		if (pLedgeEntity)
			pLedgeEntity->InvalidateTM(nWhyFlags);*/
		//const Vec3 ledgePos = ledgeInfo.GetPosition();
		Vec3 dir_xc = ledgeInfo.GetPositionPoint2() - ledgeInfo.GetPositionPoint1();
		dir_xc.normalize();
		const Vec3 ledgePos = ledgeInfo.GetPositionPoint1() + (dir_xc*dist_from_ledge_start_point);
		const Quat ledgeRot = Quat::CreateRotationVDir( ledgeInfo.GetFacingDirection() );
		
		if ((ledgePos != m_ledgePreviousPosition) || (ledgeRot != m_ledgePreviousRotation))
		{
			//player.GetAnimatedCharacter()->GetAnimLocation().t;
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(player.GetAnimatedCharacter()->GetAnimLocation().t, 0.20f, ColorB(200, 35, 135, 255));
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(ledgePos, 0.10f, ColorB(100, 180, 25, 255));
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(ledgeInfo.GetPositionPoint2(), 0.10f, ColorB(145, 45, 25, 255));
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(ledgeInfo.GetPositionPoint1(), 0.10f, ColorB(15, 45, 78, 255));
			float verticalUpViewLimit = 45.0f;
			float horizontalViewLimit = 65.0f;
			Vec3 dir_to_test(0, 1, 0);
			dir_to_test = player.GetEntity()->GetRotation()*dir_to_test;
			player.GetActorParams().viewLimits.SetViewLimit(dir_to_test.normalized(), DEG2RAD(horizontalViewLimit), DEG2RAD(verticalUpViewLimit),
				0.0f, 0.0f, SViewLimitParams::eVLS_Ledge);

			posDiff = ledgePos - m_ledgePreviousPosition;

			const Quat rotToUse = ledgeRot * Quat::CreateRotationZ(DEG2RAD(180.f));
			player.GetAnimatedCharacter()->ForceMovement(QuatT(posDiff, Quat(IDENTITY)));
			Matrix34 tmx = player.GetEntity()->GetWorldTM();
			Vec3 x_diff = player.GetAnimatedCharacter()->GetAnimLocation().t - ledgePos;
			tmx.SetTranslation(ledgePos - Vec3(0,0,1.8f));
			player.GetEntity()->SetWorldTM(tmx);
			player.GetAnimatedCharacter()->ForceOverrideRotation(rotToUse);

			m_ledgePreviousPosition = ledgePos;
			m_ledgePreviousRotation = ledgeRot;
		}

		m_ledgePreviousPositionDiff = posDiff;

		//m_ledgePreviousPosition2 = ledgePos2;

#ifdef STATE_DEBUG
		if (g_pGameCVars->pl_ledgeClamber.debugDraw)
		{
			const float XPOS = 200.0f;
			const float YPOS = 80.0f;
			const float FONT_SIZE = 2.0f;
			const float FONT_COLOUR[4] = {1,1,1,1};
			Vec3 pos = player.GetEntity()->GetPos();
			gEnv->pRenderer->Draw2dLabel(XPOS, YPOS+20.0f, FONT_SIZE, FONT_COLOUR, false, "Ledge Grab: Cur: (%f %f %f) Tgt: (%f %f %f) T:%f", pos.x, pos.y, pos.z, m_ledgeBlending.m_qtTargetLocation.t.x, m_ledgeBlending.m_qtTargetLocation.t.y, m_ledgeBlending.m_qtTargetLocation.t.z, frameTime);
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(m_ledgeBlending.m_qtTargetLocation.t, 0.15f, ColorB(0,0,255,255));
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawSphere(pos, 0.1f, ColorB(255,0,0,255));

		}
#endif //_RELEASE

#if 1
		float desiredSpeed=0.f;
		IMovementController *pMovementController = player.GetMovementController();
		if (pMovementController)
		{
			SMovementState curMovementState;
			pMovementController->GetMovementState(curMovementState);

			desiredSpeed = curMovementState.desiredSpeed;
		}

		//CryWatch("ledge: desiredSpeed=%f", desiredSpeed);

		// if we are falling or we are pushing to move on our inputs
		// pass through our moveRequest to keep the motion and mann tags consistent
		if ( (ledgeInfo.AreAnyFlagsSet(kLedgeFlag_useVault|kLedgeFlag_useHighVault) && ledgeInfo.AreFlagsSet(kLedgeFlag_endFalling)) 
			|| desiredSpeed > 0.1f )
		{
			Vec3& moveRequestVelocity = player.GetMoveRequest().velocity;
			moveRequestVelocity = m_ledgeBlending.m_qtTargetLocation.q * m_exitVelocity;

			//if (player.GetAnimatedCharacter()->GetMCMH() != eMCM_Animation)
			//{
			//	CryLog("CPlayerStateLedge::OnPrePhysicsUpdate() setting moveRequestVelocity when not animcontrolled. MCMH=%d; MCMV=%d", player.GetAnimatedCharacter()->GetMCMH(), player.GetAnimatedCharacter()->GetMCMV());
			//}

			if( !posDiff.IsZeroFast() && (g_pGameCVars->g_ledgeGrabMovingledgeExitVelocityMult + frameTime > 0.f) )
			{
				moveRequestVelocity += posDiff*__fres(frameTime)*g_pGameCVars->g_ledgeGrabMovingledgeExitVelocityMult;
			}
		}
#endif
	}
}

void CPlayerStateLedge::OnAnimFinished( CPlayer &player )
{
#if 1
	const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById( m_ledgeId );
	if ( ledgeInfo.IsValid() )
	{
		if (ledgeInfo.AreAnyFlagsSet(kLedgeFlag_useVault|kLedgeFlag_useHighVault) && ledgeInfo.AreFlagsSet(kLedgeFlag_endFalling))
		{
			player.StateMachineHandleEventMovement( PLAYER_EVENT_FALL );
		}
		else
		{
			player.StateMachineHandleEventMovement( PLAYER_EVENT_GROUND );
		}
	}
#endif

	Vec3& moveRequestVelocity = player.GetMoveRequest().velocity;
	player.GetMoveRequest().jumping=true;
	moveRequestVelocity = m_ledgeBlending.m_qtTargetLocation.q * m_exitVelocity;

	float frameTime = gEnv->pTimer->GetFrameTime();
	if( !m_ledgePreviousPositionDiff.IsZeroFast() && (g_pGameCVars->g_ledgeGrabMovingledgeExitVelocityMult * frameTime > 0.f) )
	{
		moveRequestVelocity += m_ledgePreviousPositionDiff *__fres(frameTime)*g_pGameCVars->g_ledgeGrabMovingledgeExitVelocityMult;
	}
	//player.UpdateVisibility();
	//player.SetCanTurnBody(true);
	g_pGameCVars->g_disable_ik_torso_upd = 0;
	player.GetPlayerInput()->CallAllCancelHandlers();
	CPlayerInput* pPlayerInput = static_cast<CPlayerInput*>(player.GetPlayerInput());
	if (pPlayerInput)
	{
		pPlayerInput->Reset();
	}
	//m_player.UpdateVisibility();
	player.GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Ledge);
	player.RefreshVisibilityState2();
	player.m_fTimerToReturnToDefaultCamera = 0.2f;
	//player.PartialAnimationControlled(false, PlayerCameraAnimationSettings());
	//player.DelayedUpdateVisibility();
}

void CPlayerStateLedge::OnExit( CPlayer& player )
{
	IAnimatedCharacter* pAnimatedCharacter = player.GetAnimatedCharacter();

	if( (pAnimatedCharacter != NULL) && (m_ledgeId.IsValid()) )
	{
		const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById( m_ledgeId );
		CRY_ASSERT( ledgeInfo.IsValid() );

		const bool useVault = ledgeInfo.AreAnyFlagsSet( kLedgeFlag_useVault|kLedgeFlag_useHighVault );
		const bool isClient = player.IsClient();
		if (isClient)
		{
			if (useVault)
			{
				g_pGameActions->FilterVault()->Enable(false);
			}
			else
			{
				g_pGameActions->FilterLedgeGrab()->Enable(false);
			}
		}

		player.m_params.viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Ledge);

		// Added to remove the curious lean the player has after performing a ledge grab.
		Quat viewRotation = player.GetViewRotation();
		Ang3 viewAngles;
		viewAngles.SetAnglesXYZ( viewRotation );
		viewAngles.y = 0.0f;
		viewRotation = Quat::CreateRotationXYZ( viewAngles );
		//player.SetViewRotation( viewRotation );

		pAnimatedCharacter->ForceOverrideRotation( player.GetEntity()->GetWorldRotation() );

		if(ledgeInfo.AreFlagsSet( kLedgeFlag_endCrouched ))
		{
			if(isClient)
			{
				IPlayerInput* pInput = player.GetPlayerInput();
				if ((pInput != NULL) && (pInput->GetType() == IPlayerInput::PLAYER_INPUT))
				{
					static_cast<CPlayerInput*>(pInput)->AddCrouchAction();
				}

				if(player.IsThirdPerson() == false)
				{
					player.m_torsoAimIK.Enable( true ); //Force snap for smooth transition
				}
			}
		}
	}

	m_ledgeId = LedgeId();
	//player.UpdateVisibility();
}

/*LedgeId CPlayerStateLedge::GetCurrentLedgeId()
{
	return m_ledgeId;
}*/

void CPlayerStateLedge::SetMostRecentlyEnteredAction(CActionLedgeGrab * thisAction)
{
	if (thisAction)
	{
		thisAction->AddRef();
	}

	if (m_mostRecentlyEnteredAction)
	{
		m_mostRecentlyEnteredAction->Release();
	}

	m_mostRecentlyEnteredAction = thisAction;
}

void CPlayerStateLedge::InterruptCurrentAnimation(CPlayer& player, bool cancel_main_anim)
{
	if (m_mostRecentlyEnteredAction && !m_mostRecentlyEnteredAction->SpecialTimerEnded())
		return;

	QuatT orig_loc_qt(IDENTITY);
	if (m_mostRecentlyEnteredAction)
	{
		orig_loc_qt = m_mostRecentlyEnteredAction->orig_loc;
		m_mostRecentlyEnteredAction->ForceFinish();
	}
	SetMostRecentlyEnteredAction(NULL);

	if (cancel_main_anim)
	{
		IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter();
		if (!pAnimChar)
		{
			OnAnimFinished(player);
			return;
		}

		if (!orig_loc_qt.IsIdentity())
		{
			FragmentID fragmentID_nv;
			IActionController *pActionController = pAnimChar->GetActionController();
			if (pActionController)
			{
				SAnimationContext &animContext = pActionController->GetContext();
				fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("ledge_exit_now");
			}
			QueueLedgeAction(player, new CActionExitFromCurrentLedge(this, player, orig_loc_qt, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
		}
		else
			OnAnimFinished(player);
	}
}

void CPlayerStateLedge::RequestStartLeftMoveAnim(CPlayer& player)
{
	IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter();
	if (!pAnimChar)
		return;

	Vec3 pos_rt = player.GetBonePosition("Bip01 Neck");

	SLedgeTransitionData ledge(LedgeId::invalid_id);
	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());
	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());
	float dist_to_corner = 0.60f;
	if (m_mostRecentlyEnteredAction)
	{
		if (!m_mostRecentlyEnteredAction->SpecialTimerEnded())
			return;

		FragmentID fragmentID_nv;
		IActionController *pActionController = pAnimChar->GetActionController();
		if (pActionController)
		{
			SAnimationContext &animContext = pActionController->GetContext();
			fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge_left");
			//CryLogAlways("CPlayerStateLedge::RequestStartLeftMoveAnim");
		}
		Matrix34 mtxWorldTm = player.GetEntity()->GetWorldTM();
		/*Vec3 pos_rt = player.GetBonePosition("Bip01 Neck");
		if (pos_rt.IsZero())
		{
			pos_rt = player.GetEntity()->GetPos();
		}
		pos_rt.z += 0.2f;
		mtxWorldTm.SetTranslation(pos_rt);*/
		QuatT orig_loc_qt = CalculateLedgeOffsetLocation(mtxWorldTm, Vec3(ZERO), false);
		float dist = orig_loc_qt.t.GetDistance(ledgeInfo.GetPositionPoint1());
		float dist2 = orig_loc_qt.t.GetDistance(ledgeInfo.GetPositionPoint2());
		if (dist2 < dist_to_corner && !ledgeInfo.IsFlipped())
		{
			Vec3 dir_x(-1, 0, 0);
			dir_x = player.GetEntity()->GetRotation() * dir_x;
			Vec3 pos_x = m_mostRecentlyEnteredAction->orig_loc.t;
			IMovementController * pMC = player.GetMovementController();
			SMovementState info;
			if (pMC)
				pMC->GetMovementState(info);

			if (pos_rt.IsZero())
			{
				pos_rt = info.eyePosition;
			}

			pos_x = (pos_rt + pos_x) * 0.5f;
			pos_x.z = m_mostRecentlyEnteredAction->orig_loc.t.z;
			dir_x.z = 0.0f;
			Vec3 vPosition = pos_x + (dir_x);
			pos_x.z = pos_x.z + 0.2f;
			mtxWorldTm.SetTranslation(pos_x);
			UpdateNearestGrabbableLedge3(player, vPosition, &ledge, true, -0.3f);
			if (m_ledgeId == ledge.m_nearestGrabbableLedgeId)
				return;

			const SLedgeTransitionData::EOnLedgeTransition ledgeTransition = SLedgeTransitionData::eOLT_Falling;
			if (ledgeTransition != SLedgeTransitionData::eOLT_None)
			{
				Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
				const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
				const SLedgeInfo& ledgeInfo2 = pLedgeManager->GetLedgeById(LedgeId(ledge.m_nearestGrabbableLedgeId));
				CRY_ASSERT(ledgeInfo2.IsValid());
				const LedgeId ledgeId = LedgeId(ledge.m_nearestGrabbableLedgeId);
				if (ledgeId.IsValid())
				{
					const LedgeId ledgeId_old = m_ledgeId;
					mtxWorldTm = player.GetEntity()->GetWorldTM();
					m_ledgeSpeedMultiplier = GetLedgeGrabSpeed(player);
					SLedgeTransitionData::EOnLedgeTransition transition = ledgeTransition;
					if ((transition > SLedgeTransitionData::eOLT_None) && (transition < SLedgeTransitionData::eOLT_MaxTransitions))
					{
						const SLedgeBlendingParams& ledgeGrabParams = GetLedgeGrabbingParams().m_grabTransitionsParams[transition];
						if (transition == SLedgeTransitionData::eOLT_VaultOver || transition == SLedgeTransitionData::eOLT_VaultOnto)
						{
							const float vel2D = player.GetActorPhysics().velocity.GetLength2D();
							m_exitVelocity.Set(0.f, vel2D, ledgeGrabParams.m_vExitVelocity.z);

							const float animSpeedMult = (float)__fsel(-vel2D, g_pGameCVars->g_vaultMinAnimationSpeed, max(g_pGameCVars->g_vaultMinAnimationSpeed, vel2D / s_PlayerMax2DPhysicsVelocity));

							m_ledgeSpeedMultiplier *= animSpeedMult;
						}
						else
						{
							m_exitVelocity = ledgeGrabParams.m_vExitVelocity;
						}

						StartLedgeBlending(player, ledgeGrabParams);
						float dist_cn = m_ledgeBlending.m_qtTargetLocation.t.GetDistance(m_mostRecentlyEnteredAction->orig_loc.t);
						if (dist_cn > 0.7f)
							return;
					}
					m_ledgePreviousPosition = ledgeInfo2.GetPosition();
					m_ledgePreviousRotation.SetRotationVDir(ledgeInfo2.GetFacingDirection());
					IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity(ledgeInfo2.GetEntityId());
					if (pLedgeEntity != NULL)
					{
						EntityScripts::CallScriptFunction(pLedgeEntity, pLedgeEntity->GetScriptTable(), "StartUse", ScriptHandle(player.GetEntity()));
					}
					m_ledgeId = ledgeId;
					InterruptCurrentAnimation(player);
					dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
					QueueLedgeAction(player, new CActionLedgeRight(this, player, m_ledgeBlending.m_qtTargetLocation, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
				}
				else
				{
					CryLogAlways("CPlayerStateLedge::RequestStartLeftMoveAnim ledgeId not Valid");
				}
			}
			return;
		}
		else if (ledgeInfo.IsFlipped() && dist < dist_to_corner)
		{
			Vec3 dir_x(-1, 0, 0);
			dir_x = player.GetEntity()->GetRotation() * dir_x;
			Vec3 pos_x = m_mostRecentlyEnteredAction->orig_loc.t;
			IMovementController * pMC = player.GetMovementController();
			SMovementState info;
			if (pMC)
				pMC->GetMovementState(info);

			if (pos_rt.IsZero())
			{
				pos_rt = info.eyePosition;
			}

			pos_x = (pos_rt + pos_x) * 0.5f;
			pos_x.z = m_mostRecentlyEnteredAction->orig_loc.t.z;
			dir_x.z = 0.0f;
			Vec3 vPosition = pos_x + (dir_x);
			pos_x.z = pos_x.z + 0.2f;
			mtxWorldTm.SetTranslation(pos_x);
			UpdateNearestGrabbableLedge3(player, vPosition, &ledge, true, -0.3f);
			const SLedgeTransitionData::EOnLedgeTransition ledgeTransition = SLedgeTransitionData::eOLT_Falling;
			if (m_ledgeId == ledge.m_nearestGrabbableLedgeId)
				return;

			if (ledgeTransition != SLedgeTransitionData::eOLT_None)
			{
				Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
				const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
				const SLedgeInfo& ledgeInfo2 = pLedgeManager->GetLedgeById(LedgeId(ledge.m_nearestGrabbableLedgeId));
				CRY_ASSERT(ledgeInfo2.IsValid());
				const LedgeId ledgeId = LedgeId(ledge.m_nearestGrabbableLedgeId);
				if (ledgeId.IsValid())
				{
					const LedgeId ledgeId_old = m_ledgeId;
					mtxWorldTm = player.GetEntity()->GetWorldTM();
					m_ledgeSpeedMultiplier = GetLedgeGrabSpeed(player);
					SLedgeTransitionData::EOnLedgeTransition transition = ledgeTransition;
					if ((transition > SLedgeTransitionData::eOLT_None) && (transition < SLedgeTransitionData::eOLT_MaxTransitions))
					{
						const SLedgeBlendingParams& ledgeGrabParams = GetLedgeGrabbingParams().m_grabTransitionsParams[transition];
						if (transition == SLedgeTransitionData::eOLT_VaultOver || transition == SLedgeTransitionData::eOLT_VaultOnto)
						{
							const float vel2D = player.GetActorPhysics().velocity.GetLength2D();
							m_exitVelocity.Set(0.f, vel2D, ledgeGrabParams.m_vExitVelocity.z);

							const float animSpeedMult = (float)__fsel(-vel2D, g_pGameCVars->g_vaultMinAnimationSpeed, max(g_pGameCVars->g_vaultMinAnimationSpeed, vel2D / s_PlayerMax2DPhysicsVelocity));

							m_ledgeSpeedMultiplier *= animSpeedMult;
						}
						else
						{
							m_exitVelocity = ledgeGrabParams.m_vExitVelocity;
						}

						StartLedgeBlending(player, ledgeGrabParams);
						float dist_cn = m_ledgeBlending.m_qtTargetLocation.t.GetDistance(m_mostRecentlyEnteredAction->orig_loc.t);
						if (dist_cn > 0.7f)
							return;
					}
					m_ledgePreviousPosition = ledgeInfo2.GetPosition();
					m_ledgePreviousRotation.SetRotationVDir(ledgeInfo2.GetFacingDirection());
					IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity(ledgeInfo2.GetEntityId());
					if (pLedgeEntity != NULL)
					{
						EntityScripts::CallScriptFunction(pLedgeEntity, pLedgeEntity->GetScriptTable(), "StartUse", ScriptHandle(player.GetEntity()));
					}
					m_ledgeId = ledgeId;
					InterruptCurrentAnimation(player);
					dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
					QueueLedgeAction(player, new CActionLedgeRight(this, player, m_ledgeBlending.m_qtTargetLocation, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
				}
				else
				{
					CryLogAlways("CPlayerStateLedge::RequestStartLeftMoveAnim ledgeId not Valid");
				}
			}
			return;
		}
		InterruptCurrentAnimation(player);
		dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
		QueueLedgeAction(player, new CActionLedgeLeft(this, player, orig_loc_qt, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
		//m_mostRecentlyEnteredAction->InitialiseWithParams("left");
	}
}

void CPlayerStateLedge::RequestStartRightMoveAnim(CPlayer& player)
{
	IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter();
	if (!pAnimChar)
		return;

	Vec3 pos_rt = player.GetBonePosition("Bip01 Neck");

	SLedgeTransitionData ledge(LedgeId::invalid_id);
	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());
	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());
	float dist_to_corner = 0.60f;
	if (m_mostRecentlyEnteredAction)
	{
		if (!m_mostRecentlyEnteredAction->SpecialTimerEnded())
			return;

		FragmentID fragmentID_nv;
		IActionController *pActionController = pAnimChar->GetActionController();
		if (pActionController)
		{
			SAnimationContext &animContext = pActionController->GetContext();
			fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge_right");
			//CryLogAlways("CPlayerStateLedge::RequestStartRightMoveAnim");
		}
		Matrix34 mtxWorldTm = player.GetEntity()->GetWorldTM();
		QuatT orig_loc_qt = CalculateLedgeOffsetLocation(mtxWorldTm, Vec3(ZERO), false);//m_mostRecentlyEnteredAction->orig_loc;
		float dist = orig_loc_qt.t.GetDistance(ledgeInfo.GetPositionPoint1());
		float dist2 = orig_loc_qt.t.GetDistance(ledgeInfo.GetPositionPoint2());
		if (dist < dist_to_corner && !ledgeInfo.IsFlipped())
		{
			Vec3 dir_x(1,0,0);
			dir_x = player.GetEntity()->GetRotation() * dir_x;
			Vec3 pos_x = m_mostRecentlyEnteredAction->orig_loc.t;
			IMovementController * pMC = player.GetMovementController();
			SMovementState info;
			if (pMC)
				pMC->GetMovementState(info);

			if (pos_rt.IsZero())
			{
				pos_rt = info.eyePosition;
			}

			pos_x = (pos_rt + pos_x) * 0.5f;
			pos_x.z = m_mostRecentlyEnteredAction->orig_loc.t.z;
			dir_x.z = 0.0f;
			Vec3 vPosition = pos_x + (dir_x);
			pos_x.z = pos_x.z + 0.2f;
			mtxWorldTm.SetTranslation(pos_x);
			UpdateNearestGrabbableLedge3(player, vPosition, &ledge, true, -0.3f);
			const SLedgeTransitionData::EOnLedgeTransition ledgeTransition = SLedgeTransitionData::eOLT_Falling;
			if (m_ledgeId == ledge.m_nearestGrabbableLedgeId)
				return;

			if (ledgeTransition != SLedgeTransitionData::eOLT_None)
			{
				Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
				const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
				const SLedgeInfo& ledgeInfo2 = pLedgeManager->GetLedgeById(LedgeId(ledge.m_nearestGrabbableLedgeId));
				CRY_ASSERT(ledgeInfo2.IsValid());
				const LedgeId ledgeId = LedgeId(ledge.m_nearestGrabbableLedgeId);
				if (ledgeId.IsValid())
				{
					const LedgeId ledgeId_old = m_ledgeId;
					mtxWorldTm = player.GetEntity()->GetWorldTM();
					m_ledgeSpeedMultiplier = GetLedgeGrabSpeed(player);
					SLedgeTransitionData::EOnLedgeTransition transition = ledgeTransition;
					if ((transition > SLedgeTransitionData::eOLT_None) && (transition < SLedgeTransitionData::eOLT_MaxTransitions))
					{
						const SLedgeBlendingParams& ledgeGrabParams = GetLedgeGrabbingParams().m_grabTransitionsParams[transition];
						if (transition == SLedgeTransitionData::eOLT_VaultOver || transition == SLedgeTransitionData::eOLT_VaultOnto)
						{
							const float vel2D = player.GetActorPhysics().velocity.GetLength2D();
							m_exitVelocity.Set(0.f, vel2D, ledgeGrabParams.m_vExitVelocity.z);

							const float animSpeedMult = (float)__fsel(-vel2D, g_pGameCVars->g_vaultMinAnimationSpeed, max(g_pGameCVars->g_vaultMinAnimationSpeed, vel2D / s_PlayerMax2DPhysicsVelocity));

							m_ledgeSpeedMultiplier *= animSpeedMult;
						}
						else
						{
							m_exitVelocity = ledgeGrabParams.m_vExitVelocity;
						}

						StartLedgeBlending(player, ledgeGrabParams);
						float dist_cn = m_ledgeBlending.m_qtTargetLocation.t.GetDistance(m_mostRecentlyEnteredAction->orig_loc.t);
						if (dist_cn > 0.7f)
							return;
					}
					m_ledgePreviousPosition = ledgeInfo2.GetPosition();
					m_ledgePreviousRotation.SetRotationVDir(ledgeInfo2.GetFacingDirection());
					IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity(ledgeInfo2.GetEntityId());
					if (pLedgeEntity != NULL)
					{
						EntityScripts::CallScriptFunction(pLedgeEntity, pLedgeEntity->GetScriptTable(), "StartUse", ScriptHandle(player.GetEntity()));
					}
					m_ledgeId = ledgeId;
					InterruptCurrentAnimation(player);
					dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
					QueueLedgeAction(player, new CActionLedgeRight(this, player, m_ledgeBlending.m_qtTargetLocation, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
				}
				else
				{
					CryLogAlways("CPlayerStateLedge::RequestStartRightMoveAnim ledgeId not Valid");
				}
			}
			return;
		}
		else if (ledgeInfo.IsFlipped() && dist2 < dist_to_corner)
		{
			Vec3 dir_x(1, 0, 0);
			dir_x = player.GetEntity()->GetRotation() * dir_x;
			Vec3 pos_x = m_mostRecentlyEnteredAction->orig_loc.t;
			IMovementController * pMC = player.GetMovementController();
			SMovementState info;
			if (pMC)
				pMC->GetMovementState(info);

			if (pos_rt.IsZero())
			{
				pos_rt = info.eyePosition;
			}

			pos_x = (pos_rt + pos_x) * 0.5f;
			pos_x.z = m_mostRecentlyEnteredAction->orig_loc.t.z;
			dir_x.z = 0.0f;
			Vec3 vPosition = pos_x + (dir_x);
			pos_x.z = pos_x.z + 0.2f;
			mtxWorldTm.SetTranslation(pos_x);
			UpdateNearestGrabbableLedge3(player, vPosition, &ledge, true, -0.3f);
			const SLedgeTransitionData::EOnLedgeTransition ledgeTransition = SLedgeTransitionData::eOLT_Falling;
			if (m_ledgeId == ledge.m_nearestGrabbableLedgeId)
				return;

			if (ledgeTransition != SLedgeTransitionData::eOLT_None)
			{
				Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
				const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
				const SLedgeInfo& ledgeInfo2 = pLedgeManager->GetLedgeById(LedgeId(ledge.m_nearestGrabbableLedgeId));
				CRY_ASSERT(ledgeInfo2.IsValid());
				const LedgeId ledgeId = LedgeId(ledge.m_nearestGrabbableLedgeId);
				if (ledgeId.IsValid())
				{
					const LedgeId ledgeId_old = m_ledgeId;
					mtxWorldTm = player.GetEntity()->GetWorldTM();
					m_ledgeSpeedMultiplier = GetLedgeGrabSpeed(player);
					SLedgeTransitionData::EOnLedgeTransition transition = ledgeTransition;
					if ((transition > SLedgeTransitionData::eOLT_None) && (transition < SLedgeTransitionData::eOLT_MaxTransitions))
					{
						const SLedgeBlendingParams& ledgeGrabParams = GetLedgeGrabbingParams().m_grabTransitionsParams[transition];
						if (transition == SLedgeTransitionData::eOLT_VaultOver || transition == SLedgeTransitionData::eOLT_VaultOnto)
						{
							const float vel2D = player.GetActorPhysics().velocity.GetLength2D();
							m_exitVelocity.Set(0.f, vel2D, ledgeGrabParams.m_vExitVelocity.z);

							const float animSpeedMult = (float)__fsel(-vel2D, g_pGameCVars->g_vaultMinAnimationSpeed, max(g_pGameCVars->g_vaultMinAnimationSpeed, vel2D / s_PlayerMax2DPhysicsVelocity));

							m_ledgeSpeedMultiplier *= animSpeedMult;
						}
						else
						{
							m_exitVelocity = ledgeGrabParams.m_vExitVelocity;
						}

						StartLedgeBlending(player, ledgeGrabParams);
						float dist_cn = m_ledgeBlending.m_qtTargetLocation.t.GetDistance(m_mostRecentlyEnteredAction->orig_loc.t);
						if (dist_cn > 0.7f)
							return;
					}
					m_ledgePreviousPosition = ledgeInfo2.GetPosition();
					m_ledgePreviousRotation.SetRotationVDir(ledgeInfo2.GetFacingDirection());
					IEntity *pLedgeEntity = gEnv->pEntitySystem->GetEntity(ledgeInfo2.GetEntityId());
					if (pLedgeEntity != NULL)
					{
						EntityScripts::CallScriptFunction(pLedgeEntity, pLedgeEntity->GetScriptTable(), "StartUse", ScriptHandle(player.GetEntity()));
					}
					m_ledgeId = ledgeId;
					InterruptCurrentAnimation(player);
					dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
					QueueLedgeAction(player, new CActionLedgeRight(this, player, m_ledgeBlending.m_qtTargetLocation, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
				}
				else
				{
					CryLogAlways("CPlayerStateLedge::RequestStartRightMoveAnim ledgeId not Valid");
				}
			}
			return;
		}
		InterruptCurrentAnimation(player);
		dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
		QueueLedgeAction(player, new CActionLedgeRight(this, player, orig_loc_qt, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
		//m_mostRecentlyEnteredAction->InitialiseWithParams("right");
	}
}

void CPlayerStateLedge::RequestCheckUpVect(CPlayer& player)
{
	SLedgeTransitionData ledge(LedgeId::invalid_id);
	float plzc = player.GetEntity()->GetPos().z + 2.0f;
	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());
	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());
	float ledge_poz = ledgeInfo.GetPosition().z + g_pGameCVars->g_ledge_movement_up_to_next_ledge_max_dist;
	bool ledge_up = TryLedgeGrabOnLedge(player, ledge_poz, ledgeInfo.GetPosition().z, false, &ledge, true, g_pGameCVars->g_ledge_movement_up_to_next_ledge_max_dist);

	/*if (m_ledgeId == ledge.m_nearestGrabbableLedgeId)
	{
		CryLogAlways("m_ledgeId == ledge.m_nearestGrabbableLedgeId is true");
		return;
	}*/

	if (m_mostRecentlyEnteredAction && !m_mostRecentlyEnteredAction->SpecialTimerEnded())
		return;

	if (ledge_up && !(m_ledgeId == ledge.m_nearestGrabbableLedgeId))
	{
		SStateEventLedge ledgeEvent(ledge);
		Vec3 pl_pos = player.GetEntity()->GetPos();
		//pl_pos.z = plzc;
		//player.GetEntity()->SetPos(pl_pos);
		InterruptCurrentAnimation(player);
		//player.m_stateMachineMovement.StateMachineAddFlag(EPlayerStateFlags_Ledge);
		//player.SetLastTimeInLedge(gEnv->pTimer->GetAsyncCurTime());
		//player.SetStance(STANCE_STAND);
		//player.StateMachineHandleEventMovement(ledgeEvent);
		OnEnter(player, ledgeEvent, "Ledge_next_up");
		//CryLogAlways("CPlayerStateLedge::RequestCheckUpVect: ledge_up is true");
	}
	else
	{
		if (!ledgeInfo.IsValid())
			return;

		IMovementController * pMC = player.GetMovementController();
		SMovementState info;
		if (pMC)
			pMC->GetMovementState(info);

		IPhysicalEntity *pIgnore = NULL;
		pIgnore = player.GetEntity() ? player.GetEntity()->GetPhysics() : NULL;
		ray_hit hit;
		int n;
		Vec3 dir_to_test(0, 1, 0);
		Vec3 pos_tt = Vec3(ZERO);
		pos_tt = ledgeInfo.GetPosition();
		pos_tt.z += 0.2f;
		dir_to_test = player.GetEntity()->GetRotation()*dir_to_test;
		dir_to_test = -ledgeInfo.GetFacingDirection().normalized();
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos_tt, dir_to_test*8.0f, ent_all,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, &pIgnore, pIgnore ? 1 : 0);
		if (n > 0)
		{
			float dist = hit.pt.GetDistance(pos_tt);
			if (dist < 0.60f)
				return;

			//CryLogAlways("CPlayerStateLedge::RequestCheckUpVect: RayWorldIntersection dist > 0.60");

			IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter();
			if (!pAnimChar)
				return;

			if (m_mostRecentlyEnteredAction)
			{
				FragmentID fragmentID_nv;
				IActionController *pActionController = pAnimChar->GetActionController();
				if (pActionController)
				{
					SAnimationContext &animContext = pActionController->GetContext();
					fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge_Up_to_place");
				}
				QuatT orig_loc_qt = m_mostRecentlyEnteredAction->orig_loc;
				InterruptCurrentAnimation(player);
				dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
				QueueLedgeAction(player, new CActionLedgeUpClumbingToPlace(this, player, orig_loc_qt, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
			}
		}
		else
		{
			IAnimatedCharacter* pAnimChar = player.GetAnimatedCharacter();
			if (!pAnimChar)
				return;

			//CryLogAlways("CPlayerStateLedge::RequestCheckUpVect: RayWorldIntersection n <= 0");

			if (m_mostRecentlyEnteredAction)
			{
				FragmentID fragmentID_nv;
				IActionController *pActionController = pAnimChar->GetActionController();
				if (pActionController)
				{
					SAnimationContext &animContext = pActionController->GetContext();
					fragmentID_nv = animContext.controllerDef.m_fragmentIDs.Find("Ledge_Up_to_place");
				}
				QuatT orig_loc_qt = m_mostRecentlyEnteredAction->orig_loc;
				InterruptCurrentAnimation(player);
				dist_from_ledge_start_point = ledgeInfo.GetPositionPoint1().GetDistance(m_ledgeBlending.m_qtTargetLocation.t);
				QueueLedgeAction(player, new CActionLedgeUpClumbingToPlace(this, player, orig_loc_qt, fragmentID_nv, SLedgeTransitionData::EOnLedgeTransition::eOLT_Falling, false, false, false));
			}
		}
		//CryLogAlways("CPlayerStateLedge::RequestCheckUpVect: ledge_up is false");
	}
}

void CPlayerStateLedge::RequestCheckDownVect(CPlayer& player)
{
	if (m_mostRecentlyEnteredAction && !m_mostRecentlyEnteredAction->SpecialTimerEnded())
		return;

	SLedgeTransitionData ledge(LedgeId::invalid_id);
	float plzc = player.GetEntity()->GetPos().z;
	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());
	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());
	float ledge_poz = ledgeInfo.GetPosition().z - 0.5f;
	bool ledge_down = TryLedgeGrabOnLedge(player, ledge_poz, ledgeInfo.GetPosition().z, true, &ledge, true, -0.5f);
	if (ledge_down && !(m_ledgeId == ledge.m_nearestGrabbableLedgeId))
	{
		SStateEventLedge ledgeEvent(ledge);
		Vec3 pl_pos = player.GetEntity()->GetPos();
		InterruptCurrentAnimation(player);
		OnEnter(player, ledgeEvent, "Ledge_next_down");
	}
	else
	{
		InterruptCurrentAnimation(player, true);
	}
}

void CPlayerStateLedge::QueueLedgeAction(CPlayer& player, CActionLedgeGrab * action)
{
	if (action)
	{
		player.GetAnimatedCharacter()->GetActionController()->Queue(*action);
	}
}

void CPlayerStateLedge::UpdateNearestGrabbableLedge( const CPlayer& player, SLedgeTransitionData* pLedgeState, const bool ignoreCharacterMovement )
{
	pLedgeState->m_ledgeTransition = SLedgeTransitionData::eOLT_None;
	pLedgeState->m_nearestGrabbableLedgeId = LedgeId::invalid_id;

	// get the position and movement direction for this entity
	// prepare as parameters for the ledge manager

	const Matrix34& mtxWorldTm = player.GetEntity()->GetWorldTM();
	Vec3 vPosition = mtxWorldTm.GetTranslation();

	vPosition += Vec3(0.0f, 0.0f, player.GetStanceInfo(player.GetStance())->viewOffset.z);

	Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
	vScanDirection.NormalizeFast();
	//CryLogAlways("CPlayerStateLedge::UpdateNearestGrabbableLedge 1");
	// retrieve the ledge manager and run a query on whether a ledge is within the parameters
	const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
	//CryLogAlways("CPlayerStateLedge::UpdateNearestGrabbableLedge 2");
	const LedgeId ledgeId = g_pGame->GetLedgeManager()->FindNearestLedge(vPosition, vScanDirection, GetLedgeGrabbingParams().m_ledgeNearbyParams.m_fMaxDistance, DEG2RAD(nearbyParams.m_fMaxAngleDeviationFromSearchDirInDegrees), DEG2RAD(nearbyParams.m_fMaxExtendedAngleDeviationFromSearchDirInDegrees));
	//CryLogAlways("CPlayerStateLedge::UpdateNearestGrabbableLedge 3");
	if (ledgeId.IsValid())
	{
		//CryLogAlways("CPlayerStateLedge::UpdateNearestGrabbableLedge 4");
		CWeapon* pCurrentWeapon = player.GetWeapon(player.GetCurrentItemId());
		const bool canLedgeGrabWithWeapon = (pCurrentWeapon == NULL) || (pCurrentWeapon->CanLedgeGrab());
		if (canLedgeGrabWithWeapon)
		{
			const bool characterMovesTowardLedge = IsCharacterMovingTowardsLedge(player, ledgeId);
			const bool ledgePointsTowardCharacter = IsLedgePointingTowardCharacter(player, ledgeId);
			const bool isPressingJump = player.m_jumpButtonIsPressed;
			if ((characterMovesTowardLedge || ignoreCharacterMovement) && ledgePointsTowardCharacter)
			{
				pLedgeState->m_nearestGrabbableLedgeId = ledgeId;
				pLedgeState->m_ledgeTransition = GetBestTransitionToLedge(player, player.GetEntity()->GetWorldPos(), ledgeId, pLedgeState);
			}	
		}
	}
}

void CPlayerStateLedge::UpdateNearestGrabbableLedge2(const CPlayer& player, SLedgeTransitionData* pLedgeState, const bool ignoreCharacterMovement, const float z_add_vect)
{
	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());
	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());
	//ledgeInfo.GetPosition();
	//float ledge_poz = ledgeInfo.GetPosition().z + 0.5f;

	pLedgeState->m_ledgeTransition = SLedgeTransitionData::eOLT_None;
	pLedgeState->m_nearestGrabbableLedgeId = LedgeId::invalid_id;

	// get the position and movement direction for this entity
	// prepare as parameters for the ledge manager
	Vec3 pos_rt = ledgeInfo.GetPosition();
	if (m_mostRecentlyEnteredAction)
	{
		pos_rt = m_mostRecentlyEnteredAction->orig_loc.t;
	}

	CActor* pTarget = (CActor *)&player;
	pos_rt = pTarget->GetBonePosition("Bip01 Neck");//GetEntity()->GetPos();
	if (pos_rt.IsZero())
	{
		pos_rt = player.GetEntity()->GetPos();
	}
	const Matrix34& mtxWorldTm = player.GetEntity()->GetWorldTM();
	Vec3 vPosition = pos_rt + Vec3(0, 0, z_add_vect);

	//vPosition += Vec3(0.0f, 0.0f, player.GetStanceInfo(player.GetStance())->viewOffset.z);

	Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
	vScanDirection.NormalizeFast();

	// retrieve the ledge manager and run a query on whether a ledge is within the parameters
	const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
	const LedgeId ledgeId = g_pGame->GetLedgeManager()->FindNearestLedge(vPosition, vScanDirection, GetLedgeGrabbingParams().m_ledgeNearbyParams.m_fMaxDistance, DEG2RAD(nearbyParams.m_fMaxAngleDeviationFromSearchDirInDegrees), DEG2RAD(nearbyParams.m_fMaxExtendedAngleDeviationFromSearchDirInDegrees));
	if (ledgeId.IsValid())
	{
		CWeapon* pCurrentWeapon = player.GetWeapon(player.GetCurrentItemId());
		const bool canLedgeGrabWithWeapon = (pCurrentWeapon == NULL) || (pCurrentWeapon->CanLedgeGrab());
		if (canLedgeGrabWithWeapon)
		{
			const bool characterMovesTowardLedge = IsCharacterMovingTowardsLedge(player, ledgeId);
			const bool ledgePointsTowardCharacter = IsLedgePointingTowardCharacter(player, ledgeId);
			const bool isPressingJump = player.m_jumpButtonIsPressed;
			if ((characterMovesTowardLedge || ignoreCharacterMovement) && ledgePointsTowardCharacter)
			{
				pLedgeState->m_nearestGrabbableLedgeId = ledgeId;
				pLedgeState->m_ledgeTransition = GetBestTransitionToLedge(player, player.GetEntity()->GetWorldPos(), ledgeId, pLedgeState);
			}
		}
	}
}

void CPlayerStateLedge::UpdateNearestGrabbableLedge3(const CPlayer& player, const Vec3& vPositionToFind, SLedgeTransitionData* pLedgeState, const bool ignoreCharacterMovement, const float z_add_vect)
{
	pLedgeState->m_ledgeTransition = SLedgeTransitionData::eOLT_None;
	pLedgeState->m_nearestGrabbableLedgeId = LedgeId::invalid_id;

	// get the position and movement direction for this entity
	// prepare as parameters for the ledge manager
	Vec3 pos_rt = vPositionToFind;
	Matrix34 mtxWorldTm = player.GetEntity()->GetWorldTM();
	Vec3 vPosition = pos_rt + Vec3(0, 0, z_add_vect);
	Vec3 vScanDirection = mtxWorldTm.TransformVector(GetLedgeGrabbingParams().m_ledgeNearbyParams.m_vSearchDir);
	vScanDirection.NormalizeFast();

	// retrieve the ledge manager and run a query on whether a ledge is within the parameters
	const CPlayerStateLedge::SLedgeNearbyParams& nearbyParams = GetLedgeGrabbingParams().m_ledgeNearbyParams;
	const LedgeId ledgeId = g_pGame->GetLedgeManager()->FindNearestLedge(vPosition, vScanDirection, GetLedgeGrabbingParams().m_ledgeNearbyParams.m_fMaxDistance, DEG2RAD(nearbyParams.m_fMaxAngleDeviationFromSearchDirInDegrees), DEG2RAD(nearbyParams.m_fMaxExtendedAngleDeviationFromSearchDirInDegrees));
	if (ledgeId.IsValid())
	{
		const SLedgeInfo& ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById(ledgeId);
		if (ledgeInfo.IsValid())
		{

			const SLedgeInfo& ledgeInfo2 = g_pGame->GetLedgeManager()->GetLedgeById(m_ledgeId);
			if (ledgeInfo2.IsValid())
			{
				float pos_z1 = ledgeInfo.GetPosition().z;
				float pos_z2 = ledgeInfo2.GetPosition().z;
				float pos_cn = pos_z1 - pos_z2;
				if (pos_cn < 0.0f)
				{
					if (pos_cn < -0.2f)
						return;
				}
				else
				{
					if (pos_cn > 0.2f)
						return;
				}
			}
		}
		
		CWeapon* pCurrentWeapon = player.GetWeapon(player.GetCurrentItemId());
		const bool canLedgeGrabWithWeapon = (pCurrentWeapon == NULL) || (pCurrentWeapon->CanLedgeGrab());
		if (canLedgeGrabWithWeapon)
		{
			const bool characterMovesTowardLedge = IsCharacterMovingTowardsLedge(player, ledgeId);
			const bool ledgePointsTowardCharacter = IsLedgePointingTowardCharacter(player, ledgeId);
			const bool isPressingJump = player.m_jumpButtonIsPressed;
			if ((characterMovesTowardLedge || ignoreCharacterMovement) && ledgePointsTowardCharacter)
			{
				pLedgeState->m_nearestGrabbableLedgeId = ledgeId;
				pLedgeState->m_ledgeTransition = GetBestTransitionToLedge(player, player.GetEntity()->GetWorldPos(), ledgeId, pLedgeState);
			}
		}
	}
}

SLedgeTransitionData::EOnLedgeTransition CPlayerStateLedge::GetBestTransitionToLedge(const CPlayer& player, const Vec3& refPosition, const LedgeId& ledgeId, SLedgeTransitionData* pLedgeState)
{
	CRY_ASSERT_MESSAGE(ledgeId.IsValid(), "Ledge index should be valid");

	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	const SLedgeInfo detectedLedge = pLedgeManager->GetLedgeById(ledgeId);

	CRY_ASSERT( detectedLedge.IsValid() );

	const bool useVault = detectedLedge.AreFlagsSet( kLedgeFlag_useVault );
	const bool useHighVault = detectedLedge.AreFlagsSet( kLedgeFlag_useHighVault );
	const bool isThinLedge = detectedLedge.AreFlagsSet( kLedgeFlag_isThin );
	const bool endFalling = detectedLedge.AreFlagsSet( kLedgeFlag_endFalling );
	const bool usableByMarines = detectedLedge.AreFlagsSet( kledgeFlag_usableByMarines );
	const Vec3 ledgePos = detectedLedge.GetPosition();

	if (CanReachPlatform(player, ledgePos, refPosition))
	{
		return SLedgeTransitionData::eOLT_None;
	}

	QuatT targetLocation;

	const Quat pRot = Quat::CreateRotationVDir( detectedLedge.GetFacingDirection() );

	const Vec3 vToVec = pLedgeManager->FindVectorToClosestPointOnLedge(refPosition, detectedLedge);
	targetLocation.q = pRot * Quat::CreateRotationZ(DEG2RAD(180)); 

	float bestDistanceSqr = 100.0f;
	SLedgeTransitionData::EOnLedgeTransition bestTransition = SLedgeTransitionData::eOLT_None;
	 
	// if required could add handling to reject ledges based on marines within CLedgeManager::FindNearestLedge()
	if (!usableByMarines)
	{
		return SLedgeTransitionData::eOLT_None;
	}

	//Get nearest entry point, from available transitions
	for (int i = 0; i < SLedgeTransitionData::eOLT_MaxTransitions; ++i)
	{
		const SLedgeBlendingParams& ledgeBlendParams = GetLedgeGrabbingParams().m_grabTransitionsParams[i];
		targetLocation.t = refPosition + vToVec - (Vec3(0, 0, ledgeBlendParams.m_vPositionOffset.z)) - ((targetLocation.q * FORWARD_DIRECTION) * ledgeBlendParams.m_vPositionOffset.y);

		if ((ledgeBlendParams.m_ledgeType == eLT_Thin) && !isThinLedge)
		{
			continue;
		}
		else if ((ledgeBlendParams.m_ledgeType == eLT_Wide) && isThinLedge)
		{
			continue;
		}
		else if (ledgeBlendParams.m_bIsVault != useVault)
		{
			continue;
		}
		else if (ledgeBlendParams.m_bIsHighVault != useHighVault)
		{
			continue;
		}
		else if (ledgeBlendParams.m_bEndFalling != endFalling)
		{
			continue;
		}

		float distanceSqr = (targetLocation.t - refPosition).len2();
		if (distanceSqr < bestDistanceSqr)
		{
			bestTransition = (SLedgeTransitionData::EOnLedgeTransition)(i);
			bestDistanceSqr = distanceSqr;
		}
	}

	return bestTransition;
}

bool CPlayerStateLedge::CanReachPlatform(const CPlayer& player, const Vec3& ledgePosition, const Vec3& refPosition)
{
	const SPlayerStats& stats = *player.GetActorStats();
	const SActorPhysics& actorPhysics = player.GetActorPhysics();

	const Vec3 distanceVector = ledgePosition - refPosition;

	//Some rough prediction for vertical movement...
	const Vec2 distanceVector2D = Vec2(distanceVector.x, distanceVector.y);
	const float distance2DtoWall = distanceVector2D.GetLength();
	const float estimatedTimeToReachWall = stats.speedFlat ? distance2DtoWall / stats.speedFlat : distance2DtoWall;

	const float predictedHeightWhenReachingWall = refPosition.z + (actorPhysics.velocity.z * estimatedTimeToReachWall) + (0.5f * actorPhysics.gravity.z * (estimatedTimeToReachWall*estimatedTimeToReachWall));
	const float predictedHeightWhenReachingWallDiff = (ledgePosition.z - predictedHeightWhenReachingWall);

	//If falling already ...
	if (actorPhysics.velocity.z < 0.0f)
	{
		return (predictedHeightWhenReachingWallDiff < 0.1f);
	}
	else if ((fabs(actorPhysics.gravity.z) > 0.0f) && !gEnv->bMultiplayer)		// Causes problems with network in multiplayer
	{
		//Going up ...
		const float estimatedTimeToReachZeroVerticalSpeed = -actorPhysics.velocity.z / actorPhysics.gravity.z;
		const float predictedHeightWhenReachingZeroSpeed = refPosition.z + (actorPhysics.velocity.z * estimatedTimeToReachZeroVerticalSpeed) + (0.5f * actorPhysics.gravity.z * (estimatedTimeToReachZeroVerticalSpeed*estimatedTimeToReachZeroVerticalSpeed));
		const float predictedHeightWhenReachingZeroSpeedDiff = (ledgePosition.z - predictedHeightWhenReachingZeroSpeed);

		return ((predictedHeightWhenReachingZeroSpeedDiff < 0.2f) && !player.m_jumpButtonIsPressed) ;
	}

	return false;
}

//--------------------------------------------------------------------------------
QuatT CPlayerStateLedge::CalculateLedgeOffsetLocation( const Matrix34& worldMat, const Vec3& vPositionOffset, const bool keepOrientation ) const
{
	QuatT targetLocation;
	const Vec3& vWorldPos = worldMat.GetTranslation();
	const bool doOrientationBlending = !keepOrientation;

	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());

	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById( m_ledgeId );
	assert(ledgeInfo.IsValid());

	

	const Quat pRot = doOrientationBlending ? Quat::CreateRotationVDir( ledgeInfo.GetFacingDirection() ) : Quat(worldMat);

	const Vec3 vToVec =	pLedgeManager->FindVectorToClosestPointOnLedge(vWorldPos, ledgeInfo);

	targetLocation.q = doOrientationBlending ? pRot * Quat::CreateRotationZ(DEG2RAD(180)) : pRot; 
	targetLocation.t = vWorldPos + vToVec;

	return targetLocation;
}

QuatT CPlayerStateLedge::CalculateLedgeOffsetLocation2(const Matrix34& worldMat, const Vec3& vPositionOffset, const LedgeId&	 ledgeId) const
{
	QuatT targetLocation;
	const Vec3& vWorldPos = worldMat.GetTranslation();
	const bool doOrientationBlending = false;

	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(ledgeId.IsValid());

	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(ledgeId);
	assert(ledgeInfo.IsValid());



	const Quat pRot = doOrientationBlending ? Quat::CreateRotationVDir(ledgeInfo.GetFacingDirection()) : Quat(worldMat);

	const Vec3 vToVec = pLedgeManager->FindVectorToClosestPointOnLedge(vWorldPos, ledgeInfo);

	targetLocation.q = doOrientationBlending ? pRot * Quat::CreateRotationZ(DEG2RAD(180)) : pRot;
	targetLocation.t = vWorldPos + vToVec;

	return targetLocation;
}

QuatT CPlayerStateLedge::CalculateLedgeOffsetLocation3(const Matrix34& worldMat, const Vec3& vPositionOffset)
{
	QuatT targetLocation;
	const Vec3& vWorldPos = worldMat.GetTranslation();
	const bool doOrientationBlending = false;

	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	assert(m_ledgeId.IsValid());

	const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
	assert(ledgeInfo.IsValid());



	const Quat pRot = doOrientationBlending ? Quat::CreateRotationVDir(ledgeInfo.GetFacingDirection()) : Quat(worldMat);

	const Vec3 vToVec = pLedgeManager->FindVectorToClosestPointOnLedge(vWorldPos, ledgeInfo);

	targetLocation.q = doOrientationBlending ? pRot * Quat::CreateRotationZ(DEG2RAD(180)) : pRot;
	targetLocation.t = vWorldPos + vToVec;

	return targetLocation;
}

//--------------------------------------------------------------------------------
void CPlayerStateLedge::StartLedgeBlending( CPlayer& player, const SLedgeBlendingParams &blendingParams)
{
	CRY_TODO(15, 4, 2010, "Remove this CVAR once the ledge grab is sorted, is a useful debug tool for now...");

	assert(m_ledgeSpeedMultiplier > 0);

	m_ledgeBlending.m_qtTargetLocation = CalculateLedgeOffsetLocation( player.GetEntity()->GetWorldTM(), blendingParams.m_vPositionOffset, false/*blendingParams.m_bKeepOrientation*/);

	Vec3 forceLookVector = m_ledgeBlending.m_qtTargetLocation.GetColumn1();
	forceLookVector.z = 0.0f;
	forceLookVector.Normalize();
	m_ledgeBlending.m_forwardDir = forceLookVector;

}

bool CPlayerStateLedge::IsCharacterMovingTowardsLedge( const CPlayer& player, const LedgeId& ledgeId)
{
	CRY_ASSERT_MESSAGE(ledgeId.IsValid(), "Ledge index should be valid");

	// if stick is pressed forwards, always take this ledge
	// 0.2f --> big enough to allow some room for stick deadzone
	const Vec3 inputMovement = player.GetBaseQuat().GetInverted() * player.GetLastRequestedVelocity();
	if (inputMovement.y >= 0.2f) 
	{
		const IEntity* pCharacterEntity = player.GetEntity();
		const Vec3 vCharacterPos = pCharacterEntity->GetWorldPos();

		const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
		const SLedgeInfo detectedLedge = pLedgeManager->GetLedgeById(ledgeId);
		if ( detectedLedge.IsValid() )
		{
			Vec3 vCharactertoLedge = pLedgeManager->FindVectorToClosestPointOnLedge(vCharacterPos, detectedLedge);

			vCharactertoLedge.z = 0.0f;

			Vec3 vVelocity = player.GetActorPhysics().velocity;
			vVelocity.z = 0.0f;

			return (vCharactertoLedge * vVelocity > 0.0f);
		}
	}

	return false;
}

//----------------------------------------------------------
bool CPlayerStateLedge::IsLedgePointingTowardCharacter(const CPlayer& player, const LedgeId& ledgeId)
{
	CRY_ASSERT_MESSAGE(ledgeId.IsValid(), "Ledge index should be valid");

	const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
	const SLedgeInfo detectedLedge = pLedgeManager->GetLedgeById(ledgeId);

	if (detectedLedge.IsValid())
	{
		const IEntity* pCharacterEntity = player.GetEntity();
		const Vec3 vLedgePos = detectedLedge.GetPosition();
		const Vec3 vCharacterPos = pCharacterEntity->GetWorldPos();
		const Vec3 vLedgeToCharacter = vCharacterPos - vLedgePos;

		return ((detectedLedge.GetFacingDirection() * vLedgeToCharacter) >= 0.0f);
	}

	return false;
}

void CPlayerStateLedge::SLedgeGrabbingParams::SetParamsFromXml(const IItemParamsNode* pParams)
{
	const char* gameModeParams = "LedgeGrabbingParams";
	
	const IItemParamsNode* pLedgeGrabParams = pParams ? pParams->GetChild( gameModeParams ) : NULL;

	if (pLedgeGrabParams)
	{
		pLedgeGrabParams->GetAttribute("normalSpeedUp", m_fNormalSpeedUp);
		pLedgeGrabParams->GetAttribute("mobilitySpeedUp", m_fMobilitySpeedUp);
		pLedgeGrabParams->GetAttribute("mobilitySpeedUpMaximum", m_fMobilitySpeedUpMaximum);

		m_ledgeNearbyParams.SetParamsFromXml(pLedgeGrabParams->GetChild("LedgeNearByParams"));

		m_grabTransitionsParams[SLedgeTransitionData::eOLT_MidAir].SetParamsFromXml(pLedgeGrabParams->GetChild("PullUpMidAir"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_Falling].SetParamsFromXml(pLedgeGrabParams->GetChild("PullUpFalling"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_VaultOver].SetParamsFromXml(pLedgeGrabParams->GetChild("VaultOver"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_VaultOverIntoFall].SetParamsFromXml(pLedgeGrabParams->GetChild("VaultOverIntoFall"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_VaultOnto].SetParamsFromXml(pLedgeGrabParams->GetChild("VaultOnto"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_HighVaultOver].SetParamsFromXml(pLedgeGrabParams->GetChild("HighVaultOver"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_HighVaultOverIntoFall].SetParamsFromXml(pLedgeGrabParams->GetChild("HighVaultOverIntoFall"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_HighVaultOnto].SetParamsFromXml(pLedgeGrabParams->GetChild("HighVaultOnto"));
		m_grabTransitionsParams[SLedgeTransitionData::eOLT_QuickLedgeGrab].SetParamsFromXml(pLedgeGrabParams->GetChild("PullUpQuick"));
	}
}

void CPlayerStateLedge::SLedgeNearbyParams::SetParamsFromXml(const IItemParamsNode* pParams)
{
	if (pParams)
	{
		pParams->GetAttribute("searchDir", m_vSearchDir);
		pParams->GetAttribute("maxDistance", m_fMaxDistance);
		pParams->GetAttribute("maxAngleDeviationFromSearchDirInDegrees", m_fMaxAngleDeviationFromSearchDirInDegrees);
		pParams->GetAttribute("maxExtendedAngleDeviationFromSearchDirInDegrees", m_fMaxExtendedAngleDeviationFromSearchDirInDegrees);
	}
}

void CPlayerStateLedge::SLedgeBlendingParams::SetParamsFromXml(const IItemParamsNode* pParams)
{
	if (pParams)
	{
		pParams->GetAttribute("positionOffset", m_vPositionOffset);
		pParams->GetAttribute("moveDuration", m_fMoveDuration);
		pParams->GetAttribute("correctionDuration", m_fCorrectionDuration);

		const char *pLedgeType = pParams->GetAttribute("ledgeType");
		if (pLedgeType)
		{
			if (!stricmp(pLedgeType, "Wide"))
			{
				m_ledgeType = eLT_Wide;
			}
			else if (!stricmp(pLedgeType, "Thin"))
			{
				m_ledgeType = eLT_Thin;
			}
		}

		int isVault = 0;
		pParams->GetAttribute("isVault", isVault);
		m_bIsVault = (isVault != 0);

		int isHighVault = 0;
		pParams->GetAttribute("isHighVault", isHighVault);
		m_bIsHighVault = (isHighVault != 0);

		pParams->GetAttribute("exitVelocityY", m_vExitVelocity.y);
		pParams->GetAttribute("exitVelocityZ", m_vExitVelocity.z);
		int keepOrientation = 0;
		pParams->GetAttribute("keepOrientation", keepOrientation);
		m_bKeepOrientation = (keepOrientation != 0);
		int endFalling = 0;
		pParams->GetAttribute("endFalling", endFalling);
		m_bEndFalling = (endFalling != 0);
	}
}

CPlayerStateLedge::SLedgeGrabbingParams& CPlayerStateLedge::GetLedgeGrabbingParams()
{
	return s_ledgeGrabbingParams;
}


bool CPlayerStateLedge::TryLedgeGrab( CPlayer& player, const float expectedJumpEndHeight, const float startJumpHeight, const bool bSprintJump, SLedgeTransitionData* pLedgeState, const bool ignoreCharacterMovementWhenFindingLedge )
{
	CRY_ASSERT( pLedgeState );
	if( !CanGrabOntoLedge( player ) )
	{
		return false;
	}
	UpdateNearestGrabbableLedge( player, pLedgeState, ignoreCharacterMovementWhenFindingLedge );
	
	if( pLedgeState->m_ledgeTransition != SLedgeTransitionData::eOLT_None )
	{
		bool bDoLedgeGrab = true;

		const bool isFalling = player.GetActorPhysics().velocity*player.GetActorPhysics().gravity>0.0f;
		if (!isFalling)
		{
			const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById( LedgeId(pLedgeState->m_nearestGrabbableLedgeId) );
			CRY_ASSERT( ledgeInfo.IsValid() );

			const bool isWindow = ledgeInfo.AreFlagsSet( kLedgeFlag_isWindow );
			const bool useVault = ledgeInfo.AreAnyFlagsSet( kLedgeFlag_useVault|kLedgeFlag_useHighVault );

			if (!isWindow)		// Windows always use ledge grabs
			{
				float ledgeHeight = ledgeInfo.GetPosition().z;
				if (expectedJumpEndHeight > (ledgeHeight + g_pGameCVars->g_ledgeGrabClearHeight))
				{
					if (useVault)
					{
						// TODO - this code doesn't seem right.. needs looking at
						float playerZ = player.GetEntity()->GetWorldPos().z;
						if (playerZ + g_pGameCVars->g_vaultMinHeightDiff > ledgeHeight)
						{
							bDoLedgeGrab = false;
						}
						else
						{
							bDoLedgeGrab = bSprintJump;
						}
					}
					else
					{
						bDoLedgeGrab = false;
					}
				}
			}
		}

		if (bDoLedgeGrab)
		{
			if( player.IsClient() )
			{
				const float fHeightofEntity = player.GetEntity()->GetWorldPos().z;
				CPlayerStateUtil::ApplyFallDamage( player, startJumpHeight, fHeightofEntity );
			}

		}
		return bDoLedgeGrab;
	}

	return false;
}

bool CPlayerStateLedge::TryLedgeGrabOnLedge(CPlayer& player, const float expectedJumpEndHeight, const float startJumpHeight, const bool bSprintJump, SLedgeTransitionData* pLedgeState, const bool ignoreCharacterMovementWhenFindingLedge, const float z_add_vect)
{
	CRY_ASSERT(pLedgeState);

	UpdateNearestGrabbableLedge2(player, pLedgeState, ignoreCharacterMovementWhenFindingLedge, z_add_vect);

	if (pLedgeState->m_ledgeTransition != SLedgeTransitionData::eOLT_None)
	{
		bool bDoLedgeGrab = true;

		const bool isFalling = player.GetActorPhysics().velocity*player.GetActorPhysics().gravity>0.0f;
		if (!isFalling)
		{
			const SLedgeInfo ledgeInfo = g_pGame->GetLedgeManager()->GetLedgeById(LedgeId(pLedgeState->m_nearestGrabbableLedgeId));
			CRY_ASSERT(ledgeInfo.IsValid());

			const bool isWindow = ledgeInfo.AreFlagsSet(kLedgeFlag_isWindow);
			const bool useVault = ledgeInfo.AreAnyFlagsSet(kLedgeFlag_useVault | kLedgeFlag_useHighVault);
			Vec3 pos_rt = player.GetBonePosition("Bip01 Neck");//GetEntity()->GetPos();
			if (pos_rt.IsZero())
			{
				pos_rt = player.GetEntity()->GetPos();
			}

			if (m_mostRecentlyEnteredAction)
			{
				//pos_rt = m_mostRecentlyEnteredAction->orig_loc.t;
			}
			/*const CLedgeManager* pLedgeManager = g_pGame->GetLedgeManager();
			assert(m_ledgeId.IsValid());
			const SLedgeInfo& ledgeInfo = pLedgeManager->GetLedgeById(m_ledgeId);
			assert(ledgeInfo.IsValid());*/
			Matrix34 mtxWorldTm = player.GetEntity()->GetWorldTM();
			pos_rt.z += 0.5f;//mtxWorldTm.GetTranslation().z;
			mtxWorldTm.SetTranslation(pos_rt);
			QuatT new_location_nl = CalculateLedgeOffsetLocation2(mtxWorldTm, Vec3(0, 0, 0), LedgeId(pLedgeState->m_nearestGrabbableLedgeId));
			float dist1 = new_location_nl.t.GetDistance(pos_rt);
			if (dist1 > 0.7f)
				return false;
			
			if (!isWindow)		// Windows always use ledge grabs
			{
				float ledgeHeight = ledgeInfo.GetPosition().z;
				if (expectedJumpEndHeight > (ledgeHeight + g_pGameCVars->g_ledgeGrabClearHeight))
				{
					if (useVault)
					{
						// TODO - this code doesn't seem right.. needs looking at
						float playerZ = startJumpHeight;
						if (playerZ + g_pGameCVars->g_vaultMinHeightDiff > ledgeHeight)
						{
							bDoLedgeGrab = false;
						}
						else
						{
							bDoLedgeGrab = bSprintJump;
						}
					}
					else
					{
						bDoLedgeGrab = false;
					}
				}
			}
		}

		if (bDoLedgeGrab)
		{
			if (player.IsClient())
			{
				const float fHeightofEntity = startJumpHeight;
				CPlayerStateUtil::ApplyFallDamage(player, startJumpHeight, fHeightofEntity);
			}

		}
		return bDoLedgeGrab;
	}

	return false;
}

float CPlayerStateLedge::GetLedgeGrabSpeed( const CPlayer& player ) const
{
	const float kSpeedMultiplier[3] = { GetLedgeGrabbingParams().m_fNormalSpeedUp, GetLedgeGrabbingParams().m_fMobilitySpeedUp, GetLedgeGrabbingParams().m_fMobilitySpeedUpMaximum };

	uint32 speedSelection = 0;
	return kSpeedMultiplier[speedSelection];
}

void CPlayerStateLedge::Serialize( TSerialize serializer )
{
	uint16 ledgeId = m_ledgeId;

	serializer.Value( "LedgeId", ledgeId );
	serializer.Value( "LedgeTransition", m_postSerializeLedgeTransition );

	if(serializer.IsReading())
	{
		m_ledgeId = ledgeId;
	}
}

void CPlayerStateLedge::PostSerialize( CPlayer& player )
{
	// Re-start the ledge grab
	SLedgeTransitionData ledgeData( m_ledgeId );
	ledgeData.m_ledgeTransition = (SLedgeTransitionData::EOnLedgeTransition)m_postSerializeLedgeTransition;
	
	OnEnter( player, SStateEventLedge( ledgeData ) );
}
