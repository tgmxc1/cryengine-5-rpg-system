#ifndef __PLAYERCHARACTERCREATINGSYS_H__
#define __PLAYERCHARACTERCREATINGSYS_H__



class CPlayerCharacterCreatingSys
{
public:
	CPlayerCharacterCreatingSys();
	virtual	~CPlayerCharacterCreatingSys();

	void Init();
	void Update();

	void PlayerFaceSelectionNext();
	void PlayerFaceSelectionPrev();
	void PlayerHairSelectionNext();
	void PlayerHairSelectionPrev();
	void PlayerEyesSelectionNext();
	void PlayerEyesSelectionPrev();
	void PlayerGenderSelectionMale();
	void PlayerGenderSelectionFemale();
	void RemoteActorGenderSelectionMale(EntityId actor_id);
	void RemoteActorGenderSelectionFemale(EntityId actor_id);
	void OnOptionSelected();
	enum EChrGender
	{
		eCG_Male = 0,
		eCG_Female
	};
	void SetFace(int faceId);
	void SetHair(int hairId);
	void SetEyes(int eyesId);
	void SetFaceForRemoteActor(int faceId, EntityId actor_id);
	void SetHairForRemoteActor(int hairId, EntityId actor_id);
	void SetEyesForRemoteActor(int eyesId, EntityId actor_id);
	void OnOpenedCharacterCreationMenu();
	void OnClosedCharacterCreationMenu();
	void SetCharacterName(const char* name);
	string GetCharacterName(bool addXmlExt);
	void ReLoadPlayerCharacterCsetting();
	void ReLoadRemotePlayerCharacterCsetting(EntityId actor_id);
	void SaveCharacterToXmlFile(const char* filename);
	void LoadCharacterfromXmlFile(const char* filename);
	string GetSavedCharactersListPath();
	void OnCharacterCreationCompleteEvent();
	string net_selected_character_name;
	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};

#endif