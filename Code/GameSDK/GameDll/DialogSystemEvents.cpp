#include "StdAfx.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"
#include <CryCore/StlUtils.h>
#include <ctype.h>
#include "IUIDraw.h"
#include "IPlayerInput.h"
#include "IWorldQuery.h"
#include <CryInput/IInput.h>
#include <CryAction/IMaterialEffects.h>
#include "ISaveGame.h"
#include "ILoadGame.h"
#include <CrySystem/File/ICryPak.h>
#include <CryMovie/IMovieSystem.h>
#include "GameRules.h"
#include "Item.h"
#include "Weapon.h"
#include "WeaponSystem.h"
#include "DialogSystemEvents.h"
#include "UI/HUD/HUDCommon.h"
#include "GameXMLSettingAndGlobals.h"
#include "ActorActionsNew.h"


CDialogSystemEvents::CDialogSystemEvents()
{
	//CryLogAlways("CDialogSystemEvents::CDialogSystemEvents() constructor call");
	for (int i = 1; i < MaxQuestions; i++)
	{
		question_is_pressed[i] = false;
	}
	f_time = 0.0f;
	old_ds = 0;
	is_any_question_be_pressed = false;
}

void CDialogSystemEvents::AltUpdate()
{
	if (gEnv->pTimer->GetFrameTime() > 0.05f)
	f_time -= 0.05f;
	else
		f_time -= gEnv->pTimer->GetFrameTime();

	if (f_time < 0)
	{
		for (int i = 1; i < MaxQuestions; i++)
		{
			question_is_pressed[i] = false;
		}
		is_any_question_be_pressed = false;
		f_time += 0.1f;
	}
}

void CDialogSystemEvents::SetPressedId(int id, bool state)
{
	if (id == 0)
		return;

	question_is_pressed[id] = state;
	//if (state)
	//	is_any_question_be_pressed = state;
}

bool CDialogSystemEvents::GetPressedId(int id)
{
	return question_is_pressed[id];
}

bool CDialogSystemEvents::IsAnyQuestionPressedAtThisTime()
{
	return is_any_question_be_pressed;
}

void CDialogSystemEvents::ResetPressedAtThisTime()
{
	is_any_question_be_pressed = false;
}

void CDialogSystemEvents::SetOldDs(int ds)
{
	old_ds = ds;
}

int CDialogSystemEvents::GetOldDs()
{
	return old_ds;
}

void CDialogSystemEvents::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}

void CDialogSystemEvents::ExecEvent(int eventid, int eventid1, int eventid2, int eventid3, int eventid4, int eventid5, int questionid, string m_currentdialogpath)
{
	/*int questid;
	int queststate;
	int questflag;
	int questlast;
	int oldqueststate;
	int oldquestflag;
	int oldquestlast;
	int subquestid;
	int subqueststate;
	int subquestlast;
	int oldsubqueststate;
	int oldsubquestlast;
	int queststageid;
	int dialogstagenum;
	XmlString othernpsname;
	XmlString questpath;
	XmlString questparentpath;
	XmlString itempackname;
	XmlString itemname;
	int itemid;
	XmlString npsname;
	int npsid;
	XmlString followpathname;
	XmlString objectname;
	XmlString animpathnps;
	XmlString animpathplayer;
	int objecttype;
	int objectid;
	XmlString fgfile;
	XmlString scriptfile;
	EntityId id = g_pGameCVars->g_current_dialog_char_id;
	CActor *pActorpl = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	IEntity * pEntity = pActor->GetEntity();
	const char* g_dialog_string;
	if (pActorpl->GetGender() == 0)
		g_dialog_string = "dialogsysdataM";
	else if (pActorpl->GetGender() == 1)
		g_dialog_string = "dialogsysdataF";

	SmartScriptTable props;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);

	const char *dialogpath;
	int species = 0;
	CScriptSetGetChain prop(props);
	prop.GetValue("species", species);
	prop.GetValue(g_dialog_string, dialogpath);
	IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
	if (!pxml)
		return;

	m_currentdialogpath = dialogpath;
	XmlNodeRef node = GetISystem()->LoadXmlFile(m_currentdialogpath.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("xml not loaded.");
		return;
	}

	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef statesNode = node->findChild("states");
	XmlString dialogstringanswere;
	XmlNodeRef state1Node;
	IAIObject *pAIObject = pActorpl->GetEntity()->GetAI();
	state1Node = statesNode->getChild(pActor->GetDialogStage() - 1);
	XmlNodeRef question1Node;
	if (questionid == 1)
		question1Node = state1Node->findChild("question1");
	else if (questionid == 2)
		question1Node = state1Node->findChild("question2");
	else if (questionid == 3)
		question1Node = state1Node->findChild("question3");
	else if (questionid == 4)
		question1Node = state1Node->findChild("question4");
	else if (questionid == 5)
		question1Node = state1Node->findChild("question5");
	else if (questionid == 6)
		question1Node = state1Node->findChild("question6");
	else if (questionid == 7)
		question1Node = state1Node->findChild("question7");
	else if (questionid == 8)
		question1Node = state1Node->findChild("question8");
	else if (questionid == 9)
		question1Node = state1Node->findChild("question9");
	else if (questionid == 10)
		question1Node = state1Node->findChild("question10");
	else if (questionid == 11)
		question1Node = state1Node->findChild("question11");
	else if (questionid == 12)
		question1Node = state1Node->findChild("question12");
	else if (questionid == 13)
		question1Node = state1Node->findChild("question13");
	else if (questionid == 14)
		question1Node = state1Node->findChild("question14");

	if (eventid == 1 || eventid1 == 1 || eventid2 == 1 || eventid3 == 1 || eventid4 == 1 || eventid5 == 1)
	{
		question1Node->getAttr("questpath", questpath);
		question1Node->getAttr("questid", questid);
		question1Node->getAttr("queststate", queststate);
		question1Node->getAttr("questflag", questflag);
		question1Node->getAttr("questlast", questlast);
		g_pGame->GetHUD()->AddQuest(questpath.c_str(), questid, queststate, questflag, questlast);
		g_pGame->GetHUD()->AddQuestlst(questpath);
	}
	if (eventid == 2 || eventid1 == 2 || eventid2 == 2 || eventid3 == 2 || eventid4 == 2 || eventid5 == 2)
	{
		question1Node->getAttr("questpath", questpath);
		question1Node->getAttr("questid", questid);
		g_pGame->GetHUD()->DelQuest(questpath.c_str(), questid);
	}
	if (eventid == 3 || eventid1 == 3 || eventid2 == 3 || eventid3 == 3 || eventid4 == 3 || eventid5 == 3)
	{

		question1Node->getAttr("itempackname", itempackname);

		IXmlParser*	pxml = g_pGame->GetIGameFramework()->GetISystem()->GetXmlUtils()->CreateXmlParser();
		if (!pxml)
			return;

		XmlNodeRef nodep = GetISystem()->LoadXmlFile(itempackname.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("xml not loaded.");
			return;
		}

		CActor  *pActorpl = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
		IItemSystem	*pItemSystemr = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		IInventory *pInventoryr = pActorpl->GetInventory();
		XmlString itemnamer;
		int numitems;
		XmlNodeRef paramsNoder = nodep->findChild("params");
		paramsNoder->getAttr("numitems", numitems);
		if (numitems > 0)
		{
			paramsNoder->getAttr("item01", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 1)
		{
			paramsNoder->getAttr("item02", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 2)
		{
			paramsNoder->getAttr("item03", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 3)
		{
			paramsNoder->getAttr("item04", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 4)
		{
			paramsNoder->getAttr("item05", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 5)
		{
			paramsNoder->getAttr("item06", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 6)
		{
			paramsNoder->getAttr("item07", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 7)
		{
			paramsNoder->getAttr("item08", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 8)
		{
			paramsNoder->getAttr("item09", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 9)
		{
			paramsNoder->getAttr("item10", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
		if (numitems > 10)
		{
			paramsNoder->getAttr("item11", itemnamer);
			pItemSystemr->GiveItem(pActorpl, itemnamer, false, false, false);
		}
	}
	if (eventid == 4 || eventid1 == 4 || eventid2 == 4 || eventid3 == 4 || eventid4 == 4 || eventid5 == 4)
	{
		question1Node->getAttr("itemid", itemid);
		CActor  *pActorpl = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
		EntityId sitem = itemid;
		pActorpl->DropItem(sitem);
		IItemSystem	*pItemSystemr = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
		//pItemSystemr->
		IItem *pItem = pItemSystemr->GetItem(itemid);
		pItem->GetEntity()->Hide(true);
	}
	if (eventid == 11 || eventid1 == 11 || eventid2 == 11 || eventid3 == 11 || eventid4 == 11 || eventid5 == 11)
	{
		question1Node->getAttr("questpath", questpath);
		question1Node->getAttr("questid", questid);
		question1Node->getAttr("queststate", queststate);
		question1Node->getAttr("questflag", questflag);
		question1Node->getAttr("questlast", questlast);
		question1Node->getAttr("oldqueststate", oldqueststate);
		question1Node->getAttr("oldquestflag", oldquestflag);
		question1Node->getAttr("oldquestlast", oldquestlast);
		g_pGame->GetHUDCommon()->UpdateQuest(questpath.c_str(), questid, queststate, questflag, questlast, oldqueststate, oldquestflag, oldquestlast);

	}
	if (eventid == 22 || eventid1 == 22 || eventid2 == 22 || eventid3 == 22 || eventid4 == 22 || eventid5 == 22)
	{
		SAFE_HUD_FUNC(ShowDialogSystem(false));
	}
	if (eventid == 25 || eventid1 == 25 || eventid2 == 25 || eventid3 == 25 || eventid4 == 25 || eventid5 == 25)
	{
		SAFE_HUD_FUNC(ShowTradeSystem(true));
	}
	if (eventid == 26 || eventid1 == 26 || eventid2 == 26 || eventid3 == 26 || eventid4 == 26 || eventid5 == 26)
	{
		question1Node->getAttr("animpathnps", animpathnps);

		EntityId id = g_pGameCVars->g_current_dialog_char_id;
		CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
		CryCharAnimationParams aparams;
		aparams.m_nLayerID = 1;
		aparams.m_fPlaybackSpeed = 1.f;
		aparams.m_nFlags = 0;
		pActor->GetEntity()->GetCharacter(0)->GetISkeletonAnim()->StartAnimation(animpathnps, 0, 0, 0, aparams);
	}
	if (eventid == 16 || eventid1 == 16 || eventid2 == 16 || eventid3 == 16 || eventid4 == 16 || eventid5 == 16)
	{
		CScriptSetGetChain prop(props);
		prop.SetValue("species", (species + 1));
		pActor->Reset(true);
	}
	if (eventid == 7 || eventid1 == 7 || eventid2 == 7 || eventid3 == 7 || eventid4 == 7 || eventid5 == 7)
	{
		pActor->GetEntity()->GetAI()->Following(pAIObject);
	}
	if (eventid == 29 || eventid1 == 29 || eventid2 == 29 || eventid3 == 29 || eventid4 == 29 || eventid5 == 29)
	{

	}
	if (eventid == 30 || eventid1 == 30 || eventid2 == 30 || eventid3 == 30 || eventid4 == 30 || eventid5 == 30)
	{
		question1Node->getAttr("scriptfile", scriptfile);
		gEnv->pScriptSystem->ExecuteFile(scriptfile.c_str());
	}
	if (eventid == 31 || eventid1 == 31 || eventid2 == 31 || eventid3 == 31 || eventid4 == 31 || eventid5 == 31)
	{
		question1Node->getAttr("questparentpath", questparentpath);
		question1Node->getAttr("subquestid", subquestid);
		question1Node->getAttr("subqueststate", subqueststate);
		question1Node->getAttr("subquestlast", subquestlast);
		g_pGame->GetHUD()->AddSubQuest(questparentpath.c_str(), subquestid, subqueststate, subquestlast);
		g_pGame->GetHUD()->AddSubQuestlst(questparentpath);
	}
	if (eventid == 32 || eventid1 == 32 || eventid2 == 32 || eventid3 == 32 || eventid4 == 32 || eventid5 == 32)
	{
		question1Node->getAttr("questparentpath", questparentpath);
		question1Node->getAttr("subquestid", subquestid);
		question1Node->getAttr("subqueststate", subqueststate);
		question1Node->getAttr("questflag", questflag);
		question1Node->getAttr("subquestlast", subquestlast);
		question1Node->getAttr("oldqueststate", oldsubqueststate);
		question1Node->getAttr("oldquestflag", oldquestflag);
		question1Node->getAttr("oldquestlast", oldsubquestlast);
		g_pGame->GetHUD()->UpdateSubQuest(questparentpath.c_str(), questid, queststate, 0, subquestlast, oldsubqueststate, 0, oldsubquestlast);
	}
	if (eventid == 33 || eventid1 == 33 || eventid2 == 33 || eventid3 == 33 || eventid4 == 33 || eventid5 == 33)
	{
		question1Node->getAttr("dialogstagenum", dialogstagenum);
		question1Node->getAttr("othernpsname", othernpsname);
		IEntity* pEntityNp = gEnv->pEntitySystem->FindEntityByName(othernpsname.c_str());
		CActor* pActorNp = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntityNp->GetId()));
		pActorNp->SetDialogStage(dialogstagenum);
	}
	if (eventid == 34 || eventid1 == 34 || eventid2 == 34 || eventid3 == 34 || eventid4 == 34 || eventid5 == 34)
	{
		CActor  *pActorpl = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
		pActorpl->SetGold(pActorpl->GetGold() + 100);
	}*/
}

void CDialogSystemEvents::ExecEventNew(int eventid, int questionid, string m_currentdialogpath, int event_line)
{
	int questid = NULL;
	int queststate = NULL;
	int questflag = NULL;
	int questlast = NULL;
	int oldqueststate = NULL;
	int oldquestflag = NULL;
	int oldquestlast = NULL;
	int subquestid = NULL;
	int subqueststate = NULL;
	int subquestlast = NULL;
	int oldsubqueststate = NULL;
	int oldsubquestlast = NULL;
	int queststageid = NULL;
	int dialogstagenum = NULL;
	XmlString othernpsname = NULL;
	XmlString questpath = NULL;
	XmlString questparentpath = NULL;
	XmlString itempackname = NULL;
	XmlString itemname = NULL;
	int itemid = NULL;
	XmlString npsname = NULL;
	int npsid = NULL;
	XmlString followpathname = NULL;
	XmlString objectname = NULL;
	XmlString animpathnps = NULL;
	XmlString animpathplayer = NULL;
	int objecttype = NULL;
	int objectid = NULL;
	XmlString fgfile = NULL;
	XmlString scriptfile = NULL;
	XmlString consolecmd = NULL;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor *pActorPlayer = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActorPlayer)
		return;

	CActor *pActorForDialog = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pGameGlobals->current_dialog_NPC));
	if (!pActorForDialog)
		return;

	XmlNodeRef node = GetISystem()->LoadXmlFromFile(m_currentdialogpath.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("DialogSystem xml not loaded.");
		return;
	}
	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef statesNode = node->findChild("states");
	XmlNodeRef state1Node;
	string line_cnd = "question";
	string line_event_id = "eventid";
	string line_event_param_app = "_params";
	//IAIObject *pAIObject = pActorPlayer->GetEntity()->GetAI();
	state1Node = statesNode->getChild(pActorForDialog->GetDialogStage() - 1);
	if (!state1Node)
		return;

	XmlNodeRef question1Node;
	char B_valstr[17];
	itoa(questionid, B_valstr, 10);
	string C_valstr = B_valstr;
	string Line_valstr = line_cnd + C_valstr;
	question1Node = state1Node->findChild(Line_valstr.c_str());
	if (!question1Node)
		return;
	
	char B1_valstr[17];
	itoa(event_line, B1_valstr, 10);
	string C1_valstr = B1_valstr;
	string Event_valstr = line_event_id + C1_valstr + line_event_param_app;
	if (event_line == 0)
		Event_valstr = line_event_id + line_event_param_app;

	XmlNodeRef eventNode = question1Node->findChild(Event_valstr.c_str());
	if (!eventNode)
		return;

	switch (eventid)
	{
		case eDET_no_Event:
		{
							  break;
		}
		case eDET_add_Quest:
		{
							   eventNode->getAttr("questpath", questpath);
							   eventNode->getAttr("questid", questid);
							   eventNode->getAttr("queststate", queststate);
							   eventNode->getAttr("questflag", questflag);
							   eventNode->getAttr("questlast", questlast);
							   pGameGlobals->AddQuest(questpath.c_str(), questid, queststate, questflag, questlast);
							   break;
		}
		case eDET_del_Quest:
		{
							   eventNode->getAttr("questpath", questpath);
							   eventNode->getAttr("questid", questid);
							   pGameGlobals->DelQuest(questpath.c_str(), questid);
							   break;
		}
		case eDET_add_Item:
		{
							  eventNode->getAttr("itempackname", itempackname);
							  pActorPlayer->AddAdditionalEquipPermoment(itempackname.c_str());
							  break;
		}
		case eDET_del_item:
		{
							  eventNode->getAttr("itemid", itemid);
							  EntityId sitem = itemid;
							  pActorPlayer->DropItem(sitem);
							  IItemSystem *pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
							  if (pItemSystem)
							  {
								  IItem *pItem = pItemSystem->GetItem(itemid);
								  if (pItem)
								  {
									  pItem->GetEntity()->Hide(true);
								  }
							  }
							  break;
		}
		case eDET_add_NPC_Ally:
		{
								  break;
		}
		case eDET_del_NPC_Ally:
		{
								  break;
		}
		case eDET_NPC_start_FollowToPlayer:
		{
											  //pActor->GetEntity()->GetAI()->Following(pAIObject);
											  break;
		}
		case eDET_NPC_stop_FollowToPlayer:
		{
											 break;
		}
		case eDET_add_NPC_Friend:
		{
									break;
		}
		case eDET_del_NPC_Friend:
		{
									break;
		}
		case eDET_complete_Quest:
		{
									eventNode->getAttr("questpath", questpath);
									eventNode->getAttr("questid", questid);
									eventNode->getAttr("queststate", queststate);
									eventNode->getAttr("questflag", questflag);
									eventNode->getAttr("questlast", questlast);
									eventNode->getAttr("oldqueststate", oldqueststate);
									eventNode->getAttr("oldquestflag", oldquestflag);
									eventNode->getAttr("oldquestlast", oldquestlast);
									pGameGlobals->UpdateQuest(questpath.c_str(), questid, queststate, questflag, questlast, oldqueststate, oldquestflag, oldquestlast);
									break;
		}
		case eDET_complete_QuestStage:
		{
										 break;
		}
		case eDET_fail_Quest:
		{
								break;
		}
		case eDET_fail_QuestStage:
		{
									 break;
		}
		case eDET_update_Quest:
		{
								  break;
		}
		case eDET_add_NPC_Enemy:
		{
								   /*CScriptSetGetChain prop(props);
								   prop.SetValue("species", (species + 1));
								   pActor->Reset(true);*/
								   break;
		}
		case eDET_del_NPC_Enemy:
		{
								   break;
		}
		case eDET_create_Object:
		{
								   break;
		}
		case eDET_remove_Object:
		{
								   break;
		}
		case eDET_activate_Object:
		{
									 break;
		}
		case eDET_deactivate_Object:
		{
									   break;
		}
		case eDET_quot_from_Dialog:
		{
									  //SAFE_HUD_FUNC(ShowDialogSystem(false));
									  break;
		}
		case eDET_NPC_start_FollowOnPath:
		{
											break;
		}
		case eDET_NPC_stop_FollowOnPath:
		{
										   break;
		}
		case eDET_NPC_start_Traiding:
		{
										EntityId id = pGameGlobals->current_dialog_NPC;
										CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
										if (nwAction)
										{
											nwAction->StartTraiding(id);
										}
										break;
		}
		case eDET_play_NPC_Anim:
		{
								   eventNode->getAttr("animpathnps", animpathnps);
								   EntityId id = pGameGlobals->current_dialog_NPC;
								   CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
								   if (!pActor)
									   break;

								   CryCharAnimationParams aparams;
								   aparams.m_nLayerID = 1;
								   aparams.m_fPlaybackSpeed = 1.f;
								   aparams.m_nFlags = 0;
								   pActor->GetEntity()->GetCharacter(0)->GetISkeletonAnim()->StartAnimation(animpathnps, aparams);
								   break;
		}
		case eDET_play_Player_Anim:
		{
									  break;
		}
		case eDET_play_Player_and_NPC_Anim:
		{
											  break;
		}
		case eDET_exec_Flowgraf_Struct:
		{
										  break;
		}
		case eDET_exec_Console_Struct:
		{
										 eventNode->getAttr("consolecmd", consolecmd);
										 if (gEnv->pConsole)
										 {
											 gEnv->pConsole->ExecuteString(consolecmd.c_str(), true);
										 }
		}
		case eDET_exec_Script_Struct:
		{
										eventNode->getAttr("scriptfile", scriptfile);
										if(gEnv->pScriptSystem)
											gEnv->pScriptSystem->ExecuteFile(scriptfile.c_str());

										break;
		}
		case eDET_add_SubQuest:
		{
								  eventNode->getAttr("questparentpath", questparentpath);
								  eventNode->getAttr("subquestid", subquestid);
								  eventNode->getAttr("subqueststate", subqueststate);
								  eventNode->getAttr("subquestlast", subquestlast);
								  pGameGlobals->AddSubQuest(questparentpath.c_str(), subquestid, subqueststate, subquestlast);
								  break;
		}
		case eDET_update_SubQuest:
		{
									 eventNode->getAttr("questparentpath", questparentpath);
									 eventNode->getAttr("subquestid", subquestid);
									 eventNode->getAttr("subqueststate", subqueststate);
									 eventNode->getAttr("questflag", questflag);
									 eventNode->getAttr("subquestlast", subquestlast);
									 eventNode->getAttr("oldqueststate", oldsubqueststate);
									 eventNode->getAttr("oldquestflag", oldquestflag);
									 eventNode->getAttr("oldquestlast", oldsubquestlast);
									 pGameGlobals->UpdateSubQuest(questparentpath.c_str(), questid, queststate, 0, subquestlast, oldsubqueststate, 0, oldsubquestlast);
									 break;
		}
		case eDET_set_NPC_DialogStage:
		{
										 eventNode->getAttr("dialogstagenum", dialogstagenum);
										 eventNode->getAttr("othernpsname", othernpsname);
										 IEntity* pEntityNp = gEnv->pEntitySystem->FindEntityByName(othernpsname.c_str());
										 if (!pEntityNp)
											 break;

										 CActor* pActorNp = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntityNp->GetId()));
										 if (!pActorNp)
											 break;

										 if (dialogstagenum > 0)
											 pActorNp->SetDialogStage(dialogstagenum);

										 break;
		}
		case eDET_add_gold_to_player:
		{
										pActorPlayer->SetGold(pActorPlayer->GetGold() + 100);
										break;
		}

			break;
	}
}