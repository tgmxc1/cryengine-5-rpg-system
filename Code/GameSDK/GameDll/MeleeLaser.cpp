
#include "StdAfx.h"
#include "MeleeLaser.h"
#include "Game.h"
#include "Item.h"
#include "Weapon.h"
#include "GameRules.h"
#include "Player.h"
#include "BulletTime.h"
#include <IEntitySystem.h>
#include "IMaterialEffects.h"
#include "GameCVars.h"
#include "PlayerInput.h"


#include "IRenderer.h"
#include "IRenderAuxGeom.h"	


//std::vector<Vec3> g_points;

//------------------------------------------------------------------------
CMeleeLaser::CMeleeLaser()
{
	m_noImpulse = false;
	m_animspeed_returned = true;
}

//------------------------------------------------------------------------
CMeleeLaser::~CMeleeLaser()
{
}

const float STRENGTH_MULT = 0.018f;

void CMeleeLaser::Init(IWeapon *pWeapon, const struct IItemParamsNode *params)
{
	m_pWeapon = static_cast<CWeapon *>(pWeapon);

	if (params)
		ResetParams(params);

	m_attacking = false;
	m_attacked = false;
	m_delayTimer=0.0f;
	m_durationTimer=0.0f;
	m_nextatkTimer=0.0f;
	m_ignoredEntity = 0;
	m_meleeScale = 1.0f;
	m_nextatk = 0;
	m_updtimer=0;
	m_hitedEntites.clear();
	num_hitedEntites=0;
	m_crt_hit_dmg_mult=1;
	m_updtimer2=0;

	//m_pWeapon->RequireUpdate(eIUS_FireMode);
}

//------------------------------------------------------------------------
void CMeleeLaser::Update(float frameTime, uint frameId)
{
	FUNCTION_PROFILER( GetISystem(), PROFILE_GAME );

	bool requireUpdate = false;

	CActor* pOwner_re = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	if(!pOwner_re || pOwner_re->GetHealth()<=0)
		return;

	/*if(m_updtimer>0.1f)
			{
			m_updtimer-=frameTime;
			if (m_updtimer<=0.1f)
				m_updtimer=0.0f;
			}*/
	
			/*m_updtimer2-=frameTime;
			if (m_updtimer2<=0.0f)
				m_updtimer2=1.0f;*/
			
			

			//if(m_updtimer>0.2)
			//{
				CActor *pActor = m_pWeapon->GetOwnerActor();
					
					
	
					float strength = 1.0f; //pActor->GetActorStrength();
				
							float dmgmult = 0.1f;
							strength = dmgmult/*pActor->GetActorStrength()*/;
				strength = strength * (0.2f + 0.4f * STRENGTH_MULT);
					IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);
					if(str > pActor->GetActorStrength())
					{
						strength *= 1;
					}
					else if(str < 1)
					{
						strength *= 1;
					}
					else if(str == 1)
					{
						strength *= 1;
					}
					strength = GetOwnerStrength();
					IEntityRenderProxy* pRenderProxy = (IEntityRenderProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
		AABB bbox;
		pRenderProxy->GetWorldBounds(bbox);
		IPhysicalWorld *pWorld = gEnv->pSystem->GetIPhysicalWorld();
		IPhysicalEntity **ppColliders;
		Vec3 dir = bbox.max;
		Vec3 pos = bbox.min;
		


		/*IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
	IEntity *pHeldObject = NULL;

	if(m_ignoredEntity)
		pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
	PerformRayTest(pos, dir, strength, false);*/
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);

		if(m_meleeparams.num_sides >= 1)
		{
		if(pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers()==0)
		{
			string helperstart = "weapon";
			string helperend = "weapon_term";
			Vec3 position(0,0,0);
			Vec3 positionl(0,0,0);
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(position);
			
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			IEntity *pOwner = m_pWeapon->GetOwner();
			IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
			IEntity *pHeldObject = NULL;

			if(m_ignoredEntity)
				pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
			PerformRayTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl),(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl))/*m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)*/, strength, false);
			//PerformCylinderTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position), strength, false);
			//PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position), strength, true);
			
			PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl))/*m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)*/, strength, false);
			

		}
		else
		{
			IEntity *pOwner = m_pWeapon->GetOwner();
			IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
			IEntity *pHeldObject = NULL;

			if(m_ignoredEntity)
				pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);

			//PerformCylinderTest(pos, dir, strength, false);
			//PerformRayTest(pos, dir, strength, false);
			//PerformRayTest2(pos, dir, strength, true);
			//PerformRayTest2(pos, dir, strength, false);
			
		}
		}

		if(m_meleeparams.num_sides >= 2)
		{
			if(pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers()==0)
			{
			string helperstart = "weapon1";
			string helperend = "weapon_term1";
			Vec3 position(0,0,0);
			Vec3 positionl(0,0,0);
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1,false).TransformPoint(position);
			
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			IEntity *pOwner = m_pWeapon->GetOwner();
			IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
			IEntity *pHeldObject = NULL;

			if(m_ignoredEntity)
				pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
			PerformRayTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl),(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl))/*m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)*/, strength, false);
			//PerformCylinderTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position), strength, false);
			//PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position), strength, true);
			
			PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)-m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl))/*m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position)*/, strength, false);
			}
		}
			//}
	
	CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());

		if (m_nextatkTimer>0.0f)
		{
			if(m_meleeparams.allow_anim_controlled == 1)
			{
				pOwner->GetAnimatedCharacter()->SetMovementControlMethods(eMCM_Animation,eMCM_Entity);
				if(pOwner->IsPlayer())
				{
					CPlayer* pPlayer = static_cast<CPlayer*>(pOwner);
					pPlayer->SetAnimationControlled(true);
					pPlayer->StartSpecialAction(39,m_nextatkTimer);
				}
				//pOwner->m_stats.animationControlled = true;
			}
			m_nextatkTimer-=frameTime;
			if (m_nextatkTimer<=0.0f)
			{
				m_nextatk=0;
				m_nextatkTimer=0.0f;

				if(m_meleeparams.allow_anim_controlled == 1)
				{
						pOwner->GetAnimatedCharacter()->SetMovementControlMethods(eMCM_Undefined,eMCM_Undefined);
						if(pOwner->IsPlayer())
						{
							CPlayer* pPlayer = static_cast<CPlayer*>(pOwner);
							pPlayer->SetAnimationControlled(false);
							pPlayer->StartSpecialAction(39,m_nextatkTimer);
							//pPlayer->GetPlayerInput()->Reset();
							
						}
				
				}
			}
		}


	//CPlayer *pPlayer = (CPlayer *)pOwner;
	//ICharacterInstance *pOwnerCharacter = pPlayer->GetEntity()->GetCharacter(0);
	

				if(m_durationTimer>0)
				{
					
					/*CActor *pActorr = m_pWeapon->GetOwnerActor();
					IEntityPhysicalProxy *pPhysicsProxy = m_pWeapon->GetPhysicalProxy(true);

					SEntityPhysicalizeParams params;
					params.type = PE_RIGID;
					params.mass = 40.0f;
					//params.nFlagsAND = SC_ACTIVE_RIGID;
					params.density = 1000;
					//params.nSlot = slot;
					params.nSlot = CItem::eIGS_ThirdPerson;
					pPhysicsProxy->Physicalize(params);
			pPhysicsProxy->EnablePhysics(true);
			//pPhysicsProxy->EnableRestrictedRagdoll(true);
			//IEntityPhysicalProxy *pPhysicsProxy = /*GetPhysicalProxy(true)*//*;
			IPhysicalEntity *pPhys = m_pWeapon->GetEntity()->GetPhysics();
			pe_simulation_params sp;;
			sp.iSimClass = SC_ACTIVE_RIGID;
			sp.mass = 100;
			//sp.type = PE_ARTICULATED;
			sp.maxLoggedCollisions = 100;
			pPhys->SetParams(&sp);
			m_pWeapon->GetEntity()->PrePhysicsActivate(true);
			m_pWeapon->GetEntity()->Physicalize(params);
			m_pWeapon->GetEntity()->EnablePhysics(true);
			IStatObj *pStt = m_pWeapon->GetEntity()->GetStatObj(CItem::eIGS_ThirdPerson);
				IPhysicalEntity *pPhysics = m_pWeapon->GetEntity()->GetPhysics();
				//if (pPhysics)
				//{

				//pPhysics->GetWorld()->RayWorldIntersection
					pe_action_awake action;
					action.bAwake = pActorr->GetEntityId()!=0;
					pPhysics->Action(&action);
					pPhysics->SetParams(&sp);
					
					

				//}
			//m_pWeapon->GetPhysicalProxy(true)->EnablePhysics(true);
			//m_pWeapon->GetPhysicalProxy(true)->EnablePhysics(true);
			//m_pWeapon->GetPhysicalProxy(true)->EnableRestrictedRagdoll(true);
			//m_pWeapon->Physicalize(false,false,false);
				IEntityPhysicalProxy *pPhysicsProxys = (IEntityPhysicalProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS);
	//m_pWeapon->GetPhysicalProxy(true);
		AABB bbox;
	pPhysicsProxys->GetWorldBounds( bbox );

	IPhysicalWorld *pWorld = gEnv->pSystem->GetIPhysicalWorld();
	IPhysicalEntity **ppColliders;
	int cnt = pWorld->GetEntitiesInBox( bbox.min,bbox.max, ppColliders,ent_living);
	for (int i = 0; i < cnt; i++)
	{

		IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics( ppColliders[i] );
		if (!pEntity)
			continue;
		
		// skip the vehicle itself
		if (pEntity==m_pWeapon->GetEntity())
			continue;

		IPhysicalEntity *pPhysEnt = pEntity->GetPhysics();
		if (!pPhysEnt) 
			continue;

		IActor* pActor = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());            
		if(!pActor)
			continue;

		/*if(pActor->IsPlayer())
			continue;*/
/*
		if(pActor == m_pWeapon->GetOwnerActor())
			continue;
		/*CGameRules *pGameRules = g_pGame->GetGameRules();

			int damage = m_meleeparams.damage;
			if(pActor && !pActor->IsPlayer())
				damage = m_meleeparams.damageAI;

			HitInfo info(m_pWeapon->GetOwnerId(), pEntity->GetId(), m_pWeapon->GetEntityId(),
				m_pWeapon->GetFireModeIdx(GetName()), 0.0f)/*, pGameRules->GetHitMaterialIdFromSurfaceId(surfaceIdx), partId,*/
				/*pGameRules->GetHitTypeId(m_meleeparams.hit_type.c_str()), pt, dir, normal)*//*;
				info.type = 0;
				info.pos = pActor->GetEntity()->GetWorldPos();
			//pActor->SetHealth(pActor->GetHealth()-10);

			/*info.remote = remote;
			if (!remote)
				info.seq=m_pWeapon->GenerateShootSeqN();*//*
			info.damage = m_meleeparams.damage;

			if (m_pWeapon->GetForcedHitMaterial() != -1)
				info.material=pGameRules->GetHitMaterialIdFromSurfaceId(m_pWeapon->GetForcedHitMaterial());

			pGameRules->ClientHit(info);*/
/*
		IGameRules *pGameRules = g_pGame->GetGameRules();
			//if (pGameRules)
			//{
				/*HitInfo hit;

				//EntityId shooterId=m_pVehicle->GetEntityId();
				//if (m_pVehicle->GetDriver())
					EntityId shooterId=m_pWeapon->GetOwnerActor()->GetEntityId();					

				hit.targetId = pEntity->GetId();      
				hit.shooterId = shooterId;
				hit.weaponId = m_pWeapon->GetEntityId();
				hit.damage = m_meleeparams.damage;
				hit.type = 0;
				hit.pos = pActor->GetEntity()->GetWorldPos();

				pGameRules->ClientHit(hit); */
			//}  
		/*		int damage = m_meleeparams.damage;
			/*if(m_nextatk == 0)
				damage = m_meleeparams.damage;*/
		/*	if(m_nextatk == 1)
				damage = m_meleeparams.damage;
			else if(m_nextatk == 2)
				damage = m_meleeparams.damage1;
			else if(m_nextatk == 3)
				damage = m_meleeparams.damage2;
			else if(m_nextatk == 4)
				damage = m_meleeparams.damage3;
			else if(m_nextatk == 5)
				damage = m_meleeparams.damage4;
			else if(m_nextatk == 6)
				damage = m_meleeparams.damage5;
			else if(m_nextatk == 7)
				damage = m_meleeparams.damage6;
			else if(m_nextatk == 8)
				damage = m_meleeparams.damage7;
			else if(m_nextatk == 9)
				damage = m_meleeparams.damage8;
			else if(m_nextatk == 10)
				damage = m_meleeparams.damage9;

			/*if(pActor && !pActor->IsPlayer())
				damage = m_meleeparams.damageAI;*/

		/*	HitInfo info(m_pWeapon->GetOwnerId(), pEntity->GetId(), m_pWeapon->GetEntityId(),
				m_pWeapon->GetFireModeIdx(GetName()), 1.0f, 1, 1,
				pGameRules->GetHitTypeId(m_meleeparams.hit_type.c_str()), pActor->GetEntity()->GetWorldPos(), pActor->GetEntity()->GetWorldPos(), pActor->GetEntity()->GetWorldPos());
				//info.type = 0;
				//info.pos = pActor->GetEntity()->GetWorldPos();
			//pActor->SetHealth(pActor->GetHealth()-10);
			//pActor->SetHealth(pActor->GetHealth()-100);
			bool remote = false;
			info.remote = remote;
			//if (!remote)
				info.seq=m_pWeapon->GenerateShootSeqN();
			info.damage = damage;//m_meleeparams.damage;

			//if (m_pWeapon->GetForcedHitMaterial() != -1)
				//info.material=pGameRules->GetHitMaterialIdFromSurfaceId(m_pWeapon->GetForcedHitMaterial());

			pGameRules->ClientHit(info);

				float strength = 1.0f;

							

				IMovementController * pMC = pActorr->GetMovementController();
				Vec3 pos(ZERO);
				Vec3 dir(ZERO);
				SMovementState infoo;
				pMC->GetMovementState(infoo);
				pos = infoo.eyePosition;
				dir = infoo.eyeDirection;*/
				//PerformRayTest(pos, dir, strength, false);
				//PerformCylinderTest(pos, dir, strength, false);
				//m_pWeapon->RequestMeleeAttack(m_pWeapon->GetMeleeFireMode()==this, pos, dir, m_pWeapon->GetShootSeqN());
				//} 
					
				}
				else if(m_pWeapon->IsSelected() && m_durationTimer<=0/*m_durationTimer<=0.01f && m_durationTimer>=0.07f*/)
					{
						//IEntityPhysicalProxy *pPhysicsProxy = m_pWeapon->GetPhysicalProxy(false);
						//pPhysicsProxy->EnablePhysics(false);
						//m_pWeapon->GetEntity()->EnablePhysics(false);
						m_pWeapon->Physicalize(false,false,false);
						//m_pWeapon->IsSelected();
					}
				//else
				//m_pWeapon->GetEntity()->EnablePhysics(false);
				//m_pWeapon->Physicalize(false,false,false);
				//m_pWeapon->GetOwner()->

				/*if(m_delayTimer>0.08f /*&& m_delayTimer!<0.01*//*)
				{
					ICharacterInstance *pOwnerCharacter = pOwner->GetEntity()->GetCharacter(0);
					pOwnerCharacter->SetAnimationSpeed(m_meleeparams.animspdmult);
					//m_pWeapon->Physicalize(false,false,true);
				}
				else if(m_delayTimer>=0.01f && m_delayTimer<=0.07f)
				{
					ICharacterInstance *pCharacter = pOwner->GetEntity()->GetCharacter(0);
					pCharacter->SetAnimationSpeed(1);
					m_pWeapon->Physicalize(false,false,false);
				}*/
				if(m_nextatkTimer>0.32f*3.5f)
				{
					ICharacterInstance *pOwnerCharacter = pOwner->GetEntity()->GetCharacter(0);
					pOwnerCharacter->SetAnimationSpeed(m_meleeparams.animspdmult);
					//m_pWeapon->Physicalize(false,false,true);
					m_animspeed_returned = false;
				}
				else if(m_nextatkTimer>=0.01f*3.5f && m_nextatkTimer<=0.31f*3.5f)
				{
					ICharacterInstance *pCharacter = pOwner->GetEntity()->GetCharacter(0);
					pCharacter->SetAnimationSpeed(1);
					m_pWeapon->Physicalize(false,false,false);
					m_animspeed_returned = true;

					/*if(m_meleeparams.allow_anim_controlled == 1)
					{
						pOwner->GetAnimatedCharacter()->SetMovementControlMethods(eMCM_Undefined,eMCM_Undefined);
						if(pOwner->IsPlayer())
						{
							CPlayer* pPlayer = static_cast<CPlayer*>(pOwner);
							pPlayer->SetAnimationControlled(false);
							//pPlayer->GetPlayerInput()->Reset();
							
						}
				//pOwner->m_stats.animationControlled = true;
					}*/
					//m_hitedEntites.clear();
				}
				else if(!m_animspeed_returned && m_nextatkTimer<=0)
				{
					ICharacterInstance *pCharacter = pOwner->GetEntity()->GetCharacter(0);
					pCharacter->SetAnimationSpeed(1);
					m_animspeed_returned = true;

					/*if(m_meleeparams.allow_anim_controlled == 1)
					{
						//pOwner->GetAnimatedCharacter()->SetMovementControlMethods(eMCM_DecoupledCatchUp,eMCM_DecoupledCatchUp);
						if(pOwner->IsPlayer())
						{
							CPlayer* pPlayer = static_cast<CPlayer*>(pOwner);
							pPlayer->SetAnimationControlled(false);
							pPlayer->GetPlayerInput()->Reset();
							
						}
				//pOwner->m_stats.animationControlled = true;
					}*/

				}
				/*else if(m_delayTimer<=0)
				{
					ICharacterInstance *pCharacter = pOwner->GetEntity()->GetCharacter(0);
					pCharacter->SetAnimationSpeed(1);
				}*/
	
	if (m_attacking)
	{
		requireUpdate = true;
		

		if (m_delayTimer>0.0f)
		{


			m_delayTimer-=frameTime;
			if (m_delayTimer<=0.0f)
				m_delayTimer=0.0f;
		}
		else
		{
			

			if (!m_attacked)
			{
				m_attacked = true;

				/*CActor *pActor = m_pWeapon->GetOwnerActor();
				if(!pActor)
					return;

				Vec3 pos(ZERO);
				Vec3 dir(ZERO);
				IMovementController * pMC = pActor->GetMovementController();
				if (!pMC)
					return;

				float strength = 1.0f; //pActor->GetActorStrength();
				//if (pActor->GetActorClass() == CPlayer::GetActorClassType())
				//{
					/*CPlayer *pPlayer = (CPlayer *)pActor;
					if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
					{*/
						/*ENanoMode curMode = pSuit->GetMode();*/
						/*//if (gEnv->bClient)
						//{
							//float dmgmult = 0.1f;
							//strength = dmgmult/*pActor->GetActorStrength()*/;
							//strength = strength * (0.1f + 0.2f * gEnv->bClient* STRENGTH_MULT);

							//if(!pPlayer->IsPlayer() && g_pGameCVars->g_difficultyLevel < 4)
											/*IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);*/


				//strength *= /*str*/pActor->GetActorStrength()/*g_pGameCVars->g_strength*/;
								//strength *= pActor->GetActorStrength()/*g_pGameCVars->g_strength*/;
				//}
				//else
				//{
			/*				float dmgmult = 0.1f;
							strength = dmgmult/*pActor->GetActorStrength()*//*;
				strength = strength * (0.2f + 0.4f * gEnv->bClient*STRENGTH_MULT);
					IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);
					if(str > pActor->GetActorStrength())
					{
						strength *= str;
					}
					else if(str < pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
					else if(str == pActor->GetActorStrength())
					{
						strength *= pActor->GetActorStrength();
					}
				//}
						//}
					//}
				//}

				SMovementState info;
				pMC->GetMovementState(info);
				pos = info.eyePosition;
				dir = info.eyeDirection;
				/*if(!PerformRayTest(pos, dir, strength, false))
					if(!PerformCylinderTest(pos, dir, strength, true))*//*
					/*PerformRayTest(pos, dir, strength, false);//)
					PerformCylinderTest(pos, dir, strength, true);//)
					PerformRayTest(pos, dir, strength, false);//)
					PerformCylinderTest(pos, dir, strength, true);//)
					PerformRayTest(pos, dir, strength, false);//)
					PerformCylinderTest(pos, dir, strength, true);//)*/
					/*ApplyCameraShake(false);/

				m_ignoredEntity = 0;
				m_meleeScale = 1.0f;*/

				m_ignoredEntity = 0;
				m_meleeScale = 1.0f;
				
				m_pWeapon->RequestMeleeAttack(m_pWeapon->GetMeleeFireMode()==this, pos, dir, m_pWeapon->GetShootSeqN());
			}
		}
	}
	//m_pWeapon->GetPhysicalProxy(true)->Update();
	//if (requireUpdate)
		//m_pWeapon->RequireUpdate(eIUS_FireMode);
}

//------------------------------------------------------------------------
void CMeleeLaser::Release()
{
	delete this;
}

//------------------------------------------------------------------------
void CMeleeLaser::ResetParams(const struct IItemParamsNode *params)
{
	const IItemParamsNode *melee = params?params->GetChild("melee"):0;
	const IItemParamsNode *actions = params?params->GetChild("actions"):0;

	m_meleeparams.Reset(melee);
	m_meleeactions.Reset(actions);
}

//------------------------------------------------------------------------
void CMeleeLaser::PatchParams(const struct IItemParamsNode *patch)
{
	const IItemParamsNode *melee = patch->GetChild("melee");
	const IItemParamsNode *actions = patch->GetChild("actions");

	m_meleeparams.Reset(melee, false);
	m_meleeactions.Reset(actions, false);
}

//------------------------------------------------------------------------
void CMeleeLaser::Activate(bool activate)
{
	m_attacking = m_noImpulse = false;
	m_delayTimer=0.0f;
	m_durationTimer=0.0f;
	m_nextatkTimer=0.0f;
}

//------------------------------------------------------------------------
bool CMeleeLaser::CanFire(bool considerAmmo) const
{
	return !m_attacking;
}

//------------------------------------------------------------------------
struct CMeleeLaser::StopAttackingAction
{
	

	CMeleeLaser *_this;
	StopAttackingAction(CMeleeLaser *melee): _this(melee) {};
	void execute(CItem *pItem)
	{
		_this->m_attacking = false;

		_this->m_delayTimer = 0.0f;
		_this->m_durationTimer = 0.0f;
		//_this->m_nextatkTimer = 0.0f;
		pItem->SetBusy(false);
		
		// this allows us to blend into the idle animation (for swimming) -- johnn
		//pItem->ResetAnimation();
		//pItem->ReAttachAccessories();
		//pItem->PlayAction(ItemStrings::idle, 0, false, CItem::eIPAF_CleanBlending | CItem::eIPAF_NoBlend | CItem::eIPAF_Default);
		//pItem->PlayAction(pItem->GetDefaultIdleAnimation(0), 0, false, CItem::eIPAF_CleanBlending | CItem::eIPAF_Default);

	}

	

};

void CMeleeLaser::StartFire()
{
	if (!CanFire())
		return;

	//Prevent fists melee exploit 
	if ((m_pWeapon->GetEntity()->GetClass() == CItem::sFistsClass) && m_pWeapon->IsBusy())
		return;

	CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());

	if(pOwner)
	{
		if(pOwner->GetStance()==STANCE_PRONE)
			return;

		/*if(SPlayerStats* stats = static_cast<SPlayerStats*>(pOwner->GetActorStats()))
		{
			if(stats->bLookingAtFriendlyAI)
				return;
		}*/
	}
	num_hitedEntites=0;
	m_hitedEntites.clear();
	m_attacking = true;
	m_attacked = false;
	if(/*m_meleeparams.number_combohits>1 && */m_meleeparams.number_combohits != m_nextatk)
	{
		m_nextatk+=1;
	}
	
	m_pWeapon->RequireUpdate(eIUS_FireMode);
	m_pWeapon->ExitZoom();

	bool isClient = pOwner?pOwner->IsClient():false;

	if (g_pGameCVars->bt_end_melee && isClient)
		g_pGame->GetBulletTime()->Activate(false);


	float speedOverride = -1.0f;

	if(CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor()))
	{
		CPlayer *pPlayer = (CPlayer *)pOwner;
		//if(CNanoSuit *pSuit = pPlayer->GetNanoSuit())
		//{
			/*ENanoMode curMode = pSuit->GetMode();*/
			if (/*gEnv->bClient && */pPlayer->GetStamina() > 15.0f)//NANOSUIT_STMN * 1.1f)
				speedOverride = 1.5f*m_meleeparams.atkspdmult;
				speedOverride=g_pGameCVars->g_playerItemAtkSpeed*m_meleeparams.atkspdmult;
			if (pPlayer->GetStamina() < 15.0f)
				speedOverride = 0.8f*m_meleeparams.atkspdmult2;
		//}
		
		pPlayer->PlaySound(CPlayer::ESound_Melee);
	}

	
	if(m_nextatk == 1)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay;
		m_updtimer = m_meleeparams.ray_test_time;
		m_durationTimer = m_meleeparams.duration;
		m_nextatkTimer = m_meleeparams.duration*3.5f;
	}
	/*else if(m_nextatk == 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 2 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack1.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay1;
		m_updtimer = m_meleeparams.ray_test_time1;
		m_durationTimer = m_meleeparams.duration1;
		m_nextatkTimer = m_meleeparams.duration1*3.5f;
	}
	/*else if(m_nextatk == 2  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 3 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack2.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay2;
		m_updtimer = m_meleeparams.ray_test_time2;
		m_durationTimer = m_meleeparams.duration2;
		m_nextatkTimer = m_meleeparams.duration2*3.5f;
	}
	/*else if(m_nextatk == 3  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 4 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack3.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay3;
		m_updtimer = m_meleeparams.ray_test_time3;
		m_durationTimer = m_meleeparams.duration3;
		m_nextatkTimer = m_meleeparams.duration3*3.5f;
	}
	/*else if(m_nextatk == 4  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 5 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack4.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay4;
		m_updtimer = m_meleeparams.ray_test_time4;
		m_durationTimer = m_meleeparams.duration4;
		m_nextatkTimer = m_meleeparams.duration4*3.5f;
	}
	/*else if(m_nextatk == 5  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 6 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack5.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay5;
		m_updtimer = m_meleeparams.ray_test_time5;
		m_durationTimer = m_meleeparams.duration5;
		m_nextatkTimer = m_meleeparams.duration5*3.5f;
	}
	/*else if(m_nextatk == 6  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 7 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack6.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay6;
		m_updtimer = m_meleeparams.ray_test_time6;
		m_durationTimer = m_meleeparams.duration6;
		m_nextatkTimer = m_meleeparams.duration6*3.5f;
	}
	/*else if(m_nextatk == 7  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 8 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack7.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay7;
		m_updtimer = m_meleeparams.ray_test_time7;
		m_durationTimer = m_meleeparams.duration7;
		m_nextatkTimer = m_meleeparams.duration7*3.5f;
	}
	/*else if(m_nextatk == 8  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 9 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack8.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay8;
		m_updtimer = m_meleeparams.ray_test_time8;
		m_durationTimer = m_meleeparams.duration8;
		m_nextatkTimer = m_meleeparams.duration8*3.5f;
	}
	/*else if(m_nextatk == 9  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	else if(m_nextatk == 10 && m_nextatkTimer > 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack9.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
		m_delayTimer = m_meleeparams.delay9;
		m_updtimer = m_meleeparams.ray_test_time9;
		m_durationTimer = m_meleeparams.duration9;
		m_nextatkTimer = m_meleeparams.duration9*3.5f;
	}
	/*else if(m_nextatk == 10  && m_nextatkTimer <= 0)
	{
		m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default|CItem::eIPAF_CleanBlending, speedOverride);
	}*/
	m_pWeapon->SetBusy(true);

	//ICharacterInstance *pOwnerCharacter = pOwner->GetAnimatedCharacter()->GetEntity()->GetCharacter(0);
	//ISkeletonAnim *pOwnerSkl = pOwnerCharacter->GetISkeletonAnim();
	//pOwnerCharacter->GetIAnimationSet()->
	//pOwnerCharacter->GetAnimationSpeed();

	m_beginPos = m_pWeapon->GetSlotHelperPos(CItem::eIGS_FirstPerson, m_meleeparams.helper.c_str(), true); 
	//CAnimation& animation=pOwnerSkl->GetAnimFromFIFO(0,0); 
	//uint time;

	//time = animation.m_fAnimTime;

	m_pWeapon->GetScheduler()->TimerAction(m_pWeapon/*pOwner->GetAnimatedCharacter()->GetAnimationGraph*/->GetCurrentAnimationTime(CItem::eIGS_FirstPerson), CSchedulerAction<StopAttackingAction>::Create(this), true);


	/*m_delayTimer = m_meleeparams.delay;
	m_updtimer = m_meleeparams.ray_test_time;
	
	if (g_pGameCVars->dt_enable && m_delayTimer < g_pGameCVars->dt_time)
		m_delayTimer = g_pGameCVars->dt_time;

	m_durationTimer = m_meleeparams.duration;
	m_nextatkTimer = m_meleeparams.duration*3.5f;*/


	

	m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());

	m_pWeapon->RequestStartMeleeAttack(m_pWeapon->GetMeleeFireMode()==this);
	//m_pWeapon->Physicalize(true,false);
}

//------------------------------------------------------------------------
void CMeleeLaser::StopFire()
{
	
	//m_pWeapon->Physicalize(false,false);
}

//------------------------------------------------------------------------
void CMeleeLaser::NetStartFire()
{
	/*m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());

	float speedOverride = -1.0f;

	if(CActor* pOwner = m_pWeapon->GetOwnerActor())
	{
		CPlayer *pPlayer = (CPlayer *)pOwner;
		if(CNanoSuit *pSuit = pPlayer->GetNanoSuit())
		{
			/*ENanoMode curMode = pSuit->GetMode();*//*
			if (gEnv->bClient)
				speedOverride = 1.5f;
				speedOverride=g_pGameCVars->g_playerItemAtkSpeed;
		}
	}

	m_pWeapon->PlayAction(m_meleeactions.attack.c_str(), 0, false, CItem::eIPAF_Default, speedOverride);*/
}

//------------------------------------------------------------------------
void CMeleeLaser::NetStopFire()
{
}

//------------------------------------------------------------------------
void CMeleeLaser::NetShoot(const Vec3 &hit, int ph)
{
}

//------------------------------------------------------------------------
void CMeleeLaser::NetShootEx(const Vec3 &pos, const Vec3 &dir, const Vec3 &vel, const Vec3 &hit, float extra, int ph)
{
	float strength=GetOwnerStrength();

	PerformRayTest(pos, dir, strength, false);
		PerformCylinderTest(pos, dir, strength, true);
			ApplyCameraShake(true);

	m_ignoredEntity = 0;
	m_meleeScale = 1.0f;
}

//------------------------------------------------------------------------
const char *CMeleeLaser::GetType() const
{
	return "Melee";
}

//------------------------------------------------------------------------
int CMeleeLaser::GetDamage(float distance) const
{
	// NOTE: in multiplayer m_meleeScale == 1.0,
	// however this is not a problem since we cannot pick up objects in that scenario
	//int damage = m_meleeparams.damage;
			if(m_nextatk == 0)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
			}
			else if(m_nextatk == 1)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 2)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage1 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage1*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 3)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage2 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage2*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 4)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage3 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage3*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 5)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage4 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage4*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 6)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage5 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage5*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 7)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage6 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage6*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 8)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage7 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage7*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 9)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage8 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage8*GetOwnerStrength()*m_meleeScale;
			}
			else if(m_nextatk == 10)
			{
				if(num_hitedEntites==0)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==1)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==2)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==3)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites==4)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
			else if(num_hitedEntites>4)
				return (((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	//return m_meleeparams.damage*GetOwnerStrength()*m_meleeScale;
			return ((m_meleeparams.damage9 + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
				//return m_meleeparams.damage9*GetOwnerStrength()*m_meleeScale;
			}
	
			return ((m_meleeparams.damage + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
}

//------------------------------------------------------------------------
const char *CMeleeLaser::GetDamageType() const
{
	return m_meleeparams.hit_type.c_str();
}


//------------------------------------------------------------------------
bool CMeleeLaser::PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{


	//gEnv->p3DEngine->AddWaterSplash(pos,EST_Water,1000, 1);

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;

	 IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200,0,0,128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	
	ray_hit hit;
	int n; 
	if(pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*m_meleeparams.range/*.normalized()*m_meleeparams.range*/, ent_living|ent_sleeping_rigid|ent_rigid|ent_independent/*ent_all|ent_water*/,
			rwi_colltype_any|rwi_stop_at_pierceable/*rwi_stop_at_pierceable|rwi_ignore_back_faces*/,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
	}
	else if(!pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*m_meleeparams.range/*.normalized()*m_meleeparams.range*/, ent_living|ent_sleeping_rigid|ent_rigid|ent_independent,
			rwi_colltype_any|rwi_stop_at_pierceable/*rwi_stop_at_pierceable|rwi_ignore_back_faces*/,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		//pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		//gEnv->pRenderer->
	}

	//===================OffHand melee (also in PerformCylincerTest)===================
	if(m_ignoredEntity && (n>0))
	{
		if(IEntity* pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity))
		{
			IPhysicalEntity *pHeldObjectPhysics = pHeldObject->GetPhysics();
			if(pHeldObjectPhysics==hit.pCollider)
				return false;
		}
	}

	if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	//=================================================================================

	if (n>0)
	{
		Impulse(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
		Hit(&hit, dir, strength, remote);
	}

	return n>0;
}

bool CMeleeLaser::PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{


	//gEnv->p3DEngine->AddWaterSplash(pos,EST_Water,1000, 1);

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;

	 IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200,0,0,128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	
	ray_hit hit;
	int n; 
	if(pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*m_meleeparams.range/*.normalized()*m_meleeparams.range*/, ent_static|ent_terrain|ent_water/*ent_all|ent_water*/,
			rwi_colltype_any|rwi_stop_at_pierceable/*|rwi_ignore_back_faces*/,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
	}
	else if(!pOwneract->IsPlayer())
	{
		n =gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*m_meleeparams.range/*.normalized()*m_meleeparams.range*/,  ent_static|ent_terrain|ent_water,
			rwi_colltype_any|rwi_stop_at_pierceable/*|rwi_ignore_back_faces*/,&hit, 1, &pIgnore, pIgnore?1:0);

		if(g_pGameCVars->i_debug_melee_combat_systems == 1)
			pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		//pAuxGeom->DrawLine(pos,colNormal,dir,colNormal, 5);
		//gEnv->pRenderer->
	}

	//===================OffHand melee (also in PerformCylincerTest)===================
	/*if(m_ignoredEntity && (n>0))
	{
		if(IEntity* pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity))
		{
			IPhysicalEntity *pHeldObjectPhysics = pHeldObject->GetPhysics();
			if(pHeldObjectPhysics==hit.pCollider)
				return false;
		}
	}*/
	if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	//=================================================================================

	if (n>0)
	{
		if(remote)
		{
			//Impulse2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
		}
		else
		{
			Impulse2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
			Hit2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength, remote);
		}
		//Hit(&hit, dir, strength, remote);
		//Hit2(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength, remote);
		//Impulse(hit.pt, dir, hit.n, hit.pCollider, hit.partid, hit.ipart, hit.surface_idx, strength);
	}

	return n>0;
}

void CMeleeLaser::Hit2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote)
{
	
		IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();

		TMFXEffectId effectId = pMaterialEffects->GetEffectId("melee", surfaceIdx);
		if (effectId != InvalidEffectId)
		{
			//pMaterialEffects->StopEffect(effectId);
			SMFXRunTimeEffectParams params;
			params.pos = pt;
			params.playflags = MFX_PLAY_ALL | MFX_DISABLE_DELAY;
			params.soundSemantic = eSoundSemantic_Player_Foley;
			pMaterialEffects->ExecuteEffect(effectId, params);
		}
		if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	//}

	ApplyCameraShake(true);
	
	m_pWeapon->PlayAction(m_meleeactions.hit.c_str());
	if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
}

//------------------------------------------------------------------------
bool CMeleeLaser::PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	/*IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
	IEntity *pHeldObject = NULL;

	if(m_ignoredEntity)
		pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
	primitives::cylinder cyl;
	cyl.r = 0.25f;
	cyl.axis = dir;
	cyl.hh = m_meleeparams.range/2.0f;
	cyl.center = pos + dir.normalized()*cyl.hh;
	
	float n = 0.0f;
	geom_contact *contacts;
	intersection_params params;
	params.bSweepTest = true;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = false;
	//params.
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO), 
		ent_rigid|ent_sleeping_rigid|ent_independent|ent_static|ent_terrain|ent_water, &contacts, 0,
		geom_colltype0|geom_colltype_foliage|geom_colltype_player, &params, 0, 0, &pIgnore, pIgnore?1:0);

	int ret = (int)n;

	float closestdSq = 9999.0f;
	geom_contact *closestc = 0;
	geom_contact *currentc = contacts;

	for (int i=0; i<ret; i++)
	{
		geom_contact *contact = currentc;
		if (contact)
		{
			IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->id[0]);
			if (pCollider)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				if (pEntity)
				{
					if ((pEntity == pOwner)||(pHeldObject && (pEntity == pHeldObject)))
					{
						++currentc;
						continue;
					}
				}

				float distSq = (pos-currentc->pt).len2();
				if (distSq < closestdSq)
				{
					closestdSq = distSq;
					closestc = contact;
				}
			}
		}
		++currentc;
	}

	if (ret)
	{
		WriteLockCond lockColl(*params.plock, 0);
		lockColl.SetActive(1);
	}

  
	if (closestc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->id[0]);
		
		//gEnv->pPhysicalWorld->GetEntityCount(currentc->id[1]);

		Hit(closestc, dir, strength, remote);
		Impulse(closestc->pt, dir, closestc->n, pCollider, closestc->iPrim[1], 0, closestc->id[1], strength);
	}

	return closestc!=0;*/
	/*if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}*/

		IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
	IEntity *pHeldObject = NULL;

	if(m_ignoredEntity)
		pHeldObject = gEnv->pEntitySystem->GetEntity(m_ignoredEntity);
		
	primitives::cylinder cyl;
	cyl.r = 0.25f;
	cyl.axis = dir;
	cyl.hh = m_meleeparams.range/2.0f;
	cyl.center = pos + dir.normalized()*cyl.hh;
	
	float n = 0.0f;
	geom_contact *contacts;
	intersection_params params;
	params.bStopAtFirstTri = true;
	params.bNoBorder = true;
	params.bNoAreaContacts = true;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO), 
		ent_rigid|ent_sleeping_rigid|ent_independent|ent_static|ent_terrain|ent_water|ent_living, &contacts, 0,
		0, &params, 0, 0, &pIgnore, pIgnore?1:0);

	int ret = (int)n;

	float closestdSq = 9999.0f;
	geom_contact *closestc = 0;
	geom_contact *currentc = contacts;

	for (int i=0; i<ret; i++)
	{
		geom_contact *contact = currentc;
		if (contact)
		{
			IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
			if (pCollider)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				if (pEntity)
				{
					if ((pEntity == pOwner)||(pHeldObject && (pEntity == pHeldObject)))
					{
						++currentc;
						continue;
					}
				}

				float distSq = (pos-currentc->pt).len2();
				if (distSq < closestdSq)
				{
					closestdSq = distSq;
					closestc = contact;
				}
			}
		}
		++currentc;
	}

	if (ret)
	{
		WriteLockCond lockColl(*params.plock, 0);
		lockColl.SetActive(1);
	}

  
	if (closestc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);

		Hit(closestc, dir, strength, remote);
		//Impulse(closestc->pt, dir, closestc->n, pCollider, closestc->iPrim[1], 0, closestc->id[1], strength);
	}
	else
	{
		if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	}

	return closestc!=0;

}

//------------------------------------------------------------------------
void CMeleeLaser::Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote)
{
	// generate the damage
	IEntity *pTarget = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);

	//std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
	//std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
	//int count = m_hitedEntites.size()/*sizeof(spells)*/;
				
	//for(/*; it!=end; ++it*/int i = 0; i < count, it!=end; i++ , ++it)
	//{
	//	if(pTarget->GetId() == (*it))
	//	{
	//		return;
	//	}
	//}
	CActor *pActor = m_pWeapon->GetOwnerActor();

	

	int rnd_value1 = cry_rand() % 100+1;

	if(m_pWeapon->GetWeaponType()==1)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_swords_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_swords_ml;
	}
	else if(m_pWeapon->GetWeaponType()==2)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_axes_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_axes_ml;
	}
	else if(m_pWeapon->GetWeaponType()==3)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	else if(m_pWeapon->GetWeaponType()==4)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thswords_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thswords_ml;
	}
	else if(m_pWeapon->GetWeaponType()==5)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thaxes_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thaxes_ml;
	}
	else if(m_pWeapon->GetWeaponType()==6)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	else if(m_pWeapon->GetWeaponType()==7)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_poles_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_poles_ml;
	}
	else if(m_pWeapon->GetWeaponType()==8)
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thpoles_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thpoles_ml;
	}
	else
	{
		if(rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
			m_crt_hit_dmg_mult=m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
	}
	
	//pTarget->
	// Report punch to AI system.
	// The AI notification must come before the game rules are 
	// called so that the death handler in AIsystem understands that the hit
	// came from the player.
	//CActor *pActor = m_pWeapon->GetOwnerActor();
	if (pActor && pActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer *pPlayer = (CPlayer *)pActor;
		if (pPlayer && pPlayer->GetNanoSuit())
		{
			if (pPlayer->GetEntity() && pPlayer->GetEntity()->GetAI())
			{
				SAIEVENT AIevent;
				AIevent.targetId = pTarget ? pTarget->GetId() : 0;
				//pPlayer->GetNanoSuit()->GetMode() == NANOMODE_LAST
				pPlayer->GetEntity()->GetAI()->Event(AIEVENT_PLAYER_STUNT_PUNCH, &AIevent);
			}
		}
		
	}

	bool ok = true;
	if(pTarget)
	{
		if(!gEnv->bMultiplayer && pActor && pActor->IsPlayer())
		{
			IActor* pAITarget = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
			if(pAITarget && pTarget->GetAI() && pTarget->GetAI()->IsHostile(pActor->GetEntity()->GetAI(),false))
			{
				/*ok = false;
				m_noImpulse = true;*/
				//pAITarget->
				CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
				CActor  *pActor  = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if(pActor->GetHealth() > 0)
				{
					int lvl = pActor->GetLevel();
					//if(lvl == 1)
					pClient->SetLevelXp(pClient->GetLevelXp() + lvl * m_meleeparams.damage * 0.03);
					

				}

				/*pActor->GetLevel*/
			}

			if(pAITarget && pAITarget == pActor)
			{
				return;
			}
		}

		if(ok)
		{
			//num_hitedEntites+=1;
			CGameRules *pGameRules = g_pGame->GetGameRules();


			if(pActor->IsPlayer() && m_crt_hit_dmg_mult>1)
			{
				g_pGame->GetHUD()->TextMessage("Critical Hit!");
			}

			int damage = m_meleeparams.damage;
			//if(m_nextatk == 0)
				//damage = m_meleeparams.damage;
			if(m_nextatk == 1)
				damage = m_meleeparams.damage;
			else if(m_nextatk == 2)
				damage = m_meleeparams.damage1;
			else if(m_nextatk == 3)
				damage = m_meleeparams.damage2;
			else if(m_nextatk == 4)
				damage = m_meleeparams.damage3;
			else if(m_nextatk == 5)
				damage = m_meleeparams.damage4;
			else if(m_nextatk == 6)
				damage = m_meleeparams.damage5;
			else if(m_nextatk == 7)
				damage = m_meleeparams.damage6;
			else if(m_nextatk == 8)
				damage = m_meleeparams.damage7;
			else if(m_nextatk == 9)
				damage = m_meleeparams.damage8;
			else if(m_nextatk == 10)
				damage = m_meleeparams.damage9;

			/*if(pActor && !pActor->IsPlayer())
				damage = m_meleeparams.damageAI;*/

			HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
				m_pWeapon->GetFireModeIdx(GetName()), 0.0f, pGameRules->GetHitMaterialIdFromSurfaceId(surfaceIdx), partId,
				pGameRules->GetHitTypeId(m_meleeparams.hit_type.c_str()), pt, dir, normal);

			info.remote = remote;
			if (!remote)
				info.seq=m_pWeapon->GenerateShootSeqN();

			//if(m_nextatk == 0)
				info.damage = damage;
			/*if(m_nextatk == 1)
				info.damage = m_meleeparams.damage;
			else if(m_nextatk == 2)
				info.damage = m_meleeparams.damage1;
			else if(m_nextatk == 3)
				info.damage = m_meleeparams.damage2;
			else if(m_nextatk == 4)
				info.damage = m_meleeparams.damage3;
			else if(m_nextatk == 5)
				info.damage = m_meleeparams.damage4;
			else if(m_nextatk == 6)
				info.damage = m_meleeparams.damage5;
			else if(m_nextatk == 7)
				info.damage = m_meleeparams.damage6;
			else if(m_nextatk == 8)
				info.damage = m_meleeparams.damage7;
			else if(m_nextatk == 9)
				info.damage = m_meleeparams.damage8;
			else if(m_nextatk == 10)
				info.damage = m_meleeparams.damage9;*/
			

			if (m_pWeapon->GetForcedHitMaterial() != -1)
				info.material=pGameRules->GetHitMaterialIdFromSurfaceId(m_pWeapon->GetForcedHitMaterial());

			pGameRules->ClientHit(info);
			//m_hitedEntites.push_back(pTarget->GetId());
			m_crt_hit_dmg_mult=1.0f;
			//m_pWeapon->Physicalize(false,false,true);
			if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
		}
	}



	/*IEntityPhysicalProxy *pPhysicsProxys = m_pWeapon->GetPhysicalProxy(true);

					SEntityPhysicalizeParams params;
					params.type = PE_RIGID;
					params.mass = 40.0f;
					//params.nFlagsAND = SC_ACTIVE_RIGID;
					params.density = 1000;
					//params.nSlot = slot;
					params.nSlot = CItem::eIGS_ThirdPerson;
					pPhysicsProxys->Physicalize(params);
			pPhysicsProxys->EnablePhysics(true);
			//pPhysicsProxys->EnableRestrictedRagdoll(true);
			//IEntityPhysicalProxy *pPhysicsProxy = /*GetPhysicalProxy(true)*//*;
			IPhysicalEntity *pPhys = m_pWeapon->GetEntity()->GetPhysics();
			pe_simulation_params sp;;
			sp.iSimClass = SC_ACTIVE_RIGID;
			sp.mass = 100;
			//sp.type = PE_ARTICULATED;
			sp.maxLoggedCollisions = 100;
			pPhys->SetParams(&sp);
			m_pWeapon->GetEntity()->PrePhysicsActivate(true);
			m_pWeapon->GetEntity()->Physicalize(params);
			m_pWeapon->GetEntity()->EnablePhysics(true);
			IStatObj *pStt = m_pWeapon->GetEntity()->GetStatObj(CItem::eIGS_ThirdPerson);
				IPhysicalEntity *pPhysics = m_pWeapon->GetEntity()->GetPhysics();
				//if (pPhysics)
				//{

				//pPhysics->GetWorld()->RayWorldIntersection
					pe_action_awake action;
					action.bAwake = pActor->GetEntityId()!=0;
					pPhysics->Action(&action);
					pPhysics->SetParams(&sp);
					
				//}
			//m_pWeapon->GetPhysicalProxy(true)->EnablePhysics(true);
			//m_pWeapon->GetPhysicalProxy(true)->EnablePhysics(true);
			//m_pWeapon->GetPhysicalProxy(true)->EnableRestrictedRagdoll(true);
			

	IEntityPhysicalProxy *pPhysicsProxy = (IEntityPhysicalProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS);
	//m_pWeapon->GetPhysicalProxy(true);
		AABB bbox;
	pPhysicsProxy->GetWorldBounds( bbox );

	IPhysicalWorld *pWorld = gEnv->pSystem->GetIPhysicalWorld();
	IPhysicalEntity **ppColliders;
	int cnt = pWorld->GetEntitiesInBox( bbox.min,bbox.max, ppColliders,ent_living);
	for (int i = 0; i < cnt; i++)
	{

		IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics( ppColliders[i] );
		if (!pEntity)
			continue;
		
		// skip the vehicle itself
		if (pEntity==m_pWeapon->GetEntity())
			continue;

		IPhysicalEntity *pPhysEnt = pEntity->GetPhysics();
		if (!pPhysEnt) 
			continue;

		IActor* pActor = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());            
		if(!pActor)
			continue;

		/*if(pActor->IsPlayer())
			continue;*/
/*
		if(pActor == m_pWeapon->GetOwnerActor())
			continue;

		CGameRules *pGameRules = g_pGame->GetGameRules();

			int damage = m_meleeparams.damage;
			/*if(m_nextatk == 0)
				damage = m_meleeparams.damage;*/
	/*		if(m_nextatk == 1)
				damage = m_meleeparams.damage;
			else if(m_nextatk == 2)
				damage = m_meleeparams.damage1;
			else if(m_nextatk == 3)
				damage = m_meleeparams.damage2;
			else if(m_nextatk == 4)
				damage = m_meleeparams.damage3;
			else if(m_nextatk == 5)
				damage = m_meleeparams.damage4;
			else if(m_nextatk == 6)
				damage = m_meleeparams.damage5;
			else if(m_nextatk == 7)
				damage = m_meleeparams.damage6;
			else if(m_nextatk == 8)
				damage = m_meleeparams.damage7;
			else if(m_nextatk == 9)
				damage = m_meleeparams.damage8;
			else if(m_nextatk == 10)
				damage = m_meleeparams.damage9;

			/*if(pActor && !pActor->IsPlayer())
				damage = m_meleeparams.damageAI;*/
/*
			HitInfo info(m_pWeapon->GetOwnerId(), pEntity->GetId(), m_pWeapon->GetEntityId(),
				m_pWeapon->GetFireModeIdx(GetName()), 0.0f, pGameRules->GetHitMaterialIdFromSurfaceId(surfaceIdx), partId,
				pGameRules->GetHitTypeId(m_meleeparams.hit_type.c_str()), pt, dir, normal);
				//info.type = 0;
				//info.pos = pActor->GetEntity()->GetWorldPos();
			//pActor->SetHealth(pActor->GetHealth()-10);

			info.remote = remote;
			if (!remote)
				info.seq=m_pWeapon->GenerateShootSeqN();
			info.damage = /*m_meleeparams.*//*damage;

			if (m_pWeapon->GetForcedHitMaterial() != -1)
				info.material=pGameRules->GetHitMaterialIdFromSurfaceId(m_pWeapon->GetForcedHitMaterial());

			pGameRules->ClientHit(info);

		if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	} */


	// play effects
	if(ok)
	{
		IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();

		TMFXEffectId effectId = pMaterialEffects->GetEffectId("melee", surfaceIdx);
		if (effectId != InvalidEffectId)
		{
			SMFXRunTimeEffectParams params;
			params.pos = pt;
			params.playflags = MFX_PLAY_ALL | MFX_DISABLE_DELAY;
			params.soundSemantic = eSoundSemantic_Player_Foley;
			pMaterialEffects->ExecuteEffect(effectId, params);
		}
		if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
	}

	ApplyCameraShake(true);
	
	m_pWeapon->PlayAction(m_meleeactions.hit.c_str());
	if(m_meleeparams.number_combohits <= m_nextatk)
			{
			m_nextatk = 0;
			}
}

//------------------------------------------------------------------------
void CMeleeLaser::Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale)
{
	if(m_noImpulse)
	{
		m_noImpulse = false;
		return;
	}

	if (pCollider && m_meleeparams.impulse>0.001f)
	{
		IEntity *pEntityPhy = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
		/*if(pEntityPhy)
		{
			std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
			std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
			int count = m_hitedEntites.size();
				
			for(int i = 0; i < count, it!=end; i++ , ++it)
			{
				if(pEntityPhy->GetId() == (*it))
				return;
			}
			
		}*/
		bool strengthMode = true;
		CPlayer *pPlayer = (CPlayer *)m_pWeapon->GetOwnerActor();
		if(pPlayer)
		{
			if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
			{
				/*ENanoMode curMode = pSuit->GetMode();*/
				if (gEnv->bClient)
					strengthMode = true;
			}
		}

		pe_status_dynamics dyn;

		if (!pCollider->GetStatus(&dyn))
		{
			if(strengthMode)
				impulseScale *= 0.01f;
		}
		else
			impulseScale *= clamp((dyn.mass * 0.01f), 1.0f, 15.0f);
		
		//[kirill] add impulse to phys proxy - to make sure it's applied to cylinder as well (not only skeleton) - so that entity gets pushed
		// if no pEntity - do it old way
		IEntity * pEntity = (IEntity*) pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);

		if(gEnv->bMultiplayer && pEntity)
		{
			if(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()) == NULL)
				impulseScale *= 0.33f;
		}

		if(pEntity)
		{
			{
				IEntityPhysicalProxy* pPhysicsProxy = (IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
				CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());

				if (pActor)
				{
					SActorStats* pAS = pActor->GetActorStats();
					if (pAS && pAS->isRagDoll)
					{
						//marcok: talk to me before touching this
						impulseScale = 0.01f; //jan: melee impulses were scaled down, I made sure it still "barely moves"
					}
				}

				// scale impulse up a bit for player 
				pPhysicsProxy->AddImpulse(partId, pt, dir*m_meleeparams.impulse*impulseScale*m_meleeScale, true, 0.01f);
			}
		}
		else
		{
			pe_action_impulse ai;
			ai.partid = partId;
			ai.ipart = ipart;
			ai.point = pt;
			ai.iApplyTime = 0;
			ai.impulse = dir*(m_meleeparams.impulse*impulseScale*m_meleeScale);
			pCollider->Action(&ai);
		}

		ISurfaceTypeManager *pSurfaceTypeManager = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager();
		int invId = pSurfaceTypeManager->GetSurfaceTypeByName("mat_invulnerable")->GetId();

		// create a physical collision to break trees
		pe_action_register_coll_event collision;

		collision.collMass = 0.005f; // this is actually ignored
		collision.partid[1] = partId;

		// collisions involving partId<-1 are to be ignored by game's damage calculations
		// usually created articially to make stuff break.
		collision.partid[0] = 1;
		collision.idmat[1] = surfaceIdx;
		collision.idmat[0] = invId;
		collision.n = normal;
		collision.pt = pt;
	
		// scar bullet
		// m = 0.0125
		// v = 800
		// energy: 4000
		// in this case the mass of the active collider is a player part
		// so we must solve for v given the same energy as a scar bullet
		Vec3	v = dir;
		float speed = cry_sqrtf(4000.0f/(80.0f*0.5f)); // 80.0f is the mass of the player

		// [marco] Check if an object. Should take lots of time to break stuff if not in nanosuit strength mode;
		// and still creates a very low impulse for stuff that might depend on receiving an impulse.
		IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);
		if (pBrush)
		{
			CActor *pActor = m_pWeapon->GetOwnerActor();
			if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
			{
				CPlayer *pPlayer = (CPlayer *)pActor;
				if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
				{
					/*ENanoMode curMode = pSuit->GetMode();*/
					if (gEnv->bClient)
						speed =0.003f;
				}
			}
		}

		collision.vSelf = (v.normalized()*speed*m_meleeScale);
		collision.v = Vec3(0,0,0);
		collision.pCollider = pCollider;

		IEntity *pOwner = m_pWeapon->GetOwner();
		if (pOwner && pOwner->GetCharacter(0) && pOwner->GetCharacter(0)->GetISkeletonPose()->GetCharacterPhysics())
		{
			if (ISkeletonPose *pSkeletonPose=pOwner->GetCharacter(0)->GetISkeletonPose())
			{
				if (pSkeletonPose && pSkeletonPose->GetCharacterPhysics())
					pSkeletonPose->GetCharacterPhysics()->Action(&collision);
			}

		}
	}
}

//------------------------------------------------------------------------
void CMeleeLaser::Impulse2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale)
{
	if(m_noImpulse)
	{
		m_noImpulse = false;
		return;
	}
	//dir*1.6;
	if (pCollider && m_meleeparams.impulse>0.001f)
	{
		/*IEntity *pEntityPhy = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
		if(pEntityPhy)
		{
			std::vector<EntityId>::const_iterator it = m_applyimpulse_Entites.begin();
			std::vector<EntityId>::const_iterator end = m_applyimpulse_Entites.end();
			int count = m_hitedEntites.size();
				
			for(int i = 0; i < count, it!=end; i++ , ++it)
			{
				if(pEntityPhy->GetId() == (*it))
				return;
			}
			
		}*/
		//m_applyimpulse_Entites.push_back(pEntityPhy->GetId());
		bool strengthMode = true;

		pe_status_dynamics dyn;

		/*if (pCollider->GetStatus(&dyn))
		{
			if(strengthMode)*/
				impulseScale *= 3.0f;/*
		}
		else*/
			//impulseScale *= clamp((dyn.mass * 0.01f), 1.0f, 15.0f);
		
		//[kirill] add impulse to phys proxy - to make sure it's applied to cylinder as well (not only skeleton) - so that entity gets pushed
		// if no pEntity - do it old way
		//IEntity * pEntity = (IEntity*) pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);

		/*if(gEnv->bMultiplayer && pEntity)
		{
			if(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()) == NULL)
				impulseScale *= 0.33f;
		}*/

		//if(pEntity)
		//{
			//{
				//pCollider->
				//IPhysicalProxy* pPhysicsProxy = (IEntityPhysicalProxy*)pEntity->GetProxy(ENTITY_PROXY_PHYSICS);
				/*CActor* pActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());

				if (pActor)
				{
					SActorStats* pAS = pActor->GetActorStats();
					if (pAS && pAS->isRagDoll)
					{
						//marcok: talk to me before touching this
						impulseScale = 1.0f; //jan: melee impulses were scaled down, I made sure it still "barely moves"
					}
				}*/

				// scale impulse up a bit for player 
				//pPhysicsProxy->AddImpulse(partId, pt, dir*m_meleeparams.impulse*impulseScale*m_meleeScale, true, 1.f);
		//}
		//}
		//else
		//{
			pe_action_impulse ai;
			ai.partid = partId;
			ai.ipart = ipart;
			ai.point = pt;
			ai.iApplyTime = 0;
			ai.impulse = dir*(m_meleeparams.impulse*impulseScale*m_meleeScale);
			pCollider->Action(&ai);
		//}

		ISurfaceTypeManager *pSurfaceTypeManager = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager();
		int invId = pSurfaceTypeManager->GetSurfaceTypeByName("mat_invulnerable")->GetId();

		// create a physical collision to break trees
		pe_action_register_coll_event collision;
		//pe_action_awake caaa;

		//caaa.bAwake = true;
		//pCollider->Action(&caaa);

		collision.collMass = 0.005f; // this is actually ignored
		collision.partid[1] = partId;
		//collision.

		// collisions involving partId<-1 are to be ignored by game's damage calculations
		// usually created articially to make stuff break.
		collision.partid[0] = -2;
		collision.idmat[1] = surfaceIdx;
		collision.idmat[0] = invId;
		collision.n = normal;
		collision.pt = pt;
	
		// scar bullet
		// m = 0.0125
		// v = 800
		// energy: 4000
		// in this case the mass of the active collider is a player part
		// so we must solve for v given the same energy as a scar bullet
		Vec3	v = dir;
		float speed = cry_sqrtf(4000.0f/(80.0f*0.5f)); // 80.0f is the mass of the player

		// [marco] Check if an object. Should take lots of time to break stuff if not in nanosuit strength mode;
		// and still creates a very low impulse for stuff that might depend on receiving an impulse.
		IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);
		if (pBrush)
		{
			/*CActor *pActor = m_pWeapon->GetOwnerActor();
			if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
			{
				CPlayer *pPlayer = (CPlayer *)pActor;
				if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
				{
					/*ENanoMode curMode = pSuit->GetMode();*/
					//if (gEnv->bClient)
						//speed =0.003f;
				/*}
			}*/
			//pBrush->
			//speed =1111.003f;
			/*pe_explosion explosion;
		explosion.epicenter = pt;
		explosion.rmin = 0.001;
		explosion.rmax = 1.01;
		if (explosion.rmax==0)
			explosion.rmax=0.0001f;
		explosion.r = explosion.rmin;
		explosion.impulsivePressureAtR = 111.1;
		explosion.epicenterImp = pt;
		explosion.explDir = dir;
		explosion.nGrow = 111.1;
		explosion.rminOcc = 11.01f;
		//explosion.

		// we separate the calls to SimulateExplosion so that we can define different radii for AI and physics bodies
		explosion.holeSize = 0.0f;
		explosion.nOccRes = 0.1explosion.rmax>50.0f ? 0:32;
		gEnv->pPhysicalWorld->SimulateExplosion( &explosion, 0, 0, ent_static);*/
			//pCollider->Action(&collision);
			//pCollider->Action
		}
		/*IEntityPhysicalProxy *pPhysicsProxys = (IEntityPhysicalProxy*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC);
		if(pPhysicsProxys)
		{
			pPhysicsProxys->GetEntity()->
		}*/

		collision.vSelf = (v.normalized()*speed*m_meleeScale);
		collision.v = v;//Vec3(0,0,0);
		collision.pCollider = pCollider;
		//pCollider->Action(&collision);
		//pCollider->

		/*pe_explosion explosion;
		explosion.epicenter = pt;
		explosion.rmin = 0.001;
		explosion.rmax = 1.01;
		if (explosion.rmax==0)
			explosion.rmax=0.0001f;
		explosion.r = explosion.rmin;
		explosion.impulsivePressureAtR = 111.1;
		explosion.epicenterImp = pt;
		explosion.explDir = dir;
		explosion.nGrow = 111.1;
		explosion.rminOcc = 11.01f;
		//explosion.

		// we separate the calls to SimulateExplosion so that we can define different radii for AI and physics bodies
		explosion.holeSize = 0.0f;
		explosion.nOccRes = 0.1;
		gEnv->pPhysicalWorld->SimulateExplosion( &explosion, 0, 0, ent_static);*/

		IEntity *pOwner = m_pWeapon->GetOwner();
		//pOwner->GetPhysics()->Action(&collision);
		if (pOwner && pOwner->GetCharacter(0) && pOwner->GetCharacter(0)->GetISkeletonPose()->GetCharacterPhysics())
		{
			if (ISkeletonPose *pSkeletonPose=pOwner->GetCharacter(0)->GetISkeletonPose())
			{
				if (pSkeletonPose && pSkeletonPose->GetCharacterPhysics())
					pSkeletonPose->GetCharacterPhysics()->Action(&collision);
			}

		}
	}
}
//------------------------------------------------------------------------
void CMeleeLaser::Hit(geom_contact *contact, const Vec3 &dir, float damageScale, bool remote)
{
	CActor *pOwner = m_pWeapon->GetOwnerActor();
	if (!pOwner)
		return;

	Vec3 view(0.0f, 1.0f, 0.0f);

	if (IMovementController *pMC = pOwner->GetMovementController())
	{
		SMovementState state;
		pMC->GetMovementState(state);
		view = state.pos;//eyeDirection;
	}

	// some corrections to make sure the impulse is always away from the camera, and is not a backface collision
	//bool backface = dir.Dot(contact->n)>0;
	//bool away = dir.Dot(view.normalized())>0; // away from cam?

	Vec3 normal=contact->n;
	Vec3 ndir=dir;

	/*if (backface)
	{
		if (away)
			normal = -normal;
		else
			ndir = -dir;
	}
	else
	{
		if (!away)
		{
			ndir = -dir;
			normal = -normal;
		}
	}*/

	IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->id/*iPrim*/[0]);

	Hit(contact->pt, ndir, normal, pCollider, contact->id/*iPrim*/[1], 0, contact->id[1], damageScale, remote);
}

//------------------------------------------------------------------------
void CMeleeLaser::Hit(ray_hit *hit, const Vec3 &dir, float damageScale, bool remote)
{
	Hit(hit->pt, dir, hit->n, hit->pCollider, hit->partid, hit->ipart, hit->surface_idx, damageScale, remote);
}

//-----------------------------------------------------------------------
void CMeleeLaser::ApplyCameraShake(bool hit)
{
	// Add some camera shake for client even if not hitting
	/*if(m_pWeapon->GetOwnerActor() && m_pWeapon->GetOwnerActor()->IsClient())
	{
		if(CScreenEffects* pScreenEffects = m_pWeapon->GetOwnerActor()->GetScreenEffects())
		{
			float rotateTime;
			if(!hit)
			{
				rotateTime = g_pGameCVars->hr_rotateTime*1.25f;
				pScreenEffects->CamShake(Vec3(rotateTime*0.5f,rotateTime*0.5f,rotateTime*0.25f), Vec3(0, 0.3f * g_pGameCVars->hr_rotateFactor,0), rotateTime, rotateTime);
			}
			else
			{
				rotateTime = g_pGameCVars->hr_rotateTime*2.0f;
				pScreenEffects->CamShake(Vec3(rotateTime,rotateTime,rotateTime*0.5f), Vec3(0, 0.5f * g_pGameCVars->hr_rotateFactor,0), rotateTime, rotateTime);
			}
		}
	}*/
}

float CMeleeLaser::GetOwnerStrength() const
{
	/*CActor *pActor = m_pWeapon->GetOwnerActor();
	if(!pActor)
		return 1.0f;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return 1.0f;*/

	float strength = 1.0f; //pActor->GetActorStrength();
	//if (pActor->GetActorClass() == CPlayer::GetActorClassType())
	/*{
		///CPlayer *pPlayer = (CPlayer *)pActor;/*
		if (CNanoSuit *pSuit = pPlayer->GetNanoSuit())
		{*//*
			//ENanoMode curMode = pSuit->GetMode();
			//if (gEnv->bClient)
			//{
				//if (pPlayer)
				//{*/
							float dmgmult = 0.1f;
							strength = dmgmult/*pActor->GetActorStrength()*/;
				strength = strength * (0.2f + 0.4f * 1*STRENGTH_MULT);
				
				/*IEntity *pEntity = pActor->GetEntity();
				SmartScriptTable props;
					SmartScriptTable propsStat;
					IScriptTable* pScriptTable = pEntity->GetScriptTable();
					pScriptTable && pScriptTable->GetValue("Properties", props);
					props->GetValue("Stat", propsStat);

					float str = 1.0f;
					propsStat->GetValue("strength", str);


				//strength *= /*str*///pActor->GetActorStrength() || str/*g_pGameCVars->g_strength*/;
				//}
					//if(str > pActor->GetActorStrength())
					//{
						strength *= 1;
					//}
					//else if(str < pActor->GetActorStrength())
					//{
					//	strength *= pActor->GetActorStrength();
					//}
					//else if(str == pActor->GetActorStrength())
					//{
					//	strength *= pActor->GetActorStrength();
					//}
				//else if (!pPlayer)
				//{
							//float dmgmult = 0.1f;
							//strength = dmgmult/*pActor->GetActorStrength()*/;
				//strength = strength * (0.2f + 0.4f * gEnv->bClient*STRENGTH_MULT);
					//IEntity *pEntity = pActor->GetEntity();
				//SmartScriptTable props;
					//SmartScriptTable propsStat;
					//IScriptTable* pScriptTable = pEntity->GetScriptTable();
					//pScriptTable && pScriptTable->GetValue("Properties", props);
					//props->GetValue("Stat", propsStat);

					//float str = 1.0f;
					//propsStat->GetValue("strength", str);
					//strength *= str;
				//}
			//}
		//}
	//}

	return (0.2f+(strength/10));
}

void CMeleeLaser::GetMemoryStatistics( ICrySizer * s )
{
	s->Add(*this);
	s->Add(m_name);
	m_meleeparams.GetMemoryStatistics(s);
	m_meleeactions.GetMemoryStatistics(s);
}
