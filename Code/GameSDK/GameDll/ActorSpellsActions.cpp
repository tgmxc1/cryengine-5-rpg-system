#include "StdAfx.h"
#include "ActorSpellsActions.h"
#include "Game.h"
#include "GameCVars.h"
#include "Projectile.h"
#include "GameXMLSettingAndGlobals.h"
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "SpellProjectile.h"
#include "ActorActionsNew.h"
#include "PlayerAnimation.h"
#include "Player.h"

#include <CryAISystem/IAIActor.h>

//#pragma warning(push)
//#pragma warning(disable : 4244)

CActorSpellsActions::CActorSpellsActions()
{
	//CryLogAlways("CActorSpellsActions::CActorSpellsActions()  constructor call");
	for (int i = 0; i < MAX_PER_CAST_SLOTS; i++)
	{
		current_cast_slots[i] = -1;	
		current_cast_slots_timers[i] = 0.0f;
		current_cast_slots_charge_timers[i] = 0.0f;
		spell_cast_slots_delays[i] = 0.0f;
		spell_cast_slots_requred_to_cast[i] = false;
		spell_cast_recoils[i] = 0.0f;
		particle_attachments_on_user_actor[i].clear();
		lc_emm[i] = NULL;
		Aoe_emm[i] = NULL;
		m_CastSpellActions[i] = NULL;
		is_cast_real_started = false;
		current_cast_slots_timers_rts[i] = 0.0f;
		current_cast_aoe_spell[i] = 0;
		Aoe_pos[i] = Vec3(ZERO);
	}
	/*current_cast_slots[0] = -1;
	current_cast_slots[1] = -1;
	current_cast_slots[2] = -1;
	current_cast_slots_timers[0] = 0.0f;
	current_cast_slots_timers[1] = 0.0f;
	current_cast_slots_timers[2] = 0.0f;
	current_cast_slots_charge_timers[0] = 0.0f;
	current_cast_slots_charge_timers[1] = 0.0f;
	current_cast_slots_charge_timers[2] = 0.0f;
	spell_cast_slots_delays[0] = 0.0f;
	spell_cast_slots_delays[1] = 0.0f;
	spell_cast_slots_delays[2] = 0.0f;
	spell_cast_slots_requred_to_cast[0] = false;
	spell_cast_slots_requred_to_cast[1] = false;
	spell_cast_slots_requred_to_cast[2] = false;
	spell_cast_recoils[0] = 0.0f;
	spell_cast_recoils[1] = 0.0f;
	spell_cast_recoils[2] = 0.0f;*/
	pActorUser = NULL;
	emitters_to_delete.clear();
	emitters_to_delete.resize(0);
}

CActorSpellsActions::~CActorSpellsActions()
{
	if (gEnv->p3DEngine && gEnv->p3DEngine->GetParticleManager())
	{
		std::vector<IParticleEmitter*>::iterator it_items = emitters_to_delete.begin();
		std::vector<IParticleEmitter*>::iterator end_items = emitters_to_delete.end();
		int count = emitters_to_delete.size();
		for (int ii = 0; ii < count, it_items != end_items; ii++, ++it_items)
		{
			/*
			if(*it_items != NULL)
			{
				gEnv->p3DEngine->GetParticleManager()->DeleteEmitter(*it_items);
			}
			*/
		}
	}
	emitters_to_delete.clear();
	emitters_to_delete.resize(0);
}

void CActorSpellsActions::Update(float frametime)
{
	if (!pActorUser)
		return;

	for (int i = 1; i < MAX_PER_CAST_SLOTS; i++)
	{
		bool update_pr_atts = false;
		if (i == MAX_PER_CAST_SLOTS)
			break;

		if (current_cast_slots[i] != -1)
		{
			if (current_cast_aoe_spell[i] != 0)
			{
				ProcessAOESpell(i);
			}

			if (current_cast_slots_timers[i] < 999.0f)
				current_cast_slots_timers[i] += frametime;

			if (current_cast_slots_timers_rts[i] < 999.0f)
				current_cast_slots_timers_rts[i] += frametime;

			if(current_cast_slots_charge_timers[i] == 0.0f)
				update_pr_atts = true;

			if (current_cast_slots_charge_timers[i] > 0.0f)
			{
				if (current_cast_slots_charge_timers[i] < current_cast_slots_timers[i])
				{
					RequestToPreEndCast(i);
					current_cast_slots_charge_timers[i] = 0.0f;
				}
			}
		}
		else
		{
			current_cast_slots_timers_rts[i] = 0.0f;
		}

		if (spell_cast_slots_requred_to_cast[i])
		{
			if (spell_cast_slots_delays[i] > 0.0f)
			{
				spell_cast_slots_delays[i] -= frametime;
				if (spell_cast_slots_delays[i] <= 0.0f)
				{
					spell_cast_slots_delays[i] = 0.0f;
					CastPerm(i);
					//is_cast_real_started = true;
				}
			}
		}

		if (spell_cast_recoils[i] > 0.0f)
		{
			spell_cast_recoils[i] -= frametime;
			if (spell_cast_recoils[i] <= 0.0f)
			{
				spell_cast_recoils[i] = 0.0f;
				//RequestToEndCurrentCast(i, false, false, true);
			}
		}

		if (update_pr_atts)
		{
			UpdateProjectedParticleAttachments(i);
		}
	}
}

void CActorSpellsActions::StartCast(int spell_slot, int forced_cast_slot)
{
	if (spell_slot >= MAX_USABLE_SPELLS_SLOTS)
		return;

	if (!pActorUser)
		return;

	if (pActorUser->GetSAPlayTime() > 0)
		return;

	if (pActorUser->GetRelaxedMod())
		return;

	CPlayer *pPlayer = (CPlayer*)(pActorUser);
	if (pPlayer)
	{
		if (pPlayer->IsOnLedge() || pPlayer->IsOnLadder())
			return;
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (!pItem)
		return;

	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(spell_slot);
	if (pUser_Spell_Current.spell_id == -1)
	{
		for (int i = 0; i < MAX_USABLE_SPELLS_SLOTS; i++)
		{
			pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(i);
			if (pUser_Spell_Current.spell_id > 0)
			{
				pActorUser->AddSpellToCurrentSpellsForUseSlot(pUser_Spell_Current.spell_id, forced_cast_slot);
				break;
			}
			else
			{
				pUser_Spell_Current.spell_id = -1;
			}
		}
	}

	if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
	{
		CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current_ot;
		pUser_Spell_Current_ot.spell_id = -1;
		for (int i = 0; i < MAX_PER_CAST_SLOTS; i++)
		{
			pUser_Spell_Current_ot.spell_id = -1;
			if (current_cast_slots[i] <= -1)
				continue;

			pUser_Spell_Current_ot = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[i]);
			if (pUser_Spell_Current_ot.spell_id > 0)
			{
				if (pUser_Spell_Current_ot.two_handed_spell)
				{
					return;
				}
			}
			else
				pUser_Spell_Current_ot.spell_id = -1;
		}
	}

	if (pUser_Spell_Current.spell_id > -1)
	{
		if (!pActorUser->IsRemote())
		{
			if ((pActorUser->GetHealth() - pUser_Spell_Current.spell_hp_cost) < 0.0f)
			{
				//RequestToEndCurrentCast(slot_to_use, false, false, false);
				return;
			}
			else if ((pActorUser->GetMagicka() - pUser_Spell_Current.spell_mana_cost) < 0.0f)
			{
				//RequestToEndCurrentCast(slot_to_use, false, false, false);
				return;
			}
			else if ((pActorUser->GetStamina() - pUser_Spell_Current.spell_stmn_cost) < 0.0f)
			{
				//RequestToEndCurrentCast(slot_to_use, false, false, false);
				return;
			}
		}

		if (pUser_Spell_Current.two_handed_spell)
		{
			if (!pActorUser->CanDoAnyActionWNeededTwoHands())
				return;
		}

		if (forced_cast_slot > 0 && IsCurrentSlotBusy(forced_cast_slot))
		{
			return;
		}

		bool added_to_slot = false;
		int slot_to_use = 0;
		if (forced_cast_slot > 0 && forced_cast_slot <= 2)
		{
			if (spell_cast_slots_requred_to_cast[forced_cast_slot])
				return;

			if (spell_cast_slots_delays[forced_cast_slot] > 0.0f)
				return;

			if (current_cast_slots[forced_cast_slot] == -1)
			{
				current_cast_slots[forced_cast_slot] = spell_slot;
				added_to_slot = true;
				slot_to_use = forced_cast_slot;
			}
			else
			{
				return;
			}
		}
		else
		{
			if (current_cast_slots[1] == -1)
			{
				if (spell_cast_slots_requred_to_cast[1])
					return;

				if (spell_cast_slots_delays[1] > 0.0f)
					return;

				current_cast_slots[1] = spell_slot;
				added_to_slot = true;
				slot_to_use = 1;
			}

			if (current_cast_slots[2] == -1 && !added_to_slot)
			{
				if (spell_cast_slots_requred_to_cast[2])
					return;

				if (spell_cast_slots_delays[2] > 0.0f)
					return;

				current_cast_slots[2] = spell_slot;
				added_to_slot = true;
				slot_to_use = 2;
			}
		}
		if (!added_to_slot)
			return;

		if (spell_cast_recoils[slot_to_use] > 0.0f)
		{
			RequestToEndCurrentCast(slot_to_use, false, true, false);
			return;
		}


		if (slot_to_use == 1)
		{

			if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				RequestToEndCurrentCast(slot_to_use, false, true, false);
				return;
			}

			if (pActorUser->m_lft_hand_blc)
			{
				RequestToEndCurrentCast(slot_to_use, false, true, false);
				return;
			}

			if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
			{
				if (pUser_Spell_Current.two_handed_spell)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pItem && !pItem->IsItemIgnoredByHud() && pUser_Spell_Current.can_cast_w_weapon == 0)
			{
				RequestToEndCurrentCast(slot_to_use, false, true, false);
				return;
			}

			if (pActorUser->m_blockactivenoshield || pActorUser->m_blockactiveshield)
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 3 && pActorUser->m_blockactiveshield)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
				else if (pActorUser->m_blockactivenoshield)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}
			//CryLogAlways("SetItemFlag eIF_BlockActions - StartCast fnc");
			if (!pItem->IsItemIgnoredByHud())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 0)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pItem->IsWpnTwoHanded())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 2 && (pItem->GetWeaponType() != 10 && pItem->GetMagickPwrMult() <= 1.0f))
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}
			//**********************************************************************************************************
						//pItem->SetItemFlag(CItem::eIF_BlockActions, true);
			//**********************************************************************************************************
			pActorUser->m_lft_hand_blc = true;
			if (pUser_Spell_Current.two_handed_spell)
			{
				pActorUser->m_rgt_hand_blc = true;
			}
		}
		else if (slot_to_use == 2)
		{
			CItem *pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetAnyItemInLeftHand()));
			if (pItem_lft && pItem_lft->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				RequestToEndCurrentCast(slot_to_use, false, true, false);
				return;
			}

			if (pActorUser->m_rgt_hand_blc)
			{
				RequestToEndCurrentCast(slot_to_use, false, true, false);
				return;
			}

			if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
			{
				if (pUser_Spell_Current.two_handed_spell)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pActorUser->m_blockactiveshield || pActorUser->m_blockactivenoshield)
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 3 && pActorUser->m_blockactivenoshield)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
				else if (pActorUser->m_blockactiveshield)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pItem_lft && !pItem_lft->IsItemIgnoredByHud())
			{
				if (!pUser_Spell_Current.can_cast_w_weapon)
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pItem_lft && pItem_lft->IsWpnTwoHanded())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 2 && (pItem_lft->GetWeaponType() != 10 && pItem_lft->GetMagickPwrMult() <= 1.0f))
				{
					RequestToEndCurrentCast(slot_to_use, false, true, false);
					return;
				}
			}

			if (pItem_lft)
			{
				//********************************************************************************************************************
								//pItem_lft->SetItemFlag(CItem::eIF_BlockActions, true);
				//********************************************************************************************************************
			}
			pActorUser->m_rgt_hand_blc = true;
			if (pUser_Spell_Current.two_handed_spell)
			{
				pActorUser->m_lft_hand_blc = true;
			}
		}

		spell_cast_slots_delays[slot_to_use] = pUser_Spell_Current.spell_cast_delay;
		if (pUser_Spell_Current.spell_sub_type == eSpell_type_cast_continuosly_on_target || pUser_Spell_Current.spell_sub_type == eSpell_type_cast_continuosly_on_self)
		{
			if (!pUser_Spell_Current.spell_cast_action_chargecast[slot_to_use].empty())
			{
				PlayCastAction(pUser_Spell_Current.spell_cast_action_chargecast[slot_to_use].c_str(), slot_to_use, pItem->GetEntityId());
			}
		}
		else
		{
			if (!pUser_Spell_Current.spell_cast_action_startcast[slot_to_use].empty())
			{
				PlayCastAction(pUser_Spell_Current.spell_cast_action_startcast[slot_to_use].c_str(), slot_to_use, pItem->GetEntityId());
			}
		}

		if (pUser_Spell_Current.cast_after_charge_perm && pUser_Spell_Current.spell_chargeable)
		{
			current_cast_slots_charge_timers[slot_to_use] = pUser_Spell_Current.spell_cast_max_time_to_charge;
		}
		//event/////////////////////////
		OnStartCastSpell(slot_to_use);
		////////////////////////////////
		if (pActorUser->IsThirdPerson())
		{
			for (int i = 0; i < 5; i++)
			{
				if (!pUser_Spell_Current.spell_particles[slot_to_use][i].empty())
				{
					ICharacterInstance *pCharacter = pActorUser->GetEntity()->GetCharacter(0);
					if (pCharacter)
					{
						int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(pUser_Spell_Current.spell_helpers[slot_to_use][i].c_str());
						if (id != -1)
						{
							string att_name = "spell_particle";
							string slot_name = "";
							if (slot_to_use == 1)
								slot_name = "_rigth_";
							else if (slot_to_use == 2)
								slot_name = "_left_";

							char B_valstr[17];
							itoa(i, B_valstr, 10);
							string C_valstr = B_valstr;
							att_name = att_name + slot_name + C_valstr;
							if (!pActorUser->IsAttachmentOnCharacter(0, att_name))
							{
								pActorUser->CreateParticleAttachment(0, att_name.c_str(), pUser_Spell_Current.spell_helpers[slot_to_use][i].c_str(), pUser_Spell_Current.spell_particles[slot_to_use][i].c_str(), true);
							}

							if (i == g_pGameCVars->g_spell_system_ext_particle_id_to_cl_direction)
							{
								pActorUser->HideOrUnhideAttachment(0, att_name.c_str(), true);
								particle_attachments_on_user_actor[slot_to_use] = att_name;
								//UpdateProjectedParticleAttachments();
							}
						}
						else
						{
							if (slot_to_use == 1)
							{
								if (pActorUser->GetCurrentItemId() > 0)
								{

								}
							}
							else if (slot_to_use == 2)
							{
								if (pActorUser->GetAnyItemInLeftHand() > 0)
								{

								}
							}
						}
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < 5; i++)
			{
				if (!pUser_Spell_Current.spell_particles_fp[slot_to_use][i].empty())
				{
					ICharacterInstance *pCharacter = pActorUser->GetEntity()->GetCharacter(0);
					if (pCharacter)
					{
						int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(pUser_Spell_Current.spell_helpers[slot_to_use][i].c_str());
						if (id != -1)
						{
							string att_name = "spell_particle";
							string slot_name = "";
							if (slot_to_use == 1)
								slot_name = "_rigth_";
							else if (slot_to_use == 2)
								slot_name = "_left_";

							char B_valstr[17];
							itoa(i, B_valstr, 10);
							string C_valstr = B_valstr;
							att_name = att_name + slot_name + C_valstr;
							if (!pActorUser->IsAttachmentOnCharacter(0, att_name))
							{
								pActorUser->CreateParticleAttachment(0, att_name.c_str(), pUser_Spell_Current.spell_helpers[slot_to_use][i].c_str(), pUser_Spell_Current.spell_particles_fp[slot_to_use][i].c_str(), true);
							}
							if (i == g_pGameCVars->g_spell_system_ext_particle_id_to_cl_direction)
							{
								pActorUser->HideOrUnhideAttachment(0, att_name.c_str(), true);
								particle_attachments_on_user_actor[slot_to_use] = att_name;
								//UpdateProjectedParticleAttachments();
							}
						}
						else
						{
							if (slot_to_use == 1)
							{
								if (pActorUser->GetCurrentItemId() > 0)
								{

								}
							}
							else if (slot_to_use == 2)
							{
								if (pActorUser->GetAnyItemInLeftHand() > 0)
								{

								}
							}
						}
					}
				}
			}
		}
	}
	//---------MP stuff----------
	if (!pActorUser->IsRemote())
	{
		if (gEnv->bMultiplayer)
		{
			if (gEnv->bServer)
				pActorUser->GetGameObject()->InvokeRMI(CActor::ClCastSpell(), CActor::SNetCastSlotPrm(spell_slot, forced_cast_slot), eRMI_ToRemoteClients);
			else
			{
				pActorUser->GetGameObject()->InvokeRMI(CActor::SVCastSpell(), CActor::SNetCastSlotPrm(spell_slot, forced_cast_slot), eRMI_ToServer);
			}
		}
	}
	//---------MP stuff----------
}

void CActorSpellsActions::CastPerm(int cast_slot)
{
	if (cast_slot > 2)
		return;

	if (!pActorUser)
		return;


	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (cast_slot == 1)
	{
		pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetAnyItemInLeftHand()));
		if(!pItem)
			pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetNoWeaponId()));

		if (!pItem)
			pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	}

	if (!pItem)
		return;

	const CGameXMLSettingAndGlobals::SActorSpell &pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[cast_slot]);
	if (pUser_Spell_Current.spell_id == -1)
	{
		RequestToEndCurrentCast(cast_slot, false, false, true);
		return;
	}

	if (!pActorUser->IsRemote())
	{
		pActorUser->SetHealth(pActorUser->GetHealth() - pUser_Spell_Current.spell_hp_cost);
		pActorUser->SetMagicka(pActorUser->GetMagicka() - pUser_Spell_Current.spell_mana_cost);
		pActorUser->SetStamina(pActorUser->GetStamina() - pUser_Spell_Current.spell_stmn_cost);
	}
	float damage = pUser_Spell_Current.spell_damage;
	if (pUser_Spell_Current.apply_weapon_mgc_fcr_mult)
	{
		damage *= pItem->GetMagickPwrMult();
	}

	if (pUser_Spell_Current.spell_chargeable)
	{
		if (current_cast_slots_timers[cast_slot] >= pUser_Spell_Current.spell_cast_max_time_to_charge)
		{
			damage *= pUser_Spell_Current.spell_charge_max_damege_mult;
		}
		else
		{
			float mlt = 1.0f;
			mlt = pUser_Spell_Current.spell_charge_max_damege_mult - mlt;
			float coeff = current_cast_slots_timers[cast_slot] / pUser_Spell_Current.spell_cast_max_time_to_charge;
			mlt = mlt*coeff;
			damage *= mlt;
		}
	}
	Vec3 spell_spwn_pos(ZERO);
	Vec3 spell_spwn_dir(0, 0, 0);
	Vec3 spell_spwn_vel(0, 0, 0);
	//Quat spell_bone_rot_info(IDENTITY);
	if (!pUser_Spell_Current.spell_cast_bone[cast_slot].empty())
	{
		//CryLogAlways("spell_cast_bone = %s", pUser_Spell_Current.spell_cast_bone[cast_slot]);
		spell_spwn_pos = pActorUser->GetBonePosition(pUser_Spell_Current.spell_cast_bone[cast_slot]);
		/*ICharacterInstance *pCharacter = pActorUser->GetEntity()->GetCharacter(0);
		if (pCharacter)
		{
			int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(pUser_Spell_Current.spell_cast_bone[cast_slot]);
			if (id != -1)
			{
				Vec3 JointPos = pCharacter->GetISkeletonPose()->GetAbsJointByID(id).t;
				Vec3 vBonePos = pActorUser->GetEntity()->GetSlotWorldTM(0) * JointPos;
				CryLogAlways("spell_cast_pose x = %f  y = %f  z = %f", vBonePos.x, vBonePos.y, vBonePos.z);
				spell_spwn_pos = vBonePos;
				spell_bone_rot_info = pCharacter->GetISkeletonPose()->GetAbsJointByID(id).q;
			}
		}*/
	}

	if (spell_spwn_pos.IsZero())
	{
		spell_spwn_pos = pActorUser->GetBonePosition(pUser_Spell_Current.spell_helpers[cast_slot][0].c_str());
	}

	//if (spell_spwn_pos.IsZero())
	//{
	//	spell_spwn_pos = pActorUser->GetBonePosition(pUser_Spell_Current.spell_helpers[cast_slot][0].c_str());
	//}

	IMovementController * pMC = pActorUser->GetMovementController();
	if (pMC)
	{
		SMovementState info;
		pMC->GetMovementState(info);
		if (pActorUser->IsPlayer())
		{
			spell_spwn_dir = info.fireDirection;
		}
		else
		{
			if (!pActorUser->Current_target_dir.IsZero())
			{
				spell_spwn_dir = pActorUser->Current_target_dir;
			}
			else
			{
				spell_spwn_dir = info.eyeDirection;
			}
		}
		spell_spwn_vel = spell_spwn_dir * 1.5f;
	}

	if (spell_spwn_pos.IsZero())
	{
		pItem->ForcedUpdateEntityPositionRBAttachemnOnCharacter();
		spell_spwn_pos = pItem->GetEntity()->GetPos();
	}

	float spell_time = pUser_Spell_Current.start_time;
	if (pUser_Spell_Current.apply_weapon_mgc_fcr_mult)
	{
		spell_time *= pItem->GetMagickPwrMult();
	}

	if (pUser_Spell_Current.spell_sub_type != eSpell_type_cast_on_area)
	{
		if ((pUser_Spell_Current.spell_sub_type != eSpell_type_cast_on_self) && (pUser_Spell_Current.spell_sub_type != eSpell_type_cast_continuosly_on_self))
		{
			//if (gEnv->bServer || !gEnv->bMultiplayer)
			{
				for (int i = 0; i < 15; i++)
				{
					if (!pUser_Spell_Current.spell_projectles[i].empty())
					{
						//CryLogAlways("Try to spawn spell projectle");
						CProjectile *pSpellProjecle = g_pGame->GetWeaponSystem()->SpawnAmmo(gEnv->pEntitySystem->GetClassRegistry()->FindClass(pUser_Spell_Current.spell_projectles[i].c_str()), false);
						if (pSpellProjecle)
						{
							CProjectile::SProjectileDesc projectileDesc(pActorUser->GetEntityId(), 0, 0, damage, 0, 0, 0, pSpellProjecle->GetHitType(), 0, false);
							pSpellProjecle->SetParams(projectileDesc);
							//CryLogAlways("Try to get spell projectle");
							CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pSpellProjecle);
							if (pSpellPhy)
							{
								//CryLogAlways("Try to Launch spell projectle, pos x %i y %i z %i", (int)spell_spwn_pos.x, (int)spell_spwn_pos.y, (int)spell_spwn_pos.z);
								Vec3 spell_spwn_dir_npv(ZERO);
								if (pUser_Spell_Current.spell_projectles_dir_offset[i].IsZero())
								{
									spell_spwn_dir_npv = spell_spwn_dir;
								}
								else
								{
									Vec3 dir_offset = pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_dir_offset[i];
									if (pUser_Spell_Current.spell_projectles_dir_offset_randomize)
									{
										Vec3 random_offset = pUser_Spell_Current.spell_projectles_dir_offset[i];
										Vec3 random_offset2 = random_offset;
										random_offset2 = random_offset2.Flip();
										random_offset = cry_random(random_offset2, random_offset);
										/*if (spell_bone_rot_info.IsIdentity())
											dir_offset = random_offset;
										else
											dir_offset = random_offset;*/
										dir_offset = pActorUser->GetEntity()->GetRotation() * random_offset;
									}
									spell_spwn_dir_npv = (spell_spwn_pos + (spell_spwn_dir + dir_offset) * 2.0f);
									spell_spwn_dir_npv = spell_spwn_dir_npv - spell_spwn_pos;
									spell_spwn_dir_npv.normalize();
									//set spawn pos after editing direction------
									spell_spwn_pos = (spell_spwn_pos + (spell_spwn_dir_npv * 0.0625f));
									//-------------------------------------------
								}
								Vec3 spell_spwn_pos_npv = spell_spwn_pos + (pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_pos_offset[i]);
								spell_spwn_vel = spell_spwn_dir_npv * 1.5;
								pSpellPhy->Launch(spell_spwn_pos_npv, spell_spwn_dir_npv, spell_spwn_vel, 1.0f);
								pSpellPhy->SetSpellDamage(damage);
								pSpellPhy->spell_frc = pUser_Spell_Current.spell_force;
								for (int ic = 0; ic < 15; ic++)
								{
									if (pUser_Spell_Current.spell_additive_effects_ids[i][ic] > 0)
									{
										pSpellPhy->additive_effects_on_hit[ic] = pUser_Spell_Current.spell_additive_effects_ids[i][ic];
										if (pUser_Spell_Current.start_time > 0.0f)
											pSpellPhy->additive_effects_times[ic] = spell_time;
										else
											pSpellPhy->additive_effects_times[ic] = 0.0f;
									}
								}

								if (!pUser_Spell_Current.spell_act_scr_func[2].empty())
								{
									pSpellPhy->fnc_param_on_hit[1] = pUser_Spell_Current.spell_act_scr_func[2];
									pSpellPhy->fnc_param_on_hit[2] = pUser_Spell_Current.spell_act_scr_func_arg[2];
								}
							}
							else
							{
								//CryLogAlways("Try to Launch nospell projectle, pos x %i y %i z %i", (int)spell_spwn_pos.x, (int)spell_spwn_pos.y, (int)spell_spwn_pos.z);
								Vec3 spell_spwn_dir_npv(ZERO);
								if (pUser_Spell_Current.spell_projectles_dir_offset[i].IsZero())
								{
									spell_spwn_dir_npv = spell_spwn_dir;
								}
								else
								{
									Vec3 dir_offset = pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_dir_offset[i];
									spell_spwn_dir_npv = (spell_spwn_pos + (spell_spwn_dir + dir_offset) * 2.0f);
									spell_spwn_dir_npv = spell_spwn_dir_npv - spell_spwn_pos;
									spell_spwn_dir_npv.normalize();
								}
								Vec3 spell_spwn_pos_npv = spell_spwn_pos + (pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_pos_offset[i]);
								spell_spwn_vel = spell_spwn_dir_npv * 1.5;
								pSpellProjecle->Launch(spell_spwn_pos_npv, spell_spwn_dir_npv, spell_spwn_vel, 1.0f);
							}
						}
					}
				}
			}
		}
		else
		{
			pActorUser->AddBuff(pUser_Spell_Current.spell_effect_id, spell_time);
		}
	}
	else
	{
		AOESpellCastEnd(cast_slot);
	}

	if (!pUser_Spell_Current.spell_act_scr_func[1].empty())
	{
		pActorUser->RequestToRunLuaFncFromActorScriptInstance(pUser_Spell_Current.spell_act_scr_func[1].c_str(), pUser_Spell_Current.spell_act_scr_func_arg[1].c_str());
	}

	//event/////////////////////////
	OnCastSpell(cast_slot);
	////////////////////////////////

	bool play_end_cast_anim = false;
	if (spell_cast_slots_requred_to_cast[cast_slot])
	{
		if (pUser_Spell_Current.spell_sub_type == eSpell_type_cast_continuosly_on_target)
		{
			bool request_end_cast = false;
			if (!pActorUser->IsRemote())
			{
				if ((pActorUser->GetHealth() - pUser_Spell_Current.spell_hp_cost) < pUser_Spell_Current.spell_hp_cost)
				{
					request_end_cast = true;
				}
				else if ((pActorUser->GetMagicka() - pUser_Spell_Current.spell_mana_cost) < pUser_Spell_Current.spell_mana_cost)
				{
					request_end_cast = true;
				}
				else if ((pActorUser->GetStamina() - pUser_Spell_Current.spell_stmn_cost) < pUser_Spell_Current.spell_stmn_cost)
				{
					request_end_cast = true;
				}
			}
			//play_end_cast_anim = true;
			if (!request_end_cast)
			{
				spell_cast_slots_delays[cast_slot] = pUser_Spell_Current.spell_cast_delay;
				return;
			}
			else
			{
				if (!pActorUser->IsRemote())
				{
					if (gEnv->bMultiplayer)
					{
						if (gEnv->bServer)
							pActorUser->GetGameObject()->InvokeRMI(CActor::ClEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, true), eRMI_ToRemoteClients);
						else
						{
							pActorUser->GetGameObject()->InvokeRMI(CActor::SVEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, true), eRMI_ToServer);
						}
					}
				}
				play_end_cast_anim = true;
			}
		}
	}

	if (pUser_Spell_Current.spell_cast_recoil > 0.0f)
	{
		RequestToEndCurrentCast(cast_slot, false, false, play_end_cast_anim);
		spell_cast_recoils[cast_slot] = pUser_Spell_Current.spell_cast_recoil;
	}
	else
		RequestToEndCurrentCast(cast_slot, false, false, play_end_cast_anim);
}

void CActorSpellsActions::ProcessAOESpell(int cast_slot)
{
	if (!pActorUser)
		return;

	const CGameXMLSettingAndGlobals::SActorSpell &pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(cast_slot);
	if (pUser_Spell_Current.spell_id != -1)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		CActorActionsNew::PhysIgnLst idnoredEnts;
		int num_ents_to_ignore = nwAction->GetIgnoredPhyObjects(pActorUser->GetEntityId(), idnoredEnts);
		int nbvcx = num_ents_to_ignore + 1;
		IPhysicalEntity *pEnts_to_ignore[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for (size_t i = 0; i < static_cast<size_t>(idnoredEnts.size()); ++i)
		{
			pEnts_to_ignore[i] = idnoredEnts[i];
		}
		Vec3 cam_pos(0, 0, 0);
		Vec3 cam_dir(0, 0, 0);
		float ray_length = 15.0f;
		CCamera& cam = GetISystem()->GetViewCamera();
		if (pActorUser->IsPlayer())
		{
			if (pActorUser->IsThirdPerson())
			{
				cam_pos = cam.GetPosition() + cam.GetViewdir();
				cam_dir = cam.GetViewdir()*(ray_length + nwAction->GetDistanceToPlayerFromCamera());
			}
			else
			{
				cam_pos = cam.GetPosition() + cam.GetViewdir();
				cam_dir = cam.GetViewdir()*ray_length;
			}
		}
		else
		{
			IAIActor *pActII = CastToIAIActorSafe(pActorUser->GetEntity()->GetAI());
			if (pActII)
			{
				if (pActII->GetAttentionTarget())
				{
					Vec3 targ_pos = pActII->GetAttentionTarget()->GetPos();
					Vec3 mn_pos(ZERO);
					mn_pos = pActorUser->GetBonePosition("Bip01 Head");
					if(mn_pos.IsZero())
						mn_pos = pActorUser->GetBonePosition("Bip01 Spine");

					if (mn_pos.IsZero())
					{
						AABB bounds;
						IEntityPhysicalProxy* pTargetPhysicalProxy = static_cast<IEntityPhysicalProxy*>(pActorUser->GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS));
						if (pTargetPhysicalProxy)
						{
							pTargetPhysicalProxy->GetWorldBounds(bounds);
							mn_pos = bounds.GetCenter();
						}
					}
					Vec3 targ_dir = mn_pos - targ_pos;
					cam_dir = targ_dir.GetNormalized() * ray_length;
				}
			}
		}
		ray_hit hit;
		gEnv->pPhysicalWorld->RayWorldIntersection(cam_pos, cam_dir, ent_all,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		Vec3 nv_cam_pos = hit.pt + Vec3(0.0f, 0.0f, 5.0f);
		Vec3 nv_cam_dir = (hit.pt - nv_cam_pos) * (ray_length * 2.0f);
		gEnv->pPhysicalWorld->RayWorldIntersection(nv_cam_pos, nv_cam_dir, ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (!Aoe_emm[cast_slot])
		{
			IParticleEffect* pSpellAreaPartic = gEnv->pParticleManager->FindEffect("misc.mgc_on_charc_effects.rcf_aoe_eff");
			Aoe_emm[cast_slot] = pSpellAreaPartic->Spawn(false, IParticleEffect::ParticleLoc(Vec3(ZERO)));
		}
		IPhysicalEntity *pCollider = hit.pCollider;
		if (pCollider)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
			EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
			if (pCollidedEntity)
			{
				CItem *collItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
				if (collItem)
				{
					if (collItem->GetOwner())
					{
						pCollidedEntity = collItem->GetOwner();
						collidedEntityId = collItem->GetOwnerId();
					}
				}

				if (Aoe_emm[cast_slot])
				{
					if (CActor* pActorcl = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId))
					{
						Aoe_emm[cast_slot]->SetLocation(IParticleEffect::ParticleLoc(pActorcl->GetEntity()->GetPos()));
						Aoe_pos[cast_slot] = pActorcl->GetEntity()->GetPos();
					}
					else
					{
						Aoe_emm[cast_slot]->SetLocation(IParticleEffect::ParticleLoc(hit.pt));
						Aoe_pos[cast_slot] = hit.pt;
					}
				}
			}
			else
			{
				if (Aoe_emm[cast_slot])
				{
					Aoe_emm[cast_slot]->SetLocation(IParticleEffect::ParticleLoc(hit.pt));
					Aoe_pos[cast_slot] = hit.pt;
				}
			}
		}
		else
		{
			if (Aoe_emm[cast_slot])
			{
				Aoe_emm[cast_slot]->SetLocation(IParticleEffect::ParticleLoc(hit.pt));
				Aoe_pos[cast_slot] = hit.pt;
			}
		}
	}
}

void CActorSpellsActions::AOESpellCastEnd(int cast_slot)
{
	if (!pActorUser)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (cast_slot == 1)
	{
		pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetAnyItemInLeftHand()));
		if (!pItem)
			pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetNoWeaponId()));

		if (!pItem)
			pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	}

	if (!pItem)
		return;

	if (gEnv->p3DEngine && gEnv->p3DEngine->GetParticleManager())
	{
		if (Aoe_emm[cast_slot])
		{
			Aoe_emm[cast_slot]->Activate(false);
			gEnv->p3DEngine->GetParticleManager()->DeleteEmitter(Aoe_emm[cast_slot]);
		}
		Aoe_emm[cast_slot] = NULL;
	}
	const CGameXMLSettingAndGlobals::SActorSpell &pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(cast_slot);
	if (pUser_Spell_Current.spell_id <= -1)
		return;

	float damage = pUser_Spell_Current.spell_damage;
	if (pUser_Spell_Current.apply_weapon_mgc_fcr_mult)
	{
		damage *= pItem->GetMagickPwrMult();
	}

	if (pUser_Spell_Current.spell_chargeable)
	{
		if (current_cast_slots_timers[cast_slot] >= pUser_Spell_Current.spell_cast_max_time_to_charge)
		{
			damage *= pUser_Spell_Current.spell_charge_max_damege_mult;
		}
		else
		{
			float mlt = 1.0f;
			mlt = pUser_Spell_Current.spell_charge_max_damege_mult - mlt;
			float coeff = current_cast_slots_timers[cast_slot] / pUser_Spell_Current.spell_cast_max_time_to_charge;
			mlt = mlt*coeff;
			damage *= mlt;
		}
	}
	Vec3 spell_spwn_pos(ZERO);
	Vec3 spell_spwn_dir(0, 0, 0);
	Vec3 spell_spwn_vel(0, 0, 0);
	spell_spwn_pos = Aoe_pos[cast_slot];
	spell_spwn_pos.z += 10.0f;
	spell_spwn_dir = Aoe_pos[cast_slot] - spell_spwn_pos;
	spell_spwn_dir.normalize();
	spell_spwn_vel = spell_spwn_dir * 1.5f;
	float spell_time = pUser_Spell_Current.start_time;
	if (pUser_Spell_Current.apply_weapon_mgc_fcr_mult)
	{
		spell_time *= pItem->GetMagickPwrMult();
	}

	//if (gEnv->bServer || !gEnv->bMultiplayer)
	{
		for (int i = 0; i < 15; i++)
		{
			if (!pUser_Spell_Current.spell_projectles[i].empty())
			{
				//CryLogAlways("Try to spawn spell projectle");
				CProjectile *pSpellProjecle = g_pGame->GetWeaponSystem()->SpawnAmmo(gEnv->pEntitySystem->GetClassRegistry()->FindClass(pUser_Spell_Current.spell_projectles[i].c_str()), false);
				if (pSpellProjecle)
				{
					CProjectile::SProjectileDesc projectileDesc(pActorUser->GetEntityId(), 0, 0, damage, 0, 0, 0, pSpellProjecle->GetHitType(), 0, false);
					pSpellProjecle->SetParams(projectileDesc);
					//CryLogAlways("Try to get spell projectle");
					CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pSpellProjecle);
					if (pSpellPhy)
					{
						//CryLogAlways("Try to Launch spell projectle, pos x %i y %i z %i", (int)spell_spwn_pos.x, (int)spell_spwn_pos.y, (int)spell_spwn_pos.z);
						Vec3 spell_spwn_dir_npv(ZERO);
						if (pUser_Spell_Current.spell_projectles_dir_offset[i].IsZero())
						{
							spell_spwn_dir_npv = spell_spwn_dir;
						}
						else
						{
							Vec3 dir_offset = pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_dir_offset[i];
							if (pUser_Spell_Current.spell_projectles_dir_offset_randomize)
							{
								Vec3 random_offset = pUser_Spell_Current.spell_projectles_dir_offset[i];
								Vec3 random_offset2 = random_offset;
								random_offset2 = random_offset2.Flip();
								random_offset = cry_random(random_offset2, random_offset);
								/*if (spell_bone_rot_info.IsIdentity())
								dir_offset = random_offset;
								else
								dir_offset = random_offset;*/
								dir_offset = pActorUser->GetEntity()->GetRotation() * random_offset;
							}
							spell_spwn_dir_npv = (spell_spwn_pos + (spell_spwn_dir + dir_offset) * 2.0f);
							spell_spwn_dir_npv = spell_spwn_dir_npv - spell_spwn_pos;
							spell_spwn_dir_npv.normalize();
							//set spawn pos after editing direction------
							spell_spwn_pos = (spell_spwn_pos + (spell_spwn_dir_npv * 0.0625f));
							//-------------------------------------------
						}
						Vec3 spell_spwn_pos_npv = spell_spwn_pos + (pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_pos_offset[i]);
						spell_spwn_vel = spell_spwn_dir_npv * 1.5;
						pSpellPhy->Launch(spell_spwn_pos_npv, spell_spwn_dir_npv, spell_spwn_vel, 1.0f);
						pSpellPhy->SetSpellDamage(damage);
						pSpellPhy->spell_frc = pUser_Spell_Current.spell_force;
						for (int ic = 0; ic < 15; ic++)
						{
							if (pUser_Spell_Current.spell_additive_effects_ids[i][ic] > 0)
							{
								pSpellPhy->additive_effects_on_hit[ic] = pUser_Spell_Current.spell_additive_effects_ids[i][ic];
								if (pUser_Spell_Current.start_time > 0.0f)
									pSpellPhy->additive_effects_times[ic] = spell_time;
								else
									pSpellPhy->additive_effects_times[ic] = 0.0f;
							}
						}

						if (!pUser_Spell_Current.spell_act_scr_func[2].empty())
						{
							pSpellPhy->fnc_param_on_hit[1] = pUser_Spell_Current.spell_act_scr_func[2];
							pSpellPhy->fnc_param_on_hit[2] = pUser_Spell_Current.spell_act_scr_func_arg[2];
						}
					}
					else
					{
						//CryLogAlways("Try to Launch nospell projectle, pos x %i y %i z %i", (int)spell_spwn_pos.x, (int)spell_spwn_pos.y, (int)spell_spwn_pos.z);
						Vec3 spell_spwn_dir_npv(ZERO);
						if (pUser_Spell_Current.spell_projectles_dir_offset[i].IsZero())
						{
							spell_spwn_dir_npv = spell_spwn_dir;
						}
						else
						{
							Vec3 dir_offset = pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_dir_offset[i];
							spell_spwn_dir_npv = (spell_spwn_pos + (spell_spwn_dir + dir_offset) * 2.0f);
							spell_spwn_dir_npv = spell_spwn_dir_npv - spell_spwn_pos;
							spell_spwn_dir_npv.normalize();
						}
						Vec3 spell_spwn_pos_npv = spell_spwn_pos + (pActorUser->GetEntity()->GetRotation() * pUser_Spell_Current.spell_projectles_pos_offset[i]);
						spell_spwn_vel = spell_spwn_dir_npv * 1.5;
						pSpellProjecle->Launch(spell_spwn_pos_npv, spell_spwn_dir_npv, spell_spwn_vel, 1.0f);
					}
				}
			}
		}
	}
	Aoe_pos[cast_slot] = Vec3(ZERO);
}

void CActorSpellsActions::UpdateProjectedParticleAttachments(int cast_slot)
{
	if (!pActorUser)
		return;

	//int i = cast_slot;
	//CryLogAlways("CActorSpellsActions::UpdateProjectedParticleAttachments");
	for (int i = cast_slot; i < cast_slot+1; i++)
	{
		if(i != cast_slot)
			break;
		/*CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[i]);
		if (pUser_Spell_Current.spell_id == -1)
		{
			continue;
		}
		*/

		if (particle_attachments_on_user_actor[i].empty())
			continue;

		if (!pActorUser->IsAttachmentOnCharacter(0, particle_attachments_on_user_actor[i].c_str()))
			continue;

		ICharacterInstance *pCharacter = pActorUser->GetEntity()->GetCharacter(0);
		if (!pCharacter)
			break;

		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		if (!pIAttachmentManager)
			break;

		IAttachment *pAx = pIAttachmentManager->GetInterfaceByName(particle_attachments_on_user_actor[i].c_str());
		if (pAx)
		{
			if (pAx->IsAttachmentHidden())
			{
				pAx->HideAttachment(0);
			}
			//CryLogAlways("CActorSpellsActions::UpdateProjectedParticleAttachments seq 1");
			QuatTS qts;
			qts.SetIdentity();
			qts = pAx->GetAttWorldAbsolute();
			if (!lc_emm[i])
			{
				if (CEffectAttachment *pEffectAttachment = (CEffectAttachment*)pAx->GetIAttachmentObject())
				{
					lc_emm[i] = pEffectAttachment->GetEmitter();
					if (!lc_emm[i])
						continue;

					if (IMovementController * pMC = pActorUser->GetMovementController())
					{
						Vec3 direct(0, 0, 0);
						Vec3 pos(0, 0, 0);
						Vec3 pos2(0, 0, 0);
						SMovementState info;
						pMC->GetMovementState(info);
						direct = info.fireDirection;
						/*pos = pActorUser->GetBonePosition("Bip01 Neck");
						if (pos.IsZero())
							pos = pActorUser->GetBonePosition("Bip01 Spine3");

						if (pos.IsZero())
							pos = pActorUser->GetEntity()->GetWorldPos();*/

						Quat qts2 = Quat::CreateRotationVDir(direct);
						qts.q = qts2;
						lc_emm[i]->SetLocation(qts);
					}

					if (lc_emm[i]->GetEffect() && gEnv->p3DEngine && gEnv->p3DEngine->GetParticleManager())
					{
						const char* cccas = lc_emm[i]->GetEffect()->GetFullName().c_str();
						//CryLogAlways("try to create particle w name = %s", cccas);
						IParticleEffect *pcEff = gEnv->p3DEngine->GetParticleManager()->FindEffect(cccas);
						ParticleLoc ppo = ParticleLoc(lc_emm[i]->GetLocation());
						//lc_emm[i] = pcEff->Spawn(false, ppo);
						if (lc_emm[i])
						{
							//CryLogAlways("particle spawned");
							//lc_emm[i]->SetEntity(pActorUser->GetEntity(), 0);
						}
						//lc_emm[i] = gEnv->p3DEngine->GetParticleManager()->CreateEmitter(ParticleLoc(lc_emm[i]->GetLocation()), lc_emm[i]->GetEffect()->GetParticleParams(), &lc_emm[i]->GetSpawnParams());
					}
				}
			}

			if (pAx->GetIAttachmentObject() && !pAx->IsAttachmentHiddenInShadow())
			{
				pAx->HideInShadow(true);
				//pAx->ClearBinding();
				//CryLogAlways("spell attachment cleared");
				//continue;
			}

			if (!lc_emm[i])
				continue;

			//CryLogAlways("CActorSpellsActions::UpdateProjectedParticleAttachments seq 2");
			if (IMovementController * pMC = pActorUser->GetMovementController())
			{
				Vec3 direct(0, 0, 0);
				SMovementState info;
				pMC->GetMovementState(info);
				direct = info.fireDirection;
				Quat qts2 = Quat::CreateRotationVDir(direct);
				qts.q = qts2;
				//CryLogAlways("pre spell particle set dir");
				if(lc_emm[i])
					lc_emm[i]->SetLocation(qts);
				//CryLogAlways("spell particle set dir");
			}
		}
	}
}

void CActorSpellsActions::DeleteProjectedParticleEmitters(int cast_slot)
{
	//CryLogAlways("CActorSpellsActions::DeleteProjectedParticleEmitters");
	if (!lc_emm[cast_slot])
		return;

	if (gEnv->p3DEngine && gEnv->p3DEngine->GetParticleManager())
	{
		lc_emm[cast_slot]->Activate(false);
		if (g_pGameCVars->g_enable_spellparticle_deleting_emitter > 0)
		{
			gEnv->p3DEngine->GetParticleManager()->DeleteEmitter(lc_emm[cast_slot]);
		}
		else
		{
			emitters_to_delete.push_back(lc_emm[cast_slot]);
		}
		lc_emm[cast_slot] = NULL;
	}
}

void CActorSpellsActions::PlayCastAction(const char * action, int cast_slot, EntityId itemId, bool end_of_cast)
{
	if (!pActorUser)
		return;

	if(cast_slot <= 0)
		return;

	CItem *pItem = pActorUser->GetItem(itemId);
	if (!pItem)
		return;

	FragmentID fragmentId = pItem->GetFragmentID(action);
	if (m_CastSpellActions[cast_slot] != NULL)
	{
		//m_CastSpellActions[cast_slot]->ForceFinish();
		//if (cast_slot == 1)
			m_CastSpellActions[cast_slot] = NULL;
	}
	TagState tagState = TAG_STATE_EMPTY;
	if (!end_of_cast)
	{
		/*if(cast_slot == 2)
			m_CastSpellActions[cast_slot] = new CItemAction(PP_PlayerAction, fragmentId, tagState, IAction::Interruptable | IAction::NoAutoBlendOut);
		else
			m_CastSpellActions[cast_slot] = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState);*/

		m_CastSpellActions[cast_slot] = new CItemAction(PP_PlayerAction, fragmentId, tagState);
	}
	else
	{
		/*if (m_CastSpellActions[cast_slot] && cast_slot == 2)
		{
			m_CastSpellActions[cast_slot]->Interrupt();
			m_CastSpellActions[cast_slot]->Stop();
			m_CastSpellActions[cast_slot] = NULL;
		}*/
		m_CastSpellActions[cast_slot] = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState, IAction::BlendOut);
		
		/*if(cast_slot == 2)
			m_CastSpellActions[cast_slot] = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState, IAction::BlendOut);
		else
			m_CastSpellActions[cast_slot] = new CItemAction(PP_HitReaction, fragmentId, tagState);*/
	}

	if (m_CastSpellActions[cast_slot] != NULL)
		pItem->PlayFragment(m_CastSpellActions[cast_slot]);

	if (end_of_cast)
	{
		m_CastSpellActions[cast_slot] = NULL;
	}
}

void CActorSpellsActions::RequestToPreEndCast(int cast_slot, bool forced)
{
	//if(spell_cast_slots_delays[cast_slot] <= 0.0f)
	//	return;

	if(current_cast_slots[cast_slot] == -1)
		return;

	if (cast_slot > 2)
		return;

	if (!pActorUser)
		return;

	//if (pActorUser->IsRemote())
	//	forced = true;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (!pItem)
		return;

	const CGameXMLSettingAndGlobals::SActorSpell &pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[cast_slot]);
	if (pUser_Spell_Current.spell_id > -1)
	{
		if (pUser_Spell_Current.spell_cast_time_to_can_cast > 0.0f)
		{
			if (pUser_Spell_Current.spell_sub_type == eSpell_type_cast_continuosly_on_target)
			{
				if (forced)
				{
					if (pUser_Spell_Current.spell_cast_recoil > 0.0f)
					{
						spell_cast_recoils[cast_slot] = pUser_Spell_Current.spell_cast_recoil;
					}
					spell_cast_slots_delays[cast_slot] = 0.0f;
					//---------MP stuff----------
					if (!pActorUser->IsRemote())
					{
						if (gEnv->bMultiplayer)
						{
							if (gEnv->bServer)
								pActorUser->GetGameObject()->InvokeRMI(CActor::ClEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, forced), eRMI_ToRemoteClients);
							else
							{
								pActorUser->GetGameObject()->InvokeRMI(CActor::SVEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, forced), eRMI_ToServer);
							}
						}
					}

					if (gEnv->bMultiplayer)
						pActorUser->SetSAPlayTime(0.1f);
					//---------MP stuff----------
					RequestToEndCurrentCast(cast_slot, false, false, true);
					return;
				}
			}

			if (current_cast_slots_timers[cast_slot] < pUser_Spell_Current.spell_cast_time_to_can_cast)
			{
				if (pActorUser->IsRemote())
				{
					current_cast_slots_timers[cast_slot] = pUser_Spell_Current.spell_cast_time_to_can_cast;
				}
				else
				{
					return;
				}
			}
		}

		if (spell_cast_slots_requred_to_cast[cast_slot])
			return;

		if (!pUser_Spell_Current.spell_cast_action_pre_endcast[cast_slot].empty())
		{
			if (pUser_Spell_Current.spell_sub_type != eSpell_type_cast_continuosly_on_target)
			{
				PlayCastAction(pUser_Spell_Current.spell_cast_action_pre_endcast[cast_slot].c_str(), cast_slot, pItem->GetEntityId(), true);
			}
		}

		spell_cast_slots_requred_to_cast[cast_slot] = true;
	}
	//---------MP stuff----------
	if (!pActorUser->IsRemote())
	{
		if (gEnv->bMultiplayer)
		{
			if (gEnv->bServer)
				pActorUser->GetGameObject()->InvokeRMI(CActor::ClEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, forced), eRMI_ToRemoteClients);
			else
			{
				pActorUser->GetGameObject()->InvokeRMI(CActor::SVEndCastSpell(), CActor::SNetEndCastSlotPrm(cast_slot, forced), eRMI_ToServer);
			}
		}
	}

	if (gEnv->bMultiplayer)
		pActorUser->SetSAPlayTime(0.1f);
	//---------MP stuff----------
}

void CActorSpellsActions::RequestToEndCurrentCast(int cast_slot, bool all_slots, bool simple_reset, bool with_end_action)
{
	if (cast_slot > 2)
		return;

	if (!pActorUser)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (!pItem)
		return;

	if (spell_cast_slots_delays[cast_slot] > 0.0f)
		return;

	if (simple_reset)
	{
		current_cast_slots[cast_slot] = -1;
		current_cast_slots_timers[cast_slot] = 0.0f;
		spell_cast_slots_delays[cast_slot] = 0.0f;
		spell_cast_slots_requred_to_cast[cast_slot] = false;
		return;
	}
	int slot_to_use = cast_slot;
	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[slot_to_use]);
	if (pUser_Spell_Current.spell_id > -1)
	{
		if (with_end_action)
		{
			PlayCastAction(pUser_Spell_Current.spell_cast_action_endcast[cast_slot].c_str(), cast_slot, pItem->GetEntityId(), true);
		}

		for (int i = 0; i < 5; i++)
		{
			bool have_particle = false;
			if (pActorUser->IsThirdPerson())
			{
				if (!pUser_Spell_Current.spell_particles[slot_to_use][i].empty())
					have_particle = true;
			}
			else
			{
				if (!pUser_Spell_Current.spell_particles_fp[slot_to_use][i].empty())
					have_particle = true;
			}

			if (have_particle)
			{
				ICharacterInstance *pCharacter = pActorUser->GetEntity()->GetCharacter(0);
				if (pCharacter)
				{
					int32 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(pUser_Spell_Current.spell_helpers[slot_to_use][i].c_str());
					if (id != -1)
					{
						string att_name = "spell_particle";
						string slot_name = "";
						if (slot_to_use == 1)
							slot_name = "_rigth_";
						else if (slot_to_use == 2)
							slot_name = "_left_";

						char B_valstr[17];
						itoa(i, B_valstr, 10);
						string C_valstr = B_valstr;
						att_name = att_name + slot_name + C_valstr;
						pActorUser->DeleteParticleAttachment(0, att_name, false);
						if (i == g_pGameCVars->g_spell_system_ext_particle_id_to_cl_direction)
						{
							DeleteProjectedParticleEmitters(slot_to_use);
							particle_attachments_on_user_actor[slot_to_use] = "";
						}
					}
					else
					{
						if (slot_to_use == 1)
						{
							if (pActorUser->GetCurrentItemId() > 0)
							{

							}
						}
						else if (slot_to_use == 2)
						{
							if (pActorUser->GetAnyItemInLeftHand() > 0)
							{

							}
						}
					}
				}
			}
		}
	}
	
	if (slot_to_use == 1)
	{
		//pItem->SetItemFlag(CItem::eIF_BlockActions, false);
		pActorUser->m_lft_hand_blc = false;
		if (pUser_Spell_Current.two_handed_spell)
		{
			pActorUser->m_rgt_hand_blc = false;
		}
	}
	else if (slot_to_use == 2)
	{
		CItem *pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetAnyItemInLeftHand()));
		if (pItem_lft)
		{
			//pItem_lft->SetItemFlag(CItem::eIF_BlockActions, false);
		}
		pActorUser->m_rgt_hand_blc = false;
		if (pUser_Spell_Current.two_handed_spell)
		{
			pActorUser->m_lft_hand_blc = false;
		}
	}

	//event/////////////////////////
	OnEndCastSpell(cast_slot);
	////////////////////////////////
	current_cast_slots[cast_slot] = -1;
	current_cast_slots_timers[cast_slot] = 0.0f;
	spell_cast_slots_delays[cast_slot] = 0.0f;
	spell_cast_slots_requred_to_cast[cast_slot] = false;
}

bool CActorSpellsActions::IsCurrentSlotBusy(int cast_slot)
{
	if (cast_slot > 2)
		return true;

	if (!pActorUser)
		return true;

	if(current_cast_slots[cast_slot] == -1)
		return false;
	else
		return true;
}

bool CActorSpellsActions::CanCastSpellFromSlot(int cast_slot)
{
	if(IsCurrentSlotBusy(cast_slot))
		return false;

	CPlayer *pPlayer = (CPlayer*)(pActorUser);
	if (pPlayer)
	{
		if (pPlayer->IsOnLedge() || pPlayer->IsOnLadder())
			return false;
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetCurrentItemId()));
	if (!pItem)
		return false;

	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(cast_slot);
	if (pUser_Spell_Current.spell_id == -1)
	{
		for (int i = 0; i < MAX_USABLE_SPELLS_SLOTS; i++)
		{
			pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(i);
			if (pUser_Spell_Current.spell_id > 0)
			{
				break;
			}
			else
			{
				pUser_Spell_Current.spell_id = -1;
			}
		}
	}

	if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
	{
		CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current_ot;
		pUser_Spell_Current_ot.spell_id = -1;
		for (int i = 0; i < MAX_PER_CAST_SLOTS; i++)
		{
			pUser_Spell_Current_ot.spell_id = -1;
			if (current_cast_slots[i] <= -1)
				continue;

			pUser_Spell_Current_ot = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[i]);
			if (pUser_Spell_Current_ot.spell_id > 0)
			{
				if (pUser_Spell_Current_ot.two_handed_spell)
				{
					return false;
				}
			}
			else
				pUser_Spell_Current_ot.spell_id = -1;
		}
	}

	if (pUser_Spell_Current.spell_id > -1)
	{
		if (!pActorUser->IsRemote())
		{
			if ((pActorUser->GetHealth() - pUser_Spell_Current.spell_hp_cost) <= 0.0f)
			{
				return false;
			}
			else if ((pActorUser->GetMagicka() - pUser_Spell_Current.spell_mana_cost) <= 0.0f)
			{
				return false;
			}
			else if ((pActorUser->GetStamina() - pUser_Spell_Current.spell_stmn_cost) <= 0.0f)
			{
				return false;
			}
		}

		if (pUser_Spell_Current.two_handed_spell)
		{
			if (!pActorUser->CanDoAnyActionWNeededTwoHands())
				return false;
		}
		int slot_to_use = cast_slot;
		if (current_cast_slots[cast_slot] == -1)
		{
			if (spell_cast_slots_requred_to_cast[cast_slot])
				return false;

			if (spell_cast_slots_delays[cast_slot] > 0.0f)
				return false;
		}
		if (spell_cast_recoils[slot_to_use] > 0.0f)
		{
			return false;
		}
		if (slot_to_use == 1)
		{
			if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
			if (pActorUser->m_lft_hand_blc)
			{
				return false;
			}
			if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
			{
				if (pUser_Spell_Current.two_handed_spell)
				{
					return false;
				}
			}
			if (pItem && !pItem->IsItemIgnoredByHud() && pUser_Spell_Current.can_cast_w_weapon == 0)
			{
				return false;
			}
			if (pActorUser->m_blockactivenoshield || pActorUser->m_blockactiveshield)
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 3 && pActorUser->m_blockactiveshield)
				{
					return false;
				}
				else if (pActorUser->m_blockactivenoshield)
				{
					return false;
				}
			}
			if (!pItem->IsItemIgnoredByHud())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 0)
				{
					return false;
				}
			}
			if (pItem->IsWpnTwoHanded())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 2 && (pItem->GetWeaponType() != 10 && pItem->GetMagickPwrMult() <= 1.0f))
				{
					return false;
				}
			}
		}
		else if (slot_to_use == 2)
		{
			CItem *pItem_lft = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActorUser->GetAnyItemInLeftHand()));
			if (pItem_lft && pItem_lft->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
			if (pActorUser->m_rgt_hand_blc)
			{
				return false;
			}
			if (pActorUser->m_rgt_hand_blc || pActorUser->m_lft_hand_blc)
			{
				if (pUser_Spell_Current.two_handed_spell)
				{
					return false;
				}
			}
			if (pActorUser->m_blockactiveshield || pActorUser->m_blockactivenoshield)
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 3 && pActorUser->m_blockactivenoshield)
				{
					return false;
				}
				else if (pActorUser->m_blockactiveshield)
				{
					return false;
				}
			}
			if (pItem_lft && !pItem_lft->IsItemIgnoredByHud())
			{
				if (!pUser_Spell_Current.can_cast_w_weapon)
				{
					return false;
				}
			}
			if (pItem_lft && pItem_lft->IsWpnTwoHanded())
			{
				if (pUser_Spell_Current.can_cast_w_weapon == 2 && (pItem_lft->GetWeaponType() != 10 && pItem_lft->GetMagickPwrMult() <= 1.0f))
				{
					return false;
				}
			}
		}
	}
	return true;
}

void CActorSpellsActions::OnStartCastSpell(int cast_slot)
{
	if (!pActorUser)
		return;

	if (current_cast_slots[cast_slot] == -1)
		return;

	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[cast_slot]);
	if (pUser_Spell_Current.spell_id == -1)
	{
		return;
	}
	pActorUser->RequestToRunLuaFncFromActorScriptInstance("OnStartSpellCasting", pUser_Spell_Current.spell_name);
	if (pUser_Spell_Current.spell_sub_type == eSpell_type_cast_on_area)
	{
		current_cast_aoe_spell[cast_slot] = 1;
	}
}

void CActorSpellsActions::OnCastSpell(int cast_slot)
{
	if (!pActorUser)
		return;

	if(current_cast_slots[cast_slot] == -1)
		return;

	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[cast_slot]);
	if (pUser_Spell_Current.spell_id == -1)
	{
		return;
	}
	pActorUser->RequestToRunLuaFncFromActorScriptInstance("OnSpellCasted", pUser_Spell_Current.spell_name);
}

void CActorSpellsActions::OnEndCastSpell(int cast_slot)
{
	if (!pActorUser)
		return;

	if (current_cast_slots[cast_slot] == -1)
		return;

	CGameXMLSettingAndGlobals::SActorSpell pUser_Spell_Current = pActorUser->GetSpellFromCurrentSpellsForUseSlot(current_cast_slots[cast_slot]);
	if (pUser_Spell_Current.spell_id == -1)
	{
		return;
	}
	pActorUser->RequestToRunLuaFncFromActorScriptInstance("OnEndSpellCast", pUser_Spell_Current.spell_name);
}

void CActorSpellsActions::SetUser(CActor* pActor)
{
	if (pActor)
	{
		pActorUser = pActor;
	}
}

void CActorSpellsActions::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}
