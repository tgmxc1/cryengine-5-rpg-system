#pragma once

#ifndef __THROWABLE_WEAPON_H__
#define __THROWABLE_WEAPON_H__

#include "Weapon.h"
#include "FireModeParams.h"

class CThrowableWeapon : public CWeapon
{
	typedef CWeapon BaseClass;
public:

	CThrowableWeapon();
	~CThrowableWeapon();
	virtual bool Init(IGameObject * pGameObject);
	virtual void Update(SEntityUpdateContext &ctx, int slot);
	virtual void OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value);
	virtual void StartFire();
	virtual void StartFire(const SProjectileLaunchParams& launchParams);
	virtual void StopFire();
};
#endif