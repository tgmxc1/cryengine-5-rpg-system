#include "StdAfx.h"
#include "PlayerCharacterCreatingSys.h"
#include <CrySystem/XML/IXml.h>
#include "Actor.h"
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "GameXMLSettingAndGlobals.h"
#include "UI/HUD/HUDCommon.h"

#include "PlayerInput.h"

#pragma warning(push)
#pragma warning(disable : 4244)

CPlayerCharacterCreatingSys::CPlayerCharacterCreatingSys()
{
	//CryLogAlways("CPlayerCharacterCreatingSys::CPlayerCharacterCreatingSys() constructor called");
	net_selected_character_name = "";
}

CPlayerCharacterCreatingSys::~CPlayerCharacterCreatingSys()
{

}

void CPlayerCharacterCreatingSys::Init()
{

}

void CPlayerCharacterCreatingSys::Update()
{

}

void CPlayerCharacterCreatingSys::PlayerFaceSelectionNext()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_head = "headface";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0].empty())
	{
		base_att_head = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0];
	}

	if (pClient->GetHeadType() > 1)
		pClient->SetHeadType(pClient->GetHeadType() - 1);

	if (pClient->GetGender() == 1)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(1);
	pClient->CreateSkinAttachment(0, base_att_head.c_str(), path.c_str());
	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerFaceSelectionPrev()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_head = "headface";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0].empty())
	{
		base_att_head = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0];
	}

	if (pClient->GetHeadType() < g_pGameCVars->g_pl_customization_na_heads)
		pClient->SetHeadType(pClient->GetHeadType() + 1);

	if (pClient->GetGender() == 1)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(1);
	pClient->CreateSkinAttachment(0, base_att_head.c_str(), path.c_str());
	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerHairSelectionNext()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_hair = "hair";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1].empty())
	{
		base_att_hair = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1];
	}

	if (pClient->GetHairType() > 1)
		pClient->SetHairType(pClient->GetHairType() - 1);

	if (pClient->GetGender() == 1)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}

	string pathhair = pClient->GetActorBodyPartModelPath(6);
	pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerHairSelectionPrev()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if(!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_hair = "hair";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1].empty())
	{
		base_att_hair = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1];
	}

	if (pClient->GetHairType() < g_pGameCVars->g_pl_customization_na_hairs)
		pClient->SetHairType(pClient->GetHairType() + 1);

	if (pClient->GetGender() == 1)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}

	string pathhair = pClient->GetActorBodyPartModelPath(6);
	pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerEyesSelectionNext()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0].empty())
	{
		base_att_eye_right = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0];
	}

	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1].empty())
	{
		base_att_eye_left = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1];
	}

	if (pClient->GetEyesType() > 1)
		pClient->SetEyesType(pClient->GetEyesType() - 1);

	if (pClient->GetGender() == 1)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(10);
	string path2 = pClient->GetActorBodyPartModelPath(11);
	if (0 == stricmp(PathUtil::GetExt(path.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_left.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_left.c_str(), path.c_str());
	}

	if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_right.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_right.c_str(), path2.c_str());
	}
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerEyesSelectionPrev()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0].empty())
	{
		base_att_eye_right = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0];
	}

	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1].empty())
	{
		base_att_eye_left = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1];
	}

	if (pClient->GetEyesType() < g_pGameCVars->g_pl_customization_na_eyes)
		pClient->SetEyesType(pClient->GetEyesType() + 1);

	if (pClient->GetGender() == 1)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}
	else if (pClient->GetGender() == 0)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(10);
	string path2 = pClient->GetActorBodyPartModelPath(11);
	if (0 == stricmp(PathUtil::GetExt(path.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_left.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_left.c_str(), path.c_str());
	}

	if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_right.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_right.c_str(), path2.c_str());
	}
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::PlayerGenderSelectionMale()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	short skl_slot_to_load = 0;
	if (g_pGameCVars->g_Pl_underwear_mod == 1)
		skl_slot_to_load = 1;

	pClient->SetEyesType(1);
	pClient->SetHeadType(1);
	pClient->SetHairType(1);
	pClient->SetGender(eCG_Male);
	IEntity *pEntity = pClient->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	propsStat->SetValue("gender", eCG_Male);
	string model_file = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_skeletion[skl_slot_to_load];
	pClient->SetActorModel(model_file.c_str(), true);
	pClient->UpdateActorModelFTO();
	pClient->SetPlayerModelStr(model_file.c_str());
	OnOptionSelected();
	CPlayerInput *pPlayerInput = reinterpret_cast<CPlayerInput *>(pClient->GetPlayerInput());
	if (pPlayerInput && !gEnv->bMultiplayer)
	{
		pPlayerInput->EnterInRelaxedMode();
	}
}

void CPlayerCharacterCreatingSys::PlayerGenderSelectionFemale()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	short skl_slot_to_load = 0;
	if (g_pGameCVars->g_Pl_underwear_mod == 1)
		skl_slot_to_load = 1;

	pClient->SetEyesType(1);
	pClient->SetHeadType(1);
	pClient->SetHairType(1);
	pClient->SetGender(eCG_Female);
	IEntity *pEntity = pClient->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	propsStat->SetValue("gender", eCG_Female);
	string model_file = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_skeletion[skl_slot_to_load];
	pClient->SetActorModel(model_file.c_str(), true);
	pClient->UpdateActorModelFTO();
	pClient->SetPlayerModelStr(model_file.c_str());
	OnOptionSelected();
	CPlayerInput *pPlayerInput = reinterpret_cast<CPlayerInput *>(pClient->GetPlayerInput());
	if (pPlayerInput && !gEnv->bMultiplayer)
	{
		pPlayerInput->EnterInRelaxedMode();
	}
}

void CPlayerCharacterCreatingSys::RemoteActorGenderSelectionMale(EntityId actor_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id));
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	short skl_slot_to_load = 0;
	if (g_pGameCVars->g_Pl_underwear_mod == 1)
		skl_slot_to_load = 1;

	pClient->SetEyesType(1);
	pClient->SetHeadType(1);
	pClient->SetHairType(1);
	pClient->SetGender(eCG_Male);
	IEntity *pEntity = pClient->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	propsStat->SetValue("gender", eCG_Male);
	string model_file = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_skeletion[skl_slot_to_load];
	pClient->SetActorModel(model_file.c_str(), true);
	pClient->UpdateActorModelFTO();
	pClient->SetPlayerModelStr(model_file.c_str());
}

void CPlayerCharacterCreatingSys::RemoteActorGenderSelectionFemale(EntityId actor_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id));
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	short skl_slot_to_load = 0;
	if (g_pGameCVars->g_Pl_underwear_mod == 1)
		skl_slot_to_load = 1;

	pClient->SetEyesType(1);
	pClient->SetHeadType(1);
	pClient->SetHairType(1);
	pClient->SetGender(eCG_Female);
	IEntity *pEntity = pClient->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	propsStat->SetValue("gender", eCG_Female);
	string model_file = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_skeletion[skl_slot_to_load];
	pClient->SetActorModel(model_file.c_str(), true);
	pClient->UpdateActorModelFTO();
	pClient->SetPlayerModelStr(model_file.c_str());
}

void CPlayerCharacterCreatingSys::OnOptionSelected()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	CHUDCommon *pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (!pHud->m_CharacterCreationVisible)
		return;

	if (pClient->GetGender() == eCG_Female)
	{
		CHUDCommon::SCharacterCreatingUIInfo charInf;
		charInf.hair_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_hair_info[pClient->GetHairType()];
		charInf.head_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_face_info[pClient->GetHeadType()];
		charInf.eyes_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_info[pClient->GetEyesType()];
		pHud->UpdateCharCreatingUI(charInf);
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		CHUDCommon::SCharacterCreatingUIInfo charInf;
		charInf.hair_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_hair_info[pClient->GetHairType()];
		charInf.head_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_face_info[pClient->GetHeadType()];
		charInf.eyes_info = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_info[pClient->GetEyesType()];
		pHud->UpdateCharCreatingUI(charInf);
	}
}

void CPlayerCharacterCreatingSys::SetFace(int faceId)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (faceId > g_pGameCVars->g_pl_customization_na_heads)
		return;

	string base_att_head = "headface";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0].empty())
	{
		base_att_head = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0];
	}
	pClient->SetHeadType(faceId);
	if (pClient->GetGender() == eCG_Female)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(1);
	pClient->CreateSkinAttachment(0, base_att_head.c_str(), path.c_str());
	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::SetHair(int hairId)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (hairId > g_pGameCVars->g_pl_customization_na_hairs)
		return;

	string base_att_hair = "hair";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1].empty())
	{
		base_att_hair = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1];
	}
	pClient->SetHairType(hairId);
	if (pClient->GetGender() == eCG_Female)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}

	string pathhair = pClient->GetActorBodyPartModelPath(6);
	if (0 == stricmp(PathUtil::GetExt(pathhair.c_str()), "cgf"))
	{
		pClient->CreateBoneAttachment(0, base_att_hair.c_str(), pathhair.c_str(), "Bip01 Head", true, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(pathhair.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	}
	else
	{
		pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	}

	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::SetEyes(int eyesId)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if(eyesId > g_pGameCVars->g_pl_customization_na_eyes)
		return;

	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0].empty())
	{
		base_att_eye_right = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0];
	}

	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1].empty())
	{
		base_att_eye_left = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1];
	}
	pClient->SetEyesType(eyesId);
	if (pClient->GetGender() == eCG_Female)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(10);
	string path2 = pClient->GetActorBodyPartModelPath(11);
	if (0 == stricmp(PathUtil::GetExt(path.c_str()), "cgf"))
	{
		if(!pClient->IsAttachmentOnCharacter(0, base_att_eye_left.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_left.c_str(), path.c_str());
	}

	if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_right.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_right.c_str(), path2.c_str());
	}

	//event
	OnOptionSelected();
}

void CPlayerCharacterCreatingSys::SetFaceForRemoteActor(int faceId, EntityId actor_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id));
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (faceId > g_pGameCVars->g_pl_customization_na_heads)
		return;

	string base_att_head = "headface";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0].empty())
	{
		base_att_head = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[0];
	}
	pClient->SetHeadType(faceId);
	if (pClient->GetGender() == eCG_Female)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_face[pClient->GetHeadType()];
		pClient->SetActorBodyPartModelPath(1, path.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(1);
	pClient->CreateSkinAttachment(0, base_att_head.c_str(), path.c_str());
}

void CPlayerCharacterCreatingSys::SetHairForRemoteActor(int hairId, EntityId actor_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id));
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (hairId > g_pGameCVars->g_pl_customization_na_hairs)
		return;

	string base_att_hair = "hair";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1].empty())
	{
		base_att_hair = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_head[1];
	}
	pClient->SetHairType(hairId);
	if (pClient->GetGender() == eCG_Female)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string hairpath = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_hair[pClient->GetHairType()];
		pClient->SetActorBodyPartModelPath(6, hairpath.c_str());
	}

	string pathhair = pClient->GetActorBodyPartModelPath(6);
	if (0 == stricmp(PathUtil::GetExt(pathhair.c_str()), "cgf"))
	{
		pClient->CreateBoneAttachment(0, base_att_hair.c_str(), pathhair.c_str(), "Bip01 Head", true, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(pathhair.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	}
	else
	{
		pClient->CreateSkinAttachment(0, base_att_hair.c_str(), pathhair.c_str());
	}
}

void CPlayerCharacterCreatingSys::SetEyesForRemoteActor(int eyesId, EntityId actor_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id));
	if (!pClient)
		return;

	if (!g_pGame->GetGameXMLSettingAndGlobals())
		return;

	if (eyesId > g_pGameCVars->g_pl_customization_na_eyes)
		return;

	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0].empty())
	{
		base_att_eye_right = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[0];
	}

	if (!g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1].empty())
	{
		base_att_eye_left = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_base_attachment_eyes[1];
	}
	pClient->SetEyesType(eyesId);
	if (pClient->GetGender() == eCG_Female)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_fem_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}
	else if (pClient->GetGender() == eCG_Male)
	{
		string path = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_lft[pClient->GetEyesType()];
		string path2 = g_pGame->GetGameXMLSettingAndGlobals()->customization_data.character_man_eye_rgt[pClient->GetEyesType()];
		pClient->SetActorBodyPartModelPath(10, path.c_str());
		pClient->SetActorBodyPartModelPath(11, path2.c_str());
	}

	string path = pClient->GetActorBodyPartModelPath(10);
	string path2 = pClient->GetActorBodyPartModelPath(11);
	if (0 == stricmp(PathUtil::GetExt(path.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_left.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_left.c_str(), path.c_str(), "eye_left_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_left.c_str(), path.c_str());
	}

	if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "cgf"))
	{
		if (!pClient->IsAttachmentOnCharacter(0, base_att_eye_right.c_str()))
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", true, false);
		else
			pClient->CreateBoneAttachment(0, base_att_eye_right.c_str(), path2.c_str(), "eye_right_bone", false, false);
	}
	else if (0 == stricmp(PathUtil::GetExt(path2.c_str()), "skin"))
	{
		pClient->CreateSkinAttachment(0, base_att_eye_right.c_str(), path2.c_str());
	}
}

void CPlayerCharacterCreatingSys::OnOpenedCharacterCreationMenu()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (!pClient->IsThirdPerson())
		pClient->ToggleThirdPerson();

	g_pGameCVars->cl_tpvYaw = 179;

	pClient->ResetActorVars();
	pClient->m_player_name = "";
	pClient->SetPlayerModelStr("");
	pClient->InvalidateCurrentModelName();
	net_selected_character_name = "";
	pClient->reequip_timer_cn1 = 0.0f;
	pClient->rload_pl_model_timer_after_ps = 0.0f;
	pClient->serialization_reinit_customization_setting_on_character = 0.0f;
	pClient->SetActorModel("", false);
}

void CPlayerCharacterCreatingSys::OnClosedCharacterCreationMenu()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if (g_pGameCVars->g_player_start_viev_cam_tpv == 0)
	{
		if (pClient->IsThirdPerson())
			pClient->ToggleThirdPerson();

		g_pGameCVars->cl_tpvYaw = 0;
		g_pGameCVars->cl_tpvDist = 3.5f;
	}
	net_selected_character_name = GetCharacterName(true);
	//CryLogAlways("CPlayerCharacterCreatingSys::OnCharacterCreationCompleteEvent() called");
	if (gEnv->bMultiplayer)
	{
		pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_RPG_INFO, 0.1f);
		//pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_EQUPMENT, 0.8f);
		pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_MGC, 1.0f);
		pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_STATS, 1.2f);
		pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_CURRENT_ITEM, 1.85f);
		pClient->StartDelayedSyncRPG_NET_State(CPlayer::ASPECT_HEALTH, 1.88f);
	}
}

void CPlayerCharacterCreatingSys::SetCharacterName(const char* name)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	string conv_nm = name;
	//-----------------------------------------------------------------------------
	conv_nm.replace(">", ""); conv_nm.replace("<", "");	conv_nm.replace("~", "");
	conv_nm.replace(".", ""); conv_nm.replace(",", "");	conv_nm.replace("?", "");
	conv_nm.replace("|", ""); conv_nm.replace("/", "");	conv_nm.replace("*", "");
	conv_nm.replace("-", ""); conv_nm.replace("+", "");	conv_nm.replace("=", "");
	conv_nm.replace("'", ""); conv_nm.replace("[", "");	conv_nm.replace("]", "");
	conv_nm.replace("&", ""); conv_nm.replace("{", "");	conv_nm.replace("}", "");
	conv_nm.replace("^", ""); conv_nm.replace("(", "");	conv_nm.replace(")", "");
	conv_nm.replace("%", ""); conv_nm.replace("$", "");	conv_nm.replace("#", "");
	conv_nm.replace("@", ""); conv_nm.replace("!", "");	conv_nm.replace(":", "");
	conv_nm.replace(";", ""); conv_nm.replace("�", "");
	////////////////////////////////////////////////////////////////////////////////
	pClient->m_player_name = conv_nm;
}

string CPlayerCharacterCreatingSys::GetCharacterName(bool addXmlExt)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return "";

	if (addXmlExt)
	{
		string ext = ".xml";
		string name = pClient->m_player_name + ext;
		return name;
	}
	else
	{
		return pClient->m_player_name;
	}
}

void CPlayerCharacterCreatingSys::ReLoadPlayerCharacterCsetting()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	string old_file = PathUtil::GetFileName(pClient->GetPlayerModelStr());
	string start_file = "player_invisible";
	if (old_file.empty() || old_file == start_file || pClient->GetPlayerModelStr().empty())
	{
		return;
	}

	if (pClient->m_player_name.empty())
		return;

	SetEyes(pClient->GetEyesType());
	SetHair(pClient->GetHairType());
	SetFace(pClient->GetHeadType());
}

void CPlayerCharacterCreatingSys::ReLoadRemotePlayerCharacterCsetting(EntityId actor_id)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actor_id);
	if (!pActor)
		return;

	CPlayer *pClient = static_cast<CPlayer*>(pActor);
	if (pClient)
	{
		string old_file = PathUtil::GetFileName(pClient->GetPlayerModelStr());
		string start_file = "player_invisible";
		if (old_file.empty() || old_file == start_file || pClient->GetPlayerModelStr().empty())
		{
			return;
		}

		if(pClient->m_player_name.empty())
			return;
	}
	SetEyesForRemoteActor(pActor->GetEyesType(), actor_id);
	SetHairForRemoteActor(pActor->GetHairType(), actor_id);
	SetFaceForRemoteActor(pActor->GetHeadType(), actor_id);

}

void CPlayerCharacterCreatingSys::SaveCharacterToXmlFile(const char* filename)
{
	IPlayerProfileManager* profileManager = g_pGame->GetIGameFramework()->GetIPlayerProfileManager();
	if (!profileManager)
		return;

	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	string pl_nmn___s = pClient->m_player_name;
	pl_nmn___s.replace(" ", "_");
	string path_to_file = profileManager->GetSharedSaveGameFolder();
	path_to_file = PathUtil::AddSlash(path_to_file);
	path_to_file = path_to_file + string("PlayerCharacters");
	path_to_file = PathUtil::AddSlash(path_to_file);
	string path_to_file_saved_chrs = path_to_file;
	path_to_file.append(filename);
	path_to_file_saved_chrs.append("saved_characters.xml");
	XmlNodeRef SavedChrsNode = GetISystem()->LoadXmlFromFile(path_to_file_saved_chrs);
	if (!SavedChrsNode)
	{
		SavedChrsNode = GetISystem()->CreateXmlNode("saved_characters");
	}

	if (SavedChrsNode)
	{
		XmlString ftvx = "";
		int ch_count = SavedChrsNode->getChildCount();
		XmlNodeRef SavedChrNode = NULL;
		bool resaved_info = false;
		for (int i = 0; i < ch_count; ++i)
		{
			SavedChrNode = SavedChrsNode->getChild(i);
			if (SavedChrNode)
			{
				if (SavedChrNode->getAttr("name", ftvx))
				{
					string pl_nmn___s2 = ftvx;
					pl_nmn___s2.replace(" ", "_");
					if (pl_nmn___s2 == pl_nmn___s)
					{
						SavedChrNode->setAttr("name", pl_nmn___s);
						SavedChrNode->setAttr("file", filename);
						SavedChrNode->setAttr("level", pClient->GetLevel());
						resaved_info = true;
						break;
					}
				}
			}
		}

		if (!resaved_info)
		{
			SavedChrNode = SavedChrsNode->newChild("character");
			if (SavedChrNode)
			{
				SavedChrNode->setAttr("name", pl_nmn___s);
				SavedChrNode->setAttr("file", filename);
				SavedChrNode->setAttr("level", pClient->GetLevel());
			}
		}
		SavedChrsNode->saveToFile(path_to_file_saved_chrs);
	}

	XmlNodeRef Character_node = GetISystem()->LoadXmlFromFile(path_to_file);
	if (!Character_node)
	{
		Character_node = GetISystem()->CreateXmlNode("SChrx");
	}
	else
	{
		Character_node = GetISystem()->CreateXmlNode("SChrx");
		/*for (int i = 0; i < Character_node->getChildCount(); ++i)
		{
			Character_node->getChild(i)->deleteChildAt(i);
		}*/
	}

	if(!Character_node)
		return;

	XmlNodeRef nameX_node = Character_node->newChild("CharacterName");
	nameX_node->setAttr("name", pl_nmn___s);

	XmlNodeRef ds_node = Character_node->newChild("CharacterInfo");
	XmlNodeRef ds_node_stats = ds_node->newChild("Stats");
	if (ds_node_stats)
	{
		XmlNodeRef ds_node_stats_base = ds_node_stats->newChild("Base_Attribles");
		if (ds_node_stats_base)
		{
			ds_node_stats_base->setAttr("CharacterPhysicalStat_Strength", pClient->GetStrength());
			ds_node_stats_base->setAttr("CharacterPhysicalStat_Agility", pClient->GetAgility());
			ds_node_stats_base->setAttr("CharacterPhysicalStat_Endurance", pClient->GetEndurance());
			ds_node_stats_base->setAttr("CharacterPhysicalStat_HealthMaximum", (int)pClient->GetMaxHealth());
			ds_node_stats_base->setAttr("CharacterPhysicalStat_StaminaMaximum", pClient->GetMaxStamina());
			ds_node_stats_base->setAttr("CharacterMindStat_Intelligence", pClient->GetIntelligence());
			ds_node_stats_base->setAttr("CharacterMindStat_Willpower", pClient->GetWillpower());
			ds_node_stats_base->setAttr("CharacterMindStat_MagickaMaximum", pClient->GetMaxMagicka());
			ds_node_stats_base->setAttr("CharacterCombinedStat_Personality", pClient->GetPersonality());
		}
		XmlNodeRef ds_node_stats_level_info = ds_node_stats->newChild("Level_Info");
		if (ds_node_stats_level_info)
		{
			ds_node_stats_level_info->setAttr("Character_Level", pClient->GetLevel());
			ds_node_stats_level_info->setAttr("Character_CurrentXp", pClient->GetLevelXp());
			ds_node_stats_level_info->setAttr("Character_XpToNextLevel", pClient->GetXpToNextLevel());
			ds_node_stats_level_info->setAttr("Character_AttrUpPoints", g_pGameCVars->g_leveluppointsstat);
			ds_node_stats_level_info->setAttr("Character_PerkUpPoints", g_pGameCVars->g_levelupabilitypoints);
		}
		XmlNodeRef ds_node_stats_social_info = ds_node_stats->newChild("Social_Info");
		if (ds_node_stats_social_info)
		{
			ds_node_stats_social_info->setAttr("Character_Gold", pClient->GetGold());
		}
	}
	XmlNodeRef ds_node_inventory = ds_node->newChild("Inventory");
	if (ds_node_inventory)
	{
		IInventory *pInventory = pClient->GetInventory();
		XmlNodeRef ds_node_inventory_equipped_items = ds_node_inventory->newChild("Equipped_Items");
		if (ds_node_inventory_equipped_items)
		{
			if (pInventory)
			{
				for (int i = 0; i < pInventory->GetCount() + 1; ++i)
				{
					CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pInventory->GetItem(i)));
					if (curItem)
					{
						if (pClient->GetCurrentItemId() != curItem->GetEntityId())
						{
							if (!(curItem->GetEquipped() == 1))
								continue;
						}

						XmlNodeRef this_item = ds_node_inventory_equipped_items->newChild(curItem->GetEntity()->GetClass()->GetName());
						if (!this_item)
							continue;

						this_item->setAttr("item_quantity", curItem->GetItemQuanity());
					}
				}
			}
		}
		XmlNodeRef ds_node_inventory_not_equipped_items = ds_node_inventory->newChild("NotEquipped_Items");
		if (ds_node_inventory_not_equipped_items)
		{
			if (pInventory)
			{
				for (int i = 0; i < pInventory->GetCount() + 1; ++i)
				{
					CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pInventory->GetItem(i)));
					if (curItem)
					{
						if(pClient->GetCurrentItemId() == curItem->GetEntityId())
							continue;

						if (!(curItem->GetEquipped() == 0))
							continue;

						XmlNodeRef this_item = ds_node_inventory_not_equipped_items->newChild(curItem->GetEntity()->GetClass()->GetName());
						if (!this_item)
							continue;

						this_item->setAttr("item_quantity", curItem->GetItemQuanity());
					}
				}
			}
		}
	}
	XmlNodeRef ds_node_spells = ds_node->newChild("Spells");
	if (ds_node_spells)
	{
		CGameXMLSettingAndGlobals::SActorSpell act_spell;
		act_spell.spell_id = -1;
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pClient->m_actor_spells_book.begin();
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pClient->m_actor_spells_book.end();
		int count = pClient->m_actor_spells_book.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			act_spell = *it;
			if (act_spell.spell_id > -1)
			{
				XmlNodeRef this_spell = ds_node_spells->newChild(act_spell.spell_name);
				if (this_spell)
				{
					if (!this_spell)
						continue;

					int eq_slot_ss = 0;
					if (act_spell.spell_id == pClient->GetSpellIdFromCurrentSpellsForUseSlot(1))
					{
						eq_slot_ss = 1;
					}
					else if (act_spell.spell_id == pClient->GetSpellIdFromCurrentSpellsForUseSlot(2))
					{
						eq_slot_ss = 2;
					}
					this_spell->setAttr("spell_id", act_spell.spell_id);
					this_spell->setAttr("equipped_slot", eq_slot_ss);
				}
			}
		}
	}
	XmlNodeRef ds_node_perks = ds_node->newChild("Perks");
	if (ds_node_perks)
	{
		std::vector<CActor::SActorPassiveEffectRK>::iterator it = pClient->m_passive_effects.begin();
		std::vector<CActor::SActorPassiveEffectRK>::iterator end = pClient->m_passive_effects.end();
		int perks_count = pClient->m_passive_effects.size();
		for (int i = 0; i < perks_count, it != end; i++, ++it)
		{
			if ((*it).perk_id > -1)
			{
				string str_perk = "";
				str_perk.Format("PerkXC%i", i);
				XmlNodeRef this_perk = ds_node_spells->newChild(str_perk);
				if (this_perk)
				{
					this_perk->setAttr("perk_id", (*it).perk_id);
					this_perk->setAttr("perk_lvl", (*it).perk_lvl);
				}
			}
		}
	}
	XmlNodeRef ds_node_cs_info = ds_node->newChild("cs_info");
	if (ds_node_cs_info)
	{
		ds_node_cs_info->setAttr("CharacterGender", pClient->GetGender());
		ds_node_cs_info->setAttr("CharacterHairType", pClient->GetHairType());
		ds_node_cs_info->setAttr("CharacterHeadType", pClient->GetHeadType());
		ds_node_cs_info->setAttr("CharacterEyesType", pClient->GetEyesType());
	}
	Character_node->saveToFile(path_to_file);
}

void CPlayerCharacterCreatingSys::LoadCharacterfromXmlFile(const char* filename)
{
	if (g_pGame->GetGameXMLSettingAndGlobals())
	{
		if(g_pGame->GetGameXMLSettingAndGlobals()->enable_character_creating_menu_at_game_start)
			return;
	}
	IPlayerProfileManager* profileManager = g_pGame->GetIGameFramework()->GetIPlayerProfileManager();
	if (!profileManager)
		return;

	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	string path_to_file = profileManager->GetSharedSaveGameFolder();
	path_to_file = PathUtil::AddSlash(path_to_file);
	path_to_file = path_to_file + string("PlayerCharacters");
	path_to_file = PathUtil::AddSlash(path_to_file);
	path_to_file.append(filename);
	XmlNodeRef Character_node = GetISystem()->LoadXmlFromFile(path_to_file.c_str());
	if (!Character_node)
	{
		CryLogAlways("Request to load player character from file rejected, wrong file name");
		return;
	}
	XmlNodeRef ds_node = Character_node->findChild("CharacterInfo");
	if (!ds_node)
	{
		CryLogAlways("Request to load player character from file rejected, character file corrupted");
		return;
	}
	XmlNodeRef nameX_node = Character_node->findChild("CharacterName");
	string char_name = Character_node->getTag();
	if (nameX_node)
	{
		XmlString nmn_ch = "";
		nameX_node->getAttr("name", nmn_ch);
		char_name = nmn_ch;
	}

	if (char_name.empty())
	{
		CryLogAlways("Request to load player character from file rejected, character file corrupted, no character name");
		return;
	}
	//CryLogAlways("Try to load player character with name is %s", char_name.c_str());
	char_name.replace("_", " ");
	SetCharacterName(char_name.c_str());
	XmlNodeRef ds_node_stats = ds_node->findChild("Stats");
	if (ds_node_stats)
	{
		XmlNodeRef ds_node_stats_base = ds_node_stats->findChild("Base_Attribles");
		if (ds_node_stats_base)
		{
			int str_l = -1;
			int agl_l = -1;
			int intl_l = -1;
			int endr_l = -1;
			int will_l = -1;
			int pers_l = -1;
			int MaxHp = -1;
			int MaxStmn = -1;
			int MaxMgc = -1;
			ds_node_stats_base->getAttr("CharacterPhysicalStat_Strength", str_l);
			ds_node_stats_base->getAttr("CharacterPhysicalStat_Agility", agl_l);
			ds_node_stats_base->getAttr("CharacterPhysicalStat_Endurance", endr_l);
			ds_node_stats_base->getAttr("CharacterPhysicalStat_HealthMaximum", MaxHp);
			ds_node_stats_base->getAttr("CharacterPhysicalStat_StaminaMaximum", MaxStmn);
			ds_node_stats_base->getAttr("CharacterMindStat_Intelligence", intl_l);
			ds_node_stats_base->getAttr("CharacterMindStat_Willpower", will_l);
			ds_node_stats_base->getAttr("CharacterMindStat_MagickaMaximum", MaxMgc);
			ds_node_stats_base->getAttr("CharacterCombinedStat_Personality", pers_l);
			if (str_l > -1)
				pClient->SetStrength(str_l);

			if (agl_l > -1)
				pClient->SetAgility(agl_l);

			if (endr_l > -1)
				pClient->SetEndurance(endr_l);

			if (MaxHp > -1)
				pClient->SetMaxHealth(MaxHp);

			if (MaxStmn > -1)
				pClient->SetMaxStamina(MaxStmn);

			if (intl_l > -1)
				pClient->SetIntelligence(intl_l);

			if (will_l > -1)
				pClient->SetWillpower(will_l);

			if (MaxMgc > -1)
				pClient->SetMaxMagicka(MaxMgc);

			if (pers_l > -1)
				pClient->SetPersonality(pers_l);
		}
		XmlNodeRef ds_node_stats_level_info = ds_node_stats->findChild("Level_Info");
		if (ds_node_stats_level_info)
		{
			int level_l = -1;
			int xp_l = -1;
			int nextxp_l = -1;
			int lst_l = -1;
			int abp_l = -1;
			ds_node_stats_level_info->getAttr("Character_Level", level_l);
			ds_node_stats_level_info->getAttr("Character_CurrentXp", xp_l);
			ds_node_stats_level_info->getAttr("Character_XpToNextLevel", nextxp_l);
			ds_node_stats_level_info->getAttr("Character_AttrUpPoints", lst_l);
			ds_node_stats_level_info->getAttr("Character_PerkUpPoints", abp_l);
			if (level_l > -1)
				pClient->SetLevel(level_l);

			if (xp_l > -1)
				pClient->SetLevelXp(xp_l);

			if (nextxp_l > -1)
				pClient->SetXpToNextLevel(nextxp_l);

			if (lst_l > -1)
				g_pGameCVars->g_leveluppointsstat = lst_l;

			if (abp_l > -1)
				g_pGameCVars->g_levelupabilitypoints = abp_l;
		}
		XmlNodeRef ds_node_stats_social_info = ds_node_stats->findChild("Social_Info");
		if (ds_node_stats_social_info)
		{
			int gold_l = -1;
			ds_node_stats_social_info->getAttr("Character_Gold", gold_l);
			if(gold_l > -1)
				pClient->SetGold(gold_l);
		}
	}
	
	XmlNodeRef ds_node_cs_info = ds_node->findChild("cs_info");
	if (ds_node_cs_info)
	{
		int gender = -1;
		int hair = -1;
		int head = -1;
		int eyes = -1;
		ds_node_cs_info->getAttr("CharacterGender", gender);
		ds_node_cs_info->getAttr("CharacterHairType", hair);
		ds_node_cs_info->getAttr("CharacterHeadType", head);
		ds_node_cs_info->getAttr("CharacterEyesType", eyes);

		if (gender == eCG_Male)
		{
			PlayerGenderSelectionMale();
		}
		else if (gender == eCG_Female)
		{
			PlayerGenderSelectionFemale();
		}

		if (head > -1)
			SetFace(head);

		if (hair > -1)
			SetHair(hair);

		if (eyes > -1)
			SetEyes(eyes);
	}
	XmlNodeRef ds_node_spells = ds_node->findChild("Spells");
	if (ds_node_spells)
	{
		pClient->ClearActorSB(true);
		CGameXMLSettingAndGlobals::SActorSpell act_spell;
		act_spell.spell_id = -1;
		for (int i = 0; i < ds_node_spells->getChildCount(); ++i)
		{
			XmlNodeRef this_spell = ds_node_spells->getChild(i);
			if (!this_spell)
				continue;

			int sp_id = -1;
			int eq_slot_ss = 0;
			this_spell->getAttr("spell_id", sp_id);
			this_spell->getAttr("equipped_slot", eq_slot_ss);
			if (sp_id > -1)
			{
				pClient->AddSpellToActorSB(sp_id);
				if (eq_slot_ss > 0)
				{
					pClient->AddSpellToCurrentSpellsForUseSlot(sp_id, eq_slot_ss);
				}
			}
		}
	}

	XmlNodeRef ds_node_perks = ds_node->findChild("Perks");
	if (ds_node_perks)
	{
		pClient->ClearActorPerks(true);
		for (int i = 0; i < ds_node_perks->getChildCount(); ++i)
		{
			XmlNodeRef this_perk = ds_node_perks->getChild(i);
			if (!this_perk)
				continue;

			int perk_id = 0;
			int perk_lvl = 0;
			this_perk->getAttr("perk_id", perk_id);
			this_perk->getAttr("perk_lvl", perk_lvl);
			if (perk_id > -1 && perk_lvl > 0)
			{
				for (int ic = 1; ic < perk_lvl + 1; ic++)
				{
					pClient->ActivatePerk(perk_id, true, ic);
				}
			}
		}

	}

	if (!gEnv->bServer)
	{
		XmlNodeRef ds_node_inventory = ds_node->findChild("Inventory");
		if (ds_node_inventory)
		{
			IInventory *pInventory = pClient->GetInventory();
			if (pInventory)
			{
				pInventory->RMIReqToServer_RemoveAllItems();
				Sleep(500);
			}
			XmlNodeRef ds_node_inventory_equipped_items = ds_node_inventory->findChild("Equipped_Items");
			if (ds_node_inventory_equipped_items)
			{
				if (pInventory)
				{
					for (int i = 0; i < ds_node_inventory_equipped_items->getChildCount(); ++i)
					{
						XmlNodeRef item_node = ds_node_inventory_equipped_items->getChild(i);
						if (!item_node)
							continue;

						int quan = -1;
						item_node->getAttr("item_quantity", quan);
						CActor::AddFromCharXMLItemParams prm;
						prm.item_class = item_node->getTag();
						prm.item_quantity = quan;
						prm.selected = true;
						pClient->GetGameObject()->InvokeRMI(CActor::SvAddItemWParams(), prm, eRMI_ToServer);
					}
				}
			}
			XmlNodeRef ds_node_inventory_not_equipped_items = ds_node_inventory->findChild("NotEquipped_Items");
			if (ds_node_inventory_not_equipped_items)
			{
				if (pInventory)
				{
					for (int i = 0; i < ds_node_inventory_not_equipped_items->getChildCount(); ++i)
					{
						XmlNodeRef item_node = ds_node_inventory_not_equipped_items->getChild(i);
						if (!item_node)
							continue;

						int quan = -1;
						item_node->getAttr("item_quantity", quan);
						CActor::AddFromCharXMLItemParams prm;
						prm.item_class = item_node->getTag();
						prm.item_quantity = quan;
						prm.selected = false;
						pClient->GetGameObject()->InvokeRMI(CActor::SvAddItemWParams(), prm, eRMI_ToServer);
					}
				}
			}
		}
		return;
	}

	XmlNodeRef ds_node_inventory = ds_node->findChild("Inventory");
	if (ds_node_inventory)
	{
		IInventory *pInventory = pClient->GetInventory();
		if (pInventory)
		{
			pInventory->Destroy();
		}
		XmlNodeRef ds_node_inventory_equipped_items = ds_node_inventory->findChild("Equipped_Items");
		if (ds_node_inventory_equipped_items)
		{
			if (pInventory)
			{
				for (int i = 0; i < ds_node_inventory_equipped_items->getChildCount(); ++i)
				{
					XmlNodeRef item_node = ds_node_inventory_equipped_items->getChild(i);
					if (!item_node)
						continue;

					CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(
						gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(g_pGame->GetIGameFramework()->GetClientActor(), item_node->getTag(),
							false, false, false)));
					if (curItem)
					{
						int quan = -1;
						item_node->getAttr("item_quantity", quan);
						curItem->SetItemQuanity(quan);
						if (curItem->CanSelect())
						{
							if(!pClient->GetRelaxedMod())
								pClient->SelectItem(curItem->GetEntityId(), false, false);
							else
								pClient->SetEquippedItemId(eAESlot_Weapon, curItem->GetEntityId());
						}

						pClient->EquipItem(curItem->GetEntityId());
					}
				}
			}
		}
		XmlNodeRef ds_node_inventory_not_equipped_items = ds_node_inventory->findChild("NotEquipped_Items");
		if (ds_node_inventory_not_equipped_items)
		{
			if (pInventory)
			{
				for (int i = 0; i < ds_node_inventory_not_equipped_items->getChildCount(); ++i)
				{
					XmlNodeRef item_node = ds_node_inventory_not_equipped_items->getChild(i);
					if (!item_node)
						continue;

					CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(
						gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(g_pGame->GetIGameFramework()->GetClientActor(), item_node->getTag(),
							false, false, false)));
					if (curItem)
					{
						int quan = -1;
						item_node->getAttr("item_quantity", quan);
						curItem->SetItemQuanity(quan);
					}
				}
			}
		}

		if (pInventory)
		{
			bool bHasNoWeapon = pInventory->GetCountOfClass("NoWeapon") > 0;
			bool bHasPcarWeapon = pInventory->GetCountOfClass("PickAndThrowWeapon") > 0;
			if (!bHasNoWeapon)
			{
				gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(g_pGame->GetIGameFramework()->GetClientActor(), "NoWeapon", false, false, false);
			}
			if (!bHasPcarWeapon)
			{
				gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(g_pGame->GetIGameFramework()->GetClientActor(), "PickAndThrowWeapon", false, false, false);
			}
		}
	}
}

string CPlayerCharacterCreatingSys::GetSavedCharactersListPath()
{
	IPlayerProfileManager* profileManager = g_pGame->GetIGameFramework()->GetIPlayerProfileManager();
	if (!profileManager)
		return "";

	string path_to_file = profileManager->GetSharedSaveGameFolder();
	path_to_file = PathUtil::AddSlash(path_to_file);
	path_to_file = path_to_file + string("PlayerCharacters");
	path_to_file = PathUtil::AddSlash(path_to_file);
	path_to_file.append("saved_characters.xml");
	CryLogAlways("CPlayerCharacterCreatingSys::GetSavedCharactersListPath return %s", path_to_file.c_str());
	return path_to_file;
}

void CPlayerCharacterCreatingSys::OnCharacterCreationCompleteEvent()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;
}

void CPlayerCharacterCreatingSys::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}
