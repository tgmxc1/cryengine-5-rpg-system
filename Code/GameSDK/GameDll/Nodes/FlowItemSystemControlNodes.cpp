// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "Player.h"
#include "Actor.h"
#include "Nodes/G2FlowBaseNode.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "DialogSystemEvents.h"
#include "ActorActionsNew.h"
#include "GameXMLSettingAndGlobals.h"

#include <CryString/StringUtils.h>

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowGetItemSpecialInfoNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowGetItemSpecialInfoNode(SActivationInfo * pActInfo) : m_entityId(0)
	{

	}

	~CFlowGetItemSpecialInfoNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowGetItemSpecialInfoNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Additional_Effects_slot,
		EIP_Particle_Effects_slot,
		EIP_Armor_model_path_slot,
		EIP_ArrowQ_model_path_slot
	};

	enum EOutputPorts
	{
		EOP_Damage_added,
		EOP_Damage_normal,
		EOP_Damage_throwing,
		EOP_Damage_mgc_multipler,
		EOP_Additional_effect,
		EOP_Particle_effect,
		EOP_Particle_helper,
		EOP_Armor_Model,
		EOP_Quver_Model,
		EOP_ArmorType,
		EOP_ArmorClass,
		EOP_WeaponType,
		EOP_IsEquipped,
		EOP_IsDropped,
		EOP_IsSelected,
		EOP_ItemQuantityInSlot,
		EOP_IsQuestItem,
		EOP_IsSelectableShield,
		EOP_ItemCurrentHealth,
		EOP_ItemMaxHealth,
		EOP_ItemType,
		EOP_ItemPrice,
		EOP_WeaponCrtDamageMod,
		EOP_WeaponCrtDamageChance,
		EOP_IsWeaponTwoHanded,
		EOP_IsItemStackable,
		EOP_ItemMaxStackSize,
		EOP_ItemCombineable,
		EOP_ArmorRating,
		EOP_PotionType,
		EOP_IsCanRepairOtherItems,
		EOP_RepairFrc,
		EOP_OnEquipLuaFunc_Call,
		EOP_OnDeequipLuaFunc_Call,
		EOP_ItemMass
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Get info trigger")),
			InputPortConfig<int>("Additional Effect slot", 0, _HELP("efd")),
			InputPortConfig<int>("Particle Effects slot", 0, _HELP("efd")),
			InputPortConfig<int>("Armor model path slot", 0, _HELP("efd")),
			InputPortConfig<int>("ArrowItemQ model path slot", 0, _HELP("efd")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<int>("AddedDamage", _HELP("trv")),
			OutputPortConfig<int>("NormalDamage", _HELP("trv")),
			OutputPortConfig<int>("ThrowingDamage", _HELP("trv")),
			OutputPortConfig<float>("DamageMgcMultipler", _HELP("trv")),
			OutputPortConfig<int>("AdditionalEffectId", _HELP("trv")),
			OutputPortConfig<string>("ParticleEffect", _HELP("trv")),
			OutputPortConfig<string>("ParticleHelper", _HELP("trv")),
			OutputPortConfig<string>("ArmorModel", _HELP("trv")),
			OutputPortConfig<string>("QuverModel", _HELP("trv")),
			OutputPortConfig<int>("ArmorType", _HELP("trv")),
			OutputPortConfig<int>("ArmorClass", _HELP("trv")),
			OutputPortConfig<int>("WeaponType", _HELP("trv")),
			OutputPortConfig<bool>("IsEquipped", _HELP("trv")),
			OutputPortConfig<bool>("IsDropped", _HELP("trv")),
			OutputPortConfig<bool>("IsSelected", _HELP("trv")),
			OutputPortConfig<int>("ItemQuantityInSlot", _HELP("trv")),
			OutputPortConfig<bool>("IsQuestItem", _HELP("trv")),
			OutputPortConfig<bool>("IsSelectableShield", _HELP("trv")),
			OutputPortConfig<float>("ItemCurrentHealth", _HELP("trv")),
			OutputPortConfig<float>("ItemMaxHealth", _HELP("trv")),
			OutputPortConfig<int>("ItemType", _HELP("trv")),
			OutputPortConfig<int>("ItemPrice", _HELP("trv")),
			OutputPortConfig<float>("WeaponCrtDamageMod", _HELP("trv")),
			OutputPortConfig<int>("WeaponCrtDamageChance", _HELP("trv")),
			OutputPortConfig<bool>("IsWeaponTwoHanded", _HELP("trv")),
			OutputPortConfig<bool>("IsItemStackable", _HELP("trv")),
			OutputPortConfig<int>("ItemMaxStackSize", _HELP("trv")),
			OutputPortConfig<bool>("ItemCombineable", _HELP("trv")),
			OutputPortConfig<float>("ArmorRating", _HELP("trv")),
			OutputPortConfig<int>("PotionType", _HELP("trv")),
			OutputPortConfig<bool>("IsCanRepairOtherItems", _HELP("trv")),
			OutputPortConfig<int>("RepairFrc", _HELP("trv")),
			OutputPortConfig<string>("OnEquipLuaFuncName", _HELP("trv")),
			OutputPortConfig<string>("OnDeequipLuaFuncName", _HELP("trv")),
			OutputPortConfig<float>("ItemMass", _HELP("trv")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Get Item combat params info node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
		}
		break;
		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CItem* pItem = pGameGlobals->GetThisItem(m_entityId);
			if (pItem == 0)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				int slot_ae = GetPortInt(pActInfo, EIP_Additional_Effects_slot);
				int slot_pe = GetPortInt(pActInfo, EIP_Particle_Effects_slot);
				int slot_amp = GetPortInt(pActInfo, EIP_Armor_model_path_slot);
				int slot_aqmp = GetPortInt(pActInfo, EIP_ArrowQ_model_path_slot);
				string arm_model_pth = pItem->GetArmorModelPath(slot_amp);
				if (pItem->GetOwnerActor())
				{
					if (pItem->GetOwnerActor()->GetGender() == 0)
						arm_model_pth = pItem->GetArmorMaleModelPath(slot_amp);
				}
				bool is_equipped = false;
				if (pItem->GetEquipped() > 0)
				{
					is_equipped = true;
				}
				ActivateOutput(&m_actInfo, EOP_Damage_added, pItem->GetDmgAdded());
				ActivateOutput(&m_actInfo, EOP_Damage_normal, pItem->GetDmgCurrent());
				ActivateOutput(&m_actInfo, EOP_Damage_throwing, pItem->GetThrowDmg());
				ActivateOutput(&m_actInfo, EOP_Damage_mgc_multipler, pItem->GetMagickPwrMult());
				ActivateOutput(&m_actInfo, EOP_Additional_effect, pItem->GetItemAddotionalEffect(slot_ae));
				ActivateOutput(&m_actInfo, EOP_Particle_effect, string(pItem->GetParticlePath(slot_pe)));
				ActivateOutput(&m_actInfo, EOP_Particle_helper, string(pItem->GetParticleHelper(slot_pe)));
				ActivateOutput(&m_actInfo, EOP_Armor_Model, arm_model_pth);
				ActivateOutput(&m_actInfo, EOP_Quver_Model, string(pItem->GetSharedItemParams()->params.arrow_quiver_path[slot_aqmp].c_str()));
				ActivateOutput(&m_actInfo, EOP_ArmorType, pItem->GetArmorType());
				ActivateOutput(&m_actInfo, EOP_ArmorClass, pItem->GetArmorClass());
				ActivateOutput(&m_actInfo, EOP_WeaponType, pItem->GetWeaponType());
				ActivateOutput(&m_actInfo, EOP_IsEquipped, is_equipped);
				ActivateOutput(&m_actInfo, EOP_IsDropped, pItem->GetStats().dropped);
				ActivateOutput(&m_actInfo, EOP_IsSelected, pItem->GetStats().selected);
				ActivateOutput(&m_actInfo, EOP_ItemQuantityInSlot, pItem->GetItemQuanity());
				ActivateOutput(&m_actInfo, EOP_IsQuestItem, pItem->GetItemMarkQuest() > 0);
				ActivateOutput(&m_actInfo, EOP_IsSelectableShield, pItem->GetShield() > 0);
				ActivateOutput(&m_actInfo, EOP_ItemCurrentHealth, pItem->GetItemHealth());
				ActivateOutput(&m_actInfo, EOP_ItemMaxHealth, pItem->GetSharedItemParams()->params.item_mhp);
				ActivateOutput(&m_actInfo, EOP_ItemType, pItem->GetItemType());
				ActivateOutput(&m_actInfo, EOP_ItemPrice, pItem->GetItemCost());
				ActivateOutput(&m_actInfo, EOP_WeaponCrtDamageMod, pItem->GetCriticalHitDmgModiffer());
				ActivateOutput(&m_actInfo, EOP_WeaponCrtDamageChance, pItem->GetCriticalHitBaseChance());
				ActivateOutput(&m_actInfo, EOP_IsWeaponTwoHanded, pItem->IsWpnTwoHanded());
				ActivateOutput(&m_actInfo, EOP_IsItemStackable, pItem->IsStackable());
				ActivateOutput(&m_actInfo, EOP_ItemMaxStackSize, pItem->GetMaxStackSizeOfItem());
				ActivateOutput(&m_actInfo, EOP_ItemCombineable, pItem->GetCombineAble() > 0);
				ActivateOutput(&m_actInfo, EOP_ArmorRating, pItem->GetArmorRating());
				ActivateOutput(&m_actInfo, EOP_PotionType, pItem->GetItemPotType());
				ActivateOutput(&m_actInfo, EOP_IsCanRepairOtherItems, pItem->GetReprType() > 0);
				ActivateOutput(&m_actInfo, EOP_RepairFrc, pItem->GetReprFc());
				ActivateOutput(&m_actInfo, EOP_OnEquipLuaFunc_Call, string(pItem->GetLuaFncOnEquip()));
				ActivateOutput(&m_actInfo, EOP_OnDeequipLuaFunc_Call, string(pItem->GetLuaFncOnDeEquip()));
				ActivateOutput(&m_actInfo, EOP_ItemMass, pItem->GetSharedItemParams()->params.mass);
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

REGISTER_FLOW_NODE("ItemSystem:ItemSpecInfo", CFlowGetItemSpecialInfoNode);