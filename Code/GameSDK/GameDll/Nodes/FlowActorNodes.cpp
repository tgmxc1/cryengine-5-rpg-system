// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#include "StdAfx.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameRulesTypes.h"
#include "GameRulesModules/IGameRulesKillListener.h"
#include "Nodes/G2FlowBaseNode.h"
#include "GameRules.h"
#include <CryAnimation/IFacialAnimation.h>
#include "Player.h"
#include "AI/GameAISystem.h"
#include "AI/GameAIEnv.h"
#include "GamePhysicsSettings.h"
#include <CryAISystem/IMovementSystem.h>
#include <CryAISystem/MovementRequest.h>
#include <CryAISystem/IAISystem.h>
#include <CryAISystem/IAgent.h>
#include <CryAISystem/MovementStyle.h>
#include <CryMath/Cry_Geo.h>
#include <CryAISystem/IAIActorProxy.h>
#include <CryAISystem/IAIActor.h>
#include "ActorActionsNew.h"

#include "PlayerPathFinding.h"

//////////////////////////////////////////////////////////////////////////
// Counts how many AIs died
//////////////////////////////////////////////////////////////////////////
class CFlowNode_AIBodyCount : public CFlowBaseNode<eNCT_Instanced>, SGameRulesListener
{
	enum EInputPorts
	{
		eINP_Enable = 0,
		eINP_Disable,
		eINP_Reset
	};

	enum EOutputPorts
	{
		eOUT_TotalDeaths = 0,
		eOUT_EnemyDeaths
	};
public:
	CFlowNode_AIBodyCount( SActivationInfo * pActInfo )
		: m_totalDeaths( 0 )
		, m_enemyDeaths( 0 )
	{
	}
	~CFlowNode_AIBodyCount()
	{
		CGameRules* pGameRules = g_pGame->GetGameRules();
		if(pGameRules)
			pGameRules->RemoveGameRulesListener(this);
	}
	void Serialize( SActivationInfo * pActInfo, TSerialize ser )
	{
		ser.Value("TotalDeaths", m_totalDeaths);
		ser.Value("EnemyDeaths", m_enemyDeaths);
	}
	virtual void GetConfiguration( SFlowNodeConfig &config )
	{
		static const SInputPortConfig inp_config[] = {
			InputPortConfig_Void ("Enable", _HELP("Enables the body counter (disabled by default from start)")),
			InputPortConfig_Void ("Disable", _HELP("Disables the body counter")),
			InputPortConfig_Void ("Reset", _HELP("Resets the body counter to 0 for both outputs")),
			{0}
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<int>("Total"),
			OutputPortConfig<int>("Enemy"),
			{0}
		};

		config.sDescription = _HELP( "Counts how many AIs have been killed" );
		config.pInputPorts = inp_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	virtual IFlowNodePtr Clone(SActivationInfo* pActInfo) { return new CFlowNode_AIBodyCount(pActInfo); }
	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				m_totalDeaths = 0;
				m_enemyDeaths = 0;
				break;
			}
		case eFE_Activate:
			{
				if (IsPortActive( pActInfo, eINP_Enable ))
				{
					CGameRules* pGameRules = g_pGame->GetGameRules();
					if (pGameRules)
						pGameRules->AddGameRulesListener( this );
				}

				if (IsPortActive( pActInfo, eINP_Disable ))
				{
					CGameRules* pGameRules = g_pGame->GetGameRules();
					if (pGameRules)
						pGameRules->RemoveGameRulesListener( this );
				}

				if (IsPortActive( pActInfo, eINP_Reset ))
				{
					m_totalDeaths = 0;
					m_enemyDeaths = 0;
					ActivateOutput( &m_actInfo, eOUT_TotalDeaths, m_totalDeaths );
					ActivateOutput( &m_actInfo, eOUT_EnemyDeaths, m_enemyDeaths );
				}
				break;
			}
		}
	}

	// inherited from SGameRulesListener	
	virtual void OnActorDeath( CActor* pActor )
	{
		IAIObject* pAI = pActor->GetEntity()->GetAI();

		if (!pAI)
			return;

		++m_totalDeaths;
		ActivateOutput( &m_actInfo, eOUT_TotalDeaths, m_totalDeaths );

		IAIObject* pClientAI = gEnv->pGame->GetIGameFramework()->GetClientActor()->GetEntity()->GetAI();

		if (pAI->IsHostile( pClientAI, false ))
		{
			++m_enemyDeaths;
			ActivateOutput( &m_actInfo, eOUT_EnemyDeaths, m_enemyDeaths );
		}
	}


	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	int m_totalDeaths;
	int m_enemyDeaths;
};


//////////////////////////////////////////////////////////////////////////
// Global AI enemies awareness of player.
// this supposed to represent how much player is exposed to the AI enemies
// currently
//////////////////////////////////////////////////////////////////////////
class CFlowNode_AIAwareness : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_AIAwareness( SActivationInfo * pActInfo )
	{
	}
	virtual void GetConfiguration( SFlowNodeConfig &config )
	{
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<float>( "awareness", _HELP("Global AI enemies awareness of the player\n0 - Minimum (Green)\n100 - Maximum (Red)") ),
			{0}
		};
		config.sDescription = _HELP( "Global AI enemies awareness of the player" );
		config.pInputPorts = 0;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}
	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		if ( event == eFE_Update )
		{
			const float awareness = g_pGame->GetGameAISystem()->GetAIAwarenessToPlayerHelper().GetFloatAwareness();
			ActivateOutput( pActInfo, 0, awareness );
		}
		else if ( event == eFE_Initialize )
		{
			pActInfo->pGraph->SetRegularlyUpdated( pActInfo->myID, true );
			ActivateOutput( pActInfo, 0, 0 );
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


class CFlowActorAliveCheck : public CFlowBaseNode<eNCT_Instanced>
{
	bool m_errorLogSent;
public:
	CFlowActorAliveCheck( SActivationInfo * pActInfo )
		: m_errorLogSent( false )
	{
	}

	enum EInputs
	{
		eIP_Trigger = 0
	};

	enum EOutputs
	{
		eOP_Status = 0,
		eOP_Alive,
		eOP_Dead
	};

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorAliveCheck(pActInfo);
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger this port to get the current actor status")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>("Status", _HELP("true if is alive, false if dead")),
			OutputPortConfig_Void("Alive", _HELP("triggered if is alive")),
			OutputPortConfig_Void("Dead", _HELP("triggered if is dead")),
			{0}
		};    
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.nFlags |= EFLN_TARGET_ENTITY|EFLN_AISEQUENCE_SUPPORTED;
		config.sDescription = _HELP("Check the death/alive status of an entity (actor)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			m_errorLogSent = false;
			break;

		case eFE_Activate:
			if (IsPortActive(pActInfo, eIP_Trigger))
			{
				IActor* pActor = pActInfo->pEntity ? gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()) : NULL;
				if(pActor)
				{
					bool isAlive = !pActor->IsDead();
					ActivateOutput(pActInfo, eOP_Status, isAlive );
					if (isAlive)
						ActivateOutput(pActInfo, eOP_Alive, true );
					else
						ActivateOutput(pActInfo, eOP_Dead, true );
				}
				else if (!m_errorLogSent)
				{
					IEntitySystem* pESys = gEnv->pEntitySystem;
					IEntity* pGraphEntity = pESys->GetEntity( pActInfo->pGraph->GetGraphEntity( 0 ) );
					GameWarning("[flow] Actor:AliveCheck - flowgraph entity: %d:'%s' - no entity or entity is not an actor. Entity: %d:'%s'", 
						pActInfo->pGraph->GetGraphEntity( 0 ), pGraphEntity ? pGraphEntity->GetName() : "<NULL>",
						pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0, pActInfo->pEntity ? pActInfo->pEntity->GetName() : "<NULL>" );
					m_errorLogSent = true;
				}
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


class CFlowPlayerIsInAir : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowPlayerIsInAir( SActivationInfo* pActInfo )
	{
	}

	enum EInputs
	{
		eIP_Trigger = 0
	};

	enum EOutputs
	{
		eOP_Status = 0,
		eOP_InAir,
		eOP_NotInAir
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger this port to get the player InAir status")),
			{0}
		};
		static const SOutputPortConfig outputs[] = 
		{
			OutputPortConfig<bool>("Status", _HELP("true if is InAir, false if not")),
			OutputPortConfig_Void("InAir", _HELP("triggered if is InAir")),
			OutputPortConfig_Void("NotInAir", _HELP("triggered if is NOT InAir")),
			{0}
		};    
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Check the InAir/NotInAir status of the player. InAir = jumping or falling");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			break;

		case eFE_Activate:
			if (IsPortActive(pActInfo, eIP_Trigger))
			{
				IActor* pClientActor = gEnv->pGame->GetIGameFramework()->GetClientActor();
				if (pClientActor && pClientActor->IsPlayer())
				{
					CPlayer* pPlayer = static_cast<CPlayer*>(pClientActor);
					bool isInAir = pPlayer->IsInAir() || pPlayer->IsInFreeFallDeath();
					ActivateOutput(pActInfo, eOP_Status, isInAir );
					if (isInAir)
						ActivateOutput(pActInfo, eOP_InAir, true );
					else
						ActivateOutput(pActInfo, eOP_NotInAir, true );
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};



class CFlowActorFacialAnim : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowActorFacialAnim( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIP_Start = 0,
		eIP_Stop,
		eIP_Sequence,
		eIP_Layer,
		eIP_Exclusive,
		eIP_Loop,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Start", _HELP("Trigger the facial sequence")),
			InputPortConfig_Void("Stop", _HELP("Stop the facial sequence")),
			InputPortConfig<string>("Sequence", _HELP("Sequence (fsq file) to play")),
			InputPortConfig<int>("Layer", _HELP("Layer to play that sequence")),
			InputPortConfig<bool>("Exclusive", _HELP("Exclusive animation ?")),
			InputPortConfig<bool>("Loop", _HELP("Play in Loop ?")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};    
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.sDescription = _HELP("Play facial sequences on Actors");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			if (IsPortActive(pActInfo, eIP_Start) && pActInfo->pEntity)
			{
				if (ICharacterInstance* pCharacter = pActInfo->pEntity->GetCharacter(0))
				{
					if (IFacialInstance* pInstance = pCharacter->GetFacialInstance())
					{
						const char *sSequence = GetPortString(pActInfo, eIP_Sequence).c_str();
						if (IFacialAnimSequence* pSequence = pInstance->LoadSequence(sSequence))
						{
							int iLayer = GetPortInt(pActInfo, eIP_Layer);
							bool bLoop = GetPortBool(pActInfo, eIP_Loop);
							bool bExclusive = GetPortBool(pActInfo, eIP_Exclusive);
							pInstance->PlaySequence(pSequence, (EFacialSequenceLayer) iLayer, bExclusive, bLoop);
						}
					}
				}
			}
			else if(IsPortActive(pActInfo, eIP_Stop) && pActInfo->pEntity)
			{
				if (ICharacterInstance* pCharacter = pActInfo->pEntity->GetCharacter(0))
				{
					if (IFacialInstance* pInstance = pCharacter->GetFacialInstance())
					{
						pInstance->StopSequence((EFacialSequenceLayer) GetPortInt(pActInfo, eIP_Layer));
					}
				}
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

static const uint32 INVALID_FACIAL_CHANNEL_ID = ~0;

class CFlowActorFacialExpression : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorFacialExpression( SActivationInfo * pActInfo )
	{
		m_channel = INVALID_FACIAL_CHANNEL_ID;
	}

	enum EInputs
	{
		eIP_Start = 0,
		eIP_Stop,
		eIP_Expression,
		eIP_Weight,
		eIP_FadeTime,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Start", _HELP("Trigger the facial expression")),
			InputPortConfig_Void("Stop", _HELP("Stop the facial expression")),
			InputPortConfig<string>("Expression", _HELP("Expression to play")),
			InputPortConfig<float>("Weight", _HELP("Weight")),
			InputPortConfig<float>("FadeTime", _HELP("Fade time")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};    
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.sDescription = _HELP("Play facial expressions on Actors");
		config.SetCategory(EFLN_DEBUG);
	}
	virtual IFlowNodePtr Clone(SActivationInfo* pActInfo) { return new CFlowActorFacialExpression(pActInfo); }

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			if (IsPortActive(pActInfo, eIP_Start) && pActInfo->pEntity)
			{
				if (ICharacterInstance* pCharacter = pActInfo->pEntity->GetCharacter(0))
				{
					if (IFacialInstance* pInstance = pCharacter->GetFacialInstance())
					{
						if (IFacialModel * pFacialModel = pInstance->GetFacialModel())
						{
							if (IFacialEffectorsLibrary * pLibrary = pFacialModel->GetLibrary())
							{
								IFacialEffector *pEffector = NULL;
								if (pEffector = pLibrary->Find( GetPortString(pActInfo, eIP_Expression).c_str() ))
								{
									float fWeight = GetPortFloat(pActInfo, eIP_Weight);
									float fFadeTime = GetPortFloat(pActInfo, eIP_FadeTime);

									if (m_channel != INVALID_FACIAL_CHANNEL_ID)
									{
										// we fade out with the same fadeTime as fade in
										pInstance->StopEffectorChannel(m_channel, fFadeTime);
										m_channel = INVALID_FACIAL_CHANNEL_ID;
									}

									m_channel = pInstance->StartEffectorChannel(pEffector, fWeight, fFadeTime);
								}
							}
						}
					}
				}
			}
			else if(IsPortActive(pActInfo, eIP_Stop) && pActInfo->pEntity && m_channel != INVALID_FACIAL_CHANNEL_ID)
			{
				if (ICharacterInstance* pCharacter = pActInfo->pEntity->GetCharacter(0))
				{
					if (IFacialInstance* pInstance = pCharacter->GetFacialInstance())
					{
						float fFadeTime = GetPortFloat(pActInfo, eIP_FadeTime);
						pInstance->StopEffectorChannel(m_channel, fFadeTime);
						m_channel = INVALID_FACIAL_CHANNEL_ID;
					}
				}
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	uint32 m_channel;
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_PlayerKnockDown : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_PlayerKnockDown( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIN_KnockDown = 0,
		eIN_BackwardsImpulse
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("KnockDown", _HELP("Triggers player knock down action")),
			InputPortConfig<float> ("Impulse", _HELP("Backwards impulse applied when knocking down")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Knock down local player");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_KnockDown))
				{
					CActor* pLocalActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pLocalActor)
					{
						const float backwardsImpulse = GetPortFloat(pActInfo, eIN_BackwardsImpulse);
						pLocalActor->KnockDown(backwardsImpulse);
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_PlayerInteractiveAnimation : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_PlayerInteractiveAnimation( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIN_Play = 0,
		eIN_Interaction
	};

	enum EOutputs
	{
		eOUT_Done = 0
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("Play", _HELP("Triggers interactive action")),
			InputPortConfig<string> ("InteractionName", _HELP("Interactive action to play")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done", _HELP("Triggers when the animation is done")),
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Plays interactive animation for Player");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_Play))
				{
					CActor* pLocalActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pLocalActor)
					{
						const string& interactionName = GetPortString(pActInfo, eIN_Interaction);
						pLocalActor->StartInteractiveActionByName(interactionName.c_str(), true);

						pActInfo->pGraph->SetRegularlyUpdated( pActInfo->myID, true );
					}
				}
			}
			break;
		case eFE_Update:
			{
				CPlayer* pLocalPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
				if (pLocalPlayer)
				{
					if (pLocalPlayer->IsInteractiveActionDone())
					{
						ActivateOutput(pActInfo, eOUT_Done, true );
						pActInfo->pGraph->SetRegularlyUpdated( pActInfo->myID, false );
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_PlayerCinematicControl : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_PlayerCinematicControl( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIN_HolsterWeapon = 0,
		eIN_LowerWeapon,
		eIN_RestrictMovement,
		eIN_RestrictToWalk,
		eIN_TutorialMode,
		eIN_ResetAll
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("HolsterWeapon", _HELP("Holster current player weapon")),
			InputPortConfig_Void ("LowerWeapon", _HELP("Lowers current player weapon")),
			InputPortConfig_Void ("RestrictMovement", _HELP("Disables super jump, sprint and others")),
			InputPortConfig_Void ("RestrictToWalk", _HELP("Scales movement speed by 50%")),
			InputPortConfig_Void ("TutorialMode", _HELP("Places the player in turorial mode by restricting weapon fire, lowering his weapon, etc.")),
			InputPortConfig_Void ("ResetAll", _HELP("Go back to normal control when cinematic is over")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Restrict player controls during cinematics");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_HolsterWeapon))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_HolsterWeapon);
					}
				}
				if (IsPortActive(pActInfo, eIN_LowerWeapon))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_LowerWeapon);
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_RestrictMovement);
					}
				}
				if (IsPortActive(pActInfo, eIN_RestrictMovement))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_RestrictMovement);
					}
				}
				if (IsPortActive(pActInfo, eIN_RestrictToWalk))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_WalkOnly);
					}
				}
				if (IsPortActive(pActInfo, eIN_TutorialMode))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->SetCinematicFlag(SPlayerStats::eCinematicFlag_LowerWeapon);
					}
				}
				if (IsPortActive(pActInfo, eIN_ResetAll))
				{
					CPlayer* pLocalPlayer = GetLocalPlayer();
					if (pLocalPlayer)
					{
						pLocalPlayer->ResetCinematicFlags();
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:

	CPlayer* GetLocalPlayer() const
	{
		IActor* pActor = g_pGame->GetIGameFramework()->GetClientActor();;

		if (pActor)
		{
			CRY_ASSERT(pActor->GetActorClass() == CPlayer::GetActorClassType());

			return static_cast<CPlayer*>(pActor);
		}

		return NULL;
	}	
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_PlayerLookAt : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_PlayerLookAt( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIN_EnableLookAt = 0,
		eIN_DisableLookAt,
		eIP_Force,
		eIP_Time,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("Enable", _HELP("Enables look at target option for the local player")),
			InputPortConfig_Void ("Disable", _HELP("Disables look at target option for the local player")),
			InputPortConfig<bool>("Force", _HELP("Forces the player to look at the target, even if no button is pressed.")),
			InputPortConfig<float>("InterpolationTime", 0.2f, _HELP("Smoothing time (seconds)")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.sDescription = _HELP("Enable/Disable look at target option for the player");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_EnableLookAt))
				{
					float interpolationTime = GetPortFloat( pActInfo, eIP_Time );
					CActor* pLocalActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pLocalActor)
					{
						EntityId targetId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
						if ( GetPortBool(pActInfo, eIP_Force) ) {
							pLocalActor->SetForceLookAtTargetId(targetId, interpolationTime);
						} else {
							pLocalActor->SetLookAtTargetId(targetId, interpolationTime);
						}
					}
				}
				else if (IsPortActive(pActInfo, eIN_DisableLookAt))
				{
					CActor* pLocalActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pLocalActor)
					{
						pLocalActor->SetLookAtTargetId(0);
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_ActorKillPlayer : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_ActorKillPlayer( SActivationInfo * pActInfo ) {}

	enum EInputs
	{
		eIN_Trigger = 0,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("Kill", _HELP("Triggers to kill the local player")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Instantly kills the local player.");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_Trigger))
				{
					CGameRules *pGameRules = g_pGame->GetGameRules();
					if (pGameRules)
					{
						const EntityId clientId = g_pGame->GetIGameFramework()->GetClientActorId();

						HitInfo suicideInfo(clientId, clientId, clientId,
							10000, 0, 0, -1, CGameRules::EHitType::Punish, ZERO, ZERO, ZERO);

						pGameRules->ClientHit(suicideInfo);

						//Execute a second time, to skip 'mercy time' protection
						if (!gEnv->bMultiplayer)
						{
							pGameRules->ClientHit(suicideInfo);
						}
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_ActorSetPlayerModel : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowNode_ActorSetPlayerModel( SActivationInfo * pActInfo ) 
	{

	}

	~CFlowNode_ActorSetPlayerModel()
	{
		m_pPlayerModel.reset();
	}

	enum EInputs
	{
		eIN_Set = 0,
		eIN_Model,
	};

	enum EOutputs
	{
		eOUT_Set = 0,
		eOUT_Fail,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("Set", _HELP("Triggers model changing ")),
			InputPortConfig<string> ("Model", _HELP("Model file name")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>("Set", _HELP("true if the model has been set successfully ")),
			OutputPortConfig<bool>("Fail", _HELP("true if the model hasn't been set")),
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Set local player's model");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual IFlowNodePtr Clone(SActivationInfo* pActInfo) { return new CFlowNode_ActorSetPlayerModel(pActInfo); }

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{

		switch (event)
		{
		case eFE_Initialize:
			{
				// pre-cache the model
				const string& modelName = GetPortString(pActInfo, eIN_Model);
				m_pPlayerModel = gEnv->pCharacterManager->CreateInstance(modelName.c_str());
			}
			break;

		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_Set))
				{
					CActor* pLocalActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pLocalActor != NULL && pLocalActor->GetEntity())
					{
						const string& modelName = GetPortString(pActInfo, eIN_Model);
						if(m_pPlayerModel)
						{
							const bool loaded = pLocalActor->SetActorModel(modelName.c_str());
							if (loaded)
							{
								pLocalActor->Physicalize(pLocalActor->GetStance());
							}
							ActivateOutput(pActInfo, loaded? eOUT_Set : eOUT_Fail, true);
						}
						else					
						{
							CryWarning( VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "Actor:SetPlayerModel:Character %s not valid",modelName.c_str());
							ActivateOutput(pActInfo, eOUT_Fail, true);
						}
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

private:
	_smart_ptr<ICharacterInstance> m_pPlayerModel;

};


//////////////////////////////////////////////////////////////////////////
class CFlowNode_PlayerDropObject : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowNode_PlayerDropObject( SActivationInfo * pActInfo )
	{
	}

	enum EInputs
	{
		eIN_DropIt = 0,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void ("DropIt", _HELP("When triggered, the player will drop any currently held object or enemy")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Force the player to drop any currently held object/enemy");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIN_DropIt))
				{
					CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
					if (pActor != NULL && pActor->IsPlayer())
					{
						CPlayer* pPlayer = static_cast<CPlayer*>( pActor );
						if (pPlayer->IsInPickAndThrowMode())
							pPlayer->ExitPickAndThrow();
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


//////////////////////////////////////////////////////////////////////////
class CFlowNode_GetNearestActor : public CFlowBaseNode<eNCT_Singleton>
{
public:

	CFlowNode_GetNearestActor( SActivationInfo * pActInfo )
	{
	}
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInput
	{
		eINP_Trigger= 0,
		eINP_CenterPos,
		eINP_MaxDist,
		eINP_Faction,
		eINP_FactionFilterType,
		eINP_DeadAliveFilter,
	};

	enum EOutput
	{
		eOUT_NearestActorId = 0,
		eOUT_Faction,
		eOUT_Position,
		eOUT_Distance,
		eOUT_Alive
	};

	enum EFactionFilterType
	{
		eFCT_NoFilter = 0,
		eFCT_SameFactionOnly,
		eFCT_AllButFaction,
		eFCT_AnyFriendly,
		eFCT_AnyHostile,
	};

	enum EDeadAliveFilter
	{
		eDA_Any = 0,
		eDA_Alive,
		eDA_Dead
	};

	virtual void GetConfiguration( SFlowNodeConfig &config )
	{
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<EntityId>("ActorId", _HELP("entityId of the nearest actor")),
			OutputPortConfig<int>("Faction", _HELP("Faction of the nearest actor")),
			OutputPortConfig<Vec3>("Pos", _HELP("Position of the nearest actor")),
			OutputPortConfig<float>("Distance", _HELP("Distance of the nearest actor to the centerPos check")),
			OutputPortConfig<bool>("Alive", _HELP("True if the nearest actor is alive")),
			{0}
		};
		static const SInputPortConfig inp_config[] = {
			InputPortConfig_Void ("Trigger", _HELP("The node only checks for nearest actor when this is triggered")),
			InputPortConfig<Vec3>("CenterPos", Vec3(0,0,0), _HELP("Position to compare actors with.")),            
			InputPortConfig<float> ("MaxDist", 0, _HELP("Only consider actors that are nearer than this. 0 = no distance limit.")),
			InputPortConfig<string>("Faction", "", _HELP("Used in combination with 'FactionFilterType' input to filter actors based on faction"), 0, "enum_global:Faction" ),
			InputPortConfig<int>("FactionFilterType", 0, _HELP("Defines how the 'Faction' input is used for filtering."), 0, _UICONFIG("enum_int:(0)No_faction_check=0,(1)Only_that_faction=1,(2)All_but_that_faction=2,(3)Any_friendly=3,(4)Any_Hostile=4")),
			InputPortConfig<int>("DeadAliveFilter", 0, _HELP("Filters actors by death/alive status."), 0, _UICONFIG("enum_int:(0)Any,(1)Alive=1,(2)Dead=2")),
			{0}
		};


		config.sDescription = _HELP( "Outputs the nearest actor to a given position. WARNING: potentially performance heavy, dont trigger it every frame." );
		config.pInputPorts = inp_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED);
	}

	//////////////////////////////////////////////////////////////////////////
	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eINP_Trigger))
				{
					CheckNearestActor( pActInfo );
				}
				break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CheckNearestActor( SActivationInfo* pActInfo )
	{
		float maxDist = GetPortFloat( pActInfo, eINP_MaxDist );
		float maxDist2 = maxDist==0 ? FLT_MAX : maxDist * maxDist;
		Vec3 centerPos = GetPortVec3( pActInfo, eINP_CenterPos );

		IActor* pNearestActor = NULL;
		float nearestDist2 = maxDist2;


		EFactionFilterType factionFilterType = (EFactionFilterType)GetPortInt( pActInfo, eINP_FactionFilterType );
		IFactionMap& factionMap = gEnv->pAISystem->GetFactionMap();
		uint8 factionID = factionMap.GetFactionID( GetPortString(pActInfo, eINP_Faction ));
		EDeadAliveFilter deadAliveFilter = (EDeadAliveFilter)GetPortInt( pActInfo, eINP_DeadAliveFilter );

		if (factionID==IFactionMap::InvalidFactionID) 
			factionFilterType = eFCT_NoFilter;

		IActorIteratorPtr actorIt = gEnv->pGame->GetIGameFramework()->GetIActorSystem()->CreateActorIterator();
		while (IActor *pActor=actorIt->Next())
		{
			if (pActor)
			{
				const IAIObject* pAIObject = pActor->GetEntity()->GetAI();
				if (pAIObject)
				{
					uint8 actorFactionID = pAIObject->GetFactionID();
					bool bValid = true;

					switch (factionFilterType)
					{
					case eFCT_SameFactionOnly:
						{
							bValid = actorFactionID==factionID;
							break;
						}

					case eFCT_AllButFaction:
						{
							bValid = actorFactionID!=factionID;
							break;
						}

					case eFCT_AnyFriendly:
						{
							bValid = factionMap.GetReaction( factionID, actorFactionID )==IFactionMap::Friendly;
							break;
						}

					case eFCT_AnyHostile:
						{
							bValid = factionMap.GetReaction( factionID, actorFactionID )==IFactionMap::Hostile;
							break;
						}
					}

					if (deadAliveFilter!=eDA_Any)
					{
						EDeadAliveFilter actorDeadAliveStatus = pActor->IsDead() ? eDA_Dead : eDA_Alive;
						bValid = bValid && (actorDeadAliveStatus == deadAliveFilter);
					}

					if (bValid)
					{
						float dist2 = centerPos.GetSquaredDistance( pActor->GetEntity()->GetPos() );
						if (dist2<=nearestDist2)
						{
							nearestDist2 = dist2;
							pNearestActor = pActor;
						}
					}

				}
			}
		}

		if (pNearestActor)
		{
			ActivateOutput( pActInfo, eOUT_NearestActorId, pNearestActor->GetEntityId() );
			ActivateOutput( pActInfo, eOUT_Faction, int(pNearestActor->GetEntity()->GetAI()->GetFactionID()) );
			ActivateOutput( pActInfo, eOUT_Position, pNearestActor->GetEntity()->GetPos() );
			ActivateOutput( pActInfo, eOUT_Distance, sqrt(nearestDist2) );
			ActivateOutput( pActInfo, eOUT_Alive, !pNearestActor->IsDead() );
		}
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowNode_ActorKill : public CFlowBaseNode<eNCT_Instanced>, public IGameRulesKillListener
{
public:
	CFlowNode_ActorKill( SActivationInfo * pActInfo )
	{
	}

	virtual ~CFlowNode_ActorKill()
	{
		// safety unregister
		if (CGameRules* pGameRules = g_pGame->GetGameRules())
		{
			pGameRules->UnRegisterKillListener(this);
		}
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowNode_ActorKill(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
		if (ser.IsReading())
		{
			RegisterOrUnregisterListener(pActInfo);
		}
	}

	enum EInputPorts
	{
		EIP_Enable = 0,
		EIP_KillerId,
		EIP_VictimId,
	};

	enum EOutputPorts
	{
		EOP_Kill = 0,
		EOP_CollisionKill,
		EOP_KillerId,
		EOP_VictimtId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig<bool>("Enable", false, _HELP("Enable/Disable KillInfo")),
			InputPortConfig<EntityId>("KillerId", _HELP("When connected, limit Kill report to this entity")),
			InputPortConfig<EntityId>("VictimId",  _HELP("When connected, limit Kill report to this entity")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void     ("Kill",  _HELP("Triggers if the kill conditions are fulfilled")),
			OutputPortConfig<bool>		("CollisionKill", _HELP("True if the kill was caused by a collision")),
			OutputPortConfig<EntityId>("KillerId", _HELP("EntityID of the Killer")),
			OutputPortConfig<EntityId>("VictimId",  _HELP("EntityID of the Victim")),
			{0}
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Tracks Kills on Actors.\nAll input conditions (KillerId, VictimId) must be fulfilled to output.\nIf a condition is left empty/not connected, it is regarded as fulfilled.");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			m_actInfo = *pActInfo;  // fall through and enable/disable listener
		case eFE_Activate:
			RegisterOrUnregisterListener(pActInfo);
			break;
		}
	}

	void RegisterOrUnregisterListener(SActivationInfo* pActInfo)
	{
		CGameRules* pGameRules = g_pGame->GetGameRules();
		if (!pGameRules)
		{
			GameWarning("[flow] CFlowHitInfoNode::RegisterListener: No GameRules!");
			return;
		}

		if (GetPortBool(pActInfo, EIP_Enable))
		{
			pGameRules->RegisterKillListener(this);
		}
		else
		{
			pGameRules->UnRegisterKillListener(this);
		}
	}

	// IGameRulesKillListener
	virtual void OnEntityKilledEarly(const HitInfo &hitInfo) {};

	virtual void OnEntityKilled(const HitInfo &hitInfo)
	{
		IF_UNLIKELY(GetPortBool(&m_actInfo, EIP_Enable) == false)
			return;

		const EntityId killerId = GetPortEntityId(&m_actInfo, EIP_KillerId);
		const EntityId victimId = GetPortEntityId(&m_actInfo, EIP_VictimId);

		if((killerId != 0) && (killerId != hitInfo.shooterId))
			return;

		if((victimId != 0) && (victimId != hitInfo.targetId))
			return;

		if(IsVictimActor(hitInfo.targetId) == false)
			return;

		ActivateOutput(&m_actInfo, EOP_Kill, true);
		ActivateOutput(&m_actInfo, EOP_CollisionKill, (hitInfo.type == CGameRules::EHitType::Collision));
		ActivateOutput(&m_actInfo, EOP_KillerId, killerId);
		ActivateOutput(&m_actInfo, EOP_VictimtId, victimId);		
	}
	// ~IGameRulesKillListener

	bool IsVictimActor( const EntityId victimId ) const
	{
		return (g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(victimId) != NULL);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
};

//////////////////////////////////////////////////////////////////////////
class CFlowRagdollizeCharacter : public CFlowBaseNode<eNCT_Instanced>
{
private:
  enum EInputPorts
  {
    EIP_Ragdollize = 0,
    EIP_ImpulseMin,
    EIP_ImpulseMax,
    EIP_AngImpulseMin,
    EIP_AngImpulseMax,
    EIP_ImpulsePoint,
    EIP_Unragdollize
  };

  enum EOutputPorts
  {
    EOP_OnRagdollize = 0,
    EOP_OnUnRagdollize,
    EOP_Impulse,
    EOP_AngImpulse
  };

public:
  CFlowRagdollizeCharacter( SActivationInfo * pActInfo )
  {
  }

  virtual ~CFlowRagdollizeCharacter()
  {
  }

  IFlowNodePtr Clone( SActivationInfo * pActInfo )
  {
    return new CFlowRagdollizeCharacter(pActInfo);
  }

  void Serialize(SActivationInfo* pActInfo, TSerialize ser)
  {
  }

  virtual void GetConfiguration(SFlowNodeConfig& config)
  {
    static const SInputPortConfig inputs[] = {
      InputPortConfig_Void("Ragdollize", _HELP("Ragdollize assigned actor")),
      InputPortConfig<Vec3>("ImpulseMin", Vec3(0,0,0), _HELP("")),
      InputPortConfig<Vec3>("ImpulseMax", Vec3(0,0,0), _HELP("")),
      InputPortConfig<Vec3>("AngImpulseMin", Vec3(0,0,0), _HELP("")),
      InputPortConfig<Vec3>("AngImpulseMax", Vec3(0,0,0), _HELP("")),
      InputPortConfig<Vec3>("ImpulseOrigin", Vec3(0,0,0), _HELP("")),
      InputPortConfig_Void("Unragdollize", _HELP("Unragdollize assigned actor (dead actors will be skipped)")),
      {0}
    };

    static const SOutputPortConfig outputs[] = {
      OutputPortConfig_Void ("OnRagdollize", _HELP("")),
      OutputPortConfig_Void ("UnRagdollize", _HELP("")),
      OutputPortConfig<Vec3> ("Impulse", _HELP("")),
      OutputPortConfig<Vec3> ("AngImpulse", _HELP("")),
      {0}
    };

    config.nFlags |= EFLN_TARGET_ENTITY;
    config.pInputPorts = inputs;
    config.pOutputPorts = outputs;
    config.sDescription = _HELP("(un)ragdollize a character with a defined impulse");
    config.SetCategory(EFLN_ADVANCED);
  }

  virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
  {
    switch (event)
    {
    case eFE_Activate:
      {
        if(!pActInfo->pEntity)
          return;

        CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));

        if(!pActor)
          return;

        if (IsPortActive(pActInfo, EIP_Ragdollize))
        {
          if(pActor->CanRagDollize())
          {
            pActor->RagDollize(true);

            Vec3 impulseValue = cry_random_componentwise(
              GetPortVec3(pActInfo, EIP_ImpulseMin),
              GetPortVec3(pActInfo, EIP_ImpulseMax));
            Vec3 angImpulseValue = cry_random_componentwise(
              GetPortVec3(pActInfo, EIP_AngImpulseMin),
              GetPortVec3(pActInfo, EIP_AngImpulseMax));
            Vec3 impulseOrigin = pActor->GetEntity()->GetWorldPos();
            Matrix34 transMat = pActor->GetEntity()->GetWorldTM();

            pe_action_impulse action;
            action.point = transMat.TransformPoint( impulseOrigin );
            action.impulse = transMat.TransformVector( impulseValue );

            if (!angImpulseValue.IsZero())
              action.angImpulse = transMat.TransformVector( angImpulseValue );

            IEntity* pEntityImpulse = pActor->GetEntity();
            while (pEntityImpulse->GetParent())
            {
              pEntityImpulse = pEntityImpulse->GetParent();
            }

            IPhysicalEntity* pPhysEntity = pEntityImpulse->GetPhysics();
            if (pPhysEntity)
              pPhysEntity->Action( &action );

            ActivateOutput(pActInfo,EOP_OnRagdollize, 1);

            if(!impulseValue.IsZero())
              ActivateOutput(pActInfo, EOP_Impulse, impulseValue);

            if(!angImpulseValue.IsZero())
              ActivateOutput(pActInfo, EOP_AngImpulse, angImpulseValue);
          }
        }

        if (IsPortActive(pActInfo, EIP_Unragdollize))
        {
          if(pActor->IsDead())
            return;

          if(pActor->IsPlayer())
          {
            CPlayer* pPlayer = static_cast<CPlayer*>(pActor);

            if(pPlayer)
              pPlayer->UnRagdollize();
          }
          else if(pActor->CanRagDollize())
            pActor->Revive(CActor::kRFR_Spawn);

          ActivateOutput(pActInfo,EOP_OnUnRagdollize, 1);
        }   
      }
    }
  }

  virtual void GetMemoryUsage(ICrySizer * s) const
  {
    s->Add(*this);
  }
};

//////////////////////////////////////////////////////////////////////////
class CFlowActorMovementParameter : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Set = 0,
		EIP_Get,
		EIP_JumpHeight,
		EIP_WalkSpeed,
		EIP_CrouchSpeed,
		EIP_SwimSpeed
	};

	enum EOutputPorts
	{
		EOP_JumpHeight = 0,
		EOP_WalkSpeed,
		EOP_CrouchSpeed,
		EOP_SwimSpeed
	};

public:
	CFlowActorMovementParameter( SActivationInfo * pActInfo )
	{
	}

	virtual ~CFlowActorMovementParameter()
	{
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorMovementParameter(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = 
		{
			InputPortConfig_Void("Set", _HELP("Set actor values")),
			InputPortConfig_Void("Get", _HELP("Get current actor values")),
			InputPortConfig<float>("JumpHeight", 0.f, _HELP("actor jump height")),
			InputPortConfig<float>("WalkSpeed", 0.f, _HELP("actor base walking speed (without multipliers)")),
			InputPortConfig<float>("CrouchSpeed", 0.f, _HELP("actor base crouch speed (without multipliers)")),
			InputPortConfig<float>("SwimSpeed", 0.f, _HELP("actor base swim speed (without multipliers)")),
			{0}
		};

		static const SOutputPortConfig outputs[] = 
		{
			OutputPortConfig<float>("JumpHeight", _HELP("current actor jump height")),
			OutputPortConfig<float>("WalkSpeed", _HELP("current walking speed (including multipliers)")),
			OutputPortConfig<float>("CrouchSpeed", _HELP("current crouching speed (including multipliers)")),
			OutputPortConfig<float>("SwimSpeed", _HELP("current swimming speed (including multipliers)")),
			{0}
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Set/get actor parameters during runtime. Output always includes multipliers!");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Activate:
			{
				if(!pActInfo->pEntity)
					return;

				CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));

				if(!pActor)
					return;

				SActorParams& actorParams = pActor->GetActorParams();

				if (IsPortActive(pActInfo, EIP_Set))
				{
					if (float jumpHeight = GetPortFloat(pActInfo, EIP_JumpHeight))
					{
						actorParams.jumpHeight = jumpHeight;
					}

					if (float walkSpeed = GetPortFloat(pActInfo, EIP_WalkSpeed))
					{
						pActor->SetStanceMaxSpeed(STANCE_STAND, walkSpeed);
					}

					if (float crouchSpeed = GetPortFloat(pActInfo, EIP_CrouchSpeed))
					{
						pActor->SetStanceMaxSpeed(STANCE_CROUCH, crouchSpeed);
					}

					if (float swimSpeed = GetPortFloat(pActInfo, EIP_SwimSpeed))
					{
						pActor->SetStanceMaxSpeed(STANCE_SWIM, swimSpeed);
					}
				}
				else if (IsPortActive(pActInfo, EIP_Get))
				{
					if(pActor->IsPlayer())
					{
						CPlayer* pPlayer = static_cast<CPlayer*>(pActor);

						ActivateOutput(pActInfo, EOP_JumpHeight,	actorParams.jumpHeight); 
						ActivateOutput(pActInfo, EOP_WalkSpeed,	pPlayer->GetStanceMaxSpeed(STANCE_STAND));
						ActivateOutput(pActInfo, EOP_CrouchSpeed,	pPlayer->GetStanceMaxSpeed(STANCE_CROUCH));
						ActivateOutput(pActInfo, EOP_SwimSpeed,	pPlayer->GetStanceMaxSpeed(STANCE_SWIM));
					}
				}
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowActorSetZGravity : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Set = 0,
		EIP_Value
	};

	enum EOutputPorts
	{
		EOP_Done
	};

public:
	CFlowActorSetZGravity(SActivationInfo * pActInfo)
	{
	}

	virtual ~CFlowActorSetZGravity()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSetZGravity(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Set", _HELP("Set actor z gravity")),
			InputPortConfig<float>("GravityValue", 0.f, _HELP("actor z gravity")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Set actor phy z gravity");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
							 if (!pActInfo->pEntity)
								 return;

							 CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
							 if (!pActor)
								 return;

							 if (IsPortActive(pActInfo, EIP_Set))
							 {
								 pActor->SetGravityZ(GetPortFloat(pActInfo, EIP_Value));
								 ActivateOutput(pActInfo, EOP_Done, 1);
							 }
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowActorSetPhysicalBehaviorParams : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Set = 0,
		EIP_Inertia,
		EIP_InertiaAccel,
		EIP_minSlideAngle,
		EIP_maxClimbAngle,
		EIP_minFallAngle,
		EIP_maxJumpAngle,
		EIP_maxVelGround
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
	};

public:
	CFlowActorSetPhysicalBehaviorParams(SActivationInfo * pActInfo)
	{
	}

	virtual ~CFlowActorSetPhysicalBehaviorParams()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSetPhysicalBehaviorParams(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	const float def_trigger_val = -100.0f;

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Set", _HELP("Set actor phyBh values")),
			InputPortConfig<float>("Inertia", def_trigger_val, _HELP("actor phy movement inertia, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("InertiaAcceleration", def_trigger_val, _HELP("actor phy movement inertia acceleration, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("MinimumSlideAngle", def_trigger_val, _HELP("actor phy movement slide angle, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("MaximumClimbAngle", def_trigger_val, _HELP("actor phy movement climb angle, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("MinimumFallAngle", def_trigger_val, _HELP("actor phy movement fall angle, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("MaximumJumpAngle", def_trigger_val, _HELP("actor phy movement jump angle, if seted value is -100 this parameter be setuped to default")),
			InputPortConfig<float>("MaximumVelocityOnGround", def_trigger_val, _HELP("actor phy movement ground velocity (actor cannot stand of surfaces that are moving faster than this), if seted value is -100 this parameter be setuped to default")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Node to setup physical movement parameters to selected actor, as a slide from nonflat surfaces");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));

			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Set))
			{
				float intertia_def = 10.0f;
				float intertiaAcc_def = 11.0f;
				float slideAngle_def = 45.0f;
				float climbAngle_def = 50.0f;
				float fallAngle_def = 50.0f;
				float jumpAngle_def = 50.0f;
				float velGround_def = 16.0f;
				IPhysicalEntity *pPhysEnt = pActor->GetEntity()->GetPhysics();
				if (pPhysEnt)
				{
					pe_player_dynamics pd;
					if (pPhysEnt->GetParams(&pd))
					{
						SmartScriptTable pEntityTable = pActor->GetEntity()->GetScriptTable();
						if (pEntityTable)
						{
							SmartScriptTable physicsParams;
							if (pEntityTable->GetValue("physicsParams", physicsParams))
							{
								CScriptSetGetChain physicsTableChain(physicsParams);
								SmartScriptTable livingTab;
								if (physicsTableChain.GetValue("Living", livingTab))
								{
									CScriptSetGetChain livingTableChain(livingTab);
									livingTableChain.GetValue("inertia", intertia_def);
									livingTableChain.GetValue("inertiaAccel", intertiaAcc_def);
									livingTableChain.GetValue("min_slide_angle", slideAngle_def);
									livingTableChain.GetValue("max_climb_angle", climbAngle_def);
									livingTableChain.GetValue("max_jump_angle", fallAngle_def);
									livingTableChain.GetValue("min_fall_angle", jumpAngle_def);
									livingTableChain.GetValue("max_vel_ground", velGround_def);
								}
							}

							if(jumpAngle_def <= 0)
								jumpAngle_def = 50.0f;
						}

						if (pActor->IsPlayer())
						{
							if (g_pGameCVars->g_player_phy_enable_setup_from_cvars > 0)
							{
								intertia_def = g_pGameCVars->g_player_phy_inertia;
								intertiaAcc_def = g_pGameCVars->g_player_phy_inertia_accel;
								slideAngle_def = g_pGameCVars->g_player_phy_min_slide_angle;
								climbAngle_def = g_pGameCVars->g_player_phy_max_climb_angle;
								fallAngle_def = g_pGameCVars->g_player_phy_min_fall_angle;
								jumpAngle_def = g_pGameCVars->g_player_phy_max_jump_angle;
							}
						}

						float intertia = GetPortFloat(pActInfo, EIP_Inertia);
						if (intertia == def_trigger_val)
							pd.kInertia = intertia_def;
						else
							pd.kInertia = intertia;

						float intertiaAcc = GetPortFloat(pActInfo, EIP_InertiaAccel);
						if (intertiaAcc == def_trigger_val)
							pd.kInertiaAccel = intertiaAcc_def;
						else
							pd.kInertiaAccel = intertiaAcc;

						float slideAngle = GetPortFloat(pActInfo, EIP_minSlideAngle);
						if (slideAngle == def_trigger_val)
							pd.minSlideAngle = slideAngle_def;
						else
							pd.minSlideAngle = slideAngle;

						float climbAngle = GetPortFloat(pActInfo, EIP_maxClimbAngle);
						if (climbAngle == def_trigger_val)
							pd.maxClimbAngle = climbAngle_def;
						else
							pd.maxClimbAngle = climbAngle;

						float fallAngle = GetPortFloat(pActInfo, EIP_minFallAngle);
						if (fallAngle == def_trigger_val)
							pd.minFallAngle = fallAngle_def;
						else
							pd.minFallAngle = fallAngle;

						float jumpAngle = GetPortFloat(pActInfo, EIP_maxJumpAngle);
						if (jumpAngle == def_trigger_val)
							pd.maxJumpAngle = jumpAngle_def;
						else
							pd.maxJumpAngle = jumpAngle;

						float velGround = GetPortFloat(pActInfo, EIP_maxVelGround);
						if (velGround == def_trigger_val)
							pd.maxVelGround = velGround_def;
						else
							pd.maxVelGround = velGround;

						pActor->SetInertiaAndInertiaAccel(pd.kInertia, pd.kInertiaAccel);
						pPhysEnt->SetParams(&pd);
						ActivateOutput(pActInfo, EOP_Done, 1);
					}
				}
			}
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowActorContainer : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_AddActorId = 0,
		EIP_DelActorById,
		EIP_ClearContainer,
		EIP_ActorToFind,
		EIP_DoOperations,
		EIP_GetNextActorInContainer
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
		EIP_IsActorInContainer,
		EOP_Ids,
		EOP_NextDone
	};

public:
	CFlowActorContainer(SActivationInfo * pActInfo)
	{
		mc_actors_ids.clear();
		counter = 0;
	}

	virtual ~CFlowActorContainer()
	{
		mc_actors_ids.clear();
		mc_actors_ids.resize(0);
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorContainer(pActInfo);
	}

	void Serialize(SActivationInfo * pActInfo, TSerialize ser)
	{
		ser.Value("actorsIds", mc_actors_ids);
		ser.Value("actorsCounter", counter);
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig<EntityId>("AddActorId", 0, _HELP("help")),
			InputPortConfig<EntityId>("DelActorById", 0, _HELP("help")),
			InputPortConfig_Void("ClearContainer", _HELP("help")),
			InputPortConfig<EntityId>("ActorToFind", 0, _HELP("help")),
			InputPortConfig_Void("DoOperations", _HELP("help")),
			InputPortConfig_Void("GetNext", _HELP("help")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			OutputPortConfig_Void("IsActorInContainer", _HELP("")),
			OutputPortConfig<EntityId>("Ids", _HELP("help")),
			OutputPortConfig_Void("NextDone", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-0-");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_DoOperations))
			{
				std::vector<EntityId>::const_iterator it = mc_actors_ids.begin();
				std::vector<EntityId>::const_iterator end = mc_actors_ids.end();
				int count = mc_actors_ids.size();
				if (GetPortEntityId(pActInfo, EIP_ActorToFind) > 0)
				{
					for (int i = 0; i < count, it != end; i++, ++it)
					{
						if (*it == GetPortEntityId(pActInfo, EIP_ActorToFind))
						{
							//ActivateOutput(pActInfo, EOP_Ids, GetPortEntityId(pActInfo, EIP_ActorToFind));
							ActivateOutput(pActInfo, EIP_IsActorInContainer, 1);
							break;
						}
					}
				}

				if (GetPortEntityId(pActInfo, EIP_AddActorId) > 0)
				{
					mc_actors_ids.push_back(GetPortEntityId(pActInfo, EIP_AddActorId));
					count = mc_actors_ids.size();
					it = mc_actors_ids.begin();
					end = mc_actors_ids.end();
					//ActivateOutput(pActInfo, EOP_Ids, GetPortEntityId(pActInfo, EIP_AddActorId));
				}

				if (GetPortEntityId(pActInfo, EIP_DelActorById) > 0)
				{
					for (int i = 0; i < count, it != end; i++, ++it)
					{
						if (*it == GetPortEntityId(pActInfo, EIP_DelActorById))
						{
							mc_actors_ids.erase(it);
							mc_actors_ids.resize(count-1);
							break;
						}
					}
				}
				pActInfo->pInputPorts[EIP_DoOperations].ClearUserFlag();
				ActivateOutput(pActInfo, EOP_Done, 1);
			}

			if (IsPortActive(pActInfo, EIP_GetNextActorInContainer))
			{
				std::vector<EntityId>::const_iterator it = mc_actors_ids.begin();
				std::vector<EntityId>::const_iterator end = mc_actors_ids.end();
				int count = mc_actors_ids.size();
				for (int i = 0; i < count, it != end; i++, ++it)
				{
					if (i < counter)
						continue;

					if (*it > 0)
					{
						ActivateOutput(pActInfo, EOP_Ids, *it);
						counter+=1;
						break;
					}
				}

				if (it == end)
				{
					counter = 0;
					ActivateOutput(pActInfo, EOP_NextDone, 1);
				}
			}

			if (IsPortActive(pActInfo, EIP_ClearContainer))
			{
				mc_actors_ids.clear();
				mc_actors_ids.resize(0);
				ActivateOutput(pActInfo, EOP_Done, 1);
				pActInfo->pInputPorts[EIP_ClearContainer].ClearUserFlag();
			}
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
	std::vector<EntityId> mc_actors_ids;
	int counter = 0;
};

class CFlowActorSimpleGoTo : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Go = 0,
		EIP_Stop,
		EIP_TargetId,
		EIP_PointPos,
		EIP_Direction,
		EIP_DinamicDir,
		EIP_DinamicPos,
		EIP_DistToStop,
		EIP_Stance,
		EIP_Speed,
		EIP_FollowToTarget,
		EIP_TryToJumpObstacle,
		EIP_FollowTime
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
	};

public:
	CFlowActorSimpleGoTo(SActivationInfo * pActInfo)
	{
		timer_lcn = 0.0f;
		timer_lcn2 = 0.0f;
		old_target_pos = Vec3(ZERO);
		m_dangersFlags = eMNMDangers_None;
		m_actInfo = NULL;
		m_cn_actor = NULL;
		m_targetId = 0;
		m_entityId = 0;
		start_target_pos = Vec3(ZERO);
		m_stopWithinDistance = 0.5f;
		m_stopDistanceVariation = 0.0f;
		//m_movementStyle;
		m_considerActorsAsPathObstacles = false;
		m_lengthToTrimFromThePathEnd = 0.5f;
	}

	virtual ~CFlowActorSimpleGoTo()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSimpleGoTo(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Go", _HELP("")),
			InputPortConfig_Void("Stop", _HELP("")),
			InputPortConfig<EntityId>("DestinationEntity",_HELP("Entity to use as destination. It will override the position and direction inputs.")),
			InputPortConfig<Vec3>("Position", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("Direction", Vec3(ZERO), _HELP("")),
			InputPortConfig<bool>("DinamicDir",         true, _HELP("")),
			InputPortConfig<bool>("DinamicPos",         true, _HELP("")),
			InputPortConfig<float>("EndDistance",         0.5f, _HELP("")),
			InputPortConfig<int>("Stance",                0,         0, 0, _UICONFIG("enum_int:Relaxed=0,Alert=1,Combat=2,Crouch=3")),
			InputPortConfig<int>("Speed",                 0,         0, 0, _UICONFIG("enum_int:Walk=0,Run=1,Sprint=2")),
			InputPortConfig<bool>("FollowToEntity",         false, _HELP("")),
			InputPortConfig<bool>("TryToJumpOnObstacle",         false, _HELP("")),
			InputPortConfig<float>("FollowTime",         0.0f, _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig<bool>("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Node to simple request character movement");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			if (pActor)
			{
				m_cn_actor = pActor;
			}
		}
		break;

		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			m_actInfo = *pActInfo;
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			m_cn_actor = pActor;
			if(!m_cn_actor)
				return;

			m_entityId = pActor->GetEntityId();
			RuntimeData& runtimeData = rd_date;
			if (IsPortActive(pActInfo, EIP_Stop))
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}
				runtimeData.ReleaseCurrentMovementRequest();
				RequestStop(m_cn_actor->GetEntityId());
				ActivateOutput(pActInfo, EOP_Done, true);
				return;
			}

			if (IsPortActive(pActInfo, EIP_Go))
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
				}
				m_stopWithinDistance = GetPortFloat(pActInfo, EIP_DistToStop);
				m_considerActorsAsPathObstacles = false;
				if (GetPortEntityId(pActInfo, EIP_TargetId) > 0)
				{
					IEntity *pEntity = gEnv->pEntitySystem->GetEntity(GetPortEntityId(pActInfo, EIP_TargetId));
					if (pEntity)
					{
						start_target_pos = pEntity->GetPos();
						m_targetId = pEntity->GetId();
						IAIObject *pAct = pActor->GetEntity()->GetAI();
						if (pAct)
						{
							IAIActor *pActRt = pAct->CastToIAIActor();
							if (pActRt)
							{
								pActRt->SetAttentionTargetCLH(pEntity->GetAI());
								//pActRt->SetMoveTarget(start_target_pos);
								//pActRt->GoTo(start_target_pos);
							}
						}
					}
					else
					{
						
						start_target_pos = GetPortVec3(pActInfo, EIP_PointPos);
					}
				}
				else
				{
					start_target_pos = GetPortVec3(pActInfo, EIP_PointPos);
				}
				//XmlNodeRef xml_ndadsa = GetISystem()->CreateXmlNode("wwwwwwwwcfgweee342424224141");
				//m_movementStyle.ReadFromXml(xml_ndadsa);
				//m_movementStyle.ConstructDictionariesIfNotAlreadyDone();
				m_movementStyle.SetMovingAlongDesignedPath(false);
				m_movementStyle.SetSpeedLiteral(1.0f);
				m_movementStyle.SetTurnTowardsMovementDirectionBeforeMoving(true);
				m_movementStyle.SetMovingToCover(false);
				m_movementStyle.SetSpeed(MovementStyle::Speed(GetPortInt(pActInfo, EIP_Speed)));
				m_movementStyle.SetStance(MovementStyle::Stance(GetPortInt(pActInfo, EIP_Stance)));
				m_dangersFlags = eMNMDangers_AttentionTarget;
				StartMove();
			}

			if (pActor->IsPlayer())
			{
				CPlayer *pPl = (CPlayer*)pActor;
				if (pPl)
				{
					//CryLogAlways("CFlowActorSimpleGoTo requiest GetPathFinding");
					if (pPl->GetPathFinding())
					{
						//CryLogAlways("CFlowActorSimpleGoTo requiest GetPathFinding sucess");
						if (pPl->GetPathFinding()->IsProcessingRequest())
						{
							pPl->GetPathFinding()->CancelCurrentRequest();
						}
						pPl->GetPathFinding()->RequestMoveTo(GetPortVec3(pActInfo, EIP_PointPos));
					}
				}
			}
		}
		break;
		case eFE_Update:
		{
			int clt = UpdateMovement();
			if (clt == Success)
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}
				ActivateOutput(pActInfo, EOP_Done, true);
			}
			else if (clt == Failure)
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}
				ActivateOutput(pActInfo, EOP_Done, false);
			}
			IEntity *pEntity = gEnv->pEntitySystem->GetEntity(GetPortEntityId(pActInfo, EIP_TargetId));
			if (pEntity)
			{
				old_target_pos = pEntity->GetWorldPos();
			}
		}
		break;
		}
	}
	SActivationInfo m_actInfo;
	CActor *m_cn_actor;
	EntityId m_targetId;
	EntityId m_entityId;
	float timer_lcn;
	float timer_lcn2;
	Vec3 start_target_pos;
	Vec3 old_target_pos;
	float           m_stopWithinDistance;
	float           m_stopDistanceVariation;
	MovementStyle   m_movementStyle;
	MNMDangersFlags m_dangersFlags;
	bool            m_considerActorsAsPathObstacles;
	float           m_lengthToTrimFromThePathEnd;
	enum CNStatus
	{
		Invalid,
		Success,
		Failure,
		Running
	};
	struct RuntimeData
	{
		Vec3              destinationAtTimeOfMovementRequest;
		MovementRequestID movementRequestID;
		int            pendingStatus;
		float             effectiveStopDistanceSq;

		RuntimeData()
			: destinationAtTimeOfMovementRequest(ZERO)
			, movementRequestID(0)
			, pendingStatus(Running)
			, effectiveStopDistanceSq(0.0f)
		{
		}

		~RuntimeData()
		{
			ReleaseCurrentMovementRequest();
		}

		void ReleaseCurrentMovementRequest()
		{
			if (movementRequestID)
			{
				gEnv->pAISystem->GetMovementSystem()->CancelRequest(this->movementRequestID);
				movementRequestID = MovementRequestID();
			}
		}

		void MovementRequestCallback(const MovementRequestResult& result)
		{
			assert(movementRequestID == result.requestID);

			movementRequestID = MovementRequestID();

			if (result == MovementRequestResult::ReachedDestination)
			{
				pendingStatus = Success;
			}
			else
			{
				pendingStatus = Failure;
			}
		}
	};
	RuntimeData rd_date;
	Vec3 DestinationPositionFor() const
	{
		if(!m_cn_actor)
			return Vec3(ZERO);

		if (!m_cn_actor->GetEntity()->GetAI())
			return Vec3(ZERO);

		IAIObject *pActII = m_cn_actor->GetEntity()->GetAI();
		if(!pActII)
			return Vec3(ZERO);

		IAIActor *pActIII = CastToIAIActorSafe(pActII);
		if (!pActIII)
			return Vec3(ZERO);

		if (m_targetId > 0)
		{
			IEntity *pEntTarget = gEnv->pEntitySystem->GetEntity(m_targetId);
			if(!pEntTarget)
				return Vec3(ZERO);

			Vec3 targetPosition = Vec3(ZERO);
			Vec3 targetVelocity = Vec3(ZERO);
			IAIObject *pActTrg = pEntTarget->GetAI();
			if (pActTrg)
			{
				IAIActor *pActRt = CastToIAIActorSafe(pActTrg);
				if (pActRt)
				{
					targetPosition = pActTrg->GetPosInNavigationMesh(pActIII->GetNavigationTypeID());
					targetVelocity = pActTrg->GetVelocity();
				}
				else
				{
					targetPosition = pActTrg->GetPosInNavigationMesh(pActIII->GetNavigationTypeID());
					targetVelocity = pActTrg->GetVelocity();
				}
				//CryLogAlways("DestinationPositionFor AI Ext nave");
			}
			else
			{
				//CryLogAlways("DestinationPositionFor no AI Ext");
			}

			if(targetPosition.IsZero())
				targetPosition = GetPointInNavigationMesh(pActIII->GetNavigationTypeID(), pEntTarget->GetWorldPos());

			//CryLogAlways("targetPosition x = %f y = %f z = %f", targetPosition.x, targetPosition.y, targetPosition.z);
			if (targetVelocity.IsZero())
				targetVelocity = old_target_pos - pEntTarget->GetWorldPos() * 2.0f;

			if(old_target_pos.IsZero())
				targetVelocity = Vec3(ZERO);

			targetVelocity.z = 0.0f;   // Don't consider z axe for the velocity since it could create problems when finding the location in the navigation mesh later
			return targetPosition + targetVelocity * 0.5f;
		}
		else
		{
			if (!start_target_pos.IsZero())
			{
				Vec3 targetPosition_sv = start_target_pos;
				Vec3 targetPosition = GetPointInNavigationMesh(pActIII->GetNavigationTypeID(), targetPosition_sv);
				Vec3 targetVelocity = old_target_pos - start_target_pos * 2.0f;
				if (old_target_pos.IsZero())
					targetVelocity = Vec3(ZERO);
				targetVelocity.z = 0.0f;   // Don't consider z axe for the velocity since it could create problems when finding the location in the navigation mesh later
				return targetPosition + targetVelocity * 0.5f;
			}
			return Vec3(ZERO);
		}
	}

	const Vec3 GetPointInNavigationMesh(const uint32 agentTypeID, const Vec3 inputLocation) const
	{
		Vec3 outputLocation = inputLocation;
		INavigationSystem* pAiNavSys = gEnv->pAISystem->GetNavigationSystem();
		if (!pAiNavSys)
			return inputLocation;

		NavigationMeshID meshID = pAiNavSys->GetEnclosingMeshID(static_cast<NavigationAgentTypeID>(agentTypeID), outputLocation);
		if (meshID)
		{
			pAiNavSys->GetPointInNVMMesh(meshID, outputLocation);
		}
		else
		{
			//CryLogAlways("GetPointInNavigationMesh failed, meshID is null");
		}
		return outputLocation;
	}

	void RequestMovementTo(const Vec3& position, const EntityId entityID, RuntimeData& runtimeData, const bool firstRequest)
	{
		assert(!runtimeData.movementRequestID);
		if (!m_cn_actor)
			return;

		/*if (m_cn_actor->GetMovementController())
		{
			CMovementRequest mr;
			mr.SetAlertness(2);
			mr.SetMoveTarget(position);
			mr.SetJump();
			m_cn_actor->GetMovementController()->RequestMovement(mr);
		}*/
		Vec3 postr_cg = position;
		//postr_cg.x += cry_random(-0.05f, 0.05f);
		//postr_cg.y += cry_random(-0.05f, 0.05f);
		MovementRequest movementRequest;
		movementRequest.entityID = entityID;
		movementRequest.destination = postr_cg;
		movementRequest.callback = functor(runtimeData, &RuntimeData::MovementRequestCallback);
		movementRequest.style = m_movementStyle;
		movementRequest.dangersFlags = m_dangersFlags;
		movementRequest.considerActorsAsPathObstacles = m_considerActorsAsPathObstacles;
		movementRequest.lengthToTrimFromThePathEnd = m_lengthToTrimFromThePathEnd;
		movementRequest.type = MovementRequest::MoveTo;
		if (!firstRequest)
		{
			// While chasing we do not want to trigger a separate turn again while we were
			// already following an initial path.
			movementRequest.style.SetTurnTowardsMovementDirectionBeforeMoving(false);
		}
		runtimeData.movementRequestID = gEnv->pAISystem->GetMovementSystem()->QueueRequest(movementRequest);
		runtimeData.destinationAtTimeOfMovementRequest = postr_cg;
	}

	void ChaseTarget(const EntityId entityID, RuntimeData& runtimeData)
	{
		const Vec3 targetPosition = DestinationPositionFor();

		const float targetDeviation = targetPosition.GetSquaredDistance(runtimeData.destinationAtTimeOfMovementRequest);
		const float deviationThreshold = square(0.5f);

		if (targetDeviation > deviationThreshold)
		{
			runtimeData.ReleaseCurrentMovementRequest();
			RequestMovementTo(targetPosition, entityID, runtimeData, false); // No longer the first request.
		}
	}

	float GetSquaredDistanceToDestination(RuntimeData& runtimeData)
	{
		if (!m_cn_actor)
			return 0.0f;

		const Vec3 destinationPosition = DestinationPositionFor();
		return destinationPosition.GetSquaredDistance(m_cn_actor->GetEntity()->GetWorldPos());
	}

	void RequestStop(const EntityId entityID)
	{
		MovementRequest movementRequest;
		movementRequest.entityID = entityID;
		movementRequest.type = MovementRequest::Stop;
		gEnv->pAISystem->GetMovementSystem()->QueueRequest(movementRequest);
	}

	int UpdateMovement()
	{
		if (!m_cn_actor)
			return 0;

		IEntity *pEntTarget = gEnv->pEntitySystem->GetEntity(m_targetId);
		if (pEntTarget)
		{
			IAIObject *pAct = m_cn_actor->GetEntity()->GetAI();
			if (pAct)
			{
				IAIActor *pActRt = pAct->CastToIAIActor();
				if (pActRt)
				{
					pActRt->SetAttentionTargetCLH(pEntTarget->GetAI());
				}
			}
		}
		RuntimeData& runtimeData = rd_date;
		if (runtimeData.pendingStatus != Running)
			return runtimeData.pendingStatus;

		ChaseTarget(m_cn_actor->GetEntityId(), runtimeData);
		const bool stopMovementWhenWithinCertainDistance = runtimeData.effectiveStopDistanceSq > 0.0f;

		if (stopMovementWhenWithinCertainDistance)
		{
			if (GetSquaredDistanceToDestination(runtimeData) < runtimeData.effectiveStopDistanceSq)
			{
				runtimeData.ReleaseCurrentMovementRequest();
				RequestStop(m_cn_actor->GetEntityId());
				return int(Success);
			}
		}

		return int(Running);
	}

	void StartMove()
	{
		if (!m_cn_actor)
			return;

		RuntimeData& runtimeData = rd_date;
		runtimeData.pendingStatus = Running;
		const Vec3 destinationPosition = DestinationPositionFor();
		runtimeData.effectiveStopDistanceSq = square(m_stopWithinDistance + cry_random(0.0f, m_stopDistanceVariation));
		if (Distance::Point_PointSq(m_cn_actor->GetEntity()->GetWorldPos(), destinationPosition) < runtimeData.effectiveStopDistanceSq)
		{
			runtimeData.pendingStatus = Success;
			return;
		}
		RequestMovementTo(destinationPosition, m_cn_actor->GetEntityId(), runtimeData, true); // First time we request a path.
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowActorSetViewDirection : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Set = 0,
		EIP_TargetPos,
		EIP_Direction,
		EIP_Speed,
	};

	enum EOutputPorts
	{
		EOP_Done
	};

public:
	CFlowActorSetViewDirection(SActivationInfo * pActInfo)
	{
		old_iterpol_value = Vec3(ZERO);
		Start_value = Vec3(ZERO);
		timer_c1 = 0.0f;
		timer_c2 = 0.0f;
		m_targetViewDirection = Quat::CreateIdentity();
	}

	virtual ~CFlowActorSetViewDirection()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSetViewDirection(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Set", _HELP("Set actor view direction")),
			InputPortConfig<Vec3>("TargetPos", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("Direction", Vec3(ZERO), _HELP("")),
			InputPortConfig<float>("Speed", 1.0f, _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Set actor view direction simple");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Set))
			{
				if (pActor->GetMovementController())
				{
					CMovementRequest mr;
					Vec3 dtr = Vec3(ZERO);
					if (!GetPortVec3(pActInfo, EIP_TargetPos).IsZero())
					{
						dtr = GetPortVec3(pActInfo, EIP_TargetPos);
						mr.SetLookTarget(dtr);
					}
					else if (!GetPortVec3(pActInfo, EIP_Direction).IsZero())
					{
						dtr = GetPortVec3(pActInfo, EIP_Direction);
						dtr = pActor->GetEntity()->GetWorldPos() + dtr * 5.0f;
						mr.SetLookTarget(dtr);
					}
					mr.SetDesiredSpeed(GetPortFloat(pActInfo, EIP_Speed));
					mr.SetBodyTarget(dtr);
					mr.SetDesiredBodyDirectionAtTarget(dtr - pActor->GetEntity()->GetWorldPos());
					if (pActor->IsPlayer())
					{
						if (pActInfo && pActInfo->pGraph)
						{
							pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
							nwAction->PauseAimTargetUpdating(true);
						}
						timer_c1 = GetPortFloat(pActInfo, EIP_Speed);
						Vec3 fwd_dir = pActor->GetViewRotation() * Vec3(0,1,0);
						Start_value = pActor->GetEntity()->GetWorldPos() + fwd_dir * 5.0f;
					}
					else
					{
						IAIObject *pAct = pActor->GetEntity()->GetAI();
						if (pAct)
						{
							IAIActor *pActRt = pAct->CastToIAIActor();
							if (pActRt)
							{
								pActRt->GoTo(dtr);
							}
						}
						timer_c1 = GetPortFloat(pActInfo, EIP_Speed);
						//m_targetViewDirection = Quat::CreateRotationVDir(dtr - pActor->GetEntity()->GetWorldPos());
						//pActor->GetEntity()->SetRotation(Quat::CreateSlerp(pActor->GetEntity()->GetRotation(), m_targetViewDirection, 1.0));
					}

					pActor->GetMovementController()->RequestMovement(mr);
				}

				//if (!pActor->IsPlayer())
				//	ActivateOutput(pActInfo, EOP_Done, 1);
			}
		}
		case eFE_Update:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			Vec3 dtr = Vec3(ZERO);
			Vec3 dtr_old = Vec3(ZERO);
			if (!GetPortVec3(pActInfo, EIP_TargetPos).IsZero())
			{
				dtr = GetPortVec3(pActInfo, EIP_TargetPos);
				dtr_old = dtr;
			}
			else if (!GetPortVec3(pActInfo, EIP_Direction).IsZero())
			{
				dtr = GetPortVec3(pActInfo, EIP_Direction);
				dtr = pActor->GetEntity()->GetWorldPos() + dtr * 5.0f;
				dtr_old = dtr;
			}

			if (pActor->IsPlayer())
			{
				timer_c2 += gEnv->pTimer->GetFrameTime()*timer_c1;
				m_targetViewDirection = Quat::CreateRotationVDir(dtr - pActor->GetEntity()->GetWorldPos());
				CPlayer *pPl = (CPlayer*)pActor;
				if (pPl)
				{
					if(pPl->GetViewRotation() == m_targetViewDirection)
					{
						if (pActInfo && pActInfo->pGraph)
						{
							pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
							nwAction->PauseAimTargetUpdating(false);
						}
						timer_c2 = 0.0f;
						timer_c1 = 0.0f;
						ActivateOutput(pActInfo, EOP_Done, 1);
						return;
					}
					else if (pPl->GetViewRotation().GetRotZ() >= m_targetViewDirection.GetRotZ() - 0.05f &&
						pPl->GetViewRotation().GetRotZ() <= m_targetViewDirection.GetRotZ() + 0.05f)
					{
						if (pActInfo && pActInfo->pGraph)
						{
							pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
							nwAction->PauseAimTargetUpdating(false);
						}
						timer_c2 = 0.0f;
						timer_c1 = 0.0f;
						ActivateOutput(pActInfo, EOP_Done, 1);
						return;
					}
					const float TARGET_VIEW_SMOOTH_TIME_INV = (1.0f / 8.0f);
					const float blendFactor = min(timer_c2 * TARGET_VIEW_SMOOTH_TIME_INV, 1.0f);
					pPl->SetViewRotation(Quat::CreateSlerp(pPl->GetViewRotation(), m_targetViewDirection, blendFactor));
				}
			}
			else
			{
				timer_c2 += gEnv->pTimer->GetFrameTime()*timer_c1;
				m_targetViewDirection = Quat::CreateRotationVDir(dtr - pActor->GetEntity()->GetWorldPos());
				if (pActor->GetEntity()->GetRotation() == m_targetViewDirection)
				{
					if (pActInfo && pActInfo->pGraph)
					{
						pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
					}
					timer_c2 = 0.0f;
					timer_c1 = 0.0f;
					ActivateOutput(pActInfo, EOP_Done, 1);
					return;
				}
				else if (pActor->GetEntity()->GetRotation().GetRotZ() >= m_targetViewDirection.GetRotZ() - 0.05f &&
					pActor->GetEntity()->GetRotation().GetRotZ() <= m_targetViewDirection.GetRotZ() + 0.05f)
				{
					if (pActInfo && pActInfo->pGraph)
					{
						pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
					}
					timer_c2 = 0.0f;
					timer_c1 = 0.0f;
					ActivateOutput(pActInfo, EOP_Done, 1);
					return;
				}
				const float TARGET_VIEW_SMOOTH_TIME_INV = (1.0f / 8.0f);
				const float blendFactor = min(timer_c2 * TARGET_VIEW_SMOOTH_TIME_INV, 1.0f);
				//pActor->GetEntity()->SetRotation(Quat::CreateSlerp(pActor->GetEntity()->GetRotation(), m_targetViewDirection, blendFactor));
				if(pActor->GetAnimatedCharacter())
					pActor->GetAnimatedCharacter()->SetEntityRotation(Quat::CreateSlerp(pActor->GetEntity()->GetRotation(), m_targetViewDirection, blendFactor));
			}
			old_iterpol_value = dtr;
		}
		}
	}
	Vec3 Start_value;
	Vec3 old_iterpol_value;
	float timer_c1;
	float timer_c2;
	Quat m_targetViewDirection;
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
class CFlowActorSetViewDirectionCotinuosly : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Start = 0,
		EIP_Stop,
		EIP_TargetId,
		EIP_TargetOffset,
		EIP_TargetPos,
		EIP_Direction,
		EIP_Speed,
	};

	enum EOutputPorts
	{
		EOP_Done
	};

public:
	CFlowActorSetViewDirectionCotinuosly(SActivationInfo * pActInfo)
	{
		old_iterpol_value = Vec3(ZERO);
		Start_value = Vec3(ZERO);
		timer_c1 = 0.0f;
		timer_c2 = 0.0f;
		m_targetViewDirection = Quat::CreateIdentity();
	}

	virtual ~CFlowActorSetViewDirectionCotinuosly()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSetViewDirectionCotinuosly(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Start", _HELP("")),
			InputPortConfig_Void("Stop", _HELP("")),
			InputPortConfig<EntityId>("TargetId", 0, _HELP("")),
			InputPortConfig<Vec3>("TargetOffset", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("TargetPos", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("Direction", Vec3(ZERO), _HELP("")),
			InputPortConfig<float>("Speed", 1.0f, _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Set actor view direction continuosly");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if(!nwAction)
			return;

		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Start))
			{
				if (pActor->GetMovementController())
				{
					//CMovementRequest mr;
					Vec3 dtr = Vec3(ZERO);
					if (!GetPortVec3(pActInfo, EIP_TargetPos).IsZero())
					{
						dtr = GetPortVec3(pActInfo, EIP_TargetPos);
						//mr.SetLookTarget(dtr);
					}
					else if (!GetPortVec3(pActInfo, EIP_Direction).IsZero())
					{
						dtr = GetPortVec3(pActInfo, EIP_Direction);
						dtr = pActor->GetEntity()->GetWorldPos() + dtr * 5.0f;
						//mr.SetLookTarget(dtr);
					}

					if (GetPortEntityId(pActInfo, EIP_TargetId) > 0)
					{
						IEntity *pEnt = gEnv->pEntitySystem->GetEntity(GetPortEntityId(pActInfo, EIP_TargetId));
						if (pEnt)
						{
							dtr = pEnt->GetWorldPos();
							dtr += GetPortVec3(pActInfo, EIP_TargetOffset);
						}
					}

					//mr.SetDesiredSpeed(GetPortFloat(pActInfo, EIP_Speed));
					//mr.SetBodyTarget(dtr);
					//mr.SetDesiredBodyDirectionAtTarget(dtr - pActor->GetEntity()->GetWorldPos());
					if (pActor->IsPlayer())
					{
						if (pActInfo && pActInfo->pGraph)
						{
							pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
							nwAction->PauseAimTargetUpdating(true);
						}
						timer_c1 = GetPortFloat(pActInfo, EIP_Speed);
						Vec3 fwd_dir = pActor->GetViewRotation() * Vec3(0, 1, 0);
						Start_value = pActor->GetEntity()->GetWorldPos() + fwd_dir * 5.0f;
					}
					else
					{
						IAIObject *pAct = pActor->GetEntity()->GetAI();
						if (pAct)
						{
							IAIActor *pActRt = pAct->CastToIAIActor();
							if (pActRt)
							{
								pActRt->GoTo(dtr);
							}
						}
					}

					//pActor->GetMovementController()->RequestMovement(mr);
				}
			}

			if (IsPortActive(pActInfo, EIP_Stop))
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}

				if (pActor->IsPlayer())
				{
					nwAction->PauseAimTargetUpdating(false);
				}
				timer_c2 = 0.0f;
				timer_c1 = 0.0f;
				ActivateOutput(pActInfo, EOP_Done, 1);
				return;
			}
		}
		case eFE_Update:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			Vec3 dtr = Vec3(ZERO);
			Vec3 dtr_old = Vec3(ZERO);
			if (!GetPortVec3(pActInfo, EIP_TargetPos).IsZero())
			{
				dtr = GetPortVec3(pActInfo, EIP_TargetPos);
				dtr += GetPortVec3(pActInfo, EIP_TargetOffset);
				dtr_old = dtr;
			}
			else if (!GetPortVec3(pActInfo, EIP_Direction).IsZero())
			{
				dtr = GetPortVec3(pActInfo, EIP_Direction);
				dtr = pActor->GetEntity()->GetWorldPos() + dtr * 5.0f;
				dtr += GetPortVec3(pActInfo, EIP_TargetOffset);
				dtr_old = dtr;
			}

			if (GetPortEntityId(pActInfo, EIP_TargetId) > 0)
			{
				IEntity *pEnt = gEnv->pEntitySystem->GetEntity(GetPortEntityId(pActInfo, EIP_TargetId));
				if (pEnt)
				{
					dtr = pEnt->GetWorldPos();
					dtr += GetPortVec3(pActInfo, EIP_TargetOffset);
				}
			}

			if (pActor->IsPlayer())
			{
				timer_c2 += gEnv->pTimer->GetFrameTime()*timer_c1;
				m_targetViewDirection = Quat::CreateRotationVDir(dtr - pActor->GetEntity()->GetWorldPos());
				CPlayer *pPl = (CPlayer*)pActor;
				if (pPl)
				{
					const float TARGET_VIEW_SMOOTH_TIME_INV = (1.0f / 8.0f);
					const float blendFactor = min(timer_c2 * TARGET_VIEW_SMOOTH_TIME_INV, 1.0f);
					pPl->SetViewRotation(Quat::CreateSlerp(pPl->GetViewRotation(), m_targetViewDirection, blendFactor));
				}
			}
			else
			{
				IAIObject *pAct = pActor->GetEntity()->GetAI();
				if (pAct)
				{
					IAIActor *pActRt = pAct->CastToIAIActor();
					if (pActRt)
					{
						pActRt->GoTo(dtr);
					}
				}
			}
			old_iterpol_value = dtr;
		}
		}
	}
	Vec3 Start_value;
	Vec3 old_iterpol_value;
	float timer_c1;
	float timer_c2;
	Quat m_targetViewDirection;
	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowGetActorsInRange : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Get = 0,
		EIP_OnlyDead,
		EIP_Range,
		EIP_Point,
	};

	enum EOutputPorts
	{
		EOP_ActorId,
		EOP_Done
	};

public:
	CFlowGetActorsInRange(SActivationInfo * pActInfo)
	{
		m_TimerOutput = NULL;
		xxt = 0;
	}

	virtual ~CFlowGetActorsInRange()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowGetActorsInRange(pActInfo);
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Get", _HELP("")),
			InputPortConfig<int>("OnlyDead", 0, _HELP("")),
			InputPortConfig<float>("Range", 1.0f, _HELP("")),
			InputPortConfig<Vec3>("Point", Vec3(ZERO), _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig<EntityId>("ActorId", _HELP("")),
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("------");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_Get))
			{
				m_List.clear();
				m_List.resize(0);
				m_pActInfo = pActInfo;
				if (!gEnv->pEntitySystem)
					return;

				IEntityItPtr pIt = gEnv->pEntitySystem->GetEntityIterator();
				pIt->MoveFirst();
				xxt = 0;
				for (int i = 0; !pIt->IsEnd(); ++i)
				{
					IEntity *pEntity = pIt->Next();
					if (pEntity)
					{
						CActor *pActor = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
						if (pActor)
						{
							int dead_only = GetPortInt(pActInfo, EIP_OnlyDead);
							if (dead_only == 1)
							{
								if (!pActor->IsDead())
									continue;
							}
							else if(dead_only == 2)
							{
								if (pActor->IsDead())
									continue;
							}

							Vec3 pfcs = GetPortVec3(pActInfo, EIP_Point);
							if (pfcs.GetDistance(pActor->GetEntity()->GetPos()) <= GetPortFloat(pActInfo, EIP_Range))
							{
								if (dead_only == 3)
								{
									if (pActor->IsDead())
									{
										//xxt = 3;
										pActor->RequestToDeequipAllEquippedItems();
										/*
										if (pActor->GetEntity()->GetCharacter(0))
										{
											IAttachmentManager *pAttachmentManager = pActor->GetEntity()->GetCharacter(0)->GetIAttachmentManager();
											if (pAttachmentManager)
											{
												const int att_num = pAttachmentManager->GetAttachmentCount();
												string names_att[255];
												for (int i = 0; i < att_num; i++)
												{
													IAttachment *pAttacments_player_nrm = pAttachmentManager->GetInterfaceByIndex(i);
													if (pAttacments_player_nrm)
													{
														names_att[i] = pAttacments_player_nrm->GetName();
														pAttacments_player_nrm->ClearBinding(CA_CharEditModel);
														//pAttacments_player_nrm->Release();
													}
												}

												for (int i = 0; i < att_num; i++)
												{
													if(!names_att[i].empty())
														pAttachmentManager->RemoveAttachmentByName(names_att[i]);
												}
											}
										}		*/
										
										//pActor->SetActorModel("");
										
										
										/*pActor->GetEntity()->SetSlotFlags(0, 0);
										if (pActor->GetAnimatedCharacter())
										{
											pActor->GetAnimatedCharacter()->Release();
										}

										if (pActor->GetEntity()->GetPhysics())
										{
											pActor->GetEntity()->GetPhysics()->Release();
										}
										
										if (pActor->GetEntity()->GetCharacter(0))
											pActor->GetEntity()->GetCharacter(0)->Release();*/
																									   
										pActor->GetEntity()->SetPos(Vec3(cry_random(-100000000, -10000000), cry_random(-100000000, -10000000), cry_random(-100000000, -10000000)));
										if (gEnv->IsEditor())
											pActor->GetEntity()->Hide(true);
										else
										{
											pActor->GetEntity()->Hide(true);
											//gEnv->pGame->GetIGameFramework()->GetIActorSystem()->RemoveActor(pActor->GetEntityId());
										}

										pActor->block_updates = true;
										pActor->block_updates_cn = true;
									}
									else
										continue;
								}
								//ActivateOutput(pActInfo, EOP_ActorId, pActor->GetEntityId());
								m_List.push_back(pActor->GetEntityId());
							}
						}
					}
				}

				if(xxt != 3)
					Output();
				else
				{
					if (!m_TimerOutput)
					{
						m_TimerOutput = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CFlowGetActorsInRange::DelayedOutput), NULL);
					}
				}
			}
		}
		}
	}

	void DelayedOutput(void* pUserData, IGameFramework::TimerID handler)
	{
		if (m_TimerOutput)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerOutput);
			m_TimerOutput = NULL;
		}
		Output();
	}

	void Output()
	{
		std::list<EntityId>::iterator it = m_List.begin();
		std::list<EntityId>::iterator end = m_List.end();
		const int outputCount = m_List.size();
		//CryLogAlways("CFlowBaseIteratorNode Output value outputCount = %i", outputCount);
		for (int i = 0; i < outputCount, it != end; i++, ++it)
		{
			if (xxt == 3)
			{
				CActor *pActor = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(*it));
				if (pActor && pActor->GetEntity()->GetCharacter(0))
				{
					/*IAttachmentManager *pAttachmentManager = pActor->GetEntity()->GetCharacter(0)->GetIAttachmentManager();
					if (pAttachmentManager)
					{
						const int att_num = pAttachmentManager->GetAttachmentCount();
						string names_att[255];
						for (int i = 0; i < att_num; i++)
						{
							IAttachment *pAttacments_player_nrm = pAttachmentManager->GetInterfaceByIndex(i);
							if (pAttacments_player_nrm)
							{
								names_att[i] = pAttacments_player_nrm->GetName();
								pAttacments_player_nrm->ClearBinding(CA_CharEditModel);
								//pAttacments_player_nrm->Release();
							}
						}

						for (int i = 0; i < att_num; i++)
						{
							if (!names_att[i].empty())
								pAttachmentManager->RemoveAttachmentByName(names_att[i], CA_CharEditModel);
						}
					}*/
					pActor->GetEntity()->Hide(true);
					pActor->block_updates = true;
					pActor->block_updates_cn = true;
				}
			}
			ActivateOutput(m_pActInfo, EOP_ActorId, *it);
		}

		if(outputCount > 0)
			ActivateOutput(m_pActInfo, EOP_Done, true);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	std::list<EntityId> m_List;
	uint32 m_TimerOutput;
	int xxt;
	SActivationInfo* m_pActInfo;
};

class CFlowGetActorsInRangeFtPoints : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Get = 0,
		EIP_OnlyDead,
		EIP_Point1,
		EIP_Point2,
	};

	enum EOutputPorts
	{
		EOP_ActorId,
		EOP_Done
	};

public:
	CFlowGetActorsInRangeFtPoints(SActivationInfo * pActInfo)
	{
		m_TimerOutput = NULL;
		xxt = 0;
	}

	virtual ~CFlowGetActorsInRangeFtPoints()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowGetActorsInRangeFtPoints(pActInfo);
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Get", _HELP("")),
			InputPortConfig<int>("OnlyDead", 0, _HELP("")),
			InputPortConfig<Vec3>("BoxPointMin", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("BoxPointMax", Vec3(ZERO), _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig<EntityId>("ActorId", _HELP("")),
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("------");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_Get))
			{
				m_List.clear();
				m_List.resize(0);
				m_pActInfo = pActInfo;
				if (!gEnv->pEntitySystem)
					return;

				IEntityItPtr pIt = gEnv->pEntitySystem->GetEntityIterator();
				pIt->MoveFirst();
				xxt = 0;
				for (int i = 0; !pIt->IsEnd(); ++i)
				{
					IEntity *pEntity = pIt->Next();
					if (pEntity)
					{
						CActor *pActor = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
						if (pActor)
						{
							int dead_only = GetPortInt(pActInfo, EIP_OnlyDead);
							if (dead_only == 1)
							{
								if (!pActor->IsDead())
									continue;
							}
							else if (dead_only == 2)
							{
								if (pActor->IsDead())
									continue;
							}
							AABB bbox;
							bbox.Reset();
							bbox.max = GetPortVec3(pActInfo, EIP_Point2);
							bbox.min = GetPortVec3(pActInfo, EIP_Point1);
							bool actor_in = bbox.IsContainPoint(pActor->GetEntity()->GetPos());
							if (actor_in)
							{
								if (dead_only == 3)
								{
									if (pActor->IsDead())
									{
										//xxt = 3;
										pActor->RequestToDeequipAllEquippedItems();
										pActor->GetEntity()->SetPos(Vec3(cry_random(-100000000, -10000000), cry_random(-100000000, -10000000), cry_random(-100000000, -10000000)));
										if (gEnv->IsEditor())
											pActor->GetEntity()->Hide(true);
										else
										{
											pActor->GetEntity()->Hide(true);
											//gEnv->pGame->GetIGameFramework()->GetIActorSystem()->RemoveActor(pActor->GetEntityId());
										}

										pActor->block_updates = true;
										pActor->block_updates_cn = true;
									}
									else
										continue;
								}
								else if (dead_only == 4)
								{
									if (pActor->IsDead())
									{
										if (gEnv->IsEditor())
											pActor->GetEntity()->Hide(true);
										else
										{
											pActor->GetEntity()->Hide(true);
										}

										pActor->block_updates = true;
										pActor->block_updates_cn = true;
									}
								}
								else if (dead_only == 5)
								{
									if (pActor->IsDead())
									{
										if (gEnv->IsEditor())
											pActor->GetEntity()->Hide(false);
										else
										{
											pActor->GetEntity()->Hide(false);
										}

										pActor->block_updates = false;
										pActor->block_updates_cn = false;
									}
								}
								//ActivateOutput(pActInfo, EOP_ActorId, pActor->GetEntityId());
								m_List.push_back(pActor->GetEntityId());
							}
						}
					}
				}

				if (xxt != 3)
					Output();
				else
				{
					if (!m_TimerOutput)
					{
						m_TimerOutput = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CFlowGetActorsInRangeFtPoints::DelayedOutput), NULL);
					}
				}
			}
		}
		}
	}

	void DelayedOutput(void* pUserData, IGameFramework::TimerID handler)
	{
		if (m_TimerOutput)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerOutput);
			m_TimerOutput = NULL;
		}
		Output();
	}

	void Output()
	{
		std::list<EntityId>::iterator it = m_List.begin();
		std::list<EntityId>::iterator end = m_List.end();
		const int outputCount = m_List.size();
		//CryLogAlways("CFlowBaseIteratorNode Output value outputCount = %i", outputCount);
		for (int i = 0; i < outputCount, it != end; i++, ++it)
		{
			if (xxt == 3)
			{
				CActor *pActor = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(*it));
				if (pActor && pActor->GetEntity()->GetCharacter(0))
				{
					pActor->GetEntity()->Hide(true);
					pActor->block_updates = true;
					pActor->block_updates_cn = true;
				}
			}
			ActivateOutput(m_pActInfo, EOP_ActorId, *it);
		}

		if (outputCount > 0)
			ActivateOutput(m_pActInfo, EOP_Done, true);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	std::list<EntityId> m_List;
	uint32 m_TimerOutput;
	int xxt;
	SActivationInfo* m_pActInfo;
};

class CFlowActorSndContStimul : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Start = 0,
		EIP_Stop,
		EIP_TargetId,
		EIP_TargetPos,
		EIP_TargetType,
		EIP_StimulType,
		EIP_Rate,
		EIP_SendDistantStimul,
		EIP_DistantStimulDist,
		EIP_DistantStimulName,
		EIP_NormalStimulName,
	};

	enum EOutputPorts
	{
		EOP_Done
	};

public:
	CFlowActorSndContStimul(SActivationInfo * pActInfo)
	{
		rate_timer_x = 0.0f;
	}

	virtual ~CFlowActorSndContStimul()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorSndContStimul(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Start", _HELP("")),
			InputPortConfig_Void("Stop", _HELP("")),
			InputPortConfig<EntityId>("TargetId", 0, _HELP("")),
			InputPortConfig<Vec3>("TargetPos", Vec3(ZERO), _HELP("")),
			InputPortConfig<int>("TargetType", 4, _HELP("")),
			InputPortConfig<int>("StimulType", 0, _HELP("")),
			InputPortConfig<float>("Rate", 0.1f, _HELP("")),
			InputPortConfig<bool>("SendDistantStimul", false, _HELP("")),
			InputPortConfig<float>("DistantStimulDist", 2.0f, _HELP("")),
			InputPortConfig<string>("DistantStimulName", _HELP("")),
			InputPortConfig<string>("NormalStimulName", _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Send continuosly stimul event");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Start))
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
				}
			}

			if (IsPortActive(pActInfo, EIP_Stop))
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}
				ActivateOutput(pActInfo, EOP_Done, 1);
				return;
			}
		}
		case eFE_Update:
		{
			rate_timer_x += gEnv->pTimer->GetFrameTime();
			if (rate_timer_x > GetPortFloat(pActInfo, EIP_Rate))
			{
				rate_timer_x = 0.0f;
				CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
				if (!pActor)
					return;

				IAISystem *pAISystem = gEnv->pAISystem;
				ITargetTrackManager *pTargetTrackManager = pAISystem ? pAISystem->GetTargetTrackManager() : NULL;
				IAIObject* pOwnerAI = pActor->GetEntity()->GetAI();
				if (pTargetTrackManager && pOwnerAI)
				{
					IEntity *pEnt = gEnv->pEntitySystem->GetEntity(GetPortEntityId(pActInfo, EIP_TargetId));
					IAIObject *pTargetAI = pEnt ? pEnt->GetAI() : NULL;
					if (pTargetAI)
					{
						const tAIObjectID aiOwnerId = pOwnerAI->GetAIObjectID();
						const tAIObjectID aiTargetId = pTargetAI->GetAIObjectID();
						Vec3 trg_pos = GetPortVec3(pActInfo, EIP_TargetPos);
						if (trg_pos.IsZero())
						{
							if (pEnt)
							{
								trg_pos = pEnt->GetWorldPos();
								trg_pos.z += 0.3f;
							}
						}
						string norm_stim_name = GetPortString(pActInfo, EIP_NormalStimulName);
						string dist_stim_name = GetPortString(pActInfo, EIP_DistantStimulName);
						if (norm_stim_name.empty())
						{
							norm_stim_name = "MeleeHit";
						}
						if (dist_stim_name.empty())
						{
							dist_stim_name = "MeleeHitNear";
						}
						TargetTrackHelpers::SStimulusEvent eventInfo;
						eventInfo.vPos = trg_pos;
						eventInfo.eStimulusType = TargetTrackHelpers::EAIEventStimulusType(GetPortInt(pActInfo, EIP_StimulType));
						eventInfo.eTargetThreat = EAITargetThreat(GetPortInt(pActInfo, EIP_TargetType));
						pTargetTrackManager->HandleStimulusEventForAgent(aiTargetId, aiOwnerId, norm_stim_name, eventInfo);
						if (GetPortBool(pActInfo, EIP_SendDistantStimul))
							pTargetTrackManager->HandleStimulusEventInRange(aiOwnerId, dist_stim_name, eventInfo, GetPortFloat(pActInfo, EIP_DistantStimulDist));

						IAIActor *pActRt = pOwnerAI->CastToIAIActor();
						if (pActRt)
						{
							pActRt->SetAttentionTargetCLH(pTargetAI);
							//pActRt->SetMoveTarget(trg_pos);
						}
						//CryLogAlways("HandleStimulusEventForAgent called");
					}
				}
			}
		}
		}
	}

	float rate_timer_x;

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

//////////////////////////////////////////////////////////////////////////
REGISTER_FLOW_NODE( "AI:BodyCount",		CFlowNode_AIBodyCount );
REGISTER_FLOW_NODE( "AI:AIAwareness", CFlowNode_AIAwareness );
REGISTER_FLOW_NODE( "Actor:FacialAnim",		CFlowActorFacialAnim );
REGISTER_FLOW_NODE( "Actor:FacialExpression",		CFlowActorFacialExpression );
REGISTER_FLOW_NODE( "Actor:PlayerKnockDown",		CFlowNode_PlayerKnockDown );
REGISTER_FLOW_NODE( "Actor:PlayerInteractiveAnimation", CFlowNode_PlayerInteractiveAnimation );
REGISTER_FLOW_NODE( "Actor:PlayerLookAt",		CFlowNode_PlayerLookAt );
REGISTER_FLOW_NODE( "Actor:AliveCheck", CFlowActorAliveCheck);
REGISTER_FLOW_NODE( "Actor:PlayerCinematicControl", CFlowNode_PlayerCinematicControl);
REGISTER_FLOW_NODE( "Actor:KillPlayer",	CFlowNode_ActorKillPlayer );
REGISTER_FLOW_NODE( "Actor:SetPlayerModel",	CFlowNode_ActorSetPlayerModel );
REGISTER_FLOW_NODE( "Actor:PlayerDropObject", CFlowNode_PlayerDropObject );
REGISTER_FLOW_NODE( "Actor:GetNearestActor", CFlowNode_GetNearestActor );
REGISTER_FLOW_NODE("Actor:KillInfo", CFlowNode_ActorKill);
REGISTER_FLOW_NODE( "Actor:PlayerIsInAir", CFlowPlayerIsInAir);
REGISTER_FLOW_NODE( "Actor:RagdollizeCharacter", CFlowRagdollizeCharacter);
REGISTER_FLOW_NODE( "Actor:LocalPlayerMovementParameter", CFlowActorMovementParameter);
//----special node for trg-------------------------------------------
REGISTER_FLOW_NODE("Actor:SetZgravity", CFlowActorSetZGravity);
//-------------------------------------------------------------------
REGISTER_FLOW_NODE("Actor:SetPhysicalBehaviorParams", CFlowActorSetPhysicalBehaviorParams);
REGISTER_FLOW_NODE("Actor:ActorIdsContainer", CFlowActorContainer);
REGISTER_FLOW_NODE("Actor:ActorSimpleGoTo", CFlowActorSimpleGoTo);
REGISTER_FLOW_NODE("Actor:ActorSetViewDirection", CFlowActorSetViewDirection);
REGISTER_FLOW_NODE("Actor:ActorSetViewDirectionCotinuosly", CFlowActorSetViewDirectionCotinuosly);

REGISTER_FLOW_NODE("Actor:GetActorsInRange", CFlowGetActorsInRange);
REGISTER_FLOW_NODE("Actor:GetActorsInBox", CFlowGetActorsInRangeFtPoints);

REGISTER_FLOW_NODE("Actor:ActorSndContStimul", CFlowActorSndContStimul);