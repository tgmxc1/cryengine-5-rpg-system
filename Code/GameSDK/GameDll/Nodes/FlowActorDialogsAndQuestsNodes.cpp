// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "Player.h"
#include "Actor.h"
#include "Nodes/G2FlowBaseNode.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "DialogSystemEvents.h"
#include "ActorActionsNew.h"
#include "GameXMLSettingAndGlobals.h"

#include <CryString/StringUtils.h>

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowActorDialogLoadNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogLoadNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorDialogLoadNode()
	{

	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogLoadNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_DialogLoad,
	};

	enum EOutputPorts
	{
		EOP_DialogLoad,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<bool>  ("DialogLoad", false, _HELP("dialog load or unload")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("DialogLoad", _HELP("Triggered on current dialog load")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor DialogSys Load Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					if(g_pGame->GetHUDCommon())
					{
						StageChanged(g_pGame->GetHUDCommon()->m_DialogSystemAltVisible);
					}
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
					if (pGameGlobals)
					{
						if(GetPortBool(pActInfo, EIP_DialogLoad))
							pGameGlobals->SetDialogMainNpc(pActor->GetEntityId());
						else
							pGameGlobals->SetDialogMainNpc(0);
					}

					if (g_pGame->GetHUDCommon())
					{
						g_pGame->GetHUDCommon()->ShowDialogSystemAlt(GetPortBool(pActInfo, EIP_DialogLoad));
					}
					if (g_pGame->GetHUDCommon())
					{
						StageChanged(g_pGame->GetHUDCommon()->m_DialogSystemAltVisible);
					}
				}
			}
			break;
		}
	}

	void StageChanged(bool load)
	{
		ActivateOutput(&m_actInfo, EOP_DialogLoad, load);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

class CFlowActorDialogAddQuestionNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogAddQuestionNode( SActivationInfo * pActInfo ) : m_entityId (0), autoselected_line_ID (0)
	{
		/*if(pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowActorDialogAddQuestionNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogAddQuestionNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Question,
		EIP_Questionid,
		EIP_QuestionDialogStage,
		EIP_QuestionNextDialogStage,
		EIP_QuestionLocalized,
		EIP_QuestionidAutoSelection,
		EIP_Autoclear,
	};

	enum EOutputPorts
	{
		EOP_QuestionPressed,
		
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("QuestionName", "", _HELP("")),
			InputPortConfig<int>  ("QuestionId", 0, _HELP("")),
			InputPortConfig<int>  ("QuestionDialogStage", 1, _HELP("")),
			InputPortConfig<int>  ("QuestionNextDialogStage", 0, _HELP("")),
			InputPortConfig<bool>	("QuestionLocalized", true, _HELP("")),
			InputPortConfig<bool> ("AutoQuestionId", true, _HELP("")),
			InputPortConfig<bool> ("AutoClearDialogOnPress", false, _HELP("")),
			
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("QuestionPressed", _HELP("Question pressed listner")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor DialogSys Question Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}

				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				if(!g_pGame->GetDialogSystemEvents())
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					/*if (GetPortBool(pActInfo, EIP_QuestionidAutoSelection))
						EventST(g_pGame->GetDialogSystemEvents()->GetPressedId(autoselected_line_ID));
					else
						EventST(g_pGame->GetDialogSystemEvents()->GetPressedId(GetPortInt(pActInfo, EIP_Questionid)));*/
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
					if (!pGameGlobals)
						return;

					if (!g_pGame->GetDialogSystemEvents())
						return;

					if (pActor->GetEntityId() == pGameGlobals->GetDialogMainNpc())
					{
						if(pActor->GetDialogStage() == GetPortInt(pActInfo, EIP_QuestionDialogStage))
						{
							if (g_pGame->GetHUDCommon())
							{
								g_pGame->GetDialogSystemEvents()->ResetPressedAtThisTime();
								if (pActInfo && pActInfo->pGraph)
								{
									pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								}

								string line = GetPortString(pActInfo, EIP_Question);
								bool want_to_localize = GetPortBool(pActInfo, EIP_QuestionLocalized);
								if (want_to_localize)
								{
									ILocalizationManager* pLocMgr = gEnv->pSystem->GetLocalizationManager();
									if (pLocMgr)
									{
										//line = "";
										//pLocMgr->LocalizeString(GetPortString(pActInfo, EIP_Question), line);
									}
								}

								if (GetPortBool(pActInfo, EIP_QuestionidAutoSelection))
								{
									autoselected_line_ID = g_pGame->GetHUDCommon()->GetDialogLinesLoadedNumber() + 1;
								}

								if(autoselected_line_ID > 0)
									g_pGame->GetHUDCommon()->AddDialogLine(line, autoselected_line_ID);
								else
									g_pGame->GetHUDCommon()->AddDialogLine(line, GetPortInt(pActInfo, EIP_Questionid));
							}
						}
					}
				}
			}
			break;

		case eFE_Update:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (!g_pGame->GetDialogSystemEvents())
					return;

				if (pGameGlobals->GetDialogMainNpc() <= 0)
				{
					if (pActInfo && pActInfo->pGraph)
					{
						pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
					}
					return;
				}

				if (pActor->GetEntityId() == pGameGlobals->GetDialogMainNpc())
				{
					if(pActor->GetDialogStage() == GetPortInt(pActInfo, EIP_QuestionDialogStage))
					{
						int lineID = 0;
						if (GetPortBool(pActInfo, EIP_QuestionidAutoSelection))
						{
							lineID = autoselected_line_ID;
						}

						if (lineID == 0)
							lineID = GetPortInt(pActInfo, EIP_Questionid);

						if (g_pGame->GetDialogSystemEvents()->GetPressedId(lineID) == true)
						{
							if (pActInfo && pActInfo->pGraph)
							{
								pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
							}

							g_pGame->GetDialogSystemEvents()->SetPressedId(lineID, false);
							if (GetPortInt(pActInfo, EIP_QuestionNextDialogStage) > 0)
								pActor->SetDialogStage(GetPortInt(pActInfo, EIP_QuestionNextDialogStage));

							if (GetPortBool(pActInfo, EIP_Autoclear))
							{
								if (g_pGame->GetHUDCommon())
									g_pGame->GetHUDCommon()->DialogSystemClear();
							}
							EventST(true);
						}
						else
						{
							if(g_pGame->GetDialogSystemEvents()->IsAnyQuestionPressedAtThisTime())
							{
								if (pActInfo && pActInfo->pGraph)
								{
									pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								}
							}
						}
					}
					else
					{
						if (g_pGame->GetDialogSystemEvents()->IsAnyQuestionPressedAtThisTime())
						{
							if (pActInfo && pActInfo->pGraph)
							{
								pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
							}
						}
						else
						{
							if (pActInfo && pActInfo->pGraph)
							{
								pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
							}
						}
					}
				}
			}
			break;
		}
	}

	void EventST(bool pressed)
	{
		
		ActivateOutput(&m_actInfo, EOP_QuestionPressed, pressed);
	}


	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	int autoselected_line_ID;
};

class CFlowActorDialogAddAnswereNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogAddAnswereNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorDialogAddAnswereNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogAddAnswereNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_DialogLoad,
	};

	enum EOutputPorts
	{
		EOP_DialogLoad,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("DialogAddAnswere", "", _HELP("Trigger to AddAnswere")),
			
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("DialogAddAnswere", _HELP("Triggered on AddAnswere")),
			
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor DialogSys Answere Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{

				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					if (g_pGame->GetHUDCommon())
						g_pGame->GetHUDCommon()->DialogNPCWords(GetPortString(pActInfo, EIP_DialogLoad));
				
					StageChanged(true);
				}
			}
			break;
		}
	}

	void StageChanged(bool load)
	{
		ActivateOutput(&m_actInfo, EOP_DialogLoad, load);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};

class CFlowActorDialogClearNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogClearNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorDialogClearNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogClearNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
	};

	enum EOutputPorts
	{
		EOP_DialogLoad,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("DialogCleared", _HELP("Triggered on ClearedDialog")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor DialogSys Clear Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					if (g_pGame->GetHUDCommon())
						g_pGame->GetHUDCommon()->DialogSystemClear();

					StageChanged(true);
				}
			}
			break;
		}
	}

	void StageChanged(bool load)
	{
		ActivateOutput(&m_actInfo, EOP_DialogLoad, load);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};


class CFlowActorQuestAddNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorQuestAddNode( SActivationInfo * pActInfo )
	{
	}

	~CFlowActorQuestAddNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorQuestAddNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_QuestId,
		EIP_QuestLcStage,
		EIP_QuestStage,
		EIP_QuestFlag,
		EIP_QuestLast,
		EIP_CompletePercent,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_QuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("Quest", _HELP("Trigger to add Quest")),
			InputPortConfig<int>  ("QuestId", 0, _HELP("Trigger to add QuestId")),
			InputPortConfig<int>  ("QuestLcStage", 0, _HELP("Trigger to add QuestStage")),
			InputPortConfig<int>  ("QuestStatus", 0, _HELP("Trigger to add QuestStatus")),
			InputPortConfig<int>  ("QuestFlag", 0, _HELP("Trigger to add QuestFlag")),
			InputPortConfig<int>  ("QuestLast", 0, _HELP("Trigger to add QuestLast")),
			InputPortConfig<float>("QuestCompletePercent", 0.0f, _HELP("Quest Complete Percent")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>  ("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>  ("QuestId", _HELP("Triggered on current added QuestId")),
			{0}
		};
		config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Quest add Node(legacy, be replaced by new node in next update)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				
			}
			break;
		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pGameGlobals->AddQuest(GetPortString(pActInfo, EIP_Quest), GetPortInt(pActInfo, EIP_QuestId), GetPortInt(pActInfo, EIP_QuestStage), 
						GetPortInt(pActInfo, EIP_QuestFlag), GetPortInt(pActInfo, EIP_QuestLast), GetPortInt(pActInfo, EIP_QuestLcStage),
						GetPortFloat(pActInfo, EIP_CompletePercent));

					ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
					ActivateOutput(pActInfo, EOP_QuestId, GetPortString(pActInfo, EIP_QuestId));
				}
			}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowNoXMLQuestAddNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowNoXMLQuestAddNode(SActivationInfo * pActInfo)
	{

	}

	~CFlowNoXMLQuestAddNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowNoXMLQuestAddNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_QuestSD,
		EIP_QuestFD,
		EIP_QuestId,
		EIP_QuestLcStage,
		EIP_QuestStage,
		EIP_QuestFlag,
		EIP_QuestLast,
		EIP_CompletePercent
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_QuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Add", _HELP("Trigger")),
			InputPortConfig<string>("QuestName", _HELP("Name of the added quest(can be localized)")),
			InputPortConfig<string>("QuestShortdescription", _HELP("Short description of added quest(can be localized)")),
			InputPortConfig<string>("QuestFulldescription", _HELP("Full description of added quest(can be localized)")),
			InputPortConfig<int>("QuestId", 0, _HELP("Trigger to add QuestId")),
			InputPortConfig<int>("QuestLcStage", 0, _HELP("Trigger to add QuestStage")),
			InputPortConfig<int>("QuestStatus", 0, _HELP("Trigger to add QuestStatus")),
			InputPortConfig<int>("QuestFlag", 0, _HELP("Trigger to add QuestFlag")),
			InputPortConfig<int>("QuestLast", 0, _HELP("Trigger to add QuestLast")),
			InputPortConfig<float>("QuestCompletePercent", 0.0f, _HELP("Quest Complete Percent")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>("QuestId", _HELP("Triggered on current added QuestId")),
			{ 0 }
		};
		config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Add Quest to player journal Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				string dec[2];
				dec[0] = GetPortString(pActInfo, EIP_QuestSD);
				dec[1] = GetPortString(pActInfo, EIP_QuestFD);
				pGameGlobals->AddQuestNoXML(GetPortString(pActInfo, EIP_Quest), dec,
					GetPortInt(pActInfo, EIP_QuestId), GetPortInt(pActInfo, EIP_QuestStage),
					GetPortInt(pActInfo, EIP_QuestFlag), GetPortInt(pActInfo, EIP_QuestLast), 
					GetPortInt(pActInfo, EIP_QuestLcStage),	GetPortFloat(pActInfo, EIP_CompletePercent));

				ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
				ActivateOutput(pActInfo, EOP_QuestId, GetPortString(pActInfo, EIP_QuestId));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowActorSubQuestAddNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorSubQuestAddNode( SActivationInfo * pActInfo )
	{
	}

	~CFlowActorSubQuestAddNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorSubQuestAddNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_SubQuestId,
		EIP_SubQuestParentStage,
		EIP_SubQuestStat,
		EIP_SubQuestLast,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_SubQuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("Quest", "", _HELP("Trigger to add Quest")),
			InputPortConfig<int>  ("SubQuestId", 0, _HELP("Trigger to add SubQuestId")),
			InputPortConfig<int>  ("SubQuestParentStage", 0, _HELP("Trigger to add SubQuestStatus")),
			InputPortConfig<int>  ("SubQuestStatus", 0, _HELP("Trigger to add SubQuestStatus")),
			InputPortConfig<int>  ("SubQuestLastChanged", 0, _HELP("Trigger to add SubQuestLastChanged")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>  ("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>  ("SubQuestId", _HELP("Triggered on current added SubQuestId")),
			{0}
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor SubQuest add Node(legacy, be replaced by new node in next update)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				
			}
			break;
		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pGameGlobals->AddSubQuest(GetPortString(pActInfo, EIP_Quest), GetPortInt(pActInfo, EIP_SubQuestId),
						GetPortInt(pActInfo, EIP_SubQuestStat), GetPortInt(pActInfo, EIP_SubQuestLast));
					
					ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
					ActivateOutput(pActInfo, EOP_SubQuestId, GetPortString(pActInfo, EIP_SubQuestId));
				}
			}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowNoXMLSubQuestAddNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowNoXMLSubQuestAddNode(SActivationInfo * pActInfo)
	{
	}

	~CFlowNoXMLSubQuestAddNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowNoXMLSubQuestAddNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_PNQuest,
		EIP_QuestSD,
		EIP_QuestFD,
		EIP_SubQuestId,
		EIP_SubQuestStat,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_SubQuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Add", _HELP("Trigger")),
			InputPortConfig<string>("Quest", "", _HELP("Sub Quest Name")),
			InputPortConfig<string>("ParentQuestName", _HELP("Short description of added quest(can be localized)")),
			InputPortConfig<string>("QuestShortdescription", _HELP("Short description of added quest(can be localized)")),
			InputPortConfig<string>("QuestFulldescription", _HELP("Full description of added quest(can be localized)")),
			InputPortConfig<int>("SubQuestId", 0, _HELP("Trigger to add SubQuestId")),
			InputPortConfig<int>("SubQuestStatus", 0, _HELP("Trigger to add SubQuestStatus")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>("SubQuestId", _HELP("Triggered on current added SubQuestId")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Add Sub Quest to player journal Node(is required already added parent quest in journal)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				string dec[2];
				dec[0] = GetPortString(pActInfo, EIP_QuestSD);
				dec[1] = GetPortString(pActInfo, EIP_QuestFD);
				pGameGlobals->AddSubQuestNoXML(GetPortString(pActInfo, EIP_PNQuest), GetPortString(pActInfo, EIP_Quest), dec,
					GetPortInt(pActInfo, EIP_SubQuestId), GetPortInt(pActInfo, EIP_SubQuestStat), 1);

				ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
				ActivateOutput(pActInfo, EOP_SubQuestId, GetPortString(pActInfo, EIP_SubQuestId));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowActorSubQuestUpdateNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorSubQuestUpdateNode( SActivationInfo * pActInfo )
	{
	}

	~CFlowActorSubQuestUpdateNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorSubQuestUpdateNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_SubQuestId,
		EIP_SubQuestParentStage,
		EIP_SubQuestStat,
		EIP_SubQuestLast,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_SubQuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("Quest", "", _HELP("Trigger to add Quest")),
			InputPortConfig<int>  ("SubQuestId", 0, _HELP("Trigger to add SubQuestId")),
			InputPortConfig<int>  ("SubQuestParentStage", 0, _HELP("Trigger to add SubQuestStatus")),
			InputPortConfig<int>  ("SubQuestStatus", 0, _HELP("Trigger to add SubQuestStatus")),
			InputPortConfig<int>  ("SubQuestLastChanged", 0, _HELP("Trigger to add SubQuestLastChanged")),

			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>  ("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>  ("SubQuestId", _HELP("Triggered on current added SubQuestId")),
			{0}
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor SubQuest update Node(legacy, be replaced by new node in next update)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				
			}
			break;
		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pGameGlobals->UpdateSubQuest(GetPortString(pActInfo, EIP_Quest), GetPortInt(pActInfo, EIP_SubQuestId),
						GetPortInt(pActInfo, EIP_SubQuestStat), 0, GetPortInt(pActInfo, EIP_SubQuestLast), 0, 0, 0);
					
					ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
					ActivateOutput(pActInfo, EOP_SubQuestId, GetPortString(pActInfo, EIP_SubQuestId));
				}
			}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowNoXMLSubQuestUpdateNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowNoXMLSubQuestUpdateNode(SActivationInfo * pActInfo)
	{
	}

	~CFlowNoXMLSubQuestUpdateNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowNoXMLSubQuestUpdateNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_QuestSD,
		EIP_QuestFD,
		EIP_SubQuestId,
		EIP_SubQuestStat,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_SubQuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Update", _HELP("Trigger")),
			InputPortConfig<string>("Quest", "", _HELP("Sub Quest Name")),
			InputPortConfig<string>("QuestShortdescription", _HELP("Short description of added quest(can be localized)")),
			InputPortConfig<string>("QuestFulldescription", _HELP("Full description of added quest(can be localized)")),
			InputPortConfig<int>("SubQuestId", 0, _HELP("Trigger to add SubQuestId")),
			InputPortConfig<int>("SubQuestStatus", 0, _HELP("Trigger to add SubQuestStatus")),

			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>("SubQuestId", _HELP("Triggered on current added SubQuestId")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor SubQuest add Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				string dec[2];
				dec[0] = GetPortString(pActInfo, EIP_QuestSD);
				dec[1] = GetPortString(pActInfo, EIP_QuestFD);
				pGameGlobals->UpdateSubQuestNoXML(GetPortString(pActInfo, EIP_Quest), dec,
					GetPortInt(pActInfo, EIP_SubQuestId), GetPortInt(pActInfo, EIP_SubQuestStat), 1, 1);

				ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
				ActivateOutput(pActInfo, EOP_SubQuestId, GetPortString(pActInfo, EIP_SubQuestId));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowActorQuestUpdateNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorQuestUpdateNode( SActivationInfo * pActInfo )
	{
	}

	~CFlowActorQuestUpdateNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorQuestUpdateNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_QuestId,
		EIP_QuestLcStage,
		EIP_QuestStage,
		EIP_QuestFlag,
		EIP_QuestLast,
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_QuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<string>  ("Quest", "", _HELP("Trigger to add Quest")),
			InputPortConfig<int>  ("QuestId", 0, _HELP("Trigger to add QuestId")),
			InputPortConfig<int>  ("QuestLcStage", 0, _HELP("Trigger to add QuestStage")),
			InputPortConfig<int>  ("QuestStage", 0, _HELP("Trigger to add QuestStage")),
			InputPortConfig<int>  ("QuestFlag", 0, _HELP("Trigger to add QuestFlag")),
			InputPortConfig<int>  ("QuestLast", 0, _HELP("Trigger to add QuestLast")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>  ("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>  ("QuestId", _HELP("Triggered on current added QuestId")),
			{0}
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Quest update Node(legacy, be replaced by new node in next update)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
		
			}
			break;
		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pGameGlobals->UpdateQuest(GetPortString(pActInfo, EIP_Quest), GetPortInt(pActInfo, EIP_QuestId),
						GetPortInt(pActInfo, EIP_QuestStage), GetPortInt(pActInfo, EIP_QuestFlag),
						GetPortInt(pActInfo, EIP_QuestLast), 0, 0, 0);

					ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
					ActivateOutput(pActInfo, EOP_QuestId, GetPortString(pActInfo, EIP_QuestId));
				}
			}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowNoXMLQuestUpdateNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowNoXMLQuestUpdateNode(SActivationInfo * pActInfo)
	{
	}

	~CFlowNoXMLQuestUpdateNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowNoXMLQuestUpdateNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Quest,
		EIP_QuestSD,
		EIP_QuestFD,
		EIP_QuestId,
		EIP_QuestLcStage,
		EIP_QuestStage,
		EIP_QuestFlag,
		EIP_QuestLast,
		EIP_CompletePercent
	};

	enum EOutputPorts
	{
		EOP_Quest,
		EOP_QuestId,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Update", _HELP("Trigger")),
			InputPortConfig<string>("QuestName", _HELP("Name of quest to update (can be localized)")),
			InputPortConfig<string>("QuestShortdescription", _HELP("Short description of added quest(can be localized)")),
			InputPortConfig<string>("QuestFulldescription", _HELP("Full description of added quest(can be localized)")),
			InputPortConfig<int>("QuestId", 0, _HELP("Trigger to add QuestId")),
			InputPortConfig<int>("QuestLcStage", 0, _HELP("Trigger to add QuestStage")),
			InputPortConfig<int>("QuestStatus", 0, _HELP("Trigger to add QuestStatus")),
			InputPortConfig<int>("QuestFlag", 0, _HELP("Trigger to add QuestFlag")),
			InputPortConfig<int>("QuestLast", 0, _HELP("Trigger to add QuestLast")),
			InputPortConfig<float>("QuestCompletePercent", 0.0f, _HELP("Quest Complete Percent")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<string>("Quest", _HELP("Triggered on current added Quest")),
			OutputPortConfig<int>("QuestId", _HELP("Triggered on current added QuestId")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Update Quest in player journal Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				string dec[2];
				dec[0] = GetPortString(pActInfo, EIP_QuestSD);
				dec[1] = GetPortString(pActInfo, EIP_QuestFD);
				pGameGlobals->UpdateQuestNoXML(GetPortString(pActInfo, EIP_Quest), dec, GetPortInt(pActInfo, EIP_QuestId),
					GetPortInt(pActInfo, EIP_QuestStage), GetPortInt(pActInfo, EIP_QuestFlag), GetPortInt(pActInfo, EIP_QuestLast),
					GetPortFloat(pActInfo, EIP_CompletePercent));

				ActivateOutput(pActInfo, EOP_Quest, GetPortString(pActInfo, EIP_Quest));
				ActivateOutput(pActInfo, EOP_QuestId, GetPortString(pActInfo, EIP_QuestId));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};


class CFlowActorDialogQuestionListner : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogQuestionListner( SActivationInfo * pActInfo ) : m_entityId (0)
	{
		/*if(pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowActorDialogQuestionListner()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogQuestionListner(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Questionid,
		EIP_QuestionDialogStage,
	};

	enum EOutputPorts
	{
		EOP_QuestionPressed,
		
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<int>  ("QuestionId", 0, _HELP("")),
			InputPortConfig<int>  ("QuestionDialogStage", 1, _HELP("")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("QuestionPressed", _HELP("Question pressed listner")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor DialogSys Question Pressed Listner Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					EventST(g_pGame->GetDialogSystemEvents()->GetPressedId(GetPortInt(pActInfo, EIP_Questionid)));
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					if (pActInfo && pActInfo->pGraph)
					{
						pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
					}

					if (pActor->GetEntityId() == pGameGlobals->GetDialogMainNpc())
					{

					}
				}

			}
			break;

		case eFE_Update:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (pActor->GetEntityId() == pGameGlobals->GetDialogMainNpc())
				{
					if(g_pGame->GetDialogSystemEvents()->GetOldDs() == GetPortInt(pActInfo, EIP_QuestionDialogStage))
					{
						if (g_pGame->GetDialogSystemEvents()->GetPressedId(GetPortInt(pActInfo, EIP_Questionid)) == true)
						{
							g_pGame->GetDialogSystemEvents()->SetPressedId(GetPortInt(pActInfo, EIP_Questionid), false);
							EventST(true);
						}
					}
				}
			}
			break;
		}
	}

	void EventST(bool pressed)
	{
		ActivateOutput(&m_actInfo, EOP_QuestionPressed, pressed);
		m_actInfo.pGraph->SetRegularlyUpdated(m_actInfo.myID, false);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

class CFlowQuestSetGetCMPLPercent : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowQuestSetGetCMPLPercent(SActivationInfo * pActInfo)
	{
	}

	~CFlowQuestSetGetCMPLPercent()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_TriggerSet = 0,
		EIP_TriggerGet,
		EIP_Quest,
		EIP_QuestId,
		EIP_QuestCompletePercent,
	};

	enum EOutputPorts
	{
		EOP_QuestCompletePercent,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Set", _HELP("----------")),
			InputPortConfig_Void("Get", _HELP("----------")),
			InputPortConfig<string>("Quest", _HELP("----------")),
			InputPortConfig<int>("QuestId", 0, _HELP("----------")),
			InputPortConfig<float>("QuestCompletePercentToSet", 0.0f, _HELP("----------")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<float>("QuestCompletePercent", _HELP("----------")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Get or set quest complete percents");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (IsPortActive(pActInfo, EIP_TriggerSet))
			{
				if(!GetPortString(pActInfo, EIP_Quest).empty())
					pGameGlobals->SetQuestCompletePercent(GetPortString(pActInfo, EIP_Quest), GetPortFloat(pActInfo, EIP_QuestCompletePercent));
				else
					pGameGlobals->SetQuestCompletePercentById(GetPortInt(pActInfo, EIP_QuestId), GetPortFloat(pActInfo, EIP_QuestCompletePercent));
			}

			if (IsPortActive(pActInfo, EIP_TriggerGet))
			{
				if (!GetPortString(pActInfo, EIP_Quest).empty())
					ActivateOutput(pActInfo, EOP_QuestCompletePercent, pGameGlobals->GetQuestCompletePercent(GetPortString(pActInfo, EIP_Quest)));
				else
					ActivateOutput(pActInfo, EOP_QuestCompletePercent, pGameGlobals->GetQuestCompletePercentById(GetPortInt(pActInfo, EIP_QuestId)));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowStartTrade : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowStartTrade(SActivationInfo * pActInfo)
	{
	}

	~CFlowStartTrade()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_TriggerStart = 0,
		EIP_SellerId,
		EIP_SellerName,
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Start", _HELP("----------")),
			InputPortConfig<EntityId>("Seller", 0, _HELP("----------")),
			InputPortConfig<string>("SellerName", _HELP("----------")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done", _HELP("----------")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Start traiding with seller character");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			if (IsPortActive(pActInfo, EIP_TriggerStart))
			{
				if (!GetPortString(pActInfo, EIP_SellerName).empty())
				{
					IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->FindEntityByName(GetPortString(pActInfo, EIP_SellerName));
					if (pEntityc)
						nwAction->StartTraiding(pEntityc->GetId());
				}
				else
				{
					nwAction->StartTraiding(GetPortEntityId(pActInfo, EIP_SellerId));
				}
				ActivateOutput(pActInfo, EOP_Done, true);
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowEnableOrDisableDialogCloseButton : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowEnableOrDisableDialogCloseButton(SActivationInfo * pActInfo)
	{
	}

	~CFlowEnableOrDisableDialogCloseButton()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_TriggerStart = 0,
		EIP_Enable,
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("SetButtonState", _HELP("----------")),
			InputPortConfig<bool>("ButtonState", false, _HELP("----------")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done", _HELP("----------")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-------------------------------");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{

		}
		break;
		case eFE_Activate:
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			if (IsPortActive(pActInfo, EIP_TriggerStart))
			{
				if (g_pGame->GetHUDCommon())
				{
					g_pGame->GetHUDCommon()->EnableCurrentDialogCloseButton(GetPortBool(pActInfo, EIP_Enable));
				}
				ActivateOutput(pActInfo, EOP_Done, true);
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowActorDialogEndedEvents : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogEndedEvents(SActivationInfo * pActInfo) : m_entityId(0)
	{
		
	}

	~CFlowActorDialogEndedEvents()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorDialogEndedEvents(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
	};

	enum EOutputPorts
	{
		EOP_QuestionPressed,

	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("StartChecks", _HELP("Trigger")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>("IsDialogEndedWithThisCharacter", _HELP("---")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-------");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
		}
		break;
		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			if (pActor == 0)
				return;

			if (IsPortActive(pActInfo, EIP_Trigger))
			{
				m_actInfo = *pActInfo;
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
				}
			}
		}
		break;

		case eFE_Update:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			if (pActor == 0)
				return;

			if (m_entityId != pGameGlobals->GetDialogMainNpc())
			{
				if (pActInfo && pActInfo->pGraph)
				{
					pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
				}
				EventST(true);
			}
		}
		break;
		}
	}

	void EventST(bool pressed)
	{
		ActivateOutput(&m_actInfo, EOP_QuestionPressed, pressed);
	}


	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};


REGISTER_FLOW_NODE("Game:ActorDialogLoad", CFlowActorDialogLoadNode);
REGISTER_FLOW_NODE("Game:ActorDialogAddQuestion", CFlowActorDialogAddQuestionNode);
REGISTER_FLOW_NODE("Game:ActorDialogQuestionListner", CFlowActorDialogQuestionListner);
REGISTER_FLOW_NODE("Game:ActorDialogAddAnswere", CFlowActorDialogAddAnswereNode);
REGISTER_FLOW_NODE("Game:ActorDialogClear", CFlowActorDialogClearNode);
REGISTER_FLOW_NODE("Game:ActorQuestAddNode", CFlowActorQuestAddNode);
REGISTER_FLOW_NODE("Game:ActorSubQuestAddNode", CFlowActorSubQuestAddNode);
REGISTER_FLOW_NODE("Game:ActorQuestUpdateNode", CFlowActorQuestUpdateNode);
REGISTER_FLOW_NODE("Game:ActorSubQuestUpdateNode", CFlowActorSubQuestUpdateNode);
REGISTER_FLOW_NODE("QuestSystem:AddQuest", CFlowNoXMLQuestAddNode);
REGISTER_FLOW_NODE("QuestSystem:AddSubQuestNode", CFlowNoXMLSubQuestAddNode);
REGISTER_FLOW_NODE("QuestSystem:UpdateQuestNode", CFlowNoXMLQuestUpdateNode);
REGISTER_FLOW_NODE("QuestSystem:UpdateSubQuestNode", CFlowNoXMLSubQuestUpdateNode);
REGISTER_FLOW_NODE("QuestSystem:SetOrGetQuestCompletePercents", CFlowQuestSetGetCMPLPercent);
REGISTER_FLOW_NODE("TradeSystem:StartTrade", CFlowStartTrade);
REGISTER_FLOW_NODE("DialogSystemUI:EnableOrDisableDialogCloseButton", CFlowEnableOrDisableDialogCloseButton);
REGISTER_FLOW_NODE("DialogSystemUI:DialogEndedEvents", CFlowActorDialogEndedEvents);


