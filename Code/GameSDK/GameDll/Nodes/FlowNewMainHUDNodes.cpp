// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "Player.h"
#include "Actor.h"
#include "Nodes/G2FlowBaseNode.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "DialogSystemEvents.h"
#include "ActorActionsNew.h"
#include "GameXMLSettingAndGlobals.h"

#include <CryString/StringUtils.h>

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowShowMainHud : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowShowMainHud(SActivationInfo * pActInfo)
	{

	}

	~CFlowShowMainHud()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Show
	};

	enum EOutputPorts
	{
		EOP_Done
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<bool>("show", false, _HELP("show or hide main hud")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done"),
			{ 0 }
		};
		//config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Hide or Show main hud UI");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;

		case eFE_Activate:
		{
							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 bool show = GetPortBool(pActInfo, EIP_Show);
								 if (g_pGame->GetHUDCommon())
								 {
									 if (show)
									 {
										 if (!g_pGame->GetHUDCommon()->MainHud_loaded)
											g_pGame->GetHUDCommon()->ShowMainHud();
									 }
									 else
									 {
										 if (g_pGame->GetHUDCommon()->MainHud_loaded)
											g_pGame->GetHUDCommon()->ShowMainHud();
									 }
									 g_pGame->GetHUDCommon()->ShowMinimap(show);
									 g_pGame->GetHUDCommon()->ShowSimpleCrosshair(show, show);
									 g_pGame->GetHUDCommon()->ShowSelectedWeapons(show, show);
								 }
								 ActivateOutput(&m_actInfo, EOP_Done, 0);
							 }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
};

class CFlowUpdateMainHud : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowUpdateMainHud(SActivationInfo * pActInfo)
	{

	}

	~CFlowUpdateMainHud()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Option
	};

	enum EOutputPorts
	{
		EOP_Done
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<int>("OptionKey", 0, _HELP("type of update")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done"),
			{ 0 }
		};
		//config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Update main hud UI");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;

		case eFE_Activate:
		{
							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 int opt = GetPortInt(pActInfo, EIP_Option);
								 if (g_pGame->GetHUDCommon())
								 {
									
									 g_pGame->GetHUDCommon()->Update();
								 }
								 ActivateOutput(&m_actInfo, EOP_Done, 0);
							 }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
};

class CFlowEnableCharacterCreatingMenu : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowEnableCharacterCreatingMenu(SActivationInfo * pActInfo)
	{

	}

	~CFlowEnableCharacterCreatingMenu()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_EnableMenu = 0,
		EIP_CheckEnabled,
		EIP_ClearEnableState
	};

	enum EOutputPorts
	{
		EOP_IsEnabled
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig<bool>("EnableMenu", false, _HELP("000")),
			InputPortConfig_Void("CheckEnabled", _HELP("000")),
			InputPortConfig_Void("ClearEnableState", _HELP("000")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>("IsEnabled", _HELP("000")),
			{ 0 }
		};
		//config.nFlags |= EFLN_APPROVED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Check/Enable/Disable/ClearStatus for Character Creating Menu");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_ClearEnableState))
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals && g_pGame->GetHUDCommon())
				{
					if(!g_pGame->GetHUDCommon()->m_CharacterCreationVisible)
						pGameGlobals->enable_character_creating_menu_at_game_start = true;
				}
			}

			if (IsPortActive(pActInfo, EIP_CheckEnabled))
			{
				if (g_pGame->GetHUDCommon())
				{
					if (g_pGame->GetHUDCommon()->m_PostInitScreenVisible)
					{
						ActivateOutput(pActInfo, EOP_IsEnabled, true);
						break;
					}
					ActivateOutput(pActInfo, EOP_IsEnabled, g_pGame->GetHUDCommon()->m_CharacterCreationVisible);
				}
			}

			if (IsPortActive(pActInfo, EIP_EnableMenu))
			{
				if (g_pGame->GetHUDCommon())
				{
					bool enabled = false;
					if (GetPortBool(pActInfo, EIP_EnableMenu))
					{
						if (!g_pGame->GetHUDCommon()->m_CharacterCreationVisible && !g_pGame->GetHUDCommon()->m_PostInitScreenVisible)
						{
							g_pGame->GetHUDCommon()->ShowCharacterCreatingMenu();
							enabled = g_pGame->GetHUDCommon()->m_CharacterCreationVisible;
						}
					}
					else
					{
						if (g_pGame->GetHUDCommon()->m_CharacterCreationVisible)
						{
							g_pGame->GetHUDCommon()->ShowCharacterCreatingMenu();
							enabled = g_pGame->GetHUDCommon()->m_CharacterCreationVisible;
						}
					}
					ActivateOutput(pActInfo, EOP_IsEnabled, enabled);
				}
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

class CFlowAddIngameHintMsg : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowAddIngameHintMsg(SActivationInfo * pActInfo)
	{

	}

	~CFlowAddIngameHintMsg()
	{

	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Add = 0,
		EIP_x,
		EIP_y,
		EIP_time,
		EIP_msg,
		EIP_scale
	};

	enum EOutputPorts
	{
		EOP_Done
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Add", _HELP("000")),
			InputPortConfig<int>("onscreenX", 0, _HELP("000")),
			InputPortConfig<int>("onscreenY", 0, _HELP("000")),
			InputPortConfig<float>("Time", 1.0f, _HELP("000")),
			InputPortConfig<string>("MessageText", _HELP("000")),
			InputPortConfig<float>("Scale", 1.0f, _HELP("000")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Done"),
			{ 0 }
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Add onscreen game hint");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_Add))
			{
				if (g_pGame->GetHUDCommon())
				{
					g_pGame->GetHUDCommon()->AddHint(Vec2i(GetPortInt(pActInfo, EIP_x), GetPortInt(pActInfo, EIP_y)), GetPortFloat(pActInfo, EIP_time),
						GetPortString(pActInfo, EIP_msg), GetPortFloat(pActInfo, EIP_scale));
				}
				ActivateOutput(pActInfo, EOP_Done, 0);
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

REGISTER_FLOW_NODE("UserHUD:ShowMainHUD", CFlowShowMainHud);
REGISTER_FLOW_NODE("UserHUD:UpdateMainHUD", CFlowUpdateMainHud);
REGISTER_FLOW_NODE("UserHUD:CharacterCreatingMenuControl", CFlowEnableCharacterCreatingMenu);
REGISTER_FLOW_NODE("UserHUD:AddOnScreenIngameHint", CFlowAddIngameHintMsg);