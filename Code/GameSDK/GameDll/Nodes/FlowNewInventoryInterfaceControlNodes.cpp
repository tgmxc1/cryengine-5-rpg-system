// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "Player.h"
#include "Actor.h"
#include "Nodes/G2FlowBaseNode.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "DialogSystemEvents.h"
#include "ActorActionsNew.h"
#include "GameXMLSettingAndGlobals.h"

#include <CryString/StringUtils.h>

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowAddNewAdditiveEquipmentPack : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Add = 0,
		EIP_EquipmentPackName,
		EIP_EquipItemsFromThisPack
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
	};

public:
	CFlowAddNewAdditiveEquipmentPack(SActivationInfo * pActInfo)
	{
	
	}

	virtual ~CFlowAddNewAdditiveEquipmentPack()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowAddNewAdditiveEquipmentPack(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{

	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Add", _HELP("help")),
			InputPortConfig<string>("AdditiveEquipmentPackName", _HELP("help")),
			InputPortConfig<bool>("Equip", _HELP("Equip/not equip items from added equipment pack")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-0-");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));

			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Add))
			{
				pActor->AddAdditionalEquipPermoment(GetPortString(pActInfo, EIP_EquipmentPackName).c_str(), GetPortBool(pActInfo, EIP_EquipItemsFromThisPack));
				ActivateOutput(pActInfo, EOP_Done, 1);
			}
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowClearCharacterInventoryCLR : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Clear = 0,
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
	};

public:
	CFlowClearCharacterInventoryCLR(SActivationInfo * pActInfo)
	{

	}

	virtual ~CFlowClearCharacterInventoryCLR()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowClearCharacterInventoryCLR(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{

	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Clear", _HELP("help")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-0-");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));

			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Clear))
			{
				IInventory *pInventory = pActor->GetInventory();
				if (pInventory)
				{
					std::list<EntityId> player_items;
					player_items.clear();
					for (int i = 0; i < pInventory->GetCount(); i++)
					{
						player_items.push_back(pInventory->GetItem(i));
					}
					std::list<EntityId>::iterator it_pl = player_items.begin();
					std::list<EntityId>::iterator end_pl = player_items.end();
					int count_pl_i = player_items.size();
					for (int i = 0; i < count_pl_i; ++i, ++it_pl)
					{
						CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(*it_pl));
						if (pItem)
						{
							if (pItem->GetOwnerId() != pActor->GetEntityId())
								continue;

							if (!pItem->IsItemIgnoredByHud())
							{
								if (pItem->GetEquipped() > 0)
								{
									pActor->EquipItem(pItem->GetEntityId());
								}
								pInventory->RemoveItem(pItem->GetEntityId());
							}
						}
					}
					player_items.clear();
					player_items.resize(0);
				}
				ActivateOutput(pActInfo, EOP_Done, 1);
			}
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowEquipItem : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Equip = 0,
		EIP_UnEquip,
		EIP_ItemId,
		EIP_ItemName,
		EIP_ItemClassName
	};

	enum EOutputPorts
	{
		EOP_Done = 0,
		EOP_ItemId
	};

public:
	CFlowEquipItem(SActivationInfo * pActInfo)
	{

	}

	virtual ~CFlowEquipItem()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowEquipItem(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{

	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Equip", _HELP("help")),
			InputPortConfig_Void("UnEquip", _HELP("help")),
			InputPortConfig<EntityId>("ItemId", 0, _HELP("help")),
			InputPortConfig<string>("ItemName", _HELP("help")),
			InputPortConfig<string>("ItemClassName", _HELP("help")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			OutputPortConfig<EntityId>("ItemId", _HELP("help")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("-0-");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (!pActInfo->pEntity)
				return;

			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Equip))
			{
				if (GetPortEntityId(pActInfo, EIP_ItemId) > 0)
				{
					CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetPortEntityId(pActInfo, EIP_ItemId)));
					if (pItem && pItem->GetEquipped() == 0 && pItem->GetOwnerId() == pActor->GetEntityId())
					{
						pActor->EquipItem(pItem->GetEntityId());
						ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
					}
				}
				else if (!GetPortString(pActInfo, EIP_ItemName).empty())
				{
					IInventory *pInventory = pActor->GetInventory();
					if (pInventory)
					{
						IItem *iitem = pInventory->GetItemByName(GetPortString(pActInfo, EIP_ItemName).c_str());
						if (iitem)
						{
							CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(iitem->GetEntityId()));
							if (pItem && pItem->GetEquipped() == 0 && pItem->GetOwnerId() == pActor->GetEntityId())
							{
								pActor->EquipItem(pItem->GetEntityId());
								ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
							}
						}
					}
				}
				else if (!GetPortString(pActInfo, EIP_ItemClassName).empty())
				{
					IInventory *pInventory = pActor->GetInventory();
					if (pInventory)
					{
						IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(GetPortString(pActInfo, EIP_ItemClassName).c_str());
						if (pClass)
						{
							EntityId item_id = pInventory->GetItemByClass(pClass);
							if (item_id > 0)
							{
								CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_id));
								if (pItem && pItem->GetEquipped() == 0 && pItem->GetOwnerId() == pActor->GetEntityId())
								{
									pActor->EquipItem(pItem->GetEntityId());
									ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, EOP_Done, 1);
				return;
			}

			if (IsPortActive(pActInfo, EIP_UnEquip))
			{
				if (GetPortEntityId(pActInfo, EIP_ItemId) > 0)
				{
					CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetPortEntityId(pActInfo, EIP_ItemId)));
					if (pItem && pItem->GetEquipped() == 1 && pItem->GetOwnerId() == pActor->GetEntityId())
					{
						pActor->EquipItem(pItem->GetEntityId());
						ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
					}
				}
				else if (!GetPortString(pActInfo, EIP_ItemName).empty())
				{
					IInventory *pInventory = pActor->GetInventory();
					if (pInventory)
					{
						IItem *iitem = pInventory->GetItemByName(GetPortString(pActInfo, EIP_ItemName).c_str());
						if (iitem)
						{
							CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(iitem->GetEntityId()));
							if (pItem && pItem->GetEquipped() == 1 && pItem->GetOwnerId() == pActor->GetEntityId())
							{
								pActor->EquipItem(pItem->GetEntityId());
								ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
							}
						}
					}
				}
				else if (!GetPortString(pActInfo, EIP_ItemClassName).empty())
				{
					IInventory *pInventory = pActor->GetInventory();
					if (pInventory)
					{
						IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(GetPortString(pActInfo, EIP_ItemClassName).c_str());
						if (pClass)
						{
							EntityId item_id = pInventory->GetItemByClass(pClass);
							if (item_id > 0)
							{
								CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_id));
								if (pItem && pItem->GetEquipped() == 1 && pItem->GetOwnerId() == pActor->GetEntityId())
								{
									pActor->EquipItem(pItem->GetEntityId());
									ActivateOutput(pActInfo, EOP_ItemId, pItem->GetEntityId());
								}
							}
						}
					}
				}
				ActivateOutput(pActInfo, EOP_Done, 1);
			}
		}
		}
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

class CFlowInventoryAddItemExtended : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowInventoryAddItemExtended(SActivationInfo* pActInfo)
	{
	}

	void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("add",         _HELP("Connect event here to add the item")),
			InputPortConfig<string>("item",     _HELP("The item to add to the inventory"),   _HELP("Item"),                                                                                _UICONFIG("enum_global_ref:item%s:ItemType")),
			InputPortConfig<string>("ItemType", "",                                          _HELP("Select from which items to choose"),                                                   0,                                            _UICONFIG("enum_string:All=,Givable=_givable,Selectable=_selectable")),
			InputPortConfig<bool>("Unique",     true,                                        _HELP("If true, only adds the item if the inventory doesn't already contain such an item")),
			{ 0 }
		};

		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<bool>("out", _HELP("true if the item was actually added, false if the player already had the item")),
			{ 0 }
		};

		config.sDescription = _HELP("Add an item to inventory.");
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = in_ports;
		config.pOutputPorts = out_ports;
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent(EFlowEvent event, SActivationInfo* pActInfo)
	{
		if (event == eFE_Activate && IsPortActive(pActInfo, 0))
		{
			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			IInventory* pInventory = pActor->GetInventory();
			if (!pInventory)
				return;

			const string& itemClass = GetPortString(pActInfo, 1);
			const char* pItemClass = itemClass.c_str();
			IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(pItemClass);
			EntityId id = pInventory->GetItemByClass(pClass);

			if (id == 0 || GetPortBool(pActInfo, 3) == false)
			{
				if (gEnv->bServer)
				{
					gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GiveItem(pActor, pItemClass, false, false, false);
				}
				else
				{
					pInventory->RMIReqToServer_AddItem(pItemClass);
				}
				ActivateOutput(pActInfo, 0, true);
			}
			else
			{
				ActivateOutput(pActInfo, 0, false);
			}
		}
		else if (event == eFE_PrecacheResources)
		{
			const string& itemClass = GetPortString(pActInfo, 1);

			if (!itemClass.empty())
			{
				IGameRules* pGameRules = gEnv->pGame->GetIGameFramework()->GetIGameRulesSystem()->GetCurrentGameRules();
				CRY_ASSERT_MESSAGE(pGameRules != NULL, "No game rules active, can not precache resources");
				if (pGameRules)
				{
					pGameRules->PrecacheLevelResource(itemClass.c_str(), eGameResourceType_Item);
				}
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer* s) const
	{
		s->Add(*this);
	}
};

class CFlowInventoryRemoveItemExtended : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowInventoryRemoveItemExtended(SActivationInfo* pActInfo)
	{
	}

	enum EInputs
	{
		eIn_Trigger,
		eIn_Item,
		eIn_ItemType,
	};

	enum EOutputs
	{
		eOut_Success,
	};

	void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("Remove",      _HELP("Connect event here to remove the item")),
			InputPortConfig<string>("item",     _HELP("The item to remove from the inventory"), _HELP("Item"),                               _UICONFIG("enum_global_ref:item%s:ItemType")),
			InputPortConfig<string>("ItemType", "",                                             _HELP("Select from which items to choose"),  0,                                            _UICONFIG("enum_string:All=,Givable=_givable,Selectable=_selectable")),
			{ 0 }
		};

		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<bool>("out", _HELP("true if the item was actually removed, false if the player did not have the item")),
			{ 0 }
		};

		config.sDescription = _HELP("Remove an item from inventory.");
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = in_ports;
		config.pOutputPorts = out_ports;
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent(EFlowEvent event, SActivationInfo* pActInfo)
	{
		if (event == eFE_Activate && IsPortActive(pActInfo, eIn_Trigger))
		{
			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			IInventory* pInventory = pActor->GetInventory();
			if (!pInventory)
			{
				return;
			}

			const string& itemClass = GetPortString(pActInfo, eIn_Item);
			IEntityClass* pClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(itemClass.c_str());
			EntityId id = pInventory->GetItemByClass(pClass);

			if (id != 0)
			{
				pInventory->RMIReqToServer_RemoveItem(itemClass.c_str());
				ActivateOutput(pActInfo, eOut_Success, true);
			}
			else
			{
				ActivateOutput(pActInfo, eOut_Success, false);
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer* s) const
	{
		s->Add(*this);
	}
};

class CFlowInventoryRemoveAllItemsExtended : public CFlowBaseNode<eNCT_Singleton>
{
public:
	CFlowInventoryRemoveAllItemsExtended(SActivationInfo* pActInfo)
	{
	}

	void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig in_ports[] =
		{
			InputPortConfig_Void("Activate", _HELP("Connect event here to remove all items from inventory")),
			{ 0 }
		};

		static const SOutputPortConfig out_ports[] =
		{
			OutputPortConfig<bool>("Done", _HELP("True when done successfully")),
			{ 0 }
		};

		config.sDescription = _HELP("When activated, removes all items from inventory.");
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = in_ports;
		config.pOutputPorts = out_ports;
		config.SetCategory(EFLN_APPROVED);
	}

	void ProcessEvent(EFlowEvent event, SActivationInfo* pActInfo)
	{
		if (event == eFE_Activate && IsPortActive(pActInfo, 0))
		{
			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pActInfo->pEntity->GetId()));
			if (!pActor)
				return;

			IInventory* pInventory = pActor->GetInventory();
			if (!pInventory)
				return;

			pInventory->RMIReqToServer_RemoveAllItems();
			ActivateOutput(pActInfo, 0, true);
		}
	}

	virtual void GetMemoryUsage(ICrySizer* s) const
	{
		s->Add(*this);
	}
};

REGISTER_FLOW_NODE("Inventory:AddNewAdditiveEquipmentPack", CFlowAddNewAdditiveEquipmentPack);
REGISTER_FLOW_NODE("Inventory:ClearCharacterInventoryCLR", CFlowClearCharacterInventoryCLR);
REGISTER_FLOW_NODE("Inventory:EquipItem", CFlowEquipItem);
REGISTER_FLOW_NODE("Inventory:AddItemExtended", CFlowInventoryAddItemExtended);
REGISTER_FLOW_NODE("Inventory:RemoveItemExtended", CFlowInventoryRemoveItemExtended);
REGISTER_FLOW_NODE("Inventory:RemoveAllItemsExtended", CFlowInventoryRemoveAllItemsExtended);