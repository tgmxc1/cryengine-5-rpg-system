// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "Player.h"
#include "Actor.h"
#include "Nodes/G2FlowBaseNode.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "ActorActionsNew.h"
#include "GameXMLSettingAndGlobals.h"
#include "Weapon.h"
#include "Projectile.h"
#include "WeaponSystem.h"
#include "Item.h"
#include "SpellProjectile.h"

#include <CryString/StringUtils.h>

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowStartSpecialMeleeCombatAttackNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStartSpecialMeleeCombatAttackNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowStartSpecialMeleeCombatAttackNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStartSpecialMeleeCombatAttackNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Attack_Damage_Add,
		EIP_Attack_AnimAction,
		EIP_Attack_Time,
		EIP_Attack_StartDelay
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<int>("AdditiveDamage", 0, _HELP("damage to be apply on main weapon damage formula")),
			InputPortConfig<string>("AnimationActionName", "", _HELP("attack animation action def")),
			InputPortConfig<float>("TimeToEndAction", 0.0f, _HELP("time to end attack action")),
			InputPortConfig<float>("StartDelay", 0.0f, _HELP("simle delay to start attack action, no sync with animation")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on attack action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat special complex melee action node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							 if (!pGameGlobals)
								 return;

							 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							 if (pActor == 0)
								 return;

							 CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
							 if (!nwAction)
								 return;

							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }
								 nwAction->StartWeaponMeleeAttackComplex(pActor->GetEntityId(), GetPortFloat(pActInfo, EIP_Attack_Time), 
									 GetPortFloat(pActInfo, EIP_Attack_StartDelay), GetPortInt(pActInfo, EIP_Attack_Damage_Add), GetPortString(pActInfo, EIP_Attack_AnimAction));
								 atk_timer = GetPortFloat(pActInfo, EIP_Attack_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowStartNormalMeleeAttackNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStartNormalMeleeAttackNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowStartNormalMeleeAttackNode()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStartNormalMeleeAttackNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Attack_Time,
		EIP_Attack_FromBlock
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<float>("TimeToEndAction", 0.0f, _HELP("time to end attack action")),
			InputPortConfig<int>("BlcActionSupport", 0, _HELP("can simple attack while in active blocking")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on attack action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat normal melee attack node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
							   m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							 if (!pGameGlobals)
								 return;

							 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							 if (pActor == 0)
								 return;

							 CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
							 if (!nwAction)
								 return;

							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }

								 if (pActor->m_blockactiveshield || pActor->m_blockactivenoshield)
								 {
									 nwAction->StartMeleeWeaponAttack(pActor->GetEntityId(), GetPortInt(pActInfo, EIP_Attack_FromBlock));
								 } 
								 else
									 nwAction->StartMeleeWeaponAttack(pActor->GetEntityId());
								 
								 atk_timer = GetPortFloat(pActInfo, EIP_Attack_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowStartSimpleMeleeAttackNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStartSimpleMeleeAttackNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
	}

	~CFlowStartSimpleMeleeAttackNode()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStartNormalMeleeAttackNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Attack_Time,
		EIP_Attack_FromBlock,
		EIP_Attack_Anim_Action,
		EIP_Attack_Pwr,
		EIP_Attack_Delay,
		EIP_Attack_Rec
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<float>("TimeToEndAction", 0.0f, _HELP("time to end attack action")),
			InputPortConfig<int>("BlcActionSupport", 0, _HELP("can simple attack while in active blocking")),
			InputPortConfig<string>("ActionAnim", "", _HELP("anim action")),
			InputPortConfig<float>("AttackPower", 0.0f, _HELP("power of simple attack")),
			InputPortConfig<float>("AttackDelay", 0.0f, _HELP("delay of simple attack")),
			InputPortConfig<float>("AttackRecoil", 0.0f, _HELP("recoil from simple attack")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on attack action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat simple melee attack node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							 if (!pGameGlobals)
								 return;

							 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							 if (pActor == 0)
								 return;

							 CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
							 if (!nwAction)
								 return;

							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }

								 if (pActor->m_blockactiveshield || pActor->m_blockactivenoshield)
								 {
									 if (GetPortInt(pActInfo, EIP_Attack_FromBlock) > 0)
									 {
										 nwAction->StartFistsMeleeAction(pActor->GetEntityId(), pActor->GetCurrentItemId(), GetPortString(pActInfo, EIP_Attack_Anim_Action).c_str(),
											 GetPortFloat(pActInfo, EIP_Attack_Delay), true, GetPortFloat(pActInfo, EIP_Attack_Pwr), GetPortFloat(pActInfo, EIP_Attack_Rec));
									 }
								 }
								 else
								 {
									 nwAction->StartFistsMeleeAction(pActor->GetEntityId(), pActor->GetCurrentItemId(), GetPortString(pActInfo, EIP_Attack_Anim_Action).c_str(),
										 GetPortFloat(pActInfo, EIP_Attack_Delay), true, GetPortFloat(pActInfo, EIP_Attack_Pwr), GetPortFloat(pActInfo, EIP_Attack_Rec));
								 }

								 atk_timer = GetPortFloat(pActInfo, EIP_Attack_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowStartNormalRangeAttackNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStartNormalRangeAttackNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowStartNormalRangeAttackNode()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStartNormalRangeAttackNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Attack_Time,
		EIP_Attack_Continuosly,
		EIP_Attack_Type,
		EIP_Attack_Optional_target,
		EIP_Attack_Stop_After_Timeout
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<float>("TimeToEndAction", 0.0f, _HELP("time to end attack action")),
			InputPortConfig<bool>("IsContinuoslyAttack", false, _HELP("call attack countinuosly while TimeToEndAction not 0")),
			InputPortConfig<int>("RangeAttackType", 0, _HELP("type of range attack, 0 is normal, 1 is magick attack simple request")),
			InputPortConfig<Vec3>("OptionalTarget", Vec3(ZERO), _HELP("optional aimtarget for range attack, override any other targets")),
			InputPortConfig<bool>("StopAttackAtTimeout", false, _HELP("stop attack automaticali if param TimeToEndAction is 0")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on attack action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat normal range attack node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							 if (!pGameGlobals)
								 return;

							 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							 if (pActor == 0)
								 return;

							 CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
							 if (!nwAction)
								 return;

							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }

								 int type = GetPortInt(pActInfo, EIP_Attack_Type);
								 if (type == 0)
								 {
									 nwAction->StartWeaponAttackSimple(pActor->GetEntityId());
								 }
								 else if(type == 1)
								 {
									 nwAction->StartSpellCastSimple(pActor->GetEntityId());
								 }
								 atk_timer = GetPortFloat(pActInfo, EIP_Attack_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   if (GetPortBool(pActInfo, EIP_Attack_Continuosly))
							   {
								   CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
								   if (pGameGlobals)
								   {
									   CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
									   if (pActor != 0)
									   {
										   CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
										   if (nwAction)
										   {
											   int type = GetPortInt(pActInfo, EIP_Attack_Type);
											   if (type == 0)
											   {
												   nwAction->StartWeaponAttackSimple(pActor->GetEntityId());
											   }
											   else if (type == 1)
											   {
												   nwAction->StartSpellCastSimple(pActor->GetEntityId());
											   }
										   }
									   }
								   }
							   }
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   if (GetPortBool(pActInfo, EIP_Attack_Stop_After_Timeout))
								   {
									   CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
									   if (pGameGlobals)
									   {
										   CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
										   if (pActor != 0)
										   {
											   CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
											   if (nwAction)
											   {
												   int type = GetPortInt(pActInfo, EIP_Attack_Type);
												   if (type == 0)
												   {
													   nwAction->StopWeaponAttackSimple(pActor->GetEntityId());
												   }
												   else if (type == 1)
												   {
													   nwAction->StopSpellCastSimple(pActor->GetEntityId());
												   }
											   }
										   }
									   }
								   }
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowStopNormalRangeAttackNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStopNormalRangeAttackNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowStopNormalRangeAttackNode()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStopNormalRangeAttackNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Attack_Time,
		EIP_Attack_Type
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<float>("TimeToEndAction", 0.0f, _HELP("time to end attack action")),
			InputPortConfig<int>("RangeAttackType", 0, _HELP("type of range attack to stop, 0 is normal, 1 is magick attack simple stop request")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on attack stop action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat normal stop range attack node. Its need for same range weapon types(bows, magicka, ets...)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }
								 atk_timer = GetPortFloat(pActInfo, EIP_Attack_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
								   if (pGameGlobals)
								   {
									   CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
									   if (pActor != 0)
									   {
										   CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
										   if (nwAction)
										   {
											   int type = GetPortInt(pActInfo, EIP_Attack_Type);
											   if (type == 0)
											   {
												   nwAction->StopWeaponAttackSimple(pActor->GetEntityId());
											   }
											   else if (type == 1)
											   {
												   nwAction->StopSpellCastSimple(pActor->GetEntityId());
											   }
										   }
									   }
								   }
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowStartBlockingAttacks : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowStartBlockingAttacks(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowStartBlockingAttacks()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowStartBlockingAttacks(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Blocking_Time,
		EIP_StopBlock_atTimeOut
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<float>("TimeToEndBlocking", 0.0f, _HELP("blocking time")),
			InputPortConfig<bool>("DisableAtTimeout", false, _HELP("if TimeToEndBlocking param is 0, block be disabled")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on block action done")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Combat start attack blocking");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }
								 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
								 if (pGameGlobals)
								 {
									 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
									 if (pActor != 0)
									 {
										 CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
										 if (nwAction && atk_timer > 0.0f)
										 {
											 nwAction->StartMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());
										 }
									 }
								 }
								 atk_timer = GetPortFloat(pActInfo, EIP_Blocking_Time);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (atk_timer <= 0.0f)
							   {
								   CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
								   if (pGameGlobals)
								   {
									   CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
									   if (pActor != 0)
									   {
										   CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
										   if (nwAction)
										   {
											   if (GetPortBool(pActInfo, EIP_StopBlock_atTimeOut))
											   {
												   nwAction->StopMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());
											   }
										   }
									   }
								   }
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
};

class CFlowProjectileSpawnerNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowProjectileSpawnerNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
		atk_timer = 0.0f;
		atk_old_time = 0.0f;
		/*if (pActInfo && pActInfo->pGraph)
		{
			pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
		}*/
	}

	~CFlowProjectileSpawnerNode()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowProjectileSpawnerNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Launch_Start_Pos,
		EIP_Launch_End_Pos,
		EIP_Launch_Delay,
		EIP_Launch_Timer,
		EIP_Launch_Continuosly,
		EIP_Projectile_ammo_name,
		EIP_Launch_Veloc_mult,
		EIP_Projectile_Damage_Additive,
		EIP_Projectile_Additive_Effect1,
		EIP_Projectile_Additive_Effect2,
		EIP_Projectile_Additive_Effect3,
		EIP_Projectile_Additive_Effect4,
		EIP_Projectile_Additive_Effect5,
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<Vec3>("StartPos", Vec3(ZERO), _HELP("help me!!!")),
			InputPortConfig<Vec3>("EndPos", Vec3(ZERO), _HELP("help me!!!")),
			InputPortConfig<float>("LaunchDelay", 0.0f, _HELP("help me!!!")),
			InputPortConfig<float>("LaunchTimer", 0.0f, _HELP("help me!!!")),
			InputPortConfig<bool>("Continuosly", false, _HELP("Spawn&Launch projectile continuosly while LaunchTimer value > 0")),
			InputPortConfig<string>("NameOfProjectileAmmo", _HELP("NameOfProjectileAmmo")),
			InputPortConfig<float>("LaunchForce", 1.0f, _HELP("help me!!!")),
			InputPortConfig<float>("Damage", 0.0f, _HELP("help me!!!")),
			InputPortConfig<int>("AdditiveEffect", -1, _HELP("SpellProjectile only special additive effect")),
			InputPortConfig<int>("AdditiveEffect", -1, _HELP("SpellProjectile only special additive effect")),
			InputPortConfig<int>("AdditiveEffect", -1, _HELP("SpellProjectile only special additive effect")),
			InputPortConfig<int>("AdditiveEffect", -1, _HELP("SpellProjectile only special additive effect")),
			InputPortConfig<int>("AdditiveEffect", -1, _HELP("SpellProjectile only special additive effect")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("end of this action")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Spawn&Launch projectile");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
		}
			break;
		case eFE_SetEntityId:
		{
								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
			break;

		case eFE_Activate:
		{
							
							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 if (pActInfo && pActInfo->pGraph)
								 {
									 pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, true);
								 }
								 m_actInfo = *pActInfo;
								 atk_timer = GetPortFloat(pActInfo, EIP_Launch_Timer);
								 if (atk_timer <= 0.0f)
								 {
									 atk_timer = 0.003f;
								 }
								 atk_old_time = atk_timer;
							 }
		}
			break;

		case eFE_Update:
		{
						   if (atk_timer > 0.0f)
						   {
							   atk_timer -= gEnv->pTimer->GetFrameTime();
							   if (GetPortBool(pActInfo, EIP_Launch_Continuosly))
							   {
								   if ((atk_timer + GetPortFloat(pActInfo, EIP_Launch_Delay)) < atk_old_time)
								   {
									   atk_old_time = atk_timer;
									   SpawnPrjAndLaunchIt(pActInfo);
								   }
							   }

							   if (atk_timer <= 0.0f)
							   {
								   SpawnPrjAndLaunchIt(pActInfo);
								   pActInfo->pGraph->SetRegularlyUpdated(pActInfo->myID, false);
								   atk_timer = 0.0f;
								   ActivateOutput(&m_actInfo, EOP_Done, 0);
							   }
						   }
		}
			break;
		}
	}

	virtual void SpawnPrjAndLaunchIt(SActivationInfo *pActInfo)
	{
		//CryLogAlways("SpawnPrjAndLaunchIt called");
		string prj_name = GetPortString(pActInfo, EIP_Projectile_ammo_name);
		if (prj_name.empty())
			return;

		//CryLogAlways("SpawnPrjAndLaunchIt c2");
		CProjectile *pProjectile = g_pGame->GetWeaponSystem()->SpawnAmmo(gEnv->pEntitySystem->GetClassRegistry()->FindClass(prj_name.c_str()), false);
		if (pProjectile)
		{
			CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pProjectile);
			if (pSpellPhy)
			{
				pSpellPhy->additive_effects_on_hit[0] = GetPortInt(pActInfo, EIP_Projectile_Additive_Effect1);
				pSpellPhy->additive_effects_on_hit[1] = GetPortInt(pActInfo, EIP_Projectile_Additive_Effect2);
				pSpellPhy->additive_effects_on_hit[2] = GetPortInt(pActInfo, EIP_Projectile_Additive_Effect3);
				pSpellPhy->additive_effects_on_hit[3] = GetPortInt(pActInfo, EIP_Projectile_Additive_Effect4);
				pSpellPhy->additive_effects_on_hit[4] = GetPortInt(pActInfo, EIP_Projectile_Additive_Effect5);
				CSpellProjectile::SProjectileDesc projectileDesc(m_entityId, 0, 0, pProjectile->GetDamage() + GetPortFloat(pActInfo, EIP_Projectile_Damage_Additive), 0, 0, 0, pProjectile->GetHitType(), 0, false);
				pSpellPhy->SetParams(projectileDesc);
				Vec3 direction = GetPortVec3(pActInfo, EIP_Launch_End_Pos) - GetPortVec3(pActInfo, EIP_Launch_Start_Pos);
				direction.normalize();
				Vec3 position = GetPortVec3(pActInfo, EIP_Launch_Start_Pos);
				Vec3 veloc = direction * 1.5f;
				pSpellPhy->Launch(position, direction, veloc, GetPortFloat(pActInfo, EIP_Launch_Veloc_mult));
			}
			else
			{
				//CryLogAlways("SpawnPrjAndLaunchIt c3");
				CProjectile::SProjectileDesc projectileDesc(m_entityId, 0, 0, pProjectile->GetDamage() + GetPortFloat(pActInfo, EIP_Projectile_Damage_Additive), 0, 0, 0, pProjectile->GetHitType(), 0, false);
				pProjectile->SetParams(projectileDesc);
				Vec3 direction = GetPortVec3(pActInfo, EIP_Launch_End_Pos) - GetPortVec3(pActInfo, EIP_Launch_Start_Pos);
				direction.normalize();
				Vec3 position = GetPortVec3(pActInfo, EIP_Launch_Start_Pos);
				Vec3 veloc = direction * 1.5f;
				pProjectile->Launch(position, direction, veloc, GetPortFloat(pActInfo, EIP_Launch_Veloc_mult));
			}
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	float atk_timer;
	float atk_old_time;
};

class CFlowAddOrDelDORBEffect : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowAddOrDelDORBEffect(SActivationInfo * pActInfo)
	{
		activated = false;
	}

	~CFlowAddOrDelDORBEffect()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowAddOrDelDORBEffect(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Add = 0,
		EIP_Del,
		EIP_EffectId,
		EIP_EffectTime,
		EIP_EffectLC,
		EIP_ActorId,
		EIP_OneActivation,
		EIP_Deactive
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Add", _HELP("Add effect")),
			InputPortConfig_Void("Del", _HELP("Del effect")),
			InputPortConfig<int>("EffectId", 0, _HELP("id of effect to add")),
			InputPortConfig<float>("EffectTime", 0.0f, _HELP("start time of added effect")),
			InputPortConfig<int>("EffectLC", 0, _HELP("special param of effect")),
			InputPortConfig<EntityId>("ActorId", 0, _HELP("selected actor")),
			InputPortConfig<bool>("OneActivation", false, _HELP("---")),
			InputPortConfig_Void("ClearState", _HELP("Clear activation state")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on effect added")),
			{ 0 }
		};
		config.nFlags |= EFLN_ADVANCED;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Add positive or negative effect on selected character");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_Deactive))
			{
				activated = false;
				pActInfo->pInputPorts[EIP_Deactive].ClearUserFlag();
			}

			if (IsPortActive(pActInfo, EIP_Add) || IsPortActive(pActInfo, EIP_Del))
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals)
				{
					CActor* pActor = pGameGlobals->GetThisActor(GetPortEntityId(pActInfo, EIP_ActorId));
					if (pActor != 0)
					{
						if (!activated)
						{
							if (IsPortActive(pActInfo, EIP_Add))
								pActor->AddBuff(GetPortInt(pActInfo, EIP_EffectId), GetPortFloat(pActInfo, EIP_EffectTime), GetPortInt(pActInfo, EIP_EffectLC));
							else if (IsPortActive(pActInfo, EIP_Del) && !IsPortActive(pActInfo, EIP_Add))
								pActor->DelBuff(GetPortInt(pActInfo, EIP_EffectId));
						}
						else if (!GetPortBool(pActInfo, EIP_OneActivation))
						{
							if (IsPortActive(pActInfo, EIP_Add))
								pActor->AddBuff(GetPortInt(pActInfo, EIP_EffectId), GetPortFloat(pActInfo, EIP_EffectTime), GetPortInt(pActInfo, EIP_EffectLC));
							else if (IsPortActive(pActInfo, EIP_Del) && !IsPortActive(pActInfo, EIP_Add))
								pActor->DelBuff(GetPortInt(pActInfo, EIP_EffectId));
						}

						if (GetPortBool(pActInfo, EIP_OneActivation))
						{
							activated = true;
						}

						ActivateOutput(pActInfo, EOP_Done, 0);
					}
				}
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
	bool activated;
};

class CFlowAddOrDelPassivePerkEffect : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowAddOrDelPassivePerkEffect(SActivationInfo * pActInfo) : m_entityId(0)
	{

	}

	~CFlowAddOrDelPassivePerkEffect()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowAddOrDelPassivePerkEffect(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Add = 0,
		EIP_Del,
		EIP_PerkId,
		EIP_PerkLevel
	};

	enum EOutputPorts
	{
		EOP_Done,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Add", _HELP("Add effect")),
			InputPortConfig_Void("Del", _HELP("Del effect")),
			InputPortConfig<int>("PerkId", 0, _HELP("id of perk to add")),
			InputPortConfig<int>("PerkLevel", 0, _HELP("perk level")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on perk added")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Add passive perk on selected character");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
		}
		break;
		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			if (IsPortActive(pActInfo, EIP_Add))
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals)
				{
					CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
					if (pActor != 0)
					{
						pActor->ActivatePerk(GetPortInt(pActInfo, EIP_PerkId), true, GetPortInt(pActInfo, EIP_PerkLevel));
						ActivateOutput(&m_actInfo, EOP_Done, 0);
					}
				}
				return;
			}

			if (IsPortActive(pActInfo, EIP_Del))
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals)
				{
					CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
					if (pActor != 0)
					{
						pActor->ActivatePerk(GetPortInt(pActInfo, EIP_PerkId), false, GetPortInt(pActInfo, EIP_PerkLevel));
						ActivateOutput(&m_actInfo, EOP_Done, 0);
					}
				}
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

class CFlowSetOrGetDamageResistParams : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowSetOrGetDamageResistParams(SActivationInfo * pActInfo) : m_entityId(0)
	{

	}

	~CFlowSetOrGetDamageResistParams()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowSetOrGetDamageResistParams(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Set = 0,
		EIP_Get,
		EIP_ResistToSetId,
		EIP_ResistToSetName,
		EIP_ResistToGetId,
		EIP_ResistToGetName,
		EIP_ResistToSetValue
	};

	enum EOutputPorts
	{
		EOP_Done,
		EOP_ResistVal
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Set", _HELP("Set actor damage resist value")),
			InputPortConfig_Void("Get", _HELP("Get actor damage resist value")),
			InputPortConfig<int>("ResistToSetId", 0, _HELP("id resist value to set new")),
			InputPortConfig<string>("ResistToSetName", _HELP("name resist value to set new")),
			InputPortConfig<int>("ResistToGetId", 0, _HELP("id resist to get this value")),
			InputPortConfig<string>("ResistToGetName", _HELP("name resist value to set new")),
			InputPortConfig<float>("ResistToSetValue", 0.0f, _HELP("value")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("ActionDone", _HELP("Triggered on effect added")),
			OutputPortConfig<float>("ResistVal", _HELP("Triggered on effect added")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Get/Set damage resists params to/from selected character");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
		}
		break;
		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			if (pActor == 0 || !pActor)
				return;

			if (IsPortActive(pActInfo, EIP_Set))
			{

				if (GetPortInt(pActInfo, EIP_ResistToSetId) > 0)
				{
					pActor->SetResistForDamageType(GetPortInt(pActInfo, EIP_ResistToSetId), GetPortFloat(pActInfo, EIP_ResistToSetValue));
				}
				else
				{
					if (!GetPortString(pActInfo, EIP_ResistToSetName).empty())
					{
						pActor->SetResistForDamageTypeByName(GetPortString(pActInfo, EIP_ResistToSetName).c_str(), GetPortFloat(pActInfo, EIP_ResistToSetValue));
					}
				}
				ActivateOutput(&m_actInfo, EOP_Done, 1);
			}

			if (IsPortActive(pActInfo, EIP_Get))
			{
				if (GetPortInt(pActInfo, EIP_ResistToGetId) > 0)
				{
					ActivateOutput(&m_actInfo, EOP_ResistVal, pActor->GetResistForDamageType(GetPortInt(pActInfo, EIP_ResistToGetId)));
				}
				else
				{
					if (!GetPortString(pActInfo, EIP_ResistToGetName).empty())
					{
						ActivateOutput(&m_actInfo, EOP_ResistVal, pActor->GetResistForDamageTypeByName(GetPortString(pActInfo, EIP_ResistToSetName).c_str()));
					}
				}
				ActivateOutput(&m_actInfo, EOP_Done, 1);
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

class CFlowGetActorAITargetInformation : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowGetActorAITargetInformation(SActivationInfo * pActInfo) : m_entityId(0)
	{

	}

	~CFlowGetActorAITargetInformation()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowGetActorAITargetInformation(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_GetStatus = 0,
		EIP_GetId = 0,
		
	};

	enum EOutputPorts
	{
		EOP_Status,
		EOP_TargetId
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("GetStatus", _HELP("Get actor ai target visual status (0 - no visible target, 1 - visible enemy actor target, 2 - visible frendly actor target 3 - etc...)")),
			InputPortConfig_Void("GetId", _HELP("Get actor ai target id")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<int>("Status", _HELP("n")),
			OutputPortConfig<EntityId>("TargetId", _HELP("n")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Get/Set damage resists params to/from selected character");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
			m_actInfo = *pActInfo;
		}
		break;
		case eFE_SetEntityId:
		{
			m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
		}
		break;

		case eFE_Activate:
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			if (pActor == 0 || !pActor)
				return;

			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
				return;

			if (IsPortActive(pActInfo, EIP_GetStatus))
			{
				ActivateOutput(&m_actInfo, EOP_Status, nwAction->IsAITargetHaveAndVisibleOrObstr(pActor->GetEntityId()));
			}
			if (IsPortActive(pActInfo, EIP_GetId))
			{
				ActivateOutput(&m_actInfo, EOP_TargetId, nwAction->GetAITargetFT(pActor->GetEntityId()));
			}
		}
		break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

REGISTER_FLOW_NODE("CombatSystem:StartSpecialMeleeCombatAttack", CFlowStartSpecialMeleeCombatAttackNode);
REGISTER_FLOW_NODE("CombatSystem:StartNormalMeleeAttack", CFlowStartNormalMeleeAttackNode);
REGISTER_FLOW_NODE("CombatSystem:StartSimpleMeleeAttack", CFlowStartSimpleMeleeAttackNode);
REGISTER_FLOW_NODE("CombatSystem:StartNormalRangeAttack", CFlowStartNormalRangeAttackNode);
REGISTER_FLOW_NODE("CombatSystem:StopNormalRangeAttack", CFlowStopNormalRangeAttackNode);
REGISTER_FLOW_NODE("CombatSystem:StartBlockingAttacks", CFlowStartBlockingAttacks);
REGISTER_FLOW_NODE("CombatSystem:ProjectileSpawner", CFlowProjectileSpawnerNode);
REGISTER_FLOW_NODE("CombatSystem:AddOrDelDORBEffect", CFlowAddOrDelDORBEffect);
REGISTER_FLOW_NODE("CombatSystem:AddOrDelPassivePerkEffect", CFlowAddOrDelPassivePerkEffect);
REGISTER_FLOW_NODE("CombatSystem:SetOrGetDamageResistParams", CFlowSetOrGetDamageResistParams);
REGISTER_FLOW_NODE("CombatSystem:GetActorAITargetInformation", CFlowGetActorAITargetInformation);
