// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#include "StdAfx.h"
#include "Game.h"
#include "GameCVars.h"
#include "Nodes/G2FlowBaseNode.h"
#include "GameXMLSettingAndGlobals.h"
#include "Actor.h"
#include "Player.h"

#pragma warning(push)
#pragma warning(disable : 4244)

class CFlowActorNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorNode()
	{

	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_DialogStage,
	};

	enum EOutputPorts
	{
		EOP_DialogStage,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<int>  ("DialogStage", 0, _HELP("Trigger to set dialog stage")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<int>  ("DialogStage", _HELP("Triggered on current dialog stage")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Dialog Stage Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					StageChanged(pActor->GetDialogStage());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pActor->SetDialogStage(GetPortInt(pActInfo, EIP_DialogStage));
					StageChanged(pActor->GetDialogStage());
				}
			}
			break;
		}
	}

	void StageChanged(int stage)
	{
		ActivateOutput(&m_actInfo, EOP_DialogStage, stage);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};


class CFlowActorStaminaNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorStaminaNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorStaminaNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorStaminaNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Stamina,
	};

	enum EOutputPorts
	{
		EOP_Stamina,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<float>  ("Stamina", 0, _HELP("Trigger to set Stamina")),

			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<float>  ("Stamina", _HELP("Triggered on current Stamina")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Stamina Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					StaminaChanged((int)pActor->GetStamina());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					if (!pActor->IsRemote())
						pActor->SetStamina(GetPortFloat(pActInfo, EIP_Stamina));
				}
			}
			break;
		}
	}

	void StaminaChanged(float stamina)
	{
		ActivateOutput(&m_actInfo, EOP_Stamina, stamina);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};

class CFlowActorTeleportTo : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorTeleportTo(SActivationInfo * pActInfo) : m_entityId(0)
	{
	}

	~CFlowActorTeleportTo()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorTeleportTo(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Pos,
	};

	enum EOutputPorts
	{
		EOP_Pos,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<Vec3>  ("Position", Vec3(ZERO), _HELP("Trigger to set Position")),

			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<Vec3>("Position", _HELP("Triggered on current Position")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Position tp Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					PosChanged(pActor->GetEntity()->GetPos());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
				
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					pActor->GetEntity()->SetPos(GetPortVec3(pActInfo, EIP_Pos));
				}
			}
			break;
		}
	}

	void PosChanged(Vec3 pos)
	{
		ActivateOutput(&m_actInfo, EOP_Pos, pos);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};

class CFlowActorStrNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorStrNode(SActivationInfo * pActInfo) : m_entityId(0)
	{
	}

	~CFlowActorStrNode()
	{

	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowActorStrNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Str,
	};

	enum EOutputPorts
	{
		EOP_Str,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Trigger", _HELP("Trigger")),
			InputPortConfig<int>("Str", 0, _HELP("Trigger to set Str")),

			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<int>("Str", _HELP("Triggered on current Str")),
			{ 0 }
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Str Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Initialize:
		{
							   m_actInfo = *pActInfo;
							   CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							   if (!pGameGlobals)
								   return;

							   CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							   if (pActor)
							   {
								   StrChanged(pActor->GetStrength());
							   }
		}
			break;
		case eFE_SetEntityId:
		{

								m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;

		}
			break;

		case eFE_Activate:
		{
							 CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
							 if (!pGameGlobals)
								 return;

							 CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
							 if (pActor == 0)
								 return;

							 if (IsPortActive(pActInfo, EIP_Trigger))
							 {
								 pActor->SetStrength(GetPortInt(pActInfo, EIP_Str));
							 }
		}
			break;
		}
	}

	void StrChanged(int str)
	{
		ActivateOutput(&m_actInfo, EOP_Str, str);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;

};

class CFlowActorMagickaNode : public CFlowBaseNode<eNCT_Instanced>
{
private:
	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Magicka,
	};

	enum EOutputPorts
	{
		EOP_Magicka,
	};
public:
	CFlowActorMagickaNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorMagickaNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorMagickaNode(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger")),
			InputPortConfig<float>  ("Magicka", 0, _HELP("Trigger to set Magicka")),

			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<float>  ("Magicka", _HELP("Triggered on current Magicka")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Magicka Node");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					//CryLogAlways("flow Module Actor Magicka node is Initialize");
					MagickaChanged(pActor->GetMagicka());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				//CryLogAlways("flow Module Actor Magicka node is SetEntityId");
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
				
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					//CryLogAlways("flow Module Actor Magicka node is Activate");
					pActor->SetMagicka(GetPortFloat(pActInfo, EIP_Magicka));
				}
			}
			break;
		}
	}

	void MagickaChanged(float magicka)
	{
		//CryLogAlways("flow Module Actor Magicka node Output active");
		ActivateOutput(&m_actInfo, EOP_Magicka, magicka);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};


class CFlowActorGetDialogStageNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorGetDialogStageNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorGetDialogStageNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorGetDialogStageNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
	};

	enum EOutputPorts
	{
		EOP_DialogStage,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger to get Dialog Stage")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<int>  ("Dialog Stage", _HELP("Triggered on current Dialog Stage")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor get Dialog Stage");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					DialogStage(pActor->GetDialogStage());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
				
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					DialogStage(pActor->GetDialogStage());
				}
			}
			break;
		}
	}

	void DialogStage(int st)
	{
		ActivateOutput(&m_actInfo, EOP_DialogStage, st);
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};

class CFlowActorDialogStageNvalNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorDialogStageNvalNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorDialogStageNvalNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorDialogStageNvalNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
		EIP_Value,
	};

	enum EOutputPorts
	{
		EOP_DialogStageValid,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger to get Dialog Stage and compare with value")),
			InputPortConfig<int>  ("Value", 0, _HELP("Trigger to set Value,")),

			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("Dialog Stage Valid", _HELP("Triggered on current Dialog Stage valid")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor get Dialog Stage and compare with custom value, can return true or false");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
			}
			break;
		case eFE_SetEntityId:
			{
				
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
				
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					if(pActor->GetDialogStage() == GetPortInt(pActInfo, EIP_Value))
					{
						ActivateOutput(&m_actInfo, EOP_DialogStageValid, true);
					}
					else
					{
						ActivateOutput(&m_actInfo, EOP_DialogStageValid, false);
					}
				}
			}
			break;
		}
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
	
};


class CFlowActorGetActorGenderNode : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowActorGetActorGenderNode( SActivationInfo * pActInfo ) : m_entityId (0)
	{
	}

	~CFlowActorGetActorGenderNode()
	{
		
	}

	IFlowNodePtr Clone( SActivationInfo * pActInfo )
	{
		return new CFlowActorGetActorGenderNode(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Trigger = 0,
	};

	enum EOutputPorts
	{
		EOP_ActorGenderM,
		EOP_ActorGenderF,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void  ("Trigger", _HELP("Trigger to get Actor Gender")),
			{0}
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig<bool>  ("GenderM", _HELP("Triggered on current Actor GenderM")),
			OutputPortConfig<bool>  ("GenderF", _HELP("Triggered on current Actor GenderF")),
			{0}
		};
		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Actor Get Gender( 0-male, 1-female )");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent( EFlowEvent event, SActivationInfo *pActInfo )
	{
		switch (event)
		{
		case eFE_Initialize:
			{
				m_actInfo = *pActInfo;
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor)
				{
					Gender(pActor->GetGender());
				}
			}
			break;
		case eFE_SetEntityId:
			{
				
				m_entityId = pActInfo->pEntity ? pActInfo->pEntity->GetId() : 0;
				
			}
			break;

		case eFE_Activate:
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (!pGameGlobals)
					return;

				CActor* pActor = pGameGlobals->GetThisActor(m_entityId);
				if (pActor == 0)
					return;

				if (IsPortActive(pActInfo, EIP_Trigger))
				{
					Gender(pActor->GetGender());
				}
			}
			break;
		}
	}

	void Gender(int gd)
	{
		if(gd==0)
			ActivateOutput(&m_actInfo, EOP_ActorGenderM, true);
		else if(gd==1)
			ActivateOutput(&m_actInfo, EOP_ActorGenderF, true);
		else if(gd>1||gd<0)
			return;
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}

	SActivationInfo m_actInfo;
	EntityId        m_entityId;
};

class CFlowPlayerSaveToLTL : public CFlowBaseNode<eNCT_Instanced>
{
public:
	CFlowPlayerSaveToLTL(SActivationInfo * pActInfo)
	{
	}

	~CFlowPlayerSaveToLTL()
	{
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowPlayerSaveToLTL(pActInfo);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}

	enum EInputPorts
	{
		EIP_Save = 0,
	};

	enum EOutputPorts
	{
		EOP_Saved = 0,
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] = {
			InputPortConfig_Void("Save", _HELP("Save player for next level")),
			{ 0 }
		};
		static const SOutputPortConfig outputs[] = {
			OutputPortConfig_Void("Saved", _HELP("Save done")),
			{ 0 }
		};
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Save player for next level(and all player info)");
		config.SetCategory(EFLN_APPROVED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
			if (IsPortActive(pActInfo, EIP_Save))
			{
				Serializz(pActInfo);
				ActivateOutput(pActInfo, EOP_Saved, true);
			}
			break;
		}
	}

	void Serializz(SActivationInfo* pActivationInfo)
	{
		//g_pGame->GetLevelTolevelserialization()->SaveAllPlInfoForLTL();
		CryLogAlways("SaveQuests Complete");
	}

	virtual void GetMemoryStatistics(ICrySizer * s)
	{
		s->Add(*this);
	}
};

REGISTER_FLOW_NODE("Actor:ActorDialogStage", CFlowActorNode);
REGISTER_FLOW_NODE("Actor:ActorStamina", CFlowActorStaminaNode);
REGISTER_FLOW_NODE("Actor:ActorStr", CFlowActorStrNode);
REGISTER_FLOW_NODE("Actor:ActorMagicka", CFlowActorMagickaNode);
REGISTER_FLOW_NODE("Actor:ActorGetDialogStage", CFlowActorGetDialogStageNode);
REGISTER_FLOW_NODE("Actor:ActorGetGender", CFlowActorGetActorGenderNode);
REGISTER_FLOW_NODE("Actor:ActorDialogStageCompare", CFlowActorDialogStageNvalNode);
REGISTER_FLOW_NODE("Game:PlayerSaveToLTL", CFlowPlayerSaveToLTL);
REGISTER_FLOW_NODE("Actor:ActorTeleportTo", CFlowActorTeleportTo);

