#include "StdAfx.h"
#include <CryAnimation/ICryAnimation.h>
//#include "CryCharMorphParams.h"
#include "Bow.h"
#include "Game.h"
#include "GameCVars.h"
#include "Actor.h"
#include "Player.h"
#include "Throw.h"
#include "GameActions.h"
#include "Item.h"
#include "WeaponSystem.h"
#include <CryEntitySystem/IEntitySystem.h>
#include "ActorActionsNew.h"

CBow::CBow(void)
{
	shot = false;
	hold_started = false;
	m_arrow_hold_time = 0.0f;
	m_arrow_load_time = 0.0f;
	m_arrow_load_time_sequence_1 = 0.8f;
	m_arrow_load_time_sequence_2 = 2.0f;
	m_arrow_shot_recovery_time = 0.0f;
	m_arrow_hold_time_old = 0.0f;
	m_hold_time_min_to_shot = 0.9f;
	m_hold_time_max_to_vel = 3.0f;
	m_stamina_cons_time = 0.0f;
	anim_hold_started = false;
	arrow_loaded = false;
	process_arrow_load_started = false;
	arrow_attached_to_hand = false;
	arrow_attached_to_bow = false;
	m_close_combat_damage = 15.0f;
}

CBow::~CBow(void)
{
}

bool CBow::Init(IGameObject * pGameObject)
{
	bool init_cmpl = true;
	if (!CWeapon::Init(pGameObject))
		init_cmpl = false;

	if (!GetIWeapon())
		return true;

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		CThrow *fm = (CThrow *)pFmd;
		if(!fm)
			return true;

		pBowParams = fm->GetBowFireParams();
		if (pBowParams.ccDamageNrm > 0.0f)
		{
			m_close_combat_damage = pBowParams.ccDamageNrm;
		}
	}

	return init_cmpl;
}

void CBow::Update(SEntityUpdateContext &ctx, int slot)
{
	CActor *pActorOwner = this->GetOwnerActor();
	if (!pActorOwner)
		return;

	//CWeapon::Update(ctx, slot);
	if (m_arrow_shot_recovery_time >= 0)
	{
		m_arrow_shot_recovery_time -= ctx.fFrameTime;
	}


	if (process_arrow_load_started)
	{
		m_arrow_load_time += ctx.fFrameTime;
		if (m_arrow_load_time >= m_arrow_load_time_sequence_1)
		{
			if (!arrow_attached_to_hand)
			{
				AttachArrowToHand();
			}
		}

		if (m_arrow_load_time >= m_arrow_load_time_sequence_2)
		{
			if (arrow_attached_to_hand)
			{
				DetachArrowFromHand();
			}

			if (!arrow_attached_to_bow)
			{
				CreateArrowAttachment();
			}
			arrow_loaded = true;
			process_arrow_load_started = false;
			SetItemFlag(CItem::eIF_BlockActions, false);
			if (!pActorOwner->IsPlayer() && g_pGameCVars->g_bow_ai_actors_start_hold_after_loading_arrow > 0)
			{
				StartFire();
			}
		}
	}

	if (hold_started)
	{
		m_arrow_hold_time += ctx.fFrameTime;
		if (!anim_hold_started)
			StartHoldAnim();

		if (m_stamina_cons_time < (m_arrow_hold_time - g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina_speed)&& m_arrow_hold_time > 0.2f)
		{
			if (g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina > 0.0f)
			{
				//EnableUpdate(true, slot);
				m_stamina_cons_time = m_arrow_hold_time;
				if (pActorOwner->GetStamina() > g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina)
				{
					//CWeapon::Update(ctx, slot);
					if (!pActorOwner->IsRemote())
						pActorOwner->SetStamina(pActorOwner->GetStamina() - g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina);

					if (m_arrow_hold_time > 2.0f)
					{
						//this->GetOwnerActor()->SetStamina(this->GetOwnerActor()->GetStamina() - (g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina * m_arrow_hold_time));
					}
				}

				if (g_pGameCVars->g_stamina_sys_can_bow_draw_if_low_stmn_val == 0)
				{
					if (pActorOwner->GetStamina() < (g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina * 2))
					{
						if (!shot)
							ProcessCancelAction();
					}
				}
			}
		}

		if (shot && m_arrow_hold_time >= m_hold_time_max_to_vel)
		{
			hold_started = false;
			shot = false;
			arrow_loaded = false;
			m_arrow_hold_time = 0.0f;
			EndHoldAnim();
			DeleteArrowAttachment();
			m_arrow_shot_recovery_time = 1.0f;
			ChekMainArrowItem();
			m_stamina_cons_time = 0.0f;
		}
	}

	if(shot)
	{

	}

	//CWeapon::RequireUpdate(eIUS_General);
	BaseClass::Update(ctx, slot);
	//RequireUpdate();
}

void CBow::OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	int def_active_mode = activationMode;
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (actionId == "attack1")
	{
		if (!pItem || !pActor)
			return;

		if (activationMode == eAAM_OnPress)
			ReinitFiremode();

		if (m_arrow_shot_recovery_time >= 0)
			return;

		if (!arrow_loaded && !process_arrow_load_started)
		{
			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}
			}

			if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
			{
				if (gEnv->bMultiplayer)
				{
					ReinitFiremode();
					string act_idsc = actionId.c_str();
					if (gEnv->bServer)
						GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
					else
					{
						GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
					}
				}
			}
			ProcessReloadArrow();
			return;
		}

		if (!arrow_loaded)
			return;

		if (m_arrow_hold_time == 0.0f)
			activationMode = eAAM_OnPress;

		if (g_pGameCVars->g_stamina_sys_can_bow_draw_if_low_stmn_val == 0)
		{
			if (pActor->GetStamina() < (g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina * 2))
			{
				if (activationMode == eAAM_OnPress)
					return;
			}
		}

		if (activationMode == eAAM_OnPress)
		{
			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}

				if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
				{
					return;
				}
			}

			if (m_arrow_hold_time > 0.0f)
				return;

			if (this->CanFire() && !this->IsBusy())
			{
				int idr = this->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					IEntityClass* pAmmoType = pFmd->GetAmmoType();
					if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
					{
						shot = false;
						hold_started = true;
						if (!arrow_attached_to_bow)
						{
							CreateArrowAttachment();
						}

						if (anim_hold_started)
							anim_hold_started = false;

						if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
						{
							if (gEnv->bMultiplayer)
							{
								ReinitFiremode();
								string act_idsc = actionId.c_str();
								if (gEnv->bServer)
									GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
								else
								{
									GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
								}
							}
						}
					}
				}
			}
		}
		else if (activationMode == eAAM_OnRelease)
		{
			if (!hold_started)
				return;

			if (m_arrow_hold_time < m_hold_time_min_to_shot)
				return;

			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				m_arrow_hold_time_old = m_arrow_hold_time;
				IEntityClass* pAmmoType = pFmd->GetAmmoType();
				if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
				{
					shot = true;
				}
				anim_hold_started = false;
				m_arrow_hold_time = m_hold_time_max_to_vel;
				arrow_loaded = false;
				if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
				{
					if (gEnv->bMultiplayer)
					{
						ReinitFiremode();
						string act_idsc = actionId.c_str();
						if (gEnv->bServer)
							GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
						else
						{
							GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
						}
					}
				}
			}
		}
	}
	else if (actionId == "reload")
	{
		if (m_zm && m_zm->IsZoomed())
			return;

		if(m_arrow_shot_recovery_time>=0)
			return;

		if (m_arrow_hold_time > 0.2f)
		{
			if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
			{
				if (gEnv->bMultiplayer)
				{
					ReinitFiremode();
					string act_idsc = actionId.c_str();
					if (gEnv->bServer)
						GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
					else
					{
						GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
					}
				}
			}
			ProcessCancelAction();
			return;
		}

		if (!arrow_loaded && !process_arrow_load_started)
		{
			if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
			{
				if (gEnv->bMultiplayer)
				{
					ReinitFiremode();
					string act_idsc = actionId.c_str();
					if (gEnv->bServer)
						GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
					else
					{
						GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
					}
				}
			}
			ProcessReloadArrow();
		}
		return;
	}
	else if (actionId == "attack2_xi")
	{
		if (!pActor)
			return;

		if (m_zm && m_zm->IsZoomed())
			return;

		if (pActor->GetStamina() < (pActor->stamina_cons_atk_base * pActor->stamina_cons_atk_ml))
		{
			if (g_pGameCVars->g_stamina_sys_can_melee_attack_if_low_stmn_val == 0)
				return;
		}

		if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return;
		}

		if (hold_started)
			return;

		if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
		{
			if (gEnv->bMultiplayer)
			{
				ReinitFiremode();
				string act_idsc = actionId.c_str();
				if (gEnv->bServer)
					GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
				else
				{
					GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
				}
			}
		}

		RequireUpdate(eIUS_General);
		//RequireUpdate(eIUS_FireMode);
		SpecialMeleeRequestDelayedSimpleAttack(0.3f, m_close_combat_damage + (pActor->GetActorStrength() * 0.1f), 0.4, 9);
		const ItemString &meleeAction = "bow_melee_attack";
		FragmentID fragmentId = GetFragmentID(meleeAction.c_str());
		PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		SetItemFlag(CItem::eIF_BlockActions, true);
		m_arrow_shot_recovery_time = 0.7f;
		OnMeleeNw(pActor->GetEntityId());
		return;
	}
	else if (actionId == "wpn_block_melee")
	{
		if (m_zm && m_zm->IsZoomed())
			return;

		/*if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return;
		}*/
		if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
		{
			if (gEnv->bMultiplayer)
			{
				ReinitFiremode();
				string act_idsc = actionId.c_str();
				if (gEnv->bServer)
					GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
				else
				{
					GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
				}
			}
		}
		m_arrow_shot_recovery_time = 0.5f;
		CWeapon::OnAction(actorId, actionId, activationMode, value);
		return;
	}

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
			{
				if (gEnv->bMultiplayer)
				{
					ReinitFiremode();
					string act_idsc = actionId.c_str();
					if (gEnv->bServer)
						GetGameObject()->InvokeRMI(ClOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToRemoteClients);
					else
					{
						GetGameObject()->InvokeRMI(SVOnActionXBow(), CBow::ActionRequestParamX(actorId, act_idsc, def_active_mode, value), eRMI_ToServer);
					}
				}
			}
			CWeapon::OnAction(actorId, actionId, activationMode, value);
		}
	}
}

void CBow::CreateArrowAttachment()
{
	if(!this->GetOwnerActor())
		return;

	ICharacterInstance *pCharacter = NULL;
	if (!this->GetOwnerActor()->IsThirdPerson())
		pCharacter = GetEntity()->GetCharacter(eIGS_FirstPerson);
	else
		pCharacter = GetEntity()->GetCharacter(eIGS_ThirdPerson);

	if (!pCharacter)
	{
		CryLogAlways("Error, no bow character to arrow attach");
		return;
	}

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (!pFmd)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(this->GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (pItem)
	{
		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName("arrow_att_h");
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment("arrow_att_h", CA_BONE, "ctr_bone", true);
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
				arrow_attached_to_bow = true;
			}
		}
		else
		{
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				//pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
				arrow_attached_to_bow = true;
			}
		}
		return;
	}
	//CryLogAlways("try to create arrow attachment");
	IEntityClass* pAmmoType = pFmd->GetAmmoType();
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName("arrow_att_h");
	const SAmmoParams *pAmmoParams = g_pGame->GetWeaponSystem()->GetAmmoParams(pAmmoType);
	if (pAmmoParams && !pAmmoParams->fpGeometryName.empty())
	{
		if (!pIAttachment)
		{
			//CryLogAlways(pAmmoParams->fpGeometryName.c_str());
			pIAttachment = pIAttachmentManager->CreateAttachment("arrow_att_h", CA_BONE, "ctr_bone", true);
			/*if (!pAmmoParams->fpGeometryName.empty())
			{
				if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAmmoParams->fpGeometryName.c_str()))
				{
					CCGFAttachment *pCGFAttachment = new CCGFAttachment();
					pCGFAttachment->pObj = pStatObj;
					pIAttachment->AddBinding(pCGFAttachment);
					pIAttachment->AlignJointAttachment();
					pIAttachment->HideAttachment(false);
				}
			}*/
		}

		if(pIAttachment)
		{
			if (!pAmmoParams->fpGeometryName.empty())
			{
				bool skel = false;
				if (0 == stricmp(PathUtil::GetExt(pAmmoParams->fpGeometryName.c_str()), "chr"))
				{
					skel = true;
				}

				if (0 == stricmp(PathUtil::GetExt(pAmmoParams->fpGeometryName.c_str()), "cdf"))
				{
					skel = true;
				}

				if (!skel)
				{
					if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAmmoParams->fpGeometryName.c_str()))
					{
						pIAttachment->ClearBinding();
						CCGFAttachment *pCGFAttachment = new CCGFAttachment();
						pCGFAttachment->pObj = pStatObj;
						pIAttachment->AddBinding(pCGFAttachment);
						pIAttachment->HideAttachment(false);
					}
				}
				else
				{
					if (ICharacterInstance *charInst = gEnv->pCharacterManager->CreateInstance(pAmmoParams->fpGeometryName.c_str()))
					{
						CSKELAttachment *pChrAttachment = new CSKELAttachment();
						pChrAttachment->m_pCharInstance = charInst;
						pIAttachment->AddBinding(pChrAttachment);
						pIAttachment->AlignJointAttachment();
					}
				}
			}
		}
	}
}

void CBow::DeleteArrowAttachment()
{
	ICharacterInstance *pCharacter = this->GetEntity()->GetCharacter(eIGS_FirstPerson);
	if (pCharacter)
	{
		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		IAttachment *pAttachmentt = pIAttachmentManager->GetInterfaceByName("arrow_att_h");
		if (pAttachmentt)
		{
			pAttachmentt->ClearBinding();
			arrow_attached_to_bow = false;
		}
	}
	ICharacterInstance *pCharacter2 = this->GetEntity()->GetCharacter(eIGS_ThirdPerson);
	if (pCharacter2)
	{
		IAttachmentManager* pIAttachmentManager = pCharacter2->GetIAttachmentManager();
		IAttachment *pAttachmentt = pIAttachmentManager->GetInterfaceByName("arrow_att_h");
		if (pAttachmentt)
		{
			pAttachmentt->ClearBinding();
			arrow_attached_to_bow = false;
		}
	}
}

void CBow::StartHoldAnim()
{
	if (anim_hold_started)
		return;

	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(5, 0.03);
	}
	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 5;
	aparams.m_fAllowMultilayerAnim = 1;
	//aparams.m_fLayerBlendIn = 1;
	//aparams.m_nFlags = CA_FORCE_SKELETON_UPDATE;
	aparams.m_fTransTime = 1;
	//aparams.m_fInterpolation = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_LOOP_ANIMATION;
	//aparams.m_fKeyTime = 0.00;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_v))
	{
		//CryLogAlways("Start Bow anim");
		bool anim_started_bow = this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("weaponBow_start_exect_loop", aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(5, 1);
		//if (!anim_started_bow)
			//CryLogAlways("Failed Bow anim start");
	}
	else
	{
		//CryLogAlways("Error, no bow character to start anim");
	}
	anim_hold_started = true;
}

void CBow::EndHoldAnim()
{
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(5, 0.5);
	}
	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 5;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0.1;
	aparams.m_fPlaybackSpeed = 2.5f;
	aparams.m_nFlags=0;
	aparams.m_fKeyTime = 0.05;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_v))
	{
		//CryLogAlways("Start stop Bow anim");
		bool anim_started_bow = this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("weaponBow_end_exect", aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(5, 1);
		//if (!anim_started_bow)
		//	CryLogAlways("Failed Bow anim start");
	}
}

void CBow::EndHoldAnimByCancel()
{
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(5, 0.5);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->ClearFIFOLayer(5);
	}
	else if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(5, 0.5);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->ClearFIFOLayer(5);
	}
}

void CBow::ProcessReloadArrow()
{
	//CryLogAlways("TryToStart bow reload1");
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() <= 0)
		{
			return;
		}

		if (!pItem)
		{
			pFmd->SetAmmoType("noarrow");
			return;
		}

	}
	//CryLogAlways("TryToStart bow reload2");
	if (hold_started)
		return;
	//CryLogAlways("TryToStart bow reload3");
	if (shot)
		return;
	//CryLogAlways("TryToStart bow reload4");
	m_arrow_hold_time = 0.0f;
	m_arrow_load_time = 0.0f;
	process_arrow_load_started = true;
	const ItemString &bowAction = "bow_reloading";
	FragmentID fragmentId = GetFragmentID(bowAction.c_str());
	PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	SetItemFlag(CItem::eIF_BlockActions, true);
	RequireUpdate(eIUS_General);
	RequireUpdate(eIUS_FireMode);
}

void CBow::AttachArrowToHand()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (string(pAmmoType->GetName()) == string("noarrow"))
			pAmmoType = NULL;

		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			//pItem->SetHand(0);
			//pItem->HideItem(false);
			//g//pItem->AttachToHand(true);
			if (pActor->IsPlayer())
				pItem->Select(true);
			else
			{
				//CryLogAlways("CBow::AttachArrowToHand()");
				pItem->SetHand(0);
				pItem->HideItem(false);
				pItem->AttachToHand(true);
			}

			int mode_v = 1;
			if (pActor->IsThirdPerson())
				mode_v = 2;

			arrow_attached_to_hand = true;
			GetOrPutArrowToQuiver(true);
		}
	}

}

void CBow::DetachArrowFromHand()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (pItem)
	{
		if (pActor->IsPlayer())
			pItem->Select(false);
		else
		{
			pItem->HideItem(true);
			pItem->SetHand(0);
			pItem->AttachToHand(false);
		}
		arrow_attached_to_hand = false;
	}
}

void CBow::GetOrPutArrowToQuiver(bool get)
{
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (pItem)
	{
		if (pItem->GetItemQuanity() > 10)
			return;

		string attachment_name = "arrow_quiver";
		if (ICharacterInstance *pAttachmentObjectChar = pActor->GetAttachmentObjectCharacterInstance(0, attachment_name.c_str()))
		{
			int number_arrows = pItem->GetItemQuanity();
			if (pAttachmentObjectChar->GetIAttachmentManager())
			{
				if (get)
				{
					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", pItem->GetItemQuanity() - 1);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						pAttachment_quiver_arrow->ClearBinding();
					}
				}
				else
				{

					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", pItem->GetItemQuanity());
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && !pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
						{
							CCGFAttachment *pCGFAttachment = new CCGFAttachment();
							pCGFAttachment->pObj = pStatObj;
							pAttachment_quiver_arrow->AddBinding(pCGFAttachment);
						}
					}
				}
			}
		}
	}
}

void CBow::ChekMainArrowItem()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	if(!pActor->IsClient())
		return;

	if (!pActor->IsPlayer())
	{
		if (g_pGameCVars->g_disable_ammo_consuption_for_ai_characters > 0)
			return;
	}
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (pItem && pItem->GetItemQuanity()>0)
	{
		pItem->SetItemQuanity(pItem->GetItemQuanity() - 1);
		if (pItem->GetItemQuanity() < 1)
		{
			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
				//pActor->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);
				pItem->SetEquipped(0);
				if (pItem->GetEquipped() > 0)
				{
					pActor->EquipItem(pItem->GetEntityId());
				}
				//if (pActorActionsNew)
				//	pActorActionsNew->DeEquipArmorOrCloth(pActor->GetEntityId(), pItem->GetEntityId());

				pItem->Select(false);
				pItem->AttachToHand(false, true);
				CGameRules *pGameRules = g_pGame->GetGameRules();
				if (pGameRules)
				{
					pItem->GetEntity()->SetPos(Vec3(0.0f,0.0f,cry_random(0.0f, -65535.0f)));
					if (pActor->GetInventory())
					{
						pActor->GetInventory()->RemoveItem(pItem->GetEntityId());
						pItem->Pickalize(false, false);
						pItem->GetEntity()->Hide(true);
					}

					if(gEnv->bMultiplayer)
						pGameRules->GetGameObject()->InvokeRMI(CGameRules::SvDropAndHideItem(), CGameRules::SEntityPrmIdOnly(pItem->GetEntityId()), eRMI_ToServer);
				}
			}
		}
	}
}

void CBow::ProcessCancelAction()
{
	if (shot)
		return;

	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		pFmd->Cancel();
		pFmd->Activate(false);
		pFmd->Activate(true);
		//pFmd->
		hold_started = false;
		anim_hold_started = false;
		shot = false;
		m_arrow_hold_time = 0.0f;
		EndHoldAnimByCancel();
		const ItemString &bowAction = "bow_canceling";
		FragmentID fragmentId = this->GetFragmentID(bowAction.c_str());
		this->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		m_arrow_shot_recovery_time += 1.2f;
	}
	//GetOrPutArrowToQuiver(false);
}

//staff for AI characters only--------------------------------------
void CBow::StartFire()
{
	int activationMode = eAAM_OnPress;
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (!pItem || !pActor)
		return;

	if (pActor->IsPlayer())
	{
		CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
		if (pPlayer)
		{
			if (!pPlayer->CanFire())
				return;
		}
	}

	if (pActor->IsPlayer())
	{
		CWeapon::StartFire();
		return;
	}
	else
	{
		SProjectileLaunchParams launchParams;
		launchParams.fSpeedScale = 66.0f;
		launchParams.vShootTargetPos = Vec3(0,0,0);
		launchParams.trackingId = 0;
		this->StartFire(launchParams);
		return;
	}
}

void CBow::StartFire(const SProjectileLaunchParams& launchParams)
{
	ReinitFiremode();
	//CryLogAlways("TryToStart bow attack");
	int activationMode = eAAM_OnRelease;
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	if (pActor->IsPlayer())
	{
		CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
		if (pPlayer)
		{
			if (!pPlayer->CanFire())
				return;
		}
	}

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (!pItem)
		return;

	if (pActor->IsPlayer())
	{
		CWeapon::StartFire();
		return;
	}

	CryLogAlways("TryToStart bow attack1");
	if (m_arrow_shot_recovery_time >= 0)
		return;
	CryLogAlways("TryToStart bow attack2");
	if (!arrow_loaded && !process_arrow_load_started)
	{
		CryLogAlways("TryToStart bow reload");
		ProcessReloadArrow();
		return;
	}
	CryLogAlways("TryToStart bow attack3");
	if (!arrow_loaded)
		return;
	//CryLogAlways("TryToStart bow attack4");
	if (m_arrow_hold_time == 0.0f)
		activationMode = eAAM_OnPress;
	//CryLogAlways("TryToStart bow attack5");
	if (activationMode == eAAM_OnPress)
	{
		//CryLogAlways("TryToStart bow attack6");
		if (m_arrow_hold_time > 0.0f)
			return;
		//CryLogAlways("TryToStart bow attack7");
		if (this->CanFire() && !this->IsBusy())
		{
			//CryLogAlways("TryToStart bow attack8");
			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				//CryLogAlways("TryToStart bow attack9");
				IEntityClass* pAmmoType = pFmd->GetAmmoType();
				if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
				{
					//CryLogAlways("TryToStart bow attack10");
					shot = false;
					hold_started = true;
					if (!arrow_attached_to_bow)
					{
						CreateArrowAttachment();
					}

					if (anim_hold_started)
						anim_hold_started = false;

				}
			}
		}
	}
	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			if (activationMode == eAAM_OnPress)
				CWeapon::StartFire();
		}
	}
}

void CBow::StopFire()
{
	//CryLogAlways("TryToStop bow attack");
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (!pItem || !pActor)
		return;

	if (pActor->IsPlayer())
	{
		CWeapon::StopFire();
		return;
	}

	if (!hold_started)
		return;

	if (m_arrow_hold_time < m_hold_time_min_to_shot)
		return;

	if (!pActor->IsPlayer() && (m_arrow_hold_time < m_hold_time_min_to_shot))
		return;

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		m_arrow_hold_time_old = m_arrow_hold_time;
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			shot = true;
		}
		anim_hold_started = false;
		m_arrow_hold_time = m_hold_time_max_to_vel;
		arrow_loaded = false;
	}
	CWeapon::StopFire();
}

void CBow::OnSelected(bool selected)
{
	CWeapon::OnSelected(selected);
	CryLogAlways("CBow::OnSelected");
	SetBusy(false);
	if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
	{
		SetItemFlag(CItem::eIF_BlockActions, false);
	}
	ReinitFiremode();
	shot = false;
	hold_started = false;
	m_arrow_hold_time = 0.0f;
	m_arrow_load_time = 0.0f;
	m_arrow_load_time_sequence_1 = 0.8f;
	m_arrow_load_time_sequence_2 = 2.0f;
	m_arrow_shot_recovery_time = 0.0f;
	m_arrow_hold_time_old = 0.0f;
	m_hold_time_min_to_shot = 0.9f;
	m_hold_time_max_to_vel = 3.0f;
	m_stamina_cons_time = 0.0f;
	anim_hold_started = false;
	arrow_loaded = false;
	process_arrow_load_started = false;
	arrow_attached_to_hand = false;
	arrow_attached_to_bow = false;
	//m_close_combat_damage = 15.0f;
}

void CBow::OnEnterFirstPerson()
{
	CWeapon::OnEnterFirstPerson();
}

void CBow::OnEnterThirdPerson()
{
	CWeapon::OnEnterThirdPerson();
}

void CBow::ReinitFiremode()
{
	if (hold_started || process_arrow_load_started)
		return;

	CActor *pActor = GetOwnerActor();
	//CryLogAlways(pActor->GetEntity()->GetName());
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (!pItem || !pActor)
		return;

	EntityId itemIdx = pActor->GetCurrentItemId();
	if (pActor->IsRemote())
	{
		CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
		if (pPlayer)
		{
			itemIdx = pPlayer->NetGetLastItemId();
		}
	}

	if (itemIdx > 0)
	{
		CWeapon* pWeapon = pActor->GetWeapon(itemIdx);
		if (pWeapon)
		{
			int idr = 0;
			idr = pWeapon->GetCurrentFireMode();
			IFireMode *pFmd = pWeapon->GetFireMode(idr);
			if (pFmd)
			{
				CThrow *fm = (CThrow *)pFmd;
				if (fm)
				{
					pBowParams = fm->GetBowFireParams();
					if (pBowParams.ccDamageNrm > 0.0f)
					{
						m_close_combat_damage = pBowParams.ccDamageNrm;
					}
				}
				if (pItem)
				{
					IEntityClass* pAmmoType = pFmd->GetAmmoType();
					if (pAmmoType)
					{
						pFmd->SetAmmoType(pItem->GetItemAmmoType());
						if (fm)
							fm->SetAmmoType(pItem->GetItemAmmoType());

						if (strcmp(pAmmoType->GetName(), pItem->GetItemAmmoType()))
						{
							CryLogAlways("pFmd->SetAmmoType(pItem->GetItemAmmoType()), ammo == %s", pItem->GetItemAmmoType());
							pFmd->SetAmmoType(pItem->GetItemAmmoType());
							pFmd->Activate(false);
							pFmd->Activate(true);
						}
					}
					else
					{
						pFmd->SetAmmoType(pItem->GetItemAmmoType());
						pFmd->Activate(false);
						pFmd->Activate(true);
					}
				}
			}
		}
	}
}

void CBow::NetXcAction(const ActionRequestParamX & params)
{
	ReinitFiremode();
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		//return;
	}
	//OnAction(params.actorId, params.actionName, params.activationMode, params.activationValue);
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	int ac_mode = params.activationMode;
	const ActionId& actionId = static_cast<ActionId>(params.actionName);
	if (params.actionName == "attack1")
	{
		if (!pItem || !pActor)
			return;

		if (ac_mode == eAAM_OnPress)
			ReinitFiremode();

		if (m_arrow_shot_recovery_time >= 0)
			return;

		if (!arrow_loaded && !process_arrow_load_started)
		{
			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}
			}

			if(pItem->GetItemQuanity() < 1)
				return;

			ProcessReloadArrow();
			return;
		}

		if (!arrow_loaded)
			return;

		if (m_arrow_hold_time == 0.0f)
			ac_mode = eAAM_OnPress;

		if (g_pGameCVars->g_stamina_sys_can_bow_draw_if_low_stmn_val == 0)
		{
			if (pActor->GetStamina() < (g_pGameCVars->g_stamina_sys_bow_draw_consumpt_stamina * 2))
			{
				if (ac_mode == eAAM_OnPress)
					return;
			}
		}

		if (ac_mode == eAAM_OnPress)
		{
			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}

				if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
				{
					return;
				}
			}

			if (m_arrow_hold_time > 0.0f)
				return;

			if (this->CanFire() && !this->IsBusy())
			{
				int idr = this->GetIWeapon()->GetCurrentFireMode();
				IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
				if (pFmd)
				{
					IEntityClass* pAmmoType = pFmd->GetAmmoType();
					if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
					{
						shot = false;
						hold_started = true;
						if (!arrow_attached_to_bow)
						{
							CreateArrowAttachment();
						}

						if (anim_hold_started)
							anim_hold_started = false;
					}
				}
			}
		}
		else if (ac_mode == eAAM_OnRelease)
		{
			if (!hold_started)
				return;

			//if (m_arrow_hold_time < m_hold_time_min_to_shot)
			//	return true;

			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				m_arrow_hold_time_old = m_arrow_hold_time;
				IEntityClass* pAmmoType = pFmd->GetAmmoType();
				if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
				{
					shot = true;
				}
				anim_hold_started = false;
				m_arrow_hold_time = m_hold_time_max_to_vel;
				arrow_loaded = false;
				CWeapon::StopFire();
			}
		}
	}
	else if (params.actionName == "reload")
	{
		if (m_zm && m_zm->IsZoomed())
			return;

		if (m_arrow_shot_recovery_time >= 0)
			return;

		if (m_arrow_hold_time > 0.2f)
		{
			ProcessCancelAction();
			return;
		}

		if (!arrow_loaded && !process_arrow_load_started)
		{
			ProcessReloadArrow();
		}
		return;
	}
	else if (params.actionName == "attack2_xi")
	{
		if (!pActor)
			return;

		if (hold_started)
			return;

		RequireUpdate(eIUS_General);
		const ItemString &meleeAction = "bow_melee_attack";
		FragmentID fragmentId = GetFragmentID(meleeAction.c_str());
		PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		return;
	}
	else if (params.actionName == "wpn_block_melee")
	{
		if (m_zm && m_zm->IsZoomed())
			return;

		m_arrow_shot_recovery_time = 0.5f;
		CWeapon::OnAction(params.actorId, actionId, ac_mode, params.activationValue);
		return;
	}

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			CWeapon::OnAction(params.actorId, actionId, ac_mode, params.activationValue);
		}
	}
	return;
}

void CBow::ForcedStopFire()
{
	if (arrow_loaded)
	{
		if (hold_started)
			CWeapon::StopFire();

		CActor *pActor = CWeapon::GetOwnerActor();
		if (pActor)
		{
			CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				m_arrow_hold_time_old = m_arrow_hold_time;
				IEntityClass* pAmmoType = pFmd->GetAmmoType();
				if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
				{
					shot = true;
				}
				anim_hold_started = false;
				m_arrow_hold_time = m_hold_time_max_to_vel;
				arrow_loaded = false;
				shot = false;
				arrow_loaded = false;
				m_arrow_hold_time = 0.0f;
				hold_started = false;
				EndHoldAnim();
				DeleteArrowAttachment();
				m_arrow_shot_recovery_time = 0.0f;
				ChekMainArrowItem();
				shot = false;
				hold_started = false;
				m_arrow_hold_time = 0.0f;
				m_arrow_load_time = 0.0f;
				m_arrow_load_time_sequence_1 = 0.8f;
				m_arrow_load_time_sequence_2 = 2.0f;
				m_arrow_shot_recovery_time = 0.0f;
				m_arrow_hold_time_old = 0.0f;
				m_hold_time_min_to_shot = 0.9f;
				m_hold_time_max_to_vel = 3.0f;
				anim_hold_started = false;
				arrow_loaded = false;
				process_arrow_load_started = false;
				arrow_attached_to_hand = false;
				arrow_attached_to_bow = false;
				m_stamina_cons_time = 0.0f;
			}
			GetOrPutArrowToQuiver(false);
		}
		//CWeapon::StopFire();
	}
	else if (process_arrow_load_started)
	{
		if (arrow_attached_to_hand)
		{
			DetachArrowFromHand();
			GetOrPutArrowToQuiver(false);
		}
	}
}
//-----------------------------------------------------------------
void CBow::OnHitDeathReaction()
{
	if (!GetOwnerActor())
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
	if (!pItem)
		return;

	if (process_arrow_load_started)
	{
		if (arrow_attached_to_hand)
		{
			Matrix34 mtx_rv = Matrix34::CreateIdentity();
			IAttachmentManager* pAttachmentManager = GetOwnerAttachmentManager();
			if (pAttachmentManager)
			{
				IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName(m_sharedparams->params.attachment[m_stats.hand].c_str());
				if (pAttachment)
				{
					mtx_rv = Matrix34(pAttachment->GetAttWorldAbsolute());
				}
			}
			EntityId nv_it_id = m_pItemSystem->CreateItemSimple(mtx_rv, pItem->GetBaseItemToStack());
			CItem *pItem_dropped = static_cast<CItem *>(m_pItemSystem->GetItem(nv_it_id));
			if (pItem_dropped)
			{
				pItem_dropped->Physicalize(true, true);
				pItem_dropped->Impulse(pItem_dropped->GetEntity()->GetPos(), Vec3(0, 0, 0.2), 2.0f);
			}
			DetachArrowFromHand();
			pItem->SetItemQuanity(pItem->GetItemQuanity() - 1);
		}

		m_arrow_load_time = 0.0f;
		process_arrow_load_started = false;
	}

	if (m_arrow_hold_time > 0.0f)
	{
		m_arrow_hold_time = m_hold_time_min_to_shot + 0.01f;
		StopFire();
	}
	else
	{

	}
}

IMPLEMENT_RMI(CBow, SVOnActionXBow)
{
	if (!GetOwnerActor())
		return true;

	GetGameObject()->InvokeRMI(CBow::ClOnActionXBow(), CBow::ActionRequestParamX(params.actorId, params.actionName, params.activationMode, params.activationValue), eRMI_ToOtherClients, GetOwnerActor()->GetChannelId());
	if (gEnv->bServer && gEnv->IsDedicated())
	{
		NetXcAction(params);
	}
	return true;
}

IMPLEMENT_RMI(CBow, ClOnActionXBow)
{
	NetXcAction(params);
	return true;
}