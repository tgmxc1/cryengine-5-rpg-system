/*************************************************************************
CMD

*************************************************************************/
#include "StdAfx.h"
#include "HUDCommon.h"
#include "UI/UICVars.h"
#include "UI/UIManager.h"
#include "UI/UIInput.h"
#include "IUIDraw.h"
#include "HUDSilhouettes.h"

#include <CrySystem/Scaleform/IFlashPlayer.h>
#include "EntityUtility/EntityScriptCalls.h"
#include <CryAISystem/IAIActor.h>
#include "AI/GameAISystem.h"
#include "Actor.h"
#include "Item.h"
#include "ItemSharedParams.h"
#include "IWeapon.h"

#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"

#include "ActorActionsNew.h"
#include "PlayerCharacterCreatingSys.h"
#include "DialogSystemEvents.h"
#include "GameXMLSettingAndGlobals.h"

#include "PlayerProfiles/BMPHelper.h"
#include <Cry3DEngine/ITimeOfDay.h>
#include <CrySystem/ITimer.h>

#include "Environment/Chest.h"

//#include "IImage.h"

//#include "UI/Menu3dModels/MenuRender3DModelMgr.h"

#pragma warning(push)
#pragma warning(disable : 4244)
//#pragma warning(disable : 4700)

//-----------------------------------------------------------------------------------------------------
//-- IConsoleArgs
//-----------------------------------------------------------------------------------------------------

void CHUDCommon::HUD(ICVar *pVar)
{
	
}

//-----------------------------------------------------------------------------------------------------

CHUDCommon::CHUDCommon() 
{
	//CryLogAlways("CHUDCommon::CHUDCommon()  constructor call");
	if(gEnv->pHardwareMouse)
	{
		gEnv->pHardwareMouse->AddListener(this);
	}
	Inv_loaded = false;
	ChrMenu_loaded = false;
	MainHud_loaded = false;
	Inv_ItemInfo_loaded = false;
	StatMsg_loaded = false;
	BuffInfo_loaded = false;
	SpellInfo_loaded = false;

	m_DropActive = false;
	
	m_DialogSystemVisible = false;
	m_DialogSystemAltVisible = false;
	m_ChestInvVisible = false;
	m_CharacterEquipmentVisible = false;
	m_Icmenu = false;
	m_BookVisible = false;
	m_CharacterCreationVisible = false;
	m_TradeSystemVisible = false;
	m_QuestSystemVisible = false;
	m_SpellSelectionVisible = false;
	m_CharacterPerksMenuVisible = false;
	m_CharacterSkillsMenuVisible = false;
	m_CharacterSkillInfoVisible = false;
	m_CharacterSkillSelectedId = 0;

	m_InteractiveInfoVisible = false;

	m_CM_opened_after_levelup = false;

	m_fs_tab_open = false;

	Inv_cont_menu_loaded = false;

	start_drag = false;

	m_str_val_cm = 0;
	m_agl_val_cm = 0;
	m_int_val_cm = 0;
	m_endr_val_cm = 0;
	m_will_val_cm = 0;
	m_pers_val_cm = 0;
	m_Mhp_val_cm = 0;
	m_Mst_val_cm = 0;
	m_Mmg_val_cm = 0;

	old_silhol_ent = 0;
	current_dragged_item = 0;
	current_dragged_item_slot = 0;

	item_for_drag_exchandge = 0;
	slot_for_drag_exchandge = 0;

	Inv_dragged_item_loaded = false;
	request_for_delayed_update_player_doll = false;
	delayed_update_player_doll_timer = 0.0f;
	id_hud_pl = 0;
	item_drop_request = false;

	hit_on_player_doll_clip = false;
	hit_on_drop_clip = false;
	player_doll_clip_pressed = false;
	player_doll_clip_pressed_pos = Vec2(0,0);
	player_doll_clip_pressed_pos_old = Vec2(0, 0);
	m_MinimapVisible = false;
	m_MainMapVisible = false;

	m_PostInitScreenVisible = false;

	for (int i = 0; i < 255; i++)
	{
		questionloaded[i] = false;
	}
	
	for (int i = 0; i < 2; i++)
	{
		maps_update_timers[i] = 0.0f;
		maps_update_delays[i] = 0.3f;
		sd_ui_timers[i] = 1.3f;
	}

	objInf_update_timer = 0.0f;
	objInf_update_timer2 = 0.0f;
	objInf_update_delay = 0.3f;
	objInf_update_delay2 = 0.024f;

	for (int i = 0; i < 5; i++)
	{
		old_main_player_values[i] = 100.0f;
		player_values_alpha_timers[i] = 0;
	}
	entites_ont_map_to_update.clear();
	static_markers_on_minimap_ids.clear();
	entites_ont_screen_to_update.clear();
	target_entites_ont_screen_to_update.clear();
	SimpleCrosshair_loaded = false;
	SelectedWeaponsInfo_loaded = false;
	old_player_selected_item = 0;
	old_player_selected_item2 = 0;
	old_player_selected_item_left = 0;
	old_player_selected_spell_id = -1;
	old_player_selected_spell_id_left = -1;

	character_model_clip_pose = Vec2(0, 0);

	Character_Creation_Menu_Loaded = false;
	for (int i = 0; i < 4; i++)
	{
		Effects_vals_DOF[i] = 0.0f;
	}
	const char *ppEquipmentParts[] = { "body_lower", "body_upper", "foots", "hands", "Head_helm", "left_weapon",
		"ornament_01", "ornament_02", "ornament_03", "ornament_04", "ornament_05", "ornament_06", "ornament_07", "right_weapon" };
	const int numEquipmentParts = CRY_ARRAY_COUNT(ppEquipmentParts);
	for (int i = 0; i < numEquipmentParts; ++i)
	{
		equipment_slots_names[i] = ppEquipmentParts[i];
	}
	current_equipment_dragged_slot = "";
	m_TimerInventoryUpdate = NULL;
	m_TimerInventoryDtl_disable = NULL;
	m_TimerContainerUI_disable = NULL;
	m_TimerCurrentInteractiveUI_disable = NULL;
	m_TimerGoToMainMenu = NULL;
	m_TimerLoadingMap = NULL;
	m_TimerLoadingMiniMap = NULL;
	m_TimerLoadingAblUi = NULL;
	m_TimerDialogUIDisablingBB = NULL;
	m_TimerEnInv = NULL;
	num_loaded_mm_sectors = 0;
	num_loaded_m_sectors = 0;
	current_chest_or_shop_tab_opened = 3;
	chest_or_shop_showed_player_inv = false;
	current_Seller = 0;
	current_spell_selection_tab_opened = 3;
	disconnect_requested = 0;
	m_Minimap_closed_bitui = false;
	m_QuickSelectionVisible = false;
	current_mouse_world_pos = Vec3(ZERO);
	current_mouse_world_pos_projected = Vec3(ZERO);

	m_GamePausedIU = false;
}

//-----------------------------------------------------------------------------------------------------

CHUDCommon::~CHUDCommon()
{
	
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::HandleFSCommand(const char *strCommand,const char *strArgs, void* pUserData)
{ 
	string dragAndDrop = "dragAndDrop";
	string DebugCom = "debugCL";
	string PlaySoundCom = "fsSound";
	string MapCom = "mapC";
	string InventoryCom = "invC";
	string SpellSelectionCom = "splC";
	string CharacterMenuCom = "chrC";
	string CreationMenuCom = "crmC";
	string DialogCom = "diaC";
	string QuestsCom = "queC";
	string MhudCom = "mhuC";
	string TradeCom = "trdC";
	string CommonUiCom = "cuiC";
	//-----------------------------------------------------------------------------
	string commandChecker = strCommand;
	if (commandChecker.size() >= dragAndDrop.size())
	{
		string dragAndDrop_lc = commandChecker.substr(0, dragAndDrop.size());
		if (dragAndDrop_lc == dragAndDrop)
		{
			//realization of Drag&Drop technology for inventory & etc...
			DragAndDropFSRealization(strCommand, strArgs);
			//-------------------------------------------------
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= DebugCom.size())
	{
		string DebugCom_lc = commandChecker.substr(0, DebugCom.size());
		if (DebugCom_lc == DebugCom)
		{
			DebugFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= PlaySoundCom.size())
	{
		string SoundCom_lc = commandChecker.substr(0, PlaySoundCom.size());
		if (SoundCom_lc == PlaySoundCom)
		{
			PlaySoundUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= MapCom.size())
	{
		string MapCom_lc = commandChecker.substr(0, MapCom.size());
		if (MapCom_lc == MapCom)
		{
			MapUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= InventoryCom.size())
	{
		string InvCom_lc = commandChecker.substr(0, InventoryCom.size());
		if (InvCom_lc == InventoryCom)
		{
			InventoryUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= SpellSelectionCom.size())
	{
		string SpellCom_lc = commandChecker.substr(0, SpellSelectionCom.size());
		if (SpellCom_lc == SpellSelectionCom)
		{
			SpellsMenuUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= CharacterMenuCom.size())
	{
		string CharacCom_lc = commandChecker.substr(0, CharacterMenuCom.size());
		if (CharacCom_lc == CharacterMenuCom)
		{
			ChrMenuUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= CreationMenuCom.size())
	{
		string CreaCom_lc = commandChecker.substr(0, CreationMenuCom.size());
		if (CreaCom_lc == CreationMenuCom)
		{
			CreationMenuUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= DialogCom.size())
	{
		string DialogCom_lc = commandChecker.substr(0, DialogCom.size());
		if (DialogCom_lc == DialogCom)
		{
			DialogsUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= QuestsCom.size())
	{
		string QuestCom_lc = commandChecker.substr(0, QuestsCom.size());
		if (QuestCom_lc == QuestsCom)
		{
			QuestUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= MhudCom.size())
	{
		string MhudCom_lc = commandChecker.substr(0, MhudCom.size());
		if (MhudCom_lc == MhudCom)
		{
			MhudUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= TradeCom.size())
	{
		string TrdCom_lc = commandChecker.substr(0, TradeCom.size());
		if (TrdCom_lc == TradeCom)
		{
			TradeUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//========================================================================================
	if (commandChecker.size() >= CommonUiCom.size())
	{
		string CommonCom_lc = commandChecker.substr(0, CommonUiCom.size());
		if (CommonCom_lc == CommonUiCom)
		{
			CommonUIFSRealization(strCommand, strArgs);
			return;
		}
	}
	//----------------------------------------------------------
	if (!strcmp(strCommand, "AbilitySel"))
	{
		int id = static_cast<int>(atoi(strArgs));
		if (id != m_CharacterSkillSelectedId)
		{
			m_CharacterSkillSelectedId = id;
			ShowPerkInfo(true);
		}
	}
	else if (!strcmp(strCommand, "AbilityDesel"))
	{
		m_CharacterSkillSelectedId = -1;
		ShowPerkInfo(false);
	}
	else if (!strcmp(strCommand, "AbilityPressed"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		int real_id = static_cast<int>(atoi(strArgs));
		nwAction->AbilityPressed(real_id);
		
	}
	else if (!strcmp(strCommand, "OpenChAblM"))
	{
		/*m_CharacterMenuVisible = false;
		m_animCharacterMenu.SetVisible(false);
		SAFE_HUD_FUNC(ShowChPerks(true));*/
	}
	else if (!strcmp(strCommand, "questsUIClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowQuestSystem(false);
	}
	else if (!strcmp(strCommand, "DialogClose"))
	{
		if (!m_TimerDialogUIDisablingBB)
		{
			m_TimerDialogUIDisablingBB = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::DisablingCurrentDialogUI), NULL);
		}
	}
	else if (!strcmp(strCommand, "AbilitesMenuClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowChPerks(false);
	}
	else if (!strcmp(strCommand, "AbilitesMenuShow"))
	{
		StartLoadingAblUI();
	}
	else if (!strcmp(strCommand, "STR+"))
	{
		Str_pl();
	}
	else if (!strcmp(strCommand, "STR-"))
	{
		Str_min();
	}
	else if (!strcmp(strCommand, "AGL+"))
	{
		Agl_pl();
	}
	else if (!strcmp(strCommand, "AGL-"))
	{
		Agl_min();
	}
	else if (!strcmp(strCommand, "INT+"))
	{
		Int_pl();
	}
	else if (!strcmp(strCommand, "INT-"))
	{
		Int_min();
	}
	else if (!strcmp(strCommand, "WILL+"))
	{
		Wlp_pl();
	}
	else if (!strcmp(strCommand, "WILL-"))
	{
		Wlp_min();
	}
	else if (!strcmp(strCommand, "ENDR+"))
	{
		Endr_pl();
	}
	else if (!strcmp(strCommand, "ENDR-"))
	{
		Endr_min();
	}
	else if (!strcmp(strCommand, "PERS+"))
	{
		Pers_pl();
	}
	else if (!strcmp(strCommand, "PERS-"))
	{
		Pers_min();
	}
	else if (!strcmp(strCommand, "MAXHP+"))
	{
		Hp_pl();
	}
	else if (!strcmp(strCommand, "MAXHP-"))
	{
		Hp_min();
	}
	else if (!strcmp(strCommand, "MAXSTMN+"))
	{
		St_pl();
	}
	else if (!strcmp(strCommand, "MAXSTMN-"))
	{
		St_min();
	}
	else if (!strcmp(strCommand, "MAXMGC+"))
	{
		Mp_pl();
	}
	else if (!strcmp(strCommand, "MAXMGC-"))
	{
		Mp_min();
	}
	else if (!strcmp(strCommand, "CamSetX-"))
	{
		g_pGameCVars->cl_tpvLeftDef -= g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvLeft -= g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetX+"))
	{
		g_pGameCVars->cl_tpvLeftDef += g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvLeft += g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetY-"))
	{
		g_pGameCVars->cl_tpvDistDef -= g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvDist -= g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetY+"))
	{
		g_pGameCVars->cl_tpvDistDef += g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvDist += g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetZ-"))
	{
		g_pGameCVars->cl_tpvUpDef -= g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvUp -= g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetZ+"))
	{
		g_pGameCVars->cl_tpvUpDef += g_pGameCVars->cl_tpvCs_spd;
		g_pGameCVars->cl_tpvUp += g_pGameCVars->cl_tpvCs_spd;
	}
	else if (!strcmp(strCommand, "CamSetDef"))
	{
		g_pGameCVars->cl_tpvUpDef = -0.1f;
		g_pGameCVars->cl_tpvUp = -0.1f;
		g_pGameCVars->cl_tpvDistDef = -0.7f;
		g_pGameCVars->cl_tpvDist = -0.7f;
		g_pGameCVars->cl_tpvLeftDef = 0.4f;
		g_pGameCVars->cl_tpvLeft = 0.4f;
	}
	else if (!strcmp(strCommand, "CamCnSpeed"))
	{
		/*if(g_pGameCVars->cl_tpvCs_spd < 0.05)
		{
		g_pGameCVars->cl_tpvCs_spd += 0.01;
		}*/
		float cmod(g_pGameCVars->cl_tpvCs_spd);

		cmod = (cmod + 0.01f);
		g_pGameCVars->cl_tpvCs_spd = cmod;
		if (g_pGameCVars->cl_tpvCs_spd > 0.06f)
		{
			g_pGameCVars->cl_tpvCs_spd -= 0.04f;
		}

	}
	else if (!strcmp(strCommand, "onDialogLinePressed"))
	{
		int questionid = atoi(strArgs);
		DialogLinePressed(questionid);

	}
	else if (!strcmp(strCommand, "quickSlotOnPress"))
	{
		int slot_id = atoi(strArgs);
		if (slot_id < 0)
			return;

		slot_id += 1;
		CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pPlayer)
			return;

		if (AnyInteractiveUIOpened())
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			if (Inv_loaded)
			{
				CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pGameGlobals->current_selected_Item));
				if (pItem)
				{
					CActor::AcQuickSlot pQSlot;
					pQSlot.slot_type = eQSType_Item;
					pQSlot.slot_id = slot_id;
					pQSlot.obj_in_slot_id = pGameGlobals->current_selected_Item;
					pPlayer->SetQuickSlotInfo(slot_id, pQSlot);
				}
			}
			else if (m_SpellSelectionVisible)
			{
				CGameXMLSettingAndGlobals::SActorSpell act_spell = pPlayer->GetSpellInfoFromActorSB(pGameGlobals->current_selected_spell_ids);
				if (act_spell.spell_id == -1)
					return;

				CActor::AcQuickSlot pQSlot;
				pQSlot.slot_type = eQSType_Spell;
				pQSlot.slot_id = slot_id;
				pQSlot.obj_in_slot_id = act_spell.spell_id;
				pPlayer->SetQuickSlotInfo(slot_id, pQSlot);
			}
			else
			{

			}
		}
		else
		{
			pPlayer->UseQuickSlot(slot_id);
			//timer_to_can_item_press = 0.1f;
		}
		UpdateQuickSelectionMenu();
	}
	else if (!strcmp(strCommand, "quickSlotOnSelect"))
	{

	}
	else if (!strcmp(strCommand, "quickSlotOnDeselect"))
	{

	}
}

void CHUDCommon::OnHardwareMouseEvent(int iX,int iY,EHARDWAREMOUSEEVENT eHardwareMouseEvent, int wheelDelta = 0)
{
	
}

bool CHUDCommon::OnAction(const ActionId& action, int activationMode, float value)
{
	const CGameActions &rGameActions = g_pGame->Actions();
	//CryLogAlways("Context menu pressed2");
	if (action == rGameActions.hud_mouserightbtndown)
	{
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (!pGameGlobals)
			return false;

		//CryLogAlways("Context menu pressed3");
		IUIElement* pElement_Inv = gEnv->pFlashUI->GetUIElement("Inv");
		if (pElement_Inv && !Inv_cont_menu_loaded && !start_drag)
		{
			//CryLogAlways("Context menu pressed4");
			if (Inv_loaded)
			{
				//CryLogAlways("Context menu pressed5");
				if (pGameGlobals->current_selected_Item > 0)
				{
					//CryLogAlways("Context menu pressed6");
					float x, y = 0.0f;
					gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&x, &y);
					int iX((int)x), iY((int)y);
					pElement_Inv->GetFlashPlayer()->ScreenToClient(iX, iY);
					SFlashVarValue args[2] = { iX, iY };

					EntityId idrw = pGameGlobals->current_selected_Item;

					bool dropable = false;
					bool closeable = true;
					bool destroyable = false;
					bool equipable = false;
					bool deequipable = false;
					bool enchantable = false;
					bool useable = false;
					bool haveammo = false;
					bool combineable = false;
					bool isBook = false;
					bool isReadAbleBook = false;
					int num_buttions = 1;

					CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(idrw));

					CWeapon* pWeapon = static_cast<CWeapon*>(pItem->GetIWeapon());
					int idrf = 0;
					IFireMode *pFmd = NULL;
					if (pWeapon)
					{
						int idrf = pItem->GetIWeapon()->GetCurrentFireMode();
						IFireMode *pFmd = pItem->GetIWeapon()->GetFireMode(idrf);
						if (pWeapon && pFmd && pWeapon->GetAmmoCount(pFmd->GetAmmoType()) > 0)
						{
							num_buttions += 1;
							haveammo = true;
						}
					}

					if (pItem->CanDrop())
					{
						num_buttions += 1;
						dropable = true;
					}
					if (pItem->GetEquipped() == 0 && (pItem->CanSelect() || pItem->GetArmorType() > 0 || pItem->GetItemType() == 4 || pItem->GetItemType() == 9 || pItem->GetItemType() == 10 || pItem->GetShield() == 1))
					{
						num_buttions += 1;
						equipable = true;
					}
					if (pItem->GetItemType() == 3 || pItem->GetItemType() == 5)
					{
						num_buttions += 1;
						useable = true;
						if (pItem->GetBookNumPages() > 0 || pItem->GetItemType() == 5)
						{
							isBook = true;
						}

						if (pItem->GetBookNumPages() > 0)
						{
							isReadAbleBook = true;
						}
					}
					if (pItem->GetEquipped() == 1)
					{
						num_buttions += 1;
						deequipable = true;
					}
					if (pItem->GetCombineAble() == 1)
					{
						num_buttions += 1;
						combineable = true;
					}


					if (closeable)
					{
						//CryLogAlways("Context menu pressed7");
						pElement_Inv->GetFlashPlayer()->Pause(true);
						pElement_Inv->GetFlashPlayer()->SetFSCommandHandler(NULL);
						DelayActionForPlayer();
						ShowInvContextMenu(true);
						IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
						if (pInvContextMenu)
						{
							Vec2i pscsr = GetEntityPositionOnScreenInUIElementWoProjection(pInvContextMenu,
								current_mouse_world_pos_projected, Vec3(ZERO), false);

							float fUselessSize = 0.0f;
							if (gEnv->pRenderer)
							{
								float fMovieWidth = (float)pInvContextMenu->GetFlashPlayer()->GetWidth();
								float fMovieHeight = (float)pInvContextMenu->GetFlashPlayer()->GetHeight();
								float fRendererWidth = (float)gEnv->pRenderer->GetWidth();
								float fRendererHeight = (float)gEnv->pRenderer->GetHeight();
								float fScale = fMovieHeight / fRendererHeight;
								fUselessSize = fMovieWidth - fRendererWidth * fScale;
							}
							args[0] = pscsr.x + fUselessSize;
							args[1] = pscsr.y;
							pInvContextMenu->GetFlashPlayer()->Invoke("setPos", args, 2);

							for (int i = 0; i < num_buttions; i++)
							{
								if (closeable)
								{
									SFlashVarValue argD[3] = { i, "Close me", 0 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									closeable = false;
									continue;
								}
								if (dropable)
								{
									SFlashVarValue argD[3] = { i, "Drop me", 1 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									dropable = false;
									continue;
								}
								if (equipable)
								{
									SFlashVarValue argD[3] = { i, "Equip me", 3 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									equipable = false;
									continue;
								}
								if (destroyable)
								{
									SFlashVarValue argD[3] = { i, "Destroy me", 2 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									destroyable = false;
									continue;
								}
								if (useable)
								{
									SFlashVarValue argD[3] = { i, "Use me", 6 };
									if (isBook)
									{
										argD[1] = "Read me";
									}

									if (isReadAbleBook)
									{
										argD[2] = 10;
									}
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									useable = false;
									continue;
								}
								if (deequipable)
								{
									SFlashVarValue argD[3] = { i, "Deequip me", 4 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									deequipable = false;
									continue;
								}
								if (haveammo)
								{
									SFlashVarValue argD[3] = { i, "Unload ammo", 7 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									haveammo = false;
									continue;
								}
								if (combineable)
								{
									SFlashVarValue argD[3] = { i, "Combine", 8 };
									pInvContextMenu->GetFlashPlayer()->Invoke("addContextLine", argD, 3);
									combineable = false;
									continue;
								}
							}
							return true;
						}
					}
				}
			}
		}
		else if (Inv_cont_menu_loaded && Inv_loaded)
		{
		//	CryLogAlways("Context menu pressed8");
			IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
			if (pInvContextMenu && pElement_Inv)
			{
				//CryLogAlways("Context menu pressed9");
				DisableContextMenu();
			}
		}
	}
	else if (action == rGameActions.hud_mouseleftbtndown)
	{
		if (activationMode == eAAM_OnRelease)
		{
			if (player_doll_clip_pressed)
			{
				player_doll_clip_pressed_pos_old = Vec2(0, 0);
				player_doll_clip_pressed = false;
				player_doll_clip_pressed_pos = Vec2(0, 0);
			}
		}
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::Update()
{
	//return;

	if (!gEnv->pFlashUI)
		return;

	if (post_initialization_timer > 0.0f)
	{
		if (!m_PostInitScreenVisible)
			ShowPostInitialization(true);

		if (gEnv->pTimer->GetFrameTime() < 0.04f)
			post_initialization_timer -= gEnv->pTimer->GetFrameTime();
		else
			post_initialization_timer -= 0.04f;

		if (post_initialization_timer <= 0.0f)
		{
			if (m_PostInitScreenVisible)
				ShowPostInitialization(false);

			//CryLogAlways("ShowPostInitialization(false) called");
			post_initialization_timer = -1.0f;
		}

		if (!gEnv->IsEditor() || g_pGameCVars->g_enable_postinitialization < 1)
			return;
	}

	if (m_PostInitScreenVisible)
	{
		return;
	}

	if (m_GamePausedIU && MainHud_loaded)
	{
		m_GamePausedIU = false;
	}

	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	if (pMain_hud)
	{
		if (MainHud_loaded)
		{
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pActor)
				return;

			if (pActor->GetHealth() > 0.0f && m_DeathScreenVisible)
			{
				ShowOnDeathScreen(false);
			}

			float time_to_start_elem_to_al = 3.0f;
			float time_to_elem_to_al_conv = time_to_start_elem_to_al + 110.0f;
			float time_to_elem_to_al_mult = 15.0f;

			float fHealth = (pActor->GetHealth() / (float)pActor->GetMaxHealth()) * 100.0f + 1.0f;
			float fHealthCur = (pActor->GetHealth()) * 1.0f;

			if (0 != fHealth)
			{
				SFlashVarValue args[1] = { (int)fHealth };
				pMain_hud->GetFlashPlayer()->Invoke("setHealth", args, 1);
			}

			if (old_main_player_values[0] != fHealth)
			{
				old_main_player_values[0] = fHealth;
				player_values_alpha_timers[0] = 0.0f;
				SFlashVarValue args[1] = { 100.0f };
				pMain_hud->GetFlashPlayer()->Invoke("setHealthBarAlpha", args, 1);
			}
			else
			{
				if (player_values_alpha_timers[0] < time_to_start_elem_to_al)
				{
					player_values_alpha_timers[0] += gEnv->pTimer->GetFrameTime();
				}
				else if(player_values_alpha_timers[0] >= time_to_start_elem_to_al && player_values_alpha_timers[0] < time_to_elem_to_al_conv)
				{
					player_values_alpha_timers[0] += gEnv->pTimer->GetFrameTime() * time_to_elem_to_al_mult;
					float al_time = 100.0f - (player_values_alpha_timers[0] - time_to_start_elem_to_al);
					if (al_time < 0.0f)
						al_time = 0.0f;

					SFlashVarValue args[1] = { al_time };
					pMain_hud->GetFlashPlayer()->Invoke("setHealthBarAlpha", args, 1);
				}
			}

			float fStamina = (pActor->GetStamina() / (float)pActor->GetMaxStamina()) * 100.0f + 1.0f;
			float fStaminaCur = (pActor->GetStamina()) * 1.0f;

			if (0 != fStamina)
			{
				SFlashVarValue args[1] = { (int)fStamina };
				pMain_hud->GetFlashPlayer()->Invoke("setStamina", args, 1);
			}

			if ((int)old_main_player_values[1] != (int)fStamina)
			{
				old_main_player_values[1] = fStamina;
				player_values_alpha_timers[1] = 0.0f;
				SFlashVarValue args[1] = { 100.0f };
				pMain_hud->GetFlashPlayer()->Invoke("setStaminaBarAlpha", args, 1);
			}
			else
			{
				if (player_values_alpha_timers[1] < time_to_start_elem_to_al)
				{
					player_values_alpha_timers[1] += gEnv->pTimer->GetFrameTime();
				}
				else if (player_values_alpha_timers[1] >= time_to_start_elem_to_al && player_values_alpha_timers[1] < time_to_elem_to_al_conv)
				{
					player_values_alpha_timers[1] += gEnv->pTimer->GetFrameTime() * time_to_elem_to_al_mult;
					float al_time = 100.0f - (player_values_alpha_timers[1] - time_to_start_elem_to_al);
					if (al_time < 0.0f)
						al_time = 0.0f;

					SFlashVarValue args[1] = { al_time };
					pMain_hud->GetFlashPlayer()->Invoke("setStaminaBarAlpha", args, 1);
				}
			}

			float fMagicka = (pActor->GetMagicka() / (float)pActor->GetMaxMagicka()) * 100.0f + 1.0f;
			float fMagickaCur = (pActor->GetMagicka()) * 1.0f;

			if (0 != fMagicka)
			{
				SFlashVarValue args[1] = { (int)fMagicka };
				pMain_hud->GetFlashPlayer()->Invoke("setMagicka", args, 1);
			}

			if ((int)old_main_player_values[2] != (int)fMagicka)
			{
				old_main_player_values[2] = fMagicka;
				player_values_alpha_timers[2] = 0.0f;
				SFlashVarValue args[1] = { 100.0f };
				pMain_hud->GetFlashPlayer()->Invoke("setMagickaBarAlpha", args, 1);
			}
			else
			{
				if (player_values_alpha_timers[2] < time_to_start_elem_to_al)
				{
					player_values_alpha_timers[2] += gEnv->pTimer->GetFrameTime();
				}
				else if (player_values_alpha_timers[2] >= time_to_start_elem_to_al && player_values_alpha_timers[2] < time_to_elem_to_al_conv)
				{
					player_values_alpha_timers[2] += gEnv->pTimer->GetFrameTime() * time_to_elem_to_al_mult;
					float al_time = 100.0f - (player_values_alpha_timers[2] - time_to_start_elem_to_al);
					if (al_time < 0.0f)
						al_time = 0.0f;

					SFlashVarValue args[1] = { al_time };
					pMain_hud->GetFlashPlayer()->Invoke("setMagickaBarAlpha", args, 1);
				}
			}

			if (pActor->IsPlayer())
			{
				CPlayer * pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(gEnv->pGame->GetIGameFramework()->GetClientActorId()));
				float fOxlvl = (pPlayer->GetOxLvl() * 100.0f + 1.0f);
				if (0 != fOxlvl)
				{
					SFlashVarValue args[1] = { fOxlvl };
					pMain_hud->GetFlashPlayer()->Invoke("setBreath", args, 1);
				}

				if (old_main_player_values[3] != fOxlvl)
				{
					old_main_player_values[3] = fOxlvl;
					player_values_alpha_timers[3] = 0.0f;
					SFlashVarValue args[1] = { 100.0f };
					pMain_hud->GetFlashPlayer()->Invoke("setBreathBarAlpha", args, 1);
				}
				else
				{
					time_to_start_elem_to_al = 2.0f;
					time_to_elem_to_al_conv = time_to_start_elem_to_al + 110.0f;
					time_to_elem_to_al_mult = 25.0f;

					if (player_values_alpha_timers[3] < time_to_start_elem_to_al)
					{
						player_values_alpha_timers[3] += gEnv->pTimer->GetFrameTime();
					}
					else if (player_values_alpha_timers[3] >= time_to_start_elem_to_al && player_values_alpha_timers[3] < time_to_elem_to_al_conv)
					{
						player_values_alpha_timers[3] += gEnv->pTimer->GetFrameTime() * time_to_elem_to_al_mult;
						float al_time = 100.0f - (player_values_alpha_timers[3] - time_to_start_elem_to_al);
						if (al_time < 0.0f)
							al_time = 0.0f;

						SFlashVarValue args[1] = { al_time };
						pMain_hud->GetFlashPlayer()->Invoke("setBreathBarAlpha", args, 1);
					}
				}
			}
		}
	}

	IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
	if (pChrMenu_hud)
	{
		if (ChrMenu_loaded)
		{
			UpdateMaxHealth();
			UpdateMaxStamina();
			UpdateMaxMagicka();
			UpdateStrength();
			UpdateAgility();
			UpdateWillpower();
			UpdateEndurance();
			UpdatePersonality();
			UpdateIntelligence();
			UpdateLevel();
		}
	}

	CUIManager* pHud = g_pGame->GetUI();
	if (pHud)
	{
		CHUDSilhouettes* pHSilhol = pHud->GetSilhouettes();
		if (pHSilhol)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			if (g_pGameCVars->g_player_current_interactive_silholette > 0)
			{
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(nwAction->CurrentInteractiveId));
				if (pItem)
				{
					bool enable_shl = true;
					CItem *ppItem = (CItem*)pItem;
					if (ppItem)
					{
						if (ppItem->GetOwnerActor())
							enable_shl = false;
					}

					if (old_silhol_ent > 0 && old_silhol_ent != nwAction->CurrentInteractiveId)
					{
						pHSilhol->ResetSilhouette(old_silhol_ent);
						old_silhol_ent = 0;
					}

					if (enable_shl)
					{
						pHSilhol->SetSilhouette(pItem, 1.0f, 0.3f, 1.0f, 0.001f, 0.3f, false);
						old_silhol_ent = nwAction->CurrentInteractiveId;
					}
				}
				else if (old_silhol_ent != nwAction->CurrentInteractiveId)
					pHSilhol->ResetSilhouette(old_silhol_ent);
			}
			else if (g_pGameCVars->g_player_current_interactive_silholette > 1)
			{
				IActor *pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(nwAction->CurrentInteractiveId);
				if (pActor)
				{
					pHSilhol->SetSilhouette(pActor, 1.0f, 0.3f, 1.0f, 0.001f, 0.3f, false, false);
					old_silhol_ent = nwAction->CurrentInteractiveId;
				}
				else if (old_silhol_ent != nwAction->CurrentInteractiveId)
					pHSilhol->ResetSilhouette(old_silhol_ent);
			}
		}
	}

	if (start_drag)
	{
		DraggedItemUpdatePos();
	}

	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return;

	if (request_for_delayed_update_player_doll)
	{
		if (delayed_update_player_doll_timer > 0.8f)//need more in one time//four frames
		{
			request_for_delayed_update_player_doll = false;
			delayed_update_player_doll_timer = 0.0f;

			if (id_hud_pl > 0)
				nwAction->InventoryUpdatePlayerDoll(id_hud_pl);
		}

		delayed_update_player_doll_timer += 0.1f;
	}

	if (!request_for_delayed_update_player_doll && Inv_loaded)
	{
		if (id_hud_pl > 0)
			nwAction->InventoryUpdateObjectAttachmentsOnPLdoll(id_hud_pl);
	}

	if (player_doll_clip_pressed)
	{
		//CryLogAlways("process rot pl model hud");
		CMenuRender3DModelMgr* pModelManager = CMenuRender3DModelMgr::GetInstance();
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(id_hud_pl);
		float xl, yl = 0.0f;
		gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&xl, &yl);
		if (player_doll_clip_pressed_pos_old.x < xl && pEntity)
		{
			float rt_rnd = (xl - player_doll_clip_pressed_pos_old.x)*2.0f;
			//CryLogAlways("process rot pl model hud 1");
			/*Quat lcvr = pEntity->GetRotation();
			lcvr.SetRotationZ(lcvr.GetRotZ()+0.01f);
			pEntity->SetRotation(lcvr);*/
			Ang3 lcvr = pModelManager->GetInstRndRotation(characterModelIndex);
			lcvr.z += 0.01f * rt_rnd;
			pModelManager->SetInstRndRotation(characterModelIndex, lcvr);
		}
		else if (player_doll_clip_pressed_pos_old.x > xl && pEntity)
		{
			float rt_rnd = (player_doll_clip_pressed_pos_old.x - xl)*2.0f;
			//CryLogAlways("process rot pl model hud 2");
			/*Quat lcvr = pEntity->GetRotation();
			lcvr.SetRotationZ(lcvr.GetRotZ() - 0.01f);
			pEntity->SetRotation(lcvr);*/
			Ang3 lcvr = pModelManager->GetInstRndRotation(characterModelIndex);
			lcvr.z -= 0.01f * rt_rnd;
			pModelManager->SetInstRndRotation(characterModelIndex, lcvr);
		}
		player_doll_clip_pressed_pos_old = Vec2(xl, yl);
	}

	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (pBuffsScreenInfo)
	{
		if (BuffInfo_loaded)
		{
			if (sd_ui_timers[0] > 0.0f)
			{
				sd_ui_timers[0] -= gEnv->pTimer->GetFrameTime();
				if(sd_ui_timers[0] <= 0.0f)
					sd_ui_timers[0] = 0.0f;
			}

			if (sd_ui_timers[0] == 0.0f)
			{
				if (CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor()))
				{
					sd_ui_timers[0] = -1.0f;
					if (!pActor->m_timed_effects_buffs.empty())
					{
						std::vector<CActor::SActorTimedEffectsFtg>::iterator it = pActor->m_timed_effects_buffs.begin();
						std::vector<CActor::SActorTimedEffectsFtg>::iterator end = pActor->m_timed_effects_buffs.end();
						int buffs_count = pActor->m_timed_effects_buffs.size();
						for (int i = 0; i < buffs_count, it != end; i++, ++it)
						{
							CActor::SActorTimedEffectsFtg effect = *it;
							if (effect.buff_id > 0)
							{
								
								//pActor->AddBuff(effect.buff_id, effect.buff_time, effect.effect_id);
								CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
								if (pGameGlobals)
								{
									SActorBuff* pActorBuff = NULL;
									pActorBuff = pGameGlobals->GetCharacterBuffInfo(effect.buff_id);
									if (pActorBuff)
									{
										AddBuffOrDebuff(pActorBuff->buff_id, pActorBuff->buff_icon_path.c_str(), pActor->GetBuffTime(effect.buff_id), pActorBuff->buff_type);
									}
									delete(pActorBuff);
									pActorBuff = NULL;
								}
								//CryLogAlways("CHUDCommon::Update try to add visual dorb effect on post serial event");
							}
						}
					}
				}
			}
			ITimeOfDay* pTd = gEnv->p3DEngine->GetTimeOfDay();
			if (pTd)
			{
				float timeH = pTd->GetTime();
				float timeM = 0.0f;
				float timeS = 0.0f;
				timeM = (pTd->GetTime() * 60) - (int)timeH * 60;
				timeS = ((pTd->GetTime() * 60) * 60) - (int)(timeH * 60) * 60;
				SFlashVarValue argH[1] = { (int)timeH };
				SFlashVarValue argM[1] = { (int)timeM };
				SFlashVarValue argS[1] = { (int)timeS };
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeH", argH, 1);
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeM", argM, 1);
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeS", argS, 1);
			}
		}
	}

	if (MainHud_loaded)
	{
		if (gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			if (!m_MinimapVisible && MainHud_loaded)
			{
				//minimap hud
				ShowMinimap(!m_MinimapVisible);
			}
			else if (m_MinimapVisible && !MainHud_loaded)
			{
				if(!AnyInteractiveUIOpened())
					ShowMinimap(!m_MinimapVisible);
				else if(!m_CharacterCreationVisible)
					ShowMinimap(!m_MinimapVisible, true);
				else
					ShowMinimap(!m_MinimapVisible);
			}
		}

		if (m_MinimapVisible)
		{
			IUIElement* pMinimap = gEnv->pFlashUI->GetUIElement("MinimapUI");
			if (pMinimap)
			{
				CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
				if (pActor)
				{
					float player_rot = RAD2DEG(pActor->GetEntity()->GetRotation().GetRotZ());
					SFlashVarValue argRot[1] = { player_rot };
					pMinimap->GetFlashPlayer()->Invoke("setMapRot", argRot, 1);
					float player_pos_x = (pActor->GetEntity()->GetPos().x / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_x;
					float player_pos_y = (pActor->GetEntity()->GetPos().y / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_y;
					SFlashVarValue argPos[2] = { player_pos_x, player_pos_y };
					pMinimap->GetFlashPlayer()->Invoke("setMapPos", argPos, 2);
				}
			}
			UpdateObjectsMarkersOnMinimap(gEnv->pTimer->GetFrameTime());
		}

		

		if (SimpleCrosshair_loaded)
		{
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (pActor)
			{
				if (pActor->GetCurrentItemId() != old_player_selected_item)
				{
					old_player_selected_item = pActor->GetCurrentItemId();
					if(g_pGameCVars->g_playerRotViewMode <= 0)
						ShowSimpleCrosshair(true, true);
				}
			}
		}

		if (SelectedWeaponsInfo_loaded)
		{
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (pActor)
			{
				if (pActor->GetCurrentItemId() != old_player_selected_item2)
				{
					ShowSelectedWeapons(true, false);
				}
				if (pActor->GetAnyItemInLeftHand() != old_player_selected_item_left)
				{
					ShowSelectedWeapons(true, false);
				}
				CGameXMLSettingAndGlobals::SActorSpell stSpell = pActor->GetSpellFromCurrentSpellsForUseSlot(1);
				if (stSpell.spell_id != -1)
				{
					if (stSpell.spell_id != old_player_selected_spell_id)
					{
						ShowSelectedWeapons(true, false);
					}
				}
				else
				{
					if (stSpell.spell_id != old_player_selected_spell_id)
					{
						ShowSelectedWeapons(true, false);
					}
				}
				CGameXMLSettingAndGlobals::SActorSpell stSpell_left = pActor->GetSpellFromCurrentSpellsForUseSlot(2);
				if (stSpell_left.spell_id != -1)
				{
					if (stSpell_left.spell_id != old_player_selected_spell_id_left)
					{
						ShowSelectedWeapons(true, false);
					}
				}
				else
				{
					if (stSpell_left.spell_id != old_player_selected_spell_id_left)
					{
						ShowSelectedWeapons(true, false);
					}
				}

				IUIElement* pSelectedWeaponsInfo = gEnv->pFlashUI->GetUIElement("SelectedWeaponsInfo");
				if (pSelectedWeaponsInfo)
				{
					CItem *curItem = pActor->GetItem(pActor->GetAnyItemInLeftHand());
					if (curItem)
					{
						float hp = curItem->GetItemHealth();
						int max_hp = curItem->GetItemMaxHealth();
						float fItemHp = (hp / (float)max_hp) * 100.0f + 1.0f;
						SFlashVarValue argItHp[2] = { 1, fItemHp };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetIndicPercents", argItHp, 2);
					}
					CItem *curItem2 = pActor->GetItem(pActor->GetCurrentItemId());
					if (curItem2)
					{
						if (pActor->GetCurrentItemId() != pActor->GetNoWeaponId())
						{
							float hp = curItem2->GetItemHealth();
							int max_hp = curItem2->GetItemMaxHealth();
							float fItemHp = (hp / (float)max_hp) * 100.0f + 1.0f;
							SFlashVarValue argItHp[2] = { 0, fItemHp };
							pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetIndicPercents", argItHp, 2);
						}
					}
					CActorSpellsActions *pSpellAction = pActor->m_pActorSpellsActions;
					if (pSpellAction)
					{
						if (stSpell.spell_id != -1)
						{
							float current_time_to_use = pSpellAction->current_cast_slots_charge_timers[1];
							int max_time_to_use = stSpell.spell_cast_max_time_to_charge;
							if (stSpell.spell_chargeable)
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers[1];
							}
							else
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers_rts[1];
								max_time_to_use = stSpell.spell_cast_time_to_can_cast;
							}

							if (stSpell.spell_sub_type == CActorSpellsActions::eSpell_type_cast_continuosly_on_target || 
								stSpell.spell_sub_type == CActorSpellsActions::eSpell_type_cast_continuosly_on_self)
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers_rts[1];
								max_time_to_use = stSpell.spell_cast_time_to_can_cast;
							}
							float fSpellUsability = (current_time_to_use / (float)max_time_to_use) * 100.0f + 1.0f;
							SFlashVarValue argItMgc[2] = { 2, fSpellUsability };
							pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetIndicPercents", argItMgc, 2);
						}
						if (stSpell_left.spell_id != -1)
						{
							float current_time_to_use = pSpellAction->current_cast_slots_charge_timers[2];
							int max_time_to_use = stSpell_left.spell_cast_max_time_to_charge;
							if (stSpell_left.spell_chargeable)
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers[2];
							}
							else
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers_rts[2];
								max_time_to_use = stSpell_left.spell_cast_time_to_can_cast;
							}

							if (stSpell_left.spell_sub_type == CActorSpellsActions::eSpell_type_cast_continuosly_on_target ||
								stSpell_left.spell_sub_type == CActorSpellsActions::eSpell_type_cast_continuosly_on_self)
							{
								current_time_to_use = pSpellAction->current_cast_slots_timers_rts[2];
								max_time_to_use = stSpell_left.spell_cast_time_to_can_cast;
							}
							float fSpellUsability = (current_time_to_use / (float)max_time_to_use) * 100.0f + 1.0f;
							SFlashVarValue argItMgc[2] = { 3, fSpellUsability };
							pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetIndicPercents", argItMgc, 2);
						}
					}
				}
			}
		}

		if (!SimpleCrosshair_loaded && gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			if (g_pGameCVars->g_playerRotViewMode <= 0)
				ShowSimpleCrosshair(true, false);
		}

		if (!SelectedWeaponsInfo_loaded && gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			ShowSelectedWeapons(true, false);
		}

		if (!SelectedWeaponsInfo_loaded && gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			ShowSelectedWeapons(true, false);
		}

		if (gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			if (!m_OnScreenObjInfoVisible)
			{
				ShowOnScreenObjInfo(true);
			}

			if (!m_InGameHintsVisible)
			{
				ShowHints(true);
			}
		}
	}
	else
	{
		if (SelectedWeaponsInfo_loaded)
		{
			ShowSelectedWeapons(false, false);
		}

		if (SimpleCrosshair_loaded)
		{
			ShowSimpleCrosshair(false, false);
		}

		if (m_MinimapVisible)
		{
			if (!AnyInteractiveUIOpened())
				ShowMinimap(!m_MinimapVisible);
			else if (!m_CharacterCreationVisible)
				ShowMinimap(!m_MinimapVisible, true);
			else
				ShowMinimap(!m_MinimapVisible);
		}

		if (m_OnScreenObjInfoVisible)
		{
			ShowOnScreenObjInfo(false);
		}

		if (m_InGameHintsVisible)
		{
			if (!AnyInteractiveUIOpened())
			{
				ShowHints(false);
			}

			if (g_pGameCVars->hud_disable_ingame_hints_in_dialogs > 0)
			{
				if (m_DialogSystemVisible || m_DialogSystemAltVisible)
					ShowHints(false);
			}
		}
	}

	if (g_pGameCVars->hud_enable_special_deathscreen_on_player_death > 0)
	{
		if (gEnv->pGame && gEnv->pGame->GetIGameFramework())
		{
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (pActor)
			{
				if (pActor->IsPlayer())
				{
					if (pActor->IsDead())
					{
						if (!m_DeathScreenVisible)
						{
							ShowOnDeathScreen(true);
						}
					}
					else
					{
						if (m_DeathScreenVisible)
						{
							ShowOnDeathScreen(false);
						}
					}
				}
			}
		}
	}

	if (timer_to_can_item_press > 0.0f)
	{
		timer_to_can_item_press -= gEnv->pTimer->GetFrameTime();
		if (timer_to_can_item_press <= 0.0f)
			timer_to_can_item_press = 0.0f;
	}
}

void CHUDCommon::PostUpdate()
{
	if (m_PostInitScreenVisible)
		return;

	if (!gEnv->pFlashUI)
		return;

	if (m_OnScreenObjInfoVisible)
	{
		UpdateOnScreenObjInfo();
	}

	if (Inv_loaded && gEnv->pRenderer)
	{
		float m_mouseY, m_mouseX = 0.0f;
		if (gEnv->pHardwareMouse)
		{
			gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&m_mouseX, &m_mouseY);
		}
		int invMouseY = gEnv->pRenderer->GetHeight() - m_mouseY;
		Vec3 vPos0(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 0, &vPos0.x, &vPos0.y, &vPos0.z);

		Vec3 vPos1(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 1, &vPos1.x, &vPos1.y, &vPos1.z);

		Vec3 vDir = vPos1 - vPos0;
		vDir.Normalize();
		vPos0 = vPos0 + (vDir * 10.0f);
		current_mouse_world_pos = vPos0;
		Vec3 screenPos(0, 0, 0);
		gEnv->pRenderer->ProjectToScreen(vPos0.x, vPos0.y, vPos0.z, &screenPos.x, &screenPos.y, &screenPos.z);
		current_mouse_world_pos_projected = screenPos;
	}

	if (g_pGameCVars->fix_enable_hud3dmodels_not_in_post_render > 0)
	{
		IEntity *pHudPlModel = gEnv->pEntitySystem->GetEntity(id_hud_pl);
		if (pHudPlModel && gEnv->pRenderer)
		{
			if (character_model_clip_pose.IsZero())
				return;

			CCamera& cam = GetISystem()->GetViewCamera();
			int                m_mouseX = 0.0f, m_mouseY = 0.0f;
			m_mouseX = int(character_model_clip_pose.x);
			m_mouseY = int(character_model_clip_pose.y);
			//CryLogAlways("HudCommon:Update mouse x = %i, mouse y = %i,", m_mouseX, m_mouseY);
			int invMouseY = gEnv->pRenderer->GetHeight() - m_mouseY;
			Vec3 vPos0(0, 0, 0);
			gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 0.0f, &vPos0.x, &vPos0.y, &vPos0.z);
			//vPos0 = gEnv->pRenderer->UnprojectFromScreen(m_mouseX, invMouseY);
			/*if (vPos0.GetDistance(pHudPlModel->GetPos()) > 655.0f)
			{
				return;
			}*/
			Vec3 vPos1(0, 0, 0);
			//vPos1 = cam.GetPosition();
			vPos1 = cam.GetPosition() + cam.GetViewdir() * 13.0f;
			//gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 0, &vPos1.x, &vPos1.y, &vPos1.z);
			//vPos1 = gEnv->pRenderer->UnprojectFromScreen(m_mouseX, invMouseY);
			/*if (vPos1.GetDistance(pHudPlModel->GetPos()) > 655.0f)
			{
				return;
			}*/
			Vec3 vDir = vPos0 - vPos1;//vPos1 - vPos0;
			vDir.normalize();
			//vDir.Normalize();
			//CryLogAlways("HudCommon:Update vDir x = %f, vDir y = %f, vDir z = %f", vDir.x, vDir.y, vDir.z);
			Vec3 nc_poss = vPos1 + (vDir * 10.0f);
			nc_poss.z = nc_poss.z - 1.0f;//vPos1.z;
			//CryLogAlways("HudCommon:Update pl_molel_pos x = %f, pl_molel_pos y = %f, pl_molel_pos z = %f", nc_poss.x, nc_poss.y, nc_poss.z);
			if (CMenuRender3DModelMgr* pModelManager = CMenuRender3DModelMgr::GetInstance())
			{
				Vec3 VDirect = cam.GetPosition() - nc_poss;
				VDirect.normalize();
				//Quat mdl_old_rot = pHudPlModel->GetRotation();
				//Quat::CreateRotationVDir(VDirect);
				Quat mdl_nv_rot = Quat::CreateRotationVDir(VDirect);
				//mdl_nv_rot.SetRotationZ(mdl_old_rot.GetRotZ());
				//pHudPlModel->SetRotation(mdl_nv_rot);
				Ang3 nvrAngles(mdl_nv_rot);
				pModelManager->SetInstRndPosition(characterModelIndex, nc_poss);
				/*Ang3 rot = pModelManager->GetInstRndRotation(characterModelIndex);
				rot.x = nvrAngles.x;
				rot.y = nvrAngles.y;
				rot.z = nvrAngles.z;
				pModelManager->SetInstRndRotation(characterModelIndex, rot);*/
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::Reset()
{
	if (gEnv->IsDedicated())
		return;

	if (m_MainMapVisible)
	{
		ShowMainMap(false);
	}
	if (ChrMenu_loaded)
	{
		ShowCharacterMenu();
	}
	if (Character_Creation_Menu_Loaded)
	{
		ShowCharacterCreatingMenu();
	}
	if (Inv_loaded)
	{
		if (Inv_cont_menu_loaded)
		{
			ShowInvContextMenu(false);
		}
		ShowInventory();
	}
	if (m_SpellSelectionVisible)
	{
		ShowSpellSelectionMenu(false);
	}
	if (m_DialogSystemAltVisible)
	{
		ShowDialogSystemAlt(false);
	}
	if (m_DialogSystemVisible)
	{
		ShowDialog(false, "", 0);
	}
	if (m_CharacterCreationVisible)
	{
		//ShowCharacterCreatingMenu();
	}
	if (m_BookVisible)
	{
		ShowBook(false, 0);
	}
	if (m_QuestSystemVisible)
	{
		ShowQuestSystem(false);
	}
	if (m_TradeSystemVisible)
	{
		ShowTradeSystem(0, false);
	}
	if (m_CharacterPerksMenuVisible)
	{
		ShowChPerks(false);
	}
	if (m_CharacterSkillsMenuVisible)
	{
		ShowChSkills(false);
	}
	if (m_ChestInvVisible)
	{
		ShowChestInv(false);
	}

	if (m_OnScreenObjInfoVisible)
	{
		ShowOnScreenObjInfo(false);
	}

	if (m_InGameHintsVisible)
	{
		ShowHints(false);
	}

	if (MainHud_loaded)
	{
		ShowMainHud();
	}
	///-------------------------------------------------
	if (SelectedWeaponsInfo_loaded)
	{
		ShowSelectedWeapons(false, false);
	}

	if (SimpleCrosshair_loaded)
	{
		ShowSimpleCrosshair(false, false);
	}

	if (m_MinimapVisible)
	{
		ShowMinimap(!m_MinimapVisible);
	}

	if (m_OnScreenObjInfoVisible)
	{
		ShowOnScreenObjInfo(false);
	}

	if (m_InGameHintsVisible)
	{
		ShowHints(false);
	}
	///-------------------------------------------------
	if (m_DialogSystemVisible)
		ShowDialog(false, "", 0);

	if (m_DialogSystemAltVisible)
		ShowDialogSystemAlt(false);

	if (m_QuickSelectionVisible)
		ShowQuickSelectionMenu(false);

	Inv_loaded = false;	ChrMenu_loaded = false;	MainHud_loaded = false;	Inv_ItemInfo_loaded = false; StatMsg_loaded = false; BuffInfo_loaded = false;
	SpellInfo_loaded = false; m_DropActive = false;	m_DialogSystemVisible = false; m_DialogSystemAltVisible = false; m_ChestInvVisible = false;	m_CharacterEquipmentVisible = false;
	m_Icmenu = false; m_BookVisible = false; m_CharacterCreationVisible = false; m_TradeSystemVisible = false; m_QuestSystemVisible = false; m_SpellSelectionVisible = false;
	m_CharacterPerksMenuVisible = false; m_CharacterSkillsMenuVisible = false; m_CharacterSkillInfoVisible = false;	m_CharacterSkillSelectedId = 0;	m_InteractiveInfoVisible = false;
	m_CM_opened_after_levelup = false; m_fs_tab_open = false; Inv_cont_menu_loaded = false; start_drag = false;	m_str_val_cm = 0; m_agl_val_cm = 0;	m_int_val_cm = 0; m_endr_val_cm = 0;
	m_will_val_cm = 0; m_pers_val_cm = 0; m_Mhp_val_cm = 0;	m_Mst_val_cm = 0; m_Mmg_val_cm = 0;	old_silhol_ent = 0;	current_dragged_item = 0; current_dragged_item_slot = 0;
	item_for_drag_exchandge = 0; slot_for_drag_exchandge = 0; Inv_dragged_item_loaded = false; request_for_delayed_update_player_doll = false; delayed_update_player_doll_timer = 0.0f;
	id_hud_pl = 0; item_drop_request = false; hit_on_player_doll_clip = false; hit_on_drop_clip = false; player_doll_clip_pressed = false; player_doll_clip_pressed_pos = Vec2(0, 0);
	player_doll_clip_pressed_pos_old = Vec2(0, 0); m_MinimapVisible = false; m_MainMapVisible = false;
	for (int i = 0; i < 255; i++)
	{
		questionloaded[i] = false;
	}
	for (int i = 0; i < 2; i++)
	{
		maps_update_timers[i] = 0.0f;
		maps_update_delays[i] = 0.3f;
		sd_ui_timers[i] = 1.3f;
	}
	entites_ont_map_to_update.clear(); static_markers_on_minimap_ids.clear(); SimpleCrosshair_loaded = false; SelectedWeaponsInfo_loaded = false; old_player_selected_item = 0;
	old_player_selected_item2 = 0; old_player_selected_item_left = 0; old_player_selected_spell_id = -1; old_player_selected_spell_id_left = -1;

	entites_ont_screen_to_update.clear();
	target_entites_ont_screen_to_update.clear();

	m_DeathScreenVisible = false;
	m_InGameHintsVisible = false;
	m_OnScreenObjInfoVisible = false;
	m_EnemyHPBarVisible = false;
	m_ArcadeCompassVisible = false;
	m_PostInitScreenVisible = false;
	m_QuickSelectionVisible = false;
}

void CHUDCommon::Serialize(TSerialize ser)
{
	ser.BeginGroup("HUDCommonSerialization");
	ser.BeginGroup("StaticMapMarkers");
	if (ser.IsWriting())
	{
		std::vector<SMapMarker>::iterator it_f = s_MapStaticMarkers.begin();
		std::vector<SMapMarker>::iterator end_f = s_MapStaticMarkers.end();
		int count = s_MapStaticMarkers.size();
		ser.Value("markersCount", count);
		for (int i = 0; i < count, it_f != end_f; i++, ++it_f)
		{
			SMapMarker mark;
			mark = *(it_f);
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = string("Marker_") + C_valstr;
			ser.BeginGroup(A1_valstr.c_str());
			ser.Value("marker_icon", mark.marker_icon);
			ser.Value("marker_id", mark.marker_id);
			ser.Value("marker_name", mark.marker_name);
			ser.Value("marker_pos", mark.marker_pos);
			ser.EndGroup();
		}
	}
	else
	{
		s_MapStaticMarkers.clear();
		int count = 0;
		ser.Value("markersCount", count);
		for (int i = 0; i < count; i++)
		{
			SMapMarker mark;
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = string("Marker_") + C_valstr;
			ser.BeginGroup(A1_valstr.c_str());
			ser.Value("marker_icon", mark.marker_icon);
			ser.Value("marker_id", mark.marker_id);
			ser.Value("marker_name", mark.marker_name);
			ser.Value("marker_pos", mark.marker_pos);
			s_MapStaticMarkers.push_back(mark);
			ser.EndGroup();
		}
	}
	ser.EndGroup();
	ser.EndGroup();
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::ClearChestInventory(bool reload_element)
{
	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (!pLootUI)
		return;
	
	pLootUI->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
	if (reload_element)
	{
		pLootUI->Reload();
	}
}

void CHUDCommon::ClearTradeSystem(bool reload_element)
{
	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	if (pShopInterface)
	{
		pShopInterface->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		if (reload_element)
		{
			pShopInterface->Reload();
		}
	}
}

void CHUDCommon::ClearSpellSelection(bool reload_element)
{
	IUIElement* pSpellSelectionMenu = gEnv->pFlashUI->GetUIElement("SpellSelectionUI");
	if (pSpellSelectionMenu)
	{
		pSpellSelectionMenu->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		if (reload_element)
		{
			pSpellSelectionMenu->Reload();
		}
	}
}

void CHUDCommon::UpdateChestInventory(float frameTime, EntityId chestId)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	EntityId id = 0;
	if(chestId == 0)
		id = pGameGlobals->current_opened_Chest;
	else
		id = chestId;

	CActor *pActorClient = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActorClient)
		return;

	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	if (!pActor)
	{
		pActor = pActorClient;
	}

	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (!pLootUI)
		return;

	if (pLootUI->GetFlashPlayer()->GetVisible())
	{
		pLootUI->GetFlashPlayer()->Advance(frameTime);
		pLootUI->GetFlashPlayer()->Render();
		IInventory *pInventory = pActor->GetInventory();
		if (pActor == pActorClient)
		{
			IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(id);
			if (pEntityc)
			{
				if (pGameGlobals->GetThisContainer(pEntityc->GetId()))
				{
					pInventory = pGameGlobals->GetThisContainer(pEntityc->GetId())->GetInventory();
				}
			}
		}

		if (chest_or_shop_showed_player_inv)
			pInventory = pActorClient->GetInventory();

		if (!pInventory)
			return;

		int plgold = pActorClient->GetGold();
		int trgold = pActor->GetGold();
		SFlashVarValue argGold[1] = { plgold };
		SFlashVarValue argGoldTr[1] = { trgold };
		if(pActor != pActorClient)
			pLootUI->GetFlashPlayer()->Invoke("setChestMoney", argGoldTr, 1);

		pLootUI->GetFlashPlayer()->Invoke("setPlayerMoney", argGold, 1);
		if (current_chest_or_shop_tab_opened == 3)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				EntityId id = curItem->GetHudItemPosPrimary();
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = id;
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				if (g_pGameCVars->hud_container_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pLootUI->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pLootUI->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 0)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->CanSelect())
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; ++i, ++it_pl)
			{
				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				CWeapon *pWeapon = reinterpret_cast<CWeapon *>(pItem->GetIWeapon());
				if (!pWeapon)
					continue;

				if (pItem->CanSelect())
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					if (stt == 0)
					{
						if (curItem->IsSelected())
							stt = 1;
					}
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_container_store_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = curItem->GetItemCost();
						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pLootUI->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pLootUI->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 1)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->GetArmorType() > 0)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));

				if (curItem->GetArmorType() > 0)
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_container_store_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = curItem->GetItemCost();
						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pLootUI->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pLootUI->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 2)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosPrimary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_container_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pLootUI->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pLootUI->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 4)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosSecondary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_container_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pLootUI->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pLootUI->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::UpdateTradeSystem(float frameTime)
{
	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	if (!pShopInterface)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (pShopInterface->GetFlashPlayer()->GetVisible())
	{
		pShopInterface->GetFlashPlayer()->Advance(frameTime);
		pShopInterface->GetFlashPlayer()->Render();
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		CActor *pActorClient = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActorClient)
			return;

		CActor *pActor = static_cast<CActor *>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(current_Seller));
		if (!pActor)
			return;

		int slots_numder_tr = pActor->GetMaxInvSlots();
		IInventory *pInventory = pActor->GetInventory();
		IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(pGameGlobals->current_traider_chest);
		if (pEntityc)
		{
			CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
			if (pChest)
			{
				pInventory = pChest->GetInventory();
				slots_numder_tr = pChest->GetInvSlotsNum();
			}
		}

		if(chest_or_shop_showed_player_inv)
			pInventory = pActorClient->GetInventory();

		if (!pInventory)
			return;

		int plgold = pActorClient->GetGold();
		int trgold = pActor->GetGold();
		SFlashVarValue argGold[1] = { plgold };
		SFlashVarValue argGoldTr[1] = { trgold };
		pShopInterface->GetFlashPlayer()->Invoke("setMerchantMoney", argGoldTr, 1);
		pShopInterface->GetFlashPlayer()->Invoke("setPlayerMoney", argGold, 1);
		if (current_chest_or_shop_tab_opened == 3)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				EntityId id = curItem->GetHudItemPosPrimary();
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = id;
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				if (g_pGameCVars->hud_merchant_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = 0;
					if (chest_or_shop_showed_player_inv)
						itme_val_money = nwAction->CalculateItemGoldCost(pActorClient->GetEntityId(), pActor->GetEntityId(), idIndex);
					else
						itme_val_money = nwAction->CalculateItemGoldCost(pActor->GetEntityId(), pActorClient->GetEntityId(), idIndex);

					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 0)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->CanSelect())
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; ++i, ++it_pl)
			{
				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				CWeapon *pWeapon = reinterpret_cast<CWeapon *>(pItem->GetIWeapon());
				if (!pWeapon)
					continue;

				if (pItem->CanSelect())
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					if (stt == 0)
					{
						if (curItem->IsSelected())
							stt = 1;
					}
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_merchant_store_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = 0;
						if (chest_or_shop_showed_player_inv)
							itme_val_money = nwAction->CalculateItemGoldCost(pActorClient->GetEntityId(), pActor->GetEntityId(), idIndex);
						else
							itme_val_money = nwAction->CalculateItemGoldCost(pActor->GetEntityId(), pActorClient->GetEntityId(), idIndex);

						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pShopInterface->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pShopInterface->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 1)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->GetArmorType() > 0)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));

				if (curItem->GetArmorType() > 0)
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_merchant_store_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = 0;
						if (chest_or_shop_showed_player_inv)
							itme_val_money = nwAction->CalculateItemGoldCost(pActorClient->GetEntityId(), pActor->GetEntityId(), idIndex);
						else
							itme_val_money = nwAction->CalculateItemGoldCost(pActor->GetEntityId(), pActorClient->GetEntityId(), idIndex);

						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pShopInterface->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pShopInterface->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 2)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosPrimary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_merchant_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = 0;
					if (chest_or_shop_showed_player_inv)
						itme_val_money = nwAction->CalculateItemGoldCost(pActorClient->GetEntityId(), pActor->GetEntityId(), idIndex);
					else
						itme_val_money = nwAction->CalculateItemGoldCost(pActor->GetEntityId(), pActorClient->GetEntityId(), idIndex);

					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (current_chest_or_shop_tab_opened == 4)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosSecondary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_merchant_store_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = 0;
					if (chest_or_shop_showed_player_inv)
						itme_val_money = nwAction->CalculateItemGoldCost(pActorClient->GetEntityId(), pActor->GetEntityId(), idIndex);
					else
						itme_val_money = nwAction->CalculateItemGoldCost(pActor->GetEntityId(), pActorClient->GetEntityId(), idIndex);

					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pShopInterface->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
	}
}

void CHUDCommon::UpdateSpellSelection(float frameTime)
{
	IUIElement* pSpellSelectionMenu = gEnv->pFlashUI->GetUIElement("SpellSelectionUI");
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	if (pSpellSelectionMenu->GetFlashPlayer()->GetVisible())
	{
		//current_spell_selection_tab_opened;
		pSpellSelectionMenu->GetFlashPlayer()->Advance(frameTime);
		pSpellSelectionMenu->GetFlashPlayer()->Render();
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pActor->m_actor_spells_book.begin();
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pActor->m_actor_spells_book.end();
		int count = pActor->m_actor_spells_book.size();
		int selected_spells_setuped = 0;
		int spells_counter = 0;
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if ((*it).spell_id > -1)
			{
				int selected = 0;
				if (selected_spells_setuped < 2)
				{
					if ((*it).spell_id == pActor->GetSpellIdFromCurrentSpellsForUseSlot(1))
					{
						selected_spells_setuped += 1;
						selected = 1;
						SFlashVarValue args_eq[3] = { "left_spell", (*it).spell_icon_path[2].c_str(), (*it).spell_name.c_str() };
						pSpellSelectionMenu->GetFlashPlayer()->Invoke("addEquippedSpell", args_eq, 3);
					}

					if ((*it).spell_id == pActor->GetSpellIdFromCurrentSpellsForUseSlot(2))
					{
						selected_spells_setuped += 1;
						selected = 1;
						SFlashVarValue args_eq[3] = { "right_spell", (*it).spell_icon_path[2].c_str(), (*it).spell_name.c_str() };
						pSpellSelectionMenu->GetFlashPlayer()->Invoke("addEquippedSpell", args_eq, 3);
					}
				}

				if (current_spell_selection_tab_opened == 3)
				{
					
				}
				else if (current_spell_selection_tab_opened == 0)
				{
					if ((*it).spell_sub_type != 1 && (*it).spell_sub_type != 6)
						continue;
				}
				else if (current_spell_selection_tab_opened == 1)
				{
					if ((*it).spell_sub_type != 2 && (*it).spell_sub_type != 3 && (*it).spell_sub_type != 5)
						continue;
				}
				else if (current_spell_selection_tab_opened == 2)
				{
					if ((*it).spell_sub_type != 4)
						continue;
				}
				else if (current_spell_selection_tab_opened == 4)
				{

				}
				

				if (g_pGameCVars->hud_spells_selection_type == 1)
				{
					SFlashVarValue args[6] = { spells_counter, (*it).spell_icon_path[1].c_str(), (*it).spell_name.c_str(), selected, (*it).spell_description[2].c_str(), (*it).spell_mana_cost };
					pSpellSelectionMenu->GetFlashPlayer()->Invoke("addSpellListLine", args, 6);
				}
				else
				{
					SFlashVarValue args[4] = { spells_counter, (*it).spell_icon_path[1].c_str(), (*it).spell_name.c_str(), selected };
					pSpellSelectionMenu->GetFlashPlayer()->Invoke("addSpellVisual", args, 4);
				}
				spells_counter += 1;
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::UpdatePerksVis(float frameTime)
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	if (pPlayerPerksMenu)
	{
		if (m_CharacterPerksMenuVisible)
		{
			pPlayerPerksMenu->GetFlashPlayer()->Advance(frameTime);
			pPlayerPerksMenu->GetFlashPlayer()->Render();

			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pActor)
				return;

			XmlNodeRef node = pGameGlobals->CharacterPerks_node;//GetISystem()->LoadXmlFromFile(pGameGlobals->character_perks_path.c_str());
			if (!node)
			{
				CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
				CryLogAlways("xml not loaded.");
				return;
			}
			XmlNodeRef paramsNode = node->findChild("params");
			XmlNodeRef infopathNode = node->findChild("info");
			XmlNodeRef perksNode = infopathNode->findChild("perks");
			int id;
			int real_id;
			XmlString perk_icon_path;
			XmlString perk_description;
			int perk_parent_id;
			int perk_ct;
			int perk_grp;
			int perk_is_passive;
			int perk_simply_learn;
			int perk_pos_x;
			int perk_pos_y;
			int perk_level;
			int perk_max_level;
			int perks_number;
			paramsNode->getAttr("perks_num", perks_number);
			pPlayerPerksMenu->GetFlashPlayer()->Invoke0("clearAbilitysVisuals");
			for (int i = 0; i < perks_number; i++)
			{
				XmlNodeRef perkNode = perksNode->getChild(i);
				if (perkNode)
				{
					perkNode->getAttr("id", id);
					perkNode->getAttr("real_id", real_id);
					perkNode->getAttr("perk_description", perk_description);
					perkNode->getAttr("icon", perk_icon_path);
					perkNode->getAttr("perk_parent_id", perk_parent_id);
					perkNode->getAttr("perk_ct", perk_ct);
					perkNode->getAttr("perk_grp", perk_grp);
					perkNode->getAttr("perk_is_passive", perk_is_passive);
					perkNode->getAttr("perk_simply_learn", perk_simply_learn);
					perkNode->getAttr("perk_pos_x", perk_pos_x);
					perkNode->getAttr("perk_pos_y", perk_pos_y);
					perkNode->getAttr("perk_max_level", perk_max_level);
					perk_level = pActor->GetPerkLevel(real_id);

					bool full_learned = false;
					if (perk_level >= perk_max_level)
						full_learned = true;

					bool passive_ab = false;
					if (perk_is_passive > 0)
						passive_ab = true;


					SFlashVarValue args[11] = { id, perk_ct, perk_icon_path.c_str(), perk_pos_x, perk_pos_y, perk_level, perk_max_level, full_learned, passive_ab, false, real_id };

					pPlayerPerksMenu->GetFlashPlayer()->Invoke("addAbilityVisual", args, 11);
					if (perk_parent_id >= 0)
					{
						SFlashVarValue argsw[3] = { perk_ct, perk_parent_id, real_id };
						pPlayerPerksMenu->GetFlashPlayer()->Invoke("showLineBetweenTwoAbls", argsw, 3);
					}
				}
			}
			int ab_points = g_pGameCVars->g_levelupabilitypoints;
			SFlashVarValue argsd[2] = { "Ability Points", ab_points };
			pPlayerPerksMenu->GetFlashPlayer()->Invoke("setAblPoints", argsd, 2);
		}
	}
}

void CHUDCommon::UpdateObjectsMarkersOnMinimap(float frameTime)
{
	if (m_MainMapVisible)
		return; 

	if (!m_MinimapVisible)
		return;

	IUIElement* pMinimap = gEnv->pFlashUI->GetUIElement("MinimapUI");
	if (!pMinimap)
		return;

	CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClActor)
		return;

	float player_pos_x = (pClActor->GetEntity()->GetPos().x / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_x;
	float player_pos_y = (pClActor->GetEntity()->GetPos().y / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_y;
	float player_rot = RAD2DEG(pClActor->GetEntity()->GetRotation().GetRotZ());
	SFlashVarValue argsPlMark[4] = { -1, player_pos_x, player_pos_y, player_rot };
	pMinimap->GetFlashPlayer()->Invoke("setMapMarkerParams", argsPlMark, 4);

	maps_update_timers[0] -= frameTime;
	if (maps_update_timers[0] < 0.0f)
	{
		maps_update_timers[0] = maps_update_delays[0];
	}
	else
	{
		return;
	}

	float minimap_scan_range = g_pGameCVars->g_minimap_pl_max_actors_dist_on_minimap;
	Vec3 mp_pos = pClActor->GetEntity()->GetPos();
	Vec3 boxDim(minimap_scan_range, minimap_scan_range, minimap_scan_range);
	Vec3 ptmin = mp_pos - boxDim;
	Vec3 ptmax = mp_pos + boxDim;
	IPhysicalEntity** pMmEntityList = NULL;
	static const int iObjTypes = /*ent_rigid | ent_sleeping_rigid | */ent_living;
	int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pMmEntityList, iObjTypes);
	for (int i = 0; i < numEntities; ++i)
	{
		IPhysicalEntity* pPhysicalEntity = pMmEntityList[i];
		IEntity* pMapEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));
		if (pMapEntity)
		{
			if (pMapEntity->GetId() == pClActor->GetEntity()->GetId())
				continue;

			bool ent_added = stl::find(entites_ont_map_to_update, pMapEntity->GetId());
			if (ent_added)
			{

			}
			else
			{
				CActor *pMpActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pMapEntity->GetId()));
				if (!pMpActor)
					continue;

				entites_ont_map_to_update.push_back(pMapEntity->GetId());
				uint8	m_localPlayerFaction;
				uint8	m_mpActorFaction;
				IAIObject* pAIObjectMpActor = pMpActor->GetEntity()->GetAI();
				m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
				IAIObject* pAIObjectLocPl = pClActor->GetEntity()->GetAI();
				m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
				if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
				{
					SFlashVarValue argsActMark[5] = { pMapEntity->GetId(), 0, 0, "MapAndMinimap/minimap_enemy_arrow_lowres.png", 0.5f };
					pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 5);
				}
				else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral)
				{
					SFlashVarValue argsActMark[5] = { pMapEntity->GetId(), 0, 0, "MapAndMinimap/minimap_neutral_arrow_lowres.png", 0.5f };
					pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 5);
				}
				else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
				{
					SFlashVarValue argsActMark[5] = { pMapEntity->GetId(), 0, 0, "MapAndMinimap/minimap_frendly_arrow_lowres.png", 0.5f };
					pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 5);
				}
			}
		}
	}
	std::vector<EntityId> entites_to_not_delete_from_minimap;
	entites_to_not_delete_from_minimap.clear();
	int count_added_ents = entites_ont_map_to_update.size();
	std::vector<EntityId>::const_iterator it = entites_ont_map_to_update.begin();
	std::vector<EntityId>::const_iterator end = entites_ont_map_to_update.end();
	for (int i = 0; i < count_added_ents, it != end; i++, ++it)
	{
		EntityId this_ent = (*it);
		IEntity* pMapEntity = gEnv->pEntitySystem->GetEntity(this_ent);
		if (pMapEntity)
		{
			float dist_to_pl = mp_pos.GetDistance(pMapEntity->GetPos());
			if (dist_to_pl < minimap_scan_range)
			{
				float ent_pos_x = (pMapEntity->GetPos().x / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_x;
				float ent_pos_y = (pMapEntity->GetPos().y / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_y;
				float ent_rot = RAD2DEG(pMapEntity->GetRotation().GetRotZ());
				SFlashVarValue argsEntPoint[4] = { pMapEntity->GetId(), ent_pos_x, ent_pos_y, ent_rot };
				pMinimap->GetFlashPlayer()->Invoke("setMapMarkerParams", argsEntPoint, 4);
				entites_to_not_delete_from_minimap.push_back(this_ent);
				continue;
			}
			else
			{
				SFlashVarValue argsEntPoint[1] = { this_ent };
				pMinimap->GetFlashPlayer()->Invoke("delMapMarker", argsEntPoint, 1);
				continue;
			}
		}
		else
		{
			SFlashVarValue argsEntPoint[1] = { this_ent };
			pMinimap->GetFlashPlayer()->Invoke("delMapMarker", argsEntPoint, 1);
			continue;
		}
	}
	entites_ont_map_to_update.clear();
	entites_ont_map_to_update = entites_to_not_delete_from_minimap;
	std::vector<SMapMarker>::iterator it_f = s_MapStaticMarkers.begin();
	std::vector<SMapMarker>::iterator end_f = s_MapStaticMarkers.end();
	int count = s_MapStaticMarkers.size();
	for (int i = 0; i < count, it_f != end_f; i++, ++it_f)
	{
		SMapMarker marker = *it_f;
		if (marker.marker_id != -5)
		{
			if (!stl::find(static_markers_on_minimap_ids, marker.marker_id))
			{
				Vec2 pl_pos_xy(pClActor->GetEntity()->GetPos().x, pClActor->GetEntity()->GetPos().y);
				float dist_to_mark = pl_pos_xy.GetDistance(marker.marker_pos);
				if (dist_to_mark < g_pGameCVars->g_minimap_pl_max_static_object_dist_on_minimap)
				{
					static_markers_on_minimap_ids.push_back(marker.marker_id);
					float stmark_pos_x = (marker.marker_pos.x / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_x;
					float stmark_pos_y = (marker.marker_pos.y / g_pGameCVars->g_minimap_pl_pix_mlt) + g_pGameCVars->g_minimap_pl_pos_pls_x;
					SFlashVarValue argsStMark[5] = { marker.marker_id, stmark_pos_x, stmark_pos_y, marker.marker_icon.c_str(), 1.5f };
					pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsStMark, 5);
				}
			}
			else
			{
				Vec2 pl_pos_xy(pClActor->GetEntity()->GetPos().x, pClActor->GetEntity()->GetPos().y);
				float dist_to_mark = pl_pos_xy.GetDistance(marker.marker_pos);
				if (dist_to_mark > g_pGameCVars->g_minimap_pl_max_static_object_dist_on_minimap)
				{
					SFlashVarValue argsEntPoint[1] = { marker.marker_id };
					pMinimap->GetFlashPlayer()->Invoke("delMapMarker", argsEntPoint, 1);
					int delid = marker.marker_id;
					stl::find_and_erase(static_markers_on_minimap_ids, delid);
				}
			}
		}
	}
}

void CHUDCommon::UpdateObjectsMarkersOnMap(float frameTime)
{
	/*maps_update_timers[1] -= frameTime;
	if (maps_update_timers[1] < 0.0f)
	{
		maps_update_timers[1] = maps_update_delays[1];
	}
	else
	{
		return;
	}*/

	if (!m_MainMapVisible)
		return;

	IUIElement* pMainMap = gEnv->pFlashUI->GetUIElement("MainMapUI");
	if (!pMainMap)
		return;

	CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClActor)
		return;

	string pl_name = "Player";
	CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
	if (p_PlCrSys)
	{
		pl_name = p_PlCrSys->GetCharacterName(false);
	}
	else
	{
		pl_name = pClActor->GetEntity()->GetName();
	}


	float player_pos_x = (pClActor->GetEntity()->GetPos().x / g_pGameCVars->g_mainmap_pl_pix_mlt);
	float player_pos_y = (pClActor->GetEntity()->GetPos().y / g_pGameCVars->g_mainmap_pl_pix_mlt);
	float player_rot = RAD2DEG(pClActor->GetEntity()->GetRotation().GetRotZ());
	SFlashVarValue argsPlMark[7] = { -1, "MapAndMinimap/minimap_pl_arrow_lowres.png", pl_name.c_str(), player_pos_x, player_pos_y, player_rot, 0.5f };
	pMainMap->GetFlashPlayer()->Invoke("addMapMarker", argsPlMark, 7);
	float mainmap_scan_range = g_pGameCVars->g_mainmap_pl_max_actors_dist_on_mainmap;
	Vec3 mp_pos = pClActor->GetEntity()->GetPos();
	Vec3 boxDim(mainmap_scan_range, mainmap_scan_range, mainmap_scan_range);
	Vec3 ptmin = mp_pos - boxDim;
	Vec3 ptmax = mp_pos + boxDim;
	IPhysicalEntity** pMmEntityList = NULL;
	static const int iObjTypes = /*ent_rigid | ent_sleeping_rigid | */ent_living;
	int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pMmEntityList, iObjTypes);
	for (int i = 0; i < numEntities; ++i)
	{
		IPhysicalEntity* pPhysicalEntity = pMmEntityList[i];
		IEntity* pMapEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));
		if (pMapEntity)
		{
			if (pMapEntity->GetId() == pClActor->GetEntity()->GetId())
				continue;


			CActor *pMpActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pMapEntity->GetId()));
			if (!pMpActor)
				continue;

			uint8	m_localPlayerFaction;
			uint8	m_mpActorFaction;
			IAIObject* pAIObjectMpActor = pMpActor->GetEntity()->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pClActor->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			float ent_pos_x = (pMpActor->GetEntity()->GetPos().x / g_pGameCVars->g_mainmap_pl_pix_mlt);
			float ent_pos_y = (pMpActor->GetEntity()->GetPos().y / g_pGameCVars->g_mainmap_pl_pix_mlt);
			float ent_rot = RAD2DEG(pMpActor->GetEntity()->GetRotation().GetRotZ());
			string ac_nmn = pMpActor->GetActorName();
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
			{
				SFlashVarValue argsActMark[7] = { pMapEntity->GetId(), "MapAndMinimap/minimap_enemy_arrow_lowres.png", ac_nmn.c_str(), ent_pos_x, ent_pos_y, ent_rot, 0.5f };
				pMainMap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 7);
			}
			else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral)
			{
				SFlashVarValue argsActMark[7] = { pMapEntity->GetId(), "MapAndMinimap/minimap_neutral_arrow_lowres.png", ac_nmn.c_str(), ent_pos_x, ent_pos_y, ent_rot, 0.4f };
				pMainMap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 7);
			}
			else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
			{
				SFlashVarValue argsActMark[7] = { pMapEntity->GetId(), "MapAndMinimap/minimap_frendly_arrow_lowres.png", ac_nmn.c_str(), ent_pos_x, ent_pos_y, ent_rot, 0.5f };
				pMainMap->GetFlashPlayer()->Invoke("addMapMarker", argsActMark, 7);
			}
		}
	}

	std::vector<SMapMarker>::iterator it = s_MapStaticMarkers.begin();
	std::vector<SMapMarker>::iterator end = s_MapStaticMarkers.end();
	int count = s_MapStaticMarkers.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SMapMarker marker = *it;
		if (marker.marker_id != -5)
		{
			float stmark_pos_x = (marker.marker_pos.x / g_pGameCVars->g_mainmap_pl_pix_mlt);
			float stmark_pos_y = (marker.marker_pos.y / g_pGameCVars->g_mainmap_pl_pix_mlt);
			SFlashVarValue argsStMark[7] = { marker.marker_id, marker.marker_icon.c_str(), marker.marker_name.c_str(), stmark_pos_x, stmark_pos_y, -90, 1.2f };
			pMainMap->GetFlashPlayer()->Invoke("addMapMarker", argsStMark, 7);
		}
	}
}

void CHUDCommon::DelayActionForPlayer()
{
	CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	pActor->SetSAPlayTime(0.3f);
}

void CHUDCommon::UpdateCharCreatingUI(SCharacterCreatingUIInfo sInfo)
{
	IUIElement* pCharacter_creating_UI = gEnv->pFlashUI->GetUIElement("Character_creating_UI");
	if (pCharacter_creating_UI)
	{
		if (m_CharacterCreationVisible)
		{
			SFlashVarValue arg1[1] = { sInfo.eyes_info.c_str() };
			pCharacter_creating_UI->GetFlashPlayer()->Invoke("setEyesDec", arg1, 1);
			SFlashVarValue arg2[1] = { sInfo.hair_info.c_str() };
			pCharacter_creating_UI->GetFlashPlayer()->Invoke("setHairDec", arg2, 1);
			SFlashVarValue arg3[1] = { sInfo.head_info.c_str() };
			pCharacter_creating_UI->GetFlashPlayer()->Invoke("setHeadFaceDec", arg3, 1);
		}
	}
}

void CHUDCommon::OnLoadLevel(const char * mapname, bool isServer, const char * gamerules)
{
	entites_ont_map_to_update.clear();
	static_markers_on_minimap_ids.clear();
	ClearAllMapMarkers();
	sd_ui_timers[0] = 0.1f;
	post_initialization_timer = 8.0f;
	if (m_DeathScreenVisible)
		ShowOnDeathScreen(false);
	//if (!m_PostInitScreenVisible)
	//	ShowPostInitialization(true);
}

void CHUDCommon::OnReloadLevel()
{
	sd_ui_timers[0] = 0.1f;
	if (m_DeathScreenVisible)
		ShowOnDeathScreen(false);
}

void CHUDCommon::OnSaveGame(bool shouldResume)
{
}

void CHUDCommon::OnLoadGame(bool shouldResume)
{
	ClearAllMapMarkers();
	entites_ont_map_to_update.clear();
	static_markers_on_minimap_ids.clear();
	sd_ui_timers[0] = 0.1f;
	post_initialization_timer = 8.0f;
	//if (!m_PostInitScreenVisible)
	//	ShowPostInitialization(true);
	m_GamePausedIU = false;
	if (m_DeathScreenVisible)
		ShowOnDeathScreen(false);
}

void CHUDCommon::OnPauseGame()
{
	//CryLogAlways("CHUDCommon::OnPauseGame()");
	//ShowMinimap(false, true);
	//m_GamePausedIU = true;
}

void CHUDCommon::OnResumeGame()
{
	//m_GamePausedIU = false;
	sd_ui_timers[0] = 0.1f;
	m_GamePausedIU = false;
}

void CHUDCommon::OnExitGame()
{
	ClearAllMapMarkers();
	entites_ont_map_to_update.clear();
	static_markers_on_minimap_ids.clear();
	sd_ui_timers[0] = 0.1f;
	if (m_PostInitScreenVisible)
		ShowPostInitialization(false);

	if (m_DeathScreenVisible)
		ShowOnDeathScreen(false);
}

void CHUDCommon::OnStartGame()
{
	sd_ui_timers[0] = 0.1f;
	post_initialization_timer = 8.0f;
	if (!m_PostInitScreenVisible)
		ShowPostInitialization(true);

	if (m_DeathScreenVisible)
		ShowOnDeathScreen(false);

	m_GamePausedIU = false;
}

void CHUDCommon::OnStartInGameMenu()
{
	if (MainHud_loaded)
	{
		ShowMainHud();
	}
	ShowMinimap(false, true);
	ShowSimpleCrosshair(false, false);
	ShowSelectedWeapons(false, false);
	ShowHints(false);
	ShowOnScreenObjInfo(false);
	ShowInteractiveObjectInfo("", "", 0, false);
	m_GamePausedIU = true;
}

void CHUDCommon::OnStopInGameMenu()
{
	m_GamePausedIU = false;
}

Vec2i CHUDCommon::GetEntityPositionOnScreenInUIElement(IUIElement * pElem, Vec3 ent_pos, Vec3 offset)
{
	if(!gEnv->pRenderer && !pElem)
		return Vec2i(ZERO);

	Vec3 screenPos(ZERO);
	gEnv->pRenderer->ProjectToScreen(ent_pos.x, ent_pos.y, ent_pos.z,
		&screenPos.x, &screenPos.y, &screenPos.z);
	float fMovieWidth = (float)pElem->GetFlashPlayer()->GetWidth();
	float fMovieHeight = (float)pElem->GetFlashPlayer()->GetHeight();
	float fRendererWidth = (float)gEnv->pRenderer->GetWidth();
	float fRendererHeight = (float)gEnv->pRenderer->GetHeight();
	float pfScaleX = (fMovieHeight / 100.0f) * fRendererWidth / fRendererHeight;
	float pfScaleY = (fMovieHeight / 100.0f);
	float fScale = fMovieHeight / fRendererHeight;
	float fUselessSize = fMovieWidth - fRendererWidth * fScale;
	float pfHalfUselessSize = fUselessSize * 0.5f;
	screenPos.x = screenPos.x * pfScaleX + pfHalfUselessSize;
	screenPos.y = screenPos.y * pfScaleY;
	Vec2i pos_scr = Vec2i((int)screenPos.x, (int)screenPos.y);
	return pos_scr;
}

Vec2i CHUDCommon::GetEntityPositionOnScreenInUIElementWoProjection(IUIElement * pElem, Vec3 ent_pos, Vec3 offset, bool w_hl_size)
{
	if (!pElem)
		return Vec2i(ZERO);

	float fMovieWidth = (float)pElem->GetFlashPlayer()->GetWidth();
	float fMovieHeight = (float)pElem->GetFlashPlayer()->GetHeight();
	float fRendererWidth = (float)gEnv->pRenderer->GetWidth();
	float fRendererHeight = (float)gEnv->pRenderer->GetHeight();
	float pfScaleX = (fMovieHeight / 100.0f) * fRendererWidth / fRendererHeight;
	float pfScaleY = (fMovieHeight / 100.0f);
	float fScale = fMovieHeight / fRendererHeight;
	float fUselessSize = fMovieWidth - fRendererWidth * fScale;
	float pfHalfUselessSize = fUselessSize * 0.5f;
	if (!w_hl_size)
		pfHalfUselessSize = 0;

	ent_pos.x = ent_pos.x * pfScaleX + pfHalfUselessSize;
	ent_pos.y = ent_pos.y * pfScaleY;
	Vec2i pos_scr = Vec2i((int)ent_pos.x, (int)ent_pos.y);
	return pos_scr;
}

bool CHUDCommon::IsActorVisibleByPlayer(EntityId actorId, float lgnt)
{
	CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClActor)
		return false;

	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CCamera& cam = GetISystem()->GetViewCamera();
	Vec3 cam_pos(0, 0, 0);
	Vec3 cam_dir(0, 0, 0);
	AABB targetBbox;
	pActor->GetEntity()->GetWorldBounds(targetBbox);
	Vec3 entPos = targetBbox.GetCenter();
	cam_pos = cam.GetPosition() + cam.GetViewdir();
	cam_dir = (entPos - cam_pos)*(lgnt + pClActor->GetBonePosition("Bip01 Head").GetDistance(cam_pos));
	ray_hit hit;
	IPhysicalEntity *pEnt_to_ignore = pClActor->GetEntity()->GetPhysics();
	IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	if (pEnt_to_ignore)
	{
		pEnts_to_ignore[num_ents_to_ignore] = pEnt_to_ignore;
		num_ents_to_ignore += 1;
	}

	ICharacterInstance* pCharacter_player = pClActor->GetEntity()->GetCharacter(0);
	if (pCharacter_player)
	{
		IAttachmentManager* pAm = pCharacter_player->GetIAttachmentManager();
		if (pAm)
		{

			IAttachment *ptTa = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att1_to_ignore->GetString());
			if (ptTa)
			{
				CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa->GetIAttachmentObject();
				if (entAttachment)
				{
					IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
					if (pEnttoignore2)
					{
						IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
						if (pEnt2toignore2)
						{
							pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
							num_ents_to_ignore += 1;
							//CryLogAlways("IsAITargetHaveAndVisibleOrObstr GetInterfaceByName(headede) true");
						}
					}
				}
			}
			IAttachment *ptTa2 = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att2_to_ignore->GetString());
			if (ptTa2)
			{
				CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa2->GetIAttachmentObject();
				if (entAttachment)
				{
					IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
					if (pEnttoignore2)
					{
						IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
						if (pEnt2toignore2)
						{
							pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
							num_ents_to_ignore += 1;
						}
					}
				}
			}
		}
	}

	gEnv->pPhysicalWorld->RayWorldIntersection(cam_pos, cam_dir, ent_all,
		rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

	IPhysicalEntity *pCollider = hit.pCollider;
	IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
	EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
	if (pCollidedEntity)
	{
		if (collidedEntityId == actorId)
		{
			if (cam_pos.GetDistance(pCollidedEntity->GetWorldPos()) > (lgnt - 2.5f))
				return false;

			return true;
		}

		CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
		if (curItem)
		{
			if (curItem->GetOwnerId() == actorId)
			{
				if (cam_pos.GetDistance(pCollidedEntity->GetWorldPos()) > (lgnt *0.73f))
					return false;

				return true;
			}
		}
	}
	return false;
}

void CHUDCommon::DeleteItemVisualFromInventory(int slot_id)
{
	if (!Inv_loaded)
		return;

	if (slot_id < 0)
		slot_id = 0;

	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	if (pElement)
	{
		SFlashVarValue args[5] = { slot_id, "", 0, 0, 0 };
		pElement->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
		pElement->GetFlashPlayer()->Render();
	}
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::UpdateInventory(float frameTime)
{
	if (m_TimerInventoryUpdate)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerInventoryUpdate);
		m_TimerInventoryUpdate = NULL;
	}
	IUIElement* pElementInventory = gEnv->pFlashUI->GetUIElement("Inv");

	if (!pElementInventory)
		return;

	if (pElementInventory->GetFlashPlayer()->GetVisible() && Inv_loaded)
	{
		pElementInventory->GetFlashPlayer()->Advance(frameTime);
		pElementInventory->GetFlashPlayer()->Render();
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		IInventory *pInventory = pActor->GetInventory();
		if (g_pGameCVars->g_player_inv_sel_tab_id == 3)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				EntityId id = curItem->GetHudItemPosPrimary();
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = id;
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				if (g_pGameCVars->hud_inventory_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (g_pGameCVars->g_player_inv_sel_tab_id == 0)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->CanSelect())
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; ++i, ++it_pl)
			{
				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				CWeapon *pWeapon = reinterpret_cast<CWeapon *>(pItem->GetIWeapon());
				if (!pWeapon)
					continue;

				if (pItem->CanSelect())
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_inventory_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = curItem->GetItemCost();
						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pElementInventory->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pElementInventory->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (g_pGameCVars->g_player_inv_sel_tab_id == 1)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81 && curItem->GetArmorType() > 0)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));

				if (curItem->GetArmorType() > 0)
				{
					string icon = curItem->GetIcon();
					string texture = icon;
					string itemLabel = texture;
					int stat = curItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					EntityId itemID = curItem->GetHudItemPosSecondary();
					int stt = curItem->GetEquipped();
					string occurrence = itemLabel;
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					if (g_pGameCVars->hud_inventory_type == 1)
					{
						string item_class = curItem->GetItemDec(4);
						int itme_val_money = curItem->GetItemCost();
						int itme_mass = curItem->GetSharedItemParams()->params.mass;
						int itme_hp = curItem->GetItemHealth();
						SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
						pElementInventory->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
					}
					else
					{
						SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
						pElementInventory->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
					}
				}
			}
		}
		else if (g_pGameCVars->g_player_inv_sel_tab_id == 2)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosPrimary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_inventory_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}
			}
		}
		else if (g_pGameCVars->g_player_inv_sel_tab_id == 4)
		{

			std::list<EntityId> player_items;

			for (int i = 0; i < pInventory->GetCount(); i++)
			{
				EntityId id = pInventory->GetItem(i);
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
				if (curItem && curItem->GetItemType() != 81)
				{
					player_items.push_back(id);
				}
			}

			std::list<EntityId>::iterator it_pl = player_items.begin();
			std::list<EntityId>::iterator end_pl = player_items.end();

			int count_pl_i = player_items.size();

			for (int i = 0; i < count_pl_i; i++, it_pl++)
			{

				EntityId ide = (*it_pl);
				EntityId id = i;
				CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				IItem *pItem = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
				string icon = curItem->GetIcon();
				string texture = icon;
				string itemLabel = texture;
				int stat = curItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				EntityId itemID = curItem->GetHudItemPosSecondary();
				int stt = curItem->GetEquipped();
				string occurrence = itemLabel;
				SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
				if (g_pGameCVars->hud_inventory_type == 1)
				{
					string item_class = curItem->GetItemDec(4);
					int itme_val_money = curItem->GetItemCost();
					int itme_mass = curItem->GetSharedItemParams()->params.mass;
					int itme_hp = curItem->GetItemHealth();
					SFlashVarValue args[9] = { itemID, texture.c_str(), stat, idIndex, stt, item_class.c_str(), itme_val_money, itme_mass, itme_hp };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInListLine", args, 9);
				}
				else
				{
					SFlashVarValue args[5] = { itemID, texture.c_str(), stat, idIndex, stt };
					pElementInventory->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
				}

			}
		}
		UpdateEquippedItemsInInvWindow();
	}

	request_for_delayed_update_player_doll = true;
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::ClearInventory(bool reload_element)
{
	IUIElement* pElementInventory = gEnv->pFlashUI->GetUIElement("Inv");
	if (pElementInventory)
	{
		if (pElementInventory->GetFlashPlayer() && pElementInventory->GetFlashPlayer()->GetVisible())
		{
			pElementInventory->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
			if (reload_element)
			{
				pElementInventory->Reload();
			}
		}
	}
}

bool CHUDCommon::InventoryUsePrimaryItemPos()
{
	bool prim_inv_items_pos = false;
	if (g_pGameCVars->g_player_inv_sel_tab_id == 3)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 2)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 1)
		prim_inv_items_pos = false;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 0)
		prim_inv_items_pos = false;

	return prim_inv_items_pos;
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::FillInventory()
{
	UpdateInventory(0.1f);
}
//-----------------------------------------------------------------------------------------------------

void CHUDCommon::CursorIncrementCounter()
{
	if(gEnv->pHardwareMouse)
		{
			gEnv->pHardwareMouse->IncrementCounter();
		}
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::CursorDecrementCounter()
{
	if(gEnv->pHardwareMouse)
	{
			gEnv->pHardwareMouse->DecrementCounter();
	}
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::DragAndDropFSRealization(const char *strCommand, const char *strArgs)
{
	bool prim_inv_items_pos = false;
	if(g_pGameCVars->g_player_inv_sel_tab_id == 3)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 2)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 1)
		prim_inv_items_pos = false;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 0)
		prim_inv_items_pos = false;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (!strcmp(strCommand, "dragAndDropPressed"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			if (nwAction->Current_inventory_selection_mode == nwAction->ePISMode_drop || nwAction->Current_inventory_selection_mode == nwAction->ePISMode_combine)
				return;
		}
		int id = static_cast<int>(atoi(strArgs));
		current_dragged_item_slot = id;
	}
	else if(!strcmp(strCommand, "dragAndDropPressedNv"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			if (nwAction->Current_inventory_selection_mode == nwAction->ePISMode_drop || nwAction->Current_inventory_selection_mode == nwAction->ePISMode_combine)
				return;
		}
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			current_dragged_item = id;
		}
	}
	else if (!strcmp(strCommand, "dragAndDropRelease"))
	{
		int id = static_cast<int>(atoi(strArgs));
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (pGameGlobals)
		{
			pGameGlobals->current_selected_Item = 0;
		}

		if (hit_on_player_doll_clip)
		{
			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (!nwAction)
					return;

				nwAction->InventoryItemPressed(current_dragged_item);
				UpdateInventory(0.5);
				//start_drag = false;
				ShowDraggedItemIcon(false, 0);
				current_dragged_item = 0;
				item_for_drag_exchandge = 0;
				current_dragged_item_slot = -1;
				CryLogAlways("dragAndDropHitOnPlayerDoll");
				return;
			}
		}

		if (hit_on_drop_clip)
		{
			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (!nwAction)
					return;

				nwAction->Current_inventory_selection_mode = nwAction->ePISMode_drop;
				nwAction->InventoryItemPressed(current_dragged_item);
				nwAction->Current_inventory_selection_mode = nwAction->ePISMode_equip;
				UpdateInventory(0.5);
				//start_drag = false;
				current_dragged_item = 0;
				ShowDraggedItemIcon(false, 0);
				item_for_drag_exchandge = 0;
				current_dragged_item_slot = -1;
				item_drop_request = true;
				return;
			}
		}
	}
	else if (!strcmp(strCommand, "dragAndDropReleaseNv"))
	{
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			if (current_dragged_item > 0 && start_drag)
			{
				CItem *pItem_sel = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_for_drag_exchandge));
				CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
				if (pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						int pos_sel = pItem_sel->GetHudItemPosPrimary();
						int pos_drg = pItem_dragged->GetHudItemPosPrimary();
						pItem_sel->SetHudItemPosPrimary(pos_drg);
						pItem_dragged->SetHudItemPosPrimary(pos_sel);
					}
					else
					{
						int pos_sel = pItem_sel->GetHudItemPosSecondary();
						int pos_drg = pItem_dragged->GetHudItemPosSecondary();
						pItem_sel->SetHudItemPosSecondary(pos_drg);
						pItem_dragged->SetHudItemPosSecondary(pos_sel);
					}
					//start_drag = false;
					ShowDraggedItemIcon(false, current_dragged_item);
					current_dragged_item = 0;
					item_for_drag_exchandge = 0;
					current_dragged_item_slot = -1;
					return;
				}

				if (!pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosPrimary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosPrimary(pItem_dragged->GetHudItemPosPrimary());
					}
					else
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosSecondary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosSecondary(pItem_dragged->GetHudItemPosSecondary());
					}
				}
			}
			//start_drag = false;
			ShowDraggedItemIcon(false, 0);
			current_dragged_item = 0;
			item_for_drag_exchandge = 0;
			current_dragged_item_slot = -1;
		}
		else
		{
			if (current_dragged_item > 0 && start_drag)
			{
				CItem *pItem_sel = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_for_drag_exchandge));
				CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
				if (pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						int pos_sel = pItem_sel->GetHudItemPosPrimary();
						int pos_drg = pItem_dragged->GetHudItemPosPrimary();
						pItem_sel->SetHudItemPosPrimary(pos_drg);
						pItem_dragged->SetHudItemPosPrimary(pos_sel);
					}
					else
					{
						int pos_sel = pItem_sel->GetHudItemPosSecondary();
						int pos_drg = pItem_dragged->GetHudItemPosSecondary();
						pItem_sel->SetHudItemPosSecondary(pos_drg);
						pItem_dragged->SetHudItemPosSecondary(pos_sel);
					}
					//start_drag = false;
					ShowDraggedItemIcon(false, current_dragged_item);
					current_dragged_item = 0;
					item_for_drag_exchandge = 0;
					current_dragged_item_slot = -1;
					return;
				}
					
				if (!pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosPrimary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosPrimary(pItem_dragged->GetHudItemPosPrimary());
					}
					else
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosSecondary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosSecondary(pItem_dragged->GetHudItemPosSecondary());
					}
					ShowDraggedItemIcon(false, current_dragged_item);
				}
			}
		}

		if (item_drop_request)
		{
			item_drop_request = false;
			ShowDraggedItemIcon(false, 0);
			current_dragged_item = 0;
			item_for_drag_exchandge = 0;
			current_dragged_item_slot = -1;
			return;
		}
		//start_drag = false;
		ShowDraggedItemIcon(false, current_dragged_item);
		current_dragged_item = 0;
		item_for_drag_exchandge = 0;
		current_dragged_item_slot = -1;
	}
	else if (!strcmp(strCommand, "dragAndDropMouseMove"))
	{
		int id = static_cast<int>(atoi(strArgs));
		if (start_drag)
			slot_for_drag_exchandge = id;
	}
	else if (!strcmp(strCommand, "dragAndDropMouseMoveNv"))
	{
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			item_for_drag_exchandge = id;
		}
		else
			item_for_drag_exchandge = 0;
	}
	else if (!strcmp(strCommand, "dragAndDropDragOut"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			if (nwAction->Current_inventory_selection_mode == nwAction->ePISMode_drop || nwAction->Current_inventory_selection_mode == nwAction->ePISMode_combine)
				return;
		}
		int id = static_cast<int>(atoi(strArgs));
		IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
		if (pElement)
		{
			SFlashVarValue args[5] = { id, "", 0, 0, 0 };
			pElement->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);
			pElement->GetFlashPlayer()->Render();
		}
	}
	else if (!strcmp(strCommand, "dragAndDropDragOutNv"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			if (nwAction->Current_inventory_selection_mode == nwAction->ePISMode_drop || nwAction->Current_inventory_selection_mode == nwAction->ePISMode_combine)
				return;
		}
		CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			
			if (!start_drag)
			{
				start_drag = true;
			}

			if(pItem_dragged)
			{
				ShowDraggedItemIcon(true, current_dragged_item);
				DraggedItemUpdatePos();
			}

		}
		else
		{
			if (pItem_dragged)
			{
				ShowDraggedItemIcon(true, current_dragged_item);
				DraggedItemUpdatePos();
			}
		}
	}
	else if (!strcmp(strCommand, "dragAndDropDragOver"))
	{
		int id = static_cast<int>(atoi(strArgs));
	}
	else if (!strcmp(strCommand, "dragAndDropDragOverNv"))
	{
		EntityId id = static_cast<EntityId>(atoi(strArgs));
	}
	else if (!strcmp(strCommand, "dragAndDropRollOver"))
	{
		//CryLogAlways("dragAndDropRollOver");
		int id = static_cast<int>(atoi(strArgs));
	}
	else if (!strcmp(strCommand, "dragAndDropRollOut"))
	{
		//CryLogAlways("dragAndDropRollOut");
	}
	else if (!strcmp(strCommand, "dragAndDropHitTestFail"))
	{
		//CryLogAlways("dragAndDropHitTestFail");
		slot_for_drag_exchandge = -1;
	}
	else if (!strcmp(strCommand, "dragAndDropHitOnPlayerDoll"))
	{
		int id = static_cast<int>(atoi(strArgs));
		if (id > 0)
		{
			hit_on_player_doll_clip = true;
		}
		else
		{
			hit_on_player_doll_clip = false;
		}
	}
	else if (!strcmp(strCommand, "dragAndDropHitOnDropClip"))
	{
		int id = static_cast<int>(atoi(strArgs));
		if (id > 0)
		{
			hit_on_drop_clip = true;
		}
		else
		{
			hit_on_drop_clip = false;
		}
	}
	else if (!strcmp(strCommand, "dragAndDropPlayerDollPressed"))
	{
		//CryLogAlways("dragAndDropPlayerDollPressed");
		player_doll_clip_pressed_pos = Vec2(0, 0);
		player_doll_clip_pressed = true;
		float xl, yl = 0.0f;
		gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&xl, &yl);
		player_doll_clip_pressed_pos = Vec2(xl, yl);
		player_doll_clip_pressed_pos_old = Vec2(xl, yl);
	}
	else if (!strcmp(strCommand, "dragAndDropPlayerDollReleased"))
	{
		//CryLogAlways("dragAndDropPlayerDollReleased");
		player_doll_clip_pressed_pos = Vec2(0, 0);
		player_doll_clip_pressed = false;
		player_doll_clip_pressed_pos_old = Vec2(0, 0);
	}
	else if (!strcmp(strCommand, "dragAndDropRollOverEquipped"))
	{

	}
	else if (!strcmp(strCommand, "dragAndDropRollOutEquipped"))
	{

	}
	else if (!strcmp(strCommand, "dragAndDropPressedEquipped"))
	{

	}
	else if (!strcmp(strCommand, "dragAndDropReleaseEquipped"))
	{
		bool is_normal_slot_name = false;
		/*for (int i = 0; i < eEVS_AllSlots; i++)
		{
			if (!strcmp(strArgs, equipment_slots_names[i]))
			{
				is_normal_slot_name = true;
				break;
			}
		}*/
	
		if (!strcmp(strArgs, current_equipment_dragged_slot))
		{
			is_normal_slot_name = true;
		}
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (pGameGlobals)
		{
			pGameGlobals->current_selected_Item = 0;
		}
		current_equipment_dragged_slot = "";
		if (hit_on_player_doll_clip || is_normal_slot_name)
		{
			//int id = GetItemInvSlotIdFromEquippedVisualSlot(strArgs);
			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (!nwAction)
					return;

				if (!strcmp(strArgs, equipment_slots_names[eEVS_leftWeapon]))
				{
					nwAction->InventoryItemProcessEquipItemInLeftSlot(current_dragged_item);
				}
				else
					nwAction->InventoryItemPressed(current_dragged_item);

				UpdateInventory(0.5);
				//if (!m_TimerInventoryUpdate)
				//	m_TimerInventoryUpdate = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.5f), false, functor(*this, &CHUDCommon::TimedUpdateInventory), NULL);
				//start_drag = false;
				ShowDraggedItemIcon(false, 0);
				current_dragged_item = 0;
				item_for_drag_exchandge = 0;
				current_dragged_item_slot = -1;
				//CryLogAlways("dragAndDropHitOnPlayerDoll");
				return;
			}
		}

		if (hit_on_drop_clip)
		{
			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (!nwAction)
					return;

				nwAction->Current_inventory_selection_mode = nwAction->ePISMode_drop;
				nwAction->InventoryItemPressed(current_dragged_item);
				nwAction->Current_inventory_selection_mode = nwAction->ePISMode_equip;
				UpdateInventory(0.5);
				//start_drag = false;
				current_dragged_item = 0;
				ShowDraggedItemIcon(false, 0);
				item_for_drag_exchandge = 0;
				current_dragged_item_slot = -1;
				item_drop_request = true;
				return;
			}
		}

		if (slot_for_drag_exchandge >= 0)
		{
			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (!nwAction)
					return;

				nwAction->InventoryItemPressed(current_dragged_item, true);
				if(!m_TimerInventoryUpdate)
					m_TimerInventoryUpdate = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.5f), false, functor(*this, &CHUDCommon::TimedUpdateInventory), NULL);

				//UpdateInventory(0.5);
			}
		}
	}
	else if (!strcmp(strCommand, "dragAndDropReleaseEquippedForIt"))
	{
		bool is_normal_slot_name = false;
		for (int i = 0; i < eEVS_AllSlots; i++)
		{
			if (!strcmp(strArgs, equipment_slots_names[i]))
			{
				is_normal_slot_name = true;
				break;
			}
		}

		if (!strcmp(strArgs, current_equipment_dragged_slot))
		{
			is_normal_slot_name = true;
		}
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (pGameGlobals)
		{
			pGameGlobals->current_selected_Item = 0;
		}
		current_equipment_dragged_slot = "";
		if (is_normal_slot_name)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			bool can_equip = true;
			if (current_dragged_item == GetItemIdFromEquippedSlot(strArgs))
			{
				can_equip = false;
			}

			if (IsEquippedSlotBusy(strArgs) && can_equip)
			{
				CItem *pItem_equipped = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(GetItemIdFromEquippedSlot(strArgs)));
				if (pItem_equipped)
				{
					if (pItem_equipped->GetEquipped() > 0)
					{
						//nwAction->InventoryItemPressed(pItem_equipped->GetEntityId());
					}
				}
			}

			CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
			if (pItem_dragged)
			{
				if (can_equip)
				{
					if (!strcmp(strArgs, equipment_slots_names[eEVS_leftWeapon]))
					{
						nwAction->InventoryItemProcessEquipItemInLeftSlot(current_dragged_item);
					}
					else
					{
						nwAction->InventoryItemPressed(current_dragged_item);
					}
				}

				current_equipment_dragged_slot = "";
				UpdateInventory(0.5);
				ShowDraggedItemIcon(false, 0);
				current_dragged_item = 0;
				item_for_drag_exchandge = 0;
				current_dragged_item_slot = -1;
				if (!m_TimerInventoryUpdate)
					m_TimerInventoryUpdate = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.5f), false, functor(*this, &CHUDCommon::TimedUpdateInventory), NULL);
			}
		}
	}
	else if (!strcmp(strCommand, "dragAndDropReleaseNvEquipped"))
	{
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			if (current_dragged_item > 0 && start_drag)
			{
				CItem *pItem_sel = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_for_drag_exchandge));
				CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
				if (pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						int pos_sel = pItem_sel->GetHudItemPosPrimary();
						int pos_drg = pItem_dragged->GetHudItemPosPrimary();
						pItem_sel->SetHudItemPosPrimary(pos_drg);
						pItem_dragged->SetHudItemPosPrimary(pos_sel);
					}
					else
					{
						int pos_sel = pItem_sel->GetHudItemPosSecondary();
						int pos_drg = pItem_dragged->GetHudItemPosSecondary();
						pItem_sel->SetHudItemPosSecondary(pos_drg);
						pItem_dragged->SetHudItemPosSecondary(pos_sel);
					}
					//start_drag = false;
					ShowDraggedItemIcon(false, current_dragged_item);
					current_dragged_item = 0;
					item_for_drag_exchandge = 0;
					current_dragged_item_slot = -1;
					return;
				}

				if (!pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosPrimary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosPrimary(pItem_dragged->GetHudItemPosPrimary());
					}
					else
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosSecondary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosSecondary(pItem_dragged->GetHudItemPosSecondary());
					}
				}
			}
			//start_drag = false;
			ShowDraggedItemIcon(false, 0);
			current_dragged_item = 0;
			item_for_drag_exchandge = 0;
			current_dragged_item_slot = -1;
		}
		else
		{
			if (current_dragged_item > 0 && start_drag)
			{
				CItem *pItem_sel = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_for_drag_exchandge));
				CItem *pItem_dragged = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(current_dragged_item));
				if (pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						int pos_sel = pItem_sel->GetHudItemPosPrimary();
						int pos_drg = pItem_dragged->GetHudItemPosPrimary();
						pItem_sel->SetHudItemPosPrimary(pos_drg);
						pItem_dragged->SetHudItemPosPrimary(pos_sel);
					}
					else
					{
						int pos_sel = pItem_sel->GetHudItemPosSecondary();
						int pos_drg = pItem_dragged->GetHudItemPosSecondary();
						pItem_sel->SetHudItemPosSecondary(pos_drg);
						pItem_dragged->SetHudItemPosSecondary(pos_sel);
					}
					//start_drag = false;
					ShowDraggedItemIcon(false, current_dragged_item);
					current_dragged_item = 0;
					item_for_drag_exchandge = 0;
					current_dragged_item_slot = -1;
					return;
				}

				if (!pItem_sel && pItem_dragged)
				{
					if (prim_inv_items_pos)
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosPrimary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosPrimary(pItem_dragged->GetHudItemPosPrimary());
					}
					else
					{
						if (slot_for_drag_exchandge >= 0)
							pItem_dragged->SetHudItemPosSecondary(slot_for_drag_exchandge);
						else
							pItem_dragged->SetHudItemPosSecondary(pItem_dragged->GetHudItemPosSecondary());
					}
					ShowDraggedItemIcon(false, current_dragged_item);
				}
			}
		}

		if (item_drop_request)
		{
			item_drop_request = false;
			ShowDraggedItemIcon(false, 0);
			current_dragged_item = 0;
			item_for_drag_exchandge = 0;
			current_dragged_item_slot = -1;
			return;
		}
		//start_drag = false;
		ShowDraggedItemIcon(false, current_dragged_item);
		current_dragged_item = 0;
		item_for_drag_exchandge = 0;
		current_dragged_item_slot = -1;
	}
	else if (!strcmp(strCommand, "dragAndDropDragOutEquipped"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			if (nwAction->Current_inventory_selection_mode == nwAction->ePISMode_drop || nwAction->Current_inventory_selection_mode == nwAction->ePISMode_combine)
				return;
		}
		int id = -1;
		id = GetItemInvSlotIdFromEquippedVisualSlot(strArgs);
		//CryLogAlways("slot name == %s", strArgs);
		//CryLogAlways("GetItemInvSlotIdFromEquippedVisualSlot == %i", id);
		if (id < 0)
			id = -1;

		current_equipment_dragged_slot = strArgs;
		IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
		if (pElement)
		{
			SFlashVarValue args[5] = { id, "", 0, 0, 0 };
			if(id >= 0)
				pElement->GetFlashPlayer()->Invoke("addItemInSlot", args, 5);

			SFlashVarValue args2[5] = { strArgs, "", 0, 0, 0 };
			pElement->GetFlashPlayer()->Invoke("addEquippedItem", args2, 5);
			pElement->GetFlashPlayer()->Render();
		}
	}
	else if (!strcmp(strCommand, "dragAndDropDragOverEquipped"))
	{

	}
	else if (!strcmp(strCommand, "dragAndDropMouseMoveNvOnEquipped"))
	{
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		if (id > 0)
		{
			item_for_drag_exchandge = id;
		}
		else
			item_for_drag_exchandge = 0;
	}
	else if (!strcmp(strCommand, "dragAndDropMouseMoveOnEquipped"))
	{
		int id = GetItemInvSlotIdFromEquippedVisualSlot(strArgs);
		if (start_drag)
			slot_for_drag_exchandge = id;
	}
}

void CHUDCommon::AddPlayerDollToScreen(bool add, bool delete_mrg3d)
{
	if (g_pGameCVars->hud_enable_player_model_in_inventory_menu == 0)
		return;

	string sUIElement = "Inv";
	string sMovieClipName = "Inv:player_model_clip";

	if (!add)
	{
		if (delete_mrg3d)
			CMenuRender3DModelMgr::Release(true);

		ITexture *tex = NULL;
		//unbind tex from MC
		if (gEnv->pConsole->GetCVar("r_UsePersistentRTForModelHUD")->GetIVal() > 0)
			tex = gEnv->pRenderer->EF_LoadTexture("$ModelHUD");
		else
			tex = gEnv->pRenderer->EF_LoadTexture("$BackBuffer");

		IUIElement* pElement = gEnv->pFlashUI->GetUIElement(sUIElement.c_str());
		IUIElement* pUIElement = pElement ? pElement->GetInstance(0) : NULL;
		if (pUIElement)
			pUIElement->UnloadTexFromMc(sMovieClipName.c_str(), tex);
		
		id_hud_pl = 0;
		return;
	}

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	INDENT_LOG_DURING_SCOPE();

	//CMenuRender3DModelMgr* pModelManager = CMenuRender3DModelMgr::GetInstance();
	//if (!pModelManager)
	//{
		CMenuRender3DModelMgr* pModelManager = new CMenuRender3DModelMgr();
	//}

	if (!pModelManager)
		return;

	if (!pActor->GetEntity()->GetCharacter(0))
		return;

	//CryLogAlways("try to add player doll");
	CMenuRender3DModelMgr::SSceneSettings sceneSettings;
	sceneSettings.fovScale = 0.5f;
	sceneSettings.fadeInSpeed = 0.01f;
	sceneSettings.flashEdgeFadeScale = 0.2f;
	sceneSettings.ambientLight = Vec4(0.f, 0.f, 0.f, 0.8f);
	sceneSettings.lights.resize(3);
	sceneSettings.lights[0].pos.Set(-25.f, -10.f, 30.f);
	sceneSettings.lights[0].color.Set(5.f, 6.f, 5.5f);
	sceneSettings.lights[0].specular = 4.f;
	sceneSettings.lights[0].radius = 400.f;
	sceneSettings.lights[1].pos.Set(25.f, -4.f, 30.f);
	sceneSettings.lights[1].color.Set(0.7f, 0.7f, 0.7f);
	sceneSettings.lights[1].specular = 10.f;
	sceneSettings.lights[1].radius = 400.f;
	sceneSettings.lights[2].pos.Set(60.f, 40.f, 10.f);
	sceneSettings.lights[2].color.Set(0.5f, 1.0f, 0.7f);
	sceneSettings.lights[2].specular = 10.f;
	sceneSettings.lights[2].radius = 400.f;
	pModelManager->SetSceneSettings(sceneSettings);
	

	const char * usePlayerModelName = pActor->GetEntity()->GetCharacter(0)->GetFilePath();
	CMenuRender3DModelMgr::SModelParams params;
	params.pFilename = usePlayerModelName;
	params.posOffset.Set(0.1f, 0.f, -0.2f);
	params.rot.Set(0.f, 0.f, 3.5f);
	params.scale = 1.0f;
	params.pName = "player_character";
	params.screenRect[0] = 0.f;
	params.screenRect[1] = 0.f;
	params.screenRect[2] = 0.3f;
	params.screenRect[3] = 0.8f;
	params.secondScale = 1.0f;
	params.userRotScale = 1.0f;
	const float ANIM_SPEED_MULTIPLIER = 0.5f;
	characterModelIndex = pModelManager->AddModel(params);
	pModelManager->UpdateAnim(characterModelIndex, "idle_01", ANIM_SPEED_MULTIPLIER);
	id_hud_pl = pModelManager->GetInstById(characterModelIndex);
	request_for_delayed_update_player_doll = true;
	//pModelManager->GetInstance
	//idle_01/stand_tac_idle_nw_3p_01

	ITexture *tex = NULL;
	//Fetch texture and send to movieclip
	if (gEnv->pConsole->GetCVar("r_UsePersistentRTForModelHUD")->GetIVal() > 0)
		tex = gEnv->pRenderer->EF_LoadTexture("$ModelHUD");
	else
		tex = gEnv->pRenderer->EF_LoadTexture("$BackBuffer");

	//gEnv->pRenderer->EF_DuplicateRO()->GetRE()->
	string sStr = "Inv:player_model_clip";
	string::size_type sPos = sStr.find(':');
	sUIElement = sStr.substr(0, sPos);
	sMovieClipName = sStr.substr(sPos + 1);

	IUIElement* pElement = gEnv->pFlashUI->GetUIElement(sUIElement.c_str());
	IUIElement* pUIElement = pElement ? pElement->GetInstance(0) : NULL;
	if (pUIElement)
		pUIElement->LoadTexIntoMc(sMovieClipName.c_str(), tex);
	else
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "CHUDCommon: Movieclip not found");

	if (pUIElement)
	{
		SFlashDisplayInfo info;
		if (pUIElement->GetMovieClip(sMovieClipName.c_str()))
			pUIElement->GetMovieClip(sMovieClipName.c_str())->GetDisplayInfo(info);

		character_model_clip_pose.x = info.GetX();
		character_model_clip_pose.y = info.GetY();
		int iX = int(character_model_clip_pose.x), iY = int(character_model_clip_pose.y);
		pElement->GetFlashPlayer()->ClientToScreen(iX, iY);
		int x, y, width, height;
		float aspect;
		pElement->GetFlashPlayer()->GetViewport(x, y, width, height, aspect);
		float fX = (float)iX / (float)width;
		float fY = (float)iY / (float)height;
		gEnv->pRenderer->GetViewport(&x, &y, &width, &height);
		iX = (int)(fX * (float)width);
		iY = (int)(fY * (float)height);

		character_model_clip_pose.x = iX;
		character_model_clip_pose.y = iY;
	}
}

void CHUDCommon::DebugFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "debugCLLogAlways"))
	{
		CryLogAlways("Flash special fsCommand debug message - %s", strArgs);
	}
}

void CHUDCommon::PlaySoundUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::MapUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::InventoryUIFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "invCSlotPressed"))
	{
		UpdateInventory(1);
		EntityId id = static_cast<EntityId>(atoi(strArgs));
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		if (start_drag)
		{
			start_drag = false;
			return;
		}

		nwAction->InventoryItemPressed(id);
		UpdateInventory(0.5);
	}
	else if (!strcmp(strCommand, "invCSlotSelected"))
	{
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->InventorySelectItem(id);
		}
	}
	else if (!strcmp(strCommand, "invCSlotDeselected"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		nwAction->InventoryDeselectPressed(0);
	}
	else if (!strcmp(strCommand, "invCxClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowInventory();
	}
	else if (!strcmp(strCommand, "invCSectionPressed"))
	{
		if (strArgs)
		{
			int tab = (atoi(strArgs));
			TabPressed(tab);
		}
	}
	else if (!strcmp(strCommand, "invCChestSlotSelected"))
	{
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->LootSelectItem(id);
		}
	}
	else if (!strcmp(strCommand, "invCChestSlotDeselected"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		nwAction->LootDeselectItem(0);
	}
	else if (!strcmp(strCommand, "invCChestSlotPressed"))
	{
		UpdateChestInventory(1);
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->LootItemPressed(id);
		}

		UpdateChestInventory(0.5);
	}
	else if (!strcmp(strCommand, "invCSelectedPlayerMenuItemInChestUI"))
	{
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->LootLooterSelectItem(id);
		}
	}
	else if (!strcmp(strCommand, "invCDeselectedPlayerMenuItemInChestUI"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		nwAction->LootLooterDeselectItem(0);
	}
	else if (!strcmp(strCommand, "invCPlayerSlotPressedInChestUI"))
	{
		UpdateChestInventory(1);
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->LootLooterItemPressed(id);
		}

		UpdateChestInventory(0.5);
	}
	else if (!strcmp(strCommand, "invCTakeAllButtonPressed"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		nwAction->LootTakeAllItems();
	}
	else if (!strcmp(strCommand, "invCChestUIPressedSectionITS"))
	{
		if (strArgs)
		{
			int tab = (atoi(strArgs));
			ChestTabPressed(tab);
		}
	}
	else if (!strcmp(strCommand, "invCOnPlayerInvButtonPressedInChest"))
	{
		chest_or_shop_showed_player_inv = true;
		IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
		if (!pLootUI)
			return;

		ClearChestInventory();
		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pLootUI->GetFlashPlayer()->Invoke("setChestInvVisible", args_p_inv, 1);
		UpdateChestInventory(10);
	}
	else if (!strcmp(strCommand, "invCOnChestInvButtonPressed"))
	{
		chest_or_shop_showed_player_inv = false;
		IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
		if (!pLootUI)
			return;

		ClearChestInventory();
		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pLootUI->GetFlashPlayer()->Invoke("setChestInvVisible", args_p_inv, 1);
		UpdateChestInventory(10);
	}
	else if (!strcmp(strCommand, "invCxCloseChest"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowChestInv(false);
	}
	else if (!strcmp(strCommand, "invCContextLinePressed"))
	{
		int envent_id = static_cast<int>(atoi(strArgs));
		//const char* sssd = (strArgs);
		//CryLogAlways(sssd);
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		nwAction->ContextMenuItemPressed(envent_id);
	}
}

void CHUDCommon::ChrMenuUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::CreationMenuUIFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "crmCCameraUp"))
	{
		if (g_pGameCVars->cl_tpvDist > 1.0f)
			g_pGameCVars->cl_tpvDist -= 1.0f;
	}
	else if (!strcmp(strCommand, "crmCCameraDown"))
	{
		if (g_pGameCVars->cl_tpvDist < 3.5f)
			g_pGameCVars->cl_tpvDist += 1.0f;
	}
	else if (!strcmp(strCommand, "crmCxClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowCharacterMenu();
	}
	else if (!strcmp(strCommand, "crmCOnCreateNewCharacterPressed"))
	{

	}
	else if (!strcmp(strCommand, "crmCOnSelectCharacterPressed"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		IUIElement* pCharacter_creating_UI = gEnv->pFlashUI->GetUIElement("Character_creating_UI");
		if (pCharacter_creating_UI)
		{
			if (m_CharacterCreationVisible)
			{
				//CryLogAlways("onSelectCharacterPressed");
				pCharacter_creating_UI->GetFlashPlayer()->Invoke0("setSlots");
				string pth = p_PlCrSys->GetSavedCharactersListPath();
				XmlNodeRef SavedChrsNode = GetISystem()->LoadXmlFromFile(pth.c_str());
				XmlNodeRef SavedChrNode = NULL;
				if (SavedChrsNode)
				{
					int ch_count = SavedChrsNode->getChildCount();
					XmlString ftvx_name = "";
					XmlString ftvx_pth = "";
					int char_level = 1;
					SFlashVarValue args[5] = { 0, "", "", "", char_level };
					for (int i = 0; i < ch_count; ++i)
					{
						SavedChrNode = SavedChrsNode->getChild(i);
						if (SavedChrNode)
						{
							if (SavedChrNode->getAttr("name", ftvx_name))
							{
								CryLogAlways("onSelectCharacterPressed load char to flash , name = %s", ftvx_name.c_str());
								SavedChrNode->getAttr("file", ftvx_pth);
								SavedChrNode->getAttr("level", char_level);
								args[0] = i;
								args[2] = ftvx_pth.c_str();
								ftvx_name.replace("_", " ");
								args[3] = ftvx_name.c_str();
								args[4] = char_level;
								pCharacter_creating_UI->GetFlashPlayer()->Invoke("addCharacterListLine", args, 5);
							}
						}
					}
					/*const char* key = "";
					const char* val = "";
					//CryLogAlways("NumAttributes = %i", SavedChrsNode->getNumAttributes());
					for (int i = 0; i < SavedChrsNode->getNumAttributes(); i++)
					{
					if (SavedChrsNode->getAttributeByIndex(i, &key, &val))
					{
					//CryLogAlways("SavedCharacters key = %s val= %s", key, val);
					string nm_key = key;
					nm_key.replace("_", " ");
					SFlashVarValue args[5] = { i, "", val, nm_key.c_str(), 1 };
					pCharacter_creating_UI->GetFlashPlayer()->Invoke("addCharacterListLine", args, 5);
					}
					}*/
				}
			}
		}
	}
	else if (!strcmp(strCommand, "crmCCharacterSelectionMenuPressed"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		p_PlCrSys->LoadCharacterfromXmlFile(strArgs);
	}
	else if (!strcmp(strCommand, "crmCDeselectedCharacterSelNm"))
	{

	}
	else if (!strcmp(strCommand, "crmCSelectedCharacterSelNm"))
	{

	}
	else if (!strcmp(strCommand, "crmCxGTMMPressed"))
	{
		if (gEnv->pConsole)
		{
			if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
			{
				StartGoToMainMenuTimer();
			}
			else
			{
				if (m_CharacterCreationVisible)
					ShowCharacterCreatingMenu();

				gEnv->pConsole->ExecuteString("disconnect", true, true);
			}
		}
	}
	else if (!strcmp(strCommand, "crmCxCreatePressed"))
	{
		CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pPlayer)
			return;

		if (pPlayer && !pPlayer->GetEntity()->GetCharacter(0))
		{
			StatMsg("You need to select character gender");
			return;
		}
		else if (string(PathUtil::GetFileName(pPlayer->GetEntity()->GetCharacter(0)->GetFilePath())) == string("player_invisible"))
		{
			StatMsg("You need to select character gender");
			return;
		}

		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		if (pPlayer)
		{
			if (pPlayer->m_player_name.empty())
			{
				StatMsg("You need enter character name");
				return;
			}

			if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
			{
				StartCurrentInteractiveUIDisablingTimer();
			}
			else
			{
				//if (m_CharacterCreationVisible)
				ShowCharacterCreatingMenu();
			}

			string nm_xml = pPlayer->m_player_name + string(".xml");
			p_PlCrSys->SaveCharacterToXmlFile(nm_xml);
		}

		if (gEnv->pConsole)
		{
			gEnv->pConsole->ExecuteString("save", true, true);
		}
	}
	else if (!strcmp(strCommand, "crmCSetCharacterName"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		p_PlCrSys->SetCharacterName(strArgs);
	}
	else if (!stricmp(strCommand, "crmCSelHair"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int direction = atoi(strArgs);
		if (direction == 0)
			p_PlCrSys->PlayerHairSelectionPrev();
		else if (direction == 1)
			p_PlCrSys->PlayerHairSelectionNext();
	}
	else if (!stricmp(strCommand, "crmCSelHead"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int direction = atoi(strArgs);
		if (direction == 0)
			p_PlCrSys->PlayerFaceSelectionPrev();
		else if (direction == 1)
			p_PlCrSys->PlayerFaceSelectionNext();
	}
	else if (!stricmp(strCommand, "crmCSelEyes"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int direction = atoi(strArgs);
		if (direction == 0)
			p_PlCrSys->PlayerEyesSelectionPrev();
		else if (direction == 1)
			p_PlCrSys->PlayerEyesSelectionNext();
	}
	else if (!stricmp(strCommand, "crmCSelGender"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int gender = atoi(strArgs);
		if(gender == 0)
			p_PlCrSys->PlayerGenderSelectionMale();
		else if (gender == 1)
			p_PlCrSys->PlayerGenderSelectionFemale();
	}
	else if (!stricmp(strCommand, "crmCCreationComplete"))
	{
		if (m_CharacterCreationVisible)
			ShowCharacterCreatingMenu();

		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		p_PlCrSys->OnCharacterCreationCompleteEvent();
	}
	else if (!stricmp(strCommand, "crmCSelectEyes"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int eyes_id = static_cast<int>(atoi(strArgs));
		p_PlCrSys->SetEyes(eyes_id);
	}
	else if (!stricmp(strCommand, "crmCSelectHair"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int hair_id = static_cast<int>(atoi(strArgs));
		p_PlCrSys->SetHair(hair_id);
	}
	else if (!stricmp(strCommand, "crmCSelectHead"))
	{
		CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
		if (!p_PlCrSys)
			return;

		int head_id = static_cast<int>(atoi(strArgs));
		p_PlCrSys->SetFace(head_id);
	}
}

void CHUDCommon::SpellsMenuUIFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "splCSlotPressed"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		const char *nameid = (strArgs);
		ClearSpellSelection();
		nwAction->SpellPressed(nameid);
		UpdateSpellSelection(10.0f);
	}
	else if (!strcmp(strCommand, "splCSlotSelected"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		const char *nameid = (strArgs);
		nwAction->SpellSelected(nameid);
	}
	else if (!strcmp(strCommand, "splCSlotDeselected"))
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (!nwAction)
			return;

		const char *nameid = (strArgs);
		nwAction->SpellDeselected(nameid);
	}
	else if (!strcmp(strCommand, "splCSpellsSectionPressed"))
	{
		if (strArgs)
		{
			int tab = (atoi(strArgs));
			SpellsSelectionTabPressed(tab);
		}
	}
	else if (!strcmp(strCommand, "splCxClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowSpellSelectionMenu(false);
	}
}

void CHUDCommon::DialogsUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::TradeUIFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "trdCTraderSlotSelected"))
	{
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->SellerSelectItem(id);
		}
	}
	else if (!strcmp(strCommand, "trdCPlayerSlotSelected"))
	{
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->BuyerSelectItem(id);
		}
	}
	else if (!strcmp(strCommand, "trdCTraderSlotPressed"))
	{
		UpdateTradeSystem(1);
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->SellerItemPressed(id);
		}
		UpdateTradeSystem(0.5);
	}
	else if (!strcmp(strCommand, "trdCPlayerSlotPressed"))
	{
		UpdateTradeSystem(1);
		EntityId id = 0;
		if (strArgs)
		{
			id = EntityId(atoi(strArgs));
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (!nwAction)
				return;

			nwAction->BuyerItemPressed(id);
		}
		UpdateTradeSystem(0.5);
	}
	else if (!strcmp(strCommand, "trdCOnPlayerInvButtonPressed"))
	{
		chest_or_shop_showed_player_inv = true;
		IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
		if (!pShopInterface)
			return;

		ClearTradeSystem();
		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pShopInterface->GetFlashPlayer()->Invoke("enableSellerInv", args_p_inv, 1);
		UpdateTradeSystem(10);
	}
	else if (!strcmp(strCommand, "trdCOnMerchantInvButtonPressed"))
	{
		chest_or_shop_showed_player_inv = false;
		IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
		if (!pShopInterface)
			return;

		ClearTradeSystem();
		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pShopInterface->GetFlashPlayer()->Invoke("enableSellerInv", args_p_inv, 1);
		UpdateTradeSystem(10);
	}
	else if (!strcmp(strCommand, "trdCMercantUISectionPressedITS"))
	{
		if (strArgs)
		{
			int tab = (atoi(strArgs));
			ShopTabPressed(tab);
		}
	}
	else if (!strcmp(strCommand, "trdCxClose"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
		}
		else
			ShowTradeSystem(0, false);
	}
}

void CHUDCommon::QuestUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::MhudUIFSRealization(const char * strCommand, const char * strArgs)
{
}

void CHUDCommon::CommonUIFSRealization(const char * strCommand, const char * strArgs)
{
	if (!strcmp(strCommand, "cuiCBooKxPressed"))
	{
		if (g_pGameCVars->hud_interactive_ui_delayed_closing_on_close_by_button > 0)
		{
			StartCurrentInteractiveUIDisablingTimer();
			if (m_BookVisible)
			{
				if (!m_TimerEnInv)
				{
					m_TimerEnInv = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::EnInventory), NULL);
				}
			}
		}
		else
		{
			if (m_ChestInvVisible)
			{
				ShowChestInv(false);
			}
			else if (m_BookVisible)
			{
				ShowInventory();
			}
			else if (m_TradeSystemVisible)
			{

			}
			else if (m_QuestSystemVisible)
			{

			}
			else if (m_SpellSelectionVisible)
			{

			}
		}
	}
}

void CHUDCommon::AddEntityModelToHud(EHudEntityType type, EntityId id, string ui_element, string ui_clip)
{
	bool need_apply_SSS = false;
	CMenuRender3DModelMgr* pModelManager = CMenuRender3DModelMgr::GetInstance();
	if (!pModelManager)
	{
		pModelManager = new CMenuRender3DModelMgr();
		need_apply_SSS = true;
	}

	if (!pModelManager)
		return;

	IEntity *pEntity = gEnv->pEntitySystem->GetEntity(id);
	if (!pEntity)
		return;

	if (need_apply_SSS)
	{
		CMenuRender3DModelMgr::SSceneSettings sceneSettings;
		sceneSettings.fovScale = 0.5f;
		sceneSettings.fadeInSpeed = 0.01f;
		sceneSettings.flashEdgeFadeScale = 0.2f;
		sceneSettings.ambientLight = Vec4(0.f, 0.f, 0.f, 0.8f);
		sceneSettings.lights.resize(3);
		sceneSettings.lights[0].pos.Set(-25.f, -10.f, 30.f);
		sceneSettings.lights[0].color.Set(5.f, 6.f, 5.5f);
		sceneSettings.lights[0].specular = 4.f;
		sceneSettings.lights[0].radius = 400.f;
		sceneSettings.lights[1].pos.Set(25.f, -4.f, 30.f);
		sceneSettings.lights[1].color.Set(0.7f, 0.7f, 0.7f);
		sceneSettings.lights[1].specular = 10.f;
		sceneSettings.lights[1].radius = 400.f;
		sceneSettings.lights[2].pos.Set(60.f, 40.f, 10.f);
		sceneSettings.lights[2].color.Set(0.5f, 1.0f, 0.7f);
		sceneSettings.lights[2].specular = 10.f;
		sceneSettings.lights[2].radius = 400.f;
		pModelManager->SetSceneSettings(sceneSettings);
	}
	ITexture *tex = NULL;
	switch (type)
	{
		case eHEType_none:
		{
							 break;
		}
		case eHEType_item:
		{
							 CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
							 if (pItem)
							 {
								 string nmnv = pItem->GetEntity()->GetName();
								 string nmnv_add = "HUD_mdl_";
								 string nmnn = nmnv_add + nmnv;
								 if (pItem->GetEntity()->GetStatObj(1))
								 {
									 uint32 flags = FT_USAGE_ALLOWREADSRGB;
									 const char * mModelName = pItem->GetEntity()->GetStatObj(1)->GetFilePath();
									 CMenuRender3DModelMgr::SModelParams params;
									 params.pFilename = mModelName;
									 params.posOffset.Set(0.1f, 0.f, -0.2f);
									 params.rot.Set(0.f, 0.f, 3.5f);
									 params.scale = 1.0f;
									 params.pName = nmnn.c_str();
									 params.screenRect[0] = 0.f;
									 params.screenRect[1] = 0.f;
									 params.screenRect[2] = 0.3f;
									 params.screenRect[3] = 0.8f;
									 params.secondScale = 1.0f;
									 params.userRotScale = 1.0f;
									 pModelManager->AddModel(params);
									 tex = gEnv->pRenderer->EF_LoadTexture("$BackBuffer", flags);
									 //AddPlayerDollToScreen(false, true);
									 //IDynTextureSource *tex_dn = NULL;
									 //tex_dn = gEnv->pRenderer->EF_LoadDynTexture("$BackBuffer");
									 //tex = gEnv->pRenderer->
									 //tex->SetClamp(false);
									 //tex->SetKeepSystemCopy(false);
									 //gEnv->pRenderer->CaptureFrameBufferCallBack
									 //CryLogAlways(tex_dn->GetSourceFilePath());
									// CryLogAlways(tex->GetName());
									// STexComposition *pTexComp = new STexComposition;
									// pTexComp->pTexture = tex;
									// pTexComp->nDstSlice = tex->GetTextureDstFormat();
									 //pTexComp->nSrcSlice = tex->GetTextureSrcFormat();
									// tex = gEnv->pRenderer->EF_CreateCompositeTexture(eTT_3D, "3434343434tst.jpg", 32, 32, tex->GetDepth(), tex->GetNumMips(), flags, eTF_R8G8B8A8, pTexComp, tex->GetDataSize());
									// CryLogAlways(tex_dn->GetTexture()->GetName());
									// byte* pBuffer = (byte*)tex->GetData32();
									 //CCryFile *pImg = new CCryFile;
									// pImg->Write(tex->GetData32(), tex->GetDataSize());
									 int dddc = 0;
									 tex = gEnv->pRenderer->EF_LoadTexture("Libs/UI/Icons/empty_charm_slot.DDS", flags);
									 //gEnv->pRenderer->WriteJPG(tex->GetData32(11, 11, tex->LockData(dddc)), 64, 64, "34343434341tst.jpg", 32);
									 //pBuffer += tex->GetDataSize();
									// if (pBuffer)
									// {
									//	 CryLogAlways("try to write jpg");
										 //gEnv->pRenderer->WriteJPG(pBuffer, 64, 64, "34343434341tst.jpg", 8);
									// }
									/* IImageFile *pImage = gEnv->pRenderer->EF_LoadImage(tex_dn->GetSourceFilePath(), 0);
									 if (pImage)
									 {
										 CryLogAlways("img loaded");
										 pImage->mfGet_image(0);
										 gEnv->pRenderer->WriteJPG(pImage->mfGet_image(0), 64, 64, "3434343434tst.jpg", 8);
										 //tex->GetData32();
										 //gEnv->pRenderer->;
										 /*int tex_id = gEnv->pRenderer->SF_CreateTexture(tex->GetWidth(), tex->GetHeight(), tex->GetNumMips(), pImage->mfGet_image(0), tex->GetTextureDstFormat(), flags);
										 if (tex_id > 0)
										 {
											 tex = gEnv->pRenderer->EF_GetTextureByID(tex_id);
										 }*/
										 //gEnv->p3DEngine->
										 //pModelManager->
									 /*}*/

								 }
								 //AddPlayerDollToScreen(true, false);
							 }
							 break;
		}
		case eHEType_character:
		{
							 break;
		}
		case eHEType_brush:
		{
							 break;
		}
		case eHEType_standart:
		{
							 break;
		}
		break;
	}

	/*ITexture *tex = NULL;
	if (gEnv->pConsole->GetCVar("r_UsePersistentRTForModelHUD")->GetIVal() > 0)
		tex = gEnv->pRenderer->EF_LoadTexture("$ModelHUD");
	else
		tex = gEnv->pRenderer->EF_LoadTexture("$BackBuffer");*/

	IUIElement* pElement = gEnv->pFlashUI->GetUIElement(ui_element.c_str());
	IUIElement* pUIElement = pElement ? pElement->GetInstance(0) : NULL;
	if (pUIElement && tex)
		pUIElement->LoadTexIntoMc(ui_clip.c_str(), tex);
	else
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "CHUDCommon: Movieclip not found");
}

void CHUDCommon::ClearHudModelsRnd()
{
	CMenuRender3DModelMgr* pModelManager = CMenuRender3DModelMgr::GetInstance();
	if (pModelManager)
	{
		CMenuRender3DModelMgr::Release(true);
	}
}
//-----------------------------------------------------------------------------------------------------

void CHUDCommon::ShowInventory()
{
	//if (m_ChestInvVisible)
	//	ShowChestInv(false);
	if (!Inv_loaded && !CheckForOpenedOthersUI(eCTOSUIOpen_Inventory))
		return;

	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	if (pElement)
	{
		if (!pElement->GetFlashPlayer())
			return;

		if (ChrMenu_loaded)
			return;

		if (Inv_cont_menu_loaded)
			return;

		if (Inv_ItemInfo_loaded)
			ShowItemInformation(false, false, 0);

		if (Inv_dragged_item_loaded)
			ShowDraggedItemIcon(false, 0);

		if (!Inv_loaded)
		{
			pElement->Reload();
			pElement->GetFlashPlayer()->SetVisible(true);
			pElement->SetVisible(true);
			pElement->GetFlashPlayer()->Pause(false);
			if (g_pGameCVars->hud_inventory_type == 1)
			{
				SFlashVarValue arg[1] = { true };
				pElement->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
			}
			else
			{
				SFlashVarValue arg[1] = { false };
				pElement->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
			}
			///
			if (g_pGameCVars->hud_inventory_slots_scale_global != 1.0f)
			{
				SFlashVarValue arg[1] = { g_pGameCVars->hud_inventory_slots_scale_global };
				pElement->GetFlashPlayer()->Invoke("setSlotsScale", arg, 1);
			}
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			SFlashVarValue args[1] = { pActor->GetMaxInvSlots() };
			pElement->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
			IInventory *pInventory = pActor->GetInventory();
			CreateUIActionFilter(eHUIAFT_interactive, true);
			OnInterInUIMode(eCTOSUIOpen_Inventory, true);
			int tab = g_pGameCVars->g_player_inv_sel_tab_id;
			SFlashVarValue args_tab[1] = { tab };
			pElement->GetFlashPlayer()->Invoke("selectInventorySection", args_tab, 1);
			pElement->GetFlashPlayer()->SetFSCommandHandler(this);
			//pElement->GetFlashPlayer()->SendCursorEvent
			Inv_loaded = true;
			UpdateInventory(1.0f);
			//--------------------------------
			AddPlayerDollToScreen(true);
			//--------------------------------
		}
		else
		{
			//--------------------------------
			AddPlayerDollToScreen(false);
			//--------------------------------
			CreateUIActionFilter(eHUIAFT_interactive, false);
			OnInterInUIMode(eCTOSUIOpen_Inventory, false);
			pElement->SetVisible(false);
			pElement->GetFlashPlayer()->SetVisible(false);
			pElement->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			Inv_loaded = false;
		}
	}

	
}

//-----------------------------------------------------------------------------------------------------

void CHUDCommon::ShowInvContextMenu(bool show)
{
	IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
	if (pInvContextMenu)
	{
		if (!pInvContextMenu->GetFlashPlayer())
			return;

		if (show)
		{
			pInvContextMenu->Reload();
			pInvContextMenu->GetFlashPlayer()->SetVisible(true);
			pInvContextMenu->SetVisible(true);
			CreateUIActionFilter(eHUIAFT_interactive, true);
			pInvContextMenu->GetFlashPlayer()->SetFSCommandHandler(this);
			Inv_cont_menu_loaded = show;
		}
		else
		{
			pInvContextMenu->SetVisible(false);
			pInvContextMenu->GetFlashPlayer()->SetVisible(false);
			CreateUIActionFilter(eHUIAFT_interactive, false);
			pInvContextMenu->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			Inv_cont_menu_loaded = show;
		}
	}
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::ShowCharacterMenu()
{
	if (!ChrMenu_loaded && !CheckForOpenedOthersUI(eCTOSUIOpen_CharacterMenu))
		return;

	IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
	if (pChrMenu_hud)
	{
		if (!pChrMenu_hud->GetFlashPlayer())
			return;

		if (Inv_loaded)
			return;

		if (Inv_cont_menu_loaded)
			return;

		if (!ChrMenu_loaded)
		{
			pChrMenu_hud->Reload();
			pChrMenu_hud->GetFlashPlayer()->SetVisible(true);
			pChrMenu_hud->SetVisible(true);
			CreateUIActionFilter(eHUIAFT_interactive, true);
			OnInterInUIMode(eCTOSUIOpen_CharacterMenu, true);
			UpdateMaxHealth();
			UpdateMaxStamina();
			UpdateMaxMagicka();
			UpdateStrength();
			UpdateAgility();
			UpdateWillpower();
			UpdateEndurance();
			UpdatePersonality();
			UpdateIntelligence();
			UpdateLevel();
			//UpdateAttPoints();
			//UpdateXPPoints();
			CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			float fHealth = (pActor->GetMaxHealth()) *1.0f - 0.0f;
			float fStamina = (pActor->GetMaxStamina()) *1.0f - 0.0f;
			float fMagicka = (pActor->GetMaxMagicka()) *1.0f - 0.0f;
			float fStrength = (pActor->GetActorStrength()) *1.0f - 0.0f;
			float fWillpower = (pActor->GetWillpower()) *1.0f - 0.0f;
			float fAgility = (pActor->GetAgility()) *1.0f - 0.0f;
			float fEndurance = (pActor->GetEndurance()) *1.0f - 0.0f;
			float fIntelligence = (pActor->GetIntelligence()) *1.0f - 0.0f;
			float fPersonality = (pActor->GetPersonality()) *1.0f - 0.0f;
			float fLevel = (pActor->GetLevel()) *1.0f - 0.0f;
			float fAttPoints = (g_pGameCVars->g_leveluppointsstat) *1.0f - 0.0f;

			SFlashVarValue stat_str[1] = { (int)fStrength };
			SFlashVarValue stat_wlp[1] = { (int)fWillpower };
			SFlashVarValue stat_agl[1] = { (int)fAgility };
			SFlashVarValue stat_endr[1] = { (int)fEndurance };
			SFlashVarValue stat_int[1] = { (int)fIntelligence };
			SFlashVarValue stat_pers[1] = { (int)fPersonality };
			SFlashVarValue stat_lvl[1] = { (int)fLevel };
			SFlashVarValue stat_hp[1] = { (int)fHealth };
			SFlashVarValue stat_mp[1] = { (int)fMagicka };
			SFlashVarValue stat_st[1] = { (int)fStamina };
			SFlashVarValue stat_ap[1] = { (int)fAttPoints };

			pChrMenu_hud->GetFlashPlayer()->Invoke("setStrength", stat_str, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setLevel", stat_lvl, 1);
			//float fAttPoints = (g_pGameCVars->g_leveluppointsstat) *1.0f - 0.0f;
			//m_animCharacterMenu.Invoke("setAttPoints", (int)fAttPoints);
			//float fXP = (pPlayer->GetLevelXp()) *1.0f - 0.0f;
			//m_animCharacterMenu.Invoke("setXP", (int)fXP);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setIntelligence", stat_int, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setPersonality", stat_pers, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setEndurance", stat_endr, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setWillpower", stat_wlp, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setAgility", stat_agl, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setHealth", stat_hp, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setStamina", stat_st, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setMagicka", stat_mp, 1);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setAttPoints", stat_ap, 1);
			//float fXPNL = (pPlayer->GetXpToNextLevel()) *1.0f - 0.0f;
			//m_animCharacterMenu.Invoke("setNEXTLVLXP", (int)fXPNL);

			m_str_val_cm = (int)fStrength;
			m_agl_val_cm = (int)fAgility;
			m_int_val_cm = (int)fIntelligence;
			m_endr_val_cm = (int)fEndurance;
			m_will_val_cm = (int)fWillpower;
			m_pers_val_cm = (int)fPersonality;
			m_Mhp_val_cm = (int)fHealth;
			m_Mst_val_cm = (int)fStamina;
			m_Mmg_val_cm = (int)fMagicka;

			pChrMenu_hud->GetFlashPlayer()->SetFSCommandHandler(this);
			ChrMenu_loaded = true;
		}
		else
		{
			CreateUIActionFilter(eHUIAFT_interactive, false);
			OnInterInUIMode(eCTOSUIOpen_CharacterMenu, false);
			pChrMenu_hud->SetVisible(false);
			pChrMenu_hud->GetFlashPlayer()->SetVisible(false);
			pChrMenu_hud->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			ChrMenu_loaded = false;
		}
	}
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::ShowCharacterCreatingMenu()
{
	if (!m_CharacterCreationVisible && !CheckForOpenedOthersUI(eCTOSUIOpen_CharacterCreatingMenu))
		return;

	CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
	if (!p_PlCrSys)
		return;

	IUIElement* pCharacter_creating_UI = gEnv->pFlashUI->GetUIElement("Character_creating_UI");
	if (pCharacter_creating_UI)
	{
		if (!m_CharacterCreationVisible)
		{
			//-----------Temp fix--------------------------------------------------------------------------
			if (g_pGame->m_hud_show_timer > 0.0f || !pCharacter_creating_UI->GetFlashPlayer())
			{
				CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
				if (pGameGlobals)
					pGameGlobals->enable_character_creating_menu_at_game_start = true;

				return;
			}
			//--------------------------------------------------------------------------------------------
			//CryLogAlways("CHUDCommon::ShowCharacterCreatingMenu called");
			pCharacter_creating_UI->Reload();
			pCharacter_creating_UI->GetFlashPlayer()->SetVisible(true);
			pCharacter_creating_UI->SetVisible(true);
			pCharacter_creating_UI->GetFlashPlayer()->SetFSCommandHandler(this);
			CreateUIActionFilter(eHUIAFT_dialog, true);
			OnInterInUIMode(eCTOSUIOpen_CharacterCreatingMenu, true);
			SFlashVarValue arg[1] = { 1 };
			pCharacter_creating_UI->GetFlashPlayer()->Invoke("setVisCharCrtButton", arg, 1);
			string pth = p_PlCrSys->GetSavedCharactersListPath();
			arg[0] = 0;
			XmlNodeRef SavedChrsNode = GetISystem()->LoadXmlFromFile(pth.c_str());
			if (SavedChrsNode)
			{
				if (SavedChrsNode->getChildCount() > 0)
				{
					arg[0] = 1;
				}
			}
			pCharacter_creating_UI->GetFlashPlayer()->Invoke("setVisCharSelButton", arg, 1);
			m_CharacterCreationVisible = true;
			p_PlCrSys->OnOpenedCharacterCreationMenu();
			if (MainHud_loaded)
			{
				ShowMainHud();
			}
		}
		else
		{
			pCharacter_creating_UI->GetFlashPlayer()->SetVisible(false);
			pCharacter_creating_UI->SetVisible(false);
			pCharacter_creating_UI->GetFlashPlayer()->SetFSCommandHandler(NULL);
			CreateUIActionFilter(eHUIAFT_dialog, false);
			OnInterInUIMode(eCTOSUIOpen_CharacterCreatingMenu, false);
			m_CharacterCreationVisible = false;
			p_PlCrSys->OnClosedCharacterCreationMenu();
			if (!MainHud_loaded)
			{
				ShowMainHud();
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------
void CHUDCommon::ShowMainHud()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if(!pActor)
		return;

	if (g_pGameCVars->hud_disable_all_main_ui_on_player_death > 0)
	{
		if (!MainHud_loaded && pActor->IsDead())
		{
			return;
		}
	}

	if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
	{
		if (AnyInteractiveUIOpened() && !MainHud_loaded)
		{
			return;
		}
	}

	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	if (pMain_hud)
	{
		if (!pMain_hud->GetFlashPlayer())
			return;

		if (!MainHud_loaded)
		{
			for (int i = 0; i < 5; i++)
			{
				old_main_player_values[i] = 100.0f;
				player_values_alpha_timers[i] = 0;
			}
			pMain_hud->Reload();
			pMain_hud->GetFlashPlayer()->SetVisible(true);
			pMain_hud->SetVisible(true);
			MainHud_loaded = true;
			float fHealth = (pActor->GetHealth() / (float)pActor->GetMaxHealth()) * 100.0f + 1.0f;
			float fHealthCur = (pActor->GetHealth()) * 1.0f;

			if (0 != fHealth)
			{
				SFlashVarValue args[1] = { (int)fHealth };
				pMain_hud->GetFlashPlayer()->Invoke("setHealth", args, 1);
			}

			float fStamina = (pActor->GetStamina() / (float)pActor->GetMaxStamina()) * 100.0f + 1.0f;
			float fStaminaCur = (pActor->GetStamina()) * 1.0f;

			if (0 != fStamina)
			{
				SFlashVarValue args[1] = { (int)fStamina };
				pMain_hud->GetFlashPlayer()->Invoke("setStamina", args, 1);
			}

			float fMagicka = (pActor->GetMagicka() / (float)pActor->GetMaxMagicka()) * 100.0f + 1.0f;
			float fMagickaCur = (pActor->GetMagicka()) * 1.0f;

			if (0 != fMagicka)
			{
				SFlashVarValue args[1] = { (int)fMagicka };
				pMain_hud->GetFlashPlayer()->Invoke("setMagicka", args, 1);
			}

			player_values_alpha_timers[3] = 100.0f;
		}
		else
		{
			pMain_hud->SetVisible(false);
			pMain_hud->GetFlashPlayer()->SetVisible(false);
			MainHud_loaded = false;
		}
	}
	IUIElement* pStaticMessages_hud = gEnv->pFlashUI->GetUIElement("StaticMessages");
	if (pStaticMessages_hud)
	{
		if (!StatMsg_loaded)
		{
			pStaticMessages_hud->Reload();
			pStaticMessages_hud->GetFlashPlayer()->SetVisible(true);
			pStaticMessages_hud->SetVisible(true);
			StatMsg_loaded = true;
		}
		else
		{
			if (gEnv->pGame && gEnv->pGame->GetIGameFramework() && gEnv->pGame->GetIGameFramework()->IsGamePaused())
			{
				pStaticMessages_hud->SetVisible(false);
				pStaticMessages_hud->GetFlashPlayer()->SetVisible(false);
				StatMsg_loaded = false;
			}
		}
	}
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (pBuffsScreenInfo)
	{
		if (!BuffInfo_loaded)
		{
			pBuffsScreenInfo->Reload();
			pBuffsScreenInfo->GetFlashPlayer()->SetVisible(true);
			pBuffsScreenInfo->SetVisible(true);
			BuffInfo_loaded = true;
			ITimeOfDay* pTd = gEnv->p3DEngine->GetTimeOfDay();
			if (pTd)
			{
				float timeH = pTd->GetTime();
				float timeM;
				float timeS;
				timeM = (pTd->GetTime() * 60) - (int)timeH * 60;
				timeS = ((pTd->GetTime() * 60) * 60) - (int)(timeH * 60) * 60;
				SFlashVarValue argH[1] = { (int)timeH };
				SFlashVarValue argM[1] = { (int)timeM };
				SFlashVarValue argS[1] = { (int)timeS };
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeH", argH, 1);
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeM", argM, 1);
				pBuffsScreenInfo->GetFlashPlayer()->Invoke("setTimeS", argS, 1);
			}
			sd_ui_timers[0] = 0.1f;
		}
		else
		{
			pBuffsScreenInfo->SetVisible(false);
			pBuffsScreenInfo->GetFlashPlayer()->SetVisible(false);
			BuffInfo_loaded = false;
		}
	}
}

void CHUDCommon::DisableContextMenu()
{
	ShowInvContextMenu(false);
	//unsupported, double call less bugged
	//Inv_loaded = false;
	//ShowInventory();
	//ShowInventory();
	if (Inv_loaded)
	{
		IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
		if (pElement)
		{
			CreateUIActionFilter(eHUIAFT_interactive, true);
			pElement->GetFlashPlayer()->SetVisible(true);
			pElement->SetVisible(true);
			pElement->GetFlashPlayer()->Pause(false);
			pElement->GetFlashPlayer()->SetFSCommandHandler(this);
			UpdateInventory(1.0f);
		}
	}
}

void CHUDCommon::StatMsg(const char* msg)
{
	IUIElement* pStaticMessages_hud = gEnv->pFlashUI->GetUIElement("StaticMessages");
	if (pStaticMessages_hud)
	{
		SFlashVarValue args[1] = { msg };
		pStaticMessages_hud->GetFlashPlayer()->Invoke("showStatMsg", args, 1);
	}
}

void CHUDCommon::Str_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IEntity *pEntity = pActor->GetEntity();
		SmartScriptTable props;
		SmartScriptTable propsStat;
		IScriptTable* pScriptTable = pEntity->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		props->GetValue("Stat", propsStat);

		float str = 1.0f;
		propsStat->GetValue("strength", str);
		if (g_pGameCVars->g_leveluppointsstat > 0)
		{
			str += 1.0f;
			pActor->SetStrength(pActor->GetRealValStrength() + 1);
			propsStat->SetValue("strength", str);
			g_pGameCVars->g_leveluppointsstat -= 1;
		}
		else
		{
			str += 0.0f;
			pActor->SetStrength(pActor->GetRealValStrength() + 0);
			propsStat->SetValue("strength", str);
			g_pGameCVars->g_leveluppointsstat -= 0;
		}
	}
}

void CHUDCommon::Str_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IEntity *pEntity = pActor->GetEntity();
		SmartScriptTable props;
		SmartScriptTable propsStat;
		IScriptTable* pScriptTable = pEntity->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		props->GetValue("Stat", propsStat);

		float str = 1.0f;
		propsStat->GetValue("strength", str);
		if (str > m_str_val_cm)
		{
			pActor->SetStrength(pActor->GetRealValStrength() - 1);
			str -= 1.0f;
			propsStat->SetValue("strength", str);
			g_pGameCVars->g_leveluppointsstat += 1;
		}
		else
		{
			str -= 0.0f;
			pActor->SetStrength(pActor->GetRealValStrength() - 0);
			propsStat->SetValue("strength", str);
			g_pGameCVars->g_leveluppointsstat += 0;
		}
	}
}

void CHUDCommon::Agl_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("agility", val);
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		val += 1.0f;
		pActor->SetAgility(pActor->GetRealValAgility() + 1);
		propsStat->SetValue("agility", val);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		val += 0.0f;
		pActor->SetAgility(pActor->GetRealValAgility() + 0);
		propsStat->SetValue("agility", val);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Agl_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("agility", val);
	if (val > m_agl_val_cm)
	{
		val -= 1.0f;
		pActor->SetAgility(pActor->GetRealValAgility() - 1);
		propsStat->SetValue("agility", val);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		val -= 0.0f;
		pActor->SetAgility(pActor->GetRealValAgility() - 0);
		propsStat->SetValue("agility", val);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Int_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("intelligence", val);
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		val += 1.0f;
		pActor->SetIntelligence(pActor->GetRealValIntelligence() + 1);
		propsStat->SetValue("intelligence", val);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		val += 0.0f;
		pActor->SetIntelligence(pActor->GetRealValIntelligence() + 0);
		propsStat->SetValue("intelligence", val);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Int_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("intelligence", val);
	if (val > m_int_val_cm)
	{
		val -= 1.0f;
		pActor->SetIntelligence(pActor->GetRealValIntelligence() - 1);
		propsStat->SetValue("intelligence", val);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		val -= 0.0f;
		pActor->SetIntelligence(pActor->GetRealValIntelligence() - 0);
		propsStat->SetValue("intelligence", val);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Endr_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("endurance", val);
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		val += 1.0f;
		pActor->SetEndurance(pActor->GetRealValEndurance() + 1);
		propsStat->SetValue("endurance", val);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		val += 0.0f;
		pActor->SetEndurance(pActor->GetRealValEndurance() + 0);
		propsStat->SetValue("endurance", val);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Endr_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("endurance", val);
	if (val > m_endr_val_cm)
	{
		val -= 1.0f;
		pActor->SetEndurance(pActor->GetRealValEndurance() - 1);
		propsStat->SetValue("endurance", val);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		val -= 0.0f;
		pActor->SetEndurance(pActor->GetRealValEndurance() - 0);
		propsStat->SetValue("endurance", val);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Wlp_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("willpower", val);
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		val += 1.0f;
		pActor->SetWillpower(pActor->GetRealValWillpower() + 1);
		propsStat->SetValue("willpower", val);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		val += 0.0f;
		pActor->SetWillpower(pActor->GetRealValWillpower() + 0);
		propsStat->SetValue("willpower", val);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Wlp_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("willpower", val);
	if (val > m_will_val_cm)
	{
		val -= 1.0f;
		pActor->SetWillpower(pActor->GetRealValWillpower() - 1);
		propsStat->SetValue("willpower", val);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		val -= 0.0f;
		pActor->SetWillpower(pActor->GetRealValWillpower() - 0);
		propsStat->SetValue("willpower", val);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Pers_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("personality", val);
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		val += 1.0f;
		pActor->SetPersonality(pActor->GetRealValPersonality() + 1);
		propsStat->SetValue("personality", val);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		val += 0.0f;
		pActor->SetPersonality(pActor->GetRealValPersonality() + 0);
		propsStat->SetValue("personality", val);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Pers_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);

	float val = 1.0f;
	propsStat->GetValue("personality", val);
	if (val > m_pers_val_cm)
	{
		val -= 1.0f;
		pActor->SetPersonality(pActor->GetRealValPersonality() - 1);
		propsStat->SetValue("personality", val);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		val -= 0.0f;
		pActor->SetPersonality(pActor->GetRealValPersonality() - 0);
		propsStat->SetValue("personality", val);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Hp_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetHealth();
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		pActor->SetMaxHealth(pActor->GetRealValMaxHealth() + 5);
		pActor->SetHealth(oldval);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		pActor->SetMaxHealth(pActor->GetRealValMaxHealth() + 0);
		pActor->SetHealth(oldval);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Hp_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetHealth();
	if (pActor->GetMaxHealth() > m_Mhp_val_cm)
	{
		pActor->SetMaxHealth(pActor->GetRealValMaxHealth() - 5);
		pActor->SetHealth(oldval);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		pActor->SetMaxHealth(pActor->GetRealValMaxHealth() - 0);
		pActor->SetHealth(oldval);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::St_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetStamina();
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		pActor->SetMaxStamina(pActor->GetRealValMaxStamina() + 15);
		pActor->SetStamina(oldval);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		pActor->SetStamina(oldval);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::St_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetStamina();
	if (pActor->GetMaxStamina() > m_Mst_val_cm)
	{
		pActor->SetMaxStamina(pActor->GetRealValMaxStamina() - 15);
		pActor->SetStamina(oldval);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		pActor->SetStamina(oldval);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::Mp_pl()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetMagicka();
	if (g_pGameCVars->g_leveluppointsstat > 0)
	{
		pActor->SetMaxMagicka(pActor->GetRealValMaxMagicka() + 15);
		pActor->SetMagicka(oldval);
		g_pGameCVars->g_leveluppointsstat -= 1;
	}
	else
	{
		pActor->SetMaxMagicka(pActor->GetRealValMaxMagicka() + 0);
		pActor->SetMagicka(oldval);
		g_pGameCVars->g_leveluppointsstat -= 0;
	}
}

void CHUDCommon::Mp_min()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	int oldval = pActor->GetMagicka();
	if (pActor->GetMaxMagicka() > m_Mmg_val_cm)
	{
		pActor->SetMaxMagicka(pActor->GetRealValMaxMagicka() - 15);
		pActor->SetMagicka(oldval);
		g_pGameCVars->g_leveluppointsstat += 1;
	}
	else
	{
		pActor->SetMaxMagicka(pActor->GetRealValMaxMagicka() - 0);
		pActor->SetMagicka(oldval);
		g_pGameCVars->g_leveluppointsstat += 0;
	}
}

void CHUDCommon::TabPressed(int id)
{
	SFlashVarValue stat_tab[1] = { id };
	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	if (!pElement)
		return;

	pElement->GetFlashPlayer()->Invoke("selectInventorySection", stat_tab, 1);
	if (id == g_pGameCVars->g_player_inv_sel_tab_id && !m_fs_tab_open)
	{
		g_pGameCVars->g_player_inv_sel_tab_id = id;
		m_fs_tab_open = true;
	}
	else if (id != g_pGameCVars->g_player_inv_sel_tab_id && !m_fs_tab_open)
	{
		g_pGameCVars->g_player_inv_sel_tab_id = id;
		m_fs_tab_open = true;
	}
	if (id == g_pGameCVars->g_player_inv_sel_tab_id && m_fs_tab_open)
	{
		g_pGameCVars->g_player_inv_sel_tab_id = id;
		m_fs_tab_open = true;
	}
	else if (id != g_pGameCVars->g_player_inv_sel_tab_id && m_fs_tab_open)
	{
		g_pGameCVars->g_player_inv_sel_tab_id = id;
		pElement->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		UpdateInventory(10);
		m_fs_tab_open = true;
	}
}

void CHUDCommon::ShopTabPressed(int id)
{
	SFlashVarValue stat_tab[1] = { id };
	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	if (!pShopInterface)
		return;

	pShopInterface->GetFlashPlayer()->Invoke("selectInventorySection", stat_tab, 1);
	if (id != current_chest_or_shop_tab_opened)
	{
		current_chest_or_shop_tab_opened = id;
		pShopInterface->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		UpdateTradeSystem(10);
	}
}

void CHUDCommon::ChestTabPressed(int id)
{
	SFlashVarValue stat_tab[1] = { id };
	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (!pLootUI)
		return;

	if (id != current_chest_or_shop_tab_opened)
	{
		pLootUI->GetFlashPlayer()->Invoke("selectInventorySection", stat_tab, 1);
		current_chest_or_shop_tab_opened = id;
		pLootUI->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		UpdateChestInventory(10);
	}
}

void CHUDCommon::SpellsSelectionTabPressed(int id)
{
	SFlashVarValue stat_tab[1] = { id };
	IUIElement* pSpellSelectionMenu = gEnv->pFlashUI->GetUIElement("SpellSelectionUI");
	if (!pSpellSelectionMenu)
		return;

	if (id != current_spell_selection_tab_opened)
	{
		pSpellSelectionMenu->GetFlashPlayer()->Invoke("selectInventorySection", stat_tab, 1);
		current_spell_selection_tab_opened = id;
		pSpellSelectionMenu->GetFlashPlayer()->Invoke0("clearCurrentInventorySection");
		UpdateSpellSelection(10);
	}
}

void CHUDCommon::UpdateMaxStamina()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fStamina = (pActor->GetMaxStamina()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fStamina };
		if (m_fStamina != fStamina || m_bFirstFrame)
		{
			float stamina = fStamina;
			pChrMenu_hud->GetFlashPlayer()->Invoke("setStamina", stat, 1);
			//m_animCharacterEquipment.Invoke("setStamina", (int)fStamina);
		}

		if (m_bFirstFrame)
			m_fStamina = fStamina;

		m_fStamina = fStamina;
	}
}

void CHUDCommon::UpdateMaxMagicka()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fMagicka = (pActor->GetMaxMagicka()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fMagicka };
		if (m_fMagicka != fMagicka || m_bFirstFrame)
		{
			float magicka = fMagicka;
			pChrMenu_hud->GetFlashPlayer()->Invoke("setMagicka", stat, 1);
			//m_animCharacterEquipment.Invoke("setMagicka", (int)fMagicka);
		}

		if (m_bFirstFrame)
			m_fMagicka = fMagicka;

		m_fMagicka = fMagicka;
	}
}

void CHUDCommon::UpdateMaxHealth()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fHealth = (pActor->GetMaxHealth()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fHealth };
		if (m_fHealth != fHealth || m_bFirstFrame)
		{
			//m_animPlayerStats.Invoke("setHealth", (int)fHealth);
			pChrMenu_hud->GetFlashPlayer()->Invoke("setHealth", stat, 1);
			//m_animCharacterEquipment.Invoke("setHealth", (int)fHealth);
		}

		if (m_bFirstFrame)
			m_fHealth = fHealth;

		m_fHealth = fHealth;
	}
}

void CHUDCommon::UpdateStrength()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fStrength = (pActor->GetActorStrength()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fStrength };
		if (m_fStrength != fStrength || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setStrength", stat, 1);
			//m_animCharacterEquipment.Invoke("setStrength", (int)fStrength);
		}

		if (m_bFirstFrame)
			m_fStrength = fStrength;

		m_fStrength = fStrength;
	}
}

void CHUDCommon::UpdateAgility()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fAgility = (pActor->GetAgility()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fAgility };
		if (m_fAgility != fAgility || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setAgility", stat, 1);
			//m_animCharacterEquipment.Invoke("setAgility", (int)fAgility);
		}

		if (m_bFirstFrame)
			m_fAgility = fAgility;

		m_fAgility = fAgility;
	}
}

void CHUDCommon::UpdateIntelligence()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fIntelligence = (pActor->GetIntelligence()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fIntelligence };
		if (m_fIntelligence != fIntelligence || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setIntelligence", stat, 1);
			//m_animCharacterEquipment.Invoke("setIntelligence", (int)fIntelligence);
		}

		if (m_bFirstFrame)
			m_fIntelligence = fIntelligence;

		m_fIntelligence = fIntelligence;
	}
}

void CHUDCommon::UpdateWillpower()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fWillpower = (pActor->GetWillpower()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fWillpower };
		if (m_fWillpower != fWillpower || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setWillpower", stat, 1);
			//m_animCharacterEquipment.Invoke("setWillpower", (int)fWillpower);
		}

		if (m_bFirstFrame)
			m_fWillpower = fWillpower;

		m_fWillpower = fWillpower;
	}
}

void CHUDCommon::UpdateEndurance()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fEndurance = (pActor->GetEndurance()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fEndurance };
		if (m_fEndurance != fEndurance || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setEndurance", stat, 1);
			//m_animCharacterEquipment.Invoke("setEndurance", (int)fEndurance);
		}

		if (m_bFirstFrame)
			m_fEndurance = fEndurance;

		m_fEndurance = fEndurance;
	}
}

void CHUDCommon::UpdatePersonality()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fPersonality = (pActor->GetPersonality()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fPersonality };
		if (m_fPersonality != fPersonality || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setPersonality", stat, 1);
			//m_animCharacterEquipment.Invoke("setPersonality", (int)fPersonality);
		}

		if (m_bFirstFrame)
			m_fPersonality = fPersonality;

		m_fPersonality = fPersonality;
	}
}

void CHUDCommon::UpdateLevel()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
		float fLevel = (pActor->GetLevel()) *1.0f - 0.0f;
		SFlashVarValue stat[1] = { (int)fLevel };
		if (m_fLevel != fLevel || m_bFirstFrame)
		{
			pChrMenu_hud->GetFlashPlayer()->Invoke("setLevel", stat, 1);
			//m_animCharacterEquipment.Invoke("setLevel", (int)fLevel);
			//CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			//m_animCharacterEquipment.Invoke("setChrName", pPlayer->m_player_name.c_str());
			//m_animCharacterEquipment.Invoke("setGold", (int)pActor->GetGold());
		}


		float fAttPoints = (g_pGameCVars->g_leveluppointsstat) *1.0f - 0.0f;
		SFlashVarValue Attstat[1] = { (int)fAttPoints };
		pChrMenu_hud->GetFlashPlayer()->Invoke("setAttPoints", Attstat, 1);

		float fXP = (pActor->GetLevelXp()) *1.0f - 0.0f;
		float fXPNL = (pActor->GetXpToNextLevel()) *1.0f - 0.0f;
		SFlashVarValue XP_to_lvl[1] = { (int)fXP };
		SFlashVarValue XP_to_lvl_next[1] = { (int)fXPNL };
		pChrMenu_hud->GetFlashPlayer()->Invoke("setXP", XP_to_lvl, 1);
		pChrMenu_hud->GetFlashPlayer()->Invoke("setNEXTLVLXP", XP_to_lvl_next, 1);

		if (m_bFirstFrame)
			m_fLevel = fLevel;

		m_fLevel = fLevel;
	}
}

void CHUDCommon::UpdateNumInvSlots()
{
	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	if (pElement)
	{
		if (Inv_cont_menu_loaded)
			return;

		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (pActor)
		{
			SFlashVarValue args[1] = { pActor->GetMaxInvSlots() };
			pElement->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
		}
	}
}

void CHUDCommon::ShowItemInformation(bool show, bool update, EntityId item_id)
{
	IUIElement* pItemInfo = gEnv->pFlashUI->GetUIElement("ItemInformation");
	if (pItemInfo)
	{
		if (!pItemInfo->GetFlashPlayer())
			return;

		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_id));
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (show && !Inv_ItemInfo_loaded)
		{
			pItemInfo->Reload();
			pItemInfo->GetFlashPlayer()->SetVisible(true);
			pItemInfo->SetVisible(true);
			Inv_ItemInfo_loaded = show;
		}
		else if (!show)
		{
			pItemInfo->SetVisible(false);
			pItemInfo->GetFlashPlayer()->SetVisible(false);
			Inv_ItemInfo_loaded = show;
		}

		if (!pItem)
			return;

		if (!pActor)
			return;

		if (show && update)
		{
			string itemText = pItem->GetItemDec(0);
			SFlashVarValue args[1] = { itemText.c_str() };
			pItemInfo->GetFlashPlayer()->Invoke("setText", args, 1);
			float hp = pItem->GetItemHealth();
			float fHp = (hp)*1.0f;
			SFlashVarValue args_hp[3] = { "item health", (int)fHp, 1 };
			pItemInfo->GetFlashPlayer()->Invoke("addParamVisual", args_hp, 3);
			if (pItem->GetArmorType()>0)
			{
				float armor = pItem->GetArmorRating();
				float fArm = (armor)*1.0f;
				SFlashVarValue args_arm[3] = { "armor rating", (int)fArm, 2 };
				pItemInfo->GetFlashPlayer()->Invoke("addParamVisual", args_arm, 3);
			}
			CWeapon *m_pWeapon = pActor->GetWeapon(item_id);
			if (m_pWeapon)
			{
				float dmg2 = pItem->GetDmgAdded();
				float dmg = m_pWeapon->GetDmgCurrent();
				float fAtk = (dmg + dmg2) *1.0f;
				SFlashVarValue args_atk[3] = { "attack rating", (int)fAtk, 2 };
				pItemInfo->GetFlashPlayer()->Invoke("addParamVisual", args_atk, 3);
			}
			SFlashVarValue args_icon[1] = { pItem->GetIcon() };
			pItemInfo->GetFlashPlayer()->Invoke("setImage", args_icon, 1);
		}
	}
}

void CHUDCommon::ShowSpellInformation(bool show, bool update, int id)
{
	IUIElement* pSpellInfo = gEnv->pFlashUI->GetUIElement("SpellInformation");
	if (pSpellInfo)
	{
		if (!pSpellInfo->GetFlashPlayer())
			return;

		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (!pGameGlobals)
			return;

		if (show && !SpellInfo_loaded)
		{
			pSpellInfo->Reload();
			pSpellInfo->GetFlashPlayer()->SetVisible(true);
			pSpellInfo->SetVisible(true);
			SpellInfo_loaded = show;
		}
		else if (!show)
		{
			pSpellInfo->SetVisible(false);
			pSpellInfo->GetFlashPlayer()->SetVisible(false);
			SpellInfo_loaded = show;
			pGameGlobals->current_selected_spell = "";
			pGameGlobals->current_selected_spell_ids = -1;
			return;
		}
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActor)
			return;

		CGameXMLSettingAndGlobals::SActorSpell p_spell_inf;
		p_spell_inf.spell_id = -1;
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pActor->m_actor_spells_book.begin();
		std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pActor->m_actor_spells_book.end();
		int count = pActor->m_actor_spells_book.size();
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if ((*it).spell_id == id)
			{
				p_spell_inf = (*it);
				break;
			}
		}

		if (show && update)
		{
			if (p_spell_inf.spell_id > -1)
			{
				SFlashVarValue arg_dec[1] = { p_spell_inf.spell_description[1].c_str() };
				SFlashVarValue arg_icon[1] = { p_spell_inf.spell_icon_path[1].c_str() };
				SFlashVarValue arg_type[3] = { "Spell Type", p_spell_inf.spell_type.c_str(), 1 };
				SFlashVarValue arg_force[3] = { "Spell Force", (int)p_spell_inf.spell_force, 2 };
				SFlashVarValue arg_time[3] = { "Spell Time", (int)p_spell_inf.start_time, 3 };
				SFlashVarValue arg_cost[3] = { "Spell Mana Cost", (int)p_spell_inf.spell_mana_cost, 4 };
				pSpellInfo->GetFlashPlayer()->Invoke("setText", arg_dec, 1);
				pSpellInfo->GetFlashPlayer()->Invoke("setImage", arg_icon, 1);
				pSpellInfo->GetFlashPlayer()->Invoke("addParamVisual", arg_type, 3);
				pSpellInfo->GetFlashPlayer()->Invoke("addParamVisual", arg_force, 3);
				pSpellInfo->GetFlashPlayer()->Invoke("addParamVisual", arg_time, 3);
				pSpellInfo->GetFlashPlayer()->Invoke("addParamVisual", arg_cost, 3);
			}
		}
	}
}

void CHUDCommon::ShowDraggedItemIcon(bool show, EntityId item_id)
{
	IUIElement* pDraggedItemIcon = gEnv->pFlashUI->GetUIElement("DraggedItemIcon");
	if (pDraggedItemIcon)
	{
		if (!pDraggedItemIcon->GetFlashPlayer())
			return;

		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_id));
		if (show)
		{
			Inv_dragged_item_loaded = true;
			pDraggedItemIcon->Reload();
			pDraggedItemIcon->GetFlashPlayer()->SetVisible(show);
			pDraggedItemIcon->SetVisible(show);
			if (pItem)
			{
				string ui_elem = "DraggedItemIcon";
				string ui_clip = "itemInfo_mc.icon_mc.img_mc";
				//CryLogAlways("dragged item showed");
				SFlashVarValue args_icon[1] = { pItem->GetIcon() };
				string itemText = "Drag&Drop";
				SFlashVarValue args[1] = { itemText.c_str() };
				pDraggedItemIcon->GetFlashPlayer()->Invoke("setText", args, 1);
				pDraggedItemIcon->GetFlashPlayer()->Invoke("setImage", args_icon, 1);
				pDraggedItemIcon->GetFlashPlayer()->Invoke0("startDragL");
				//test---------------------------------------------------------
				//AddEntityModelToHud(eHEType_item, item_id, ui_elem, ui_clip);
				//-----fail----------------------------------------------------
			}
		}
		else if (!show)
		{
			Inv_dragged_item_loaded = false;
			pDraggedItemIcon->SetVisible(show);
			pDraggedItemIcon->GetFlashPlayer()->SetVisible(show);
		}
	}

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(item_id));
		if (pItem && pItem->GetOwnerActor())
		{
			pGameGlobals->current_selected_Item = item_id;
		}
		else
		{
			pGameGlobals->current_selected_Item = 0;
		}
	}
}

void CHUDCommon::DraggedItemUpdatePos()
{
	//this disabled
	/*IUIElement* pDraggedItemIcon = gEnv->pFlashUI->GetUIElement("DraggedItemIcon");
	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	if (pDraggedItemIcon && pMain_hud)
	{
		if (pDraggedItemIcon->GetFlashPlayer() && pDraggedItemIcon->GetFlashPlayer()->GetVisible())
		{
			//CryLogAlways("dragged item pos updated");
			float xl, yl = 0.0f;
			gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&xl, &yl);
			int iX((int)xl), iY((int)yl);
			int x, y, width, height;
			gEnv->pRenderer->GetViewport(&x, &y, &width, &height);
			pDraggedItemIcon->GetFlashPlayer()->SetViewport(x, y, width, height);
			float fX = (float)iX / (float)width;
			float fY = (float)iY / (float)height;
			float aspect;
			pDraggedItemIcon->GetFlashPlayer()->GetViewport(x, y, width, height, aspect);
			iX = (int)(fX * (float)width*1.2);
			iY = (int)(fY * (float)height*1.2);
			
			pDraggedItemIcon->GetFlashPlayer()->ClientToScreen(iX, iY);
			SFlashVarValue args[2] = { iX, iY };
			pDraggedItemIcon->GetFlashPlayer()->Invoke("setPos", args, 2);

		}
	}*/
}

void CHUDCommon::DialogLinePressed(int id)
{
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return;

	nwAction->DialogSysLinePressed(id);
}

void CHUDCommon::DialogNPCWords(string words)
{
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	if (pDialogUi && m_DialogSystemVisible)
	{
		SFlashVarValue args_wrd[1] = { words.c_str() };
		pDialogUi->GetFlashPlayer()->Invoke("addNPCActorWords", args_wrd, 1);
		pDialogUi->GetFlashPlayer()->Invoke0("clearCurrentDialog");
	}
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	if (pDialogAltUi && m_DialogSystemAltVisible)
	{
		SFlashVarValue args_wrd[1] = { words.c_str() };
		pDialogAltUi->GetFlashPlayer()->Invoke("addNPCActorWords", args_wrd, 1);
		pDialogAltUi->GetFlashPlayer()->Invoke0("clearCurrentDialog");
	}
}

void CHUDCommon::AddDialogLine(string line, int num)
{
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	if (pDialogUi && m_DialogSystemVisible)
	{
		if (num == 0)
			return;

		SFlashVarValue args_line[2] = { num, line.c_str() };
		pDialogUi->GetFlashPlayer()->Invoke("addPlayerDialogLine", args_line, 2);
	}
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	if (pDialogAltUi && m_DialogSystemAltVisible)
	{
		if (questionloaded[num])
			return;

		SFlashVarValue args_line[2] = { num, line.c_str() };
		pDialogAltUi->GetFlashPlayer()->Invoke("addPlayerDialogLine", args_line, 2);
		if (!questionloaded[num])
		{
			questionloaded[num] = true;
		}
	}
}

void CHUDCommon::ShowDialog(bool show, string dialog_main_file, int dialog_stage)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_DialogSystemHud))
		return;

	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	if (pDialogUi)
	{
		if (!pDialogUi->GetFlashPlayer())
			return;

		if (!m_DialogSystemVisible && show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, true);
			OnInterInUIMode(eCTOSUIOpen_DialogSystemHud, true);
			pDialogUi->Reload();
			pDialogUi->GetFlashPlayer()->SetVisible(true);
			pDialogUi->SetVisible(true);
			XmlNodeRef node = GetISystem()->LoadXmlFromFile(dialog_main_file.c_str());
			if (!node)
			{
				CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
				CryLogAlways("DialogSystem xml not loaded.");
				return;
			}
			XmlNodeRef paramsNode = node->findChild("params");
			XmlNodeRef statesNode = node->findChild("states");
			XmlString npsname;
			XmlString dialogstringquestion;
			XmlString dialogstringanswere;
			int numquestions;
			XmlNodeRef state1Node;
			state1Node = statesNode->getChild(dialog_stage - 1);
			XmlNodeRef answerNode = state1Node->findChild("greeting");
			answerNode->getAttr("greetings", dialogstringanswere);
			XmlNodeRef numquestionsNode = state1Node->findChild("numquestionsinstate");
			numquestionsNode->getAttr("numquestions", numquestions);
			paramsNode->getAttr("npsname", npsname);
			string sName = npsname;
			SFlashVarValue args_name[1] = { sName.c_str() };
			SFlashVarValue args_wrd[1] = { dialogstringanswere.c_str() };
			pDialogUi->GetFlashPlayer()->Invoke("addNPCActorWords", args_wrd, 1);
			pDialogUi->GetFlashPlayer()->Invoke0("clearCurrentDialog");
			string line_cnd = "question";
			for (int i = 0; i < numquestions; i++)
			{
				char B_valstr[17];
				itoa(i, B_valstr, 10);
				string C_valstr = B_valstr;
				string Line_valstr = line_cnd + C_valstr;
				XmlNodeRef questionNodeLineStruct = state1Node->findChild(Line_valstr.c_str());
				if (questionNodeLineStruct)
				{
					questionNodeLineStruct->getAttr("dialogstringquestion", dialogstringquestion);
					AddDialogLine(dialogstringquestion.c_str(), i);
				}
			}
			pDialogUi->GetFlashPlayer()->SetFSCommandHandler(this);
			m_DialogSystemVisible = show;
			if (g_pGameCVars->g_disable_all_ui_if_dialog_ui_loaded > 0)
			{
				if (MainHud_loaded)
				{
					ShowMainHud();
				}
			}
		}

		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, false);
			OnInterInUIMode(eCTOSUIOpen_DialogSystemHud, false);
			pDialogUi->GetFlashPlayer()->SetVisible(false);
			pDialogUi->SetVisible(false);
			pDialogUi->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_DialogSystemVisible = show;
			if (g_pGameCVars->g_disable_all_ui_if_dialog_ui_loaded > 0)
			{
				if (!MainHud_loaded)
				{
					ShowMainHud();
				}
			}
		}
	}
}

//*****************************************
void CHUDCommon::ShowDialogSystemAlt(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_DialogSystemHud))
		return;

	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	if (pDialogAltUi)
	{
		if (!pDialogAltUi->GetFlashPlayer())
			return;

		if (!m_DialogSystemAltVisible && show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, true);
			OnInterInUIMode(eCTOSUIOpen_DialogSystemHud, true);
			pDialogAltUi->Reload();
			pDialogAltUi->GetFlashPlayer()->SetVisible(true);
			pDialogAltUi->SetVisible(true);
			pDialogAltUi->GetFlashPlayer()->SetFSCommandHandler(this);
			m_DialogSystemAltVisible = show;
			if (g_pGameCVars->g_disable_all_ui_if_dialog_ui_loaded > 0)
			{
				if (MainHud_loaded)
				{
					ShowMainHud();
				}
			}
		}

		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, false);
			OnInterInUIMode(eCTOSUIOpen_DialogSystemHud, false);
			pDialogAltUi->GetFlashPlayer()->SetVisible(false);
			pDialogAltUi->SetVisible(false);
			pDialogAltUi->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_DialogSystemAltVisible = show;
			if (g_pGameCVars->g_disable_all_ui_if_dialog_ui_loaded > 0)
			{
				if (!MainHud_loaded)
				{
					ShowMainHud();
				}
			}
		}
	}
}
//*****************************************

void CHUDCommon::DialogSystemClear()
{
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	if (pDialogUi && m_DialogSystemVisible)
	{
		pDialogUi->GetFlashPlayer()->Invoke0("clearCurrentDialog");
	}
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	if (pDialogAltUi && m_DialogSystemAltVisible)
	{
		pDialogAltUi->GetFlashPlayer()->Invoke0("clearCurrentDialog");
	}

	for (int i = 0; i < 255; i++)
	{
		questionloaded[i] = false;
	}
}

int CHUDCommon::GetDialogLinesLoadedNumber()
{
	int number_lines = 0;
	for (int i = 0; i < 255; i++)
	{
		if (questionloaded[i] == true)
			number_lines += 1;
	}
	return number_lines;
}

void CHUDCommon::EnableCurrentDialogCloseButton(bool enable)
{
	int enbl = 0;
	if (enable)
		enbl = 1;

	SFlashVarValue arg_enable[1] = { enbl };
	if (m_DialogSystemAltVisible)
	{
		IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
		if (pDialogAltUi)
		{
			pDialogAltUi->GetFlashPlayer()->Invoke("enableCloseButton", arg_enable, 1);
		}
	}
	else if (m_DialogSystemVisible)
	{
		IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
		if (pDialogUi)
		{
			pDialogUi->GetFlashPlayer()->Invoke("enableCloseButton", arg_enable, 1);
		}
	}
}

void CHUDCommon::ShowBook(bool show, EntityId item_id)
{
	//CryLogAlways("CHUDCommon::ShowBook");
	EntityId id = item_id;
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
	if (!pItem && show)
		return;

	IUIElement* pInteractiveBookUI = gEnv->pFlashUI->GetUIElement("InteractiveBookUI");
	if (pInteractiveBookUI)
	{
		if (!m_BookVisible && show)
		{
			CreateUIActionFilter(eHUIAFT_interactive, true);
			OnInterInUIMode(eCTOSUIOpen_BookUI, true);
			pInteractiveBookUI->Reload();
			pInteractiveBookUI->GetFlashPlayer()->SetVisible(true);
			pInteractiveBookUI->SetVisible(true);
			pInteractiveBookUI->GetFlashPlayer()->SetFSCommandHandler(this);
			m_BookVisible = show;
			for (int i = 0; i < pItem->GetBookNumPages(); i++)
			{
				string page = pItem->GetBookText(i);
				SFlashVarValue args[3] = { 0, page.c_str(), "" };
				pInteractiveBookUI->GetFlashPlayer()->Invoke("addNewPage", args, 3);
				//CryLogAlways("addNewPage");
			}
			pInteractiveBookUI->GetFlashPlayer()->Invoke0("buildPages");
			//CryLogAlways("addPagesToArray");
		}
		
		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_interactive, false);
			OnInterInUIMode(eCTOSUIOpen_BookUI, false);
			pInteractiveBookUI->GetFlashPlayer()->SetVisible(false);
			pInteractiveBookUI->SetVisible(false);
			pInteractiveBookUI->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_BookVisible = show;
		}
	}
}

void CHUDCommon::ShowChSkills(bool show)
{
}

void CHUDCommon::ShowChPerks(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_CharacterPerks))
		return;

	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	if (pPlayerPerksMenu)
	{
		if (!m_CharacterPerksMenuVisible && show)
		{

			CreateUIActionFilter(eHUIAFT_interactive, true);
			OnInterInUIMode(eCTOSUIOpen_CharacterPerks, true);
			pPlayerPerksMenu->Reload();
			pPlayerPerksMenu->GetFlashPlayer()->SetVisible(true);
			pPlayerPerksMenu->SetVisible(true);
			pPlayerPerksMenu->GetFlashPlayer()->SetFSCommandHandler(this);
			m_CharacterPerksMenuVisible = show;

			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pActor)
				return;

			XmlNodeRef node = pGameGlobals->CharacterPerks_node;//GetISystem()->LoadXmlFromFile(pGameGlobals->character_perks_path.c_str());
			if (!node)
			{
				CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
				CryLogAlways("xml not loaded.");
				return;
			}
			XmlNodeRef paramsNode = node->findChild("params");
			XmlNodeRef infopathNode = node->findChild("info");
			XmlNodeRef perksNode = infopathNode->findChild("perks");
			int id;
			int real_id;
			XmlString perk_icon_path;
			XmlString perk_description;
			int perk_parent_id;
			int perk_ct;
			int perk_grp;
			int perk_is_passive;
			int perk_simply_learn;
			int perk_pos_x;
			int perk_pos_y;
			int perk_level;
			int perk_max_level;
			int perks_number;
			paramsNode->getAttr("perks_num", perks_number);
			for (int i = 0; i < perks_number; i++)
			{
				XmlNodeRef perkNode = perksNode->getChild(i);
				if (perkNode)
				{
					perkNode->getAttr("id", id);
					perkNode->getAttr("real_id", real_id);
					perkNode->getAttr("perk_description", perk_description);
					perkNode->getAttr("icon", perk_icon_path);
					perkNode->getAttr("perk_parent_id", perk_parent_id);
					perkNode->getAttr("perk_ct", perk_ct);
					perkNode->getAttr("perk_grp", perk_grp);
					perkNode->getAttr("perk_is_passive", perk_is_passive);
					perkNode->getAttr("perk_simply_learn", perk_simply_learn);
					perkNode->getAttr("perk_pos_x", perk_pos_x);
					perkNode->getAttr("perk_pos_y", perk_pos_y);
					perkNode->getAttr("perk_max_level", perk_max_level);
					perk_level = pActor->GetPerkLevel(real_id);
					bool full_learned = false;
					if (perk_level >= perk_max_level)
						full_learned = true;

					bool passive_ab = false;
					if (perk_is_passive > 0)
						passive_ab = true;

					SFlashVarValue args[11] = { id, perk_ct, perk_icon_path.c_str(), perk_pos_x, perk_pos_y, perk_level, perk_max_level, full_learned, passive_ab, false, real_id };
					pPlayerPerksMenu->GetFlashPlayer()->Invoke("addAbilityVisual", args, 11);
					if (perk_parent_id >= 0)
					{
						SFlashVarValue argsw[3] = { perk_ct, perk_parent_id, real_id };
						pPlayerPerksMenu->GetFlashPlayer()->Invoke("showLineBetweenTwoAbls", argsw, 3);
					}
				}
			}
			int ab_points = g_pGameCVars->g_levelupabilitypoints;
			SFlashVarValue argsd[2] = { "Ability Points", ab_points };
			pPlayerPerksMenu->GetFlashPlayer()->Invoke("setAblPoints", argsd, 2);
			m_CharacterSkillInfoVisible = false;
			m_CharacterSkillSelectedId = -1;
		}

		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_interactive, false);
			OnInterInUIMode(eCTOSUIOpen_CharacterPerks, false);
			pPlayerPerksMenu->GetFlashPlayer()->SetVisible(false);
			pPlayerPerksMenu->SetVisible(false);
			pPlayerPerksMenu->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_CharacterPerksMenuVisible = show;
		}
	}
}

void CHUDCommon::ShowPerkInfo(bool show)
{
	if (!m_CharacterPerksMenuVisible)
		return;

	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	if (pPlayerPerksMenu)
	{
		if (show)
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pActor)
				return;

			XmlNodeRef node = GetISystem()->LoadXmlFromFile(pGameGlobals->character_perks_path.c_str());
			if (!node)
			{
				CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
				CryLogAlways("xml not loaded.");
				return;
			}
			SFlashVarValue args_gvr[2] = { 8, 2 };
			pPlayerPerksMenu->GetFlashPlayer()->Invoke("setGridAmounts", args_gvr, 2);
			XmlNodeRef paramsNode = node->findChild("params");
			XmlNodeRef infopathNode = node->findChild("info");
			XmlNodeRef perksNode = infopathNode->findChild("perks");
			int real_id = 0;
			XmlString perk_description = "";
			int perks_number = 0;
			paramsNode->getAttr("perks_num", perks_number);
			for (int i = 0; i < perks_number; i++)
			{
				XmlNodeRef perkNode = perksNode->getChild(i);
				if (perkNode)
				{
					perkNode->getAttr("real_id", real_id);
					perkNode->getAttr("perk_description", perk_description);
					if (real_id == m_CharacterSkillSelectedId)
					{
						SFlashVarValue args[1] = { perk_description.c_str() };
						SFlashVarValue argFs[1] = { 1 };
						pPlayerPerksMenu->GetFlashPlayer()->Invoke("showAbilityInfo", argFs, 1);
						pPlayerPerksMenu->GetFlashPlayer()->Invoke("setAbilityDescription", args, 1);
					}
				}
			}
		}
		else
		{
			SFlashVarValue argFs[1] = { 0 };
			pPlayerPerksMenu->GetFlashPlayer()->Invoke("showAbilityInfo", argFs, 1);
		}
	}
}

void CHUDCommon::ShowMinimap(bool show, bool by_interactive_ui)
{
	if ((m_DialogSystemVisible || m_DialogSystemAltVisible || m_CharacterCreationVisible || m_GamePausedIU) && show)
		return;

	if(gEnv->pGame->GetIGameFramework()->IsGamePaused() && show)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	IUIElement* pMinimap = gEnv->pFlashUI->GetUIElement("MinimapUI");
	if (pMinimap)
	{
		if (!pMinimap->GetFlashPlayer())
			return;

		if (by_interactive_ui && !show)
		{
			SFlashVarValue argVis[1] = { false };
			pMinimap->GetFlashPlayer()->Invoke("setUIElementVisible", argVis, 1);
			m_Minimap_closed_bitui = true;
			m_MinimapVisible = show;
			return;
		}

		if (!show)
		{
			pMinimap->GetFlashPlayer()->SetVisible(show);
			pMinimap->SetVisible(show);
			m_MinimapVisible = show;
			m_Minimap_closed_bitui = false;
			entites_ont_map_to_update.clear();
			static_markers_on_minimap_ids.clear();
		}
		else
		{
			if (m_Minimap_closed_bitui)
			{
				SFlashVarValue argVis[1] = { true };
				pMinimap->GetFlashPlayer()->Invoke("setUIElementVisible", argVis, 1);
				m_Minimap_closed_bitui = false;
				m_MinimapVisible = show;
				if (!pMinimap->GetFlashPlayer()->GetVisible() || !pMinimap->IsVisible())
				{
					m_MinimapVisible = false;
					ShowMinimap(true, false);
				}
				return;
			}
			m_Minimap_closed_bitui = false;
			pMinimap->Reload();
			pMinimap->GetFlashPlayer()->SetVisible(show);
			pMinimap->SetVisible(show);
			m_MinimapVisible = show;
			ILevelInfo::SMinimapInfo m_minimapInfo;
			ILevelInfo* pLevel = gEnv->pGame->GetIGameFramework()->GetILevelSystem()->GetCurrentLevel();
			if (pLevel)
				m_minimapInfo = pLevel->GetMinimapInfo();

			if (m_minimapInfo.iPartsNumber > 0)
			{
				if (m_minimapInfo.iGlobalSize <= 0)
					return;

				float map_coof = m_minimapInfo.iGlobalSize / m_minimapInfo.iWidth;
				string path_to_sectors_image = "MapAndMinimap/" + string(pLevel->GetName()) + string("/minimap/");
				int sector_size = 0;
				if (m_minimapInfo.iPartsNumber == 1)
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber / 0.5f);
				else
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber * 0.5f);

				g_pGameCVars->g_minimap_pl_pix_mlt = map_coof;
				g_pGameCVars->g_minimap_pl_pos_pls_x = m_minimapInfo.fStartX;
				g_pGameCVars->g_minimap_pl_pos_pls_y = m_minimapInfo.fStartY;
				g_pGameCVars->g_minimap_scale_mlt = m_minimapInfo.fMapStartScale;
				pGameGlobals->GetCurrentPlayerPos();
				const int pnc = m_minimapInfo.iPartsNumber;
				Vec2 sectors[MAX_MINIMAP_PARTS];
				s_minimap_sectors_pts.clear();
				s_minimap_sectors_pts.resize(0);
				num_loaded_mm_sectors = 0;
				for (int i = 0; i < m_minimapInfo.iPartsNumber; i++)
				{
					sectors[i] = Vec2(ZERO);
					if (i == 0)
					{
						sectors[i].x = sector_size;
						sectors[i].y = sector_size;
					}
					else if (i == 1)
					{
						sectors[i].x = sector_size;
						sectors[i].y = sector_size * 2;
					}
					else if (i == 2)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size;
					}
					else if (i == 3)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size * 2;
					}
					else if (i == 4)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size * 4;
					}

					if (i <= 3)
					{
						if (!m_minimapInfo.sMinimapParts[i].empty())
						{
							string full_path_to_img_part = path_to_sectors_image + string(m_minimapInfo.sMinimapParts[i].c_str());
							//SFlashVarValue args[2] = { i, full_path_to_img_part.c_str() };
							s_minimap_sectors_pts.push_back(full_path_to_img_part);
							//pMinimap->GetFlashPlayer()->Invoke("setMapPartTexture", args, 2);
						}
					}
				}
				SFlashVarValue argScale[1] = { g_pGameCVars->g_minimap_scale_mlt };
				pMinimap->GetFlashPlayer()->Invoke("setMapScale", argScale, 1);
				//SFlashVarValue argsStartPos[2] = { 0, 0 };
				//pMinimap->GetFlashPlayer()->Invoke("setStartPos", argsStartPos, 2);
				SFlashVarValue argsPlMark[5] = { -1, 0, 0, "MapAndMinimap/minimap_pl_arrow_lowres.png", 0.7f };
				pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsPlMark, 5);
				if (m_TimerLoadingMiniMap)
				{
					gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingMiniMap);
					m_TimerLoadingMiniMap = NULL;
				}
				StartLoadingMinimapSectors();
			}
		}
	}
}

void CHUDCommon::SetMinimapScale(float scale)
{
	IUIElement* pMinimap = gEnv->pFlashUI->GetUIElement("MinimapUI");
	if (pMinimap)
	{
		if (m_MinimapVisible)
		{
			entites_ont_map_to_update.clear();
			static_markers_on_minimap_ids.clear();
			pMinimap->Reload();
			pMinimap->GetFlashPlayer()->SetVisible(true);
			pMinimap->SetVisible(true);
			ILevelInfo::SMinimapInfo m_minimapInfo;
			ILevelInfo* pLevel = gEnv->pGame->GetIGameFramework()->GetILevelSystem()->GetCurrentLevel();
			if (pLevel)
				m_minimapInfo = pLevel->GetMinimapInfo();

			if (m_minimapInfo.iPartsNumber > 0)
			{
				if (m_minimapInfo.iGlobalSize <= 0)
					return;

				float map_coof = m_minimapInfo.iGlobalSize / m_minimapInfo.iWidth;
				string path_to_sectors_image = "MapAndMinimap/" + string(pLevel->GetName()) + string("/minimap/");
				int sector_size = 0;
				if (m_minimapInfo.iPartsNumber == 1)
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber / 0.5f);
				else
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber * 0.5f);

				g_pGameCVars->g_minimap_pl_pix_mlt = map_coof;
				g_pGameCVars->g_minimap_pl_pos_pls_x = m_minimapInfo.fStartX;
				g_pGameCVars->g_minimap_pl_pos_pls_y = m_minimapInfo.fStartY;
				g_pGameCVars->g_minimap_scale_mlt = m_minimapInfo.fMapStartScale;
				const int pnc = m_minimapInfo.iPartsNumber;
				Vec2 sectors[MAX_MINIMAP_PARTS];
				s_minimap_sectors_pts.clear();
				s_minimap_sectors_pts.resize(0);
				num_loaded_mm_sectors = 0;
				for (int i = 0; i < m_minimapInfo.iPartsNumber; i++)
				{
					sectors[i] = Vec2(ZERO);
					if (i == 0)
					{
						sectors[i].x = sector_size;
						sectors[i].y = sector_size;
					}
					else if (i == 1)
					{
						sectors[i].x = sector_size;
						sectors[i].y = sector_size * 2;
					}
					else if (i == 2)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size;
					}
					else if (i == 3)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size * 2;
					}
					else if (i == 4)
					{
						sectors[i].x = sector_size * 2;
						sectors[i].y = sector_size * 4;
					}

					if (i <= 3)
					{
						if (!m_minimapInfo.sMinimapParts[i].empty())
						{
							string full_path_to_img_part = path_to_sectors_image + string(m_minimapInfo.sMinimapParts[i].c_str());
							s_minimap_sectors_pts.push_back(full_path_to_img_part);
							//	SFlashVarValue args[2] = { i, full_path_to_img_part.c_str() };
							//	pMinimap->GetFlashPlayer()->Invoke("setMapPartTexture", args, 2);
						}
					}
				}
				

				if (scale == 0.5f)
				{
					g_pGameCVars->g_minimap_scale_mlt *= scale;
					g_pGameCVars->g_minimap_pl_pix_mlt = g_pGameCVars->g_minimap_scale_mlt * 8;
				}
				else if (scale == 2.0f)
				{
					g_pGameCVars->g_minimap_scale_mlt *= scale;
					g_pGameCVars->g_minimap_pl_pix_mlt = g_pGameCVars->g_minimap_scale_mlt * 0.5f;
				}
				else if (scale == 4.0f)
				{
					g_pGameCVars->g_minimap_scale_mlt *= scale;
					g_pGameCVars->g_minimap_pl_pix_mlt = g_pGameCVars->g_minimap_scale_mlt / 8;
				}

				SFlashVarValue argScale[1] = { g_pGameCVars->g_minimap_scale_mlt };
				pMinimap->GetFlashPlayer()->Invoke("setMapScale", argScale, 1);
				//SFlashVarValue argsStartPos[2] = { 0, 0 };
				//pMinimap->GetFlashPlayer()->Invoke("setStartPos", argsStartPos, 2);
				SFlashVarValue argsPlMark[5] = { -1, 0, 0, "MapAndMinimap/minimap_pl_arrow_lowres.png", 0.7f };
				pMinimap->GetFlashPlayer()->Invoke("addMapMarker", argsPlMark, 5);
				if (m_TimerLoadingMiniMap)
				{
					gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingMiniMap);
					m_TimerLoadingMiniMap = NULL;
				}
				StartLoadingMinimapSectors();
			}
		}
	}
}

void CHUDCommon::ShowMainMap(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_MainMap))
		return;

	IUIElement* pMainMap = gEnv->pFlashUI->GetUIElement("MainMapUI");
	if (pMainMap)
	{
		if (!show)
		{
			pMainMap->GetFlashPlayer()->SetVisible(show);
			pMainMap->SetVisible(show);
			pMainMap->GetFlashPlayer()->SetFSCommandHandler(NULL);
			CreateUIActionFilter(eHUIAFT_interactive, show);
			OnInterInUIMode(eCTOSUIOpen_MainMap, show);
			m_MainMapVisible = show;
		}
		else
		{
			pMainMap->Reload();
			pMainMap->GetFlashPlayer()->SetVisible(show);
			pMainMap->SetVisible(show);
			pMainMap->GetFlashPlayer()->SetFSCommandHandler(this);
			CreateUIActionFilter(eHUIAFT_interactive, show);
			OnInterInUIMode(eCTOSUIOpen_MainMap, show);
			m_MainMapVisible = show;
			ILevelInfo::SMinimapInfo m_minimapInfo;
			ILevelInfo* pLevel = gEnv->pGame->GetIGameFramework()->GetILevelSystem()->GetCurrentLevel();
			if (pLevel)
				m_minimapInfo = pLevel->GetMinimapInfo();

			if (m_minimapInfo.iPartsNumber > 0)
			{
				if (m_minimapInfo.iGlobalSize <= 0)
					return;

				float map_coof = m_minimapInfo.iGlobalSize / m_minimapInfo.iWidth;
				string path_to_sectors_image = "MapAndMinimap/" + string(pLevel->GetName()) + string("/mainmap/");
				int sector_size = 0;
				if (m_minimapInfo.iPartsNumber == 1)
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber / 0.5f);
				else
					sector_size = m_minimapInfo.iGlobalSize / (m_minimapInfo.iPartsNumber * 0.5f);

				g_pGameCVars->g_mainmap_pl_pix_mlt = m_minimapInfo.fMapStartScale;
				num_loaded_m_sectors = 0;
				SFlashVarValue argsPartsNum[1] = { m_minimapInfo.iPartsNumber };
				pMainMap->GetFlashPlayer()->Invoke("setMapSectorsNumber", argsPartsNum, 1);
				s_map_sectors_pts.clear();
				s_map_sectors_pts.resize(0);
				for (int i = 0; i < m_minimapInfo.iPartsNumber; i++)
				{
					if (!m_minimapInfo.sMinimapParts[i].empty())
					{

						string nm = "";
						if (1 < 2)
						{
							nm.Format("A_%i", i);
						}
						else if (1 > 2)
						{
							nm.Format("B_%i", i);
						}
						string full_path_to_img_part = path_to_sectors_image + string(m_minimapInfo.sMinimapParts[i].c_str());
						//SFlashVarValue args[2] = { nm.c_str(), full_path_to_img_part.c_str() };
						s_map_sectors_pts.push_back(full_path_to_img_part);
						//pMainMap->GetFlashPlayer()->Invoke("setMapPartTexture", args, 2);
					}
				}

				CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
				if (!pClActor)
					return;

				float player_pos_x = (pClActor->GetEntity()->GetPos().x / g_pGameCVars->g_mainmap_pl_pix_mlt);
				float player_pos_y = (pClActor->GetEntity()->GetPos().y / g_pGameCVars->g_mainmap_pl_pix_mlt);
				//SFlashVarValue argsStartPos[2] = { 0, 0 };
				//pMainMap->GetFlashPlayer()->Invoke("setStartPos", argsStartPos, 2);
				UpdateObjectsMarkersOnMap(1.0f);
				if (m_TimerLoadingMap)
				{
					gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingMap);
					m_TimerLoadingMap = NULL;
				}
				StartLoadingMapSectors();
			}

			SFlashVarValue argLGI1[2] = { "MapAndMinimap/MapLegendIcons/map_icon_cave.png", "Dungeon" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI1, 2);
			SFlashVarValue argLGI2[2] = { "MapAndMinimap/MapLegendIcons/map_icon_house.png", "House" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI2, 2);
			SFlashVarValue argLGI3[2] = { "MapAndMinimap/MapLegendIcons/map_icon_village.png", "Village" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI3, 2);
			SFlashVarValue argLGI4[2] = { "MapAndMinimap/MapLegendIcons/map_icon_it_point.png", "Point of interest" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI4, 2);
			SFlashVarValue argLGI5[2] = { "MapAndMinimap/MapLegendIcons/map_icon_quest_mark.png", "Active quest marker" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI5, 2);
			SFlashVarValue argLGI6[2] = { "MapAndMinimap/MapLegendIcons/map_icon_ruins.png", "Bulding ruins" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI6, 2);
			SFlashVarValue argLGI7[2] = { "MapAndMinimap/MapLegendIcons/map_icon_tree.png", "Forest" };
			pMainMap->GetFlashPlayer()->Invoke("addMapLegendObject", argLGI7, 2);
		}
	}
}

void CHUDCommon::ShowQuickSelectionMenu(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_QuickSelection))
		return;

	IUIElement* pQuickSelectionMenu = gEnv->pFlashUI->GetUIElement("QuickSelectionMenu");
	if (pQuickSelectionMenu)
	{
		if (!m_QuickSelectionVisible && show)
		{
			CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pClActor)
				return;

			if(!AnyInteractiveUIOpened())
				CreateUIActionFilter(eHUIAFT_interactive, show);

			OnInterInUIMode(eCTOSUIOpen_QuickSelection, show);
			pQuickSelectionMenu->Reload();
			pQuickSelectionMenu->GetFlashPlayer()->SetVisible(show);
			pQuickSelectionMenu->SetVisible(show);
			pQuickSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(this);
			m_QuickSelectionVisible = show;
			for (int i = 1; i < MAX_QUICK_EQUIPMENT_SLOTS+1; i++)
			{
				CActor::AcQuickSlot pQSlot = pClActor->GetQuickSlotInfo(i);
				if (pQSlot.slot_type <= 0)
					continue;

				string slot_icon = "";
				int quantity = 0;
				int type_id = 0;
				int is_selected = 0;
				if (pQSlot.slot_type == eQSType_Item)
				{
					EntityId item_id = (EntityId)pQSlot.obj_in_slot_id;
					if (item_id <= 0)
						continue;

					CItem *pItemInSlot = pClActor->GetItem(item_id);
					if (!pItemInSlot)
						continue;

					slot_icon = pItemInSlot->GetIcon();
					quantity = pItemInSlot->GetItemQuanity();
					type_id = 1;
					is_selected = pItemInSlot->GetEquipped();
				}
				else if (pQSlot.slot_type == eQSType_Spell)
				{
					CGameXMLSettingAndGlobals::SActorSpell act_spell = pClActor->GetSpellInfoFromActorSB(pQSlot.obj_in_slot_id);
					if (act_spell.spell_id == -1)
						continue;

					slot_icon = act_spell.spell_icon_path[1];
					quantity = 0;
					type_id = 2;
					is_selected = (pClActor->GetSpellIdFromCurrentSpellsForUseSlot(1) > 0 || 
						pClActor->GetSpellIdFromCurrentSpellsForUseSlot(2) > 0);
				}
				else if (pQSlot.slot_type == eQSType_Ability)
				{

				}
				SFlashVarValue argQSlot[5] = { i, slot_icon.c_str(), quantity, type_id, is_selected };
				pQuickSelectionMenu->GetFlashPlayer()->Invoke("addObjVisToQuickSlot", argQSlot, 5);
			}
		}

		if (!show)
		{
			if (!AnyInteractiveUIOpened())
				CreateUIActionFilter(eHUIAFT_interactive, show);

			OnInterInUIMode(eCTOSUIOpen_QuickSelection, show);
			pQuickSelectionMenu->GetFlashPlayer()->SetVisible(show);
			pQuickSelectionMenu->SetVisible(show);
			pQuickSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_QuickSelectionVisible = show;
			timer_to_can_item_press = 0.2f;
		}
	}

}

void CHUDCommon::UpdateQuickSelectionMenu()
{
	IUIElement* pQuickSelectionMenu = gEnv->pFlashUI->GetUIElement("QuickSelectionMenu");
	if (!pQuickSelectionMenu)
		return;

	if (!m_QuickSelectionVisible)
		return;
	
	CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClActor)
		return;

	for (int i = 1; i < MAX_QUICK_EQUIPMENT_SLOTS + 1; i++)
	{
		SFlashVarValue argQSlot[5] = { i, "", 0, 0, 0 };
		pQuickSelectionMenu->GetFlashPlayer()->Invoke("addObjVisToQuickSlot", argQSlot, 5);
	}

	for (int i = 1; i < MAX_QUICK_EQUIPMENT_SLOTS + 1; i++)
	{
		CActor::AcQuickSlot pQSlot = pClActor->GetQuickSlotInfo(i);
		if (pQSlot.slot_type <= 0)
			continue;

		string slot_icon = "";
		int quantity = 0;
		int type_id = 0;
		int is_selected = 0;
		if (pQSlot.slot_type == eQSType_Item)
		{
			EntityId item_id = (EntityId)pQSlot.obj_in_slot_id;
			if (item_id <= 0)
				continue;

			CItem *pItemInSlot = pClActor->GetItem(item_id);
			if (!pItemInSlot)
				continue;

			slot_icon = pItemInSlot->GetIcon();
			quantity = pItemInSlot->GetItemQuanity();
			type_id = 1;
			is_selected = pItemInSlot->GetEquipped();
		}
		else if (pQSlot.slot_type == eQSType_Spell)
		{
			CGameXMLSettingAndGlobals::SActorSpell act_spell = pClActor->GetSpellInfoFromActorSB(pQSlot.obj_in_slot_id);
			if (act_spell.spell_id == -1)
				continue;

			slot_icon = act_spell.spell_icon_path[1];
			quantity = 0;
			type_id = 2;
			is_selected = (pClActor->GetSpellIdFromCurrentSpellsForUseSlot(1) > 0 ||
				pClActor->GetSpellIdFromCurrentSpellsForUseSlot(2) > 0);
		}
		else if (pQSlot.slot_type == eQSType_Ability)
		{

		}
		SFlashVarValue argQSlot[5] = { i, slot_icon.c_str(), quantity, type_id, is_selected };
		pQuickSelectionMenu->GetFlashPlayer()->Invoke("addObjVisToQuickSlot", argQSlot, 5);
	}
}

void CHUDCommon::ShowOnDeathScreen(bool show)
{
	IUIElement* pDeathScreen = gEnv->pFlashUI->GetUIElement("DeathScreen");
	if (pDeathScreen)
	{
		if (!m_DeathScreenVisible && show)
		{
			//if (!AnyInteractiveUIOpened())
			//	CreateUIActionFilter(eHUIAFT_interactive, show);

			//OnInterInUIMode(eCTOSUIOpen_QuickSelection, show);
			pDeathScreen->Reload();
			pDeathScreen->GetFlashPlayer()->SetVisible(show);
			pDeathScreen->SetVisible(show);
			pDeathScreen->GetFlashPlayer()->SetFSCommandHandler(this);
			m_DeathScreenVisible = show;
		}

		if (!show)
		{
			pDeathScreen->GetFlashPlayer()->SetVisible(show);
			pDeathScreen->SetVisible(show);
			pDeathScreen->GetFlashPlayer()->SetFSCommandHandler(NULL);
			//DelayActionForPlayer();
			m_DeathScreenVisible = show;
		}
	}
}

void CHUDCommon::ShowHints(bool show)
{
	IUIElement* pHintsUI = gEnv->pFlashUI->GetUIElement("InGameHints");
	if (pHintsUI)
	{
		if (!pHintsUI->GetFlashPlayer())
			return;

		if (!m_InGameHintsVisible && show)
		{
			pHintsUI->Reload();
			pHintsUI->GetFlashPlayer()->SetVisible(show);
			pHintsUI->SetVisible(show);
			pHintsUI->GetFlashPlayer()->SetFSCommandHandler(this);
			pHintsUI->GetFlashPlayer()->Invoke0("LoadClips");
			m_InGameHintsVisible = show;
		}

		if (!show)
		{
			pHintsUI->GetFlashPlayer()->SetVisible(show);
			pHintsUI->SetVisible(show);
			pHintsUI->GetFlashPlayer()->SetFSCommandHandler(NULL);
			m_InGameHintsVisible = show;
		}
	}
}

void CHUDCommon::AddHint(Vec2i xy, float time, string msg, float scale)
{
	IUIElement* pHintsUI = gEnv->pFlashUI->GetUIElement("InGameHints");
	if (pHintsUI)
	{
		if (m_InGameHintsVisible)
		{
			SFlashVarValue argsHint[5] = { xy.x, xy.y, time, msg.c_str(), scale };
			pHintsUI->GetFlashPlayer()->Invoke("AddMsg", argsHint, 5);
		}
	}
}

void CHUDCommon::ShowOnScreenObjInfo(bool show)
{
	IUIElement* pOnScreenObjInfo = gEnv->pFlashUI->GetUIElement("OnScreenObjInfo");
	if (pOnScreenObjInfo)
	{
		if (!pOnScreenObjInfo->GetFlashPlayer())
			return;

		if (!m_OnScreenObjInfoVisible && show)
		{
			pOnScreenObjInfo->Reload();
			pOnScreenObjInfo->GetFlashPlayer()->SetVisible(show);
			pOnScreenObjInfo->SetVisible(show);
			pOnScreenObjInfo->GetFlashPlayer()->SetFSCommandHandler(this);
			m_OnScreenObjInfoVisible = show;
			entites_ont_screen_to_update.clear();
			target_entites_ont_screen_to_update.clear();
		}

		if (!show)
		{
			pOnScreenObjInfo->GetFlashPlayer()->SetVisible(show);
			pOnScreenObjInfo->SetVisible(show);
			pOnScreenObjInfo->GetFlashPlayer()->SetFSCommandHandler(NULL);
			m_OnScreenObjInfoVisible = show;
		}
	}
}

void CHUDCommon::UpdateOnScreenObjInfo()
{
	if (!m_OnScreenObjInfoVisible)
		return;

	IUIElement* pOnScreenObjInfo = gEnv->pFlashUI->GetUIElement("OnScreenObjInfo");
	if (!pOnScreenObjInfo)
		return;

	if (!pOnScreenObjInfo->GetFlashPlayer())
		return;

	CActor *pClActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClActor)
		return;

	bool do_prim_update = false;
	bool do_second_update = false;
	objInf_update_timer -= gEnv->pTimer->GetFrameTime();
	if (objInf_update_timer < 0.0f)
	{
		objInf_update_timer = objInf_update_delay;
		do_prim_update = true;
	}
	objInf_update_timer2 -= gEnv->pTimer->GetFrameTime();
	if (objInf_update_timer2 < 0.0f)
	{
		objInf_update_timer2 = objInf_update_delay2;
		do_second_update = true;
	}

	if (!do_prim_update && !do_second_update)
		return;

	float scan_range = g_pGameCVars->g_minimap_pl_max_actors_dist_on_minimap;
	Vec3 mp_pos = pClActor->GetEntity()->GetPos();
	if (do_prim_update)
	{
		Vec3 boxDim(scan_range, scan_range, scan_range);
		Vec3 ptmin = mp_pos - boxDim;
		Vec3 ptmax = mp_pos + boxDim;
		IPhysicalEntity** pMmEntityList = NULL;
		static const int iObjTypes = /*ent_rigid | ent_sleeping_rigid | */ent_living;
		int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pMmEntityList, iObjTypes);
		for (int i = 0; i < numEntities; ++i)
		{
			IPhysicalEntity* pPhysicalEntity = pMmEntityList[i];
			IEntity* pMapEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));
			if (pMapEntity)
			{
				if (pMapEntity->GetId() == pClActor->GetEntity()->GetId())
					continue;

				bool ent_added = stl::find(entites_ont_screen_to_update, pMapEntity->GetId());
				if (ent_added)
				{

				}
				else
				{
					CActor *pMpActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pMapEntity->GetId()));
					if (!pMpActor)
						continue;

					if(!IsActorVisibleByPlayer(pMapEntity->GetId(), scan_range))
						continue;

					float hp = pMpActor->GetHealth();
					int max_hp = pMpActor->GetMaxHealth();
					float fActHp = (hp / (float)max_hp) * 100.0f + 1.0f;
					Vec3 entPos = pMapEntity->GetWorldPos();
					float dist = pClActor->GetEntity()->GetWorldPos().GetDistance(entPos);
					entPos.z += 1.95f;
					Vec3 screenPos;
					gEnv->pRenderer->ProjectToScreen(entPos.x, entPos.y, entPos.z, &screenPos.x, &screenPos.y, &screenPos.z);
					screenPos.x *= 0.01f;
					screenPos.y *= 0.01f;
					int falloffx = screenPos.x < 0 ? -1 : screenPos.x > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
					int falloffy = screenPos.y < 0 ? -1 : screenPos.y > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
					bool in_scr = (falloffx + falloffy == 0);
					if(!in_scr)
						continue;

					if (dist > scan_range)
					{
						dist = scan_range;
					}
					float scale = (dist / scan_range) * 100.0f + 1.0f;
					if (scale > 100.0f)
						scale = 100.0f;

					scale *= 0.01;
					Vec2i pscsr = GetEntityPositionOnScreenInUIElement(pOnScreenObjInfo, entPos, Vec3(ZERO));
					SFlashVarValue argsActMark[6] = { pscsr.x, pscsr.y, pMapEntity->GetId(), 1, fActHp, scale };
					bool add_to_vcr = true;
					uint8	m_localPlayerFaction;
					uint8	m_mpActorFaction;
					IAIObject* pAIObjectMpActor = pMpActor->GetEntity()->GetAI();
					m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
					IAIObject* pAIObjectLocPl = pClActor->GetEntity()->GetAI();
					m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
					if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
					{
						if (g_pGameCVars->hud_show_enemes_hp_bars <= 0)
							continue;

						pOnScreenObjInfo->GetFlashPlayer()->Invoke("AddElement", argsActMark, 6);
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral)
					{
						if (g_pGameCVars->hud_show_neutral_hp_bars <= 0)
							continue;

						argsActMark[3] = 3;
						pOnScreenObjInfo->GetFlashPlayer()->Invoke("AddElement", argsActMark, 6);
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
					{
						if (g_pGameCVars->hud_show_friendly_hp_bars <= 0)
							continue;

						argsActMark[3] = 3;
						pOnScreenObjInfo->GetFlashPlayer()->Invoke("AddElement", argsActMark, 6);
					}

					if (add_to_vcr)
					{
						entites_ont_screen_to_update.push_back(pMapEntity->GetId());
					}
				}
			}
		}
	}

	if (do_second_update)
	{
		std::vector<EntityId> entites_to_not_delete_from_screen;
		entites_to_not_delete_from_screen.clear();
		int count_added_ents = entites_ont_screen_to_update.size();
		std::vector<EntityId>::const_iterator it = entites_ont_screen_to_update.begin();
		std::vector<EntityId>::const_iterator end = entites_ont_screen_to_update.end();
		for (int i = 0; i < count_added_ents, it != end; i++, ++it)
		{
			EntityId this_ent = (*it);
			IEntity* pMapEntity = gEnv->pEntitySystem->GetEntity(this_ent);
			if (pMapEntity)
			{
				Vec3 entPos = pMapEntity->GetWorldPos();
				entPos.z += 1.95f;
				Vec3 screenPos;
				gEnv->pRenderer->ProjectToScreen(entPos.x, entPos.y, entPos.z, &screenPos.x, &screenPos.y, &screenPos.z);
				screenPos.x *= 0.01f;
				screenPos.y *= 0.01f;
				int falloffx = screenPos.x < 0 ? -1 : screenPos.x > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
				int falloffy = screenPos.y < 0 ? -1 : screenPos.y > 1 ? 1 : screenPos.z < 1 ? 0 : -1;
				bool in_scr = (falloffx + falloffy == 0);

				float fActHp = 100.0f;
				CActor *pMpActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pMapEntity->GetId()));
				if (pMpActor)
				{
					float hp = pMpActor->GetHealth();
					int max_hp = pMpActor->GetMaxHealth();
					fActHp = (hp / (float)max_hp) * 100.0f + 1.0f;
				}

				if (!pMpActor)
					continue;

				float dist_to_pl = mp_pos.GetDistance(pMapEntity->GetPos());
				if (dist_to_pl < scan_range && !pMpActor->IsDead() && in_scr)
				{
					float dist = pClActor->GetEntity()->GetWorldPos().GetDistance(entPos);
					if (dist > scan_range)
					{
						dist = scan_range;
					}
					float scale = (dist / scan_range) * 100.0f + 1.0f;
					if (scale > 100.0f)
						scale = 100.0f;

					scale *= 0.01;
					Vec2i pscsr = GetEntityPositionOnScreenInUIElement(pOnScreenObjInfo, entPos, Vec3(ZERO));

					SFlashVarValue argsActMark[6] = { pscsr.x, pscsr.y, pMapEntity->GetId(), 1, fActHp, scale };
					pOnScreenObjInfo->GetFlashPlayer()->Invoke("UpdateElement", argsActMark, 6);
					entites_to_not_delete_from_screen.push_back(this_ent);
					continue;
				}
				else
				{
					SFlashVarValue argsEntPoint[2] = { this_ent, 1 };
					pOnScreenObjInfo->GetFlashPlayer()->Invoke("RemoveElement", argsEntPoint, 2);
					continue;
				}
			}
			else
			{
				SFlashVarValue argsEntPoint[2] = { this_ent, 1 };
				pOnScreenObjInfo->GetFlashPlayer()->Invoke("RemoveElement", argsEntPoint, 2);
				continue;
			}
		}
		entites_ont_screen_to_update.clear();
		entites_ont_screen_to_update = entites_to_not_delete_from_screen;
		std::vector<EntityId>::const_iterator it_f = target_entites_ont_screen_to_update.begin();
		std::vector<EntityId>::const_iterator end_f = target_entites_ont_screen_to_update.end();
		int count = target_entites_ont_screen_to_update.size();
		for (int i = 0; i < count, it_f != end_f; i++, ++it_f)
		{
			EntityId this_ent = (*it_f);
			IEntity* pTargetEntity = gEnv->pEntitySystem->GetEntity(this_ent);
			if (pTargetEntity)
			{
				AABB targetBbox;
				pTargetEntity->GetWorldBounds(targetBbox);
				Vec3 entPos = targetBbox.GetCenter();
				//entPos.z += 0.5f;
				Vec2i pscsr = GetEntityPositionOnScreenInUIElement(pOnScreenObjInfo, entPos, Vec3(ZERO));
				SFlashVarValue argsActMark[6] = { pscsr.x, pscsr.y, pTargetEntity->GetId(), 2, 0.0f, 1.0f };
				pOnScreenObjInfo->GetFlashPlayer()->Invoke("UpdateElement", argsActMark, 6);
			}
		}
	}
}

void CHUDCommon::AddTargetToScreenObjInfo(EntityId id)
{
	if (!m_OnScreenObjInfoVisible)
		return;

	IUIElement* pOnScreenObjInfo = gEnv->pFlashUI->GetUIElement("OnScreenObjInfo");
	if (!pOnScreenObjInfo)
		return;

	std::vector<EntityId>::const_iterator it_f = target_entites_ont_screen_to_update.begin();
	std::vector<EntityId>::const_iterator end_f = target_entites_ont_screen_to_update.end();
	int count = target_entites_ont_screen_to_update.size();
	for (int i = 0; i < count, it_f != end_f; i++, ++it_f)
	{
		EntityId this_ent = (*it_f);
		if (this_ent == id)
		{
			DelTargetFromScreenObjInfo(id);
			break;
		}
	}

	target_entites_ont_screen_to_update.push_back(id);
	IEntity* pTargetEntity = gEnv->pEntitySystem->GetEntity(id);
	if (pTargetEntity)
	{
		AABB targetBbox;
		pTargetEntity->GetWorldBounds(targetBbox);
		targetBbox.GetCenter();
		Vec3 entPos = targetBbox.GetCenter();
		Vec2i pscsr = GetEntityPositionOnScreenInUIElement(pOnScreenObjInfo, entPos, Vec3(ZERO));
		SFlashVarValue argsActMark[6] = { pscsr.x, pscsr.y, pTargetEntity->GetId(), 2, 0.0f, 1.0f };
		pOnScreenObjInfo->GetFlashPlayer()->Invoke("AddElement", argsActMark, 6);
	}
}

void CHUDCommon::DelTargetFromScreenObjInfo(EntityId id)
{
	if (!m_OnScreenObjInfoVisible)
		return;

	IUIElement* pOnScreenObjInfo = gEnv->pFlashUI->GetUIElement("OnScreenObjInfo");
	if (!pOnScreenObjInfo)
		return;

	std::vector<EntityId>::const_iterator it_f = target_entites_ont_screen_to_update.begin();
	std::vector<EntityId>::const_iterator end_f = target_entites_ont_screen_to_update.end();
	int count = target_entites_ont_screen_to_update.size();
	for (int i = 0; i < count, it_f != end_f; i++, ++it_f)
	{
		EntityId this_ent = (*it_f);
		if (this_ent == id)
		{
			target_entites_ont_screen_to_update.erase(it_f);
			target_entites_ont_screen_to_update.resize(count - 1);
			SFlashVarValue argsEntPoint[2] = { this_ent, 2 };
			pOnScreenObjInfo->GetFlashPlayer()->Invoke("RemoveElement", argsEntPoint, 2);
			break;
		}
	}
}

void CHUDCommon::ShowEnemyHPBar(bool show)
{

}

void CHUDCommon::ShowArcadeCompass(bool show)
{

}

void CHUDCommon::ShowPostInitialization(bool show)
{
	if (gEnv->IsEditor() && show)
		return;

	if(g_pGameCVars->g_enable_postinitialization < 1 && show)
		return;

	if (m_DeathScreenVisible && show)
		ShowOnDeathScreen(false);

	IUIElement* pLevelPostInitializationScreen = gEnv->pFlashUI->GetUIElement("LevelPostInitializationScreen");
	if (pLevelPostInitializationScreen)
	{
		if (!pLevelPostInitializationScreen->GetFlashPlayer())
			return;

		if (!m_PostInitScreenVisible && show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, true);
			pLevelPostInitializationScreen->Reload();
			pLevelPostInitializationScreen->GetFlashPlayer()->SetVisible(show);
			pLevelPostInitializationScreen->SetVisible(show);
			pLevelPostInitializationScreen->GetFlashPlayer()->SetFSCommandHandler(this);
			m_PostInitScreenVisible = show;
			if (gEnv->pConsole)
			{
				gEnv->pConsole->ExecuteString("ai_NoUpdate 1");
				gEnv->pConsole->ExecuteString("p_fixed_timestep 0.00001");
			}
		}

		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_dialog, false);
			pLevelPostInitializationScreen->GetFlashPlayer()->SetVisible(show);
			pLevelPostInitializationScreen->SetVisible(show);
			pLevelPostInitializationScreen->GetFlashPlayer()->SetFSCommandHandler(NULL);
			m_PostInitScreenVisible = show;
			if (gEnv->pConsole)
			{
				gEnv->pConsole->ExecuteString("ai_NoUpdate 0");
				gEnv->pConsole->ExecuteString("p_fixed_timestep 0");
			}
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
				nwAction->DelayedUpdatePlShadowChr(0.41f);
		}
	}
}

void CHUDCommon::ShowSpellSelectionMenu(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_SpellSelectionMenu))
		return;

	IUIElement* pSpellSelectionMenu = gEnv->pFlashUI->GetUIElement("SpellSelectionUI");
	if (pSpellSelectionMenu)
	{
		if (!m_SpellSelectionVisible && show)
		{
			CreateUIActionFilter(eHUIAFT_interactive, true);
			OnInterInUIMode(eCTOSUIOpen_SpellSelectionMenu, show);
			pSpellSelectionMenu->Reload();
			pSpellSelectionMenu->GetFlashPlayer()->SetVisible(true);
			pSpellSelectionMenu->SetVisible(true);
			pSpellSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(this);
			m_SpellSelectionVisible = show;
			if (g_pGameCVars->hud_spells_selection_type == 1)
			{
				SFlashVarValue arg[1] = { true };
				pSpellSelectionMenu->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
			}
			else
			{
				SFlashVarValue arg[1] = { false };
				pSpellSelectionMenu->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
			}
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (!pGameGlobals)
				return;

			CActor  *pActor = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
			if (!pActor)
				return;

			SFlashVarValue args[1] = { int(pActor->m_actor_spells_book.size()) };
			pSpellSelectionMenu->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
			SFlashVarValue args_tab[1] = { current_spell_selection_tab_opened };
			pSpellSelectionMenu->GetFlashPlayer()->Invoke("selectInventorySection", args_tab, 1);
			UpdateSpellSelection(1.0f);
		}

		if (!show)
		{
			CreateUIActionFilter(eHUIAFT_interactive, false);
			OnInterInUIMode(eCTOSUIOpen_SpellSelectionMenu, show);
			pSpellSelectionMenu->GetFlashPlayer()->SetVisible(false);
			pSpellSelectionMenu->SetVisible(false);
			pSpellSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(NULL);
			DelayActionForPlayer();
			m_SpellSelectionVisible = show;
			if (SpellInfo_loaded)
			{
				ShowSpellInformation(false, false, 0);
			}
		}
	}
}

void CHUDCommon::ShowChestInv(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_ContainerInventory))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	EntityId id = pGameGlobals->current_opened_Chest;
	CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	if (!pActor && show)
		return;

	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (!pLootUI)
		return;

	if (Inv_ItemInfo_loaded)
		ShowItemInformation(false, false, 0);

	if (Inv_dragged_item_loaded)
		ShowDraggedItemIcon(false, 0);

	if (show && !m_ChestInvVisible)
	{
		current_chest_or_shop_tab_opened = 3;
		chest_or_shop_showed_player_inv = false;
		CreateUIActionFilter(eHUIAFT_interactive, true);
		OnInterInUIMode(eCTOSUIOpen_ContainerInventory, show);
		pLootUI->Reload();
		pLootUI->GetFlashPlayer()->SetVisible(true);
		pLootUI->SetVisible(true);
		pLootUI->GetFlashPlayer()->SetFSCommandHandler(this);
		CActor  *pClient = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pClient)
			return;

		int plgold = pClient->GetGold();
		int trgold = pActor->GetGold();
		SFlashVarValue argGold[1] = { plgold };
		SFlashVarValue argGoldTr[1] = { trgold };
		pLootUI->GetFlashPlayer()->Invoke("setChestMoney", argGoldTr, 1);
		pLootUI->GetFlashPlayer()->Invoke("setPlayerMoney", argGold, 1);
		if (g_pGameCVars->hud_container_store_type == 1)
		{
			SFlashVarValue arg[1] = { true };
			pLootUI->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}
		else
		{
			SFlashVarValue arg[1] = { false };
			pLootUI->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}

		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pLootUI->GetFlashPlayer()->Invoke("setChestInvVisible", args_p_inv, 1);
		SFlashVarValue args[1] = { pActor->GetMaxInvSlots() };
		if (chest_or_shop_showed_player_inv)
			args[0] = pClient->GetMaxInvSlots();

		pLootUI->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
		SFlashVarValue args_tab[1] = { current_chest_or_shop_tab_opened };
		pLootUI->GetFlashPlayer()->Invoke("selectInventorySection", args_tab, 1);
		//extra support drop items to container/////////////////////////
		IEntity *pEntity = pActor->GetEntity();
		SmartScriptTable props;
		IScriptTable* pScriptTable = pEntity->GetScriptTable();
		int drop_to_support = 0;
		if (pScriptTable && pScriptTable->GetValue("Properties", props))
		{
			CScriptSetGetChain prop(props);
			prop.GetValue("dropable_container", drop_to_support);
		}

		if (drop_to_support > 0)
		{
			CryLogAlways("drop_to_support > 0 ShowSelectionBar");
			SFlashVarValue arg[1] = { true };
			pLootUI->GetFlashPlayer()->Invoke("ShowSelectionBar", arg, 1);
		}
		else
		{
			SFlashVarValue arg[1] = { false };
			pLootUI->GetFlashPlayer()->Invoke("ShowSelectionBar", arg, 1);
		}
		////////////////////////////////////////////////////////////////
		m_ChestInvVisible = show;
		//ClearChestInventory();
		UpdateChestInventory(1.0f);
	}

	if (!show)
	{
		CreateUIActionFilter(eHUIAFT_interactive, false);
		OnInterInUIMode(eCTOSUIOpen_ContainerInventory, show);
		pLootUI->GetFlashPlayer()->SetVisible(false);
		pLootUI->SetVisible(false);
		pLootUI->GetFlashPlayer()->SetFSCommandHandler(NULL);
		DelayActionForPlayer();
		m_ChestInvVisible = show;
	}
}

void CHUDCommon::ShowChestInvsss(EntityId entityId, bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_ContainerInventory))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor  *pClient = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	EntityId id = entityId;
	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	if (!pActor)
	{
		pActor = pClient;
	}

	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (!pLootUI)
		return;

	if (Inv_ItemInfo_loaded)
		ShowItemInformation(false, false, 0);

	if (Inv_dragged_item_loaded)
		ShowDraggedItemIcon(false, 0);

	if (show && !m_ChestInvVisible)
	{
		current_chest_or_shop_tab_opened = 3;
		chest_or_shop_showed_player_inv = false;
		CreateUIActionFilter(eHUIAFT_interactive, true);
		OnInterInUIMode(eCTOSUIOpen_ContainerInventory, show);
		pLootUI->Reload();
		pLootUI->GetFlashPlayer()->SetVisible(true);
		pLootUI->SetVisible(true);
		pLootUI->GetFlashPlayer()->SetFSCommandHandler(this);

		int plgold = pClient->GetGold();
		int trgold = pActor->GetGold();
		SFlashVarValue argGold[1] = { plgold };
		SFlashVarValue argGoldTr[1] = { trgold };
		if(pActor != pClient)
			pLootUI->GetFlashPlayer()->Invoke("setChestMoney", argGoldTr, 1);

		pLootUI->GetFlashPlayer()->Invoke("setPlayerMoney", argGold, 1);
		if (g_pGameCVars->hud_container_store_type == 1)
		{
			SFlashVarValue arg[1] = { true };
			pLootUI->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}
		else
		{
			SFlashVarValue arg[1] = { false };
			pLootUI->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}

		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pLootUI->GetFlashPlayer()->Invoke("setChestInvVisible", args_p_inv, 1);
		SFlashVarValue args[1] = { pActor->GetMaxInvSlots() };
		//extra support drop items to container/////////////////////////
		int drop_to_support = 0;
		if (pActor != pClient)
		{
			IEntity *pEntity = pActor->GetEntity();
			SmartScriptTable props;
			IScriptTable* pScriptTable = pEntity->GetScriptTable();
			if (pScriptTable && pScriptTable->GetValue("Properties", props))
			{
				CScriptSetGetChain prop(props);
				prop.GetValue("dropable_container", drop_to_support);
			}
		}
		else
		{
			IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(id);
			if (pEntityc)
			{
				if (pGameGlobals->GetThisContainer(pEntityc->GetId()))
				{
					CryLogAlways("ShowChestInvsss called 3");
					args[0] = pGameGlobals->GetThisContainer(pEntityc->GetId())->GetInvSlotsNum();
					drop_to_support = pGameGlobals->GetThisContainer(pEntityc->GetId())->IsStoreAble();
				}
			}
		}
		//-----------------------------------------------------------------------------------
		if (chest_or_shop_showed_player_inv)
			args[0] = pClient->GetMaxInvSlots();

		pLootUI->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
		SFlashVarValue args_tab[1] = { current_chest_or_shop_tab_opened };
		pLootUI->GetFlashPlayer()->Invoke("selectInventorySection", args_tab, 1);
		if (drop_to_support > 0)
		{
			CryLogAlways("drop_to_support > 0 ShowSelectionBar");
			SFlashVarValue arg[1] = { true };
			pLootUI->GetFlashPlayer()->Invoke("ShowSelectionBar", arg, 1);
		}
		else
		{
			SFlashVarValue arg[1] = { false };
			pLootUI->GetFlashPlayer()->Invoke("ShowSelectionBar", arg, 1);
		}
		////////////////////////////////////////////////////////////////
		m_ChestInvVisible = show;
		//ClearChestInventory();
		UpdateChestInventory(1.0f, entityId);
	}

	if (!show)
	{
		CreateUIActionFilter(eHUIAFT_interactive, false);
		OnInterInUIMode(eCTOSUIOpen_ContainerInventory, show);
		pLootUI->GetFlashPlayer()->SetVisible(false);
		pLootUI->SetVisible(false);
		pLootUI->GetFlashPlayer()->SetFSCommandHandler(NULL);
		DelayActionForPlayer();
		m_ChestInvVisible = show;
	}
}

void CHUDCommon::ShowInteractiveObjectInfo(string icon, string text, int interactive, bool show)
{
	//CryLogAlways("CHUDCommon::ShowInteractiveObjectInfo");
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_InteractiveObjectInfo))
		return;

	IUIElement* pInteractiveInfo = gEnv->pFlashUI->GetUIElement("InteractiveInfo");
	if (!pInteractiveInfo)
		return;

	if(!pInteractiveInfo->GetFlashPlayer())
		return;

	if (!m_InteractiveInfoVisible && show)
	{
		if (!icon.empty() || !text.empty())
			isInteractiveInfoObjectShowed = true;

		if (icon.empty() && text.empty())
			isInteractiveInfoObjectShowed = false;

		pInteractiveInfo->Reload();
		pInteractiveInfo->GetFlashPlayer()->SetVisible(true);
		pInteractiveInfo->SetVisible(true);
		m_InteractiveInfoVisible = show;
		const char* icon_str = icon.c_str();
		const char* txt_str = text.c_str();
		SFlashVarValue args_icon[1] = { icon_str };
		SFlashVarValue args_txt[1] = { txt_str };
		pInteractiveInfo->GetFlashPlayer()->Invoke("setImage", args_icon, 1);
		pInteractiveInfo->GetFlashPlayer()->Invoke("setText", args_txt, 1);
	}

	if (!show)
	{
		pInteractiveInfo->GetFlashPlayer()->SetVisible(show);
		pInteractiveInfo->SetVisible(show);
		m_InteractiveInfoVisible = show;
		isInteractiveInfoObjectShowed = false;
	}
}

void CHUDCommon::ShowQuestSystem(bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_QuestSystemUI))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	IUIElement* pPlayerQuestInfo = gEnv->pFlashUI->GetUIElement("PlayerQuestInfo");
	if (!pPlayerQuestInfo)
		return;

	if (!pPlayerQuestInfo->GetFlashPlayer())
		return;

	if (show && !m_QuestSystemVisible)
	{
		CreateUIActionFilter(eHUIAFT_interactive, true);
		OnInterInUIMode(eCTOSUIOpen_QuestSystemUI, show);
		pPlayerQuestInfo->Reload();
		pPlayerQuestInfo->GetFlashPlayer()->SetVisible(true);
		pPlayerQuestInfo->SetVisible(true);
		pPlayerQuestInfo->GetFlashPlayer()->SetFSCommandHandler(this);
		m_QuestSystemVisible = show;
		for (std::vector<CGameXMLSettingAndGlobals::SPlayerQuest>::iterator it = pGameGlobals->s_pl_Quests.begin(); it != pGameGlobals->s_pl_Quests.end(); ++it)
		{
			CGameXMLSettingAndGlobals::SPlayerQuest quest = *it;
			SFlashVarValue args[6] = { quest.m_questname, quest.m_sdec, quest.m_ldec, quest.m_queststat, quest.m_lastcn, quest.m_questflag };
			pPlayerQuestInfo->GetFlashPlayer()->Invoke("showJournalQuest", args, 6);
		}
		for (std::vector<CGameXMLSettingAndGlobals::SPlayerSubQuest>::iterator itr = pGameGlobals->s_pl_SubQuests.begin(); itr != pGameGlobals->s_pl_SubQuests.end(); ++itr)
		{
			CGameXMLSettingAndGlobals::SPlayerSubQuest subquest = *itr;
			SFlashVarValue argss[6] = { subquest.m_questname, subquest.m_sdec, subquest.m_ldec, subquest.m_queststat, subquest.m_lastcn, subquest.m_parentquestname };
			pPlayerQuestInfo->GetFlashPlayer()->Invoke("showJournalSubQuest", argss, 6);
		}
	}

	if (!show)
	{
		/*for (std::vector<CGameXMLSettingAndGlobals::SPlayerQuest>::iterator it = pGameGlobals->s_pl_Quests.begin(); it != pGameGlobals->s_pl_Quests.end(); ++it)
		{
			CGameXMLSettingAndGlobals::SPlayerQuest quest = *it;
			quest.m_lastcn = 0;
		}
		for (std::vector<CGameXMLSettingAndGlobals::SPlayerSubQuest>::iterator itr = pGameGlobals->s_pl_SubQuests.begin(); itr != pGameGlobals->s_pl_SubQuests.end(); ++itr)
		{
			CGameXMLSettingAndGlobals::SPlayerSubQuest subquest = *itr;
			subquest.m_lastcn = 0;
		}*/
		CreateUIActionFilter(eHUIAFT_interactive, false);
		OnInterInUIMode(eCTOSUIOpen_QuestSystemUI, show);
		pPlayerQuestInfo->GetFlashPlayer()->SetVisible(false);
		pPlayerQuestInfo->SetVisible(false);
		pPlayerQuestInfo->GetFlashPlayer()->SetFSCommandHandler(NULL);
		DelayActionForPlayer();
		m_QuestSystemVisible = show;
	}
}

void CHUDCommon::ShowTradeSystem(EntityId entityId, bool show)
{
	if (show && !CheckForOpenedOthersUI(eCTOSUIOpen_TraiderInventory))
		return;

	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	if (!pShopInterface)
		return;

	if (!pShopInterface->GetFlashPlayer())
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (Inv_ItemInfo_loaded)
		ShowItemInformation(false, false, 0);

	if (Inv_dragged_item_loaded)
		ShowDraggedItemIcon(false, 0);

	if (!m_TradeSystemVisible && show)
	{
		current_Seller = entityId;
		current_chest_or_shop_tab_opened = 3;
		chest_or_shop_showed_player_inv = false;
		CreateUIActionFilter(eHUIAFT_interactive, true);
		OnInterInUIMode(eCTOSUIOpen_TraiderInventory, show);
		pShopInterface->Reload();
		pShopInterface->GetFlashPlayer()->SetVisible(true);
		pShopInterface->SetVisible(true);
		pShopInterface->GetFlashPlayer()->SetFSCommandHandler(this);
		m_TradeSystemVisible = show;

		EntityId idft = entityId;
		CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(idft));
		CActor  *pClient = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActor || !pClient)
			return;

		int slots_numder_tr = pActor->GetMaxInvSlots();
		IInventory *pInventory = pActor->GetInventory();
		IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(pGameGlobals->current_traider_chest);
		if (pEntityc)
		{
			CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
			if (pChest)
			{
				pInventory = pChest->GetInventory();
				slots_numder_tr = pChest->GetInvSlotsNum();
			}
		}
		IInventory *pInventoryplayer = pClient->GetInventory();
		if (!pInventory || !pInventoryplayer)
			return;

		IEntity * pEntity = pActor->GetEntity();
		IEntity * pEntitypl = pClient->GetEntity();
		SmartScriptTable props;
		IScriptTable* pScriptTable = pEntity->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		CScriptSetGetChain prop(props);
		float tradingskill;
		prop.GetValue("tradingskill", tradingskill);
		const char *charactername;
		prop.GetValue("charactername", charactername);
		SmartScriptTable propspl;
		IScriptTable* pScriptTablepl = pEntitypl->GetScriptTable();
		pScriptTablepl && pScriptTablepl->GetValue("Properties", propspl);
		CScriptSetGetChain proppl(propspl);
		const char *characternamepl;
		proppl.GetValue("charactername", characternamepl);
		string plicon = "";
		string plname = characternamepl;
		int plgold = pClient->GetGold();
		string tricon = "";
		string trname = charactername;
		int trgold = pActor->GetGold();
		SFlashVarValue argGold[1] = { plgold };
		SFlashVarValue argGoldTr[1] = { trgold };
		pShopInterface->GetFlashPlayer()->Invoke("setMerchantMoney", argGoldTr, 1);
		pShopInterface->GetFlashPlayer()->Invoke("setPlayerMoney", argGold, 1);
		if (g_pGameCVars->hud_merchant_store_type == 1)
		{
			SFlashVarValue arg[1] = { true };
			pShopInterface->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}
		else
		{
			SFlashVarValue arg[1] = { false };
			pShopInterface->GetFlashPlayer()->Invoke("enableListViewInventory", arg, 1);
		}

		SFlashVarValue args_p_inv[1] = { !chest_or_shop_showed_player_inv };
		pShopInterface->GetFlashPlayer()->Invoke("enableSellerInv", args_p_inv, 1);
		SFlashVarValue args[1] = { slots_numder_tr };
		if (chest_or_shop_showed_player_inv)
			args[0] = pClient->GetMaxInvSlots();

		pShopInterface->GetFlashPlayer()->Invoke("setSlotsQuantity", args, 1);
		SFlashVarValue args_tab[1] = { current_chest_or_shop_tab_opened };
		pShopInterface->GetFlashPlayer()->Invoke("selectInventorySection", args_tab, 1);
		UpdateTradeSystem(1.0f);
	}

	if (!show)
	{
		CreateUIActionFilter(eHUIAFT_interactive, false);
		OnInterInUIMode(eCTOSUIOpen_TraiderInventory, show);
		pShopInterface->GetFlashPlayer()->SetVisible(false);
		pShopInterface->SetVisible(false);
		pShopInterface->GetFlashPlayer()->SetFSCommandHandler(NULL);
		DelayActionForPlayer();
		m_TradeSystemVisible = show;
		current_Seller = 0;
		if (g_pGameCVars->hud_auto_start_dialog_after_traiding_end > 0)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->ReEanableDialogAfterTraidingEnd();
			}
		}
	}
}

void CHUDCommon::ShowSimpleCrosshair(bool show, bool update_request)
{
	if ((m_DialogSystemVisible || m_DialogSystemAltVisible || m_CharacterCreationVisible || m_GamePausedIU) && show)
		return;

	if (gEnv->pGame->GetIGameFramework()->IsGamePaused() && show)
		return;

	CActor* pClient = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	IUIElement* pSimpleCrosshair = gEnv->pFlashUI->GetUIElement("SimpleCrosshair");
	if (pSimpleCrosshair)
	{
		if (!pSimpleCrosshair->GetFlashPlayer())
		{
			if (!show)
			{
				pSimpleCrosshair->SetVisible(show);
				SimpleCrosshair_loaded = show;
			}
			return;
		}

		if (update_request)
		{
			if (SimpleCrosshair_loaded)
			{
				float cr_scale = g_pGameCVars->hud_simple_crosshair_scale;
				float cr_alpha = g_pGameCVars->hud_simple_crosshair_alpha;
				SFlashVarValue args_scale[1] = { cr_scale };
				SFlashVarValue args_alpha[1] = { cr_alpha };
				pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairScale", args_scale, 1);
				pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairAlpha", args_alpha, 1);
				CItem *pCurrentItem = static_cast<CItem*>(pClient->GetCurrentItem());
				if (pCurrentItem)
				{
					string texture = pCurrentItem->GetPathToCrosshairTexture();
					if (texture.empty())
						texture = "empty_crosshair.png";

					SFlashVarValue args[1] = { texture.c_str() };
					pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairTexture", args, 1);
				}
				else
				{
					SFlashVarValue args[1] = { "empty_crosshair.png" };
					pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairTexture", args, 1);
				}
				return;
			}
		}

		if (show)
		{
			pSimpleCrosshair->Reload();
			pSimpleCrosshair->GetFlashPlayer()->SetVisible(show);
			pSimpleCrosshair->SetVisible(show);
			SimpleCrosshair_loaded = show;
			float cr_scale = g_pGameCVars->hud_simple_crosshair_scale;
			float cr_alpha = g_pGameCVars->hud_simple_crosshair_alpha;
			SFlashVarValue args_scale[1] = { cr_scale };
			SFlashVarValue args_alpha[1] = { cr_alpha };
			pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairScale", args_scale, 1);
			pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairAlpha", args_alpha, 1);
			CItem *pCurrentItem = static_cast<CItem*>(pClient->GetCurrentItem());
			if (pCurrentItem)
			{
				string texture = pCurrentItem->GetPathToCrosshairTexture();
				if (texture.empty())
					texture = "empty_crosshair.png";

				SFlashVarValue args[1] = { texture.c_str() };
				pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairTexture", args, 1);
			}
			else
			{
				SFlashVarValue args[1] = { "empty_crosshair.png" };
				pSimpleCrosshair->GetFlashPlayer()->Invoke("SetCrosshairTexture", args, 1);
			}
		}
		else
		{
			CryLogAlways("pSimpleCrosshair::OnPauseGame()");
			pSimpleCrosshair->GetFlashPlayer()->SetVisible(show);
			pSimpleCrosshair->SetVisible(show);
			SimpleCrosshair_loaded = show;
		}
	}
}

void CHUDCommon::ShowSelectedWeapons(bool show, bool update_request)
{
	if ((m_DialogSystemVisible || m_DialogSystemAltVisible || m_CharacterCreationVisible || m_GamePausedIU) && show)
		return;

	CActor* pClient = static_cast<CActor*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	IUIElement* pSelectedWeaponsInfo = gEnv->pFlashUI->GetUIElement("SelectedWeaponsInfo");
	if (pSelectedWeaponsInfo)
	{
		if (!pSelectedWeaponsInfo->GetFlashPlayer())
			return;

		if (update_request)
		{
			if (SelectedWeaponsInfo_loaded)
			{
				return;
			}
		}

		if (show)
		{
			pSelectedWeaponsInfo->Reload();
			pSelectedWeaponsInfo->GetFlashPlayer()->SetVisible(show);
			pSelectedWeaponsInfo->SetVisible(show);
			pSelectedWeaponsInfo->GetFlashPlayer()->SetFSCommandHandler(this);
			SelectedWeaponsInfo_loaded = show;
			for (int i = 0; i < 4; i++)
			{
				if (i == 0)
				{
					CItem *pPrimHandItem = static_cast<CItem*>(pClient->GetItem(pClient->GetCurrentEquippedItemId(eAESlot_Weapon)));
					if (pPrimHandItem)
					{
						string texture = pPrimHandItem->GetIcon();
						if (texture.empty())
							texture = "empty_prim_weapon.png";

						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetPrimWeaponIcon", args, 1);
						old_player_selected_item2 = pPrimHandItem->GetEntityId();
					}
					else
					{
						string texture = "empty_prim_weapon.png";
						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetPrimWeaponIcon", args, 1);
						old_player_selected_item2 = pClient->GetCurrentItemId();
						SFlashVarValue argsInd[2] = { 0, false };
						//if (pClient->GetCurrentItemId() != pClient->GetNoWeaponId())
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("ShowIndic", argsInd, 2);
					}

					if (pClient->GetCurrentItemId() == pClient->GetNoWeaponId())
					{
						SFlashVarValue argsInd[2] = { 0, false };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("ShowIndic", argsInd, 2);
					}
				}
				else if (i == 1)
				{
					CItem *pSecondHandItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pClient->GetAnyItemInLeftHand()));
					if (pSecondHandItem)
					{
						string texture = pSecondHandItem->GetIcon();
						if (texture.empty())
							texture = "empty_second_weapon.png";

						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetSecondWeaponIcon", args, 1);
						old_player_selected_item_left = pSecondHandItem->GetEntityId();
					}
					else
					{
						string texture = "empty_second_weapon.png";
						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetSecondWeaponIcon", args, 1);
						old_player_selected_item_left = 0;
						SFlashVarValue argsInd[2] = { 1, false };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("ShowIndic", argsInd, 2);
					}
				}
				else if (i == 2)
				{
					CGameXMLSettingAndGlobals::SActorSpell stSpell = pClient->GetSpellFromCurrentSpellsForUseSlot(1);
					if (stSpell.spell_id != -1)
					{
						string texture = stSpell.spell_icon_path[1].c_str();
						if (texture.empty())
							texture = "empty_prim_spell_icon.png";

						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetPrimSpellSlotIcon", args, 1);
						old_player_selected_spell_id = stSpell.spell_id;
					}
					else
					{
						string texture = "empty_prim_spell_icon.png";
						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetPrimSpellSlotIcon", args, 1);
						old_player_selected_spell_id = -1;
						SFlashVarValue argsInd[2] = { 2, false };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("ShowIndic", argsInd, 2);
					}
				}
				else if (i == 3)
				{
					CGameXMLSettingAndGlobals::SActorSpell stSpell = pClient->GetSpellFromCurrentSpellsForUseSlot(2);
					if (stSpell.spell_id != -1)
					{
						string texture = stSpell.spell_icon_path[1].c_str();
						if (texture.empty())
							texture = "empty_second_spell_icon.png";

						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetSecondSpellSlotIcon", args, 1);
						old_player_selected_spell_id_left = stSpell.spell_id;
					}
					else
					{
						string texture = "empty_second_spell_icon.png";
						SFlashVarValue args[1] = { texture.c_str() };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("SetSecondSpellSlotIcon", args, 1);
						old_player_selected_spell_id_left = -1;
						SFlashVarValue argsInd[2] = { 3, false };
						pSelectedWeaponsInfo->GetFlashPlayer()->Invoke("ShowIndic", argsInd, 2);
					}
				}
			}
		}
		else
		{
			pSelectedWeaponsInfo->GetFlashPlayer()->SetVisible(show);
			pSelectedWeaponsInfo->SetVisible(show);
			pSelectedWeaponsInfo->GetFlashPlayer()->SetFSCommandHandler(NULL);
			SelectedWeaponsInfo_loaded = show;
		}
	}
}

void CHUDCommon::AddBuffOrDebuff(int buff_id, const char *buff_icon, int time, int type)
{
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (!pBuffsScreenInfo)
		return;

	if (!pBuffsScreenInfo->GetFlashPlayer())
		return;

	if (!BuffInfo_loaded)
		return;
	//buff_id = buff_id + 1;
	//CryLogAlways("AddBuffOrDebuff %i", buff_id);
	if (type == 1)
	{
		SFlashVarValue args[3] = { buff_id, buff_icon, time };
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("addTimedPositiveEffect", args, 3);
	}
	else if (type == 2)
	{
		SFlashVarValue args[3] = { buff_id, buff_icon, time };
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("addTimedNegativeEffect", args, 3);
	}
}

void CHUDCommon::RemoveBuffOrDebuff(int buff_id, int type)
{
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (!pBuffsScreenInfo)
		return;

	if (!pBuffsScreenInfo->GetFlashPlayer())
		return;
	
	if (!BuffInfo_loaded)
		return;
	//buff_id = buff_id + 1;
	//CryLogAlways("RemoveBuffOrDebuff type %i", type);
	SFlashVarValue args_buff[1] = { buff_id };
	if (type == 1)
	{
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("delTimedPositiveEffect", args_buff, 1);
	}
	else if (type == 2)
	{
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("delTimedNegativeEffect", args_buff, 1);
	}
}

void CHUDCommon::UpdateTimeForBuffOrDebuff(int buff_id, int time, int type)
{
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (!pBuffsScreenInfo)
		return;

	if (!pBuffsScreenInfo->GetFlashPlayer())
		return;

	//buff_id = buff_id + 1;
	if (type == 1)
	{
		SFlashVarValue argss[2] = { buff_id, time };
		/*pBuffsScreenInfo->GetFlashPlayer()->Invoke("setBuffTimer", argss, 2);
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("setCurrentBuffTime", argss, 2);*/
	}
	else if (type == 2)
	{
		SFlashVarValue argss[2] = { buff_id, time };
		/*pBuffsScreenInfo->GetFlashPlayer()->Invoke("setDebuffTimer", argss, 2);
		pBuffsScreenInfo->GetFlashPlayer()->Invoke("setCurrentBuffTime", argss, 2);*/
	}
}

bool CHUDCommon::CheckForOpenedOthersUI(ECallToOpenSystemUI SysUI)
{
	if (!gEnv->pFlashUI)
		return false;

	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	IUIElement* pPlayerQuestInfo = gEnv->pFlashUI->GetUIElement("PlayerQuestInfo");
	IUIElement* pInteractiveInfo = gEnv->pFlashUI->GetUIElement("InteractiveInfo");
	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	IUIElement* pInteractiveBookUI = gEnv->pFlashUI->GetUIElement("InteractiveBookUI");
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	IUIElement* pItemInfo = gEnv->pFlashUI->GetUIElement("ItemInformation");
	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	IUIElement* pStaticMessages_hud = gEnv->pFlashUI->GetUIElement("StaticMessages");
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
	bool can_open_ui = true;
	switch (SysUI)
	{
		case eCTOSUIOpen_none:
		{
								 break;
		}
		case eCTOSUIOpen_Inventory:
		{
									  if (m_QuickSelectionVisible)
										  ShowQuickSelectionMenu(false);

									  if (m_ChestInvVisible)
										  ShowChestInv(false);

									  if (m_DialogSystemVisible)
										  can_open_ui = false;

									  if (m_DialogSystemAltVisible)
										  can_open_ui = false;

									  //if (m_CharacterEquipmentVisible)
									//	  return true;

									 // if (m_Icmenu)
									//	  return true;

									  if (m_MainMapVisible)
										  ShowMainMap(false);

									  if (m_BookVisible)
										  ShowBook(false, 0);

									  if (m_CharacterCreationVisible)
										  can_open_ui = false;

									  if (m_TradeSystemVisible)
										  can_open_ui = false;

									  if (m_QuestSystemVisible)
										  ShowQuestSystem(false);

									  if (m_SpellSelectionVisible)
										  ShowSpellSelectionMenu(false);

									  if (m_CharacterPerksMenuVisible)
										  ShowChPerks(false);

									  if (m_CharacterSkillsMenuVisible)
										  ShowChSkills(false);

									  if (m_CharacterSkillInfoVisible)
										  ShowChSkills(false);

									  if (m_InteractiveInfoVisible)
										  ShowInteractiveObjectInfo("", "", 0, false);
									  
									  //if (Inv_loaded)
								  //	 can_open_ui = true;

									  if (ChrMenu_loaded)
										  ShowCharacterMenu();

									 // if (MainHud_loaded)
									//	  return true;

									  if (Inv_cont_menu_loaded)
										  DisableContextMenu();

									  if (Inv_ItemInfo_loaded)
										  ShowItemInformation(false, false, 0);

									 // if (StatMsg_loaded)
									//	  return true;
									  break;

		}
		case eCTOSUIOpen_CharacterMenu:
		{
										  if (m_QuickSelectionVisible)
										   	  ShowQuickSelectionMenu(false);

										  if (m_ChestInvVisible)
											  ShowChestInv(false);

										  if (m_DialogSystemVisible)
											  can_open_ui = false;

										  if (m_DialogSystemAltVisible)
											  can_open_ui = false;

										  //if (m_CharacterEquipmentVisible)
										  //	  return true;

										  // if (m_Icmenu)
										  //	  return true;

										  if (m_MainMapVisible)
											  ShowMainMap(false);

										  if (m_BookVisible)
											  ShowBook(false, 0);

										  if (m_CharacterCreationVisible)
											  can_open_ui = false;

										  if (m_TradeSystemVisible)
											  can_open_ui = false;

										  if (m_QuestSystemVisible)
											  ShowQuestSystem(false);

										  if (m_SpellSelectionVisible)
											  ShowSpellSelectionMenu(false);

										  if (m_CharacterPerksMenuVisible)
											  ShowChPerks(false);

										  if (m_CharacterSkillsMenuVisible)
											  ShowChSkills(false);

										  if (m_CharacterSkillInfoVisible)
											  ShowChSkills(false);

										  if (m_InteractiveInfoVisible)
											  ShowInteractiveObjectInfo("", "", 0, false);

										  if (Inv_loaded)
											  ShowInventory();

										  //if (ChrMenu_loaded)
										//	  can_open_ui = true;

										  // if (MainHud_loaded)
										  //	  return true;

										  if (Inv_cont_menu_loaded)
											  DisableContextMenu();

										  if (Inv_ItemInfo_loaded)
											  ShowItemInformation(false, false, 0);

										  // if (StatMsg_loaded)
										  //	  return true;
										  break;
		}
		case eCTOSUIOpen_ContainerInventory:
		{
											if (m_QuickSelectionVisible)
												ShowQuickSelectionMenu(false);

											   //if (m_ChestInvVisible)
											//	   ShowChestInv(false);

											   if (m_DialogSystemVisible)
												   can_open_ui = false;

											   if (m_DialogSystemAltVisible)
												   can_open_ui = false;

											   //if (m_CharacterEquipmentVisible)
											   //	  return true;

											   // if (m_Icmenu)
											   //	  return true;

											   if (m_MainMapVisible)
												   ShowMainMap(false);

											   if (m_BookVisible)
												   can_open_ui = false;

											   if (m_CharacterCreationVisible)
												   can_open_ui = false;

											   if (m_TradeSystemVisible)
												   can_open_ui = false;

											   if (m_QuestSystemVisible)
												   can_open_ui = false;

											   if (m_SpellSelectionVisible)
												   can_open_ui = false;

											   if (m_CharacterPerksMenuVisible)
												   can_open_ui = false;

											   if (m_CharacterSkillsMenuVisible)
												   can_open_ui = false;

											   if (m_CharacterSkillInfoVisible)
												   can_open_ui = false;

											   if (m_InteractiveInfoVisible)
												   ShowInteractiveObjectInfo("", "", 0, false);

											   if (Inv_loaded)
												   can_open_ui = false;

											   if (ChrMenu_loaded)
												   can_open_ui = false;

											   // if (MainHud_loaded)
											   //	  return true;

											   if (Inv_cont_menu_loaded)
												   can_open_ui = false;

											   if (Inv_ItemInfo_loaded)
												   can_open_ui = false;

											   // if (StatMsg_loaded)
											   //	  return true;
											   break;
		}
		case eCTOSUIOpen_TraiderInventory:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

											 if (m_ChestInvVisible)
												 ShowChestInv(false);

											 if (m_DialogSystemVisible)
												 ShowDialog(false, "", 0);

											 if (m_DialogSystemAltVisible)
												 ShowDialogSystemAlt(false);

											 //if (m_CharacterEquipmentVisible)
											 //	  return true;

											 // if (m_Icmenu)
											 //	  return true;

											 if (m_MainMapVisible)
												 ShowMainMap(false);

											 if (m_BookVisible)
												 ShowBook(false, 0);

											 if (m_CharacterCreationVisible)
												 can_open_ui = false;

											 //if (m_TradeSystemVisible)
											//	 ShowTradeSystem(0, false);

											 if (m_QuestSystemVisible)
												 ShowQuestSystem(false);

											 if (m_SpellSelectionVisible)
												 ShowSpellSelectionMenu(false);

											 if (m_CharacterPerksMenuVisible)
												 ShowChPerks(false);

											 if (m_CharacterSkillsMenuVisible)
												 ShowChSkills(false);

											 if (m_CharacterSkillInfoVisible)
												 ShowChSkills(false);

											 if (m_InteractiveInfoVisible)
												 ShowInteractiveObjectInfo("", "", 0, false);

											 if (Inv_loaded)
												 ShowInventory();

											 if (ChrMenu_loaded)
												 ShowCharacterMenu();

											 // if (MainHud_loaded)
											 //	  return true;

											 if (Inv_cont_menu_loaded)
												 DisableContextMenu();

											 if (Inv_ItemInfo_loaded)
												 ShowItemInformation(false, false, 0);

											 // if (StatMsg_loaded)
											 //	  return true;
											 break;
		}
		case eCTOSUIOpen_DialogSystemHud:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

											if (m_ChestInvVisible)
												ShowChestInv(false);

											//if (m_DialogSystemVisible)
											//	can_open_ui = false;

											//if (m_DialogSystemAltVisible)
											//	can_open_ui = false;

											//if (m_CharacterEquipmentVisible)
											//	  return true;

											// if (m_Icmenu)
											//	  return true;

											if (m_MainMapVisible)
												ShowMainMap(false);

											if (m_BookVisible)
												ShowBook(false, 0);

											if (m_CharacterCreationVisible)
												can_open_ui = false;

											if (m_TradeSystemVisible)
												ShowTradeSystem(0, false);

											if (m_QuestSystemVisible)
												ShowQuestSystem(false);

											if (m_SpellSelectionVisible)
												ShowSpellSelectionMenu(false);

											if (m_CharacterPerksMenuVisible)
												ShowChPerks(false);

											if (m_CharacterSkillsMenuVisible)
												ShowChSkills(false);

											if (m_CharacterSkillInfoVisible)
												ShowChSkills(false);

											if (m_InteractiveInfoVisible)
												ShowInteractiveObjectInfo("", "", 0, false);

											if (Inv_loaded)
												ShowInventory();

											if (ChrMenu_loaded)
												ShowCharacterMenu();

											// if (MainHud_loaded)
											//	  return true;

											if (Inv_cont_menu_loaded)
												DisableContextMenu();

											if (Inv_ItemInfo_loaded)
												ShowItemInformation(false, false, 0);

											// if (StatMsg_loaded)
											//	  return true;
											break;
		}
		case eCTOSUIOpen_InventoryContextMenu:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);
		}
		case eCTOSUIOpen_MainHud:
		{

		}
		case eCTOSUIOpen_ItemInformation:
		{

		}
		case eCTOSUIOpen_StaticMessages:
		{

		}
		case eCTOSUIOpen_BookUI:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);
		}
		case eCTOSUIOpen_QuestSystemUI:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

										  if (m_ChestInvVisible)
											  ShowChestInv(false);

										  if (m_DialogSystemVisible)
											  can_open_ui = false;

										  if (m_DialogSystemAltVisible)
											  can_open_ui = false;

										  //if (m_CharacterEquipmentVisible)
										  //	  return true;

										  // if (m_Icmenu)
										  //	  return true;

										  if (m_MainMapVisible)
											  ShowMainMap(false);

										  if (m_BookVisible)
											  ShowBook(false, 0);

										  if (m_CharacterCreationVisible)
											  can_open_ui = false;

										  if (m_TradeSystemVisible)
											  can_open_ui = false;

										  //if (m_QuestSystemVisible)
										//	  ShowQuestSystem(false);

										  if (m_SpellSelectionVisible)
											  ShowSpellSelectionMenu(false);

										  if (m_CharacterPerksMenuVisible)
											  ShowChPerks(false);

										  if (m_CharacterSkillsMenuVisible)
											  ShowChSkills(false);

										  if (m_CharacterSkillInfoVisible)
											  ShowChSkills(false);

										  if (m_InteractiveInfoVisible)
											  ShowInteractiveObjectInfo("", "", 0, false);

										  if (Inv_loaded)
											  ShowInventory();

										  if (ChrMenu_loaded)
											  ShowCharacterMenu();

										  // if (MainHud_loaded)
										  //	  return true;

										  if (Inv_cont_menu_loaded)
											  DisableContextMenu();

										  if (Inv_ItemInfo_loaded)
											  ShowItemInformation(false, false, 0);

										  // if (StatMsg_loaded)
										  //	  return true;
										  break;
		}
		case eCTOSUIOpen_SpellSelectionMenu:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

											   if (m_ChestInvVisible)
												   ShowChestInv(false);

											   if (m_DialogSystemVisible)
												   can_open_ui = false;

											   if (m_DialogSystemAltVisible)
												   can_open_ui = false;

											   //if (m_CharacterEquipmentVisible)
											   //	  return true;

											   // if (m_Icmenu)
											   //	  return true;

											   if (m_MainMapVisible)
												   ShowMainMap(false);

											   if (m_BookVisible)
												   ShowBook(false, 0);

											   if (m_CharacterCreationVisible)
												   can_open_ui = false;

											   if (m_TradeSystemVisible)
												   can_open_ui = false;

											   if (m_QuestSystemVisible)
											   	  ShowQuestSystem(false);

											   //if (m_SpellSelectionVisible)
											   //   ShowSpellSelectionMenu(false);

											   if (m_CharacterPerksMenuVisible)
												   ShowChPerks(false);

											   if (m_CharacterSkillsMenuVisible)
												   ShowChSkills(false);

											   if (m_CharacterSkillInfoVisible)
												   ShowChSkills(false);

											   if (m_InteractiveInfoVisible)
												   ShowInteractiveObjectInfo("", "", 0, false);

											   if (Inv_loaded)
												   ShowInventory();

											   if (ChrMenu_loaded)
												   ShowCharacterMenu();

											   // if (MainHud_loaded)
											   //	  return true;

											   if (Inv_cont_menu_loaded)
												   DisableContextMenu();

											   if (Inv_ItemInfo_loaded)
												   ShowItemInformation(false, false, 0);

											   // if (StatMsg_loaded)
											   //	  return true;
											   break;
		}
		case eCTOSUIOpen_CharacterSkills:
		{

		}
		case eCTOSUIOpen_CharacterPerks:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

										   if (m_ChestInvVisible)
											   ShowChestInv(false);

										   if (m_DialogSystemVisible)
											   can_open_ui = false;

										   if (m_DialogSystemAltVisible)
											   can_open_ui = false;

										   //if (m_CharacterEquipmentVisible)
										   //	  return true;

										   // if (m_Icmenu)
										   //	  return true;

										   if (m_BookVisible)
											   ShowBook(false, 0);

										   if (m_CharacterCreationVisible)
											   can_open_ui = false;

										   if (m_TradeSystemVisible)
											   can_open_ui = false;

										   if (m_QuestSystemVisible)
										   	  ShowQuestSystem(false);

										   if (m_SpellSelectionVisible)
											   ShowSpellSelectionMenu(false);

										   if (m_MainMapVisible)
											   ShowMainMap(false);

										   //if (m_CharacterPerksMenuVisible)
											//   ShowChPerks(false);

										   if (m_CharacterSkillsMenuVisible)
											   ShowChSkills(false);

										   if (m_CharacterSkillInfoVisible)
											   ShowChSkills(false);

										   if (m_InteractiveInfoVisible)
											   ShowInteractiveObjectInfo("", "", 0, false);

										   if (Inv_loaded)
											   ShowInventory();

										   if (ChrMenu_loaded)
											   ShowCharacterMenu();

										   // if (MainHud_loaded)
										   //	  return true;

										   if (Inv_cont_menu_loaded)
											   DisableContextMenu();

										   if (Inv_ItemInfo_loaded)
											   ShowItemInformation(false, false, 0);

										   // if (StatMsg_loaded)
										   //	  return true;
										   break;
		}
		case eCTOSUIOpen_InteractiveObjectInfo:
		{
												  if (m_ChestInvVisible)
													  can_open_ui = false;

												  if (m_DialogSystemVisible)
													  can_open_ui = false;

												  if (m_DialogSystemAltVisible)
													  can_open_ui = false;

												  //if (m_CharacterEquipmentVisible)
												  //	  return true;

												  // if (m_Icmenu)
												  //	  return true;

												  if (m_BookVisible)
													  can_open_ui = false;

												  if (m_CharacterCreationVisible)
													  can_open_ui = false;

												  if (m_TradeSystemVisible)
													  can_open_ui = false;

												  if (m_QuestSystemVisible)
													  can_open_ui = false;

												  if (m_SpellSelectionVisible)
													  can_open_ui = false;

												  if (m_MainMapVisible)
													  can_open_ui = false;

												  //if (m_CharacterPerksMenuVisible)
												  //   ShowChPerks(false);

												  if (m_CharacterSkillsMenuVisible)
													  can_open_ui = false;

												  if (m_CharacterSkillInfoVisible)
													  can_open_ui = false;

												  //if (m_InteractiveInfoVisible)
												//	  ShowInteractiveObjectInfo("", "", 0, false);

												  if (Inv_loaded)
													  can_open_ui = false;

												  if (ChrMenu_loaded)
													  can_open_ui = false;

												  // if (MainHud_loaded)
												  //	  return true;

												  if (Inv_cont_menu_loaded)
													  can_open_ui = false;

												  //if (Inv_ItemInfo_loaded)
												//	  can_open_ui = false;

												  // if (StatMsg_loaded)
												  //	  return true;
												  break;
		}
		case eCTOSUIOpen_CharacterCreatingMenu:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

			if (m_ChestInvVisible)
				ShowChestInv(false);

			if (m_DialogSystemVisible)
				can_open_ui = false;

			if (m_DialogSystemAltVisible)
				can_open_ui = false;

			if (m_MainMapVisible)
				ShowMainMap(false);

			if (m_BookVisible)
				ShowBook(false, 0);

			if (m_TradeSystemVisible)
				ShowTradeSystem(0, false);

			if (m_QuestSystemVisible)
				ShowQuestSystem(false);

			if (m_SpellSelectionVisible)
				ShowSpellSelectionMenu(false);

			if (m_CharacterPerksMenuVisible)
				ShowChPerks(false);

			if (m_CharacterSkillsMenuVisible)
				ShowChSkills(false);

			if (m_CharacterSkillInfoVisible)
				ShowChSkills(false);

			if (m_InteractiveInfoVisible)
				ShowInteractiveObjectInfo("", "", 0, false);

			if (Inv_loaded)
				ShowInventory();

			if (ChrMenu_loaded)
				ShowCharacterMenu();

			if (Inv_cont_menu_loaded)
				DisableContextMenu();

			if (Inv_ItemInfo_loaded)
				ShowItemInformation(false, false, 0);

			break;
		}
		case eCTOSUIOpen_QuickSelection:
		{
			if (m_BookVisible || m_ChestInvVisible || m_DialogSystemAltVisible ||
				m_DialogSystemVisible || m_CharacterCreationVisible || m_QuestSystemVisible || 
				m_TradeSystemVisible || ChrMenu_loaded || Inv_cont_menu_loaded || m_MainMapVisible)
				can_open_ui = false;

			break;
		}
		case eCTOSUIOpen_MainMap:
		{
			if (m_QuickSelectionVisible)
				ShowQuickSelectionMenu(false);

									if (m_ChestInvVisible)
										ShowChestInv(false);

									if (m_DialogSystemVisible)
										can_open_ui = false;

									if (m_DialogSystemAltVisible)
										can_open_ui = false;

									//if (m_CharacterEquipmentVisible)
									//	  return true;

									// if (m_Icmenu)
									//	  return true;

									//if (m_MainMapVisible)
										//ShowMainMap(false);

									if (m_BookVisible)
										ShowBook(false, 0);

									if (m_CharacterCreationVisible)
										can_open_ui = false;

									if (m_TradeSystemVisible)
										can_open_ui = false;

									if (m_QuestSystemVisible)
									    ShowQuestSystem(false);

									if (m_SpellSelectionVisible)
										ShowSpellSelectionMenu(false);

									if (m_CharacterPerksMenuVisible)
										ShowChPerks(false);

									if (m_CharacterSkillsMenuVisible)
										ShowChSkills(false);

									if (m_CharacterSkillInfoVisible)
										ShowChSkills(false);

									if (m_InteractiveInfoVisible)
										ShowInteractiveObjectInfo("", "", 0, false);

									if (Inv_loaded)
										ShowInventory();

									if (ChrMenu_loaded)
										ShowCharacterMenu();

									// if (MainHud_loaded)
									//	  return true;

									if (Inv_cont_menu_loaded)
										DisableContextMenu();

									if (Inv_ItemInfo_loaded)
										ShowItemInformation(false, false, 0);

									// if (StatMsg_loaded)
									//	  return true;
												  break;
		}
			break;
	}

	if (m_PostInitScreenVisible)
		can_open_ui = false;

	return can_open_ui;
}

void CHUDCommon::CreateFlashPlayerInstancesForUIElements()
{
	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	if (pShopInterface)
	{
		pShopInterface->GetFlashPlayer();
	}
	IUIElement* pPlayerQuestInfo = gEnv->pFlashUI->GetUIElement("PlayerQuestInfo");
	if (pPlayerQuestInfo)
	{
		pPlayerQuestInfo->GetFlashPlayer();
	}
	IUIElement* pInteractiveInfo = gEnv->pFlashUI->GetUIElement("InteractiveInfo");
	if (pInteractiveInfo)
	{
		pInteractiveInfo->GetFlashPlayer();
	}
	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	if (pLootUI)
	{
		pLootUI->GetFlashPlayer();
	}
	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	if (pPlayerPerksMenu)
	{
		pPlayerPerksMenu->GetFlashPlayer();
	}
	IUIElement* pInteractiveBookUI = gEnv->pFlashUI->GetUIElement("InteractiveBookUI");
	if (pInteractiveBookUI)
	{
		pInteractiveBookUI->GetFlashPlayer();
	}
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	if (pDialogAltUi)
	{
		pDialogAltUi->GetFlashPlayer();
	}
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	if (pDialogUi)
	{
		pDialogUi->GetFlashPlayer();
	}
	IUIElement* pItemInfo = gEnv->pFlashUI->GetUIElement("ItemInformation");
	if (pItemInfo)
	{
		pItemInfo->GetFlashPlayer();
	}
	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	if (pMain_hud)
	{
		pMain_hud->GetFlashPlayer();
	}
	IUIElement* pStaticMessages_hud = gEnv->pFlashUI->GetUIElement("StaticMessages");
	if (pStaticMessages_hud)
	{
		pStaticMessages_hud->GetFlashPlayer();
	}
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	if (pBuffsScreenInfo)
	{
		pBuffsScreenInfo->GetFlashPlayer();
	}
	IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
	if (pChrMenu_hud)
	{
		pChrMenu_hud->GetFlashPlayer();
	}
	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	if (pElement)
	{
		pElement->GetFlashPlayer();
	}
	IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
	if (pInvContextMenu)
	{
		pInvContextMenu->GetFlashPlayer();
	}
	IUIElement* pHintsUI = gEnv->pFlashUI->GetUIElement("InGameHints");
	if (pHintsUI)
	{
		pHintsUI->GetFlashPlayer();
	}
	IUIElement* pCharacter_creating_UI = gEnv->pFlashUI->GetUIElement("Character_creating_UI");
	if (pCharacter_creating_UI)
	{
		pCharacter_creating_UI->GetFlashPlayer();
	}
	IUIElement* pOnScreenObjInfo = gEnv->pFlashUI->GetUIElement("OnScreenObjInfo");
	if (pOnScreenObjInfo)
	{
		pOnScreenObjInfo->GetFlashPlayer();
	}
}

EntityId CHUDCommon::GetPlayerItemByPrimHudPosInINV(int id)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return 0;

	IInventory *pInventory = pActor->GetInventory();
	if (!pInventory)
		return 0;

	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId ide = pInventory->GetItem(i);
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItem)
		{
			if (pItem->GetHudItemPosPrimary() == id && !pItem->IsItemIgnoredByHud())
				return ide;
		}
	}

	return 0;
}

EntityId CHUDCommon::GetPlayerItemBySecondHudPosInINV(int id)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return 0;

	IInventory *pInventory = pActor->GetInventory();
	if (!pInventory)
		return 0;

	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId ide = pInventory->GetItem(i);
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItem)
		{
			if (pItem->GetHudItemPosSecondary() == id && !pItem->IsItemIgnoredByHud())
				return ide;
		}
	}

	return 0;
}

void CHUDCommon::AddMapMarker(int id, string icon, string name, Vec2 pos, bool updateMapUi)
{
	DeleteMapMarker(id, updateMapUi);
	SMapMarker marker;
	marker.marker_id = id;
	marker.marker_icon = icon;
	marker.marker_name = name;
	marker.marker_pos = pos;
	s_MapStaticMarkers.push_back(marker);
}

void CHUDCommon::DeleteMapMarker(int id, bool updateMapUi)
{
	std::vector<SMapMarker>::iterator it = s_MapStaticMarkers.begin();
	std::vector<SMapMarker>::iterator end = s_MapStaticMarkers.end();
	int count = s_MapStaticMarkers.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		SMapMarker marker = *it;
		if (marker.marker_id == id)
		{
			s_MapStaticMarkers.erase(it);
			s_MapStaticMarkers.resize(count-1);
			return;
		}
	}
}

void CHUDCommon::ClearAllMapMarkers(bool updateMapUi)
{
	s_MapStaticMarkers.clear();
	s_MapStaticMarkers.resize(0);
}

void CHUDCommon::CreateUIActionFilter(int uiType, bool enable)
{
	if (uiType == eHUIAFT_interactive)
	{
		g_pGame->Actions().FilterNoMouse()->Enable(enable);
		g_pGame->Actions().FilterNoMove()->Enable(enable);
		g_pGame->Actions().FilterNvcActions()->Enable(enable);
		//g_pGame->Actions().FilterUseKeyOnly()->Enable(enable);
	}
	else if (uiType == eHUIAFT_dialog)
	{
		g_pGame->Actions().FilterNoMouse()->Enable(enable);
		g_pGame->Actions().FilterNoMove()->Enable(enable);
		g_pGame->Actions().FilterNvcActions()->Enable(enable);
		//g_pGame->Actions().FilterUseKeyOnly()->Enable(enable);
	}
}

void CHUDCommon::OnInterInUIMode(int uiType, bool enable)
{
	IUIElement* pShopInterface = gEnv->pFlashUI->GetUIElement("ShopInterface");
	IUIElement* pPlayerQuestInfo = gEnv->pFlashUI->GetUIElement("PlayerQuestInfo");
	IUIElement* pInteractiveInfo = gEnv->pFlashUI->GetUIElement("InteractiveInfo");
	IUIElement* pLootUI = gEnv->pFlashUI->GetUIElement("LootPickUpMenu");
	IUIElement* pPlayerPerksMenu = gEnv->pFlashUI->GetUIElement("PerksMenuUI");
	IUIElement* pInteractiveBookUI = gEnv->pFlashUI->GetUIElement("InteractiveBookUI");
	IUIElement* pDialogAltUi = gEnv->pFlashUI->GetUIElement("DialogUI_Alt");
	IUIElement* pDialogUi = gEnv->pFlashUI->GetUIElement("DialogUI_Normal");
	IUIElement* pItemInfo = gEnv->pFlashUI->GetUIElement("ItemInformation");
	IUIElement* pMain_hud = gEnv->pFlashUI->GetUIElement("Main_Hud");
	IUIElement* pStaticMessages_hud = gEnv->pFlashUI->GetUIElement("StaticMessages");
	IUIElement* pBuffsScreenInfo = gEnv->pFlashUI->GetUIElement("BuffsScreenInfo");
	IUIElement* pChrMenu_hud = gEnv->pFlashUI->GetUIElement("CharacterMenu");
	IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
	IUIElement* pInvContextMenu = gEnv->pFlashUI->GetUIElement("InvContextMenu");
	IUIElement* pSpellSelectionMenu = gEnv->pFlashUI->GetUIElement("SpellSelectionUI");

	I3DEngine* p3DEngine = gEnv->p3DEngine;
	if (!p3DEngine)
		return;

	if (uiType == eCTOSUIOpen_QuickSelection)
	{
		if (enable)
		{
			if (Inv_loaded && pElement)
			{
				pElement->GetFlashPlayer()->Pause(true);
				pElement->GetFlashPlayer()->SetFSCommandHandler(NULL);
			}
			else if (m_SpellSelectionVisible && pSpellSelectionMenu)
			{
				pSpellSelectionMenu->GetFlashPlayer()->Pause(true);
				pSpellSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(NULL);
			}

			if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
			{
				if (MainHud_loaded)
				{
					ShowMainHud();
				}
			}

			if (g_pGameCVars->hud_enable_slowmotion_on_quickselection_menu_open > 0)
			{
				if (!AnyInteractiveUIOpened())
				{
					if (gEnv->pConsole->GetCVar("t_scale"))
					{
						old_tscale = gEnv->pConsole->GetCVar("t_scale")->GetFVal();
					}
					float scale = 1.0f - g_pGameCVars->hud_slowmotion_scale_on_quickselection_menu_open;
					if (scale < 0.0f)
						scale = 0.0f;
					string con_command = "";
					con_command.Format("t_scale = %f", scale);
					gEnv->pConsole->ExecuteString(con_command.c_str(), true);
				}
			}
			return;
		}
		else if (!enable)
		{
			if (Inv_loaded && pElement)
			{
				pElement->GetFlashPlayer()->Pause(false);
				pElement->GetFlashPlayer()->SetFSCommandHandler(this);
			}
			else if (m_SpellSelectionVisible && pSpellSelectionMenu)
			{
				pSpellSelectionMenu->GetFlashPlayer()->Pause(false);
				pSpellSelectionMenu->GetFlashPlayer()->SetFSCommandHandler(this);
			}

			if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
			{
				if (!MainHud_loaded)
				{
					ShowMainHud();
				}
			}

			if (g_pGameCVars->hud_enable_slowmotion_on_quickselection_menu_open > 0)
			{
				float scale = old_tscale;
				string con_command = "";
				con_command.Format("t_scale = %f", scale);
				gEnv->pConsole->ExecuteString(con_command.c_str(), true);
			}
			return;
		}
	}

	if (m_QuickSelectionVisible && !enable)
	{
		ShowQuickSelectionMenu(false);
	}

	if (uiType == eCTOSUIOpen_CharacterCreatingMenu && enable)
	{
		if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
		{
			if (MainHud_loaded)
			{
				ShowMainHud();
			}
		}

		if (g_pGameCVars->hud_enable_slowmotion_on_interactive_menu_open > 0)
		{
			if (gEnv->pConsole->GetCVar("t_scale"))
			{
				old_tscale = gEnv->pConsole->GetCVar("t_scale")->GetFVal();
			}
		}
		return;
	}

	if (uiType != eCTOSUIOpen_DialogSystemHud && enable)
	{
		//Get current DOF params
		p3DEngine->GetPostEffectParam("Dof_User_Active", Effects_vals_DOF[0]);
		p3DEngine->GetPostEffectParam("Dof_User_FocusDistance", Effects_vals_DOF[1]);
		p3DEngine->GetPostEffectParam("Dof_User_FocusRange", Effects_vals_DOF[2]);
		p3DEngine->GetPostEffectParam("Dof_User_BlurAmount", Effects_vals_DOF[3]);
		//Apply new DOF params.
		p3DEngine->SetPostEffectParam("Dof_User_Active", 1.0f, true);
		p3DEngine->SetPostEffectParam("Dof_User_FocusDistance", 0.1f, true);
		p3DEngine->SetPostEffectParam("Dof_User_FocusRange", 0.1f, true);
		p3DEngine->SetPostEffectParam("Dof_User_BlurAmount", 12.0f, true);
		//double setup because bugged "SetPostEffectParam"
		p3DEngine->SetPostEffectParam("Dof_User_Active", 1.0f, true);
		p3DEngine->SetPostEffectParam("Dof_User_FocusDistance", 0.1f, true);
		p3DEngine->SetPostEffectParam("Dof_User_FocusRange", 0.1f, true);
		p3DEngine->SetPostEffectParam("Dof_User_BlurAmount", 12.0f, true);
		if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
		{
			if (MainHud_loaded)
			{
				ShowMainHud();
			}
		}

		if (g_pGameCVars->hud_enable_slowmotion_on_interactive_menu_open > 0)
		{
			if (gEnv->pConsole->GetCVar("t_scale"))
			{
				old_tscale = gEnv->pConsole->GetCVar("t_scale")->GetFVal();
			}
			float scale = 1.0f - g_pGameCVars->hud_slowmotion_scale_on_interactive_menu_open;
			if (scale < 0.0f)
				scale = 0.0f;
			string con_command = "";
			con_command.Format("t_scale = %f", scale);
			gEnv->pConsole->ExecuteString(con_command.c_str(), true);
		}
	}
	else if(uiType != eCTOSUIOpen_DialogSystemHud && !enable)
	{
		//Restore old DOF params
		p3DEngine->SetPostEffectParam("Dof_User_FocusDistance", Effects_vals_DOF[1], true);
		p3DEngine->SetPostEffectParam("Dof_User_FocusRange", Effects_vals_DOF[2], true);
		p3DEngine->SetPostEffectParam("Dof_User_BlurAmount", Effects_vals_DOF[3], true);
		p3DEngine->SetPostEffectParam("Dof_User_Active", Effects_vals_DOF[0], true);
		if (g_pGameCVars->g_disable_all_ui_if_interactive_ui_loaded > 0)
		{
			if (!MainHud_loaded)
			{
				ShowMainHud();
			}
		}

		if (g_pGameCVars->hud_enable_slowmotion_on_interactive_menu_open > 0)
		{
			float scale = old_tscale;
			string con_command = "";
			con_command.Format("t_scale = %f", scale);
			gEnv->pConsole->ExecuteString(con_command.c_str(), true);
		}
	}
}

bool CHUDCommon::AnyInteractiveUIOpened()
{
	if (ChrMenu_loaded || Character_Creation_Menu_Loaded || Inv_loaded || m_SpellSelectionVisible ||
		m_DialogSystemAltVisible || m_DialogSystemVisible || m_CharacterCreationVisible ||
		m_BookVisible || m_QuestSystemVisible || m_TradeSystemVisible ||
		m_CharacterPerksMenuVisible || m_CharacterSkillsMenuVisible ||
		m_ChestInvVisible || m_MainMapVisible)
		return true;
	else
		return false;
}

int CHUDCommon::GetItemInvSlotIdFromEquippedVisualSlot(const char * slot)
{
	bool prim_inv_items_pos = false;
	if (g_pGameCVars->g_player_inv_sel_tab_id == 3)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 2)
		prim_inv_items_pos = true;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 1)
		prim_inv_items_pos = false;
	else if (g_pGameCVars->g_player_inv_sel_tab_id == 0)
		prim_inv_items_pos = false;

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return -1;

	if (!stricmp(slot, equipment_slots_names[eEVS_bodyLower]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Pants)));
		if (pItem)
		{
			//CryLogAlways("pItem eEVS_bodyLower eAESlot_Pants");
			if (pItem->GetOwnerActor() == pActor)
			{
				//CryLogAlways("pItem eEVS_bodyLower eAESlot_Pants 2");
				if (pItem->GetEquipped() > 0)
				{
					//CryLogAlways("pItem eEVS_bodyLower eAESlot_Pants 3");
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_bodyUpper]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_foots]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Boots)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_hands]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Arms)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_headHelm]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Helmet)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_leftWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetAnyItemInLeftHand()));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament01]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_1)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament02]))
	{
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament03]))
	{
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament04]))
	{
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament05]))
	{
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament06]))
	{
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament07]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_2)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_rightWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Weapon)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					if (prim_inv_items_pos)
						return pItem->GetHudItemPosPrimary();
					else
						return pItem->GetHudItemPosSecondary();
				}
			}
		}
		return -1;
	}

	return -1;
}

bool CHUDCommon::IsEquippedSlotBusy(const char * slot)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return false;

	if (!stricmp(slot, equipment_slots_names[eEVS_bodyLower]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Pants)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_bodyUpper]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_foots]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Boots)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_hands]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Arms)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_headHelm]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Helmet)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_leftWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetAnyItemInLeftHand()));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament01]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_1)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament02]))
	{
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament03]))
	{
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament04]))
	{
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament05]))
	{
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament06]))
	{
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament07]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_2)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_rightWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Weapon)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	return false;
}

EntityId CHUDCommon::GetItemIdFromEquippedSlot(const char * slot)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return 0;

	if (!stricmp(slot, equipment_slots_names[eEVS_bodyLower]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Pants)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_bodyUpper]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_foots]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Boots)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_hands]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Arms)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_headHelm]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Helmet)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_leftWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetAnyItemInLeftHand()));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament01]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_1)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament02]))
	{
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament03]))
	{
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament04]))
	{
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament05]))
	{
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament06]))
	{
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_ornament07]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_2)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	else if (!stricmp(slot, equipment_slots_names[eEVS_rightWeapon]))
	{
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Weapon)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					return pItem->GetEntityId();
				}
			}
		}
		return 0;
	}
	return 0;
}

void CHUDCommon::UpdateEquippedItemsInInvWindow()
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	IUIElement* pElementInventory = gEnv->pFlashUI->GetUIElement("Inv");

	if (!pElementInventory)
		return;

	for (int i = 0; i < eEVS_AllSlots; i++)
	{
		SFlashVarValue args[5] = { equipment_slots_names[i], "", 0, 0, 0 };
		pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Weapon)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				if (pActor->GetNoWeaponId() != pItem->GetEntityId())
				{
					string icon = pItem->GetIcon();
					int stat = pItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					int stt = pItem->GetEquipped();
					SFlashVarValue args[5] = { equipment_slots_names[eEVS_rightWeapon], icon.c_str(), stat, idIndex, stt };
					pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
					pItem = NULL;
				}
				else
				{
					pItem = NULL;
				}
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Boots)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_foots], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Arms)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_hands], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_1)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_ornament01], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso_Decor_2)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_ornament07], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Helmet)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_headHelm], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torso)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_bodyUpper], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Pants)));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_bodyLower], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}

	pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetAnyItemInLeftHand()));
	if (pItem)
	{
		if (pItem->GetOwnerActor() == pActor)
		{
			if (pItem->GetEquipped() > 0)
			{
				string icon = pItem->GetIcon();
				int stat = pItem->GetItemQuanity();
				EntityId idIndex = pItem->GetEntityId();
				int stt = pItem->GetEquipped();
				SFlashVarValue args[5] = { equipment_slots_names[eEVS_leftWeapon], icon.c_str(), stat, idIndex, stt };
				pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
				pItem = NULL;
			}
		}
	}
	else
	{
		pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));
		if (pItem)
		{
			if (pItem->GetOwnerActor() == pActor)
			{
				if (pItem->GetEquipped() > 0)
				{
					string icon = pItem->GetIcon();
					int stat = pItem->GetItemQuanity();
					EntityId idIndex = pItem->GetEntityId();
					int stt = pItem->GetEquipped();
					SFlashVarValue args[5] = { equipment_slots_names[eEVS_leftWeapon], icon.c_str(), stat, idIndex, stt };
					pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
					pItem = NULL;
				}
			}
		}
		else
		{
			pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
			if (pItem)
			{
				if (pItem->GetOwnerActor() == pActor)
				{
					if (pItem->GetEquipped() > 0)
					{
						string icon = pItem->GetIcon();
						int stat = pItem->GetItemQuanity();
						EntityId idIndex = pItem->GetEntityId();
						int stt = pItem->GetEquipped();
						SFlashVarValue args[5] = { equipment_slots_names[eEVS_leftWeapon], icon.c_str(), stat, idIndex, stt };
						pElementInventory->GetFlashPlayer()->Invoke("addEquippedItem", args, 5);
						pItem = NULL;
					}
				}
			}
		}
	}
}

void CHUDCommon::StartDtlDisableTimer(bool book_autoenable)
{
	if (!book_autoenable)
	{
		if (Inv_loaded)
		{
			if (!m_TimerInventoryDtl_disable)
				m_TimerInventoryDtl_disable = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::DisablingContextMenu), NULL);
		}
		return;
	}

	if (Inv_loaded)
	{
		if (!m_TimerInventoryDtl_disable)
		{
			CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
			if (pGameGlobals)
			{
				if(temportary_book_id == 0)
					temportary_book_id = pGameGlobals->current_selected_Item;
			}
			m_TimerInventoryDtl_disable = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::DtlBook), NULL);
		}
	}
}

void CHUDCommon::DtlBook(void * pUserData, IGameFramework::TimerID handler)
{

	if (Inv_cont_menu_loaded)
	{
		ShowInvContextMenu(false);
	}

	if (Inv_loaded)
	{
		ShowInventory();
		ShowBook(true, temportary_book_id);
	}
	else
	{
		ShowBook(true, temportary_book_id);
	}

	if (m_TimerInventoryDtl_disable)
	{
		temportary_book_id = 0;
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerInventoryDtl_disable);
		m_TimerInventoryDtl_disable = NULL;
	}
}

void CHUDCommon::DisablingContextMenu(void * pUserData, IGameFramework::TimerID handler)
{
	timer_to_can_item_press += 0.1f;
	if (Inv_cont_menu_loaded)
	{
		ShowInvContextMenu(false);
	}

	if (Inv_loaded)
	{
		IUIElement* pElement = gEnv->pFlashUI->GetUIElement("Inv");
		if (pElement)
		{
			CreateUIActionFilter(eHUIAFT_interactive, true);
			//OnInterInUIMode(eCTOSUIOpen_Inventory, true);
			pElement->GetFlashPlayer()->SetVisible(true);
			pElement->SetVisible(true);
			pElement->GetFlashPlayer()->Pause(false);
			pElement->GetFlashPlayer()->SetFSCommandHandler(this);
			UpdateInventory(1.0f);
		}
		//ShowInventory();
		//ShowInventory();
	}

	if (m_TimerInventoryDtl_disable)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerInventoryDtl_disable);
		m_TimerInventoryDtl_disable = NULL;
	}
}

void CHUDCommon::StartDSChestInvTimer()
{
	if (m_ChestInvVisible)
	{
		if (!m_TimerContainerUI_disable)
			m_TimerContainerUI_disable = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::DisablingChestInv), NULL);
	}
}

void CHUDCommon::DisablingChestInv(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_ChestInvVisible)
	{
		ShowChestInv(false);
	}

	if (m_TimerContainerUI_disable)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerContainerUI_disable);
		m_TimerContainerUI_disable = NULL;
	}
}

void CHUDCommon::StartCurrentInteractiveUIDisablingTimer()
{
	if (AnyInteractiveUIOpened())
	{
		if (!m_TimerCurrentInteractiveUI_disable)
			m_TimerCurrentInteractiveUI_disable = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::DisablingCurrentInteractiveUI), NULL);
	}
}

void CHUDCommon::DisablingCurrentInteractiveUI(void * pUserData, IGameFramework::TimerID handler)
{
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (nwAction)
	{
		nwAction->CloseAnyInteractiveUIOpened();
	}

	if (m_CharacterCreationVisible)
	{
		ShowCharacterCreatingMenu();
	}

	if (m_TimerCurrentInteractiveUI_disable)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerCurrentInteractiveUI_disable);
		m_TimerCurrentInteractiveUI_disable = NULL;
	}
}

void CHUDCommon::StartGoToMainMenuTimer()
{
	if (m_CharacterCreationVisible)
	{
		if (!m_TimerGoToMainMenu)
			m_TimerGoToMainMenu = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.01f), false, functor(*this, &CHUDCommon::GoToMainMenu), NULL);
	}
}

void CHUDCommon::GoToMainMenu(void * pUserData, IGameFramework::TimerID handler)
{
	if (disconnect_requested == 0)
	{
		//if (m_CharacterCreationVisible)
		ShowCharacterCreatingMenu();
		m_CharacterCreationVisible = false;
		disconnect_requested = 1;
		if (m_TimerGoToMainMenu)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerGoToMainMenu);
			m_TimerGoToMainMenu = NULL;
		}

		if (!m_TimerGoToMainMenu)
			m_TimerGoToMainMenu = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::GoToMainMenu), NULL);
	}
	else if (disconnect_requested == 1)
	{
		if (m_TimerGoToMainMenu)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerGoToMainMenu);
			m_TimerGoToMainMenu = NULL;
		}
		disconnect_requested = 0;
		gEnv->pConsole->ExecuteString("disconnect", true, true);
	}
}

void CHUDCommon::StartLoadingMinimapSectors()
{
	if (m_MinimapVisible)
	{
		if (!m_TimerLoadingMiniMap)
		{
			m_TimerLoadingMiniMap = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::LoadNextMinimapSector), NULL);
		}
	}
}

void CHUDCommon::StartLoadingMapSectors()
{
	if (m_MainMapVisible)
	{
		if (!m_TimerLoadingMap)
		{
			m_TimerLoadingMap = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::LoadNextMapSector), NULL);
		}
	}
}

void CHUDCommon::LoadNextMinimapSector(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_MinimapVisible)
	{
		if (m_TimerLoadingMiniMap)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingMiniMap);
			m_TimerLoadingMiniMap = NULL;
		}

		if (s_minimap_sectors_pts.size() <= num_loaded_mm_sectors)
			return;

		IUIElement* pMinimap = gEnv->pFlashUI->GetUIElement("MinimapUI");
		if (pMinimap)
		{
			std::list<string>::iterator it = s_minimap_sectors_pts.begin();
			std::list<string>::iterator end = s_minimap_sectors_pts.end();
			for (int i = 0; i < s_minimap_sectors_pts.size(); i++, it++)
			{
				if (i == num_loaded_mm_sectors)
				{
					string tsts_pth = (*it);
					SFlashVarValue args[2] = { i, tsts_pth.c_str() };
					pMinimap->GetFlashPlayer()->Invoke("setSectorImg", args, 2);
					num_loaded_mm_sectors += 1;
					break;
				}
			}

			if (num_loaded_mm_sectors != s_minimap_sectors_pts.size())
			{
				if (!m_TimerLoadingMiniMap)
				{
					m_TimerLoadingMiniMap = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::LoadNextMinimapSector), NULL);
				}
			}
		}
	}
}

void CHUDCommon::LoadNextMapSector(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_MainMapVisible)
	{
		if (m_TimerLoadingMap)
		{
			gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingMap);
			m_TimerLoadingMap = NULL;
		}

		if (s_map_sectors_pts.size() <= num_loaded_m_sectors)
			return;

		IUIElement* pMainMap = gEnv->pFlashUI->GetUIElement("MainMapUI");
		if (pMainMap)
		{
			std::list<string>::iterator it = s_map_sectors_pts.begin();
			std::list<string>::iterator end = s_map_sectors_pts.end();
			for (int i = 0; i < s_map_sectors_pts.size(); i++, it++)
			{
				if (i == num_loaded_m_sectors)
				{
					string nm = "";
					if (1 < 2)
					{
						nm.Format("A_%i", i);
					}
					else if (1 > 2)
					{
						nm.Format("B_%i", i);
					}
					string tsts_pth = (*it);
					SFlashVarValue args[2] = { nm.c_str(), tsts_pth.c_str() };
					pMainMap->GetFlashPlayer()->Invoke("setSectorImg", args, 2);
					num_loaded_m_sectors += 1;
					break;
				}
			}

			if (num_loaded_m_sectors != s_map_sectors_pts.size())
			{
				if (!m_TimerLoadingMap)
				{
					m_TimerLoadingMap = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::LoadNextMapSector), NULL);
				}
			}
		}
	}
}

void CHUDCommon::StartLoadingAblUI()
{
	if (!m_TimerLoadingAblUi)
	{
		m_TimerLoadingAblUi = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(0.1f), false, functor(*this, &CHUDCommon::LoadAblUI), NULL);
	}
}

void CHUDCommon::LoadAblUI(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_TimerLoadingAblUi)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerLoadingAblUi);
		m_TimerLoadingAblUi = NULL;
	}

	ShowChPerks(true);
}

void CHUDCommon::DisablingCurrentDialogUI(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_TimerDialogUIDisablingBB)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerDialogUIDisablingBB);
		m_TimerDialogUIDisablingBB = NULL;
	}

	if (m_DialogSystemVisible)
	{
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (pGameGlobals)
		{
			pGameGlobals->SetDialogMainNpc(0);
		}
		ShowDialog(false, "", 0);
	}
	else if (m_DialogSystemAltVisible)
	{
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (pGameGlobals)
		{
			pGameGlobals->SetDialogMainNpc(0);
		}
		ShowDialogSystemAlt(false);
	}
}

void CHUDCommon::EnInventory(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_TimerEnInv)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_TimerEnInv);
		m_TimerEnInv = NULL;
	}

	if (!Inv_loaded)
	{
		ShowInventory();
	}
}

void CHUDCommon::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}
