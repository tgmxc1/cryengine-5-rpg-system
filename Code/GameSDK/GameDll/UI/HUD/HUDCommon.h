/*************************************************************************
Player Interactive Interface Header
*************************************************************************/


#ifndef __HUDCOMMON_H__
#define __HUDCOMMON_H__

//-----------------------------------------------------------------------------------------------------
#include <list>
#include "IGameObject.h"
#include <CrySystem/Scaleform/IFlashPlayer.h>
#include <CrySystem/Scaleform/IFlashUI.h>
#include <CryInput/IHardwareMouse.h>
#include "StdAfx.h"
#include "UI/Menu3dModels/MenuRender3DModelMgr.h"
//#include "IMeshBaking.h"


class CHUDCommon : public IFSCommandHandler, public IHardwareMouseEventListener
{
	
public:
	// IFSCommandHandler
	virtual void HandleFSCommand(const char *strCommand,const char *strArgs, void* pUserData);
	// ~IFSCommandHandler
	// IHardwareMouseEventListener
	virtual void OnHardwareMouseEvent(int iX,int iY,EHARDWAREMOUSEEVENT eHardwareMouseEvent, int wheelDelta);
	// ~IHardwareMouseEventListener
	static void HUD					(ICVar *pVar);
	CHUDCommon();
	virtual	~	CHUDCommon();
	virtual void Update();
	virtual void PostUpdate();
	virtual void Reset();
	void Serialize(TSerialize ser);
	void UpdateInventory(float frameTime);
	ILINE void TimedUpdateInventory(void* pUserData, IGameFramework::TimerID handler) { UpdateInventory(1.0f); };
	void ClearInventory(bool reload_element = false);
	bool InventoryUsePrimaryItemPos();
	// Cursor handling
	void CursorIncrementCounter();
	void CursorDecrementCounter();

	void DragAndDropFSRealization(const char *strCommand, const char *strArgs);
	void AddPlayerDollToScreen(bool add, bool delete_mrg3d = true);

	void DebugFSRealization(const char *strCommand, const char *strArgs);

	void PlaySoundUIFSRealization(const char *strCommand, const char *strArgs);
	void MapUIFSRealization(const char *strCommand, const char *strArgs);
	void InventoryUIFSRealization(const char *strCommand, const char *strArgs);
	void ChrMenuUIFSRealization(const char *strCommand, const char *strArgs);
	void CreationMenuUIFSRealization(const char *strCommand, const char *strArgs);
	void SpellsMenuUIFSRealization(const char *strCommand, const char *strArgs);
	void DialogsUIFSRealization(const char *strCommand, const char *strArgs);
	void TradeUIFSRealization(const char *strCommand, const char *strArgs);
	void QuestUIFSRealization(const char *strCommand, const char *strArgs);
	void MhudUIFSRealization(const char *strCommand, const char *strArgs);
	void CommonUIFSRealization(const char *strCommand, const char *strArgs);

	enum ECallToOpenSystemUI
	{
		eCTOSUIOpen_none = 0,
		eCTOSUIOpen_Inventory,
		eCTOSUIOpen_CharacterMenu,
		eCTOSUIOpen_ContainerInventory,
		eCTOSUIOpen_TraiderInventory,
		eCTOSUIOpen_DialogSystemHud,
		eCTOSUIOpen_InventoryContextMenu,
		eCTOSUIOpen_MainHud,
		eCTOSUIOpen_ItemInformation,
		eCTOSUIOpen_StaticMessages,
		eCTOSUIOpen_BookUI,
		eCTOSUIOpen_QuestSystemUI,
		eCTOSUIOpen_SpellSelectionMenu,
		eCTOSUIOpen_CharacterSkills,
		eCTOSUIOpen_CharacterPerks,
		eCTOSUIOpen_InteractiveObjectInfo,
		eCTOSUIOpen_CharacterCreatingMenu,
		eCTOSUIOpen_MainMap,
		eCTOSUIOpen_QuickSelection
	};

	enum EHudEntityType
	{
		eHEType_none = 0,
		eHEType_item,
		eHEType_character,
		eHEType_brush,
		eHEType_standart
	};

	enum EHudUIActionFilterType
	{
		eHUIAFT_none = 0,
		eHUIAFT_interactive,
		eHUIAFT_dialog
	};

	enum EEquipmentVisualSlots
	{
		eEVS_bodyLower = 0,
		eEVS_bodyUpper,
		eEVS_foots,
		eEVS_hands,
		eEVS_headHelm,
		eEVS_leftWeapon,
		eEVS_ornament01,
		eEVS_ornament02,
		eEVS_ornament03,
		eEVS_ornament04,
		eEVS_ornament05,
		eEVS_ornament06,
		eEVS_ornament07,
		eEVS_rightWeapon,
		eEVS_AllSlots
	};

	struct SCharacterCreatingUIInfo
	{
		SCharacterCreatingUIInfo() : eyes_info(""), head_info(""), hair_info("") {};
		SCharacterCreatingUIInfo(string eyes, string head, string hair) : eyes_info(eyes), head_info(head), hair_info(hair) {};
		string eyes_info;
		string head_info;
		string hair_info;
	};

	void AddEntityModelToHud(EHudEntityType type, EntityId id, string ui_element, string ui_clip);
	void ClearHudModelsRnd();

	void ShowInventory();
	void ShowCharacterMenu();
	void ShowCharacterCreatingMenu();
	void ShowContainerInventory();
	void ShowTraiderInventory();
	void ShowDialogSystemHud();
	void ShowInvContextMenu(bool show);
	void ShowMainHud();
	void ShowItemInformation(bool show, bool update, EntityId item_id);
	void ShowSpellInformation(bool show, bool update, int id);
	void ShowDraggedItemIcon(bool show, EntityId item_id);
	void DraggedItemUpdatePos();

	void TabPressed(int id);

	void ShopTabPressed(int id);

	void ChestTabPressed(int id);

	void SpellsSelectionTabPressed(int id);

	void DialogLinePressed(int id);

	void DisableContextMenu();

	void FillInventory();

	bool CheckForOpenedOthersUI(ECallToOpenSystemUI SysUI);

	void CreateFlashPlayerInstancesForUIElements();

	void StatMsg(const char* msg);

	void AddDialogLine(string line, int num);
	void DialogNPCWords(string words);
	void ShowDialog(bool show, string dialog_main_file, int dialog_stage);
	void ShowDialogSystemAlt(bool show);
	bool questionloaded[255];
	void DialogSystemClear();
	int GetDialogLinesLoadedNumber();
	void EnableCurrentDialogCloseButton(bool enable);
	
	void ShowBook(bool show, EntityId item_id);
	void ShowChSkills(bool show);
	void ShowChPerks(bool show);
	void ShowPerkInfo(bool show);
	void ShowMinimap(bool show, bool by_interactive_ui = false);
	void SetMinimapScale(float scale);
	void ShowMainMap(bool show);

	void ShowQuickSelectionMenu(bool show);
	void UpdateQuickSelectionMenu();

	void ShowOnDeathScreen(bool show);
	void ShowHints(bool show);
	void AddHint(Vec2i xy, float time, string msg, float scale = 1.0f);
	void ShowOnScreenObjInfo(bool show);
	void UpdateOnScreenObjInfo();
	void AddTargetToScreenObjInfo(EntityId id);
	void DelTargetFromScreenObjInfo(EntityId id);
	void ShowEnemyHPBar(bool show);
	void ShowArcadeCompass(bool show);
	void ShowPostInitialization(bool show);

	void ShowSpellSelectionMenu(bool show);

	void ShowChestInv(bool show);
	void ShowChestInvsss(EntityId entityId, bool show);

	void ShowInteractiveObjectInfo(string icon, string text, int interactive, bool show);
	bool isInteractiveInfoObjectShowed = false;
	void ShowQuestSystem(bool show);

	void ShowTradeSystem(EntityId entityId, bool show);

	void ShowSimpleCrosshair(bool show, bool update_request);
	void ShowSelectedWeapons(bool show, bool update_request);

	void AddBuffOrDebuff(int buff_id, const char *buff_icon, int time, int type);
	void RemoveBuffOrDebuff(int buff_id, int type);
	void UpdateTimeForBuffOrDebuff(int buff_id, int time, int type);

	void Str_pl();
	void Str_min();
	void Agl_pl();
	void Agl_min();
	void Int_pl();
	void Int_min();
	void Endr_pl();
	void Endr_min();
	void Wlp_pl();
	void Wlp_min();
	void Pers_pl();
	void Pers_min();
	void Hp_pl();
	void Hp_min();
	void St_pl();
	void St_min();
	void Mp_pl();
	void Mp_min();

	void UpdateMaxHealth();
	void UpdateMaxStamina();
	void UpdateMaxMagicka();
	void UpdateHealth();
	void UpdateStamina();
	void UpdateMagicka();
	void UpdateStrength();
	void UpdateAgility();
	void UpdateWillpower();
	void UpdateEndurance();
	void UpdatePersonality();
	void UpdateIntelligence();
	void UpdateLevel();

	void UpdateNumInvSlots();

	void ClearChestInventory(bool reload_element = false);
	void ClearTradeSystem(bool reload_element = false);
	void ClearSpellSelection(bool reload_element = false);
	void UpdateChestInventory(float frameTime, EntityId chestId = 0);
	void UpdateTradeSystem(float frameTime);
	void UpdateSpellSelection(float frameTime);
	void UpdatePerksVis(float frameTime);
	void UpdateObjectsMarkersOnMinimap(float frameTime);
	void UpdateObjectsMarkersOnMap(float frameTime);

	void DelayActionForPlayer();

	void UpdateCharCreatingUI(SCharacterCreatingUIInfo sInfo);

	//ui events
	void OnLoadLevel(const char* mapname, bool isServer, const char* gamerules);
	void OnReloadLevel();
	void OnSaveGame(bool shouldResume);
	void OnLoadGame(bool shouldResume);
	void OnPauseGame();
	void OnResumeGame();
	void OnExitGame();
	void OnStartGame();
	void OnStartInGameMenu();
	void OnStopInGameMenu();
	//--------------------------------------------------------------------------------------------------------------------------------------
	//----Call able from post update only---------------------------------------------------------------------------------------------------
	Vec2i GetEntityPositionOnScreenInUIElement(IUIElement* pElem, Vec3 ent_pos, Vec3 offset);
	//--------------------------------------------------------------------------------------------------------------------------------------
	Vec2i GetEntityPositionOnScreenInUIElementWoProjection(IUIElement* pElem, Vec3 ent_pos, Vec3 offset, bool w_hl_size = true);
	bool IsActorVisibleByPlayer(EntityId actorId, float lgnt);

	void DeleteItemVisualFromInventory(int slot_id);

	Vec3 current_mouse_world_pos;
	Vec3 current_mouse_world_pos_projected;

	bool Inv_loaded;
	bool ChrMenu_loaded;
	bool MainHud_loaded;
	bool Inv_cont_menu_loaded;
	bool Inv_ItemInfo_loaded;
	bool Inv_dragged_item_loaded;
	bool StatMsg_loaded;
	bool BuffInfo_loaded;
	bool SpellInfo_loaded;

	bool Character_Creation_Menu_Loaded;

	bool SimpleCrosshair_loaded;
	EntityId old_player_selected_item;
	EntityId old_player_selected_item2;
	EntityId old_player_selected_item_left;
	int old_player_selected_spell_id;
	int old_player_selected_spell_id_left;
	bool SelectedWeaponsInfo_loaded;

	int m_str_val_cm;
	int m_agl_val_cm;
	int m_int_val_cm;
	int m_endr_val_cm;
	int m_will_val_cm;
	int m_pers_val_cm;
	int m_Mhp_val_cm;
	int m_Mst_val_cm;
	int m_Mmg_val_cm;

	EntityId old_silhol_ent;
	EntityId current_dragged_item;
	int current_dragged_item_slot;
	bool start_drag;
	EntityId item_for_drag_exchandge;
	int slot_for_drag_exchandge;
	bool item_drop_request;

	bool hit_on_player_doll_clip;
	bool hit_on_drop_clip;
	bool player_doll_clip_pressed;
	Vec2 player_doll_clip_pressed_pos;
	Vec2 player_doll_clip_pressed_pos_old;
	CMenuRender3DModelMgr::TAddedModelIndex characterModelIndex = NULL;

	EntityId GetPlayerItemByPrimHudPosInINV(int id);
	EntityId GetPlayerItemBySecondHudPosInINV(int id);

	float		m_fStamina;
	float		m_fMagicka;
	float		m_fHealth;
	float		m_fStrength;
	float		m_fAgility;
	float		m_fWillpower;
	float		m_fEndurance;
	float		m_fPersonality;
	float		m_fIntelligence;
	float		m_fLevel;
	float		m_fArmor;
	float		m_fAtk;
	float		m_fAttPoints;
	float		m_fXP;

	bool m_Minimap_closed_bitui;
	bool m_DropActive;
	//bool m_CharacterInventoryVisible;
	bool m_DialogSystemVisible;
	bool m_DialogSystemAltVisible;
	bool m_ChestInvVisible;
	bool m_CharacterEquipmentVisible;
	bool m_Icmenu;
	bool m_BookVisible;
	bool m_CharacterCreationVisible;
	bool m_TradeSystemVisible;
	bool m_QuestSystemVisible;
	bool m_SpellSelectionVisible;
	bool m_CharacterPerksMenuVisible;
	bool m_CharacterSkillsMenuVisible;
	bool m_CharacterSkillInfoVisible;
	int m_CharacterSkillSelectedId;
	bool m_InteractiveInfoVisible;

	bool m_DeathScreenVisible;
	bool m_InGameHintsVisible;
	bool m_OnScreenObjInfoVisible;
	bool m_EnemyHPBarVisible;
	bool m_ArcadeCompassVisible;
	bool m_PostInitScreenVisible;

	bool m_QuickSelectionVisible;

	bool m_CM_opened_after_levelup;

	bool m_fs_tab_open;

	bool m_bFirstFrame;

	bool m_GamePausedIU;

	bool m_MinimapVisible;
	bool m_MainMapVisible;
	float maps_update_timers[2];
	float maps_update_delays[2];
	float objInf_update_timer;
	float objInf_update_timer2;
	float objInf_update_delay;
	float objInf_update_delay2;
	std::vector<EntityId> entites_ont_screen_to_update;
	std::vector<EntityId> target_entites_ont_screen_to_update;
	float sd_ui_timers[2];
	float post_initialization_timer = 0.0f;
	std::vector<EntityId> entites_ont_map_to_update;
	std::vector<int> static_markers_on_minimap_ids;

	bool request_for_delayed_update_player_doll;
	float delayed_update_player_doll_timer;
	EntityId id_hud_pl;
	//***************************************************************************//
	bool OnAction(const ActionId& action, int activationMode, float value);
	//***************************************************************************//
	struct SMapMarker
	{
		SMapMarker()
		{
			marker_icon = "";
			marker_name = "";
			marker_pos = Vec2(0, 0);
			marker_id = -5;
		}
		string marker_icon;
		string marker_name;
		Vec2 marker_pos;
		int marker_id;
	};
	std::vector<SMapMarker> s_MapStaticMarkers;
	void AddMapMarker(int id, string icon, string name, Vec2 pos, bool updateMapUi = false);
	void DeleteMapMarker(int id, bool updateMapUi = false);
	void ClearAllMapMarkers(bool updateMapUi = false);

	Vec2 character_model_clip_pose;

	void CreateUIActionFilter(int uiType, bool enable);
	void OnInterInUIMode(int uiType, bool enable);
	float Effects_vals_DOF[4];
	bool  AnyInteractiveUIOpened();
	const char* equipment_slots_names[eEVS_AllSlots];
	int GetItemInvSlotIdFromEquippedVisualSlot(const char* slot);
	bool IsEquippedSlotBusy(const char* slot);
	EntityId GetItemIdFromEquippedSlot(const char* slot);
	void UpdateEquippedItemsInInvWindow();
	const char* current_equipment_dragged_slot;
	//float hud_simple_update_timers[8];
	uint32 m_TimerInventoryUpdate;
	uint32 m_TimerInventoryDtl_disable;
	uint32 m_TimerContainerUI_disable;
	uint32 m_TimerCurrentInteractiveUI_disable;
	uint32 m_TimerGoToMainMenu;
	uint32 m_TimerLoadingMap;
	uint32 m_TimerLoadingMiniMap;
	uint32 m_TimerLoadingAblUi;
	uint32 m_TimerDialogUIDisablingBB;
	uint32 m_TimerEnInv;
	int num_loaded_mm_sectors;
	int num_loaded_m_sectors;
	float old_tscale = 1.0f;
	std::list<string> s_minimap_sectors_pts;
	std::list<string> s_map_sectors_pts;

	void StartDtlDisableTimer(bool book_autoenable = true);
	void DtlBook(void* pUserData, IGameFramework::TimerID handler);
	void DisablingContextMenu(void* pUserData, IGameFramework::TimerID handler);
	void StartDSChestInvTimer();
	void DisablingChestInv(void* pUserData, IGameFramework::TimerID handler);
	void StartCurrentInteractiveUIDisablingTimer();
	void DisablingCurrentInteractiveUI(void* pUserData, IGameFramework::TimerID handler);
	void StartGoToMainMenuTimer();
	void GoToMainMenu(void* pUserData, IGameFramework::TimerID handler);
	void StartLoadingMinimapSectors();
	void StartLoadingMapSectors();
	void LoadNextMinimapSector(void* pUserData, IGameFramework::TimerID handler);
	void LoadNextMapSector(void* pUserData, IGameFramework::TimerID handler);
	void StartLoadingAblUI();
	void LoadAblUI(void* pUserData, IGameFramework::TimerID handler);
	void DisablingCurrentDialogUI(void* pUserData, IGameFramework::TimerID handler);
	void EnInventory(void* pUserData, IGameFramework::TimerID handler);
	int current_chest_or_shop_tab_opened;
	bool chest_or_shop_showed_player_inv;
	EntityId current_Seller;
	int current_spell_selection_tab_opened;
	EntityId temportary_book_id = 0;
	int disconnect_requested;
	float old_main_player_values[5];
	float player_values_alpha_timers[5];
	//---fix
	float timer_to_can_item_press = 0.0f;

	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};

//-----------------------------------------------------------------------------------------------------

#endif

//-----------------------------------------------------------------------------------------------------
