#pragma once

#ifndef __CROSSBOW_H__
#define __CROSSBOW_H__

#include "Weapon.h"
#include "FireModeParams.h"

#define MAX_BOLTS_LOADED 12
class CCrossBow : public CWeapon, public IWeaponFiringLocator
{
	typedef CWeapon BaseClass;
public:

	CCrossBow();
	~CCrossBow();
	virtual bool Init(IGameObject * pGameObject);
	virtual void Update(SEntityUpdateContext &ctx, int slot);
	virtual void OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value);
	virtual void StartFire();
	virtual void StartFire(const SProjectileLaunchParams& launchParams);
	virtual void StopFire();
	virtual void OnSelected(bool selected);
	virtual void OnEnterFirstPerson();
	virtual void OnEnterThirdPerson();
	virtual void OnReset();

	virtual bool GetProbableHit(EntityId weaponId, const IFireMode* pFireMode, Vec3& hit);
	virtual bool GetFiringPos(EntityId weaponId, const IFireMode* pFireMode, Vec3& pos);
	virtual bool GetFiringDir(EntityId weaponId, const IFireMode* pFireMode, Vec3& dir, const Vec3& probableHit, const Vec3& firingPos);
	virtual bool GetActualWeaponDir(EntityId weaponId, const IFireMode* pFireMode, Vec3& dir, const Vec3& probableHit, const Vec3& firingPos);
	virtual bool GetFiringVelocity(EntityId weaponId, const IFireMode* pFireMode, Vec3& vel, const Vec3& firingDir);
	virtual void WeaponReleased();

	int max_number_of_loaded_bolts_in_crossbow;
	float bolt_damage_mult;
	float bolt_speed_mult;
	float reload_time_for_one_bolt;
	float reload_timer;
	float reload_time_sequence1;
	float reload_time_sequence2;
	float recoil_after_shot_time;
	float recoil_after_reload_time;
	float recoil_timer;
	bool can_shot;
	bool process_reload_started;
	int current_loaded_bolts_number;
	int reload_type;
	bool bolt_attached_to_hand;
	bool bolt_attached_to_crossbow[MAX_BOLTS_LOADED];
	string bolts_models_saved[MAX_BOLTS_LOADED];
	string bolts_ammo[MAX_BOLTS_LOADED];
	//************************************************************************************
	//------------------------------------------------------------------------------------
	void ConsumptAmmoItem();
	void ProcessReloadBolt();
	void UpdateLoadedBolts();
	void AttachBoltToHand();
	void DetachBoltFromHand();
	void CreateBoltAttachmentOnCrossbow(int bolt_id);
	void CreateBoltAttachmentOnCrossbowFromSaved(int bolt_id);
	void DeleteBoltAttachmentFromCrossbow(int bolt_id);
	void EnableLoadedAnim(int bolt_id);
	void PlayWeaponShotAnim(int bolt_id);
	void Shot(int bolt_id);
	void ReinitFiremode();
	bool IsNoAmmoType();
	bool IsAmmoTypeCorrected();
	void ResetLoadedState();
	void OnHitDeathReaction();
	void GetOrPutBoltToQuiver(bool get);
private:
	SCrossBowParams pCrossbowParams;
};
#endif