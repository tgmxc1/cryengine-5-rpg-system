
#ifndef __MELEETIMED_H__
#define __MELEETIMED_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include "Weapon.h"
//#include "Fists.h"
//#include "ItemParamReader.h"
#include "IGameRulesSystem.h"


#define ResetValue(name, defaultValue) if (defaultInit) name=defaultValue; reader.Read(#name, name)
#define ResetValueEx(name, var, defaultValue) if (defaultInit) var=defaultValue; reader.Read(name, var)

class CMeleeTimed :
	public IFireMode
{
	struct StopAttackingAction;

	typedef struct SMeleeTimedParams
	{
		SMeleeTimedParams() { Reset(); };
		void Reset(const IItemParamsNode *params=0, bool defaultInit=true)
		{
			//CItemParamReader reader(params);
			/*ResetValue(helper,				"");
			ResetValue(range,					1.75f);
			ResetValue(animspdmult,					1.0f);
			ResetValue(ray_test_time,					1.0f);
			ResetValue(ray_test_time1,					1.0f);
			ResetValue(ray_test_time2,					1.0f);
			ResetValue(ray_test_time3,					1.0f);
			ResetValue(ray_test_time4,					1.0f);
			ResetValue(ray_test_time5,					1.0f);
			ResetValue(ray_test_time6,					1.0f);
			ResetValue(ray_test_time7,					1.0f);
			ResetValue(ray_test_time8,					1.0f);
			ResetValue(ray_test_time9,					1.0f);

			ResetValue(ray_test_upd,					0.2f);

			ResetValue(atkspdmult,					1.0f);
			ResetValue(atkspdmult2,					1.0f);
			ResetValue(number_combohits,					1);
			ResetValue(damage,				32);
			ResetValue(damage1,				32);
			ResetValue(damage2,				32);
			ResetValue(damage3,				32);
			ResetValue(damage4,				32);
			ResetValue(damage5,				32);
			ResetValue(damage6,				32);
			ResetValue(damage7,				32);
			ResetValue(damage8,				32);
			ResetValue(damage9,				32);
			ResetValue(damageAI,			10);
			ResetValue(crosshair,			"default");
			ResetValue(hit_type,			"melee");
			ResetValue(impulse,				50.0f);

			ResetValue(delay,					0.5f);
			ResetValue(delay1,					0.5f);
			ResetValue(delay2,					0.5f);
			ResetValue(delay3,					0.5f);
			ResetValue(delay4,					0.5f);
			ResetValue(delay5,					0.5f);
			ResetValue(delay6,					0.5f);
			ResetValue(delay7,					0.5f);
			ResetValue(delay8,					0.5f);
			ResetValue(delay9,					0.5f);

			ResetValue(duration,			0.5f);
			ResetValue(duration1,			0.5f);
			ResetValue(duration2,			0.5f);
			ResetValue(duration3,			0.5f);
			ResetValue(duration4,			0.5f);
			ResetValue(duration5,			0.5f);
			ResetValue(duration6,			0.5f);
			ResetValue(duration7,			0.5f);
			ResetValue(duration8,			0.5f);
			ResetValue(duration9,			0.5f);

			ResetValue(first_hited_ent_dmg_mlt,					1.0f);
			ResetValue(second_hited_ent_dmg_mlt,					1.0f);
			ResetValue(third_hited_ent_dmg_mlt,					1.0f);
			ResetValue(fourth_hited_ent_dmg_mlt,					1.0f);
			ResetValue(other_hited_ent_dmg_mlt,					1.0f);*/
		}

		void GetMemoryStatistics(ICrySizer * s)
		{
			s->Add(crosshair);
			s->Add(hit_type);
		}

		ItemString	helper;
		float				range;
		float				animspdmult;
		float				ray_test_time;
		float				ray_test_time1;
		float				ray_test_time2;
		float				ray_test_time3;
		float				ray_test_time4;
		float				ray_test_time5;
		float				ray_test_time6;
		float				ray_test_time7;
		float				ray_test_time8;
		float				ray_test_time9;

		float				ray_test_upd;

		float				atkspdmult;
		float				atkspdmult2;
		short				number_combohits;// 1 to 10 max

		short				damage;
		short				damage1;
		short				damage2;
		short				damage3;
		short				damage4;
		short				damage5;
		short				damage6;
		short				damage7;
		short				damage8;
		short				damage9;
		short				damageAI;
		ItemString	crosshair;
		ItemString	hit_type;


		float first_hited_ent_dmg_mlt;
		float second_hited_ent_dmg_mlt;
		float third_hited_ent_dmg_mlt;
		float fourth_hited_ent_dmg_mlt;
		float other_hited_ent_dmg_mlt;

		float		impulse;

		float		delay;
		float		delay1;
		float		delay2;
		float		delay3;
		float		delay4;
		float		delay5;
		float		delay6;
		float		delay7;
		float		delay8;
		float		delay9;

		float		duration;
		float		duration1;
		float		duration2;
		float		duration3;
		float		duration4;
		float		duration5;
		float		duration6;
		float		duration7;
		float		duration8;
		float		duration9;

	} SMeleeTimedParams;

	typedef struct SMeleeTimedActions
	{
		SMeleeTimedActions() { Reset(); };
		void Reset(const IItemParamsNode *params=0, bool defaultInit=true)
		{
			//CItemParamReader reader(params);
			ResetValue(attack,			"attack");
			ResetValue(attack1,			"attack1");
			ResetValue(attack2,			"attack2");
			ResetValue(attack3,			"attack3");
			ResetValue(attack4,			"attack4");
			ResetValue(attack5,			"attack5");
			ResetValue(attack6,			"attack6");
			ResetValue(attack7,			"attack7");
			ResetValue(attack8,			"attack8");
			ResetValue(attack9,			"attack9");
			ResetValue(hit,					"hit");
		}

		void GetMemoryStatistics(ICrySizer * s)
		{
			s->Add(attack);
			s->Add(attack1);
			s->Add(attack2);
			s->Add(attack3);
			s->Add(attack4);
			s->Add(attack5);
			s->Add(attack6);
			s->Add(attack7);
			s->Add(attack8);
			s->Add(attack9);
			s->Add(hit);
		}

		ItemString	attack;
		ItemString	attack1;
		ItemString	attack2;
		ItemString	attack3;
		ItemString	attack4;
		ItemString	attack5;
		ItemString	attack6;
		ItemString	attack7;
		ItemString	attack8;
		ItemString	attack9;

		ItemString	hit;
	} SMeleeTimedActions;

public:
	CMeleeTimed();
	virtual ~CMeleeTimed();

	//IFireMode
	virtual void Init(IWeapon *pWeapon, const struct IItemParamsNode *params);
	virtual void Update(float frameTime, uint frameId);
	virtual void PostUpdate(float frameTime) {};
	virtual void UpdateFPView(float frameTime) {};
	virtual void Release();
	virtual void GetMemoryStatistics(ICrySizer * s);

	virtual void ResetParams(const struct IItemParamsNode *params);
	virtual void PatchParams(const struct IItemParamsNode *patch);

	virtual void Activate(bool activate);

	virtual int GetAmmoCount() const { return 0; };
	virtual int GetClipSize() const { return 0; };

	virtual bool OutOfAmmo() const { return false; };
	virtual bool CanReload() const { return false; };
	virtual void Reload(int zoomed) {};
	virtual bool IsReloading() { return false; };
	virtual void CancelReload() {};
	virtual bool CanCancelReload() { return false; };

	virtual bool AllowZoom() const { return true; };
	virtual void Cancel() {};

	virtual float GetRecoil() const { return 0.0f; };
	virtual float GetSpread() const { return 0.0f; };
	virtual float GetMinSpread() const { return 0.0f; }
	virtual float GetMaxSpread() const { return 0.0f; }
	virtual const char *GetCrosshair() const { return ""; };

	virtual bool CanFire(bool considerAmmo=true) const;
	virtual void StartFire();
	virtual void StopFire();
	virtual bool IsFiring() const { return m_attacking; };
	virtual float GetHeat() const { return 0.0f; };
	virtual bool	CanOverheat() const {return false;};

	virtual void NetShoot(const Vec3 &hit, int ph);
	virtual void NetShootEx(const Vec3 &pos, const Vec3 &dir, const Vec3 &vel, const Vec3 &hit, float extra, int ph);
	virtual void NetEndReload() {};

	virtual void NetStartFire();
	virtual void NetStopFire();

	virtual EntityId GetProjectileId() const { return 0; };
	virtual EntityId RemoveProjectileId() { return 0; };
	virtual void SetProjectileId(EntityId id) {};

	virtual const char *GetType() const;
	virtual IEntityClass* GetAmmoType() const { return 0; };
	virtual int GetDamage(float distance=0.0f) const;
	virtual const char *GetDamageType() const;

	virtual float GetSpinUpTime() const { return 0.0f; };
	virtual float GetSpinDownTime() const { return 0.0f; };
  virtual float GetNextShotTime() const { return 0.0f; };
	virtual void SetNextShotTime(float time) {};
  virtual float GetFireRate() const { return 0.0f; };

	virtual void Enable(bool enable) { m_enabled = enable; };
	virtual bool IsEnabled() const { return m_enabled; };

	virtual Vec3 GetFiringPos(const Vec3 &probableHit) const {return ZERO;}
	virtual Vec3 GetFiringDir(const Vec3 &probableHit, const Vec3& firingPos) const {return ZERO;}
	virtual void SetName(const char *name) { m_name = name; };
	virtual const char *GetName() { return m_name.empty()?0:m_name.c_str();};

  virtual bool HasFireHelper() const { return false; }
  virtual Vec3 GetFireHelperPos() const { return Vec3(ZERO); }
  virtual Vec3 GetFireHelperDir() const { return FORWARD_DIRECTION; }

  virtual int GetCurrentBarrel() const { return 0; }
	virtual void Serialize(TSerialize ser) {};
	virtual void PostSerialize(){};

	virtual void SetRecoilMultiplier(float recoilMult) { }
	virtual float GetRecoilMultiplier() const { return 1.0f; }

	virtual void PatchSpreadMod(SSpreadModParams &sSMP){};
	virtual void ResetSpreadMod(){};

	virtual void PatchRecoilMod(SRecoilModParams &sRMP){};
	virtual void ResetRecoilMod(){};

	virtual void ResetLock() {};
	virtual void StartLocking(EntityId targetId, int partId) {};
	virtual void Lock(EntityId targetId, int partId) {};
	virtual void Unlock() {};

	virtual void SetAmmoType(const char* type) { };

	//~IFireMode

	virtual bool PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual bool PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual void Hit2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote);
	virtual bool PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual void Hit(ray_hit *hit, const Vec3 &dir, float damageScale, bool remote);
	virtual void Hit(geom_contact *contact, const Vec3 &dir, float damageScale, bool remote);
	virtual void Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote);
	virtual void Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale);
	virtual void Impulse2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale);

	virtual void ApplyCameraShake(bool hit);

	//Special case when performing melee with an object (offHand)
	//It must ignore also the held entity!
	virtual void IgnoreEntity(EntityId entityId) { m_ignoredEntity = entityId; }
	virtual void MeleeScale(float scale) { m_meleeScale = scale; }
	virtual float GetOwnerStrength() const;

protected:
	CWeapon *m_pWeapon;
	bool		m_enabled;

	bool		m_attacking;
	Vec3		m_last_pos;

	float		m_delayTimer;
	float		m_durationTimer;
	float		m_nextatkTimer;
	short		m_nextatk;
	string	m_name;
	Vec3		m_beginPos;
	bool		m_attacked;

	EntityId	m_ignoredEntity;
	float			m_meleeScale;

	SMeleeTimedParams	m_meleeparams;
	SMeleeTimedActions	m_meleeactions;

	bool			m_noImpulse;

	float		m_updtimer;
	float		m_updtimer2;
	std::vector<EntityId> m_hitedEntites;

	float			m_crt_hit_dmg_mult;
	int num_hitedEntites;

	bool			m_animspeed_returned;

}


#endif