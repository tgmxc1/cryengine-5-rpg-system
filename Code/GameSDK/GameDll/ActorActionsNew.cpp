#include "StdAfx.h"
#include "ActorActionsNew.h"
#include <CrySystem/XML/IXml.h>
#include "Actor.h"
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "GameXMLSettingAndGlobals.h"
#include "UI/HUD/HUDCommon.h"
#include "DialogSystemEvents.h"
#include "PlayerPlugin_Interaction.h"
#include "IGameObject.h"
#include "IWorldQuery.h"
#include "Bow.h"
#include "WeaponSystem.h"
#include <CryAISystem/IAIActor.h>

#include "Environment/Chest.h"

#include "PlayerEntityInteraction.h"

#include "NoWeapon.h"

#include "LevelTolevelSerialization.h"

#include "PlayerCamera.h"

//#pragma warning(push)
#pragma warning(disable : 4244)

CActorActionsNew::CActorActionsNew()
{
	//CryLogAlways("CActorActionsNew::CActorActionsNew()  constructor call");
	CurrentInteractiveId = 0;
	Current_inventory_selection_mode = ePISMode_equip;
	request_for_delayed_update_vis_attachments_on_player = false;
	frames_for_delayed_update_vis_attachments_on_player = 0.0f;
	current_player_id = 0;
	player_shadow_slot_updated = false;
	current_aim_target_id = 0;
	stop_aim_target_update = false;
	m_Timer_PL_Shadow_Update = NULL;
	current_aim_target_pos = Vec3(ZERO);
}

CActorActionsNew::~CActorActionsNew()
{

}

void CActorActionsNew::Update(float frametime)
{
	if (g_pGame)
	{
		CDialogSystemEvents *pDsystemEvent = g_pGame->GetDialogSystemEvents();
		if (pDsystemEvent)
			pDsystemEvent->AltUpdate();

		if (pl_view_shake_timer_x > 0.0f)
			pl_view_shake_timer_x -= frametime;
	}

	if (request_for_delayed_update_vis_attachments_on_player)
	{
		if (frames_for_delayed_update_vis_attachments_on_player <= 0.2f)
			frames_for_delayed_update_vis_attachments_on_player += frametime;

		if (frames_for_delayed_update_vis_attachments_on_player > 0.1f && !player_shadow_slot_updated)
		{
			//UpdatePlayerShadowCharacter(current_player_id);
			player_shadow_slot_updated = true;
		}

		if (frames_for_delayed_update_vis_attachments_on_player > 0.2f)
		{
			frames_for_delayed_update_vis_attachments_on_player = 0.0f;
			request_for_delayed_update_vis_attachments_on_player = false;
			player_shadow_slot_updated = false;
			if (gEnv->pGame && gEnv->pGame->GetIGameFramework() && gEnv->pGame->GetIGameFramework()->GetIActorSystem())
			{
				//CryLogAlways("CActorActionsNew update vis pl");
				CPlayer* pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(current_player_id));
				if (pPlayer)
					pPlayer->CheckForNewAttachmentsAndUpdate();
			}
		}
	}

	if (g_pGameCVars->g_test_cam_anim_controlled > 0.0f)
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (pPlayer)
		{
			if (pPlayer->GetSACurrent() == eSpec_act_none)
			{
				/*if(pPlayer->GetPlayerCamera()->GetCameraMode() != eCameraMode_Default)
					pPlayer->GetPlayerCamera()->SetCameraMode(eCameraMode_Default, "");

				if(!pPlayer->IsThirdPerson())
					pPlayer->m_torsoAimIK.Enable(true);*/

				//CryLogAlways("g_pGameCVars->g_test_cam_anim_controlled > 0.0f and pPlayer->GetSACurrent() == eSpec_act_none");
				//if (pPlayer->GetAnimatedCharacter() && (pPlayer->GetAnimatedCharacter()->GetMCMH() != eMCM_Animation && pPlayer->GetAnimatedCharacter()->GetMCMH() != eMCM_AnimationHCollision))
				g_pGameCVars->g_test_cam_anim_controlled = 0.0f;
				pPlayer->SetViewRotation(Quat::CreateRotationVDir(gEnv->pSystem->GetViewCamera().GetViewdir()));
			}
		}
	}

	if(!stop_aim_target_update && current_aim_target_id > 0)
		UpdateAimTarget();
}

void CActorActionsNew::StartMeleeBlockAction(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (g_pGameCVars->g_stamina_sys_can_block_if_low_stmn_val == 0)
	{
		if (pActor->GetStamina() <= 0)
			return;
	}

	if(pActor->m_lft_hand_blc || pActor->m_rgt_hand_blc)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;
//fixes-------------------------------------------------------------------------------
	if (pActor->GetSACurrent() == eSpec_act_combat_recoil)
	{
		float cnd_val = pActor->GetSpecRecoilAct(eARMode_recoil_dodge);
		const SActorStats* pActorStats = pActor->GetActorStats();
		if (pActorStats)
		{
			if (cnd_val > 0.0f && pActorStats->inAir > 0.1f)
			{
				return;
			}
			else if (cnd_val > 0.0f && pActorStats->speedFlat > 0.5f)
			{
				return;
			}
		}
	}

	CWeapon * pWpn = pActor->GetWeapon(itemId);
	if (pWpn)
	{
		if (pWpn->GetWeaponHoldedToThrow())
			return;
	}

	if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		return;
//--------------------------------------------------------------------------------------

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (!pActor->GetEquippedState(eAESlot_Shield) && !pActor->GetEquippedState(eAESlot_Torch))
	{
		pActor->m_blockactivenoshield = true;
		pActor->SetBlockActive(2, true);
		const ItemString &meleeAction = "block_melee_no_shield";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		pItem->SetItemFlag(CItem::eIF_BlockActions, true);
		NetRequestPlayItemAction2(actorId, meleeAction.c_str());
	}
	else if (pActor->GetEquippedState(eAESlot_Shield))
	{
		pActor->m_blockactiveshield = true;
		pActor->SetBlockActive(1, true);
		const ItemString &meleeAction = "block_melee_shield";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		pItem->SetItemFlag(CItem::eIF_BlockActions, true);
		NetRequestPlayItemAction2(actorId, meleeAction.c_str());
	}
	else if (pActor->GetEquippedState(eAESlot_Torch))
	{
		pActor->m_blockactiveshield = true;
		pActor->SetBlockActive(1, true);
		const ItemString &meleeAction = "block_melee_torch";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		pItem->SetItemFlag(CItem::eIF_BlockActions, true);
		NetRequestPlayItemAction2(actorId, meleeAction.c_str());
	}
}

void CActorActionsNew::StopMeleeBlockAction(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (!pActor->m_blockactivenoshield && !pActor->m_blockactiveshield)
		return;

	const ItemString &meleeAction = "block_melee_end";
	FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	//pItem->GetGameObject()->InvokeRMI(CItem::SvRequestLeaveModify(), CItem::EmptyParams(), eRMI_ToServer);
	pItem->SetItemFlag(CItem::eIF_BlockActions, false);
	pActor->m_blockactivenoshield = false;
	pActor->m_blockactiveshield = false;
	pActor->SetBlockActive(3, false);
	//net sync
	NetRequestPlayItemAction2(actorId, meleeAction.c_str());
}

void CActorActionsNew::MeleeBlockReplayAnim(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->m_blockactivenoshield && !pActor->GetEquippedState(eAESlot_Shield) && !pActor->GetEquippedState(eAESlot_Torch))
	{
		const ItemString &meleeAction = "block_melee_no_shield";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync
		NetRequestPlayItemAction2(actorId, meleeAction.c_str());
	}
	else if (pActor->m_blockactiveshield && pActor->GetEquippedState(eAESlot_Shield))
	{
		const ItemString &meleeAction = "block_melee_shield";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync
		NetRequestPlayItemAction2(actorId, meleeAction.c_str());
	}
	else if (pActor->m_blockactiveshield && pActor->GetEquippedState(eAESlot_Torch))
	{
		const ItemString &meleeAction = "block_melee_torch";
		FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync
		NetRequestPlayItemAction(actorId, fragmentId);
	}
}

string CActorActionsNew::GetSafeSkinAttachmentPth(EntityId actorId, int slot, string att_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return string();

	ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(slot);
	if (!pOwnerCharacter)
		return string();

	IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
	if (!pAttachmentManager)
		return string();

	IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName(att_name.c_str());
	if (!pAttachment)
		return string();

	IAttachmentSkin* pAttachmentSkin = pAttachment->GetIAttachmentSkin();
	if (!pAttachmentSkin)
		return string();

	ISkin* pSkinGm = pAttachmentSkin->GetISkin();
	if (!pSkinGm)
		return string();

	return string(pSkinGm->GetModelFilePath());
}

void CActorActionsNew::SetupActorBodyModelPathPart(EntityId actorId, int slot, int part_id, bool clear_att_pth)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(slot);
	if (!pOwnerCharacter)
		return;

	IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
	if (!pAttachmentManager)
		return;

	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}
	}
	string requested_att_name = "";
	if (part_id == eABPart_Head)
		requested_att_name = base_att_head;
	else if (part_id == eABPart_UpperBody)
		requested_att_name = base_att_upperBd;
	else if (part_id == eABPart_LowerBody)
		requested_att_name = base_att_lowerBd;
	else if (part_id == eABPart_Hands)
		requested_att_name = base_att_hands;
	else if (part_id == eABPart_Foots)
		requested_att_name = base_att_foots;
	else if (part_id == eABPart_Hair)
		requested_att_name = base_att_hair;
	else if (part_id == eABPart_PPP)
		requested_att_name = base_att_upperCnp;
	else if (part_id == eABPart_UnderWear1)
		requested_att_name = base_att_lowerUdw;
	else if (part_id == eABPart_UnderWear2)
		requested_att_name = base_att_upperUdw;

	IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName(requested_att_name);
	if (pAttachment)
	{
		pActor->SetActorBodyPartModelPath(part_id, GetSafeSkinAttachmentPth(actorId, 0, requested_att_name));
		if(clear_att_pth)
			pAttachment->ClearBinding();
	}
}

void CActorActionsNew::EquipArmorOrCloth(EntityId actorId, EntityId itemId)
{
	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}
	}
	//CryLogAlways("TryEquipAndWearThisItem");
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	bool gender = true;
	if (pActor->GetGender() == 1)
		gender = false;

	if (pItem->GetArmorType() == 1)
	{
		SetupActorBodyModelPathPart(actorId, 0, eABPart_Foots);
		string attachment_name = "armorboots";
		for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			if (!gender)
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorModelPath(i));
			else
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorMaleModelPath(i));
		}
		pActor->SetEquippedArmorNumParts(eAESlot_Boots, pItem->GetNumArmorParts(gender));
		pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
		pActor->SetEquippedState(eAESlot_Boots, true);
	}
	else if (pItem->GetArmorType() == 2)
	{
		if (!gender)
		{
			SetupActorBodyModelPathPart(actorId, 0, eABPart_UnderWear1);
			SetupActorBodyModelPathPart(actorId, 0, eABPart_LowerBody);
		}
		else
		{
			if (g_pGameCVars->g_Pl_underwear_mod == 1)
			{
				SetupActorBodyModelPathPart(actorId, 0, eABPart_UnderWear1);
			}
			else
			{
				SetupActorBodyModelPathPart(actorId, 0, eABPart_PPP);
				SetupActorBodyModelPathPart(actorId, 0, eABPart_LowerBody);
			}
		}
		string attachment_name = "armorlegs";
		for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			if (!gender)
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorModelPath(i));
			else
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorMaleModelPath(i));
		}
		pActor->SetEquippedArmorNumParts(eAESlot_Pants, pItem->GetNumArmorParts(gender));
		pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
		pActor->SetEquippedState(eAESlot_Pants, true);
	}
	else if (pItem->GetArmorType() == 3)
	{
		if (!gender)
		{
			SetupActorBodyModelPathPart(actorId, 0, eABPart_UnderWear2);
		}
		SetupActorBodyModelPathPart(actorId, 0, eABPart_UpperBody);
		string attachment_name = "armorchest";
		for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			if (!gender)
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorModelPath(i));
			else
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorMaleModelPath(i));
		}
		pActor->SetEquippedArmorNumParts(eAESlot_Torso, pItem->GetNumArmorParts(gender));
		pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
		pActor->SetEquippedState(eAESlot_Torso, true);
	}
	else if (pItem->GetArmorType() == 4)
	{
		SetupActorBodyModelPathPart(actorId, 0, eABPart_Hands);
		string attachment_name = "armorarms";
		for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			if (!gender)
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorModelPath(i));
			else
				pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorMaleModelPath(i));
		}
		pActor->SetEquippedArmorNumParts(eAESlot_Arms, pItem->GetNumArmorParts(gender));
		pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
		pActor->SetEquippedState(eAESlot_Arms, true);
	}
	else if (pItem->GetArmorType() == 5)
	{
		string attachment_name = "armorhead";
		const char *ps_arm_path = "";
		if (!gender)
			ps_arm_path = pItem->GetArmorModelPath(0);
		else
			ps_arm_path = pItem->GetArmorMaleModelPath(0);

		if (0 == stricmp(PathUtil::GetExt(ps_arm_path), "xml"))
		{
			pActor->CreateSkinAttachment(0, attachment_name.c_str(), ps_arm_path);
		}
		else if (0 == stricmp(PathUtil::GetExt(ps_arm_path), "skin"))
		{
			pActor->CreateSkinAttachment(0, attachment_name.c_str(), ps_arm_path);
		}
		else
		{
			pActor->CreateBoneAttachment(0, attachment_name.c_str(), ps_arm_path, "Bip01 Head", true, false);
		}

		pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
		pActor->SetEquippedState(eAESlot_Helmet, true);
	}

	if (pItem->GetShield() == 1)
	{
		string attachment_name = "armorshieldproxy";
		string attachment_name2 = "armorshield";
		//pActor->CreateBoneAttachment(0, attachment_name.c_str(), pItem->GetArmorModelPath(0), "Bip01 L Hand", true, true, pItem->GetEntityId());
		if (!pActor->GetEquippedState(eAESlot_Shield))
		{
			pItem->SetHand(1);
			pItem->HideItem(false);
			pItem->AttachToHand(true, false);
			pActor->SetEquippedState(eAESlot_Shield, true);
			pItem->GetEntity()->EnablePhysics(true);
			pItem->Physicalize(true, true);
			IPhysicalEntity *pPhysicalEntity = pItem->GetEntity()->GetPhysics();
			if (pPhysicalEntity != NULL)
			{
				pe_params_part pp;
				pp.flagsAND = ~geom_colltype_player;
				//pp.type = PE_RIGID;
				pp.flagsOR = pp.flagsOR | geom_colltype_ray;
				pp.flagsAND = pp.flagsAND | geom_colltype_ray;
				pPhysicalEntity->SetParams(&pp);
			}
			else
			{
				CryLogAlways("No Phy Entity for shield");
			}

			if (pActor->IsPlayer())
			{
				/*if (pActor->reequip_timer_cn1 != 0.0f)
				{
					pItem->AttachToHand(false, false);
					pItem->AttachToHand(true, false);
				}*/
			}
			bool view_fp = true;
			if (!pActor->IsPlayer())
				view_fp = false;
			if (pActor->IsThirdPerson())
				view_fp = false;

			int view = 1;
			//pActor->CreateBoneAttachment(0, attachment_name.c_str(), pItem->GetArmorModelPath(0), "Bip01 L Hand", false, false);
			if (!view_fp)
			{
				view = 2;
			}

			if (pActor->IsPlayer() && !pActor->IsThirdPerson())
				pItem->SetViewMode(view);
		}
	}

	if (pItem->GetItemType() == 3)
	{
		/*float hp = m_params.hprestoration;
		CActor *pOwner = static_cast<CActor *>(GetOwnerActor());
		pOwner->SetHealth(pOwner->GetHealth() + hp);*/
	}
	else if (pItem->GetItemType() == 9)
	{
		string attachment_name = "arrow_quiver";
		pActor->CreateBoneAttachment(0, attachment_name.c_str(), pItem->GetSharedItemParams()->params.arrow_quiver_path[0].c_str(),
			"Quiver", !pActor->IsAttachmentOnCharacter(0, attachment_name.c_str()), false);

		pActor->update_quiver_attachment = 2;
	}
	else if (pItem->GetItemType() == 10)
	{
		string attachment_name = "bolts_quiver";
		pActor->CreateBoneAttachment(0, attachment_name.c_str(), pItem->GetSharedItemParams()->params.arrow_quiver_path[0].c_str(),
			"Quiver", !pActor->IsAttachmentOnCharacter(0, attachment_name.c_str()), false);

		pActor->update_quiver_attachment = 2;
	}
	else if (pItem->GetItemType() == 5)
	{
		/*int spell_id = m_params.additional_effect_id;
		CHUD *pHUD = g_pGame->GetHUD();
		pHUD->AddSpellToPlayerSpellBook(spell_id);*/
	}
	else if (pItem->GetItemType() == 4)
	{
		string attachment_name = "torch";
		string attachment_name2 = "torchlight";
		string attachment_name3 = "torchparticle";
		bool view_fp = true;
		if (!pActor->IsPlayer())
			view_fp = false;
		if (pActor->IsThirdPerson())
			view_fp = false;

		int view = 1;
		//pActor->CreateBoneAttachment(0, attachment_name.c_str(), pItem->GetArmorModelPath(0), "Bip01 L Hand", false, false);
		if (!view_fp)
		{
			if (g_pGameCVars->g_enable_light_torch_alternative_activation == 0)
			{
				pActor->CreateLightAttachment(0, attachment_name2.c_str(), "Bip01 L Hand", false, ColorF(23.5f, 12.2f, 2.3f, 0.01f), 10.0f, 0.01f, true);
				pActor->CreateParticleAttachment(0, attachment_name3.c_str(), "Bip01 L Hand", pItem->GetParticlePath(0), false);
			}
			view = 2;
		}

		if (pActor->IsPlayer())
			pItem->SetViewMode(0);

		pItem->SetHand(1);
		pItem->HideItem(false);
		pItem->AttachToHand(true, false);
		if (pActor->IsPlayer())
			pItem->SetViewMode(view);
		//pItem->Select(true);
		pActor->SetEquippedState(eAESlot_Torch, true);
		if (g_pGameCVars->g_enable_light_torch_alternative_activation == 0)
		{
			string helperfire = "particle_helper";
			string helperlight = "light_helper";
			if (view_fp)
			{
				SEntitySlotInfo slotInfo;
				bool validSlot = pItem->GetEntity()->GetSlotInfo(eIGS_FirstPerson, slotInfo);
				if (validSlot)
				{
					pItem->CreateLightAttachment(eIGS_FirstPerson, attachment_name2.c_str(), helperlight.c_str(), true, ColorF(23.5f, 12.2f, 2.3f, 0.01f), 10.0f, 0.01f, true);
					pItem->CreateParticleAttachment(eIGS_FirstPerson, attachment_name3.c_str(), helperfire.c_str(), pItem->GetSharedItemParams()->params.item_particle_effect[0].c_str(), true);
					pActor->DeleteLightAttachment(0, attachment_name2.c_str(), true);
					pActor->DeleteParticleAttachment(0, attachment_name3.c_str(), true);
				}
			}
		}
		StartActionTorchEquipping(pActor->GetEntityId(), pItem->GetEntityId());
		pItem->EnableUpdate(true, eIUS_General);
	}

	//--player shadow character fix-------------
	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		if (pPlayer)
		{
			ICharacterInstance *pOwnerShadowChr = pPlayer->GetShadowCharacter();
			if (pOwnerShadowChr)
			{
				current_player_id = pPlayer->GetEntityId();
				request_for_delayed_update_vis_attachments_on_player = true;
				//UpdatePlayerShadowCharacter(pPlayer->GetEntityId());
			}
		}
	}
	//------------------------------------------
}

void CActorActionsNew::DeEquipArmorOrCloth(EntityId actorId, EntityId itemId)
{
	string base_att_foots = "foots";
	string base_att_lowerBd = "lowerbody";
	string base_att_upperBd = "upperbody";
	string base_att_lowerUdw = "Underwearlow";
	string base_att_upperUdw = "Underwearupper";
	string base_att_upperCnp = "pp";
	string base_att_hands = "hands";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_foots[0].empty())
		{
			base_att_foots = pGameGlobals->customization_data.character_base_attachment_foots[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[0].empty())
		{
			base_att_lowerBd = pGameGlobals->customization_data.character_base_attachment_legs[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[1].empty())
		{
			base_att_lowerUdw = pGameGlobals->customization_data.character_base_attachment_legs[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_legs[2].empty())
		{
			base_att_upperCnp = pGameGlobals->customization_data.character_base_attachment_legs[2];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[0].empty())
		{
			base_att_upperBd = pGameGlobals->customization_data.character_base_attachment_torso[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_torso[1].empty())
		{
			base_att_upperUdw = pGameGlobals->customization_data.character_base_attachment_torso[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_hands[0].empty())
		{
			base_att_hands = pGameGlobals->customization_data.character_base_attachment_hands[0];
		}
	}
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	bool gender = true;
	if (pActor->GetGender() == 1)
		gender = false;

	if (pItem->GetArmorType() == 1)
	{
		string attachment_name = "armorboots";
		for (int i = 0; i < pActor->GetEquippedArmorNumParts(eAESlot_Boots); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			string att_pth_nv = "";
			if (!gender)
				att_pth_nv = pItem->GetArmorModelPath(i);
			else
				att_pth_nv = pItem->GetArmorMaleModelPath(i);

			pActor->DeleteSkinAttachment(0, A1_valstr.c_str(), true, att_pth_nv.c_str());
		}
		ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(0);
		IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
		IAttachment *pAttachmentFoots = pAttachmentManager->GetInterfaceByName(base_att_foots.c_str());
		if (pAttachmentFoots)
		{
			string pathfoots = pActor->GetActorBodyPartModelPath(5);
			pActor->CreateSkinAttachment(0, base_att_foots.c_str(), pathfoots.c_str());
			//pAttachmentFoots->HideAttachment(0);
		}
		EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Boots);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItemiii)
		{
			float sss = pItemiii->GetArmorRating();
			pActor->SetArmor(pActor->GetArmor() - sss);
		}
		pActor->SetEquippedState(eAESlot_Boots, false);
		pActor->SetEquippedArmorNumParts(eAESlot_Boots, 0);
	}
	else if (pItem->GetArmorType() == 2)
	{
		string attachment_name = "armorlegs";
		for (int i = 0; i < pActor->GetEquippedArmorNumParts(eAESlot_Pants); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			string att_pth_nv = "";
			if (!gender)
				att_pth_nv = pItem->GetArmorModelPath(i);
			else
				att_pth_nv = pItem->GetArmorMaleModelPath(i);

			pActor->DeleteSkinAttachment(0, A1_valstr.c_str(), true, att_pth_nv.c_str());
		}
		ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(0);
		IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
		if (gender)
		{
			IAttachment *pAttachmentLowerbodypp = pAttachmentManager->GetInterfaceByName(base_att_upperCnp.c_str());
			if (pAttachmentLowerbodypp)
			{
				string pathlowerbodypp = pActor->GetActorBodyPartModelPath(7);
				pActor->CreateSkinAttachment(0, base_att_upperCnp.c_str(), pathlowerbodypp.c_str());
				//pAttachmentLowerbodypp->HideAttachment(0);
			}
			IAttachment *pAttachmentUnderwearlow = pAttachmentManager->GetInterfaceByName(base_att_lowerUdw.c_str());
			if (pAttachmentUnderwearlow)
			{
				string path_underwearlow = pActor->GetActorBodyPartModelPath(8);
				pActor->CreateSkinAttachment(0, base_att_lowerUdw.c_str(), path_underwearlow.c_str());
				//pAttachmentUnderwearlow->HideAttachment(0);
			}
		}
		else
		{
			IAttachment *pAttachmentUnderwearlow = pAttachmentManager->GetInterfaceByName(base_att_lowerUdw.c_str());
			if (pAttachmentUnderwearlow)
			{
				string path_underwearlow = pActor->GetActorBodyPartModelPath(8);
				pActor->CreateSkinAttachment(0, base_att_lowerUdw.c_str(), path_underwearlow.c_str());
				//pAttachmentUnderwearlow->HideAttachment(0);
			}
		}
		IAttachment *pAttachmentLowerbody = pAttachmentManager->GetInterfaceByName(base_att_lowerBd.c_str());
		if (pAttachmentLowerbody)
		{
			string pathlowerbody = pActor->GetActorBodyPartModelPath(3);
			pActor->CreateSkinAttachment(0, base_att_lowerBd.c_str(), pathlowerbody.c_str());
			//pAttachmentLowerbody->HideAttachment(0);
		}
		EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Pants);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItemiii)
		{
			float sss = pItemiii->GetArmorRating();
			pActor->SetArmor(pActor->GetArmor() - sss);
		}
		pActor->SetEquippedState(eAESlot_Pants, false);
		pActor->SetEquippedArmorNumParts(eAESlot_Pants, 0);
	}
	else if (pItem->GetArmorType() == 3)
	{
		string attachment_name = "armorchest";
		for (int i = 0; i < pActor->GetEquippedArmorNumParts(eAESlot_Torso); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			string att_pth_nv = "";
			if (!gender)
				att_pth_nv = pItem->GetArmorModelPath(i);
			else
				att_pth_nv = pItem->GetArmorMaleModelPath(i);

			pActor->DeleteSkinAttachment(0, A1_valstr.c_str(), true, att_pth_nv.c_str());
		}
		ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(0);
		IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
		if (!gender)
		{
			IAttachment *pAttachmentUnderwearupper = pAttachmentManager->GetInterfaceByName(base_att_upperUdw.c_str());
			if (pAttachmentUnderwearupper)
			{
				string path_underwearupper = pActor->GetActorBodyPartModelPath(9);
				pActor->CreateSkinAttachment(0, base_att_upperUdw.c_str(), path_underwearupper.c_str());
				//pAttachmentUnderwearupper->HideAttachment(0);
			}
		}
		IAttachment *pAttachmentupperbody = pAttachmentManager->GetInterfaceByName(base_att_upperBd.c_str());
		if (pAttachmentupperbody)
		{
			string pathupperbody = pActor->GetActorBodyPartModelPath(2);
			pActor->CreateSkinAttachment(0, base_att_upperBd.c_str(), pathupperbody.c_str());
			//pAttachmentupperbody->HideAttachment(0);
		}
		EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Torso);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItemiii)
		{
			float sss = pItemiii->GetArmorRating();
			pActor->SetArmor(pActor->GetArmor() - sss);
		}
		pActor->SetEquippedState(eAESlot_Torso, false);
		pActor->SetEquippedArmorNumParts(eAESlot_Torso, 0);
	}
	else if (pItem->GetArmorType() == 4)
	{
		string attachment_name = "armorarms";
		for (int i = 0; i < pActor->GetEquippedArmorNumParts(eAESlot_Arms); i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string A1_valstr = attachment_name + C_valstr;
			string att_pth_nv = "";
			if (!gender)
				att_pth_nv = pItem->GetArmorModelPath(i);
			else
				att_pth_nv = pItem->GetArmorMaleModelPath(i);

			pActor->DeleteSkinAttachment(0, A1_valstr.c_str(), true, att_pth_nv.c_str());
		}
		ICharacterInstance *pOwnerCharacter = pActor->GetEntity()->GetCharacter(0);
		IAttachmentManager *pAttachmentManager = pOwnerCharacter->GetIAttachmentManager();
		IAttachment *pAttachmenthands = pAttachmentManager->GetInterfaceByName(base_att_hands.c_str());
		if (pAttachmenthands)
		{
			string pathhands = pActor->GetActorBodyPartModelPath(4);
			pActor->CreateSkinAttachment(0, base_att_hands.c_str(), pathhands.c_str());
			//pAttachmenthands->HideAttachment(0);
		}
		EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Arms);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItemiii)
		{
			float sss = pItemiii->GetArmorRating();
			pActor->SetArmor(pActor->GetArmor() - sss);
		}
		pActor->SetEquippedState(eAESlot_Arms, false);
		pActor->SetEquippedArmorNumParts(eAESlot_Arms, 0);
	}
	else if (pItem->GetArmorType() == 5)
	{
		string attachment_name = "armorhead";
		string att_pth_nv = "";
		if (!gender)
			att_pth_nv = pItem->GetArmorModelPath(0);
		else
			att_pth_nv = pItem->GetArmorMaleModelPath(0);

		pActor->DeleteSkinAttachment(0, attachment_name.c_str(), true, att_pth_nv.c_str());
		EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Helmet);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		if (pItemiii)
		{
			float sss = pItemiii->GetArmorRating();
			pActor->SetArmor(pActor->GetArmor() - sss);
		}
		pActor->SetEquippedState(eAESlot_Helmet, false);
	}

	if (pItem->GetShield() == 1)
	{
		string attachment_name = "armorshieldproxy";
		string attachment_name2 = "armorshield";
		//pActor->DeleteBoneAttachment(0, attachment_name.c_str(), true);
		//pActor->DeleteBoneAttachment(0, attachment_name2.c_str(), true);
		CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Shield)));
		if (pItemiii)
		{
			pActor->SetEquippedState(eAESlot_Shield, false);
			pItem->SetHand(1);
			pItem->AttachToHand(false);
			pItem->HideItem(true);
			pItemiii->AttachToHand(false);
			IPhysicalEntity *pPhysicalEntity = pItemiii->GetEntity()->GetPhysics();
			if (pPhysicalEntity != NULL)
			{
				pe_params_part pp;
				pp.flagsOR = geom_colltype_player;
				pPhysicalEntity->SetParams(&pp);
			}
			pItemiii->Physicalize(false, false);
			pItemiii->HideItem(true);
		}
	}

	if (pItem->GetItemType() == 4)
	{
		string attachment_name = "torch";
		string attachment_name2 = "torchlight";
		string attachment_name3 = "torchparticle";
		bool view_fp = true;
		if (!pActor->IsPlayer())
			view_fp = false;

		if (view_fp)
		{
			SEntitySlotInfo slotInfo;
			bool validSlot = pItem->GetEntity()->GetSlotInfo(eIGS_FirstPerson, slotInfo);
			if (validSlot)
			{
				if (g_pGameCVars->g_enable_light_torch_alternative_activation == 0)
				{
					pItem->DeleteLightAttachment(eIGS_FirstPerson, attachment_name2.c_str(), true);
					pItem->DeleteParticleAttachment(eIGS_FirstPerson, attachment_name3.c_str(), true);
				}
			}
		}
		pItem->SetHand(1);
		pItem->AttachToHand(false);
		if (pActor->IsPlayer())
			pItem->SetViewMode(0);

		pItem->Hide(true);
		//pActor->DeleteBoneAttachment(0, attachment_name.c_str(), true);
		if (g_pGameCVars->g_enable_light_torch_alternative_activation == 0)
		{
			pActor->DeleteLightAttachment(0, attachment_name2.c_str(), true);
			pActor->DeleteParticleAttachment(0, attachment_name3.c_str(), true);
		}
		pActor->SetEquippedState(eAESlot_Torch, false);
		StartActionTorchDeEquipping(pActor->GetEntityId(), pItem->GetEntityId());
		pItem->EnableUpdate(false, eIUS_General);
	}
	else if (pItem->GetItemType() == 9)
	{
		string attachment_name = "arrow_quiver";
		if (ICharacterInstance *pAttachmentObjectChar = pActor->GetAttachmentObjectCharacterInstance(0, attachment_name.c_str()))
		{
			if (pAttachmentObjectChar->GetIAttachmentManager())
			{
				for (int i = 0; i < 10; i++)
				{
					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", i);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						pAttachment_quiver_arrow->ClearBinding();
					}
				}
			}
		}
		pActor->DeleteBoneAttachment(0, attachment_name.c_str(), true);
	}
	else if (pItem->GetItemType() == 10)
	{
		string attachment_name = "bolts_quiver";
		if (ICharacterInstance *pAttachmentObjectChar = pActor->GetAttachmentObjectCharacterInstance(0, attachment_name.c_str()))
		{
			if (pAttachmentObjectChar->GetIAttachmentManager())
			{
				for (int i = 0; i < 10; i++)
				{
					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", i);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						pAttachment_quiver_arrow->ClearBinding();
					}
				}
			}
		}
		pActor->DeleteBoneAttachment(0, attachment_name.c_str(), true);
	}


//----temp fix for dead character shadows----------------------------------------------------
	if (pActor->IsDead())
	{
		pActor->CreateBoneAttachment(0, "RE_F1_NOT_DELETE", "NLC", "Bip01", true, false);
		pActor->DeleteBoneAttachment(0, "RE_F1_NOT_DELETE", false);
	}
//-------------------------------------------------------------------------------------------
	//--player shadow character fix-------------
	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		if (pPlayer)
		{
			ICharacterInstance *pOwnerShadowChr = pPlayer->GetShadowCharacter();
			if (pOwnerShadowChr)
			{
				current_player_id = pPlayer->GetEntityId();
				request_for_delayed_update_vis_attachments_on_player = true;
				//UpdatePlayerShadowCharacter(pPlayer->GetEntityId());
			}
		}
	}
	//------------------------------------------
}

void CActorActionsNew::EquipDecorationItem(EntityId actorId, EntityId itemId, int slot)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	bool gender = true;
	if (pActor->GetGender() == 1)
		gender = false;

	char slot_str[17];
	itoa(slot, slot_str, 10);
	string attachment_name = "decor_attach_" + string(slot_str) + string("_fx");
	for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
	{
		char B_valstr[17];
		itoa(i, B_valstr, 10);
		string C_valstr = B_valstr;
		string A1_valstr = attachment_name + C_valstr;
		if (!gender)
			pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorModelPath(i));
		else
			pActor->CreateSkinAttachment(0, A1_valstr.c_str(), pItem->GetArmorMaleModelPath(i));
	}
	pActor->SetArmor(pActor->GetArmor() + pItem->GetArmorRating());
	//--player shadow character fix-------------
	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		if (pPlayer)
		{
			ICharacterInstance *pOwnerShadowChr = pPlayer->GetShadowCharacter();
			if (pOwnerShadowChr)
			{
				current_player_id = pPlayer->GetEntityId();
				request_for_delayed_update_vis_attachments_on_player = true;
				//UpdatePlayerShadowCharacter(pPlayer->GetEntityId());
			}
		}
	}
	//------------------------------------------
}

void CActorActionsNew::DeEquipDecorationItem(EntityId actorId, EntityId itemId, int slot)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	bool gender = true;
	if (pActor->GetGender() == 1)
		gender = false;

	char slot_str[17];
	itoa(slot, slot_str, 10);
	string attachment_name = "decor_attach_" + string(slot_str) + string("_fx");
	for (int i = 0; i < pItem->GetNumArmorParts(gender); i++)
	{
		char B_valstr[17];
		itoa(i, B_valstr, 10);
		string C_valstr = B_valstr;
		string A1_valstr = attachment_name + C_valstr;
		string att_pth_nv = "";
		if (!gender)
			att_pth_nv = pItem->GetArmorModelPath(i);
		else
			att_pth_nv = pItem->GetArmorMaleModelPath(i);

		pActor->DeleteSkinAttachment(0, A1_valstr.c_str(), true, att_pth_nv.c_str());
	}
	EntityId ide = pActor->GetCurrentEquippedItemId(slot);
	CItem *pItemiii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
	if (pItemiii)
	{
		float sss = pItemiii->GetArmorRating();
		pActor->SetArmor(pActor->GetArmor() - sss);
	}
	pActor->SetEquippedState(slot, false);
	pActor->SetEquippedArmorNumParts(slot, 0);
	pActor->SetEquippedItemId(slot, 0);
	//----temp fix for dead character shadows----------------------------------------------------
	if (pActor->IsDead())
	{
		pActor->CreateBoneAttachment(0, "RE_F1_NOT_DELETE", "NLC", "Bip01", true, false);
		pActor->DeleteBoneAttachment(0, "RE_F1_NOT_DELETE", false);
	}
	//-------------------------------------------------------------------------------------------
	//--player shadow character fix-------------
	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		if (pPlayer)
		{
			ICharacterInstance *pOwnerShadowChr = pPlayer->GetShadowCharacter();
			if (pOwnerShadowChr)
			{
				current_player_id = pPlayer->GetEntityId();
				request_for_delayed_update_vis_attachments_on_player = true;
				//UpdatePlayerShadowCharacter(pPlayer->GetEntityId());
			}
		}
	}
	//------------------------------------------
}

void CActorActionsNew::UseItemCommon(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pItem->GetItemPotType() == 1)
	{
		for (int i = 0; i < 15; i++)
		{
			int ids_eff = pItem->GetItemAddotionalEffect(i);
			if (ids_eff <= 0)
				continue;

			pActor->AddBuff(ids_eff, pItem->GetReprFc());
		}
	}
	else if (pItem->GetItemPotType() == 2)
	{
		for (int i = 0; i < 3; i++)
		{
			if (i == 0)
				continue;

			float pot_eff_frc = pItem->GetItemPotEff(i);
			if (i == 1)
			{
				if (pot_eff_frc > 0.0f)
					pActor->SetHealth(pActor->GetHealth() + pot_eff_frc);
			}
			else if (i == 2)
			{
				if (pot_eff_frc > 0.0f)
					pActor->SetMagicka(pActor->GetMagicka() + pot_eff_frc);
			}
			else if (i == 3)
			{
				if (pot_eff_frc > 0.0f)
					pActor->SetStamina(pActor->GetStamina() + pot_eff_frc);
			}
		}
	}
	else if (pItem->GetItemPotType() == 3)
	{

	}

	if (pItem->GetItemPotType() > 0)
	{
		pItem->SetItemQuanity(pItem->GetItemQuanity() - 1);
		if (pItem->GetItemQuanity() < 1)
		{
			//pItem->Drop(1, false, false);
			//pItem->GetEntity()->Hide(true);
			if (gEnv->bMultiplayer && !gEnv->bServer)
			{
				CGameRules *pGameRules = g_pGame->GetGameRules();
				if (pGameRules)
				{
					pItem->GetEntity()->SetPos(Vec3(ZERO));
					if (pActor->GetInventory())
					{
						pActor->GetInventory()->RemoveItem(pItem->GetEntityId());
						//pItem->NetSetOwnerId(0);
						//pItem->SetOwnerId(0);
						pItem->Pickalize(false, false);
						pItem->GetEntity()->Hide(true);
					}
					pGameRules->GetGameObject()->InvokeRMI(CGameRules::SvDropAndHideItem(), CGameRules::SEntityPrmIdOnly(pItem->GetEntityId()), eRMI_ToServer);
				}
			}
			else
			{
				pItem->Drop(1, false, false);
				pItem->GetEntity()->Hide(true);
			}
		}
	}

	if (pItem->GetItemType() == 5)
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		for (int i = 0; i < 15; i++)
		{
			int spell_id = pItem->GetItemAddotionalEffect(i);
			if (spell_id <= 0)
				continue;

			if (!pActor->IsSpellInActorSB(spell_id))
			{
				pActor->AddSpellToActorSB(spell_id);
				//fast test
				//pActor->AddSpellToCurrentSpellsForUseSlot(spell_id, 1);
			}
			else
			{
				if (pHud && pActor->IsPlayer() && !pActor->IsRemote())
				{
					pHud->StatMsg("Spell is already in user spellbook");
				}
			}
		}
	}
}

void CActorActionsNew::StartActionShieldEquipping(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	const ItemString &itemAction = "shield_equipping";
	FragmentID fragmentId = pItem->GetFragmentID(itemAction.c_str());
	pItem->PlayAction(fragmentId, 5, false, CItem::eIPAF_Default);
}

void CActorActionsNew::StartActionShieldDeEquipping(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	const ItemString &itemAction = "shield_deequipping";
	FragmentID fragmentId = pItem->GetFragmentID(itemAction.c_str());
	pItem->PlayAction(fragmentId, 5, false, CItem::eIPAF_Default);
}

void CActorActionsNew::StartActionTorchEquipping(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	const ItemString &itemAction = "torch_equipping";
	FragmentID fragmentId = pItem->GetFragmentID(itemAction.c_str());
	pItem->PlayAction(fragmentId, 5, false, CItem::eIPAF_Default);
}

void CActorActionsNew::StartActionTorchDeEquipping(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	const ItemString &itemAction = "torch_deequipping";
	FragmentID fragmentId = pItem->GetFragmentID(itemAction.c_str());
	pItem->PlayAction(fragmentId, 5, false, CItem::eIPAF_Default);
}

void CActorActionsNew::UpdateItemViewModeForced(EntityId itemId)
{
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;
}

bool CActorActionsNew::TestHitBlock(EntityId T_actorId, EntityId H_actorId, int damage)
{
	CActor* pCAITarget = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(T_actorId));
	if (!pCAITarget)
		return false;

	CActor* pCAIHiter = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(H_actorId));
	if (!pCAIHiter)
		return false;

	EntityId itm_id_cur_hiter = 0;
	EntityId itm_id_cur_target = 0;
	CItem* pCurItemHiter = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAIHiter->GetCurrentItemId()));
	if (pCurItemHiter)
		itm_id_cur_hiter = pCurItemHiter->GetEntityId();

	CItem* pCurItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAITarget->GetCurrentItemId()));
	if (pCurItemTarget)
		itm_id_cur_target = pCurItemTarget->GetEntityId();

	if (pCAITarget->m_blockactivenoshield)
	{
		float behindTargetAngleDegrees = 175.0f;
		Vec3 v3HiterToTarget = pCAIHiter->GetEntity()->GetWorldPos() - pCAITarget->GetEntity()->GetWorldPos();
		Vec2 vHiterToTarget(v3HiterToTarget);
		float cosineBehindTargetHalfAngleRadians = cos(DEG2RAD(behindTargetAngleDegrees / 2.0f));
		Vec2 vBehindTargetDir = -Vec2(pCAITarget->GetAnimatedCharacter()->GetAnimLocation().GetColumn1()).GetNormalizedSafe();
		float dot = vHiterToTarget.GetNormalizedSafe() * vBehindTargetDir;
		if (dot <= cosineBehindTargetHalfAngleRadians)
		{
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_no_shield_hited, 0.3f);
			NVcStartSpecialAction(H_actorId, itm_id_cur_hiter, eSpec_act_combat_hit_to_block_no_shield, 0.8f);
			if (pCAITarget->GetStamina() > 5)
			{
				pCAITarget->SetStamina(pCAITarget->GetStamina() - (damage*pCAITarget->stamina_cons_block_ml));
				NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_safe_no_shield, 0.3f);
				return false;
			}
			else
			{
				NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_is_broken_no_shield, 1.9f);
				return true;
			}
		}
	}
	else if (pCAITarget->m_blockactiveshield)
	{
		float behindTargetAngleDegrees = 175.0f;
		Vec3 v3HiterToTarget = pCAIHiter->GetEntity()->GetWorldPos() - pCAITarget->GetEntity()->GetWorldPos();
		Vec2 vHiterToTarget(v3HiterToTarget);
		float cosineBehindTargetHalfAngleRadians = cos(DEG2RAD(behindTargetAngleDegrees / 2.0f));
		Vec2 vBehindTargetDir = -Vec2(pCAITarget->GetAnimatedCharacter()->GetAnimLocation().GetColumn1()).GetNormalizedSafe();
		float dot = vHiterToTarget.GetNormalizedSafe() * vBehindTargetDir;
		if (dot <= cosineBehindTargetHalfAngleRadians)
		{
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_with_shield_hited, 0.5f);
			NVcStartSpecialAction(H_actorId, itm_id_cur_hiter, eSpec_act_combat_hit_to_block_with_shield, 0.8f);
			if (pCAITarget->GetStamina()>5)
			{
				pCAITarget->SetStamina(pCAITarget->GetStamina() - (damage*pCAITarget->stamina_cons_block_ml));
				NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_safe_with_shield, 0.5f);
				return false;
			}
			else
			{
				NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_is_broken_with_shield, 1.9f);
				return true;
			}
		}
	}

	return true;
}

bool CActorActionsNew::TestHitOnActorEquippedItem(EntityId T_actorId, int damage)
{
	CActor* pCAITarget = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(T_actorId));
	if (!pCAITarget)
		return false;

	EntityId itm_id_cur_target = 0;

	CItem* pCurItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAITarget->GetCurrentItemId()));
	if (pCurItemTarget)
		itm_id_cur_target = pCurItemTarget->GetEntityId();

	if (pCAITarget->m_blockactivenoshield)
	{
		NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_no_shield_hited, 0.3f);
		if (pCAITarget->GetStamina() > 5)
		{
			pCAITarget->SetStamina(pCAITarget->GetStamina() - (damage*pCAITarget->stamina_cons_block_ml));
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_safe_no_shield, 0.3f);
			return false;
		}
		else
		{
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_is_broken_no_shield, 1.9f);
			return true;
		}
	}
	else if (pCAITarget->m_blockactiveshield)
	{

		NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_with_shield_hited, 0.5f);
		if (pCAITarget->GetStamina() > 5)
		{
			pCAITarget->SetStamina(pCAITarget->GetStamina() - (damage*pCAITarget->stamina_cons_block_ml));
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_safe_with_shield, 0.5f);
			return false;
		}
		else
		{
			NVcStartSpecialAction(T_actorId, itm_id_cur_target, eSpec_act_combat_block_is_broken_with_shield, 1.9f);
			return true;
		}
	}
	return true;
}

void CActorActionsNew::BreakHitOnActorEquippedItem(EntityId T_actorId, EntityId itemId)
{

}

bool CActorActionsNew::AddSpecEffectFromWpnToTargetIfExist(EntityId T_actorId, EntityId H_actorId, Vec3 pos, Vec3 nrm)
{
	CActor* pCAITarget = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(T_actorId));
	if (!pCAITarget)
		return false;

	CActor* pCAIHiter = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(H_actorId));
	if (!pCAIHiter)
		return false;

	EntityId itm_id_cur_hiter = 0;
	EntityId itm_id_cur_target = 0;
	CItem* pCurItemHiter = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAIHiter->GetCurrentItemId()));
	if (pCurItemHiter)
		itm_id_cur_hiter = pCurItemHiter->GetEntityId();

	CItem* pCurItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAITarget->GetCurrentItemId()));
	if (pCurItemTarget)
		itm_id_cur_target = pCurItemTarget->GetEntityId();

	bool behind = true;
	float behindTargetAngleDegrees = 175.0f;
	Vec3 v3HiterToTarget = pCAIHiter->GetEntity()->GetWorldPos() - pCAITarget->GetEntity()->GetWorldPos();
	Vec2 vHiterToTarget(v3HiterToTarget);
	float cosineBehindTargetHalfAngleRadians = cos(DEG2RAD(behindTargetAngleDegrees / 2.0f));
	Vec2 vBehindTargetDir = -Vec2(pCAITarget->GetAnimatedCharacter()->GetAnimLocation().GetColumn1()).GetNormalizedSafe();
	float dot = vHiterToTarget.GetNormalizedSafe() * vBehindTargetDir;
	if (dot <= cosineBehindTargetHalfAngleRadians)
	{
		behind = false;
	}

	bool effect_added = false;

	for (int i = 0; i < 15; i++)
	{
		if (itm_id_cur_hiter > 0)
		{
			int ids_eff = pCurItemHiter->GetItemAddotionalEffect(i);
			if (ids_eff <= 0)
				continue;

			bool add_effect_to_trg = false;
			float percent_to_add_effect = 100.0f;
			if (pCAITarget->GetArmor() > 10)
				percent_to_add_effect = percent_to_add_effect / (pCAITarget->GetArmor() * 0.1f);

			percent_to_add_effect = percent_to_add_effect - (pCAITarget->GetAgility() * 0.4f);
			percent_to_add_effect = percent_to_add_effect + (pCAIHiter->GetAgility() * 0.6f);
			if (pCAITarget->m_blockactivenoshield)
			{
				if (behind)
				{
					add_effect_to_trg = true;
				}
				else
				{
					percent_to_add_effect *= 0.8f;
					if (itm_id_cur_target > 0)
					{
						float dmg_coof = (pCurItemTarget->GetDmgCurrent() + pCurItemTarget->GetDmgAdded())*0.2;
						if (dmg_coof > 50.0f)
							dmg_coof = 50.0f;

						percent_to_add_effect = percent_to_add_effect - dmg_coof;
					}

					add_effect_to_trg = true;
				}
			}
			else if (pCAITarget->m_blockactiveshield)
			{
				if (behind)
				{
					add_effect_to_trg = true;
				}
				else
				{
					bool is_shield = true;
					CItem* pCurTargetShield = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAITarget->GetCurrentEquippedItemId(eAESlot_Shield)));
					if (!pCurTargetShield)
					{
						pCurTargetShield = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pCAITarget->GetCurrentEquippedItemId(eAESlot_Torch)));
						if (pCurTargetShield)
							is_shield = false;
					}

					if (pCurTargetShield)
					{
						if (is_shield)
						{
							percent_to_add_effect *= 0.4f;
							float arm_coof = pCurTargetShield->GetArmorRating()*0.55;
							if (arm_coof > 75.0f)
								arm_coof = 75.0f;

							percent_to_add_effect = percent_to_add_effect - arm_coof;
						}
						else
						{
							percent_to_add_effect *= 0.9f;
							float dmg_coof = (pCurTargetShield->GetDmgCurrent() + pCurTargetShield->GetDmgAdded())*0.1;
							if (dmg_coof > 25.0f)
								dmg_coof = 25.0f;

							percent_to_add_effect = percent_to_add_effect - dmg_coof;
						}
					}
					add_effect_to_trg = true;
				}
			}
			else
			{
				if (behind)
				{
					percent_to_add_effect *= 1.2f;
					if (pCAITarget->GetRelaxedMod())
						percent_to_add_effect *= 1.4f;

					add_effect_to_trg = true;
				}
				else
				{
					add_effect_to_trg = true;
				}
			}

			if (add_effect_to_trg)
			{
				float ch_val_c = cry_random(0.0f, 100.0f);
				if (ch_val_c <= percent_to_add_effect)
				{
					if (!pos.IsZero())
					{
						pCAITarget->SetLastDamagePosAndNormal(pos, nrm);
					}
					pCAITarget->AddBuff(ids_eff, 0.0f);
					effect_added = true;
				}
			}
		}
	}
	return effect_added;
}

void CActorActionsNew::NVcStartSpecialAction(EntityId actorId, EntityId itemId, ESpecialActions special_action, float sa_time)
{
	CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId));
	if (!pActor)
		return;

	CItem* pCurItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pCurItem)
		return;

	if (pActor->GetSAPlayTime() > 0.0f)
	{
		if (pActor->GetSACurrent() == eSpec_act_relaxed_mode_on)
		{
			if (g_pGameCVars->i_player_wpn_attcon_alternative == 1)
			{
				pActor->SetSAPlayTime(0.0f);
				pActor->SetSACurrent(eSpec_act_none);
				const ItemString &s_Action = "weapon_on_back_stop";
				FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
				pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
				pActor->DeleteBoneAttachment(0, "wpn_ssd_h", true);
				pActor->CreateBoneAttachment(0, "wpn_ssd_nf", pCurItem->GetEntity()->GetStatObj(1)->GetFilePath(), "weaponPos_rifle01", false, false);
			}
		}
		else if (pActor->GetSACurrent() == eSpec_act_relaxed_mode_off)
		{
			pActor->SetSAPlayTime(0.0f);
			pActor->SetSACurrent(eSpec_act_none);
			const ItemString &s_Action = "weapon_on_back_stop";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			if (g_pGameCVars->i_player_wpn_attcon_alternative == 1)
			{
				pActor->DeleteBoneAttachment(0, "wpn_ssd_nf", true);
			}
		}
	}

	if (special_action == eSpec_act_relaxed_mode_on && pActor->GetSAPlayTime() <= 0.0f)
	{
		pActor->SetSAPlayTime(sa_time);
		pActor->SetSACurrent(special_action);
		if (g_pGameCVars->i_player_wpn_attcon_alternative == 1)
		{
			pActor->CreateBoneAttachment(0, "wpn_ssd_h", pCurItem->GetEntity()->GetStatObj(1)->GetFilePath(), "weapon_bone", false, false);
		}
		pActor->SetItemOnBackId(itemId);
		const ItemString &s_Action = "weapon_on_back_on";
		FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
		pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}
	else if (special_action == eSpec_act_relaxed_mode_off && pActor->GetSAPlayTime() <= 0.0f)
	{
		pActor->SetSAPlayTime(sa_time);
		pActor->SetSACurrent(special_action);
		const ItemString &s_Action = "weapon_on_back_off";
		FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
		pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		if (g_pGameCVars->i_player_wpn_attcon_alternative == 1)
		{
			pActor->DeleteBoneAttachment(0, "wpn_ssd_h", true);
		}
	}
	else if (special_action == 3 && pActor->GetSAPlayTime() <= 0.0f)
	{
		pActor->SetSAPlayTime(sa_time);
		pActor->SetSACurrent(special_action);
	}
	else if (special_action == 39 && pActor->GetSAPlayTime() <= 0.0f)
	{
		pActor->SetSAPlayTime(sa_time);
	}

	switch (special_action)
	{
		case eSpec_act_combat_block_no_shield_hited:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_no_shield_hited";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_block_is_broken_no_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_is_broken_no_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			pCurItem->SetItemFlag(CItem::eIF_BlockActions, false);
			pActor->m_blockactivenoshield = false;
			pActor->m_blockactiveshield = false;
			pActor->SetBlockActive(3, false);
			break;
		}
		case eSpec_act_combat_hit_to_block_no_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_hit_to_block_no_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_block_safe_no_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_safe_no_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_block_with_shield_hited:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_with_shield_hited";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_hit_to_block_with_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_hit_to_block_with_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_block_is_broken_with_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_is_broken_with_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			pCurItem->SetItemFlag(CItem::eIF_BlockActions, false);
			pActor->m_blockactivenoshield = false;
			pActor->m_blockactiveshield = false;
			pActor->SetBlockActive(3, false);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_block_safe_with_shield:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			const ItemString &s_Action = "combat_block_safe_with_shield";
			FragmentID fragmentId = pCurItem->GetFragmentID(s_Action.c_str());
			pCurItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
			break;
		}
		case eSpec_act_combat_push_recoil:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
				break;
		}
		case eSpec_act_combat_recoil:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			break;
		}
		case eSpec_act_combat_falling:
		{
			pActor->SetSAPlayTime(sa_time);
			pActor->SetSACurrent(special_action);
			break;
		}
		break;
	}
}

void CActorActionsNew::StartFistsMeleeAction(EntityId actorId, EntityId itemId, const char *action, float delay, bool inp_block, float pwr, float rec, int num_rays, float range)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetStamina() < (pActor->stamina_cons_atk_base * pActor->stamina_cons_atk_ml))
	{
		if (g_pGameCVars->g_stamina_sys_can_melee_attack_if_low_stmn_val == 0)
			return;
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		return;

	pWpn->RequireUpdate(eIUS_General);
	pWpn->SpecialMeleeRequestDelayedSimpleAttack(delay, pwr, rec, num_rays, range);
	const ItemString &meleeAction = action;
	FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	//net sync
	NetRequestPlayItemAction(actorId, fragmentId);

	if (inp_block)
		pItem->SetItemFlag(CItem::eIF_BlockActions, true);

	pWpn->OnMeleeNw(actorId);
}

void CActorActionsNew::StartBlockActionAttack(EntityId actorId, EntityId itemId, float delay, bool simple, float pwr, float rec, int num_rays, bool attack_pref_left_item)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (!pActor->m_blockactiveshield && !pActor->m_blockactivenoshield)
		return;

	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	if (pActor->m_blockactiveshield)
	{
		pWpn->RequireUpdate(eIUS_General);
		if (simple)
		{
			CItem *pItem_left = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Shield)));
			if (!pItem_left)
				pItem_left = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torch)));

			if (pItem_left && attack_pref_left_item)
			{
				const ItemString &meleeAction = "left_item_block_attack";
				FragmentID fragmentId = pItem_left->GetFragmentID(meleeAction.c_str());
				pItem_left->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
				float dmg = ((pItem_left->GetDmgCurrent()*0.1) + (pActor->GetActorStrength()) / 4.0f) / 2.0f;
				pWpn->SpecialMeleeRequestDelayedSimpleAttack(delay, dmg, rec, num_rays);
				pWpn->OnMeleeNw(actorId);
				return;
			}
			const ItemString &meleeAction = "rigth_item_block_attack";
			FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
			pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//pItem->SetItemFlag(CItem::eIF_BlockActions, true);
			float dmg = ((pWpn->GetDmgCurrent()*0.4) + (pActor->GetActorStrength()) / 4.0f) / 2.0f;
			pWpn->SpecialMeleeRequestDelayedSimpleAttack(delay, dmg, rec, num_rays);
			pWpn->OnMeleeNw(actorId);
		}
	}
	else if (pActor->m_blockactivenoshield)
	{
		pWpn->RequireUpdate(eIUS_General);
		if (simple)
		{
			CItem *pItem_left = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Weapon_Left)));
			if (pItem_left && attack_pref_left_item)
			{
				CWeapon *pWpn_left = pActor->GetWeapon(pActor->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
				if (pWpn_left)
				{
					const ItemString &meleeAction = "left_item_block_attack";
					FragmentID fragmentId = pItem_left->GetFragmentID(meleeAction.c_str());
					pItem_left->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
					//pItem_left->SetItemFlag(CItem::eIF_BlockActions, true);
					float dmg = ((pWpn_left->GetDmgCurrent()*0.2) + (pActor->GetActorStrength()) / 4.0f) / 2.0f;
					pWpn_left->SpecialMeleeRequestDelayedSimpleAttack(delay, dmg, rec, num_rays);
					pWpn_left->OnMeleeNw(actorId);
					return;
				}
			}
			const ItemString &meleeAction = "rigth_item_block_attack";
			FragmentID fragmentId = pItem->GetFragmentID(meleeAction.c_str());
			pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//pItem->SetItemFlag(CItem::eIF_BlockActions, true);
			float dmg = ((pWpn->GetDmgCurrent()*0.4) + (pActor->GetActorStrength())/4.0f) / 2.0f;
			pWpn->SpecialMeleeRequestDelayedSimpleAttack(delay, dmg, rec, num_rays);
			pWpn->OnMeleeNw(actorId);
		}
	}
}

void CActorActionsNew::StartMeleeWeaponAttack(EntityId actorId, int spc_nv)
{
	//CryLogAlways("CActorActionsNew::StartMeleeWeaponAttack called");
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	//CryLogAlways("CActorActionsNew::StartMeleeWeaponAttack called 2");
	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	if (pActor->GetCurrentItemId() == pActor->GetNoWeaponId())
	{
		if (!ActorCanDoAnyAgressiveActions(pActor->GetEntityId(), pActor->GetNoWeaponId()))
			return;
		
		if (pWpn->spec_melee_attack_simple_delay == 0.0f && pWpn->spec_melee_attack_simple_recoil == 0.0f)
		{
			string anim_actions[4];
			anim_actions[0] = "melee_fists_attack_rgt_upperc";
			anim_actions[1] = "melee_fists_attack_rgt";
			anim_actions[2] = "melee_fists_attack_lgt_upperc";
			anim_actions[3] = "melee_fists_attack_lgt";
			int anim_act = cry_random(0, 3);
			if (pActor->m_rgt_hand_blc && anim_act < 2)
				return;

			if (pActor->m_lft_hand_blc && anim_act >= 2)
				return;

			StartFistsMeleeAction(pActor->GetEntityId(), pActor->GetNoWeaponId(), anim_actions[anim_act], 0.2f, true,
				cry_random(6.0f, 10.0f), 0.5f, 7);
		}
		return;
	}

	//CryLogAlways("CActorActionsNew::StartMeleeWeaponAttack called 3");

	if (spc_nv == 0)
	{
		if (pWpn->CanMeleeAttack())
		{
			//CryLogAlways("CActorActionsNew::StartMeleeWeaponAttack melee request");
			pWpn->MeleeAttack();
		}

		//CryLogAlways("CActorActionsNew::StartMeleeWeaponAttack called end");
	}
	else if (spc_nv == 1)
	{
		if (pActor->m_blockactiveshield)
		{
			CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Shield)));
			if (!pItem)
				pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Torch)));

			if (pItem)
			{
				StopMeleeBlockAction(actorId, pWpn->GetEntityId());
				pWpn->SpecialMeleeRequestDelayedSimpleAttack(0.2f, pItem->GetDmgCurrent(), 0.4f, 7);
				const ItemString &meleeAction = "melee_shield_attack";
				FragmentID fragmentId = pWpn->GetFragmentID(meleeAction.c_str());
				pWpn->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
				//net sync
				NetRequestPlayItemAction(actorId, fragmentId);
			}
		}
		else if (pActor->m_blockactivenoshield)
		{
			StopMeleeBlockAction(actorId, pWpn->GetEntityId());
			pWpn->SpecialMeleeRequestDelayedSimpleAttack(0.2f, 0.0f, 0.4f, 7);
			const ItemString &meleeAction = "melee_block_wpn_attack";
			FragmentID fragmentId = pWpn->GetFragmentID(meleeAction.c_str());
			pWpn->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync
			NetRequestPlayItemAction(actorId, fragmentId);
		}
	}
}

void CActorActionsNew::StartWeaponAttackSimple(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	if (pWpn->CanFire())
	{
		if(pWpn->GetWeaponType() != 9)
			pWpn->StartFire();
		else
		{
			SProjectileLaunchParams launchParams;
			launchParams.fSpeedScale = 66.0f;
			launchParams.vShootTargetPos = Vec3(0, 0, 0);
			launchParams.trackingId = 0;
			pWpn->StartFire(launchParams);
			//temp fix---------------------------------------------------
			if (pActor->attack_timer_smpl <= 0.0f)
				pActor->attack_timer_smpl = cry_random(1.3f, 6.0f);
			//-----------------------------------------------------------
		}
	}
}

void CActorActionsNew::StopWeaponAttackSimple(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	pWpn->StopFire();
}

void CActorActionsNew::StartSpellCastSimple(EntityId actorId, int sc_slot, int sc_slot_forced)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CActorSpellsActions *pSpellAction = pActor->m_pActorSpellsActions;
	if (!pSpellAction)
		return;

	pSpellAction->StartCast(sc_slot, sc_slot_forced);
}

void CActorActionsNew::StopSpellCastSimple(EntityId actorId, int sc_slot, bool forced)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CActorSpellsActions *pSpellAction = pActor->m_pActorSpellsActions;
	if (!pSpellAction)
		return;

	pSpellAction->RequestToPreEndCast(sc_slot, forced);
}

void CActorActionsNew::SelectAnySpellFromSbToSlotSimple(EntityId actorId, int sc_slot)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pActor->m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pActor->m_actor_spells_book.end();
	int count = pActor->m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).spell_id > -1)
		{
			int stop = cry_random(0, 6);
			if (stop > 4)
			{
				pActor->AddSpellToCurrentSpellsForUseSlot((*it).spell_id, sc_slot);
				break;
			}
			else
			{
				if ((count - 1) == i)
				{
					pActor->AddSpellToCurrentSpellsForUseSlot((*it).spell_id, sc_slot);
					break;
				}
			}
		}
	}
}

void CActorActionsNew::SwitchToWeaponInInventory(EntityId actorId, const char *item_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (!pActor->GetInventory())
		return;

	IEntityClass *pWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(item_name);
	EntityId WeaponId = pActor->GetInventory()->GetItemByClass(pWeaponClass);
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(WeaponId));
	if (!pItem)
		return;

	if (!pItem->IsSelected())
		pActor->ScheduleItemSwitch(WeaponId, true);
}

bool CActorActionsNew::SwitchToAnyMeleeWeapon(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (!pActor->GetInventory())
		return false;

	CItem *pItem_wpn_s = pActor->GetItem(pActor->GetCurrentItemId());
	if (pItem_wpn_s)
	{
		if (pItem_wpn_s->GetItemType() != 81)
		{
			CWeapon *pWpn = pActor->GetWeapon(pItem_wpn_s->GetEntityId());
			if (pWpn && pWpn->IsMeleeWpn())
			{
				if(pItem_wpn_s->GetItemHealth() > 10.0f)
					return true;
			}

			if (pItem_wpn_s->GetWeaponType() == 9)
			{
				//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItem_wpn_s->GetEntityId(), "Bow"));
				CBow *pBow = static_cast<CBow*>(pItem_wpn_s);
				if (pBow)
				{
					if (pBow->process_arrow_load_started || pBow->hold_started)
						return false;
					else
						pBow->DeleteArrowAttachment();
				}
			}

			if (pItem_wpn_s->IsBusy() || pItem_wpn_s->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
		}
	}

	int num_of_wpns_in_in_of_type = GetItemsNumInInvByWpnType(actorId, true);
	bool selected = false;
	IInventory *pInventory = pActor->GetInventory();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId id = pInventory->GetItem(i);
		CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (curItem)
		{
			if (pItem_wpn_s && pItem_wpn_s->GetEntityId() == id)
				continue;

			if (curItem->GetItemType() == 81)
				continue;

			if (curItem->GetWeaponType() != 9 && curItem->GetWeaponType() > 0)
			{
				if (!curItem->IsSelected())
				{
					num_of_wpns_in_in_of_type -= 1;
					if (i < pInventory->GetCount())
					{
						float rnd_vlc = cry_random(0.0f, 2.0f);
						if (rnd_vlc > 0.9f)
						{
							pActor->ScheduleItemSwitch(id, true);
							selected = true;
							break;
						}
					}
					else if (i == pInventory->GetCount())
					{
						pActor->ScheduleItemSwitch(id, true);
						selected = true;
						break;
					}
					else if (num_of_wpns_in_in_of_type <= 0)
					{
						pActor->ScheduleItemSwitch(id, true);
						selected = true;
						break;
					}
				}
			}
		}
	}
	return selected;
}

bool CActorActionsNew::SwitchToAnyWeapon(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (!pActor->GetInventory())
		return false;

	CItem *pItem_wpn_s = pActor->GetItem(pActor->GetCurrentItemId());
	if (pItem_wpn_s)
	{
		if (pItem_wpn_s->GetItemType() != 81)
		{
			if (pItem_wpn_s->GetWeaponType() == 9)
			{
				//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItem_wpn_s->GetEntityId(), "Bow"));
				CBow *pBow = static_cast<CBow*>(pItem_wpn_s);
				if (pBow)
				{
					if (pBow->process_arrow_load_started || pBow->hold_started)
						return false;
					else
						pBow->DeleteArrowAttachment();
				}
			}

			if (pItem_wpn_s->IsBusy() || pItem_wpn_s->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
		}
	}

	bool selected = false;
	IInventory *pInventory = pActor->GetInventory();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId id = pInventory->GetItem(i);
		CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (curItem)
		{
			if (curItem->GetItemType() == 81)
				continue;

			if (curItem->GetWeaponType() > 0)
			{
				if (!curItem->IsSelected())
				{
					if (i < pInventory->GetCount())
					{
						float rnd_vlc = cry_random(0.0f, 2.0f);
						if (rnd_vlc > 0.9f)
						{
							pActor->ScheduleItemSwitch(id, true);
							selected = true;
							break;
						}
					}
					else if (i == pInventory->GetCount())
					{
						pActor->ScheduleItemSwitch(id, true);
						selected = true;
						break;
					}
				}
			}
		}
	}
	return selected;
}

bool CActorActionsNew::SwitchToAnyRangeWeapon(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (!pActor->GetInventory())
		return false;

	CItem *pItem_wpn_s = pActor->GetItem(pActor->GetCurrentItemId());
	if (pItem_wpn_s)
	{
		if (pItem_wpn_s->GetWeaponType() == 9 || pItem_wpn_s->GetWeaponType() == 11)
		{
			if (pItem_wpn_s->GetItemHealth() > 10.0f)
				return true;
		}
		if (pItem_wpn_s->GetItemType() != 81)
		{
			if (pItem_wpn_s->GetWeaponType() == 9)
			{
				//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItem_wpn_s->GetEntityId(), "Bow"));
				CBow *pBow = static_cast<CBow*>(pItem_wpn_s);
				if (pBow)
				{
					if (pBow->process_arrow_load_started || pBow->hold_started)
						return false;
					else
						pBow->DeleteArrowAttachment();
				}
			}

			if (pItem_wpn_s->IsBusy() || pItem_wpn_s->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
		}
	}

	int num_of_wpns_in_in_of_type = GetItemsNumInInvByWpnType(actorId, false);
	bool selected = false;
	IInventory *pInventory = pActor->GetInventory();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId id = pInventory->GetItem(i);
		CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (curItem)
		{
			if (curItem->GetItemType() == 81)
				continue;

			if (curItem->GetWeaponType() == 9 && curItem->GetWeaponType() > 0)
			{
				if (!curItem->IsSelected())
				{
					num_of_wpns_in_in_of_type -= 1;
					if (i < pInventory->GetCount())
					{
						float rnd_vlc = cry_random(0.0f, 2.0f);
						if (rnd_vlc > 0.9f)
						{
							pActor->ScheduleItemSwitch(id, true);
							selected = true;
							break;
						}
					}
					else if (i == pInventory->GetCount())
					{
						pActor->ScheduleItemSwitch(id, true);
						selected = true;
						break;
					}
					else if (num_of_wpns_in_in_of_type <= 0)
					{
						pActor->ScheduleItemSwitch(id, true);
						selected = true;
						break;
					}
				}
			}
		}
	}
	return selected;
}

bool CActorActionsNew::SwitchToNoWpn(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (!pActor->GetInventory())
		return false;

	IInventory *pInventory = pActor->GetInventory();
	IEntityClass *pNoWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("NoWeapon");
	EntityId noWeaponId = NULL;
	noWeaponId = pInventory->GetItemByClass(pNoWeaponClass);
	if (noWeaponId != NULL && pActor->GetCurrentItemId() != noWeaponId)
	{
		pActor->ScheduleItemSwitch(noWeaponId, false);
		return true;
	}

	return false;
}

bool CActorActionsNew::CheckCurrentWeaponAmmo(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!curItem)
		return false;

	if (curItem->GetWeaponType() > 0)
	{
		if (curItem->GetWeaponType() == 9)
		{
			EntityId id_ammo = pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows);
			if (id_ammo > 0)
			{
				CItem *ammo_item = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id_ammo));
				if (ammo_item)
				{
					if (ammo_item->GetItemQuanity() > 0)
						return true;
					else
						return false;
				}
			}
		}
	}
	return false;
}

int CActorActionsNew::CheckCurrentWpnType(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return 0;

	CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!curItem)
		return 0;

	return curItem->GetWeaponType();
}

int CActorActionsNew::IsAITargetHaveAndVisibleOrObstr(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return eAITVS_none;

	Vec3 targ_pos(0, 0, 0);
	IAIActor *pActII = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
	if (pActII)
	{
		if (pActII->GetAttentionTarget())
		{
			targ_pos = pActII->GetAttentionTarget()->GetPos();
		}
	}

	if (!targ_pos.IsZero())
	{
		Vec3 test_dir = Vec3(ZERO);
		Vec3 head_position = Vec3(ZERO);
		head_position = pActor->GetBonePosition("Bip01 Head");
		if (head_position.IsZero())
		{
			if (pActor->GetEntity()->GetCharacter(0))
			{
				head_position = pActor->GetEntity()->GetCharacter(0)->GetAABB().GetCenter();
			}
			else
			{
				head_position = pActor->GetEntity()->GetPos();
				head_position.z += 0.1f;
			}
		}
		test_dir = targ_pos - head_position;
		test_dir.z += 0.8f;
		ray_hit hit;
		IPhysicalEntity *pEnt_to_ignore = pActor->GetEntity()->GetPhysics();
		IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
		int num_ents_to_ignore = 0;
		if (pEnt_to_ignore)
		{
			pEnts_to_ignore[num_ents_to_ignore] = pEnt_to_ignore;
			num_ents_to_ignore += 1;
		}

		ICharacterInstance* pCharacter_player = pActor->GetEntity()->GetCharacter(0);
		if (pCharacter_player)
		{
			IAttachmentManager* pAm = pCharacter_player->GetIAttachmentManager();
			if (pAm)
			{

				IAttachment *ptTa = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att1_to_ignore->GetString());
				if (ptTa)
				{
					CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa->GetIAttachmentObject();
					if (entAttachment)
					{
						IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
						if (pEnttoignore2)
						{
							IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
							if (pEnt2toignore2)
							{
								pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
								num_ents_to_ignore += 1;
								//CryLogAlways("IsAITargetHaveAndVisibleOrObstr GetInterfaceByName(headede) true");
							}
						}
					}
				}
				IAttachment *ptTa2 = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att2_to_ignore->GetString());
				if (ptTa2)
				{
					CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa2->GetIAttachmentObject();
					if (entAttachment)
					{
						IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
						if (pEnttoignore2)
						{
							IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
							if (pEnt2toignore2)
							{
								pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
								num_ents_to_ignore += 1;
							}
						}
					}
				}
			}
		}

		if (!gEnv->pPhysicalWorld)
			return eAITVS_none;

		bool ai_vts_priority[eAITVS_full];
		for (int i = 0; i < eAITVS_full; i++)
		{
			ai_vts_priority[i] = false;
		}

		const int num_rtes = 25;
		EntityId TargetId[num_rtes];
		EntityId ObctructedTargetId[num_rtes];
		EntityId ActorTargetIdCl = 0;
		EntityId ObctructedTargetIdCl = 0;
		for (int i = 0; i < num_rtes; i++)
		{
			TargetId[i] = 0;
			ObctructedTargetId[i] = 0;
			Vec3 temp_test_dir = test_dir;
			if (i == 0)
			{
				gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir*1.3f, ent_all,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
			}
			else
			{
				temp_test_dir = (targ_pos + Vec3(cry_random(-0.9f, 0.9f), cry_random(-0.9f, 0.9f), cry_random(-0.9f, 0.9f))) - head_position;
				gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir*1.3f, ent_all,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
			}

			IPhysicalEntity *pCollider = hit.pCollider;
			if (pCollider)
			{
				IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
				if (pCollidedEntity)
				{
					CItem *collItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
					if (collItem)
					{
						if (collItem->GetOwner())
						{
							pCollidedEntity = collItem->GetOwner();
							collidedEntityId = collItem->GetOwnerId();
						}
					}
					float lc_lengt = 0.0f;
					AABB bbox;
					pCollidedEntity->GetWorldBounds(bbox);
					Vec3 target_cntr = bbox.GetCenter();
					if (!target_cntr.IsZero())
						lc_lengt = target_cntr.GetDistance(hit.pt);

					if (lc_lengt > 0.3f)
					{
						Vec3 temp_test_dir2 = target_cntr - head_position;
						ray_hit hit2;
						gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir2*1.3f, ent_all,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit2, 1, pEnts_to_ignore, num_ents_to_ignore);

						IPhysicalEntity *pvCollider = hit2.pCollider;
						if (pvCollider)
						{
							IEntity* pvCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pvCollider);
							EntityId collidedcvEntityId = pvCollidedEntity ? pvCollidedEntity->GetId() : 0;
							if (collidedcvEntityId != collidedEntityId)
							{
								ai_vts_priority[eAITVS_have_obstructed_target] = true;
								ObctructedTargetId[i] = collidedEntityId;
								//return eAITVS_have_obstructed_target;
							}
						}
						else
						{
							ai_vts_priority[eAITVS_have_obstructed_target] = true;
							ObctructedTargetId[i] = collidedEntityId;
							//return eAITVS_have_obstructed_target;
						}
					}
					uint8	m_localPlayerFaction;
					uint8	m_mpActorFaction;
					IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
					m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
					IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
					m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
					CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
					if (!pTargetActor)
					{
						if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
						{
							ai_vts_priority[eAITVS_have_danger_entity_target] = true;
							TargetId[i] = collidedEntityId;
							//return eAITVS_have_danger_entity_target;
						}
						else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral ||
							gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
						{
							ai_vts_priority[eAITVS_have_simple_entity_target] = true;
							TargetId[i] = collidedEntityId;
							//return eAITVS_have_simple_entity_target;
						}
						continue;
					}

					if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
					{
						ai_vts_priority[eAITVS_have_actor_target] = true;
						TargetId[i] = collidedEntityId;
						//return eAITVS_have_actor_target;
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral)
					{
						ai_vts_priority[eAITVS_have_frendly_actor_target] = true;
						TargetId[i] = collidedEntityId;
						//return eAITVS_have_frendly_actor_target;
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
					{
						ai_vts_priority[eAITVS_have_frendly_actor_target] = true;
						TargetId[i] = collidedEntityId;
						//return eAITVS_have_frendly_actor_target;
					}
				}
			}
		}
		//distance process, select closect distance
		float old_obj_distance = 10000.0f;
		for (int i = 0; i < num_rtes; i++)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(TargetId[i]);
			if (!pCollidedEntity)
				continue;

			uint8	m_localPlayerFaction;
			uint8	m_mpActorFaction;
			IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) != IFactionMap::Hostile)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					continue;
				}
			}
			float this_dist = pActor->GetEntity()->GetPos().GetDistance(pCollidedEntity->GetPos());
			if (this_dist < old_obj_distance)
			{
				old_obj_distance = this_dist;
				ActorTargetIdCl = TargetId[i];
			}
		}
		old_obj_distance = 10000.0f;
		for (int i = 0; i < num_rtes; i++)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(ObctructedTargetId[i]);
			if (!pCollidedEntity)
				continue;

			uint8	m_localPlayerFaction;
			uint8	m_mpActorFaction;
			IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) != IFactionMap::Hostile)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					continue;
				}
			}
			float this_dist = pActor->GetEntity()->GetPos().GetDistance(pCollidedEntity->GetPos());
			if (this_dist < old_obj_distance)
			{
				old_obj_distance = this_dist;
				ObctructedTargetIdCl = ObctructedTargetId[i];
			}
		}

		IEntity* pCLEntity = gEnv->pEntitySystem->GetEntity(ActorTargetIdCl);
		IEntity* pCLEntityObstr = gEnv->pEntitySystem->GetEntity(ObctructedTargetIdCl);
		if (pCLEntity && !pCLEntityObstr)
		{
			CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pCLEntity->GetId());
			if (pTargetActor)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					if (pTargetActor->IsDead())
						return eAITVS_have_dead_actor_target;
					else
						return eAITVS_have_actor_target;
				}
				else if (ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
					return eAITVS_have_frendly_actor_target;
			}
			else
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
					return eAITVS_have_danger_entity_target;
				else if (ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
					return eAITVS_have_simple_entity_target;
			}
		}
		else if (pCLEntity && pCLEntityObstr)
		{
			float lnt_dist = pCLEntity->GetPos().GetDistance(pActor->GetEntity()->GetPos());
			float lnt_dist2 = pCLEntityObstr->GetPos().GetDistance(pActor->GetEntity()->GetPos());
			CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pCLEntity->GetId());
			if (lnt_dist < 0.8f || lnt_dist < 0.8f)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					if (pTargetActor)
					{
						if (pTargetActor->IsDead())
							return eAITVS_have_dead_actor_target;
						else
							return eAITVS_have_actor_target;
					}
					else
					{
						return eAITVS_have_danger_entity_target;
					}
				}
			}

			if (lnt_dist2 < lnt_dist)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
					return eAITVS_have_obstructed_target;
				else if(ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
					return eAITVS_have_obstructed_target;
			}
			else
			{
				if (pTargetActor)
				{
					if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
					{
						if (pTargetActor->IsDead())
							return eAITVS_have_dead_actor_target;
						else
							return eAITVS_have_actor_target;
					}
					else if (ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
						return eAITVS_have_frendly_actor_target;
				}
				else
				{
					if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
						return eAITVS_have_danger_entity_target;
					else if (ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
						return eAITVS_have_simple_entity_target;
				}
			}
		}
		else if (!pCLEntity && pCLEntityObstr)
		{ 
			if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				return eAITVS_have_obstructed_target;
			else if (ai_vts_priority[eAITVS_have_frendly_actor_target] || ai_vts_priority[eAITVS_have_simple_entity_target])
				return eAITVS_have_obstructed_target;
		}
	}
	else
	{
		return eAITVS_none;
	}
	return eAITVS_none;
}

EntityId  CActorActionsNew::GetAITargetFT(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return 0;

	Vec3 targ_pos(0, 0, 0);
	IAIActor *pActII = CastToIAIActorSafe(pActor->GetEntity()->GetAI());
	if (pActII)
	{
		if (pActII->GetAttentionTarget())
		{
			targ_pos = pActII->GetAttentionTarget()->GetPos();
		}
	}

	if (!targ_pos.IsZero())
	{
		Vec3 test_dir = Vec3(ZERO);
		Vec3 head_position = Vec3(ZERO);
		head_position = pActor->GetBonePosition("Bip01 Head");
		if (head_position.IsZero())
		{
			if (pActor->GetEntity()->GetCharacter(0))
			{
				head_position = pActor->GetEntity()->GetCharacter(0)->GetAABB().GetCenter();
			}
			else
			{
				head_position = pActor->GetEntity()->GetPos();
				head_position.z += 0.1f;
			}
		}
		test_dir = targ_pos - head_position;
		test_dir.z += 0.8f;
		ray_hit hit;
		IPhysicalEntity *pEnt_to_ignore = pActor->GetEntity()->GetPhysics();
		IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
		int num_ents_to_ignore = 0;
		if (pEnt_to_ignore)
		{
			pEnts_to_ignore[num_ents_to_ignore] = pEnt_to_ignore;
			num_ents_to_ignore += 1;
		}

		ICharacterInstance* pCharacter_player = pActor->GetEntity()->GetCharacter(0);
		if (pCharacter_player)
		{
			IAttachmentManager* pAm = pCharacter_player->GetIAttachmentManager();
			if (pAm)
			{

				IAttachment *ptTa = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att1_to_ignore->GetString());
				if (ptTa)
				{
					CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa->GetIAttachmentObject();
					if (entAttachment)
					{
						IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
						if (pEnttoignore2)
						{
							IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
							if (pEnt2toignore2)
							{
								pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
								num_ents_to_ignore += 1;
								//CryLogAlways("IsAITargetHaveAndVisibleOrObstr GetInterfaceByName(headede) true");
							}
						}
					}
				}
				IAttachment *ptTa2 = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att2_to_ignore->GetString());
				if (ptTa2)
				{
					CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa2->GetIAttachmentObject();
					if (entAttachment)
					{
						IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
						if (pEnttoignore2)
						{
							IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
							if (pEnt2toignore2)
							{
								pEnts_to_ignore[num_ents_to_ignore] = pEnt2toignore2;
								num_ents_to_ignore += 1;
							}
						}
					}
				}
			}
		}

		if (!gEnv->pPhysicalWorld)
			return 0;

		bool ai_vts_priority[eAITVS_full];
		for (int i = 0; i < eAITVS_full; i++)
		{
			ai_vts_priority[i] = false;
		}

		const int num_rtes = 25;
		EntityId TargetId[num_rtes];
		EntityId ObctructedTargetId[num_rtes];
		EntityId ActorTargetIdCl = 0;
		EntityId ObctructedTargetIdCl = 0;
		for (int i = 0; i < num_rtes; i++)
		{
			TargetId[i] = 0;
			ObctructedTargetId[i] = 0;
			Vec3 temp_test_dir = test_dir;
			if (i == 0)
			{
				gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir*1.3f, ent_all,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
			}
			else
			{
				temp_test_dir = (targ_pos + Vec3(cry_random(-0.9f, 0.9f), cry_random(-0.9f, 0.9f), cry_random(-0.9f, 0.9f))) - head_position;
				gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir*1.3f, ent_all,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
			}

			IPhysicalEntity *pCollider = hit.pCollider;
			if (pCollider)
			{
				IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
				if (pCollidedEntity)
				{
					CItem *collItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
					if (collItem)
					{
						if (collItem->GetOwner())
						{
							pCollidedEntity = collItem->GetOwner();
							collidedEntityId = collItem->GetOwnerId();
						}
					}
					float lc_lengt = 0.0f;
					AABB bbox;
					pCollidedEntity->GetWorldBounds(bbox);
					Vec3 target_cntr = bbox.GetCenter();
					if (!target_cntr.IsZero())
						lc_lengt = target_cntr.GetDistance(hit.pt);

					if (lc_lengt > 0.3f)
					{
						Vec3 temp_test_dir2 = target_cntr - head_position;
						ray_hit hit2;
						gEnv->pPhysicalWorld->RayWorldIntersection(head_position, temp_test_dir2*1.3f, ent_all,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit2, 1, pEnts_to_ignore, num_ents_to_ignore);

						IPhysicalEntity *pvCollider = hit2.pCollider;
						if (pvCollider)
						{
							IEntity* pvCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pvCollider);
							EntityId collidedcvEntityId = pvCollidedEntity ? pvCollidedEntity->GetId() : 0;
							if (collidedcvEntityId != collidedEntityId)
							{
								ai_vts_priority[eAITVS_have_obstructed_target] = true;
								ObctructedTargetId[i] = collidedEntityId;
							}
						}
						else
						{
							ai_vts_priority[eAITVS_have_obstructed_target] = true;
							ObctructedTargetId[i] = collidedEntityId;
						}
					}
					uint8	m_localPlayerFaction;
					uint8	m_mpActorFaction;
					IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
					m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
					IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
					m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
					CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
					if (!pTargetActor)
					{
						if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
						{
							ai_vts_priority[eAITVS_have_danger_entity_target] = true;
							TargetId[i] = collidedEntityId;
						}
						else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral ||
							gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
						{
							ai_vts_priority[eAITVS_have_simple_entity_target] = true;
							TargetId[i] = collidedEntityId;
						}
						continue;
					}

					if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
					{
						ai_vts_priority[eAITVS_have_actor_target] = true;
						TargetId[i] = collidedEntityId;
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Neutral)
					{
						ai_vts_priority[eAITVS_have_frendly_actor_target] = true;
						TargetId[i] = collidedEntityId;
					}
					else if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Friendly)
					{
						ai_vts_priority[eAITVS_have_frendly_actor_target] = true;
						TargetId[i] = collidedEntityId;
					}
				}
			}
		}
		//distance process, select closect distance
		float old_obj_distance = 10000.0f;
		for (int i = 0; i < num_rtes; i++)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(TargetId[i]);
			if (!pCollidedEntity)
				continue;

			uint8	m_localPlayerFaction;
			uint8	m_mpActorFaction;
			IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) != IFactionMap::Hostile)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					continue;
				}
			}
			float this_dist = pActor->GetEntity()->GetPos().GetDistance(pCollidedEntity->GetPos());
			if (this_dist < old_obj_distance)
			{
				old_obj_distance = this_dist;
				ActorTargetIdCl = TargetId[i];
			}
		}
		old_obj_distance = 10000.0f;
		for (int i = 0; i < num_rtes; i++)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(ObctructedTargetId[i]);
			if (!pCollidedEntity)
				continue;

			uint8	m_localPlayerFaction;
			uint8	m_mpActorFaction;
			IAIObject* pAIObjectMpActor = pCollidedEntity->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pActor->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) != IFactionMap::Hostile)
			{
				if (ai_vts_priority[eAITVS_have_actor_target] || ai_vts_priority[eAITVS_have_danger_entity_target])
				{
					continue;
				}
			}
			float this_dist = pActor->GetEntity()->GetPos().GetDistance(pCollidedEntity->GetPos());
			if (this_dist < old_obj_distance)
			{
				old_obj_distance = this_dist;
				ObctructedTargetIdCl = ObctructedTargetId[i];
			}
		}

		IEntity* pCLEntity = gEnv->pEntitySystem->GetEntity(ActorTargetIdCl);
		IEntity* pCLEntityObstr = gEnv->pEntitySystem->GetEntity(ObctructedTargetIdCl);
		if (pCLEntity && !pCLEntityObstr)
		{
			return ActorTargetIdCl;
		}
		else if (pCLEntity && pCLEntityObstr)
		{
			float lnt_dist = pCLEntity->GetPos().GetDistance(pActor->GetEntity()->GetPos());
			float lnt_dist2 = pCLEntityObstr->GetPos().GetDistance(pActor->GetEntity()->GetPos());
			CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pCLEntity->GetId());
			if (lnt_dist < 0.8f || lnt_dist < 0.8f)
			{
				return ActorTargetIdCl;
			}

			if (lnt_dist2 < lnt_dist)
			{
				return ObctructedTargetIdCl;
			}
			else
			{
				return ActorTargetIdCl;
			}
		}
		else if (!pCLEntity && pCLEntityObstr)
		{
			return ObctructedTargetIdCl;
		}
	}
	else
	{
		return 0;
	}
	return 0;
}

int CActorActionsNew::GetItemsNumInInvByWpnType(EntityId actorId, bool melee)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return 0;

	if (!pActor->GetInventory())
		return 0;

	int number_items = 0;
	IInventory *pInventory = pActor->GetInventory();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		EntityId id = pInventory->GetItem(i);
		CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (curItem)
		{
			if (curItem->GetItemType() == 81)
				continue;

			if (!curItem->IsSelected())
			{
				if (melee)
				{
					if (curItem->GetWeaponType() != 9 && curItem->GetWeaponType() > 0)
					{
						number_items++;
					}
				}
				else
				{
					if (curItem->GetWeaponType() == 9 && curItem->GetWeaponType() > 0)
					{
						number_items++;
					}
				}
			}
		}
	}
	return number_items;
}

void CActorActionsNew::EquipItemFromInventory(EntityId actorId, const char *item_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (!pActor->GetInventory())
		return;

	IEntityClass *pItemClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(item_name);
	EntityId ItemId = pActor->GetInventory()->GetItemByClass(pItemClass);
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ItemId));
	if (!pItem)
		return;

	if (!pItem->GetEquipped()==0)
		pActor->EquipItem(ItemId);
}

void CActorActionsNew::StartWeaponMeleeAttackComplex(EntityId actorId, float atk_time, float atk_delay, float atk_additive_damage, const char *m_action_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetStamina() < (pActor->stamina_cons_atk_base * pActor->stamina_cons_atk_ml))
	{
		if (g_pGameCVars->g_stamina_sys_can_melee_attack_if_low_stmn_val == 0)
			return;
	}

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	CWeapon *pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (!pWpn)
		return;

	if (pWpn->CanMeleeAttack())
	{
		pWpn->SpecialMeleeAttackWiSpDamage(atk_time, atk_delay, m_action_name, atk_additive_damage);
		pWpn->RequireUpdate(eIUS_General);
	}
}

void CActorActionsNew::PlaySimpleAnimAction(EntityId actorId, const char *action_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return;

	const ItemString &requestedAction = action_name;
	FragmentID fragmentId = pItem->GetFragmentID(requestedAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	//net sync
	NetRequestPlayItemAction(actorId, fragmentId);
}

bool CActorActionsNew::CanCastSpell(EntityId actorId, int sc_slot)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CActorSpellsActions *pSpellAction = pActor->m_pActorSpellsActions;
	if (!pSpellAction)
		return false;

	if(!pSpellAction->CanCastSpellFromSlot(sc_slot))
		return false;

	return true;
}

void CActorActionsNew::ForcedDisableBow(EntityId itemId)
{
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (pItem)
	{
		//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(itemId, "Bow"));
		CBow *pBow = static_cast<CBow*>(pItem);
		if (pBow)
		{
			pBow->ForcedStopFire();
		}
	}
}

void CActorActionsNew::StartPreLaunchWeapon(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (pItem->GetWeaponType() != 7)
		return;

	const ItemString &holdAction = "holding_wpn_pre";
	FragmentID fragmentId = pItem->GetFragmentID(holdAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	pItem->SetItemFlag(CItem::eIF_BlockActions, true);
}

void CActorActionsNew::StopPreLaunchWeaponAndLaunchThis(EntityId actorId, EntityId itemId, float pwr)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	bool view_slot = eIGS_FirstPerson;
	if (!pActor->IsPlayer())
		view_slot = eIGS_ThirdPerson;

	if (!pActor->IsThirdPerson())
		view_slot = eIGS_FirstPerson;

	if(pItem->GetWeaponType() != 7)
		return;

	const ItemString &holdAction = "holding_wpn_post";
	FragmentID fragmentId = pItem->GetFragmentID(holdAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	pItem->GetGameObject()->InvokeRMI(CItem::SvRequestLeaveModify(), CItem::EmptyParams(), eRMI_ToServer);
	pItem->SetItemFlag(CItem::eIF_BlockActions, false);
	if (IMovementController * pMC = pActor->GetMovementController())
	{
		Vec3 veloc(0, 0, 0);
		Vec3 direct(0, 0, 0);
		Vec3 posit(0, 0, 0);
		

		SMovementState info;
		pMC->GetMovementState(info);
		direct = info.fireDirection;
		
		CWeapon * pWpn = pActor->GetWeapon(itemId);
		if (pWpn)
		{
			IWeaponFiringLocator *pLocator = pWpn->GetFiringLocator();
			if (pLocator)
			{
				//pLocator->GetFiringVelocity(pWpn->GetEntityId(), this, veloc, direct);
			}
			posit = pItem->GetSlotHelperPos(view_slot, "weapond", true);
			CActor *pActor = pWpn->GetOwnerActor();
			if (pActor)
			{
				IPhysicalEntity *pPE = pActor->GetEntity()->GetPhysics();
				if (pPE)
				{
					pe_status_dynamics sv;
					if (pPE->GetStatus(&sv))
					{
						if (sv.v.len2()>0.01f)
						{
							float dot = max(sv.v.GetNormalized().Dot(direct), 0.0f);
							veloc =  sv.v*dot;
						}
					}
				}
			}
		}
		pItem->LaunchItem(pActor->GetStrength()+15.0f*pwr, info.fireDirection, posit, veloc);
	}
}

void CActorActionsNew::UpdateQuiverAttachment(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	string attachment_name = "arrow_quiver";
	if (itemId == pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts))
		attachment_name = "bolts_quiver";

	if (ICharacterInstance *pAttachmentObjectChar = pActor->GetAttachmentObjectCharacterInstance(0, attachment_name.c_str()))
	{
		if (pAttachmentObjectChar->GetIAttachmentManager())
		{
			int number_arrows = pItem->GetItemQuanity();
			if (number_arrows > 0)
			{
				if (number_arrows > 10)
					number_arrows = 10;

				for (int i = 0; i < number_arrows; i++)
				{
					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", i);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && !pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
						{
							CCGFAttachment *pCGFAttachment = new CCGFAttachment();
							pCGFAttachment->pObj = pStatObj;
							pAttachment_quiver_arrow->AddBinding(pCGFAttachment);
						}
					}
				}
			}
		}
	}
}

void CActorActionsNew::SimpleAttachItemToHand(EntityId actorId, EntityId itemId, bool attach, int hand)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	bool view_fp = true;
	if (!pActor->IsPlayer())
		view_fp = false;
	if (pActor->IsThirdPerson())
		view_fp = false;

	int view = 1;
	if (!view_fp)
	{
		view = 2;
	}

	if (attach)
	{
		if (pActor->IsPlayer())
			pItem->SetViewMode(0);

		pItem->SetHand(hand);
		pItem->HideItem(false);
		pItem->AttachToHand(true, false);
		//pItem->GetStats().selected = true;
		if (pActor->IsPlayer())
			pItem->SetViewMode(view);

		pItem->EnableUpdate(true, eIUS_General);
	}
	else
	{
		pItem->SetHand(hand);
		pItem->AttachToHand(false);
		if (pActor->IsPlayer())
			pItem->SetViewMode(0);

		pItem->Hide(true);
		pItem->EnableUpdate(false, eIUS_General);
		pItem->SetHand(0);
	}
}

bool CActorActionsNew::ActorCanDoAnyAgressiveActions(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->IsDead())
		return false;

	if (pActor->GetSAPlayTime() > 0.0f)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (itemId == 0)
	{
		if (!pActor->CanDoAnyActionInCastStateWithSWeapon(pActor->GetCurrentItemId()))
			return false;
	}
	else
	{
		if (!pActor->CanDoAnyActionInCastStateWithSWeapon(itemId))
			return false;
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	
	if (!pItem)
		return false;

	if (pItem->IsWpnTwoHanded())
	{
		if (!pActor->CanDoAnyActionWNeededTwoHands())
		{
			return false;
		}
	}

	if (pActor->GetSpecRecoilAct(eARMode_recoil_dodge) > 0.0f)
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return false;
		}

		float rc_time_et = (g_pGameCVars->g_dodge_delay_debug + g_pGameCVars->g_dodge_time_countine) - pActor->GetSpecRecoilAct(eARMode_recoil_dodge);
		if ((g_pGameCVars->g_dodge_time_countine + 0.1f) > rc_time_et)
			return false;
	}
	return true;
}
int CActorActionsNew::GetIgnoredPhyObjects(EntityId actorId, PhysIgnLst & skipList, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor) 
		return 0;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
	{
		pItem = pActor->GetItem(pActor->GetCurrentItemId());
	}

	if (!pItem)
		return 0;

	//CryLogAlways("CActorActionsNew::GetIgnoredPhyObjects called");

	IPhysicalEntity *pEnt_to_ignore = pActor->GetEntity()->GetPhysics();
	int num_ents_to_ignore = 0;
	if (pEnt_to_ignore)
	{
		stl::push_back_unique(skipList, pEnt_to_ignore);
		//skipList[num_ents_to_ignore] = pEnt_to_ignore;
		num_ents_to_ignore += 1;
	}

	if (IPhysicalEntity *pEnt_to_ignore23 = pItem->GetEntity()->GetPhysics())
	{
		stl::push_back_unique(skipList, pEnt_to_ignore23);
		//skipList[num_ents_to_ignore] = pEnt_to_ignore23;
		num_ents_to_ignore += 1;
	}

	CItem *pItem_left = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetAnyItemInLeftHand()));
	if (pItem_left)
	{
		if (IPhysicalEntity *pEnt_to_ignore23 = pItem_left->GetEntity()->GetPhysics())
		{
			stl::push_back_unique(skipList, pEnt_to_ignore23);
			//skipList[num_ents_to_ignore] = pEnt_to_ignore23;
			num_ents_to_ignore += 1;
		}
	}

	ICharacterInstance* pCharacter_player = pActor->GetEntity()->GetCharacter(0);
	if (pCharacter_player)
	{
		IAttachmentManager* pAm = pCharacter_player->GetIAttachmentManager();
		if (pAm)
		{

			IAttachment *ptTa = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att1_to_ignore->GetString());
			if (ptTa)
			{
				CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa->GetIAttachmentObject();
				if (entAttachment)
				{
					IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
					if (pEnttoignore2)
					{
						IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
						if (pEnt2toignore2)
						{
							stl::push_back_unique(skipList, pEnt2toignore2);
							//skipList[num_ents_to_ignore] = pEnt2toignore2;
							num_ents_to_ignore += 1;
						}
					}
				}
			}
			IAttachment *ptTa2 = pAm->GetInterfaceByName(g_pGameCVars->ai_actor_e_att2_to_ignore->GetString());
			if (ptTa2)
			{
				CEntityAttachment *entAttachment = (CEntityAttachment *)ptTa2->GetIAttachmentObject();
				if (entAttachment)
				{
					IEntity *pEnttoignore2 = gEnv->pEntitySystem->GetEntity(entAttachment->GetEntityId());
					if (pEnttoignore2)
					{
						IPhysicalEntity *pEnt2toignore2 = pEnttoignore2->GetPhysics();
						if (pEnt2toignore2)
						{
							stl::push_back_unique(skipList, pEnt2toignore2);
							//skipList[num_ents_to_ignore] = pEnt2toignore2;
							num_ents_to_ignore += 1;
						}
					}
				}
			}
		}
	}
	return num_ents_to_ignore;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------"TravelerNotes" Game functions new--------------------------------------------------------------------------
void CActorActionsNew::ProcessDodge(EntityId actorId, EntityId itemId, float force, Vec3 direction, const char *dir_name_anim_add, bool start_attack)
{
	//CryLogAlways("CActorActionsNew::ProcessDodge");
	force = g_pGameCVars->g_dodge_force_debug;
	direction.z = g_pGameCVars->g_dodge_z_vec_debug;

	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s1");
	if (g_pGameCVars->g_stamina_sys_can_dodge_if_low_stmn_val == 0)
	{
		if (pActor->GetStamina() < g_pGameCVars->g_stamina_sys_dodge_consumpt_stmn_val)
			return;
	}
	//CryLogAlways("CActorActionsNew::ProcessDodge s2");
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
	{
		//CryLogAlways("CActorActionsNew::ProcessDodge no pItem");
		return;
	}
//---------------------------block fix-------------------------------------------
	if (pActor->m_blockactivenoshield | pActor->m_blockactiveshield)
	{
		StopMeleeBlockAction(actorId, itemId);
	}
	//CryLogAlways("CActorActionsNew::ProcessDodge s3");
//---------------------------fix spear or any trowable---------------------------
	if (CWeapon * pWpn = pActor->GetWeapon(itemId))
	{
		if (pWpn->GetWeaponHoldedToThrow())
			pWpn->RequestToCancelThrowWpn();

		if (pWpn->GetWeaponType() > 0 && pWpn->IsBusy())
		{
			//CryLogAlways("CActorActionsNew::ProcessDodge pWpn->IsBusy() true");
			if (pActor->IsPlayer())
				CancelAnyAttackAction();
			else
				return;
		}
	}
	//CryLogAlways("CActorActionsNew::ProcessDodge s4");
//---------------------------bow fix---------------------------------------------
	if (CWeapon * pWpn = pActor->GetWeapon(itemId))
	{
		if (pWpn->GetWeaponType() == 9)
		{
			CBow *pBow = static_cast<CBow*>(pWpn);
			if (pBow)
			{
				if (pBow->process_arrow_load_started || pBow->hold_started)
					return;
			}
		}
	}
//-------------------------------------------------------------------------------
	//CryLogAlways("CActorActionsNew::ProcessDodge s5");
	if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
	{
		//CryLogAlways("CActorActionsNew::ProcessDodge stoped, flag eIF_BlockActions has on current item");
		return;
	}

	if (pActor->GetSAPlayTime() > 0)
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s6");
	if (pActor->GetRelaxedMod())
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s7");
	if (pActor->GetSACurrent() == eSpec_act_combat_recoil)
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s8");
	if (pActor->IsSwimming() || pActor->IsHeadUnderWater())
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s9");
	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return;

	//CryLogAlways("CActorActionsNew::ProcessDodge s10");
	const SActorStats* pActorStats = pActor->GetActorStats();
	if (pActorStats)
	{
		if (pActorStats->inAir > 0.1f)
			return;
	}

	//CryLogAlways("CActorActionsNew::ProcessDodge started");

	if (pActor->spec_action_counter_max_in_combo[eSpec_act_combat_recoil] == 0)
	{
		pActor->spec_action_counter_max_in_combo[eSpec_act_combat_recoil] = g_pGameCVars->g_dodge_mode_max_charges;
	}

	if (pActor->spec_action_counter_in_combo[eSpec_act_combat_recoil] >= pActor->spec_action_counter_max_in_combo[eSpec_act_combat_recoil])
	{
		if (g_pGameCVars->g_dodge_mode_out_of_charges_randomization > 0)
		{
			int rnd_val2 = cry_random(0,5);
			if (rnd_val2 == 1)
			{
				dir_name_anim_add = "_back";
				direction = Vec3(0.0f, -1.0f, direction.z);
			}
			else if(rnd_val2 == 2)
			{
				dir_name_anim_add = "_left";
				direction = Vec3(-1.0f, 0.0f, direction.z);
			}
			else if (rnd_val2 == 3)
			{
				dir_name_anim_add = "_fwd";
				direction = Vec3(0.0f, 1.0f, direction.z);
			}
			else if (rnd_val2 == 4)
			{
				dir_name_anim_add = "_right";
				direction = Vec3(1.0f, 0.0f, direction.z);
			}
		}
		else
			return;
	}
	else
	{
		pActor->spec_action_counter_in_combo[eSpec_act_combat_recoil] += 1;
	}

	if (g_pGameCVars->g_dodge_attack_after_end_action > 0)
	{
		if (start_attack)
		{
			//CryLogAlways("CActorActionsNew::ProcessDodge w start_attack");
			if (pActor->spec_action_timers_out_combo[eSpec_act_combat_dodge_attack_rq] == 0.0f)
				pActor->spec_action_timers_out_combo[eSpec_act_combat_dodge_attack_rq] = g_pGameCVars->g_dodge_time_countine;

			start_attack = false;
		}
	}

	bool view_slot = eIGS_FirstPerson;
	if (!pActor->IsPlayer())
		view_slot = eIGS_ThirdPerson;

	if (!pActor->IsThirdPerson())
		view_slot = eIGS_FirstPerson;

	if (pActor->IsPlayer() && view_slot == eIGS_FirstPerson)
	{
		CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId));
		if (pPlayer)
		{
			//pPlayer->AnimationControlled(true, true);
		}
	}

	string direction_str = dir_name_anim_add;
	string dodge = "dodge_start";
	string action = dodge + direction_str;
	const ItemString &dodgeAction = action.c_str();
	FragmentID fragmentId = pItem->GetFragmentID(dodgeAction.c_str());
	if (!start_attack)
	{
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync
		NetRequestPlayItemAction(actorId, fragmentId);
	}

	if (start_attack)
	{
		//CryLogAlways("Try to start attack with dodge");
		dodge = "attack_dodge";
		action = dodge + direction_str;
		CWeapon * pWpn = pActor->GetWeapon(itemId);
		if (pWpn)
		{
			if (pWpn->CanMeleeAttack())
			{
				pWpn->SpecialMeleeAttack(0.4f, 0.01f, action);
				pWpn->RequireUpdate(eIUS_General);
			}
		}
	}

	Vec3 posit(ZERO);
	posit = pActor->GetEntity()->GetPos();
	posit.z = posit.z + 1.1f*g_pGameCVars->g_dodge_z_pos_add_multipler_debug;
	if (pActor->IsThirdPerson())
	{
		if (IAnimatedCharacter* pAnimChar = pActor->GetAnimatedCharacter())
		{
			//pAnimChar->GetAnimationPlayerProxy()->
			//pAnimChar->GetGroundAlignmentParams().SetFlag(eGA_PoseAlignerUseRootOffset, false);
		}
	}

	if (g_pGameCVars->g_dodge_animation_drive_only > 0)
		direction.z = 0.0f;

	float add_time = 0.0f;
	if (direction.z == 0.0f)
		add_time = g_pGameCVars->g_dodge_time_countine;

	if (g_pGameCVars->g_dodge_z_pos_add_multipler_debug > 0.0f)
		pActor->GetEntity()->SetPos(posit);
	//CryLogAlways("try to add impulse to actor");
	Vec3 direct(ZERO);
	direct = pActor->GetEntity()->GetRotation() * direction;
	pe_action_set_velocity asv;
	asv.v = direct*force;
	if (g_pGameCVars->g_dodge_animation_drive_only == 0)
		pActor->GetEntity()->GetPhysics()->Action(&asv);

	NVcStartSpecialAction(actorId, itemId, eSpec_act_combat_recoil, add_time + g_pGameCVars->g_dodge_delay_debug);

	IGameFramework* pGF = gEnv->pGame->GetIGameFramework();
	IView* pView = 0;
	IView* pActiveView = pGF->GetIViewSystem()->GetActiveView();
	pView = pActiveView;
	if (pView == 0 || pView != pActiveView)
		return;

	float angl_ec = 15.0f;
	if (!strcmp(dir_name_anim_add, "_left"))
	{
		angl_ec = -15.0f;
	}
	else if (!strcmp(dir_name_anim_add, "_right"))
	{
		angl_ec = 15.0f;
	}
	else
	{
		angl_ec = 0;
	}

	if (pActor->GetStamina() > g_pGameCVars->g_stamina_sys_dodge_consumpt_stmn_val)
	{
		pActor->SetStamina(pActor->GetStamina() - g_pGameCVars->g_stamina_sys_dodge_consumpt_stmn_val);
	}
	pActor->spec_action_time_to_recharge_combo[eSpec_act_combat_recoil] += g_pGameCVars->g_dodge_mode_time_to_recharge_add_per_dodge;
	//temp fix
	g_pGameCVars->g_test_cam_anim_controlled = 11.1f;
	//---------
	//pView->SetFrameAdditiveCameraAngles(angles);
	/*const SViewParams *vp = pView->GetCurrentParams();
	SViewParams  params = (*vp);
	Ang3  angles = Ang3(DEG2RAD(Vec3(params.rotation.GetFwdX(), params.rotation.GetFwdY()+angl_ec, params.rotation.GetFwdZ())));
	params.rotation = params.rotation.CreateRotationXYZ(angles);
	pView->SetCurrentParams(params);//SetViewShake(angles, Vec3(ZERO),1,0,0,87);*/
}

void CActorActionsNew::ProcessFalling(EntityId actorId, EntityId itemId, float force, Vec3 direction, const char *dir_name_anim_add)
{
	if (g_pGameCVars->g_enable_falling_mode == 0)
		return;

	force = g_pGameCVars->g_falling_start_force_debug;
	direction.z = g_pGameCVars->g_falling_start_z_vec_debug;

	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetRelaxedMod())
		return;

	if (pActor->GetSACurrent() == eSpec_act_combat_recoil)
		return;

	if (pActor->GetSACurrent() == eSpec_act_combat_falling)
		return;

	if (pActor->IsSwimming() | pActor->IsHeadUnderWater())
		return;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return;

	const SActorStats* pActorStats = pActor->GetActorStats();
	if (pActorStats)
	{
		if (pActorStats->inAir<0.1f)
			return;
	}

	string direction_str = dir_name_anim_add;
	string dodge = "falling_start";
	string action = dodge + direction_str;
	const ItemString &dodgeAction = action.c_str();
	FragmentID fragmentId = pItem->GetFragmentID(dodgeAction.c_str());
	pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	if (pActor->GetActorPhysics().velocity.z < 0.0f)
		direction.z = (pActor->GetActorPhysics().velocity.z*0.2) + direction.z;

	Vec3 direct(ZERO);
	direct = pActor->GetEntity()->GetRotation() * direction;
	pe_action_set_velocity asv;
	asv.v = direct*force;
	pActor->GetEntity()->GetPhysics()->Action(&asv);
	NVcStartSpecialAction(actorId, itemId, eSpec_act_combat_falling, 0.5f);
	pActor->m_process_falling_current_vector = direct*force;
	if (g_pGameCVars->g_enable_falling_controll_by_mouse > 0 && g_pGameCVars->g_enable_falling_controll_by_mouse < 2)
	{
		pActor->m_process_falling_controll_vector = direction;
		pActor->m_process_falling_controll_start_rot = pActor->GetEntity()->GetRotation();
		pActor->m_process_falling_controll_dinamic_rot = pActor->GetEntity()->GetRotation();
	}
}

void CActorActionsNew::ForceStopFalling(EntityId actorId, EntityId itemId, bool start_attack)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pActor->GetSAPlayTime() > 0)
		return;

	if (pActor->GetSACurrent() != eSpec_act_combat_falling)
		return;

	
	bool attack_started = false;
	if (start_attack)
	{
		if (!pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			string action = "attack_falling_end";
			CWeapon * pWpn = pActor->GetWeapon(itemId);
			if (pWpn)
			{
				if (pWpn->CanMeleeAttack())
				{
					pWpn->SpecialMeleeAttack(0.7f, 0.01f, action);
					attack_started = true;
				}

			}
		}
	}

	string action = "falling_end";
	const ItemString &dodgeAction = action.c_str();
	FragmentID fragmentId = pItem->GetFragmentID(dodgeAction.c_str());
	if (!start_attack || !attack_started)
		pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);

	pActor->m_process_falling_timer_c1 = 0.0f;
	pActor->m_process_falling_timer_c2 = 0.0f;
	pActor->m_process_falling_current_vector = Vec3(ZERO);
	pActor->SetSpecRecoilAct(eARMode_recoil_dodge, 0.0f);
	pActor->SetSACurrent(eSpec_act_combat_recoil);
	pActor->SetSAPlayTime(-0.1f);
	pActor->SetSpecRecoilAct(eARMode_recoil_fly, g_pGameCVars->g_falling_delay_debug);
}

void CActorActionsNew::ProcessFallingControlle(EntityId actorId, Vec3 direction)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->GetSACurrent() != eSpec_act_combat_falling)
		return;
	
	if (g_pGameCVars->g_enable_falling_controll_by_mouse > 1)
		pActor->m_process_falling_controll_vector = direction;
}

void CActorActionsNew::AttackStartOnGroundAfterFalling(EntityId actorId, EntityId itemId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if (pActor->IsSwimming() | pActor->IsHeadUnderWater())
		return;

	if (pActor->GetStamina() < (pActor->stamina_cons_atk_base * pActor->stamina_cons_atk_ml))
	{
		if (g_pGameCVars->g_stamina_sys_can_melee_attack_if_low_stmn_val == 0)
			return;
	}

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if (!pItem)
		return;

	if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		return;

	string action = "attack_falling_end";
	CWeapon * pWpn = pActor->GetWeapon(itemId);
	if (pWpn)
	{
		pWpn->SpecialMeleeAttack(0.7f, 0.01f, action);
	}
}

bool CActorActionsNew::PreRequestPushToCameraDirection(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return false;

	if(pActor->GetActorStats()->inAir > 0.01f)
		return false;

	if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] > 0)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (pActor->GetSACurrent() == eSpec_act_combat_push_recoil)
		return false;

	if (pActor->IsSwimming() | pActor->IsHeadUnderWater())
		return false;

	if (pActor->GetActorPhysics().velocity.z != 0.0f)
	{
		return false;
	}

	//if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
	//	return false;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return false;

	if (g_pGameCVars->g_pushup_z_veloc_restriction_mode > 0)
	{
		if (g_pGameCVars->g_pushup_z_veloc_restriction_mode == 1)
		{
			if (g_pGameCVars->g_pushup_z_veloc_restriction > 0.0f)
			{
				if (pActor->GetActorPhysics().velocity.z > g_pGameCVars->g_pushup_z_veloc_restriction)
					return false;
			}
			else
			{
				if (pActor->GetActorPhysics().velocity.z < g_pGameCVars->g_pushup_z_veloc_restriction)
					return false;
			}
		}
	}

	if (pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil] == 0)
	{
		pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil] = g_pGameCVars->g_pushup_mode_max_charges;
	}

	if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_recoil] >= pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil])
		return false;

	const ItemString &pushInAirAction = "push_in_air_hold";
	if (!pushInAirAction.empty())
	{
		FragmentID fragmentId = pItem->GetFragmentID(pushInAirAction.c_str());
		if (fragmentId != TAG_ID_INVALID)
		{
			//CryLogAlways("CActorActionsNew::PreRequestPushToCameraDirection called, pushInAirAction str not empty, call animation");
			//pItem->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			_smart_ptr<class IAction> m_PushUpAction;
			TagState tagState = TAG_STATE_EMPTY;
			m_PushUpAction = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState);
			pItem->PlayFragment(m_PushUpAction);
			pItem->SetItemFlag(CItem::eIF_BlockActions, true);
		}
	}

	pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] = 1;
	return true;
}

bool CActorActionsNew::RequestPushToCameraDirection(EntityId actorId, float force, float recoil, const char *push_action_name)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return false;

	if (pActor->GetSAPlayTime() > 0)
		return false;

	if (pActor->GetRelaxedMod())
		return false;

	if (pActor->GetSACurrent() == eSpec_act_combat_push_recoil)
		return false;

	if (pActor->IsSwimming() | pActor->IsHeadUnderWater())
		return false;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return false;

	if (g_pGameCVars->g_pushup_z_veloc_restriction_mode > 0)
	{
		if (g_pGameCVars->g_pushup_z_veloc_restriction_mode == 1)
		{
			if (g_pGameCVars->g_pushup_z_veloc_restriction > 0.0f)
			{
				if (pActor->GetActorPhysics().velocity.z > g_pGameCVars->g_pushup_z_veloc_restriction)
					return false;
			}
			else
			{
				if (pActor->GetActorPhysics().velocity.z < g_pGameCVars->g_pushup_z_veloc_restriction)
					return false;
			}
		}
		else if (g_pGameCVars->g_pushup_z_veloc_restriction_mode == 2)
		{
			if (g_pGameCVars->g_pushup_z_veloc_restriction > 0.0f)
			{
				if (pActor->GetActorPhysics().velocity.z > g_pGameCVars->g_pushup_z_veloc_restriction)
					force *= 0.1f;
			}
			else
			{
				if (pActor->GetActorPhysics().velocity.z < g_pGameCVars->g_pushup_z_veloc_restriction)
					force *= 0.1f;
			}
		}
	}

	if (pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil] == 0)
	{
		pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil] = g_pGameCVars->g_pushup_mode_max_charges;
	}

	if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_recoil] >= pActor->spec_action_counter_max_in_combo[eSpec_act_combat_push_recoil])
		return false;

	
	const ItemString &pushInAirAction = push_action_name;
	if (!pushInAirAction.empty())
	{
		FragmentID fragmentId = pItem->GetFragmentID(pushInAirAction.c_str());
		if (fragmentId != TAG_ID_INVALID)
		{
			string act_ftg = string(push_action_name) + string("_ft");
			const ItemString &pushInAirAction_ft = act_ftg.c_str();
			if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] == 1)
			{
				FragmentID fragmentId2 = pItem->GetFragmentID(pushInAirAction_ft.c_str());
				if (fragmentId2 != TAG_ID_INVALID)
				{
					_smart_ptr<class IAction> m_PushUpAction;
					TagState tagState = TAG_STATE_EMPTY;
					m_PushUpAction = new CItemAction(PP_PlayerActionUrgent, fragmentId2, tagState);
					pItem->PlayFragment(m_PushUpAction);
				}

				pItem->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
				
			}
			else if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] == 2)
			{

			}
			else
			{
				FragmentID fragmentId2 = pItem->GetFragmentID(pushInAirAction_ft.c_str());
				if (fragmentId2 != TAG_ID_INVALID)
				{
					_smart_ptr<class IAction> m_PushUpAction;
					TagState tagState = TAG_STATE_EMPTY;
					m_PushUpAction = new CItemAction(PP_PlayerActionUrgent, fragmentId2, tagState);
					pItem->PlayFragment(m_PushUpAction);
				}

				pItem->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			}
			//pItem->SetItemFlag(CItem::eIF_BlockActions, true);
			//CryLogAlways("CActorActionsNew::RequestPushToCameraDirection called, push_action_name str not empty, call animation");
		}
	}
	pItem->SetItemFlag(CItem::eIF_BlockActions, false);
	SMovementState info;
	pMC->GetMovementState(info);
	pe_action_set_velocity asv;
	Vec3 direction = info.fireDirection;
	CPlayer *pPlayer = static_cast<CPlayer*>(pActor);
	if (g_pGameCVars->g_pushup_mode_zero_z_direction_on_ground > 0)
	{
		if (pPlayer && pPlayer->IsOnGround() && !pPlayer->IsInAir())
		{
			//CryLogAlways("fireDirection z = %f", direction.z);
			if (direction.z < 0.0f)
			{
				Vec3 fwd_dir(0, 1, 0);
				direction = pActor->GetEntity()->GetRotation() * fwd_dir;
			}
		}
	}
	//direction.z = direction.z + pActor->GetActorPhysics().velocity.z;
	Vec2 xy_dir = Vec2(direction.x*force, direction.y*force); 
	xy_dir.x = xy_dir.x * g_pGameCVars->g_pushup_force_xy_mult;
	xy_dir.y = xy_dir.y * g_pGameCVars->g_pushup_force_xy_mult;
	float dir_z = (direction.z*force) * g_pGameCVars->g_pushup_force_z_mult;
	dir_z = dir_z + g_pGameCVars->g_pushup_z_veloc_add_cn;
	if (pPlayer && pPlayer->GetEntityId() == pActor->GetEntityId())
	{
		if (pPlayer->IsOnGround() && !pPlayer->IsInAir())
		{
			xy_dir.x = xy_dir.x * g_pGameCVars->g_pushup_force_xy_on_ground_mult;
			xy_dir.y = xy_dir.y * g_pGameCVars->g_pushup_force_xy_on_ground_mult;
		}
	}

	if (g_pGameCVars->g_pushup_enable_xy_impulse_charge > 0)
	{
		xy_dir.x = xy_dir.x + pActor->GetActorPhysics().velocity.x;
		xy_dir.y = xy_dir.y + pActor->GetActorPhysics().velocity.y;
	}
	asv.v = Vec3(xy_dir.x, xy_dir.y, dir_z + pActor->GetActorPhysics().velocity.z);
	pActor->GetEntity()->GetPhysics()->Action(&asv);
	NVcStartSpecialAction(actorId, pActor->GetCurrentItemId(), eSpec_act_combat_push_recoil, recoil);
	pActor->spec_action_time_to_recharge_combo[eSpec_act_combat_push_recoil] += g_pGameCVars->g_pushup_mode_time_to_recharge_add_per_push;
	pActor->spec_action_counter_in_combo[eSpec_act_combat_push_recoil] += 1;
	pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] = 2;
	return true;
}

void CActorActionsNew::ActionPushToCameraDirectionEnd(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return;

	pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] = 0;
	pActor->spec_action_counter_in_combo[eSpec_act_combat_push_state] = 0;
	//pItem->SetItemFlag(CItem::eIF_BlockActions, false);
	const ItemString &pushInAirAction = "push_in_air_end";
	if (!pushInAirAction.empty())
	{
		FragmentID fragmentId = pItem->GetFragmentID(pushInAirAction.c_str());
		if (fragmentId != TAG_ID_INVALID)
		{
			pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			/*_smart_ptr<class IAction> m_PushUpAction;
			TagState tagState = TAG_STATE_EMPTY;
			m_PushUpAction = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState);
			pItem->PlayFragment(m_PushUpAction);*/
		}
	}
}

void CActorActionsNew::ActionPushToCameraDirectionCollisionEvent(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return;

	const ItemString &pushInAirAction = "push_in_air_collide";
	if (!pushInAirAction.empty())
	{
		FragmentID fragmentId = pItem->GetFragmentID(pushInAirAction.c_str());
		if (fragmentId != TAG_ID_INVALID)
		{
			//pItem->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			_smart_ptr<class IAction> m_PushUpAction;
			TagState tagState = TAG_STATE_EMPTY;
			m_PushUpAction = new CItemAction(PP_PlayerActionUrgent, fragmentId, tagState);
			pItem->PlayFragment(m_PushUpAction);
		}
	}
}

void CActorActionsNew::AttackStartAfterDodge(EntityId actorId, const char *dir_name_anim_add)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	string direction_str = dir_name_anim_add;
	string dodge_atk = "attack_after_dodge";
	string action = dodge_atk + direction_str;
	CWeapon * pWpn = pActor->GetWeapon(pActor->GetCurrentItemId());
	if (pWpn)
	{
		if (!pWpn->IsMeleeWpn())
			return;

		if (!pWpn->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			//pWpn-
			//CryLogAlways("CActorActionsNew::AttackStartAfterDodge complete");
			pWpn->SpecialMeleeAttack(0.4f, 0.1f, action);
			pWpn->RequireUpdate(eIUS_General);
		}
	}

}
void CActorActionsNew::QuickSlotItemPressed(EntityId id, EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if(pActor->IsPlayer())
		InventoryItemPressed(id, true);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------
void CActorActionsNew::RequestReinitActorPhyMassVCItems(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	//if (!pActor->IsPlayer())
	//	return;

	if (g_pGameCVars->g_enable_actor_equipment_weigth_charge <= 0)
		return;

	if (pActor->IsDead())
		return;

	if (!pActor->GetInventory())
		return;

	if (pActor->GetActorPhysics().mass <= 0.0f)
		return;

	if (pActor->base_mass_val <= 0.0f)
		pActor->base_mass_val = pActor->GetActorPhysics().mass;

	pActor->inventory_mass_val = 0.0f;
	IInventory *pInventory = pActor->GetInventory();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		CItem *pCurItem = pActor->GetItem(pInventory->GetItem(i));
		if (pCurItem)
		{
			if(pCurItem->GetSharedItemParams())
				pActor->inventory_mass_val += pCurItem->GetSharedItemParams()->params.mass;
		}
	}

	if (pActor->inventory_mass_val > 0.0f)
		pActor->Physicalize();
}
//player only functions -------------------------------------------------------------------------------------------------------------
void CActorActionsNew::ContextMenuItemPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	int envent_id = id;

	if (envent_id == 0)
	{
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;
	}
	else if (envent_id == 1)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		EntityId ide = pGameGlobals->current_selected_Item;
		int hud_slot_old_pos = -1;
		if (CItem *pItem = pGameGlobals->GetThisItem(ide))
		{
			if (pHud->InventoryUsePrimaryItemPos())
				hud_slot_old_pos = pItem->GetHudItemPosPrimary();
			else
				hud_slot_old_pos = pItem->GetHudItemPosSecondary();
		}
		pActor->DropItem(ide, 1, false);
		pHud->DeleteItemVisualFromInventory(hud_slot_old_pos);
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 2)
	{
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 3)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		EntityId ide = pGameGlobals->current_selected_Item;
		IItem *pItem = gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide);
		bool equipable = true;
		equipable = pActor->EquipItem(ide);
		if (pItem->CanSelect() && equipable)
		{
			CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
			if (!pItemii->IsSelected())
			{
				pActor->SelectItem(ide, true, true);
			}
			else
			{
				pActor->DeSelectItem(ide, true);
			}
		}
		//pActor->EquipItem(ide);
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 4)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		EntityId ide = pGameGlobals->current_selected_Item;
		IItem *pItem = gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide);
		bool equipable = true;
		equipable = pActor->EquipItem(ide);
		if (pItem->CanSelect() && equipable)
		{
			CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
			if (!pItemii->IsSelected())
			{
				pActor->ScheduleItemSwitch(ide, true);
			}
			else
			{
				pActor->DeSelectItem(ide, true);
			}
		}
		//pActor->EquipItem(ide);
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 5)
	{
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 6)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		EntityId ide = pGameGlobals->current_selected_Item;
		pActor->EquipItem(ide);
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;

	}
	else if (envent_id == 7)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		EntityId ide = g_pGameCVars->g_pl_inv_menu_selected_item;
		CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
		CWeapon* pWeapon = static_cast<CWeapon*>(pItem->GetIWeapon());
		int idrf = 0;
		IFireMode *pFmd = NULL;
		if (pWeapon)
		{
			int idrf = pItem->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = pItem->GetIWeapon()->GetFireMode(idrf);
			if (pWeapon && pFmd && pWeapon->GetAmmoCount(pFmd->GetAmmoType()) > 0)
			{
				IInventory *pInventory = pActor->GetInventory();
				const SAmmoParams *pAmmoParams = g_pGame->GetWeaponSystem()->GetAmmoParams(pFmd->GetAmmoType());
				string fnl = "";//pAmmoParams->def_ammo_pickup;
				if (!fnl.empty())
				{
					IEntityClass* fnl_class = gEnv->pEntitySystem->GetClassRegistry()->FindClass(fnl.c_str());
					if (pInventory->GetCountOfClass(fnl_class->GetName()) == 0)
					{
						int ac = pWeapon->GetAmmoCount(pFmd->GetAmmoType());
						pWeapon->SetInventoryAmmoCount(pFmd->GetAmmoType(), pWeapon->GetInventoryAmmoCount(pFmd->GetAmmoType()) + pFmd->GetAmmoCount() - 1);
						pWeapon->SetAmmoCount(pFmd->GetAmmoType(), 0);
					}
					else
					{
						pWeapon->SetInventoryAmmoCount(pFmd->GetAmmoType(), pWeapon->GetInventoryAmmoCount(pFmd->GetAmmoType()) + pFmd->GetAmmoCount());
						pWeapon->SetAmmoCount(pFmd->GetAmmoType(), 0);
					}
				}
				else
				{
					pWeapon->SetInventoryAmmoCount(pFmd->GetAmmoType(), pWeapon->GetInventoryAmmoCount(pFmd->GetAmmoType()) + pFmd->GetAmmoCount());
					pWeapon->SetAmmoCount(pFmd->GetAmmoType(), 0);
				}
			}
		}
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;
	}
	else if (envent_id == 8)
	{
		//CryLogAlways("envent_id == 8");
		pGameGlobals->current_item_to_combine = pGameGlobals->current_selected_Item;
		Current_inventory_selection_mode = ePISMode_combine; //items combine mode
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;
	}
	else if (envent_id == 9)
	{
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer(false);
		return;
	}
	else if (envent_id == 10)
	{
		//pHud->DisableContextMenu();
		pHud->StartDtlDisableTimer();
		InventoryItemPressed(pGameGlobals->current_selected_Item);
	}
}

void CActorActionsNew::InventoryItemPressed(EntityId id, bool wo_inv_update)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if(pHud->timer_to_can_item_press > 0.0f)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (id >= 1)
	{
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActor)
			return;

		if (pActor->m_blockactivenoshield || pActor->m_blockactiveshield)
			StopMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());

		if(pActor->GetNoWeaponId() == id)
			return;

		if (Current_inventory_selection_mode == ePISMode_equip)
		{
			if (CItem *pItem = pGameGlobals->GetThisItem(id))
			{
				int hud_slot_old_pos = -1;
				if(pHud->InventoryUsePrimaryItemPos())
					hud_slot_old_pos = pItem->GetHudItemPosPrimary();
				else
					hud_slot_old_pos = pItem->GetHudItemPosSecondary();

				bool equipable = true;
				equipable = pActor->EquipItem(id);
				if (pItem->CanSelect() && equipable)
				{
					if (id == pActor->GetCurrentEquippedItemId(eAESlot_Weapon_Left))
					{
						pActor->EquipWeaponToLeftHand(id, false);
					}
					else
					{
						CItem *pItemii = pItem;
						if (!pItemii->IsSelected())
						{
							pActor->ScheduleItemSwitch(id, true);
						}
						else
						{
							pActor->DeSelectItem(id, true);
						}
					}
				}
				CItem *pItemiif = pItem;
				if (pItemiif && pItemiif->GetItemQuanity() < 1 && pItemiif->GetItemType() == 3)
				{
					pHud->DeleteItemVisualFromInventory(hud_slot_old_pos);
				}

				if (pItemiif && pItemiif->GetBookNumPages() > 0)
				{
					pHud->temportary_book_id = id;
					pHud->StartDtlDisableTimer();
					return;
				}
				else
				{
					if(!wo_inv_update)
						pHud->UpdateInventory(10);
				}
			}
		}
		else if (Current_inventory_selection_mode == ePISMode_drop)
		{
			int hud_slot_old_pos = -1;
			if (CItem *pItem = pGameGlobals->GetThisItem(id))
			{
				if (pHud->InventoryUsePrimaryItemPos())
					hud_slot_old_pos = pItem->GetHudItemPosPrimary();
				else
					hud_slot_old_pos = pItem->GetHudItemPosSecondary();
			}
			EntityId ide = id;
			pActor->DropItem(ide, 1, false);
			pHud->DeleteItemVisualFromInventory(hud_slot_old_pos);
			//pHud->ClearInventory();
			if (!wo_inv_update)
				pHud->UpdateInventory(10);
		}
		else if (Current_inventory_selection_mode == ePISMode_combine)
		{
			CryLogAlways("Current_inventory_selection_mode == ePISMode_combine");
			if (id == pGameGlobals->current_item_to_combine)
			{
				Current_inventory_selection_mode = ePISMode_equip;
				pGameGlobals->current_item_to_combine = 0;
				return;
			}
			CryLogAlways("Current_inventory_selection_mode == ePISMode_combine 2");
			CItem *pItem_this = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			CItem *pItem_to_combine = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pGameGlobals->current_item_to_combine));
			if (pItem_this && pItem_to_combine)
			{
				EntityId ide = id;
				if (ide == pGameGlobals->current_item_to_combine)
				{
					Current_inventory_selection_mode = ePISMode_equip;
					pGameGlobals->current_item_to_combine = 0;
					return;
				}

				if (pItem_this->GetReprType() == 1)
				{
					pItem_to_combine->OnHit(pItem_this->GetReprFc(), g_pGame->GetGameRules()->GetHitTypeId("repair"));
				}
				else if (pItem_to_combine->GetReprType() == 1)
				{
					pItem_this->OnHit(pItem_to_combine->GetReprFc(), g_pGame->GetGameRules()->GetHitTypeId("repair"));
				}
				//start process combination
				ProcessCombineItems(pItem_this->GetEntityId(), pItem_to_combine->GetEntityId());
			}
			Current_inventory_selection_mode = ePISMode_equip;
			pGameGlobals->current_item_to_combine = 0;
			return;
		}
	}
}

void CActorActionsNew::InventoryItemProcessEquipItemInLeftSlot(EntityId id)
{
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pItem || !pActor)
		return;

	if (pActor->m_blockactivenoshield || pActor->m_blockactiveshield)
		StopMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());

	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (pItem->GetShield() > 0 || pItem->GetItemType() == 4 || pItem->GetItemType() == 10 ||
		pItem->GetItemType() == 9)
	{
		InventoryItemPressed(id, true);
		return;
	}
	CWeapon * pWpn = pActor->GetWeapon(pItem->GetEntityId());
	if (pWpn)
	{
		if (pWpn->IsWpnTwoHanded())
		{
			InventoryItemPressed(id, true);
			return;
		}
		CItem *pItem_current = (CItem*)(pActor->GetItem(pActor->GetCurrentItemId()));
		if (pItem_current)
		{
			if (pItem_current->IsWpnTwoHanded())
			{
				InventoryItemPressed(id, true);
				return;
			}

			if (g_pGameCVars->g_enable_equip_weapons_in_left_hand_support > 0)
			{
				bool equipable = pActor->EquipItem(id);
				if (pItem->CanSelect() && equipable)
				{
					pActor->EquipWeaponToLeftHand(id);
				}
			}
			else
			{
				if(!pActor->IsRemote())
					pHud->StatMsg("You can not equip weapon in left hand");
			}
		}
	}
	pHud->UpdateInventory(10);
	//CryLogAlways("CActorActionsNew::InventoryItemProcessEquipItemInLeftSlot called");
}

void CActorActionsNew::InventorySelectItem(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());

	if (id >= 1)
	{
		pGameGlobals->current_selected_Item = id;
		pHud->ShowItemInformation(true, true, id);
	}
	else
	{
		if (pHud->Inv_ItemInfo_loaded)
			pHud->ShowItemInformation(false, false, 0);

		pGameGlobals->current_selected_Item = 0;
		return;
	}
}

void CActorActionsNew::InventoryDeselectPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pHud->Inv_ItemInfo_loaded)
		pHud->ShowItemInformation(false, false, 0);

	pGameGlobals->current_selected_Item = 0;
}

void CActorActionsNew::QuickMenuSelectSlot(int id)
{
}

void CActorActionsNew::QuickMenuDeselectSlot(int id)
{
}

void CActorActionsNew::DialogSysLinePressed(int id)
{
	CDialogSystemEvents *pDsystemEvent = g_pGame->GetDialogSystemEvents();
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;
	
	if (!pDsystemEvent)
		return;

	if (!pGameGlobals)
		return;

	int questionid = id;
	pDsystemEvent->SetPressedId(questionid);
	EntityId ida = pGameGlobals->GetDialogMainNpc();
	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(ida));
	pDsystemEvent->SetOldDs(pActor->GetDialogStage());
	if (pHud->m_DialogSystemVisible)
	{
		CActor *pActorpl = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		XmlNodeRef node = GetISystem()->LoadXmlFromFile(pGameGlobals->current_dialog_path.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("DialogSys xml not loaded.");
			return;
		}

		XmlNodeRef paramsNode = node->findChild("params");
		XmlNodeRef statesNode = node->findChild("states");
		XmlString dialogstringanswere;
		XmlNodeRef state1Node;
		int nextstate = 0;
		int eventid = 0;
		int numquestions = 0;
		int numeventsinquestion = 0;
		string line_cnd = "question";
		string line_event_id = "eventid";
		state1Node = statesNode->getChild(pActor->GetDialogStage() - 1);
		if (!state1Node)
			return;

		XmlNodeRef numquestionsNode = state1Node->findChild("numquestionsinstate");
		numquestionsNode->getAttr("numquestions", numquestions);
		for (int i = 0; i < numquestions; i++)
		{
			char B_valstr[17];
			itoa(i, B_valstr, 10);
			string C_valstr = B_valstr;
			string Line_valstr = line_cnd + C_valstr;
			if (questionid == i)
			{
				XmlNodeRef question1Node = state1Node->findChild(Line_valstr.c_str());
				if (!question1Node)
					return;

				question1Node->getAttr("gotostate", nextstate);
				question1Node->getAttr("numevents", numeventsinquestion);
				for (int i = 0; i < numeventsinquestion; i++)
				{
					char B1_valstr[17];
					itoa(i, B1_valstr, 10);
					string C1_valstr = B1_valstr;
					string Event_valstr = line_event_id + C1_valstr;
					if (i == 0)
						Event_valstr = "eventid";

					question1Node->getAttr(Event_valstr.c_str(), eventid);
					g_pGame->GetDialogSystemEvents()->ExecEventNew(eventid, questionid, pGameGlobals->current_dialog_path, i);
				}
				pActor->SetDialogStage(nextstate);
				question1Node->getAttr("dialogstringanswere", dialogstringanswere);
				pHud->DialogNPCWords(dialogstringanswere.c_str());
				UpdateDialog();
			}
		}
	}
}

void CActorActionsNew::UpdateDialog()
{
	CDialogSystemEvents *pDsystemEvent = g_pGame->GetDialogSystemEvents();
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if (!pGameGlobals)
		return;

	if (!pDsystemEvent)
		return;

	EntityId id = pGameGlobals->GetDialogMainNpc();
	CActor *pActorpl = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	XmlNodeRef node = GetISystem()->LoadXmlFromFile(pGameGlobals->current_dialog_path.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("DialogSys xml not loaded.");
		return;
	}

	XmlNodeRef paramsNode = node->findChild("params");
	XmlNodeRef statesNode = node->findChild("states");
	XmlString dialogstringquestion = "";
	int numquestions = 0;
	XmlNodeRef state1Node;
	state1Node = statesNode->getChild(pActor->GetDialogStage() - 1);
	if (!state1Node)
		return;

	string line_cnd = "question";
	XmlNodeRef numquestionsNode = state1Node->findChild("numquestionsinstate");
	numquestionsNode->getAttr("numquestions", numquestions);
	for (int i = 0; i < numquestions; i++)
	{
		char B_valstr[17];
		itoa(i, B_valstr, 10);
		string C_valstr = B_valstr;
		string Line_valstr = line_cnd + C_valstr;
		XmlNodeRef questionNodeLineStruct = state1Node->findChild(Line_valstr.c_str());
		if (questionNodeLineStruct)
		{
			questionNodeLineStruct->getAttr("dialogstringquestion", dialogstringquestion);
			pHud->AddDialogLine(dialogstringquestion.c_str(), i);
		}
	}
}

void CActorActionsNew::StartDialog(EntityId actorId)
{
	CDialogSystemEvents *pDsystemEvent = g_pGame->GetDialogSystemEvents();
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if (!pGameGlobals)
		return;

	if (!pDsystemEvent)
		return;

	pGameGlobals->SetDialogMainNpc(actorId);
	CActor *pActorpl = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	CActor  *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId));
	IEntity * pEntity = pActor->GetEntity();
	const char* g_dialog_string;
	if (pActorpl->GetGender() == 0)
		g_dialog_string = "dialogsysdataM";
	else if (pActorpl->GetGender() == 1)
		g_dialog_string = "dialogsysdataF";

	SmartScriptTable props;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	CScriptSetGetChain prop(props);
	string dialogpath;
	prop.GetValue(g_dialog_string, dialogpath);
	pGameGlobals->current_dialog_path = dialogpath;
	pHud->ShowDialog(true, pGameGlobals->current_dialog_path, pActor->GetDialogStage());
}

bool CActorActionsNew::TryForDialogAbleTarget()
{
	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return false;

	const SInteractionInfo& interactionInfo = pPlayer->GetCurrentInteractionInfo();
	if (interactionInfo.interactionType == eInteraction_Use)
	{
		//CryLogAlways("try to start dialog2");
		EntityId ActorId = interactionInfo.interactiveEntityId;
		CActor  *pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(ActorId));
		if (!pTargetActor)
			return false;

		if (pTargetActor->GetHealth() <= 0.0f)
			return false;

		//CryLogAlways("try to start dialog3");
		IEntity *pEntity = pTargetActor->GetEntity();
		SmartScriptTable props;
		IScriptTable* pScriptTable = pEntity->GetScriptTable();
		pScriptTable && pScriptTable->GetValue("Properties", props);
		CScriptSetGetChain prop(props);
		int dialogable = 0;
		prop.GetValue("dialogable", dialogable);
		if (dialogable > 0)
		{
			//CryLogAlways("try to start dialog4");
			StartDialog(pEntity->GetId());
			return true;
		}
	}
	return false;
}

bool CActorActionsNew::TryForLootAbleTarget()
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return false;

	float ray_length = 2.0f;
	ray_hit rayhit;
	unsigned int flags = rwi_stop_at_pierceable | rwi_colltype_any;
	CCamera& cam = GetISystem()->GetViewCamera();
	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	Vec3 cam_pos(0, 0, 0);
	Vec3 cam_dir(0, 0, 0);
	IPhysicalEntity *pIgnore = pPlayer ? pPlayer->GetEntity()->GetPhysics() : 0;
	if (pPlayer && pPlayer->IsThirdPerson())
	{
		//cam_pos = pPlayer->GetLocalEyePos() + cam.GetViewdir()*-1.0000f;
		//cam_dir = cam.GetViewdir()*-ray_length;
		cam_pos = cam.GetPosition() + cam.GetViewdir();
		cam_dir = cam.GetViewdir()*(ray_length + 60.0f);
	}
	else
	{
		cam_pos = cam.GetPosition() + cam.GetViewdir();
		cam_dir = cam.GetViewdir()*ray_length;
	}

	/*if (pPlayer)
	{
		const SInteractionInfo& interactionInfo = pPlayer->GetCurrentInteractionInfo();
		if (interactionInfo.interactionType != eInteraction_None)
		{
			return false;
		}
	}*/

	if (gEnv->pPhysicalWorld->RayWorldIntersection(cam_pos, cam_dir, ent_static | ent_sleeping_rigid | ent_rigid | ent_living | ent_independent, flags, &rayhit, 1, &pIgnore, pIgnore ? 1 : 0))
	{
		IPhysicalEntity *pCollider = rayhit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (pCollidedEntity)
		{
			CActor *pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId));
			if (pTargetActor)
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 0 && chest < 2)
					{
						StartLooting(collidedEntityId);
						return true;
					}
				}

				if (pTargetActor->IsDead())
				{
					StartLooting(collidedEntityId);
					return true;
				}
			}
			else
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 1 && chest < 3)
					{
						SmartScriptTable propsOnUse;
						const char* chestname;
						props->GetValue("OnUse", propsOnUse);
						propsOnUse->GetValue("ChestName", chestname);
						IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->FindEntityByName(chestname);
						if (pEntityc)
						{
							EntityId idft = pEntityc->GetId();
							StartLooting(idft);
							return true;
						}
					}
					else
					{
						if (pGameGlobals->GetThisContainer(collidedEntityId))
							StartLooting(collidedEntityId);
					}
				}
			}
		}
	}
	else if (pPlayer->IsThirdPerson())
	{
		const EntityId collidedEntityId = pPlayer->GetGameObject()->GetWorldQuery()->GetLookAtEntityId();
		IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(collidedEntityId);
		if (pCollidedEntity)
		{
			Vec3 pos1 = pCollidedEntity->GetWorldPos();
			Vec3 pos2 = pPlayer->GetEntity()->GetWorldPos();
			if (pos2.GetDistance(pos1) > 2.0f)
				return false;

			CActor  *pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId));
			if (pTargetActor)
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 0 && chest < 2)
					{
						StartLooting(collidedEntityId);
						return true;
					}
				}

				if (pTargetActor->IsDead())
				{
					StartLooting(collidedEntityId);
					return true;
				}
			}
			else
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 1 && chest < 3)
					{
						SmartScriptTable propsOnUse;
						string chestname;
						props->GetValue("OnUse", propsOnUse);
						propsOnUse->GetValue("ChestName", chestname);
						IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->FindEntityByName(chestname);
						if (pEntityc)
						{
							EntityId idft = pEntityc->GetId();
							StartLooting(idft);
							return true;
						}
					}
					else
					{
						if (pGameGlobals->GetThisContainer(collidedEntityId))
							StartLooting(collidedEntityId);
					}
				}
			}
		}
	}
	return false;
}

float CActorActionsNew::GetDistanceToPlayerFromCamera()
{
	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return 0.0f;

	CCamera& cam = GetISystem()->GetViewCamera();
	return cam.GetPosition().GetDistance(pPlayer->GetBonePosition("Bip01 Head"));
}

float CActorActionsNew::GetDistanceToPlayerFromCameraFCL()
{
	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return 0.0f;

	CCamera& cam = GetISystem()->GetViewCamera();
	return cam.GetPosition().GetDistance(pPlayer->GetBonePosition("Bip01 Head"));
}

void CActorActionsNew::ReEanableDialogAfterTraidingEnd()
{
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	IEntity* pTrdEntity = gEnv->pEntitySystem->GetEntity(pGameGlobals->current_dialog_NPC);
	SmartScriptTable props;
	IScriptTable* pScriptTable = pTrdEntity->GetScriptTable();
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		int dialogable = 0;
		props->GetValue("dialogable", dialogable);
		if (dialogable > 1)
		{
			CPlayerEntityInteraction& playerEntityInteractor = pPlayer->GetPlayerEntityInteration();
			playerEntityInteractor.ForcedUseEntity(pPlayer, pGameGlobals->current_dialog_NPC);
		}
		else if (dialogable == 1)
		{
			CHUDCommon* pHud = g_pGame->GetHUDCommon();
			if (!pHud)
				return;

			StartDialog(pGameGlobals->current_dialog_NPC);
		}
	}
}

void CActorActionsNew::StartLooting(EntityId targetId)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	pGameGlobals->current_opened_Chest = targetId;
	CActor *pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(targetId));
	if (pTargetActor)
	{
		pHud->ShowChestInv(true);
	}
	else
	{
		pHud->ShowChestInvsss(targetId ,true);
	}
}

void CActorActionsNew::StartTraiding(EntityId traiderId)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	pGameGlobals->current_traider_chest = 0;
	IEntity* pTrdEntity = gEnv->pEntitySystem->GetEntity(traiderId);
	SmartScriptTable props;
	IScriptTable* pScriptTable = pTrdEntity->GetScriptTable();
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		string chest = "";
		props->GetValue("traiding_chest_name", chest);
		if (!chest.empty())
		{
			IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->FindEntityByName(chest);
			if (pEntityc)
			{
				CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
				if (pChest)
				{
					pGameGlobals->current_traider_chest = pEntityc->GetId();
				}
				else
				{
					chest = "";
				}
			}
			else
			{
				chest = "";
			}
		}
		
		if (chest.empty())
			pGameGlobals->current_traider_chest = 0;
	}
	pGameGlobals->current_traider = traiderId;
	pHud->ShowTradeSystem(pGameGlobals->current_traider, true);
}

void CActorActionsNew::UpdateInteractiveObjectInfo()
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CPlayer *pPlayer = static_cast<CPlayer *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	if (pPlayer->IsDead())
		return;

	bool target_is_showed = pHud->isInteractiveInfoObjectShowed;
	bool stop_ncr = true;

	const SInteractionInfo& interactionInfo = pPlayer->GetCurrentInteractionInfo();
	if (interactionInfo.interactionType == eInteraction_Use)
	{
		EntityId ActorId = interactionInfo.interactiveEntityId;
		if (CurrentInteractiveId != ActorId)
		{
			//CryLogAlways("Try to show interactive icon1");
			CurrentInteractiveId = ActorId;
			target_is_showed = false;
			pHud->ShowInteractiveObjectInfo("", "", 0, false);
			stop_ncr = false;
		}
		
		if (stop_ncr)
		{
			//CryLogAlways("interactive icon1 is showed already");
			target_is_showed = pHud->isInteractiveInfoObjectShowed;
			return;
		}
		CActor  *pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(ActorId));
		if (!pTargetActor)
			return;

		if (pTargetActor->GetHealth() <= 0.0f)
			return;

		SmartScriptTable props;
		IScriptTable* pScriptTable = pTargetActor->GetEntity()->GetScriptTable();
		if (pScriptTable && pScriptTable->GetValue("Properties", props))
		{
			int dialogable = 0;
			props->GetValue("dialogable", dialogable);
			const char *charactername;
			props->GetValue("charactername", charactername);
			if (dialogable > 0)
			{
				if (!target_is_showed)
					pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/C_Elm01_v2.png", "Talk " + string(charactername), 1, true);

				return;
			}
		}
		return;
	}
	else if (interactionInfo.interactionType == eInteraction_PickupItem || interactionInfo.interactionType == eInteraction_ExchangeItem || interactionInfo.interactionType == eInteraction_PickupAmmo)
	{
		//CryLogAlways("Try to show interactive icon eInteraction_PickupItem");
		
		stop_ncr = true;
		EntityId ItemId = interactionInfo.interactiveEntityId;
		/*
		if (CurrentInteractiveId != ItemId)
		{
			//CryLogAlways("Try to show interactive icon2");
			CurrentInteractiveId = ItemId;
			target_is_showed = false;
			pHud->ShowInteractiveObjectInfo("", "", 0, false);
			stop_ncr = false;
		}
		
		if (stop_ncr)
		{
			//CryLogAlways("interactive icon2 is showed already");
			target_is_showed = pHud->isInteractiveInfoObjectShowed;
			return;
		}
		*/
		CItem *pItem = static_cast<CItem *>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ItemId));
		if (pItem)
		{
			string icon_name = pItem->GetIcon();
			string icon_pth_pls = "icons/";
			string icon = icon_pth_pls + icon_name;
			string txt = pItem->GetItemDec(0);
			//CryLogAlways(pItem->GetItemDec(2));
			if (txt == "")
				txt = "Pick up this item";

			if (!target_is_showed)
				pHud->ShowInteractiveObjectInfo(icon, txt, 1, true);

			return;
		}
		
		return;
	}

	float ray_length = 2.0f;
	ray_hit rayhit;
	unsigned int flags = rwi_stop_at_pierceable | rwi_colltype_any;
	CCamera& cam = GetISystem()->GetViewCamera();
	Vec3 cam_pos(0, 0, 0);
	Vec3 cam_dir(0, 0, 0);
	IPhysicalEntity *pIgnore = pPlayer ? pPlayer->GetEntity()->GetPhysics() : 0;
	if (pPlayer && pPlayer->IsThirdPerson())
	{
		cam_pos = cam.GetPosition() + cam.GetViewdir();
		cam_dir = cam.GetViewdir()*(ray_length + 60.0f);
	}
	else
	{
		cam_pos = cam.GetPosition() + cam.GetViewdir();
		cam_dir = cam.GetViewdir()*ray_length;
	}

	if (gEnv->pPhysicalWorld->RayWorldIntersection(cam_pos, cam_dir, ent_all, flags, &rayhit, 1, &pIgnore, pIgnore ? 1 : 0))
	{
		IPhysicalEntity *pCollider = rayhit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		bool wo_dist = true;
		if ((rayhit.dist - GetDistanceToPlayerFromCamera()) > ray_length)
			wo_dist = false;

		if (pCollidedEntity && wo_dist)
		{
			stop_ncr = true;
			if (CurrentInteractiveId != collidedEntityId)
			{
				//CryLogAlways("Try to show interactive icon3");
				CurrentInteractiveId = collidedEntityId;
				target_is_showed = false;
				pHud->ShowInteractiveObjectInfo("", "", 0, false);
				stop_ncr = false;
			}
			else
			{
				if (!pHud->m_InteractiveInfoVisible)
				{
					target_is_showed = false;
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
					stop_ncr = false;
				}
			}
			
			if (stop_ncr)
			{
				//CryLogAlways("interactive icon3 is showed already");
				target_is_showed = pHud->isInteractiveInfoObjectShowed;
				return;
			}

			CActor  *pActorr = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId));
			if (pActorr)
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 0 && chest < 2)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", "Open", 1, true);

						return;
					}
				}

				if (pActorr->IsDead())
				{
					if (!target_is_showed)
						pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", "Find Items", 1, true);

					return;
				}
			}
			else
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int door = 0;
					int chest = 0;
					props->GetValue("chest", chest);
					props->GetValue("door", door);
					const char *usetext = "";
					props->GetValue("UseMessage", usetext);
					if (door > 0 && door < 2)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else if (door > 1 && door < 3)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else if (chest > 1 && chest < 3)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else if (!string(usetext).empty())
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 0, true);

						return;
					}
					else
					{
						pHud->ShowInteractiveObjectInfo("", "", 0, false);
						return;
					}
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
					return;
				}
				else
				{
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
					return;
				}
			}
		}
		else
		{
			CurrentInteractiveId = 0;
			pHud->ShowInteractiveObjectInfo("", "", 0, false);
		}
	}
	else if (pPlayer->IsThirdPerson())
	{
		const EntityId collidedEntityId = pPlayer->GetGameObject()->GetWorldQuery()->GetLookAtEntityId();
		IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntity(collidedEntityId);
		if (pCollidedEntity)
		{
			Vec3 pos1 = pCollidedEntity->GetWorldPos();
			Vec3 pos2 = pPlayer->GetEntity()->GetWorldPos();
			if (pos2.GetDistance(pos1) > 2.0f)
			{
				if (interactionInfo.interactionType == eInteraction_None)
				{
					CurrentInteractiveId = 0;
					target_is_showed = false;
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
				}
				return;
			}
			stop_ncr = true;
			if (CurrentInteractiveId != collidedEntityId)
			{
				//CryLogAlways("Try to show interactive icon3");
				CurrentInteractiveId = collidedEntityId;
				target_is_showed = false;
				pHud->ShowInteractiveObjectInfo("", "", 0, false);
				stop_ncr = false;
			}
			
			if (stop_ncr)
			{
				//CryLogAlways("interactive icon3 is showed already");
				target_is_showed = pHud->isInteractiveInfoObjectShowed;
				return;
			}

			CActor  *pActorr = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId));
			if (pActorr)
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int chest = 0;
					props->GetValue("chest", chest);
					if (chest > 0 && chest < 2)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", "Open", 1, true);

						return;
					}
				}

				if (pActorr->IsDead())
				{
					if (!target_is_showed)
						pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", "Find Items", 1, true);

					return;
				}
			}
			else
			{
				SmartScriptTable props;
				IScriptTable* pScriptTable = pCollidedEntity->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					int door = 0;
					int chest = 0;
					props->GetValue("chest", chest);
					props->GetValue("door", door);
					const char *usetext;
					props->GetValue("UseMessage", usetext);
					if (door > 0 && door < 2)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else if (door > 1 && door < 3)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else if (chest > 1 && chest < 3)
					{
						if (!target_is_showed)
							pHud->ShowInteractiveObjectInfo("icons/free_icons_pcs/resized_to64px_1/I_Key01.png", usetext, 1, true);

						return;
					}
					else
					{
						pHud->ShowInteractiveObjectInfo("", "", 0, false);
						return;
					}
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
					return;
				}
				else
				{
					pHud->ShowInteractiveObjectInfo("", "", 0, false);
					return;
				}
			}
		}
		else
		{
			CurrentInteractiveId = 0;
			pHud->ShowInteractiveObjectInfo("", "", 0, false);
		}
	}
	else
	{
		/*if (interactionInfo.interactionType == eInteraction_None)
		{
			CurrentInteractiveId = 0;
			target_is_showed = false;
			pHud->ShowInteractiveObjectInfo("", "", 0, false);
		}*/
		pHud->ShowInteractiveObjectInfo("", "", 0, false);
		CurrentInteractiveId = 0;
		target_is_showed = false;
	}

	if (interactionInfo.interactionType == eInteraction_None)
	{
		CurrentInteractiveId = 0;
		target_is_showed = false;
		pHud->ShowInteractiveObjectInfo("", "", 0, false);
	}
}

void CActorActionsNew::LootSelectItem(int id)
{
	InventorySelectItem(id);
}

void CActorActionsNew::LootDeselectItem(int id)
{
	InventoryDeselectPressed(id);
}

void CActorActionsNew::LootItemPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;
	
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	//pHud->UpdateChestInventory(1);
	if (id >= 1)
	{
		EntityId idf = pGameGlobals->current_opened_Chest;
		CActor  *pActorchest = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(idf));
		if (pActorchest)
		{
			CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			if (!pItemii)
				return;

			IInventory *pInventorychest = pActorchest->GetInventory();
			if (!ChekForInventoryCapacity(pActor->GetEntityId()) && pItemii->GetItemType() != 100)
			{
				if (!pActor->IsRemote())
				{
					if (pHud)
						pHud->StatMsg("You Inventory is full, you can't take this item");
				}

				return;
			}
			for (int i = 1; i < eAESlots_Number; i++)
			{
				if (id == pActorchest->GetCurrentEquippedItemId(i))
				{
					pActorchest->EquipItem(id);
					pItemii->SetEquipped(0);
				}
			}

			bool no_ops = false;
			if (pItemii->GetItemType() == 100)
			{
				if (pItemii->GetItemQuanity() < 0)
				{
					pHud->ClearChestInventory();
					pHud->UpdateChestInventory(10);
					no_ops = true;
				}
			}
			//quick temportary fix
			if (!no_ops)
			{
				pActorchest->DropItem(id, 0.0f, false);
				if (pItemii->GetItemType() != 100)
				{
					pItemii->PickUp(pActor->GetEntityId(), false, false);
				}
				else
				{
					pInventorychest->RemoveItem(id);
					pActor->SetGold(pActor->GetGold() + pItemii->GetItemQuanity());
					pItemii->SetItemQuanity(-1);
					pItemii->RemoveEntity();
				}

			}
		}
		else
		{
			IInventory *pInventorychest = NULL;
			IEntity *pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(idf);
			if (pEntityc)
			{
				CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
				if (pChest)
				{
					pInventorychest = pChest->GetInventory();
				}
			}

			if (!pInventorychest)
				return;

			if (!ChekForInventoryCapacity(pActor->GetEntityId()))
			{
				if (pHud)
					pHud->StatMsg("You Inventory is full, you can't take this item");

				return;
			}
			CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			if (pItemii)
			{
				pInventorychest->RemoveItem(id);
				pItemii->SetOwnerId(0);
				pItemii->PickUp(pActor->GetEntityId(), false, false);
			}
		}
		pHud->ClearChestInventory();
		pHud->UpdateChestInventory(10);
	}
}

void CActorActionsNew::LootTakeAllItems()
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	EntityId idf = pGameGlobals->current_opened_Chest;
	CActor  *pActorchest = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(idf));
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	if (!pActorchest)
	{
		IInventory *pInventorychest = NULL;
		IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(idf);
		if (pEntityc)
		{
			CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
			if (pChest)
			{
				pInventorychest = pChest->GetInventory();
			}
		}

		if (!pInventorychest)
			return;

		std::list<EntityId> items_to_player;
		for (int i = 0; i < pInventorychest->GetCount(); i++)
		{
			EntityId id = pInventorychest->GetItem(i);
			CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			if (curItem && curItem->GetItemType() != 81)
			{
				items_to_player.push_back(id);
			}
		}
		std::list<EntityId>::iterator it_pl = items_to_player.begin();
		std::list<EntityId>::iterator end_pl = items_to_player.end();
		int count_pl_i = items_to_player.size();
		for (int i = 0; i < count_pl_i; i++, it_pl++)
		{

			EntityId ide = (*it_pl);
			CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
			if (curItem)
			{
				pInventorychest->RemoveItem(ide);
				curItem->SetOwnerId(0);
				curItem->PickUp(pActor->GetEntityId(), false, false);
			}
		}
	}
	else
	{
		IInventory *pInventorychest = pActorchest->GetInventory();
		if (!pInventorychest)
			return;

		std::list<EntityId> items_to_player;
		for (int i = 0; i < pInventorychest->GetCount(); i++)
		{
			EntityId id = pInventorychest->GetItem(i);
			CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			if (curItem && curItem->GetItemType() != 81)
			{
				items_to_player.push_back(id);
			}
		}
		std::list<EntityId>::iterator it_pl = items_to_player.begin();
		std::list<EntityId>::iterator end_pl = items_to_player.end();
		int count_pl_i = items_to_player.size();
		for (int i = 0; i < count_pl_i; i++, it_pl++)
		{

			EntityId ide = (*it_pl);
			CItem *curItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(ide));
			if (curItem)
			{
				for (int ic = 1; ic < eAESlots_Number; ic++)
				{
					if (ide == pActorchest->GetCurrentEquippedItemId(ic))
					{
						pActorchest->EquipItem(ide);
						curItem->SetEquipped(0);
					}
				}
				pActorchest->DropItem(ide, 0.0f, false);
				curItem->PickUp(pActor->GetEntityId(), false, false);
			}
		}
	}
	//pHud->ShowChestInv(false);
	pHud->StartDSChestInvTimer();
}

void CActorActionsNew::LootLooterSelectItem(int id)
{
	InventorySelectItem(id);
}

void CActorActionsNew::LootLooterDeselectItem(int id)
{
	InventoryDeselectPressed(id);
}

void CActorActionsNew::LootLooterItemPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	//pHud->UpdateChestInventory(1);
	if (id >= 1)
	{
		EntityId idf = pGameGlobals->current_opened_Chest;
		CActor  *pActorchest = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(idf));
		if (pActorchest)
		{
			IInventory *pInventorychest = pActorchest->GetInventory();
			if (!ChekForInventoryCapacity(pActorchest->GetEntityId()))
			{
				if (pHud)
					pHud->StatMsg("Chest store is full, you can't put this item to store");

				return;
			}
			IInventory *pInventory = pActor->GetInventory();
			IItem *pItem = gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id);
			CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
			IItemSystem	*m_pItemSystem = gEnv->pGame->GetIGameFramework()->GetIItemSystem();
			for (int i = 1; i < eAESlots_Number; i++)
			{
				if (id == pActorchest->GetCurrentEquippedItemId(i))
				{
					pActorchest->EquipItem(id);
					pItemii->SetEquipped(0);
				}
			}
			pActor->DropItem(id, 0.0f, false);
			pItemii->PickUp(pActorchest->GetEntityId(), false, false);

		}
		else
		{
			IInventory *pInventorychest = NULL;
			IEntity *pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(idf);
			if (pEntityc)
			{
				CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
				if (pChest)
				{
					pInventorychest = pChest->GetInventory();
					if (!pInventorychest)
						return;

					if (pChest->GetInvSlotsNum() <= pInventorychest->GetCount())
					{
						if (pHud)
							pHud->StatMsg("Chest store is full, you can't put this item to store");

						return;
					}

					if (CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id)))
					{
						pActor->DropItem(id, 0.0f, false);
						pInventorychest->AddItem(id);
						pItemii->SetOwnerId(pEntityc->GetId());
						pItemii->HideItem(true);
					}
				}
			}
		}
		pHud->ClearChestInventory();
		pHud->UpdateChestInventory(10);
	}
}

void CActorActionsNew::SellerSelectItem(int id)
{
	InventorySelectItem(id);
}

void CActorActionsNew::SellerDeselectItem(int id)
{
	InventoryDeselectPressed(id);
}

void CActorActionsNew::SellerItemPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (id >= 1)
	{
		CActor *pActortrader = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pGameGlobals->current_dialog_NPC));
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActortrader || !pActor)
			return;

		bool is_chest = false;
		IInventory *pInventorytrader = pActortrader->GetInventory();
		IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(pGameGlobals->current_traider_chest);
		if (pEntityc)
		{
			CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
			if (pChest)
			{
				pInventorytrader = pChest->GetInventory();
				is_chest = true;
			}
		}
		IInventory *pInventory = pActor->GetInventory();
		if (!pInventorytrader || !pInventory)
			return;

		if (!ChekForInventoryCapacity(pActor->GetEntityId()))
		{
			if (pHud)
				pHud->StatMsg("You Inventory is full, you can't buy this item");

			return;
		}
		CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (!pItemii)
			return;

		int gold_cost = CalculateItemGoldCost(pActortrader->GetEntityId(), pActor->GetEntityId(), pItemii->GetEntityId());
		if (pActor->GetGold() >= gold_cost && pItemii->GetItemMarkQuest()<1)
		{
			pActor->SetGold(pActor->GetGold() - gold_cost);
			pActortrader->SetGold(pActortrader->GetGold() + gold_cost);
			if (!is_chest)
				pActortrader->DropItem(id);
			else
			{
				pInventorytrader->RemoveItem(id);
				pItemii->SetOwnerId(0);
			}

			if(!pActor->PickUpItem(pItemii->GetEntityId(), false, false))
				pItemii->PickUp(pActor->GetEntityId(), false, false);

			pHud->ClearTradeSystem();
			pHud->UpdateTradeSystem(10);
		}
	}
}

void CActorActionsNew::BuyerSelectItem(int id)
{
	InventorySelectItem(id);
}

void CActorActionsNew::BuyerDeselectItem(int id)
{
	InventoryDeselectPressed(id);
}

void CActorActionsNew::BuyerItemPressed(int id)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	if (id >= 1)
	{
		CActor *pActortrader = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pGameGlobals->current_dialog_NPC));
		CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pActortrader || !pActor)
			return;

		bool is_chest = false;
		IInventory *pInventorytrader = pActortrader->GetInventory();
		IEntity * pEntityc = gEnv->pSystem->GetIEntitySystem()->GetEntity(pGameGlobals->current_traider_chest);
		if (pEntityc)
		{
			CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
			if (pChest)
			{
				pInventorytrader = pChest->GetInventory();
				is_chest = true;
			}
		}
		IInventory *pInventory = pActor->GetInventory();
		if (!pInventorytrader || !pInventory)
			return;

		if (!is_chest)
		{
			if (!ChekForInventoryCapacity(pActortrader->GetEntityId()))
			{
				if (pHud)
				{
					if (pActortrader->GetGender() == 0)
						pHud->StatMsg("Merchant Inventory is full, you can't sell this item to him");
					else
						pHud->StatMsg("Merchant Inventory is full, you can't sell this item to her");
				}
				return;
			}
		}
		else
		{
			if (pEntityc)
			{
				CChest *pChest = pGameGlobals->GetThisContainer(pEntityc->GetId());
				if (pChest)
				{
					if (pInventorytrader->GetCount() >= pChest->GetInvSlotsNum())
					{
						if (pHud)
						{
							if (!pActor->IsRemote())
							{
								if (pActortrader->GetGender() == 0)
									pHud->StatMsg("Merchant Store is full, you can't sell this item to him");
								else
									pHud->StatMsg("Merchant Store is full, you can't sell this item to her");
							}
						}
						return;
					}
				}
			}
		}
		CItem *pItemii = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(id));
		if (!pItemii)
			return;

		int gold_cost = CalculateItemGoldCost(pActor->GetEntityId(), pActortrader->GetEntityId(), pItemii->GetEntityId());
		if (pActortrader->GetGold() >= gold_cost && pItemii->GetItemMarkQuest() < 1 && pItemii->GetEquipped() < 1 && pItemii->GetItemCost() > 0)
		{
			pActortrader->SetGold(pActortrader->GetGold() - gold_cost);
			pActor->SetGold(pActor->GetGold() + gold_cost);
			pActor->DropItem(id);
			if (!is_chest)
			{
				if (!pActortrader->PickUpItem(pItemii->GetEntityId(), false, false))
					pItemii->PickUp(pActortrader->GetEntityId(), false, false);
			}
			else
			{
				pItemii->HideItem(true);
				pInventorytrader->AddItem(id);
				pItemii->SetOwnerId(pEntityc->GetId());
			}

			pHud->ClearTradeSystem();
			pHud->UpdateTradeSystem(10);
		}
	}
}

void CActorActionsNew::SpellSelected(string name)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if (name.empty() || name == string("undefined"))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pActor->m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pActor->m_actor_spells_book.end();
	int count = pActor->m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).spell_id > -1)
		{
			string spell_name = (*it).spell_name.c_str();
			if (spell_name == name)
			{
				pHud->ShowSpellInformation(true, true, (*it).spell_id);
				pGameGlobals->current_selected_spell = spell_name;
				pGameGlobals->current_selected_spell_ids = (*it).spell_id;
				break;
			}
		}
	}
}

void CActorActionsNew::SpellDeselected(string name)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	pHud->ShowSpellInformation(false, false, -1);
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	pGameGlobals->current_selected_spell = "";
	pGameGlobals->current_selected_spell_ids = -1;
}

void CActorActionsNew::SpellPressed(string name)
{
	if (name.empty() || name == string("undefined"))
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pActor->m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pActor->m_actor_spells_book.end();
	int count = pActor->m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it).spell_id > -1)
		{
			string spell_name = (*it).spell_name.c_str();
			if (spell_name == name)
			{
				if(Current_inventory_selection_mode == ePISMode_drop)
				{
					if ((*it).spell_id == pActor->GetSpellIdFromCurrentSpellsForUseSlot(1))
						pActor->DelSpellFromCurrentSpellsForUseSlot((*it).spell_id, 1);
					else
						pActor->AddSpellToCurrentSpellsForUseSlot((*it).spell_id, 1);

					if (g_pGameCVars->g_spell_system_enable_equip_one_spell_to_all_hands > 0)
						break;
				}
				else
				{
					if ((*it).spell_id == pActor->GetSpellIdFromCurrentSpellsForUseSlot(2))
						pActor->DelSpellFromCurrentSpellsForUseSlot((*it).spell_id, 2);
					else
						pActor->AddSpellToCurrentSpellsForUseSlot((*it).spell_id, 2);

					if (g_pGameCVars->g_spell_system_enable_equip_one_spell_to_all_hands > 0)
						break;
				}
				break;
			}
		}
	}
}

void CActorActionsNew::AbilityPressed(int id)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pActor)
	{
		CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
		if (!pGameGlobals)
			return;

		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (!pHud)
			return;

		XmlNodeRef node = pGameGlobals->CharacterPerks_node;//GetISystem()->LoadXmlFromFile(pGameGlobals->character_perks_path.c_str());
		if (!node)
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
			CryLogAlways("Abilitys xml not loaded.");
			return;
		}
		int real_id = id;
		XmlNodeRef paramsNode = node->findChild("params");
		XmlNodeRef infopathNode = node->findChild("info");
		XmlNodeRef perksNode = infopathNode->findChild("perks");
		int id;
		int rl_id;
		XmlString perk_icon_path;
		XmlString perk_description;
		int perk_parent_id;
		int perk_ct;
		int perk_grp;
		int perk_is_passive;
		int perk_simply_learn;
		int perk_pos_x;
		int perk_pos_y;
		//int perk_level;
		int perk_max_level;
		int perks_number;
		paramsNode->getAttr("perks_num", perks_number);
		for (int i = 0; i < perks_number; i++)
		{
			XmlNodeRef perkNode = perksNode->getChild(i);
			if (perkNode)
			{
				perkNode->getAttr("id", id);
				perkNode->getAttr("real_id", rl_id);
				perkNode->getAttr("perk_description", perk_description);
				perkNode->getAttr("icon", perk_icon_path);
				perkNode->getAttr("perk_parent_id", perk_parent_id);
				perkNode->getAttr("perk_ct", perk_ct);
				perkNode->getAttr("perk_grp", perk_grp);
				perkNode->getAttr("perk_is_passive", perk_is_passive);
				perkNode->getAttr("perk_simply_learn", perk_simply_learn);
				perkNode->getAttr("perk_pos_x", perk_pos_x);
				perkNode->getAttr("perk_pos_y", perk_pos_y);
				perkNode->getAttr("perk_max_level", perk_max_level);
				if (rl_id == real_id)
				{
					int lvl = pActor->GetPerkLevel(real_id);
					if (lvl < perk_max_level && g_pGameCVars->g_levelupabilitypoints>0)
					{
						if (perk_grp == 1 && !pActor->IsPerkActive(perk_parent_id))
							return;

						pActor->ActivatePerk(real_id, true, lvl + 1);
						g_pGameCVars->g_levelupabilitypoints -= 1;
						pHud->UpdatePerksVis(0.5);
					}
				}
			}
		}
	}
}

void CActorActionsNew::ProcessCombineItems(EntityId item_comb_Id_n1, EntityId item_comb_Id_n2)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	if(!pItemSystem)
		return;

	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGameGlobals)
		return;

	CItem *pItem_ft = pGameGlobals->GetThisItem(item_comb_Id_n1);
	CItem *pItem_st = pGameGlobals->GetThisItem(item_comb_Id_n2);
	if (!pItem_ft || !pItem_st)
		return;

	string class_name_ft = pItem_ft->GetEntity()->GetClass()->GetName();
	string class_name_st = pItem_st->GetEntity()->GetClass()->GetName();

	int hud_slot_old_pos = -1;
	if (pHud->InventoryUsePrimaryItemPos())
		hud_slot_old_pos = pItem_ft->GetHudItemPosPrimary();
	else
		hud_slot_old_pos = pItem_ft->GetHudItemPosSecondary();

	XmlNodeRef node = GetISystem()->LoadXmlFromFile(pGameGlobals->items_combinations_def.c_str());
	if (!node)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "xml not loaded.");
		CryLogAlways("Items combinations xml not loaded.");
		return;
	}

	XmlNodeRef combinationsNode = node->findChild("combinations");
	if(!combinationsNode)
		return;

	bool do_combine = false;
	for (int i = 0; i < combinationsNode->getChildCount(); i++)
	{
		XmlNodeRef pairNode = combinationsNode->getChild(i);
		if (pairNode)
		{
			bool items_equall[2];
			items_equall[0] = false;
			items_equall[1] = false;
			XmlString item_class_ft = "";
			XmlString item_class_st = "";
			pairNode->getAttr("item_class_ft", item_class_ft);
			pairNode->getAttr("item_class_st", item_class_st);
			if (item_class_ft == class_name_ft)
			{
				items_equall[0] = true;
			}

			if (item_class_st == class_name_st)
			{
				items_equall[1] = true;
			}

			if (item_class_st == class_name_ft)
			{
				items_equall[0] = true;
			}

			if (item_class_ft == class_name_st)
			{
				items_equall[1] = true;
			}

			if (items_equall[0] && items_equall[1])
			{
				XmlString new_item_class_name = "";
				pairNode->getAttr("new_item_class_name", new_item_class_name);
				do_combine = true;
				if (pItem_ft->GetOwnerActor())
				{
					int hud_slot_old_pos2 = -1;
					if (pHud->InventoryUsePrimaryItemPos())
						hud_slot_old_pos2 = pItem_st->GetHudItemPosPrimary();
					else
						hud_slot_old_pos2 = pItem_st->GetHudItemPosSecondary();

					pHud->DeleteItemVisualFromInventory(hud_slot_old_pos);
					pHud->DeleteItemVisualFromInventory(hud_slot_old_pos2);
					//--------------------Spawn new item and pick up this------------------------------------------------------
					EntityId ids = pItemSystem->GiveItem(pItem_ft->GetOwnerActor(), new_item_class_name, false, false, false);
					CItem *pItem_end = pGameGlobals->GetThisItem(ids);
					if (pItem_end)
					{
						if (pHud->InventoryUsePrimaryItemPos())
							pItem_end->SetHudItemPosPrimary(hud_slot_old_pos);
						else
							pItem_end->SetHudItemPosSecondary(hud_slot_old_pos);
					}
				}
			}
		}
	}
	
	if (do_combine)
	{
		pHud->UpdateInventory(1.0f);
	}
}

void CActorActionsNew::InventoryUpdatePlayerDoll(EntityId id)
{
	CActor *pActor = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pActor)
		return;

	IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id);
	if (pEntity)
	{
		ICharacterInstance* pCharacter = pEntity->GetCharacter(0);
		if (!pCharacter)
			return;

		ICharacterInstance* pCharacter_player = pActor->GetEntity()->GetCharacter(0);
		if (!pCharacter_player)
			return;

		for (int i = 0; i < eAESlots_Number; i++)
		{
			CItem *pItem_this = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentEquippedItemId(i)));
			if (pItem_this)
			{
				if (!pItem_this->CanSelect())
				{
					bool gender = true;
					if (pActor->GetGender() == 1)
						gender = false;

					for (int i = 0; i < pItem_this->GetNumArmorParts(gender); i++)
					{
						string arm_pth = pItem_this->GetArmorModelPath(i);
						if (gender)
							arm_pth = pItem_this->GetArmorMaleModelPath(i);

						if (0 == stricmp(PathUtil::GetExt(arm_pth), "xml"))
						{
							if(pCharacter->GetIAttachmentManager())
								pCharacter->GetIAttachmentManager()->LoadAttachmentList(arm_pth, false, false);
						}
					}
				}
			}
		}

		const int max_attach = 175;
		//IAttachment **pAttacments_player[max_attach];
		IAttachment *pAttacments_player[max_attach];
		string att_names[max_attach];
		int att_num = pCharacter_player->GetIAttachmentManager()->GetAttachmentCount();
		if (att_num > max_attach)
			att_num = max_attach;

		for (int i = 0; i < att_num; i++)
		{
			pAttacments_player[i] = pCharacter_player->GetIAttachmentManager()->GetInterfaceByIndex(i);
			IAttachment *pAttacments_player_nrm = pAttacments_player[i];
			att_names[i] = pAttacments_player_nrm->GetName();
			IAttachment *pAttacments_doll_nrm = pCharacter->GetIAttachmentManager()->GetInterfaceByName(att_names[i].c_str());
			if (pAttacments_doll_nrm)
			{
				if (att_names[i] == string("armorshield"))
				{
					EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Shield);
					CItem *pItem_shield = pActor->GetItem(ide);
					if (pItem_shield)
					{
						pAttacments_doll_nrm->ClearBinding();
						if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem_shield->GetArmorModelPath(0)))
						{
							CCGFAttachment *pCGFAttachment = new CCGFAttachment();
							pCGFAttachment->pObj = pStatObj;
							pAttacments_doll_nrm->AddBinding(pCGFAttachment);
							pAttacments_doll_nrm->UpdateAttModelRelative();
							continue;
						}
					}
					else
						pAttacments_doll_nrm->ClearBinding();

					//CryLogAlways("armorshield attachment on pl model doll");
					continue;
				}
				else if (att_names[i] == string("weapon"))
				{
					EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Weapon);
					CItem *pItem_wpn = pActor->GetItem(ide);
					if (pItem_wpn)
					{
						pAttacments_doll_nrm->ClearBinding();
						const char* fileExt = PathUtil::GetExt(pItem_wpn->GetArmorModelPath(0));
						if (0 == stricmp(fileExt, "cgf"))
						{
							if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem_wpn->GetArmorModelPath(0), NULL, NULL, false))
							{
								CCGFAttachment* pCGFAttachment = new CCGFAttachment();
								pCGFAttachment->pObj = pStatObj;
								pAttacments_doll_nrm->AddBinding(pCGFAttachment);
								pAttacments_doll_nrm->UpdateAttModelRelative();
								continue;
							}
						}
						else if (0 == stricmp(fileExt, "skin"))
						{
							/*if (IAttachment* pAttach = pCharacter->GetIAttachmentManager()->GetInterfaceByNameCRC(params.attachmentName.crc))
							{
								if (IAttachmentSkin* pSkinAttach = pAttach->GetIAttachmentSkin())
								{
									if (ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(params.objectFilename.c_str(), 0))
									{
										CSKINAttachment* pSkinAttachment = new CSKINAttachment();
										pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
										pAttachment->AddBinding(pSkinAttachment, pSkin, 0);
									}
								}
							}*/
							continue;
						}
						else if (ICharacterInstance *m_pAttachedCharInst = gEnv->pCharacterManager->CreateInstance(pItem_wpn->GetArmorModelPath(0)))
						{
							CSKELAttachment *pChrAttachment = new CSKELAttachment();
							pChrAttachment->m_pCharInstance = m_pAttachedCharInst;
							pAttacments_doll_nrm->AddBinding(pChrAttachment);
							pAttacments_doll_nrm->UpdateAttModelRelative();
							continue;
						}
					}
					else
						pAttacments_doll_nrm->ClearBinding();

					continue;
				}
				else if (att_names[i] == string("left_weapon"))
				{
					EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Weapon_Left);
					CItem *pItem_wpn = pActor->GetItem(ide);
					if (pItem_wpn)
					{
						pAttacments_doll_nrm->ClearBinding();
						const char* fileExt = PathUtil::GetExt(pItem_wpn->GetArmorModelPath(0));
						if (0 == stricmp(fileExt, "cgf"))
						{
							if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem_wpn->GetArmorModelPath(0), NULL, NULL, false))
							{
								CCGFAttachment* pCGFAttachment = new CCGFAttachment();
								pCGFAttachment->pObj = pStatObj;
								pAttacments_doll_nrm->AddBinding(pCGFAttachment);
								pAttacments_doll_nrm->UpdateAttModelRelative();
								continue;
							}
						}
						else if (0 == stricmp(fileExt, "skin"))
						{
							/*if (IAttachment* pAttach = pCharacter->GetIAttachmentManager()->GetInterfaceByNameCRC(params.attachmentName.crc))
							{
							if (IAttachmentSkin* pSkinAttach = pAttach->GetIAttachmentSkin())
							{
							if (ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(params.objectFilename.c_str(), 0))
							{
							CSKINAttachment* pSkinAttachment = new CSKINAttachment();
							pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
							pAttachment->AddBinding(pSkinAttachment, pSkin, 0);
							}
							}
							}*/
							continue;
						}
						else if (ICharacterInstance *m_pAttachedCharInst = gEnv->pCharacterManager->CreateInstance(pItem_wpn->GetArmorModelPath(0)))
						{
							CSKELAttachment *pChrAttachment = new CSKELAttachment();
							pChrAttachment->m_pCharInstance = m_pAttachedCharInst;
							pAttacments_doll_nrm->AddBinding(pChrAttachment);
							pAttacments_doll_nrm->UpdateAttModelRelative();
							continue;
						}
					}
					else
						pAttacments_doll_nrm->ClearBinding();

					continue;
				}
				else if (att_names[i] == string("torch"))
				{
					EntityId ide = pActor->GetCurrentEquippedItemId(eAESlot_Torch);
					CItem *pItem_wpn = pActor->GetItem(ide);
					if (pItem_wpn)
					{
						pAttacments_doll_nrm->ClearBinding();
						const char* fileExt = PathUtil::GetExt(pItem_wpn->GetArmorModelPath(0));
						if (0 == stricmp(fileExt, "cgf"))
						{
							if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem_wpn->GetArmorModelPath(0), NULL, NULL, false))
							{
								CCGFAttachment* pCGFAttachment = new CCGFAttachment();
								pCGFAttachment->pObj = pStatObj;
								pAttacments_doll_nrm->AddBinding(pCGFAttachment);
								pAttacments_doll_nrm->UpdateAttModelRelative();
								continue;
							}
						}
						else if (0 == stricmp(fileExt, "skin"))
						{
							/*if (IAttachment* pAttach = pCharacter->GetIAttachmentManager()->GetInterfaceByNameCRC(params.attachmentName.crc))
							{
							if (IAttachmentSkin* pSkinAttach = pAttach->GetIAttachmentSkin())
							{
							if (ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(params.objectFilename.c_str(), 0))
							{
							CSKINAttachment* pSkinAttachment = new CSKINAttachment();
							pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
							pAttachment->AddBinding(pSkinAttachment, pSkin, 0);
							}
							}
							}*/
						}
						else if (ICharacterInstance *m_pAttachedCharInst = gEnv->pCharacterManager->CreateInstance(pItem_wpn->GetArmorModelPath(0)))
						{
							CSKELAttachment *pChrAttachment = new CSKELAttachment();
							pChrAttachment->m_pCharInstance = m_pAttachedCharInst;
							pAttacments_doll_nrm->AddBinding(pChrAttachment);
							pAttacments_doll_nrm->UpdateAttModelRelative();
							continue;
						}
					}
					else
						pAttacments_doll_nrm->ClearBinding();

					continue;
				}

				pAttacments_doll_nrm->ClearBinding();
				if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj())
				{
					if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj()->GetFilePath()))
					{
						CCGFAttachment *pCGFAttachment = new CCGFAttachment();
						pCGFAttachment->pObj = pStatObj;
						pAttacments_doll_nrm->AddBinding(pCGFAttachment);
						pAttacments_doll_nrm->UpdateAttModelRelative();
					}
				}

				if (pAttacments_player_nrm->GetIAttachmentSkin() && pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin())
				{
					ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin()->GetModelFilePath(), 0);
					if (pSkin)
					{
						IAttachmentSkin* pSkinAttach = pAttacments_doll_nrm->GetIAttachmentSkin();
						if (pSkinAttach)
						{
							CSKINAttachment* pSkinAttachment = new CSKINAttachment();
							pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
							pAttacments_doll_nrm->AddBinding(pSkinAttachment, pSkin, 0);
							pAttacments_doll_nrm->HideAttachment(0);
							pAttacments_doll_nrm->UpdateAttModelRelative();
						}
					}
				}

				if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance())
				{
					CSKELAttachment *pChrAttachment = new CSKELAttachment();
					pChrAttachment->m_pCharInstance = pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance();
					IAttachmentObject* pNewAttachment = NULL;
					pNewAttachment = pChrAttachment;
					pAttacments_doll_nrm->AddBinding(pNewAttachment);
					pAttacments_doll_nrm->HideAttachment(0);
					pAttacments_doll_nrm->UpdateAttModelRelative();
				}

				if (pAttacments_player_nrm->GetIAttachmentObject() && 
					pAttacments_player_nrm->GetIAttachmentObject()->GetAttachmentType() == 4)
				{
					CItem *pItem_sel = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
					if (pItem_sel && pItem_sel->IsAttachedToHand())
					{
						IAttachmentObject* pNewAttachment = NULL;
						SEntitySlotInfo slotInfo;
						bool validSlot = pItem_sel->GetEntity()->GetSlotInfo(eIGS_ThirdPerson, slotInfo);
						if (validSlot)
						{
							if (slotInfo.pCharacter)
							{
								CSKELAttachment *pChrAttachment = new CSKELAttachment();
								pChrAttachment->m_pCharInstance = slotInfo.pCharacter;
								pNewAttachment = pChrAttachment;
							}
							else if (slotInfo.pStatObj)
							{
								CCGFAttachment* pCGFAttachment = new CCGFAttachment();
								pCGFAttachment->pObj = slotInfo.pStatObj;
								pCGFAttachment->m_pReplacementMaterial = slotInfo.pMaterial;
								pNewAttachment = pCGFAttachment;
							}
						}

						if (pNewAttachment)
						{
							pAttacments_doll_nrm->AddBinding(pNewAttachment);
							pAttacments_doll_nrm->HideAttachment(0);
							pAttacments_doll_nrm->UpdateAttModelRelative();
						}
					}
				}
				continue;
				//pCharacter->GetIAttachmentManager()->RemoveAttachmentByName(att_names[i].c_str());
			}

			if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance())
			{
				IDefaultSkeleton* pIDefaultSkeleton = &pCharacter_player->GetIDefaultSkeleton();
				string bone = pIDefaultSkeleton->GetJointNameByID(pAttacments_player_nrm->GetJointID());
				IAttachment *pAttacments_doll_new = pCharacter->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_BONE, bone.c_str(), true);
				CSKELAttachment *pChrAttachment = new CSKELAttachment();
				pChrAttachment->m_pCharInstance = pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance();
				IAttachmentObject* pNewAttachment = NULL;
				pNewAttachment = pChrAttachment;
				pAttacments_doll_new->AddBinding(pNewAttachment);
				pAttacments_doll_new->HideAttachment(0);
				pAttacments_doll_nrm->UpdateAttModelRelative();
			}

			if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj())
			{
				IDefaultSkeleton* pIDefaultSkeleton = &pCharacter_player->GetIDefaultSkeleton();
				string bone = pIDefaultSkeleton->GetJointNameByID(pAttacments_player_nrm->GetJointID());
				IAttachment *pAttacments_doll_new = pCharacter->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_BONE, bone.c_str(), true);
				if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj()->GetFilePath()))
				{
					CCGFAttachment *pCGFAttachment = new CCGFAttachment();
					pCGFAttachment->pObj = pStatObj;
					pAttacments_doll_new->AddBinding(pCGFAttachment);
					pAttacments_doll_new->AlignJointAttachment();
					pAttacments_doll_new->UpdateAttModelRelative();
				}
			}

			if (pAttacments_player_nrm->GetIAttachmentSkin() && pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin())
			{
				IAttachment *pAttacments_doll_new = pCharacter->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_SKIN, 0, false);
				ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin()->GetModelFilePath(), 0);
				if (pSkin)
				{
					IAttachmentSkin* pSkinAttach = pAttacments_doll_new->GetIAttachmentSkin();
					if (pSkinAttach)
					{
						CSKINAttachment* pSkinAttachment = new CSKINAttachment();
						pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
						pAttacments_doll_new->AddBinding(pSkinAttachment, pSkin, 0);
						pAttacments_doll_new->HideAttachment(0);
					}
				}
			}
		}
	}
}

void CActorActionsNew::InventoryUpdateObjectAttachmentsOnPLdoll(EntityId id)
{
	IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id);
	if (pEntity)
	{
		ICharacterInstance* pCharacter = pEntity->GetCharacter(0);
		if (!pCharacter)
			return;

		const int att_num = pCharacter->GetIAttachmentManager()->GetAttachmentCount();
		for (int i = 0; i < att_num; i++)
		{
			IAttachment *pAttacments_player_nrm = pCharacter->GetIAttachmentManager()->GetInterfaceByIndex(i);
			if (pAttacments_player_nrm)
			{
				if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj())
				{
					pAttacments_player_nrm->UpdateAttModelRelative();
				}

				if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance())
				{
					pAttacments_player_nrm->UpdateAttModelRelative();
				}
			}
		}
	}
}

bool CActorActionsNew::PlayerCanInterInThirdPerson(EntityId id)
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	if (!pPlayer)
		return false;

	if (pPlayer->IsOnLedge())
		return false;

	if (pPlayer->GetRelaxedMod())
		return true;

	if (!pPlayer->CanDoAnyActionWNeededTwoHands())
		return false;

	CItem *pItem_wpn_s = pPlayer->GetItem(pPlayer->GetCurrentItemId());
	if (pItem_wpn_s)
	{
		if (pItem_wpn_s->GetItemType() != 81)
		{
			if (pItem_wpn_s->GetWeaponType() == 9)
			{
				//CBow *pBow = static_cast<CBow*>(g_pGame->GetIGameFramework()->QueryGameObjectExtension(pItem_wpn_s->GetEntityId(), "Bow"));
				CBow *pBow = static_cast<CBow*>(pItem_wpn_s);
				if (pBow)
				{
					if (pBow->process_arrow_load_started || pBow->hold_started)
					{
						return false;
					}
					else
					{
						pBow->arrow_loaded = false;
						pBow->DeleteArrowAttachment();
					}
				}
			}

			if (pItem_wpn_s->IsBusy() || pItem_wpn_s->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
			{
				return false;
			}
		}
	}
	return true;
}

bool CActorActionsNew::UpdatePlayerShadowCharacter(EntityId id)
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(id));
	if (!pPlayer)
		return false;

	if (!pPlayer->IsPlayer())
		return false;

	string base_att_eye_right = "eyergt";
	string base_att_eye_left = "eyelft";
	string base_att_head = "headface";
	string base_att_hair = "hair";
	string base_att_weapon = "weapon";
	CGameXMLSettingAndGlobals *pGameGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (pGameGlobals)
	{
		if (!pGameGlobals->customization_data.character_base_attachment_eyes[0].empty())
		{
			base_att_eye_right = pGameGlobals->customization_data.character_base_attachment_eyes[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_eyes[1].empty())
		{
			base_att_eye_left = pGameGlobals->customization_data.character_base_attachment_eyes[1];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[0].empty())
		{
			base_att_head = pGameGlobals->customization_data.character_base_attachment_head[0];
		}

		if (!pGameGlobals->customization_data.character_base_attachment_head[1].empty())
		{
			base_att_hair = pGameGlobals->customization_data.character_base_attachment_head[1];
		}
	}

	ICharacterInstance* pCharacter_shadow = pPlayer->GetShadowCharacter();
	if (!pCharacter_shadow)
		return false;

	ICharacterInstance* pCharacter_player = pPlayer->GetEntity()->GetCharacter(0);
	if (!pCharacter_player)
		return false;

	bool pl_viev = true;
	if (pPlayer->IsThirdPerson())
		pl_viev = false;

	//if (pl_viev)
	//	pPlayer->m_torsoAimIK.Disable(true);

	const int max_attach = 175;
	//IAttachment **pAttacments_player[max_attach];
	IAttachment *pAttacments_player[max_attach];
	string att_names[max_attach];
	int att_num = pCharacter_player->GetIAttachmentManager()->GetAttachmentCount();
	for (int i = 0; i < att_num; i++)
	{
		pAttacments_player[i] = pCharacter_player->GetIAttachmentManager()->GetInterfaceByIndex(i);
		IAttachment *pAttacments_player_nrm = pAttacments_player[i];
		att_names[i] = pAttacments_player_nrm->GetName();
//head fix-------------------------------------------------------------------------------------------------------------------------------------------------------
		const char *ppThirdPersonPartsRestricted[] = { base_att_head.c_str(), base_att_eye_right.c_str(), base_att_eye_left.c_str(), base_att_hair.c_str(), base_att_weapon.c_str() };
		const int ppThirdPersonPartsRestrictedNum = CRY_ARRAY_COUNT(ppThirdPersonPartsRestricted);
		bool contrd = false;
		for (int ic = 0; ic < ppThirdPersonPartsRestrictedNum; ic++)
		{
			if (!strcmp(ppThirdPersonPartsRestricted[ic], pAttacments_player_nrm->GetName()))
			{
				contrd = true;
			}
		}

		if (!contrd)
		{
			pAttacments_player_nrm->HideInRecursion(pl_viev);
			pAttacments_player_nrm->HideInShadow(pl_viev);
		}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------
		IAttachment *pAttacments_doll_nrm = pCharacter_shadow->GetIAttachmentManager()->GetInterfaceByName(att_names[i].c_str());
		if (pAttacments_doll_nrm)
		{
			pAttacments_doll_nrm->ClearBinding();
			if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj())
			{
				if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj()->GetFilePath()))
				{
					CCGFAttachment *pCGFAttachment = new CCGFAttachment();
					pCGFAttachment->pObj = pStatObj;
					pAttacments_doll_nrm->AddBinding(pCGFAttachment);
				}
			}

			if (pAttacments_player_nrm->GetIAttachmentSkin() && pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin())
			{
				ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin()->GetModelFilePath(), 0);
				if (pSkin)
				{
					IAttachmentSkin* pSkinAttach = pAttacments_doll_nrm->GetIAttachmentSkin();
					if (pSkinAttach)
					{
						CSKINAttachment* pSkinAttachment = new CSKINAttachment();
						pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
						pAttacments_doll_nrm->AddBinding(pSkinAttachment, pSkin, 0);
					}
				}
			}

			if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance())
			{
				CSKELAttachment *pChrAttachment = new CSKELAttachment();
				pChrAttachment->m_pCharInstance = pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance();
				IAttachmentObject* pNewAttachment = NULL;
				pNewAttachment = pChrAttachment;
				pAttacments_doll_nrm->AddBinding(pNewAttachment);
			}
			pAttacments_doll_nrm->HideAttachment(1);
			pAttacments_doll_nrm->HideInRecursion(!pl_viev);
			pAttacments_doll_nrm->HideInShadow(!pl_viev);
			continue;
		}

		if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance())
		{
			IDefaultSkeleton* pIDefaultSkeleton = &pCharacter_player->GetIDefaultSkeleton();
			string bone = pIDefaultSkeleton->GetJointNameByID(pAttacments_player_nrm->GetJointID());
			IAttachment *pAttacments_doll_new = pCharacter_shadow->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_BONE, bone.c_str(), true);
			CSKELAttachment *pChrAttachment = new CSKELAttachment();
			pChrAttachment->m_pCharInstance = pAttacments_player_nrm->GetIAttachmentObject()->GetICharacterInstance();
			IAttachmentObject* pNewAttachment = NULL;
			pNewAttachment = pChrAttachment;
			pAttacments_doll_new->AddBinding(pNewAttachment);
			pAttacments_doll_new->HideAttachment(1);
			pAttacments_doll_new->HideInRecursion(!pl_viev);
			pAttacments_doll_new->HideInShadow(!pl_viev);
		}

		if (pAttacments_player_nrm->GetIAttachmentObject() && pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj())
		{
			IDefaultSkeleton* pIDefaultSkeleton = &pCharacter_player->GetIDefaultSkeleton();
			string bone = pIDefaultSkeleton->GetJointNameByID(pAttacments_player_nrm->GetJointID());
			IAttachment *pAttacments_doll_new = pCharacter_shadow->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_BONE, bone.c_str(), true);
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAttacments_player_nrm->GetIAttachmentObject()->GetIStatObj()->GetFilePath()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pAttacments_doll_new->AddBinding(pCGFAttachment);
				pAttacments_doll_new->AlignJointAttachment();
				pAttacments_doll_new->HideAttachment(1);
				pAttacments_doll_new->HideInRecursion(!pl_viev);
				pAttacments_doll_new->HideInShadow(!pl_viev);
			}
		}

		if (pAttacments_player_nrm->GetIAttachmentSkin() && pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin())
		{
			IAttachment *pAttacments_doll_new = pCharacter_shadow->GetIAttachmentManager()->CreateAttachment(att_names[i].c_str(), CA_SKIN, 0, false);
			ISkin* pSkin = gEnv->pCharacterManager->LoadModelSKIN(pAttacments_player_nrm->GetIAttachmentSkin()->GetISkin()->GetModelFilePath(), 0);
			if (pSkin)
			{
				IAttachmentSkin* pSkinAttach = pAttacments_doll_new->GetIAttachmentSkin();
				if (pSkinAttach)
				{
					CSKINAttachment* pSkinAttachment = new CSKINAttachment();
					pSkinAttachment->m_pIAttachmentSkin = pSkinAttach;
					pAttacments_doll_new->AddBinding(pSkinAttachment, pSkin, 0);
					pAttacments_doll_new->HideAttachment(1);
					pAttacments_doll_new->HideInRecursion(!pl_viev);
					pAttacments_doll_new->HideInShadow(!pl_viev);
				}
			}
		}
	}

	//if (pl_viev)
	//	pPlayer->m_torsoAimIK.Enable(true);

	return true;
}

bool CActorActionsNew::PlayerCanDoAnyAgressiveActions()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return false;

	if (pPlayer->IsDead())
		return false;

	if (pPlayer->GetSAPlayTime() > 0.0f)
		return false;

	if (pPlayer->GetRelaxedMod())
		return false;

	if (pPlayer->IsOnLedge() || pPlayer->IsOnLadder())
		return false;
	//if (!pPlayer->CanDoAnyActionInCastStateWithSWeapon(pPlayer->GetCurrentItemId()))
	//	return false;

	if (IsAnyInteractiveUIOpened())
		return false;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pPlayer->GetCurrentItemId()));
	if (!pItem)
		return false;

	if (pItem->IsWpnTwoHanded())
	{
		if (!pPlayer->CanDoAnyActionWNeededTwoHands())
		{
			return false;
		}
	}

	if (pPlayer->GetSpecRecoilAct(eARMode_recoil_dodge) > 0.0f)
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return false;
		}

		float rc_time_et = (g_pGameCVars->g_dodge_delay_debug + g_pGameCVars->g_dodge_time_countine) - pPlayer->GetSpecRecoilAct(eARMode_recoil_dodge);
		if ((g_pGameCVars->g_dodge_time_countine + 0.1f) > rc_time_et)
			return false;
	}

	/*EMovementControlMethod horizMCM = pPlayer->GetAnimatedCharacter()->GetMCMH();
	//CryLogAlways("EMovementControlMethod %i", horizMCM);
	if (horizMCM == eMCM_AnimationHCollision || horizMCM == eMCM_Animation || horizMCM == eMCM_DecoupledCatchUp)
	{
		return false;
	}*/

	if(g_pGame->GetGameXMLSettingAndGlobals() && g_pGame->GetGameXMLSettingAndGlobals()->IsCharacterCreatingVis())
		return false;

	if (g_pGame->GetHUDCommon() && g_pGame->GetHUDCommon()->m_PostInitScreenVisible)
		return false;

	return true;
}

void CActorActionsNew::UpdatePlShadowChr(void * pUserData, IGameFramework::TimerID handler)
{
	if (!gEnv->pGame)
		return;

	if (!gEnv->pGame->GetIGameFramework())
		return;

	if (m_Timer_PL_Shadow_Update)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_Timer_PL_Shadow_Update);
		m_Timer_PL_Shadow_Update = NULL;
	}

	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pPlayer)
	{
		UpdatePlayerShadowCharacter(pPlayer->GetEntityId());
	}
}

void CActorActionsNew::DelayedUpdatePlShadowChr(float time)
{
	if (!gEnv->pGame)
		return;

	if (!gEnv->pGame->GetIGameFramework())
		return;

	if (m_Timer_PL_Shadow_Update)
	{
		gEnv->pGame->GetIGameFramework()->RemoveTimer(m_Timer_PL_Shadow_Update);
		m_Timer_PL_Shadow_Update = NULL;
	}

	if (!m_Timer_PL_Shadow_Update)
		m_Timer_PL_Shadow_Update = gEnv->pGame->GetIGameFramework()->AddTimer(CTimeValue(time), false, functor(*this, &CActorActionsNew::UpdatePlShadowChr), NULL);
}

bool CActorActionsNew::IsAnyInteractiveUIOpened()
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return false;

	return pHud->AnyInteractiveUIOpened();
	/*if (pHud->ChrMenu_loaded || pHud->Character_Creation_Menu_Loaded || pHud->Inv_loaded || pHud->m_SpellSelectionVisible ||
		pHud->m_DialogSystemAltVisible || pHud->m_DialogSystemVisible || pHud->m_CharacterCreationVisible ||
		pHud->m_BookVisible || pHud->m_QuestSystemVisible || pHud->m_TradeSystemVisible ||
		pHud->m_CharacterPerksMenuVisible || pHud->m_CharacterSkillsMenuVisible ||
		pHud->m_ChestInvVisible)
		return true;
	else
		return false;*/
}

void CActorActionsNew::CloseAnyInteractiveUIOpened()
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (!pHud)
		return;

	if (pHud->m_MainMapVisible)
	{
		pHud->ShowMainMap(false);
	}
	if (pHud->ChrMenu_loaded)
	{
		pHud->ShowCharacterMenu();
	}
	if (pHud->Character_Creation_Menu_Loaded)
	{
		pHud->ShowCharacterCreatingMenu();
	}
	if (pHud->Inv_loaded)
	{
		if (pHud->Inv_cont_menu_loaded)
		{
			pHud->ShowInvContextMenu(false);
		}
		pHud->ShowInventory();
	}
	if (pHud->m_SpellSelectionVisible)
	{
		pHud->ShowSpellSelectionMenu(false);
	}
	if (pHud->m_DialogSystemAltVisible)
	{
		pHud->ShowDialogSystemAlt(false);
	}
	if (pHud->m_DialogSystemVisible)
	{
		pHud->ShowDialog(false, "", 0);
	}
	if (pHud->m_CharacterCreationVisible)
	{
		//pHud->ShowCharacterCreatingMenu();
	}
	if (pHud->m_BookVisible)
	{
		pHud->ShowBook(false, 0);
	}
	if (pHud->m_QuestSystemVisible)
	{
		pHud->ShowQuestSystem(false);
	}
	if (pHud->m_TradeSystemVisible)
	{
		pHud->ShowTradeSystem(0, false);
	}
	if (pHud->m_CharacterPerksMenuVisible)
	{
		pHud->ShowChPerks(false);
	}
	if (pHud->m_CharacterSkillsMenuVisible)
	{
		pHud->ShowChSkills(false);
	}
	if (pHud->m_ChestInvVisible)
	{
		pHud->ShowChestInv(false);
	}

	if (g_pGameCVars->hud_disable_all_main_ui_on_player_death > 0)
	{
		if (pHud->MainHud_loaded)
		{
			pHud->ShowMainHud();
		}
	}

	if (pHud->m_QuickSelectionVisible)
		pHud->ShowQuickSelectionMenu(false);
}

bool CActorActionsNew::ActionSpectacularKillRequest(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	CPlayer *pPlayer = static_cast<CPlayer*>(pActor);
	if (!pPlayer)
		return false;

	float ray_length = 2.0f;
	//ray_hit rayhit;
	//unsigned int flags = rwi_stop_at_pierceable | rwi_colltype_any;
	CCamera& cam = GetISystem()->GetViewCamera();
	Vec3 cam_pos(0, 0, 0);
	Vec3 cam_dir(0, 0, 0);
	IPhysicalEntity *pIgnore = pPlayer ? pPlayer->GetEntity()->GetPhysics() : 0;
	cam_pos = cam.GetPosition();
	cam_dir = cam.GetViewdir()*ray_length;

	if (!gEnv->pPhysicalWorld)
		return false;


	//if (gEnv->pPhysicalWorld->RayWorldIntersection(cam_pos, cam_dir, ent_all, flags, &rayhit, 1, &pIgnore, pIgnore ? 1 : 0))
	//{
	primitives::cylinder cyl;
	cyl.r = 0.15f;
	cyl.axis = cam_dir;
	cyl.hh = ray_length *0.5f;
	cyl.center = cam_pos + cam.GetViewdir().GetNormalized()*cyl.hh;

	float n = 0.0f;
	geom_contact *contacts;
	intersection_params params;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = true;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO),
		ent_living | ent_independent, &contacts, 0,
		geom_colltype_foliage | geom_colltype_player, &params, 0, 0, &pIgnore, pIgnore ? 1 : 0);

	geom_contact *currentc = contacts;
	if (currentc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(currentc->iPrim[0]);//rayhit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (pCollidedEntity)
		{
			CActor* pTargetActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
			if (pTargetActor)
			{
				//CryLogAlways("CActorActionsNew::ActionSpectacularKillRequest called, seq 4");
				if (pTargetActor->GetHealth() <= 10.0f && !pTargetActor->IsFallen())
				{
					//CryLogAlways("CActorActionsNew::ActionSpectacularKillRequest called, seq 5");
					return pPlayer->RequestSpectacularKillNv(collidedEntityId, 5);
				}
				else if (pTargetActor->IsFallen())
				{
					//CryLogAlways("CActorActionsNew::ActionSpectacularKillRequest called, seq 6");
					return pPlayer->RequestSpectacularKillNv(collidedEntityId, 6);
				}
			}
		}
	}
	return false;
}

void CActorActionsNew::OnMeleeHitToSurface(EntityId actorId, int surfaceId, bool isOnTarget)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	if(!pActor->IsPlayer() || pActor->IsRemote())
		return;

	CPlayer *pPlayer = static_cast<CPlayer*>(pActor);
	if (!pPlayer)
		return;

	if (pl_view_shake_timer_x <= 0.0f)
	{
		if (!isOnTarget && g_pGameCVars->g_melee_system_player_camera_shake_on_hit_to_static_surfaces > 0)
		{
			float frc_cx = g_pGameCVars->g_melee_system_player_cam_sh_force_htss;
			if (pPlayer->IsThirdPerson())
				frc_cx = g_pGameCVars->g_melee_system_player_cam_tpv_sh_force_htss;

			float angle_cx = cry_random(-19.5f,19.5f) * frc_cx;
			float shift_cx = cry_random(0.03f, 0.08f) * frc_cx;
			float freq_cx = cry_random(0.04f, 0.2f) * frc_cx;
			float time_cx = cry_random(0.2f, 0.8f);
			pl_view_shake_timer_x = time_cx + g_pGameCVars->g_melee_system_player_camera_shake_add_timer;
			pPlayer->CameraShake(angle_cx, shift_cx, time_cx, freq_cx, Vec3(ZERO), pl_view_shake_ids_x, "");
			pl_view_shake_ids_x += 1;
			if (pl_view_shake_ids_x > 500)
				pl_view_shake_ids_x = 56;
		}
		else if (g_pGameCVars->g_melee_system_player_camera_shake_on_hit_to_rigid_surfaces > 0)
		{
			float frc_cx = g_pGameCVars->g_melee_system_player_cam_sh_force_htrs;
			if(pPlayer->IsThirdPerson())
				frc_cx = g_pGameCVars->g_melee_system_player_cam_tpv_sh_force_htrs;

			float angle_cx = cry_random(-12.5f,12.5f) * frc_cx;
			float shift_cx = cry_random(0.03f, 0.08f) * frc_cx;
			float freq_cx = cry_random(0.04f, 0.2f) * frc_cx;
			float time_cx = cry_random(0.2f, 0.8f);
			pl_view_shake_timer_x = time_cx + g_pGameCVars->g_melee_system_player_camera_shake_add_timer;
			pPlayer->CameraShake(angle_cx, shift_cx, time_cx, freq_cx, Vec3(ZERO), pl_view_shake_ids_x, "");
			pl_view_shake_ids_x += 1;
			if (pl_view_shake_ids_x > 500)
				pl_view_shake_ids_x = 56;
		}
	}
}

void CActorActionsNew::NetRequestPlayItemAction(EntityId actorId, int32 tag)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	//---------MP stuff----------
	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
		{
			pActor->GetGameObject()->InvokeRMI(CActor::ClPlayMQAction(), CActor::SNetPlayMQActionOnCurrentItem(tag, false), eRMI_ToRemoteClients);
		}
		else
		{
			pActor->GetGameObject()->InvokeRMI(CActor::SVPlayMQAction(), CActor::SNetPlayMQActionOnCurrentItem(tag, false), eRMI_ToServer);
		}
	}
	//---------MP stuff----------
}

void CActorActionsNew::NetRequestPlayItemAction2(EntityId actorId, string tag)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	//---------MP stuff----------
	if (gEnv->bMultiplayer)
	{
		if (gEnv->bServer)
		{
			pActor->GetGameObject()->InvokeRMI(CActor::ClPlayMQAction2(), CActor::SNetPlayMQActionOnCurrentItemWName(tag, false), eRMI_ToRemoteClients);
		}
		else
		{
			pActor->GetGameObject()->InvokeRMI(CActor::SVPlayMQAction2(), CActor::SNetPlayMQActionOnCurrentItemWName(tag, false), eRMI_ToServer);
		}
	}
	//---------MP stuff----------
}

void CActorActionsNew::CancelAnyAttackAction()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pPlayer->GetCurrentItemId()));
	if (!pItem)
		return;

	if (CWeapon * pWpn = pPlayer->GetWeapon(pItem->GetEntityId()))
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions) || pWpn->IsBusy())
		{
			pWpn->CancelAnyMelee();
		}
	}
}

bool CActorActionsNew::SelectAimTarget(float range)
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (pHud)
	{
		if(pHud->AnyInteractiveUIOpened())
			return false;
	}

	if (g_pGameCVars->g_playerRotViewMode > 0)
		return false;

	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return false;

	Vec3 boxDim(range, range, range);
	Vec3 m_effectCenter = pPlayer->GetEntity()->GetWorldPos();
	Vec3 ptmin = m_effectCenter - boxDim;
	Vec3 ptmax = m_effectCenter + boxDim;
	IPhysicalEntity** pEntityList = NULL;
	static const int iObjTypes = ent_living;
	int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pEntityList, iObjTypes);
	float old_obj_distance = 10000.0f;
	EntityId temp_aim_target_id = 0;
	EntityId old_aim_target_id = current_aim_target_id;
	uint8	m_localPlayerFaction;
	uint8	m_mpActorFaction;
	for (int i = 0; i < numEntities; ++i)
	{
		IPhysicalEntity* pPhysicalEntity = pEntityList[i];
		IEntity* pEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));
		if (pEntity)
		{
			if (pEntity->GetId() == pPlayer->GetEntityId())
				continue;

			if(pEntity->GetId() == current_aim_target_id)
				continue;

			CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
			if(!pActor)
				continue;

			if(pActor->IsDead())
				continue;

			IAIObject* pAIObjectMpActor = pEntity->GetAI();
			m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
			IAIObject* pAIObjectLocPl = pPlayer->GetEntity()->GetAI();
			m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
			if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
			{
				float this_dist = pPlayer->GetEntity()->GetWorldPos().GetDistance(pEntity->GetWorldPos());
				if (this_dist < old_obj_distance)
				{
					old_obj_distance = this_dist;
					temp_aim_target_id = pEntity->GetId();
				}
			}
		}
	}

	if(temp_aim_target_id == 0)
		return false;

	current_aim_target_id = temp_aim_target_id;
	if (pHud)
	{
		if(old_aim_target_id > 0)
			pHud->DelTargetFromScreenObjInfo(old_aim_target_id);

		pHud->AddTargetToScreenObjInfo(current_aim_target_id);
	}

	if(current_aim_target_id > 0)
		return true;
	else
		return false;
}

bool CActorActionsNew::DeselectAimTarget()
{
	if (current_aim_target_id > 0)
	{
		CHUDCommon* pHud = g_pGame->GetHUDCommon();
		if (pHud)
		{
			pHud->DelTargetFromScreenObjInfo(current_aim_target_id);
		}
		current_aim_target_id = 0;
		current_aim_target_pos = Vec3(ZERO);
		return true;
	}
	else
	{
		return false;
	}
}

void CActorActionsNew::UpdateAimTarget()
{
	CHUDCommon* pHud = g_pGame->GetHUDCommon();
	if (pHud)
	{
		if (pHud->AnyInteractiveUIOpened())
		{
			if (current_aim_target_id > 0)
			{
				DeselectAimTarget();
				return;
			}
		}
	}

	if (stop_aim_target_update)
		return;

	if (current_aim_target_id <= 0)
		return;

	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	if (pPlayer->IsDead() && current_aim_target_id > 0)
	{
		DeselectAimTarget();
		return;
	}

	IEntity* pEntityTarget = static_cast<IEntity*>(gEnv->pEntitySystem->GetEntity(current_aim_target_id));
	if (!pEntityTarget && current_aim_target_id > 0)
	{
		DeselectAimTarget();
		return;
	}

	if(!pEntityTarget)
		return;

	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(current_aim_target_id);
	if (pActor)
	{
		if (pActor->IsDead())
		{
			if (!SelectAimTarget(45.0f))
				DeselectAimTarget();

			return;
		}
	}

	AABB bounds;
	AABB Playerbounds;
	IEntityPhysicalProxy* pPlayerPhysicalProxy = static_cast<IEntityPhysicalProxy*>(pPlayer->GetEntity()->GetProxy(ENTITY_PROXY_PHYSICS));
	if(!pPlayerPhysicalProxy)
		return;

	pPlayerPhysicalProxy->GetWorldBounds(Playerbounds);
	IEntityPhysicalProxy* pTargetPhysicalProxy = static_cast<IEntityPhysicalProxy*>(pEntityTarget->GetProxy(ENTITY_PROXY_PHYSICS));
	if (!pTargetPhysicalProxy)
		return;

	pTargetPhysicalProxy->GetWorldBounds(bounds);
	Vec3 pPlPoint = Playerbounds.GetCenter();
	Vec3 pTargPoint = bounds.GetCenter();
	current_aim_target_pos = pTargPoint;
	if (pActor)
	{
		pTargPoint = pActor->GetBonePosition("Bip01 Spine");
		pPlPoint = pPlayer->GetBonePosition("Bip01 Spine");
		if (pPlayer->IsThirdPerson())
		{
			CCamera& cam = GetISystem()->GetViewCamera();
			float old_z = pPlPoint.z;
			pPlPoint = cam.GetPosition();
			pPlPoint.z = old_z;
		}
	}
	float dist = pPlPoint.GetDistance(pTargPoint);
	if (dist > 50.0f)
	{
		DeselectAimTarget();
		return;
	}
	Vec3 DirectionToTarget = pTargPoint - pPlPoint;
	Vec3 DirectionToFwd = pPlayer->GetViewQuatFinal() * Vec3(0,1,0);
	DirectionToFwd = DirectionToFwd * dist;
	Vec3 PosXvr = pPlPoint + DirectionToFwd;
	float dist_fwd_to_target = PosXvr.GetDistance(pTargPoint);

	Limit(dist_fwd_to_target, 0.5f, 5.0f);
	Matrix33 rotation = Matrix33::CreateRotationVDir((DirectionToTarget).GetNormalizedSafe());
	Quat TargetViewDirection = Quat(rotation);//Quat::CreateRotationVDir(DirectionToTarget);
	const float TARGET_VIEW_SMOOTH_TIME_INV = (1.0f / 8.0f);
	const float blendFactor = min(dist_fwd_to_target * TARGET_VIEW_SMOOTH_TIME_INV, 1.0f);
	pPlayer->SetViewRotation(Quat::CreateSlerp(pPlayer->GetViewQuatFinal(), TargetViewDirection, blendFactor));
}

void CActorActionsNew::PauseAimTargetUpdating(bool pause)
{
	stop_aim_target_update = pause;
}
//-----------------------------------------------------------------------------

bool CActorActionsNew::ChekForInventoryCapacity(EntityId actorId)
{
	CActor *pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId));
	if (!pActor)
		return false;

	IInventory *pInventory = pActor->GetInventory();
	if (!pInventory)
		return false;

	if (pActor->GetRealItemsCountInInventory() >= (pActor->GetMaxInvSlots() + pActor->GetNumNOItemsInInv() + 1))
	{
		return false;
	}
	return true;
}

int CActorActionsNew::CalculateItemGoldCost(EntityId sellerId, EntityId buyerId, EntityId itemId)
{
	CActor *pActortrader = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(sellerId));
	CActor *pActorbuyer = static_cast<CActor *>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(buyerId));
	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(itemId));
	if(!pActortrader || !pActorbuyer || !pItem)
		return 0;

	IEntity *pEntity = pActortrader->GetEntity();
	if (pActortrader->IsPlayer())
		pEntity = pActorbuyer->GetEntity();

	SmartScriptTable props;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	float tradingskill = 0.0f;
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		CScriptSetGetChain prop(props);
		prop.GetValue("tradingskill", tradingskill);
	}

	if (tradingskill < 0)
		tradingskill = 0;

	if (pItem->GetItemCost() == 0)
		return 0;

	if (pActortrader->IsPlayer())
	{
		//CryLogAlways(" pre itm_cost1 = %f", float(pItem->GetItemCost()) / float((tradingskill / 2)));
		//CryLogAlways(" pre itm_cost2 = %f", float(pActorbuyer->GetPersonality() / 2));
		float itm_cost = (float(pItem->GetItemCost()) / float((tradingskill / 2)))*float(pActorbuyer->GetPersonality() / 2);
		//CryLogAlways("itm_cost = %f", itm_cost);
		return int(itm_cost);
	}
	else
	{
		return (pItem->GetItemCost()*(tradingskill / 2)) / (pActorbuyer->GetPersonality() / 2);
	}
}

void CActorActionsNew::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}

bool CActorActionsNew::PlayerCanChangeWeapon()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return false;

	//if (IsPctrStateIsBusy())
	//	return false;

	if (pPlayer->IsDead())
		return false;

	if (pPlayer->GetSAPlayTime() > 0.0f)
		return false;

	if (pPlayer->GetSACurrent() == eSpec_act_combat_recoil)
		return false;

	if (pPlayer->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] > 0)
		return false;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pPlayer->GetCurrentItemId()));
	if (pItem)
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return false;
		}

		if (CWeapon * pWpn = pPlayer->GetWeapon(pItem->GetEntityId()))
		{
			if (pWpn->IsBusy())
			{
				return false;
			}
		}
	}
	return true;
}

bool CActorActionsNew::ActorCanChangeWeapon(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return false;

	if (pActor->IsDead())
		return false;

	if (pActor->GetSAPlayTime() > 0.0f)
		return false;

	if (pActor->GetSACurrent() == eSpec_act_combat_recoil)
		return false;

	if (pActor->spec_action_counter_in_combo[eSpec_act_combat_push_prerequest] > 0)
		return false;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (pItem)
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return false;
		}

		if (CWeapon * pWpn = pActor->GetWeapon(pItem->GetEntityId()))
		{
			if (pWpn->IsBusy())
			{
				return false;
			}
		}
	}
	return true;
}

void CActorActionsNew::OnHDReaction(EntityId actorId)
{
	ActorCancelAnyAttackAction(actorId);
}

void CActorActionsNew::ActorCancelAnyAttackAction(EntityId actorId)
{
	CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(actorId);
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
	if (!pItem)
		return;

	if (CWeapon * pWpn = pActor->GetWeapon(pItem->GetEntityId()))
	{
		if (pItem->AreAnyItemFlagsSet(CItem::eIF_BlockActions) || pWpn->IsBusy())
		{
			pWpn->CancelAnyMelee();
		}
	}
}

