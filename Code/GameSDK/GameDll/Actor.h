// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
 -------------------------------------------------------------------------
  $Id$
  $DateTime$
  Description: Simple Actor implementation
  
 -------------------------------------------------------------------------
  History:
  - 7:10:2004   14:46 : Created by Márcio Martins

*************************************************************************/
#ifndef __Actor_H__
#define __Actor_H__

#if _MSC_VER > 1000
# pragma once
#endif


//#include "Game.h" // for stance enum
#include <CryAISystem/IAgent.h> // for stance enum
#include <CryAISystem/IAIActorProxy.h> // for stance enum
#include "AutoEnum.h"

#include "ActorDamageEffectController.h"
#include <CryAISystem/IAIObject.h>
#include "ActorTelemetry.h"
#include "ActorDefinitions.h"
#include "ActorLuaCache.h"

#include "BodyDefinitions.h"

#include "Health.h"
#include "ICryMannequinDefs.h"

#include <IWeapon.h>

//need this
#include "Animation/PoseModifier/IKTorsoAim.h"
#include "GameXMLSettingAndGlobals.h"
#include "ActorSpellsActions.h"

#define ITEM_SWITCH_TIMER_ID	525
#define REFILL_AMMO_TIMER_ID	526
#define ITEM_SWITCH_THIS_FRAME	527
#define RECYCLE_AI_ACTOR_TIMER_ID 528
#define ON_RAGDOLLIZED_GRAVITY_CHECK_TIMER_ID 529

#define MAX_DAMAGE_RESIST_TYPES 150
#define MAX_BUFFS_EFF_TYPES 65535
#define MAX_PERKS_TYPES 65535
#define MAX_SKILLS_TYPES 65535
#define MAX_ABILITY_TYPES 65535
//--------------------------------------------
#define MAX_USABLE_SPELLS_SLOTS 10
#define MAX_QUICK_EQUIPMENT_SLOTS 17
#define ACTOR_TOTAL_RMI_COUNT 128//for now 55 RMI'S used

enum EActorEquipmentSlots
{
	eAESlot_None = 0,
	eAESlot_Weapon,
	eAESlot_Boots,
	eAESlot_Arms,
	eAESlot_Helmet,
	eAESlot_Torso,
	eAESlot_Pants,
	eAESlot_Shield,
	eAESlot_Torch,
	eAESlot_Ammo_Arrows,
	eAESlot_Weapon_Left,
	eAESlot_Ammo_Bolts,
	eAESlot_Ring_Left_Hand_1,
	eAESlot_Ring_Left_Hand_2,
	eAESlot_Ring_Left_Hand_3,
	eAESlot_Ring_Left_Hand_4,
	eAESlot_Ring_Left_Hand_5,
	eAESlot_Ring_Left_Hand_6,
	eAESlot_Ring_Left_Hand_7,
	eAESlot_Ring_Left_Hand_8,
	eAESlot_Ring_Left_Hand_9,
	eAESlot_Ring_Rigth_Hand_1,
	eAESlot_Ring_Rigth_Hand_2,
	eAESlot_Ring_Rigth_Hand_3,
	eAESlot_Ring_Rigth_Hand_4,
	eAESlot_Ring_Rigth_Hand_5,
	eAESlot_Ring_Rigth_Hand_6,
	eAESlot_Ring_Rigth_Hand_7,
	eAESlot_Ring_Rigth_Hand_8,
	eAESlot_Ring_Rigth_Hand_9,
	eAESlot_Neckale_1,
	eAESlot_Neckale_2,
	eAESlot_Neckale_3,
	eAESlot_Neckale_4,
	eAESlot_Neckale_5,
	eAESlot_Ears_left_1,
	eAESlot_Ears_left_2,
	eAESlot_Ears_left_3,
	eAESlot_Ears_left_4,
	eAESlot_Ears_left_5,
	eAESlot_Ears_Rigth_1,
	eAESlot_Ears_Rigth_2,
	eAESlot_Ears_Rigth_3,
	eAESlot_Ears_Rigth_4,
	eAESlot_Ears_Rigth_5,
	eAESlot_Head_Decor_1,
	eAESlot_Head_Decor_2,
	eAESlot_Head_Decor_3,
	eAESlot_Head_Decor_4,
	eAESlot_Head_Decor_5,
	eAESlot_Torso_Decor_1,
	eAESlot_Torso_Decor_2,
	eAESlot_Torso_Decor_3,
	eAESlot_Torso_Decor_4,
	eAESlot_Torso_Decor_5,
	eAESlot_Arms_Decor_1,
	eAESlot_Arms_Decor_2,
	eAESlot_Arms_Decor_3,
	eAESlot_Arms_Decor_4,
	eAESlot_Arms_Decor_5,
	eAESlot_Arms_Decor_6,
	eAESlot_Legs_Decor_1,
	eAESlot_Legs_Decor_2,
	eAESlot_Legs_Decor_3,
	eAESlot_Legs_Decor_4,
	eAESlot_Legs_Decor_5,
	eAESlot_Legs_Decor_6,
	eAESlot_Foots_Decor_1,
	eAESlot_Foots_Decor_2,
	eAESlot_Foots_Decor_3,
	eAESlot_Foots_Decor_4,
	eAESlot_Foots_Decor_5,
	eAESlot_Foots_Decor_6,
	eAESlots_Number
};

enum EActorBodyPartsIds
{
	eABPart_None = 0,
	eABPart_Head,
	eABPart_UpperBody,
	eABPart_LowerBody,
	eABPart_Hands,
	eABPart_Foots,
	eABPart_Hair,
	eABPart_PPP,
	eABPart_UnderWear1,
	eABPart_UnderWear2,
	eABPart_EyeL,
	eABPart_EyeR,
	eABPart_All
};

enum EQuickSlotTypes
{
	eQSType_None = 0,
	eQSType_Item,
	eQSType_Spell,
	eQSType_Ability
};

struct SHitImpulse;
struct SAutoaimTargetRegisterParams;
class CItem;
class CWeapon;
class CActor;
DECLARE_SHARED_POINTERS(CActor);
class CActorImpulseHandler;
DECLARE_SHARED_POINTERS(CActorImpulseHandler);
class CProceduralContextRagdoll;

namespace PlayerActor
{
	namespace Stumble
	{
		struct StumbleParameters;
	}
};

class CActor :
	public CGameObjectExtensionHelper<CActor, IActor, ACTOR_TOTAL_RMI_COUNT>,
	public IGameObjectView,
	public IGameObjectProfileManager
{
	friend class CStatsRecordingMgr;

protected:

	enum EActorClass
	{
		eActorClass_Actor = 0,
		eActorClass_Player
	};


public:

	struct ItemIdParam
	{
		ItemIdParam(): itemId(0), pickOnlyAmmo(false), select(true) {};
		ItemIdParam(EntityId item): itemId(item), pickOnlyAmmo(false), select(true) {};
		ItemIdParam(EntityId item, bool onlyAmmo): itemId(item), pickOnlyAmmo(onlyAmmo), select(true) {};
		ItemIdParam(EntityId item, bool onlyAmmo, bool inSelect): itemId(item), pickOnlyAmmo(onlyAmmo), select(inSelect) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("itemId", itemId, 'eid');
			ser.Value("pickOnlyAmmo", pickOnlyAmmo, 'bool');
			ser.Value("select", select, 'bool');
		}
		EntityId itemId;
		bool pickOnlyAmmo;
		bool select;
	};

	struct EquipItemParams
	{
		EquipItemParams() : item_to_equip(0), need_equip(false), slot(0) {};
		EquipItemParams(EntityId itemtoeq, bool equ, uint8 slt) : item_to_equip(itemtoeq), need_equip(equ), slot(slt){};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("itemtoEquip", item_to_equip, 'eid');
			ser.Value("needEquip", need_equip, 'bool');
			ser.Value("slotEq", slot, 'ui8');
		}
		EntityId item_to_equip;
		bool need_equip;
		uint8 slot;
	};

	struct AcSpecialAction
	{
		AcSpecialAction() : action_id(0), action_time(0.0f) {};
		AcSpecialAction(int act_id, float act_tm) : action_id(act_id), action_time(act_tm) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("actionId", action_id, 'i16');
			ser.Value("actionTime", action_time, 'vHRd');
		}
		int action_id;
		float action_time;
	};

	struct AcBlcAction
	{
		AcBlcAction() : block_enable(false), block_type(0) {};
		AcBlcAction(bool block, int type) : block_enable(block), block_type(type) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("blockType", block_type, 'i16');
			ser.Value("blockEnable", block_enable, 'bool');
		}
		int block_type;
		bool block_enable;
	};

	struct AcModelParams
	{
		AcModelParams() : model_path(""), from_lua(false){};
		AcModelParams(string model, bool fromlua) : model_path(model), from_lua(fromlua){};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("model_path", model_path);
			ser.Value("from_lua", from_lua, 'bool');
		}
		string model_path;
		bool from_lua;
	};

	struct AcCustomisingInfo
	{
		AcCustomisingInfo() : gender_type(0), head_type(1), hair_type(1), eyes_type(1), character_name("") {};
		AcCustomisingInfo(int gender, int head, int hair, int eyes, string name) :gender_type(gender), head_type(head), hair_type(hair), eyes_type(eyes), character_name(name) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("gender_type", gender_type, 'i8');
			ser.Value("head_type", head_type, 'i16');
			ser.Value("hair_type", hair_type, 'i16');
			ser.Value("eyes_type", eyes_type, 'i16');
			ser.Value("character_name", character_name);
		}
		int gender_type;
		int head_type;
		int hair_type;
		int eyes_type;
		string character_name;
	};

	struct AcEffectParams
	{
		AcEffectParams() : buff_id(0), buff_time(0.0f), eff_id(0) {};
		AcEffectParams(int buffid, float btime, int efid) : buff_id(buffid), buff_time(btime), eff_id(efid) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("buff_id", buff_id, 'i32');
			ser.Value("buff_time", buff_time, 'vHRd');
			ser.Value("eff_id", eff_id, 'i32');
		}
		int buff_id;
		float buff_time;
		int eff_id;
	};

	struct SNetCastSlotPrm
	{
		SNetCastSlotPrm() : cast_slot(0), forced_slot(0) {};
		SNetCastSlotPrm(int slt, int fslt) : cast_slot(slt), forced_slot(fslt) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("cast_slot", cast_slot, 'i32');
			ser.Value("forced_slot", forced_slot, 'i32');
		}
		int cast_slot;
		int forced_slot;
	};

	struct SNetEndCastSlotPrm
	{
		SNetEndCastSlotPrm() : cast_slot(0), forced(0) {};
		SNetEndCastSlotPrm(int slt, bool fslt) : cast_slot(slt), forced(fslt) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("cast_slot", cast_slot, 'i32');
			ser.Value("forced", forced, 'bool');
		}
		int cast_slot;
		bool forced;
	};

	struct SNetPlayMQActionOnCurrentItem
	{
		SNetPlayMQActionOnCurrentItem() : tag_id(0), is_left_item(0) {};
		SNetPlayMQActionOnCurrentItem(int32 slt, bool fslt) : tag_id(slt), is_left_item(fslt) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("tag_id", tag_id, 'i32');
			ser.Value("is_left_item", is_left_item, 'bool');
		}
		int32 tag_id;
		bool is_left_item;
	};

	struct SNetPlayMQActionOnCurrentItemWName
	{
		SNetPlayMQActionOnCurrentItemWName() : tag_name(""), is_left_item(0) {};
		SNetPlayMQActionOnCurrentItemWName(string slt, bool fslt) : tag_name(slt), is_left_item(fslt) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("tag_name", tag_name);
			ser.Value("is_left_item", is_left_item, 'bool');
		}
		string tag_name;
		bool is_left_item;
	};

	struct SNvRPGInfoPack
	{
		SNvRPGInfoPack()
		{
			chrmodelstr = "";
			gender = 0;
			player_name = "";
			plheadtype = 0;
			plhairtype = 0;
			pleyestype = 0;
			stealth_mode = false;
			relaxed_mode = false;
		};
		SNvRPGInfoPack(string model, string name, bool smod, bool rmod, int gnd, int head, int hair, int eyes ) :
			chrmodelstr(model), player_name(name), stealth_mode(smod),
			relaxed_mode(rmod), gender(gnd), plheadtype(head),
			plhairtype(hair), pleyestype(eyes) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("gender", gender, 'i8');
			ser.Value("chrmodelstr", chrmodelstr);
			ser.Value("player_name", player_name);
			ser.Value("plheadtype", plheadtype, 'i16');
			ser.Value("plhairtype", plhairtype, 'i16');
			ser.Value("pleyestype", pleyestype, 'i16');
			ser.Value("stealth_mode", stealth_mode, 'bool');
			ser.Value("relaxed_mode", relaxed_mode, 'bool');
		}
		string chrmodelstr;
		string player_name;
		bool stealth_mode;
		bool relaxed_mode;
		int gender;
		int plheadtype;
		int plhairtype;
		int pleyestype;
	};

	struct SNetMgcSlots
	{
		SNetMgcSlots() : mgc_slot1(0), mgc_slot2(0) {};
		SNetMgcSlots(int slot_1, int slot_2) : mgc_slot1(slot_1), mgc_slot2(slot_2) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("mgc_slot1", mgc_slot1, 'i32');
			ser.Value("mgc_slot2", mgc_slot2, 'i32');
		}
		int mgc_slot1;
		int mgc_slot2;
	};

	struct AcQuickSlot
	{
		AcQuickSlot() : slot_id(0), slot_type(0), obj_in_slot_id(0) {};
		int slot_id;
		int slot_type;
		int obj_in_slot_id;
	};

	struct ExchangeItemParams
	{
		ExchangeItemParams() : dropItemId(0), pickUpItemId(0) {};
		ExchangeItemParams(EntityId drop, EntityId pickup) : dropItemId(drop), pickUpItemId(pickup) {};

		void SerializeWith(TSerialize ser)
		{
			ser.Value("dropId", dropItemId, 'eid');
			ser.Value("pickId", pickUpItemId, 'eid');
		}

		EntityId dropItemId;
		EntityId pickUpItemId;
	};

	struct DropItemParams
	{
		DropItemParams(): itemId(0), selectNext(true), byDeath(false) {};
		DropItemParams(EntityId item, bool next=true, bool death=false): itemId(item), selectNext(next), byDeath(death) {};
		
		void SerializeWith(TSerialize ser)
		{
			ser.Value("itemId", itemId, 'eid');
			ser.Value("selectNext", selectNext, 'bool');
			ser.Value("byDeath", byDeath, 'bool');
		}

		EntityId itemId;
		bool selectNext;
		bool byDeath;
	};

	struct AddFromCharXMLItemParams
	{
		AddFromCharXMLItemParams() : item_class(""), item_quantity(0), selected(false) {};
		AddFromCharXMLItemParams(string itemClass, int itemQuantity = 0, bool Selected = false) : item_class(itemClass), item_quantity(itemQuantity), selected(Selected) {};

		void SerializeWith(TSerialize ser)
		{
			ser.Value("itemClass", item_class);
			ser.Value("itemQuantity", item_quantity, 'i32');
			ser.Value("selected", selected, 'bool');
		}

		string item_class;
		int item_quantity;
		bool selected;
	};

	struct ReviveParams
	{
		ReviveParams(): teamId(0), spawnPointIdx(0), physCounter(0), modelIndex(MP_MODEL_INDEX_DEFAULT) {};
		ReviveParams(uint8 tId, const uint16 idx, uint8 counter, uint8 _modelIndex): spawnPointIdx(idx), teamId(tId), physCounter(counter), modelIndex(_modelIndex) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("teamId", teamId, 'team');
			ser.Value("spawnPointId", spawnPointIdx, 'ui9');
			ser.Value("physCounter", physCounter, 'ui2');
			ser.Value("modelIndex", modelIndex, MP_MODEL_INDEX_NET_POLICY);
		};

		int	teamId;
		uint16 spawnPointIdx;
		uint8 physCounter;
		uint8 modelIndex;
	};

	struct KillParams
	{
		KillParams()
		: shooterId(0),
			targetId(0),
			weaponId(0),
			projectileId(0),
			itemIdToDrop(0),
			weaponClassId(0),
			damage(0.0f),
			material(0),
			hit_type(0),
			hit_joint(0),
			projectileClassId(~uint16(0)),
			penetration(0),
			impulseScale(0),
			dir(ZERO),
#if USE_LAGOMETER
			lagOMeterHitId(0),
#endif
			ragdoll(false),
			winningKill(false),
			firstKill(false),
			bulletTimeReplay(false),
			fromSerialize(false),
			killViaProxy(false),
			forceLocalKill(false),
			targetTeam(0)
		{}

		explicit KillParams( const HitInfo& hitInfo )
		:	shooterId					(hitInfo.shooterId),
			targetId					(hitInfo.targetId),
			weaponId					(hitInfo.weaponId),
			projectileId			(hitInfo.projectileId),
			itemIdToDrop			(-1),
			weaponClassId			(hitInfo.weaponClassId),
			damage						(hitInfo.damage),
			impulseScale			(hitInfo.impulseScale),
			dir								(hitInfo.dir),
			material					(-1),
			hit_type					(hitInfo.type),
			targetTeam				(0),
			hit_joint					(hitInfo.partId),
			projectileClassId	(hitInfo.projectileClassId),
			penetration				(hitInfo.penetrationCount),
#if USE_LAGOMETER
			lagOMeterHitId		(0),
#endif
			ragdoll						(false),
			winningKill				(false),
			firstKill					(false),
			bulletTimeReplay	(false),
			killViaProxy			(hitInfo.hitViaProxy),
			forceLocalKill		(hitInfo.forceLocalKill),
			fromSerialize			(false)
		{}


		EntityId shooterId;
		EntityId targetId;
		EntityId weaponId;
		EntityId projectileId;
		EntityId itemIdToDrop;
		int weaponClassId;
		float damage;
		float impulseScale;
		Vec3 dir;
		int material;
		int hit_type;
		int targetTeam;
		uint16 hit_joint;
		uint16 projectileClassId;
		uint8 penetration;
#if USE_LAGOMETER
		uint8 lagOMeterHitId;
#endif
		bool ragdoll;
		bool winningKill;
		bool firstKill;
		bool bulletTimeReplay;
		bool killViaProxy;
		bool forceLocalKill; // Not serialised. skips prohibit death reaction checks. 

		// Special case - used when actor is killed from FullSerialize to supress certain logic from running
		bool fromSerialize;

		void SerializeWith(TSerialize ser)
		{
			ser.Value("shooterId", shooterId, 'eid');
			ser.Value("targetId", targetId, 'eid');
			ser.Value("weaponId", weaponId, 'eid');
			ser.Value("projectileId", projectileId, 'eid');
			ser.Value("itemIdToDrop", itemIdToDrop , 'eid');
			ser.Value("weaponClassId", weaponClassId, 'clas');
			ser.Value("damage", damage, 'dmg');
			ser.Value("material", material, 'mat');
			ser.Value("hit_type", hit_type, 'hTyp');
			ser.Value("hit_joint", hit_joint, 'ui16');
			ser.Value("projectileClassId", projectileClassId, 'ui16');
			ser.Value("penetration", penetration, 'ui8');
			ser.Value("dir", dir, 'dir1');
			ser.Value("impulseScale", impulseScale, 'impS');
			ser.Value("ragdoll", ragdoll, 'bool');
#if USE_LAGOMETER
			ser.Value("lagOMeterHitId", lagOMeterHitId, 'ui4');
#endif
			ser.Value("winningKill", winningKill, 'bool');
			ser.Value("firstKill", firstKill, 'bool');
			ser.Value("bulletTimeReplay", bulletTimeReplay, 'bool');
			ser.Value("proxyKill", killViaProxy, 'bool');
			ser.Value("targetTeam", targetTeam, 'team');
		};
	};
	struct MoveParams
	{
		MoveParams() {};
		MoveParams(const Vec3 &p, const Quat &q): pos(p), rot(q) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("pos", pos, 'wrld');
			ser.Value("rot", rot, 'ori1');
		}
		Vec3 pos;
		Quat rot;
	};

	struct PickItemParams
	{
		PickItemParams(): itemId(0), select(false), sound(false), pickOnlyAmmo(false) {};
		PickItemParams(EntityId item, bool slct, bool snd): itemId(item), select(slct), sound(snd), pickOnlyAmmo(false) {};
		PickItemParams(EntityId item, bool slct, bool snd, bool onlyAmmo): itemId(item), select(slct), sound(snd), pickOnlyAmmo(onlyAmmo) {};
		void SerializeWith(TSerialize ser)
		{
			ser.Value("itemId", itemId, 'eid');
			ser.Value("select", select, 'bool');
			ser.Value("sound", sound, 'bool');
			ser.Value("pickOnlyAmmo", pickOnlyAmmo, 'bool');
		}

		EntityId	itemId;
		bool			select;
		bool			sound;
		bool      pickOnlyAmmo;
	};

	struct NoParams
	{
		void SerializeWith(const TSerialize& ser) {};
	};


	struct KillCamFPData
	{
		static const int DATASIZE = 50;
		static const int UNIQPACKETIDS = 16;
		uint8 m_data[DATASIZE];
		uint16 m_size;
		EntityId m_victim;
		uint8 m_numPacket;
		uint8 m_packetId;
		uint8 m_packetType;
		bool m_bFinalPacket;
		bool m_bToEveryone;

		KillCamFPData() { }
		KillCamFPData(uint8 packetType, uint8 packetId, uint8 numPacket, EntityId victim, uint32 size, void *buffer, bool bLastPacket, bool bToEveryone)
		{
			m_packetType=packetType;
			m_packetId=packetId;
			m_numPacket=numPacket;
			m_victim=victim;
			m_bFinalPacket=bLastPacket;
			m_bToEveryone=bToEveryone;
			if (size>sizeof(m_data))
			{
				CryFatalError("Trying to send packet of size %d bytes when maximum allowed is %" PRISIZE_T " bytes\n", size, sizeof(m_data));
				size=sizeof(m_data);
			}
			m_size=size;
			memcpy(m_data, buffer, size);
		}

		void SerializeWith(TSerialize ser)
		{
			ser.Value("packetType", m_packetType, 'ui2'); // 0-3
			ser.Value("packetId", m_packetId, 'ui4');     // 0-15
			ser.Value("numPacket", m_numPacket, 'ui8');   // 0-255 (PacketSize=50bytes so 0-12.4Kb)
			ser.Value("victim", m_victim, 'eid');					// u16?
			ser.Value("dataSize", m_size, 'ui10');					// 0 - 64
			ser.Value("finalPacket", m_bFinalPacket, 'bool');
			ser.Value("toEveryone", m_bToEveryone, 'bool');
			for (int i = 0; i < m_size; ++i)
			{
				char temp[255];
				cry_sprintf(temp, "data%d", i);
				ser.Value(temp, m_data[i], 'ui8');
			}
		};
	};

	struct AttachmentsParams
	{
		struct SWeaponAttachment
		{
			uint16 m_classId;
			bool m_default;

			SWeaponAttachment()
			{
				m_classId = ~uint16(0);
				m_default = false;
			}

			SWeaponAttachment(uint16 classId, bool isDefault)
			{
				m_classId = classId;
				m_default = isDefault;
			}

			void SerializeWith(TSerialize ser)
			{
				ser.Value("classId", m_classId, 'clas');
				ser.Value("default", m_default, 'bool');
			}
		};

		AttachmentsParams()
		{
			m_loadoutIdx = 0;
		}

		#define MAX_WEAPON_ATTACHMENTS 31
		CryFixedArray<SWeaponAttachment, MAX_WEAPON_ATTACHMENTS> m_attachments;
		uint8																											m_loadoutIdx;

		void SerializeWith(TSerialize ser)
		{
			int numAttachments = m_attachments.size();
			bool isReading = ser.IsReading();

			ser.Value("loadoutIdx", m_loadoutIdx, 'ui4' );
			ser.Value("numAttachments", numAttachments, 'ui5'); // 0-31, needs to be kept in sync with MAX_WEAPON_ATTACHMENTS
			for(int i=0; i < numAttachments; i++)
			{
				if(!isReading)
				{
					m_attachments[i].SerializeWith(ser);
				}
				else
				{
					SWeaponAttachment data;
					data.SerializeWith(ser);
					m_attachments.push_back(data);
				}
			}
		}
	};

	struct SBlendRagdollParams
	{
		SBlendRagdollParams()
			: m_blendInTagState(TAG_STATE_EMPTY)
			, m_blendOutTagState(TAG_STATE_EMPTY)
		{
		}

		TagState m_blendInTagState;
		TagState m_blendOutTagState;
	};

	struct SDinamicPhysicalTemportaryParams
	{
		SDinamicPhysicalTemportaryParams()
		{
			intertia = -100.0f;
			intertiaAcc = -100.0f;
			slideAngle = -100.0f;
			climbAngle = -100.0f;
			fallAngle = -100.0f;
			jumpAngle = -100.0f;
			velGround = -100.0f;
		}
		float intertia;
		float intertiaAcc;
		float slideAngle;
		float climbAngle;
		float fallAngle;
		float jumpAngle;
		float velGround;
	};

	SDinamicPhysicalTemportaryParams temp_phy_dinamic_values;

	AUTOENUM_BUILDENUMWITHTYPE(EReasonForRevive, ReasonForReviveList);

	enum EActorSpectatorState
	{
		eASS_None = 0,						// Just joined - fixed spectating, no actor.
		eASS_Ingame,							// Ingame, dead ready to respawn, with actor.
		eASS_ForcedEquipmentChange,	// Has actor, showing the equipment screen to choose new loadout, i.e. between rounds or switching race
		eASS_SpectatorMode,				// Is currently viewing the game as a spectator - has an actor, but will not respawn unless they leave this mode
	};

	enum EActorSpectatorMode
	{
		eASM_None = 0,												// normal, non-spectating

		eASM_FirstMPMode,
		eASM_Fixed = eASM_FirstMPMode,				// fixed position camera
		eASM_Free,														// free roaming, no collisions
		eASM_Follow,													// follows an entity in 3rd person
		eASM_Killer,													// Front view of the killer in 3rdperson.
		eASM_LastMPMode = eASM_Killer,

		eASM_Cutscene,												// HUDInterfaceEffects.cpp sets this
	};

	enum EActorDamageResistsTypes
	{
		eADRT_None = 0,
		eADRT_Melee,
		eADRT_Collision,
		eADRT_Normal = 11,
		eADRT_Fire = 12,
		eADRT_Electricity = 21,
		eADRT_Frost = 26,
		eADRT_DarkMagick,
		eADRT_LightMagick,
		eADRT_Arrow,
		eADRT_Bolt,
		eADRT_ThrowAbleWeapon,
		eADRT_Poison,
		eADRT_UnNormal,
		eADRT_Bleeding,
		eADRT_Healing
	};

	

	DECLARE_SERVER_RMI_NOATTACH(SvRequestDropItem, DropItemParams, eNRT_ReliableOrdered);
	DECLARE_SERVER_RMI_NOATTACH(SvRequestPickUpItem, ItemIdParam, eNRT_ReliableOrdered);
	DECLARE_SERVER_RMI_NOATTACH(SvRequestExchangeItem, ExchangeItemParams, eNRT_ReliableOrdered);
	DECLARE_SERVER_RMI_URGENT(SvRequestUseItem, ItemIdParam, eNRT_ReliableOrdered);
	// cannot be _FAST - see comment on InvokeRMIWithDependentObject
	DECLARE_CLIENT_RMI_NOATTACH(ClPickUp, PickItemParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClClearInventory, NoParams, eNRT_ReliableOrdered);

	DECLARE_CLIENT_RMI_NOATTACH(ClDrop, DropItemParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClStartUse, ItemIdParam, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClStopUse, ItemIdParam, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClUseRequestProcessed, NoParams, eNRT_ReliableOrdered);

	//virtual void SendRevive(const Vec3& position, const Quat& orientation, int team, bool clearInventory);
	DECLARE_CLIENT_RMI_NOATTACH(ClRevive, ReviveParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClSimpleKill, NoParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_URGENT(ClKill, KillParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClMoveTo, MoveParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvKillFPCamData, KillCamFPData, eNRT_ReliableUnordered);
	DECLARE_CLIENT_RMI_NOATTACH(ClKillFPCamData, KillCamFPData, eNRT_ReliableUnordered);

	DECLARE_SERVER_RMI_NOATTACH(SvEquipItem, EquipItemParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClEquipItem, EquipItemParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvAddBuff, AcEffectParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClAddBuff, AcEffectParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvDelBuff, AcEffectParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClDelBuff, AcEffectParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvUpdateActorBodyAndAttachments, NoParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClUpdateActorBodyAndAttachments, NoParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvAddAdditionalEquip, NoParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClAddAdditionalEquip, NoParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvAddAdditionalEquip2, NoParams, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClAddAdditionalEquip2, NoParams, eNRT_ReliableOrdered);

	DECLARE_CLIENT_RMI_NOATTACH(ClAssignWeaponAttachments, AttachmentsParams, eNRT_ReliableOrdered);

	DECLARE_SERVER_RMI_NOATTACH(SvAddItemWParams, AddFromCharXMLItemParams, eNRT_ReliableOrdered);
	//**********************SpellActions rMIs********************************************************
	DECLARE_SERVER_RMI_NOATTACH(SVCastSpell, SNetCastSlotPrm, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClCastSpell, SNetCastSlotPrm, eNRT_ReliableOrdered);
	DECLARE_SERVER_RMI_NOATTACH(SVEndCastSpell, SNetEndCastSlotPrm, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClEndCastSpell, SNetEndCastSlotPrm, eNRT_ReliableOrdered);
	DECLARE_SERVER_RMI_NOATTACH(SVUdateMgcSlotsInfo, SNetMgcSlots, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_NOATTACH(ClUdateMgcSlotsInfo, SNetMgcSlots, eNRT_ReliableOrdered);
	//***********************************************************************************************
	DECLARE_SERVER_RMI_URGENT(SVPlayMQAction, SNetPlayMQActionOnCurrentItem, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_URGENT(ClPlayMQAction, SNetPlayMQActionOnCurrentItem, eNRT_ReliableOrdered);
	//***********************************************************************************************
	//***********************************************************************************************
	DECLARE_SERVER_RMI_URGENT(SVPlayMQAction2, SNetPlayMQActionOnCurrentItemWName, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_URGENT(ClPlayMQAction2, SNetPlayMQActionOnCurrentItemWName, eNRT_ReliableOrdered);
	//***********************************************************************************************
	DECLARE_SERVER_RMI_URGENT(SvUpdateRPGInfo, SNvRPGInfoPack, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_URGENT(ClUpdateRPGInfo, SNvRPGInfoPack, eNRT_ReliableOrdered);
	//***********************************************************************************************
	DECLARE_SERVER_RMI_URGENT(SvSetBlockState, AcBlcAction, eNRT_ReliableOrdered);
	DECLARE_CLIENT_RMI_URGENT(ClSetBlockState, AcBlcAction, eNRT_ReliableOrdered);
	//***********************************************************************************************
	CItem *GetItem(EntityId itemId) const;
	CItem *GetItemByClass(IEntityClass* pClass) const;
	CWeapon *GetWeapon(EntityId itemId) const;
	CWeapon *GetWeaponByClass(IEntityClass* pClass) const;
	
	void HideLeftHandObject(bool inHide);

	EntityId ComputeNextItem(const int startSlot, const int category, const int delta, bool& inOutKeepHistory, IItem* pCurrentItem, const bool currWeaponExplosive) const;

	virtual void SelectNextItem(int direction, bool keepHistory, int category=0);
	virtual void SwitchToWeaponWithAccessoryFireMode();
	virtual void HolsterItem(bool holster, bool playSelect = true, float selectSpeedBias = 1.0f, bool hideLeftHandObject = true);
	virtual void SelectLastItem(bool keepHistory, bool forceNext = false);
	virtual void SelectItemByName(const char *name, bool keepHistory, bool forceFastSelect=false);
	virtual void SelectItem(EntityId itemId, bool keepHistory, bool forceSelect);

	//*************************************************************************************************************************
	virtual void DeSelectItem(EntityId itemId, bool keepHistory);
	//-------------------------------------------------------------------------------------------------------------------------
	virtual bool IsItemInActorInventory(EntityId itemId);
	//-------------------------------------------------------------------------------------------------------------------------
	virtual bool ScheduleItemSwitch(EntityId itemId, bool keepHistory, int category = 0, bool forceFastSelect=false);
	ILINE void CancelScheduledSwitch() 
	{ 
		GetEntity()->KillTimer(ITEM_SWITCH_TIMER_ID); 
		GetEntity()->KillTimer(ITEM_SWITCH_THIS_FRAME); 
		SActorStats::SItemExchangeStats& exchangeItemStats = GetActorStats()->exchangeItemStats;
		exchangeItemStats.switchingToItemID = 0; 
		exchangeItemStats.switchThisFrame = false; 
	}
	void ClearItemActionControllers();

	virtual bool UseItem(EntityId itemId);
	virtual bool PickUpItem(EntityId itemId, bool sound, bool select);
	virtual bool DropItem(EntityId itemId, float impulseScale=1.0f, bool selectNext=true, bool byDeath=false);
	virtual void DropAttachedItems();
	void ExchangeItem(CItem* pCurrentItem, CItem* pNewItem);
	void ServerExchangeItem(CItem* pCurrentItem, CItem* pNewItem);
	bool PickUpItemAmmo(EntityId itemId);


	//**********************************************
	const char* GetActorArmorPartModelPath(int part_id, int model_slot);
	const char* GetActorBodyPartModelPath(int part_id);
	void SetActorArmorPartModelPath(int part_id, int model_slot, const char* model_path);
	void SetActorBodyPartModelPath(int part_id, const char* model_path);
	EntityId GetCurrentEquippedItemId(int id);
	void SetEquippedItemId(int id, EntityId item_id);
	void SetEquippedState(int state_id, bool state);
	bool GetEquippedState(int state_id);
	void SetEquippedArmorNumParts(int part_id, int part_num);
	int GetEquippedArmorNumParts(int part_id);
	//attachments functions---------------------------------------------------------------------------------
	void CreateSkinAttachment(int characterSlot, const char *attachmentName, const char *attachmentPth, int spec_fl = 0);
	void CreateBoneAttachment(int characterSlot, const char *attachmentName, const char *attachmentPth, const char *BoneName, bool setDefPose, bool ent, EntityId id = 0, bool skel = false);
	void DeleteSkinAttachment(int characterSlot, const char *attachmentName, bool clear_only, const char *attachmentPth = "");
	void DeleteBoneAttachment(int characterSlot, const char *attachmentName, bool clear_only);
	void CreateLightAttachment(int characterSlot, const char *attachmentName, const char *BoneName, bool setDefPose, ColorF col, float rad, float intense, bool shadows);
	void CreateParticleAttachment(int characterSlot, const char *attachmentName, const char *BoneName, const char *ParticleEffectName, bool setDefPose);
	void DeleteLightAttachment(int characterSlot, const char *attachmentName, bool clear_only);
	void DeleteParticleAttachment(int characterSlot, const char *attachmentName, bool clear_only);
	bool IsAttachmentOnCharacter(int characterSlot, const char *attachmentName);
	ICharacterInstance *GetAttachmentObjectCharacterInstance(int characterSlot, const char *attachmentName);
	//**********************************************
	virtual bool EquipItem(EntityId itemId);
	virtual bool Net_EquipItem(EntityId itemId);
	virtual void AddAdditionalEquip();
	virtual void AddAdditionalEquipNotWear();
	virtual void AddAdditionalEquip2();
	virtual void AddAdditionalEquipNotWear2();
	virtual void AddAdditionalEquipPermoment(const char *eqpack, bool equip = false);
	void ActivateBuff(int id, float time, bool active, int eff_id = 0);
	void ActivatePerk(int id, bool active, int level);
	bool IsPerkActive(int id);
	int GetPerkLevel(int id);
	void ClearActorPerks(bool full);
	void UpdateBuffs(float frametime);
	virtual void AddBuff(int id, float time, int eff_id = 0);
	virtual void DelBuff(int id);
	virtual void Net_AddBuff(int id, float time, int eff_id = 0);
	virtual void Net_DelBuff(int id);
	bool GetBuffActive(int id);
	float GetBuffTime(int id);
	void AddBuffTime(int id, float time);
	void SendPhyAndAiUpdateEvent();
	std::list<EntityId> m_back_items;
	virtual void EquipWeaponToLeftHand(EntityId itemId, bool select = true);
	virtual void RequestToAddItemWAttCRC(const char *item_base_id, uint32 base_crc, int number, EntityId att_ent = 0);
	//void StartSpecialAttackAction(int id, int dmg);
	string GetCurrentModelPth();
	struct SComplexAttachments
	{
		SComplexAttachments() : ca_pth(""), ca_nmn(""){};
		string ca_pth;
		string ca_nmn;
	};
	std::vector<SComplexAttachments> m_complex_attachments_to_attach;


	void NetReviveAt(const Vec3 &pos, const Quat &rot, int teamId, uint8 modelIndex);
	virtual void NetReviveInVehicle(EntityId vehicleId, int seatId, int teamId);
	virtual void NetSimpleKill();
	virtual void NetKill(const KillParams &killParams);

	void ForceRagdollizeAndApplyImpulse(const HitInfo& hitInfo); 

	Vec3 GetWeaponOffsetWithLean(EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets = false) const;
	Vec3 GetWeaponOffsetWithLean(CWeapon* pCurrentWeapon, EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets = false) const;
	Vec3 GetWeaponOffsetWithLeanForAI(CWeapon* pCurrentWeapon, EStance stance, float lean, float peekOver, const Vec3& eyeOffset, const bool useWhileLeanedOffsets = false) const;

	virtual bool CanRagDollize() const;

	virtual bool IsStillWaitingOnServerUseResponse() const {return m_bAwaitingServerUseResponse;}; 
	virtual void SetStillWaitingOnServerUseResponse(bool waiting);
	void UpdateServerResponseTimeOut( const float frameTime );

	void OnHostMigrationCompleted(); 
		
public:
	CActor();
	virtual ~CActor();

	// IEntityEvent
	virtual	void ProcessEvent( SEntityEvent &event );
	virtual IComponent::ComponentEventPriority GetEventPriority( const int eventID ) const;
	// ~IEntityEvent

	// IActor
	virtual void Release();
	virtual void ResetAnimationState();
	virtual bool NetSerialize( TSerialize ser, EEntityAspects aspect, uint8 profile, int flags );
	virtual void PostSerialize();
	virtual void SetChannelId(uint16 id);
	virtual void  SerializeLevelToLevel( TSerialize &ser );
	virtual IInventory* GetInventory() const;
	virtual void NotifyCurrentItemChanged(IItem* newItem) {};

	virtual bool IsPlayer() const;
	virtual bool IsClient() const;
	virtual bool IsMigrating() const { return m_isMigrating; }
	virtual void SetMigrating(bool isMigrating) { m_isMigrating = isMigrating; }

	virtual bool Init( IGameObject * pGameObject );
	virtual void InitClient( int channelId );
	virtual void PostInit( IGameObject * pGameObject );
	virtual void PostInitClient(int channelId) {};
	virtual bool ReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params );
	virtual void PostReloadExtension( IGameObject * pGameObject, const SEntitySpawnParams &params );
	virtual bool GetEntityPoolSignature( TSerialize signature );
	virtual void Update(SEntityUpdateContext& ctx, int updateSlot);
	virtual void UpdateView(SViewParams &viewParams) {};
	virtual void PostUpdateView(SViewParams &viewParams) {};
	void UpdateBodyDestruction(float frameTime);
	virtual void ReadDataFromXML(bool isReloading = false);

	virtual void InitLocalPlayer() {};

	virtual void SetIKPos(const char *pLimbName, const Vec3& goalPos, int priority);

	virtual void HandleEvent( const SGameObjectEvent& event );
	virtual void PostUpdate(float frameTime);
	virtual void PostRemoteSpawn() {};

	virtual bool IsThirdPerson() const { return true; };
	virtual void ToggleThirdPerson(){}


	//**************************************

	virtual void UpdateNewStats(float frameTime);
	virtual void UpdateActorScriptFnc(float frameTime);
	virtual void UpdateAimProcess(float frameTime);

	virtual bool ActCanJump();
	virtual void ActJump();
	//virtual void SetHealth(int health);
	virtual void SetGender(int gender);
	virtual void SetStamina(int stamina);
	virtual void SetMagicka(int magicka);
	virtual void SetStrength(float strength);
	virtual void SetAgility(float agility);
	virtual void SetWillpower(float willpower);
	virtual void SetEndurance(float endurance);
	virtual void SetPersonality(float personality);
	virtual void SetIntelligence(float intelligence);
	virtual void SetLevel(float level);
	virtual void SetDialogStage(int stage);
   
	virtual void SetArmor(float armor);
	virtual void SetGold(int gold);
	//virtual void DamageInfo(EntityId shooterID, EntityId weaponID, float damage, const char *damageType);
	//virtual IAnimatedCharacter * GetAnimatedCharacter() { return m_pAnimatedCharacter; }
	//void SetMaxHealth(int maxHealth);
	void SetMaxStamina(int maxStamina);
	void SetMaxMagicka(int maxMagicka);
	virtual int32 GetStrength() const { return int32(m_strength + str_mod); }
	//virtual int32 GetHealth() const { return int32(m_health); }
	virtual int32 GetLevel() const { return int32(m_level); }
	virtual int32 GetWillpower() const { return int32(m_willpower + will_mod); }
	virtual int32 GetEndurance() const { return int32(m_endurance + endr_mod); }
	virtual int32 GetPersonality() const { return int32(m_personality + pers_mod); }
	virtual int32 GetAgility() const { return int32(m_agility + agl_mod); }
	virtual int32 GetIntelligence() const { return int32(m_intelligence + int_mod); }
	//virtual int32 GetMaxHealth() const { return int32((m_maxHealth + maxhp_mod)*maxhp_ml); }
	virtual int32 GetMagicka() const { return int32(m_magicka); }
	virtual int32 GetMaxMagicka() const { return int32((m_maxMagicka + maxmp_mod)*maxmp_ml); }
	virtual int32 GetArmor() const { return int32(m_armor); }
	virtual int32 GetMaxArmor() const { return 0; }
	virtual int32 GetStamina() const { return int32(m_stamina); }
	virtual int32 GetMaxStamina() const { return int32((m_maxStamina + maxst_mod)*maxst_ml); }
	virtual int32 GetDialogStage() const { return int32(m_dialogstage); }
	//virtual f32 GetSAPlayTime() const { return f32(m_special_action_anim_playing_time); }

	virtual int32 GetRealValMaxStamina() const { return int32(m_maxStamina); }
	virtual int32 GetRealValMaxHealth() const { return (int32)m_health.GetHealthMax(); }
	virtual int32 GetRealValMaxMagicka() const { return int32(m_maxMagicka); }

	virtual int32 GetRealValWillpower() const { return int32(m_willpower); }
	virtual int32 GetRealValEndurance() const { return int32(m_endurance); }
	virtual int32 GetRealValPersonality() const { return int32(m_personality); }
	virtual int32 GetRealValAgility() const { return int32(m_agility); }
	virtual int32 GetRealValIntelligence() const { return int32(m_intelligence); }
	virtual int32 GetRealValStrength() const { return int32(m_strength); }

	virtual int  GetGender() const;
	virtual int  GetGold() const;

	virtual float GetActorStrength() const;

	////////////////////////////////////////////////////////////////////
	virtual void SetRelaxedMod(bool relaxed_mode);
	virtual bool GetRelaxedMod() const { return m_relaxed_mode; }
	virtual void SetStealthMod(bool stealth_mode);
	virtual bool GetStealthMod() const { return m_stealth_mode; }
	////////////////////////////////////////////////////////////////////
	virtual void SetItemOnBackId(EntityId ItemId);
	virtual EntityId GetItemOnBackId() const { return item_on_back; }
	virtual f32 GetSAPlayTime() const { return f32(m_special_action_anim_playing_time); }
	virtual void SetSAPlayTime(float p_time);
	virtual void SetSACurrent(int id);
	virtual int GetSACurrent() const { return m_special_action_id; }
	virtual void AddXperiencePoints(float xp_points);
	virtual void ChekForLevelUP(int lvl, int lvlxp);
	virtual int GetMaxInvSlots() { return inventory_bag_max_slots; }//not more then i_inventory_capacity
	virtual void SetMaxInvSlots(int slots);
	virtual int GetNumNOItemsInInv();
	virtual void SetSpecRecoilAct(int id, float time);
	virtual float GetSpecRecoilAct(int id);
	virtual void RequestForDelayedPhysicaliseActor();
	virtual void RequestForDelayedAddEquipPacks();
	virtual void RequestToDeequipAllItemsFromEquipPacks();
	virtual void RequestToDeequipAllEquippedItems();
	virtual void RequestToReequipUsedItemsOnPSerialize();
	virtual void NET_RequestToReequipUsedItemsOnPSerialize();
	virtual void RequestToReloadActorModelOrReinitAttachments(bool clear, bool serializing_event = false);
	virtual float GetResistForDamageType(int type);
	virtual void SetResistForDamageType(int type, float resist_val);
	virtual void AddResistForDamageType(int type, float resist_val);
	virtual void DelResistForDamageType(int type, float resist_val);
	virtual float GetResistForDamageTypeByName(const char *name);
	virtual void SetResistForDamageTypeByName(const char *name, float resist_val);
	virtual void AddResistForDamageTypeByName(const char *name, float resist_val);
	virtual void DelResistForDamageTypeByName(const char *name, float resist_val);
	virtual EntityId GetAnyItemInLeftHand();
	virtual EntityId GetAnyItemInLeftHandCC() const;//need const to call in some const functions
	virtual EntityId GetNoWeaponId();
	virtual bool CanDoAnyActionInCastStateWithSWeapon(EntityId selected_wpn_id);
	////////////////////////////////////////////////////////////////////
	//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*--*-*-*-*--***--*-*-*-*-

	virtual void RequestFacialExpression(const char* pExpressionName /* = NULL */, f32* sequenceLength = NULL);
	virtual void PrecacheFacialExpression(const char* pExpressionName);

	virtual void NotifyInventoryAmmoChange(IEntityClass* pAmmoClass, int amount);
	virtual EntityId	GetGrabbedEntityId() const { return 0; }

	virtual void HideAllAttachments(bool isHiding);

	virtual void OnAIProxyEnabled(bool enabled);
	virtual void OnReturnedToPool() {};
	virtual void OnPreparedFromPool() {};

	virtual void OnReused(IEntity *pEntity, SEntitySpawnParams &params);
	// ~IActor

	// IGameObjectProfileManager
	virtual bool SetAspectProfile( EEntityAspects aspect, uint8 profile );
	virtual uint8 GetDefaultProfile( EEntityAspects aspect ) { return aspect == eEA_Physics? eAP_NotPhysicalized : 0; }
	// ~IGameObjectProfileManager

	// IActionListener
	virtual void OnAction(const ActionId& actionId, int activationMode, float value);
	// ~IActionListener

	virtual void AddHeatPulse(const float intensity, const float time);

	void SetGrabbedByPlayer(IEntity* pPlayerEntity, bool grabbed);

	//------------------------------------------------------------------------
	//переменные и модификаторы характеристик персонажа
	//------------------------------------------------------------------------
	//модификаторы базовых атрибутов персонажа
	int str_mod = 0;
	int agl_mod = 0;
	int int_mod = 0;
	int will_mod = 0;
	int endr_mod = 0;
	int pers_mod = 0;
	//модификаторы максимального уровня трёх базовых характеристик
	float maxhp_mod = 0;
	float maxmp_mod = 0;
	float maxst_mod = 0;
	//тип блока и активен ли он
	bool m_blockactiveshield = false;
	bool m_blockactivenoshield = false;
	//множители урона оружием определённого типа
	float bows_ml = 0;
	float swords_ml = 0;
	float thswords_ml = 0;
	float poles_ml = 0;
	float thpoles_ml = 0;
	float axes_ml = 0;
	float thaxes_ml = 0;
	float crossbows_ml = 1.0f;
	//множители скорости регенерации трёх базовых характеристик
	float hpreg_ml = 0;
	float mpreg_ml = 0;
	float streg_ml = 0;
	//множители максимального уровня трёх базовых характеристик
	float maxhp_ml = 0;
	float maxmp_ml = 0;
	float maxst_ml = 0;
	//базовый параметр скорости регенерации трёх базовых характеристик
	float hpreg_base = 0;
	float mpreg_base = 0;
	float streg_base = 0;
	//базовый параметр скорости растраты выносливости при беге
	float st_run_drain_base = 0;
	//тайминг растраты выносливости при беге
	float st_run_drain_timing = 0;
	//тайминг скорости регенерации трёх базовых характеристик(1 секунда по умолчанию)
	float hpreg_timing = 1.0f;
	float mpreg_timing = 1.0f;
	float streg_timing = 1.0f;

	float streg_paused_time = 0.0f;

	float update_actor_script_timing = 1.0f;
	float update_actor_script_time = 0.0f;

	float hpreg_ft = 0;
	float mpreg_ft = 0;
	float streg_ft = 0;
	//множители вероятности и урона при критической атаке для разных типов оружия
	float crtchance_common_ml = 0;
	float crtdmg_common_ml = 0;

	float crtchance_bows_ml = 0;
	float crtdmg_bows_ml = 0;

	float crtchance_crossbows_ml = 1.0f;
	float crtdmg_crossbows_ml = 1.0f;

	float crtchance_swords_ml = 0;
	float crtdmg_swords_ml = 0;

	float crtchance_thswords_ml = 0;
	float crtdmg_thswords_ml = 0;

	float crtchance_poles_ml = 0;
	float crtdmg_poles_ml = 0;

	float crtchance_thpoles_ml = 0;
	float crtdmg_thpoles_ml = 0;

	float crtchance_axes_ml = 0;
	float crtdmg_axes_ml = 0;

	float crtchance_thaxes_ml = 0;
	float crtdmg_thaxes_ml = 0;
	//множители растраты маны на заклинания и их мощность
	float selfspells_mpcons_common_ml = 0;
	float spells_mpcons_common_ml = 0;
	float selfspells_pwr_common_ml = 0;
	float spells_pwr_common_ml = 0;
	//базовые параметры потребления выносливости при определённых действиях
	float stamina_cons_jump_base = 0;
	float stamina_cons_run_base = 0;
	float stamina_cons_atk_base = 0;
	//множители растраты выносливости при определённых действиях
	float stamina_cons_jump_ml = 0;
	float stamina_cons_run_ml = 0;
	float stamina_cons_atk_ml = 0;
	float stamina_cons_block_ml = 0;
	//множители растраты выносливости при при перегреве и замерзании персонажа
	float stamina_cons_ml_overheat = 0;
	float stamina_cons_ml_frost = 0;
	//множители растраты маны при при перегреве и замерзании персонажа
	float magicka_cons_ml_overheat = 0;
	float magicka_cons_ml_frost = 0;

	bool perks[65535];
	int perks_level[65535];
	bool buffs[65535];
	float buffs_time[65535];

	std::vector<int> m_spells_ids_for_serializing;

	std::vector<EntityId> m_delayed_left_hand_phy_wpns_equip;
	void DelayedReequipPhyReqItems(void* pUserData, IGameFramework::TimerID handler);
	uint32 m_TimerDelayedReequipPhyReqItems;

	struct SActorPassiveEffectRK
	{
		SActorPassiveEffectRK()
		{
			perk_id = 0;
			perk_lvl = 0;
		};
		int perk_id;
		int perk_lvl;
	};
	std::vector<SActorPassiveEffectRK> m_passive_effects;
	struct SActorTimedEffectsFtg
	{
		SActorTimedEffectsFtg()
		{
			buff_id = 0;
			buff_time = 0.0f;
			effect_id = 0;
		};
		int buff_id;
		float buff_time;
		int effect_id;
	};
	std::vector<SActorTimedEffectsFtg> m_timed_effects_buffs;
	//-----------------Equipment quick slots--------------------
	AcQuickSlot m_quick_eq_slots[MAX_QUICK_EQUIPMENT_SLOTS];
	AcQuickSlot GetQuickSlotInfo(int slot);
	void SetQuickSlotInfo(int slot, AcQuickSlot info);
	void RemoveQuickSlotInfo(int slot);
	void UseQuickSlot(int slot);
	//----------------------------------------------------------
	bool m_rgt_hand_blc= false;
	bool m_lft_hand_blc = false;

	int num_perks = 0;
	int	num_buffs = 0;
	std::list<int> m_buffs;
	//****************************************** переменные инстанции игрока, но могут быть использованы ИИ персонажами.
	int levelxp;
	float m_hpRegenRate;
	float m_mpRegenRate;
	float m_stRegenRate;
	float m_hpTime;
	float m_hpAcc;
	float m_stAcc;
	float m_mpAcc;
	float m_run_culldown;
	float m_run_multipler;
	float m_jump_timer;

	int inventory_bag_max_slots;
	//-------------------------------TravelerNotes Game variable----------------
	float m_process_falling_timer_c1;
	float m_process_falling_timer_c2;
	Vec3 m_process_falling_current_vector;
	Vec3 m_process_falling_controll_vector;
	Quat m_process_falling_controll_start_rot;
	Quat m_process_falling_controll_dinamic_rot;
	float m_falling_max_speed_add;
	float m_falling_max_speed_multipler;
	float m_falling_base_speed;
	float m_falling_vector_z_add;
	float m_falling_vector_max_vel_xyz;
	float old_veloc_z = 0.0f;
	float old_gravity_z = 0.0f;
	//------------------------------------------------------------------------
	float base_mass_val = 0.0f;
	float inventory_mass_val = 0.0f;
	Vec3 Current_target_dir;
	float attack_timer_smpl = 0.0f;
	float alt_aim_pose_old_angle_horiz = 0.0f;
	float alt_aim_pose_old_angle_vert = 0.0f;
	std::vector<CGameXMLSettingAndGlobals::SActorSpell> m_actor_spells_book;
	CGameXMLSettingAndGlobals::SActorSpell Current_actor_spells[MAX_USABLE_SPELLS_SLOTS];
	float damage_resists[MAX_DAMAGE_RESIST_TYPES];
	Vec3 last_damage_hit_pos;
	Vec3 last_damage_hit_normal;
	int blood_loose_effect_counter = 0;
	float spec_action_timers_out_combo[100];
	float spec_action_time_to_recharge_combo[100];
	int spec_action_counter_in_combo[100];
	int spec_action_counter_max_in_combo[100];
	float reequip_timer_cn1 = 0.0f;
	float rload_pl_model_timer_after_ps = 0.0f;
	float serialization_reinit_customization_setting_on_character = 0.0f;
	EntityId equipped_cngtie[eAESlots_Number];
	string equipped_cngtie_names[eAESlots_Number];
	bool imd_equip_at_next;
	std::vector<EntityId> m_actor_items_to_imd_equip;//items equipped on next frame
	Quat m_to_target_quaternion;
	float m_complex_attachments_timer = 0.0f;
	float lazy_update_timer = 0.0f;
	float lazy_update_timer_next_ft = 0.0f;
	float start_update_timer_xc = 0.0f;
	bool block_updates = false;
	bool block_updates_cn = false;
	///---------------------------------------
	void CreateBloodLooseEffect();
	int GetBloodLooseEffectNumder();
	void SetBloodLooseEffectNumder(int num);
	void ClearBloodLooseEffects();
	void ResetActorVars();
	void OnCollisionEvent(EventPhysCollision *physCollision);
	void RequestToRunLuaFncFromActorScriptInstance(const char* func, const char* arg, bool fnc_from_gamerules = false);
	void AddSpellToActorSB(int id, bool from_xml = false);
	CGameXMLSettingAndGlobals::SActorSpell GetSpellInfoFromActorSB(int id);
	bool IsSpellInActorSB(int id);
	void AddSpellToCurrentSpellsForUseSlot(int id, int slot);
	void NET_AddSpellToCurrentSpellsForUseSlot(int id, int slot);
	void DelSpellFromCurrentSpellsForUseSlot(int id, int slot);
	void ClearActorSB(bool full);
	CGameXMLSettingAndGlobals::SActorSpell GetSpellFromCurrentSpellsForUseSlot(int slot);
	int GetSpellIdFromCurrentSpellsForUseSlot(int slot);
	CActorSpellsActions* m_pActorSpellsActions;
	virtual int32 FindNearestBone(Vec3 pos, float distance = 0.0f);
	virtual Vec3 GetBonePosition(const char* bone_name, bool local = false);
	virtual Quat GetBoneRotation(const char* bone_name);
	virtual void SetLastDamagePosAndNormal(Vec3 pos, Vec3 normal);
	virtual void SetGravityZ(float gr_z);
	virtual void SetInertiaAndInertiaAccel(float inertia, float inertiaAcc);
	bool IsActorVelocityMoreThan(int velType, float velValue);
	int GetRealItemsCountInInventory();
	bool CanDoAnyActionWNeededTwoHands();
	string GetActorName();
	void CheckActorBodyPartsAfterPSerialize(int chr_slot = 0);
	void CheckActorBodyAndAllAttachments(int chr_slot = 0);
	void Net_CheckActorBodyAndAllAttachments(int chr_slot = 0);
	void UpdateAllSkinAttachments(int chr_slot = 0);
	void HideOrUnhideAttachment(int characterSlot, const char * attachmentName, bool hide);
	int update_all_skin_attachment_at_next_frame = 0;
	int update_quiver_attachment = 0;
	void UpdateQuivers();
	void UpdateSkinAttachmentsAndEts(float frameTime);
	void UpdateAdditionalEquipmentPacksAdding(float frameTime, int mode);
	void UpdateSpecialActions(float frameTime);
	void UpdateTargeting(float frameTime);
	void UpdateNonPlayerActorsFallingEvents(float frameTime);
	void SetBlockActive(int blc_type, bool active);
	struct SDelayedAttachments
	{
		SDelayedAttachments()
		{
			attachment_name = "";
			attachment_binding_path = "";
		};
		string attachment_name;
		string attachment_binding_path;
		//string attachment_binding_material;
	};
	std::vector<SDelayedAttachments> s_SDelayedAttachments;
	ILINE Vec3 GetLastDamagePos(){ return last_damage_hit_pos; }
	ILINE Vec3 GetLastDamageNormal(){ return last_damage_hit_normal; }

	ILINE float GetAirControl() const { return m_airControl; };
	ILINE float GetAirResistance() const { return m_airResistance; };
	ILINE float GetInertia() const { return m_inertia; }
	ILINE float GetInertiaAccel() const { return m_inertiaAccel; }
	ILINE float GetTimeImpulseRecover() const { return m_timeImpulseRecover; };

	virtual void SetViewRotation( const Quat &rotation ) {};
	virtual Quat GetViewRotation() const { return GetEntity()->GetRotation(); };
	virtual void EnableTimeDemo( bool bTimeDemo ) {};

	// offset to add to the computed camera angles every frame
	virtual void AddViewAngleOffsetForFrame(const Ang3 &offset);

	//------------------------------------------------------------------------
	virtual void Revive( EReasonForRevive reasonForRevive = kRFR_Spawn );
	virtual void Reset(bool toGame);
	//physicalization
	static bool LoadPhysicsParams(SmartScriptTable pEntityTable, const char* szEntityClassName, SEntityPhysicalizeParams &outPhysicsParams, 
		pe_player_dimensions &outPlayerDim, pe_player_dynamics &outPlayerDyn);
	virtual void Physicalize(EStance stance=STANCE_NULL);
	virtual void PostPhysicalize();
	virtual void RagDollize( bool fallAndPlay ) {}
	void ShutDown();
	//
  virtual int IsGod(){ return 0; }

	//reset function clearing state and animations for teleported actor
	virtual void OnTeleported() {}

	virtual void SetSpectatorState(uint8 state) {}
	virtual EActorSpectatorState GetSpectatorState() const { return eASS_None; }

	virtual float GetSpectatorOrbitYawSpeed() const { return 0.f; }
	virtual void SetSpectatorOrbitYawSpeed(float yawSpeed, bool singleFrame) {}
	virtual bool CanSpectatorOrbitYaw() const { return false; }
	virtual float GetSpectatorOrbitPitchSpeed() const { return 0.f; }
	virtual void SetSpectatorOrbitPitchSpeed(float pitchSpeed, bool singleFrame) {}
	virtual bool CanSpectatorOrbitPitch() const { return false; }
	virtual void ChangeCurrentFollowCameraSettings(bool increment) {}

	virtual void SetSpectatorModeAndOtherEntId(const uint8 _mode, const EntityId _othEntId, bool isSpawning=false) {};

	virtual uint8 GetSpectatorMode() const { return 0; };
	virtual void SetSpectatorTarget(EntityId targetId) {};
	virtual EntityId GetSpectatorTarget() const { return 0; };
	virtual void SetSpectatorFixedLocation(EntityId locId) {};
	virtual EntityId GetSpectatorFixedLocation() const { return 0; };

	//get actor status
	virtual SActorStats *GetActorStats() { return 0; };
	virtual const SActorStats *GetActorStats() const { return 0; };
	SActorParams &GetActorParams() { return m_params; };
	const SActorParams &GetActorParams() const { return m_params; };

	float GetSpeedMultiplier(SActorParams::ESpeedMultiplierReason reason);
	void SetSpeedMultipler(SActorParams::ESpeedMultiplierReason reason, float fSpeedMult);
	void MultSpeedMultiplier(SActorParams::ESpeedMultiplierReason reason, float fSpeedMult);
	void SetStanceMaxSpeed(uint32 stance, float fValue);

	virtual void SetStats(SmartScriptTable &rTable);
	virtual ICharacterInstance *GetFPArms(int i) const { return GetEntity()->GetCharacter(3+i); };
	//set/get actor params
	static bool LoadGameParams(SmartScriptTable pEntityTable, SActorGameParams &outGameParams);
	static bool LoadDynamicAimPoseElement(CScriptSetGetChain& gameParamsTableChain, const char* szName, string& output);
	void InitGameParams();
	virtual void SetParamsFromLua(SmartScriptTable &rTable);
	//
	virtual void Freeze(bool freeze) {};
	virtual void Fall(Vec3 hitPos = Vec3(0,0,0));
	void Fall(const HitInfo& hitInfo);
	virtual void KnockDown(float backwardsImpulse);

  virtual void SetLookAtTargetId(EntityId targetId, float interpolationTime=1.f);
  virtual void SetForceLookAtTargetId(EntityId targetId, float interpolationTime=1.f);

	virtual void StandUp();
	virtual bool IsFallen() const;
	virtual bool IsDead() const;

	//
	virtual IEntity *LinkToVehicle(EntityId vehicleId);
	virtual void LinkToMountedWeapon(EntityId weaponId) {};
	virtual IEntity *LinkToEntity(EntityId entityId, bool bKeepTransformOnDetach=true);
	virtual void StartInteractiveAction(EntityId entityId, int interactionIndex = 0);
	virtual void StartInteractiveActionByName(const char* interaction, bool bUpdateVisibility, float actionSpeed = 1.0f);
	virtual void EndInteractiveAction(EntityId entityId);
	
	virtual bool	AllowLandingBob() { return true; }
	
	virtual IEntity *GetLinkedEntity() const
	{
		return m_linkStats.GetLinked();
	}

	virtual IVehicle *GetLinkedVehicle() const
	{
		return m_linkStats.GetLinkedVehicle();
	}

	float GetLookFOV(const SActorParams &actorParams) const
	{
		return GetLinkedVehicle() ? actorParams.lookInVehicleFOVRadians : actorParams.lookFOVRadians;
	}

	uint32 GetAimIKLayer(const SActorParams &actorParams) const
	{
		return actorParams.aimIKLayer;
	}

	uint32 GetLookIKLayer(const SActorParams &actorParams) const
	{
		return actorParams.lookIKLayer;
	}

	virtual void SetViewInVehicle(Quat viewRotation) {};

	virtual void SupressViewBlending() {};

	ILINE Vec3 GetLBodyCenter()
	{
		const SStanceInfo *pStance(GetStanceInfo(GetStance()));
		return Vec3(0,0,(pStance->viewOffset.z - pStance->heightPivot) * 0.5f);
	}

	ILINE Vec3 GetWBodyCenter()
	{
		return GetEntity()->GetWorldTM() * GetLBodyCenter();
	}

	//for animations
	virtual void PlayAction(const char *action,const char *extension, bool looping=false) {};
	//
	virtual void SetMovementTarget(const Vec3 &position,const Vec3 &looktarget,const Vec3 &up,float speed) {};
	//
	virtual void CreateScriptEvent(const char *event,float value,const char *str = NULL);
	virtual bool CreateCodeEvent(SmartScriptTable &rTable);
	virtual void AnimationEvent(ICharacterInstance *pCharacter, const AnimEventInstance &event);

	virtual void SetTurnAnimationParams(const float turnThresholdAngle, const float turnThresholdTime);

	virtual void CameraShake(float angle,float shift,float duration,float frequency,Vec3 pos,int ID,const char* source="") {};
	//
	virtual void SetAngles(const Ang3 &angles) {};
	virtual Ang3 GetAngles() {return Ang3(0,0,0);};
	virtual void AddAngularImpulse(const Ang3 &angular,float deceleration=0.0f,float duration=0.0f){}
	//
	virtual void SetViewLimits(Vec3 dir,float rangeH,float rangeV) {};
	virtual void DamageInfo(EntityId shooterID, EntityId weaponID, IEntityClass *pProjectileClass, float damage, int damageType, const Vec3 hitDirection);
	virtual IAnimatedCharacter * GetAnimatedCharacter() { return m_pAnimatedCharacter; }
	virtual const IAnimatedCharacter * GetAnimatedCharacter() const { return m_pAnimatedCharacter; }
	virtual void PlayExactPositioningAnimation( const char* sAnimationName, bool bSignal, const Vec3& vPosition, const Vec3& vDirection, float startWidth, float startArcAngle, float directionTolerance ) {}
	virtual void CancelExactPositioningAnimation() {}
	virtual void PlayAnimation( const char* sAnimationName, bool bSignal ) {}
	virtual EntityId GetCurrentTargetEntityId() const { return 0; }
	virtual const Vec3 * GetCurrentTargetPos() const { return NULL; }

	virtual void  SetMaxHealth( float maxHealth );
	virtual float GetMaxHealth() const { return ((m_health.GetHealthMax() + maxhp_mod)*maxhp_ml); }
	virtual void  SetHealth( float health );
	virtual float GetHealth() const { return m_health.GetHealth(); }
	virtual int   GetHealthAsRoundedPercentage() const { return m_health.GetHealthAsRoundedPercentage(); }
	//virtual int32 GetArmor() const { return 0; }
	//virtual int32 GetMaxArmor() const { return 0; }
	virtual int   GetTeamId() const { return m_teamId; }
	virtual void Kill();
	virtual void DeathIncome();
	//-------------------------------------------------------------------------------------
	virtual void SetLevelXp(int levelxp);
	virtual int32 GetLevelXp() const { return int32(m_levelxp); }
	virtual void SetXpToNextLevel(int xptonextlevel);
	virtual int32 GetXpToNextLevel() const { return int32(m_xptonextlevel); }
	virtual void SetHeadType(int plheadtype);
	virtual int32 GetHeadType() const { return int32(m_plheadtype); }
	virtual void SetHairType(int plhairtype);
	virtual int32 GetHairType() const { return int32(m_plhairtype); }
	virtual void SetEyesType(int pleyestype);
	virtual int32 GetEyesType() const { return int32(m_pleyestype); }
	//virtual void SetPlayerModelStr(const char* mdl);
	//--------------------------------------------------------------------------------------
	void ImmuneToForbiddenZone(const bool immune);
	const bool ImmuneToForbiddenZone() const;

	void NotifyInventoryAboutOwnerActivation();
	void NotifyInventoryAboutOwnerDeactivation();

	virtual bool IsSwimming() const {	return false; };
	virtual bool IsHeadUnderWater() const { return false; }

	virtual bool IsSprinting() const { return false; }
	virtual bool CanFire() const { return true; }

	//stances
	ILINE EStance GetStance() const 
	{
		return m_stance.Value();
	}

	ILINE const SStanceInfo *GetStanceInfo(EStance stance) const 
	{
		if (stance < 0 || stance > STANCE_LAST)
			return &m_defaultStance;
		return &m_stances[stance];
	}

	CActorWeakPtr GetWeakPtr() const { return m_pThis; }
	CActorTelemetry *GetTelemetry() { return &m_telemetry; }

	// forces the animation graph to select a state
	void QueueAnimationState( const char * state );

	//
	ILINE int GetBoneID(int ID) const
	{
		CRY_ASSERT((ID >= 0) && (ID < BONE_ID_NUM));
		if((ID >= 0) && (ID < BONE_ID_NUM))
		{
			return m_boneIDs[ID];
		}

		return -1;
	}

	ILINE bool HasBoneID(int ID) const
	{
		CRY_ASSERT((ID >= 0) && (ID < BONE_ID_NUM));
		return m_boneIDs[ID] >= 0;
	}

	ILINE const QuatT &GetBoneTransform(int ID) const
	{
		CRY_ASSERT((ID >= 0) && (ID < BONE_ID_NUM));
		CRY_ASSERT_MESSAGE(m_boneIDs[ID] >= 0, string().Format("Accessing unmapped bone %s in %s", s_BONE_ID_NAME[ID], GetEntity()->GetName()));

		return m_boneTrans[ID];
	}

	virtual Vec3 GetLocalEyePos() const;
	QuatT GetCameraTran() const;
	QuatT GetHUDTran() const;

	ILINE static const SActorAnimationEvents& GetAnimationEventsTable() { return s_animationEventsTable; };

	virtual void UpdateMountedGunController(bool forceIKUpdate);

	virtual void OnPhysicsPreStep(float frameTime){};

	virtual bool CheckInventoryRestrictions(const char *itemClassName);

	//
	void ProcessIKLimbs(float frameTime);

	//IK limbs
	int GetIKLimbIndex(const char *limbName);
	ILINE SIKLimb *GetIKLimb(int limbIndex)
	{
		return &m_IKLimbs[limbIndex];
	}
	void CreateIKLimb(const SActorIKLimbInfo &limbInfo);

	//
	virtual IMovementController * GetMovementController() const
	{
		return m_pMovementController;
	}

	CDamageEffectController& GetDamageEffectController() { return m_damageEffectController; }
	uint8 GetActiveDamageEffects() const { return m_damageEffectController.GetActiveEffects(); }
	uint8 GetDamageEffectsResetSwitch() const { return m_damageEffectController.GetEffectResetSwitch(); }
	uint8 GetDamageEffectsKilled() const { return m_damageEffectController.GetEffectsKilled(); }
	void SetActiveDamageEffects(uint8 active) { m_damageEffectController.SetActiveEffects(active); }
	void SetDamageEffectsResetSwitch(uint8 reset) { m_damageEffectController.SetEffectResetSwitch(reset); }
	void SetDamageEffectsKilled(uint8 killed) { m_damageEffectController.SetEffectsKilled(killed); }
	CActorImpulseHandlerPtr GetImpulseHander() { return m_pImpulseHandler; }

	//stances
	void OnSetStance( EStance stance );
	virtual void SetStance(EStance stance);
	virtual void OnStanceChanged(EStance newStance, EStance oldStance);
	virtual bool TrySetStance(EStance stance);
	
	//Cloak material
	enum eFadeRules
	{
		eAllowFades = 0,
		eDisallowFades,
	};
	virtual void SetCloakLayer(bool set, eFadeRules config = eAllowFades);
	
	IAnimationGraphState * GetAnimationGraphState();
	void SetFacialAlertnessLevel(int alertness);

	//weapons
	virtual IItem *GetCurrentItem(bool includeVehicle=false) const;
	EntityId GetCurrentItemId(bool includeVehicle=false) const;
	virtual IItem *GetHolsteredItem() const;
	EntityId GetHolsteredItemId() const;
	void ProceduralRecoil( float duration, float kinematicImpact, float kickIn/*=0.8f*/, int arms = 0/*0=both, 1=right, 2=left*/);

	//Net
	EntityId NetGetCurrentItem() const;
	void NetSetCurrentItem(EntityId id, bool forceDeselect);

	EntityId NetGetLastItemId() { return m_lastNetItemId; };

	EntityId NetGetScheduledItem() const;
	void NetSetScheduledItem(EntityId id);

	virtual void SwitchDemoModeSpectator(bool activate) {};	//this is a player only function

	//Body damage / destruction
	ILINE TBodyDamageProfileId GetCurrentBodyDamageProfileId() const { return (m_OverrideBodyDamageProfileId != INVALID_BODYDAMAGEPROFILEID) ? m_OverrideBodyDamageProfileId : m_DefaultBodyDamageProfileId; }
	void ReloadBodyDestruction();

	const CBodyDestrutibilityInstance& GetBodyDestructibilityInstance() const { return m_bodyDestructionInstance; }

	float GetBodyDamageMultiplier(const HitInfo &hitInfo) const;
	float GetBodyExplosionDamageMultiplier(const HitInfo &hitInfo) const;
	uint32 GetBodyDamagePartFlags(const int partID, const int materialID) const;
	TBodyDamageProfileId GetBodyDamageProfileID(const char* bodyDamageFileName, const char* bodyDamagePartsFileName) const;
	void OverrideBodyDamageProfileID(const TBodyDamageProfileId profileID);
	bool IsHeadShot(const HitInfo &hitInfo) const;
	bool IsHelmetShot(const HitInfo & hitInfo) const;
	bool IsGroinShot(const HitInfo &hitInfo) const;
	bool IsFootShot(const HitInfo &hitInfo) const;
	bool IsKneeShot(const HitInfo &hitInfo) const;
	bool IsWeakSpotShot(const HitInfo &hitInfo) const;

	void FillHitInfoFromKillParams(const CActor::KillParams& killParams, HitInfo &hitInfo) const;

	void ProcessDestructiblesHit(const HitInfo& hitInfo, const float previousHealth, const float newHealth);
	void ProcessDestructiblesOnExplosion(const HitInfo& hitInfo, const float previousHealth, const float newHealth);

	//misc
	virtual const char* GetActorClassName() const { return "CActor"; };
	const IItemParamsNode* GetActorParamsNode() const;
	
	static  ActorClass GetActorClassType() { return (ActorClass)eActorClass_Actor; }
	virtual ActorClass GetActorClass() const { return (ActorClass)eActorClass_Actor; };

	virtual const char* GetEntityClassName() const { return GetEntity()->GetClass()->GetName(); }
	const IItemParamsNode* GetEntityClassParamsNode() const;
	static const char DEFAULT_ENTITY_CLASS_NAME[];

	bool IsPoolEntity() const;

	virtual void SetAnimTentacleParams(pe_params_rope& rope, float animBlend) {};

  virtual bool IsCloaked() const { return m_cloakLayerActive; }

  virtual void DumpActorInfo();

	virtual bool IsFriendlyEntity(EntityId entityId, bool bUsingAIIgnorePlayer = true) const;
	virtual float GetReloadSpeedScale() const { return 1.0f; }
	virtual float GetOverchargeDamageScale() const  { return 1.0f; }

	ILINE bool AllowSwitchingItems() { return m_enableSwitchingItems; }
	void EnableSwitchingItems(bool enable);
	void EnableIronSights(bool enable);
	void EnablePickingUpItems(bool enable);

	bool CanUseIronSights() const { return m_enableIronSights; }
	bool CanPickupItems() const { return m_enablePickupItems; }
	
	virtual void BecomeRemotePlayer();
	virtual bool BecomeAggressiveToAgent(EntityId entityID);

	ILINE uint8 GetNetPhysCounter() { return m_netPhysCounter; }

	virtual void GetMemoryUsage( ICrySizer * pSizer ) const;
	void GetInternalMemoryUsage( ICrySizer * pSizer ) const;
	
	static bool LoadFileModelInfo(SmartScriptTable pEntityTable, SmartScriptTable pProperties, SActorFileModelInfo &outFileModelInfo);
	virtual bool SetActorModel(const char* modelName=NULL, bool not_from_lua = false);
	//special for multiplayer support-------------------------------------------------------
	virtual bool Net_SetActorModel(const char* modelName = NULL, bool not_from_lua = false);
	virtual void Net_UpdateCharacterCustomozationInfo(int gender, int head, int hair, int eyes, string name);
	//--------------------------------------------------------------------------------------
	void UpdateActorModel();
	bool FullyUpdateActorModel();
	void UpdateActorModelFTO();
	void InvalidateCurrentModelName();

	void OnItemUPD(int state);
	
	virtual void PrepareLuaCache();

	void LockInteractor(EntityId lockId, bool lock);

	bool AllowPhysicsUpdate(uint8 newCounter) const;
	static bool AllowPhysicsUpdate(uint8 newCounter, uint8 oldCounter);

	virtual bool IsRemote() const;

	void AddLocalHitImpulse(const SHitImpulse& hitImpulse);
	
	static bool LoadAutoAimParams(SmartScriptTable pEntityTable, SAutoaimTargetRegisterParams &outAutoAimParams);

	float GetLastUnCloakTime(){return m_lastUnCloakTime;}

  virtual void EnableStumbling(PlayerActor::Stumble::StumbleParameters* stumbleParameters) {};
  virtual void DisableStumbling() {};

	const char *GetShadowFileModel();

	void CloakSyncAttachments(bool bFade);
	void CloakSyncEntity(EntityId entityId, bool bFade);

	virtual const float GetCloakBlendSpeedScale();

	EntityId SimpleFindItemIdInCategory(const char *category) const;
	void RegisterInAutoAimManager();
	void UnRegisterInAutoAimManager();

	void SetTag(TagID tagId, bool enable);
	void SetTagByCRC(uint32 tagId, bool enable);
	ILINE const SActorPhysics& GetActorPhysics() const { return m_actorPhysics; }

	bool IsInMercyTime() const;

	bool CanSwitchSpectatorStatus() const;
	void RequestChangeSpectatorStatus(bool spectate);
	void OnSpectateModeStatusChanged(bool spectate);

	virtual bool ShouldMuteWeaponSoundStimulus() const { return false; }

	void OnFall(const HitInfo& hitInfo);

	void EnableHitReactions() { m_shouldPlayHitReactions = true; }
	void DisableHitReactions() { m_shouldPlayHitReactions = false; }
	bool ShouldPlayHitReactions() const { return m_shouldPlayHitReactions; }

	EntityId GetPendingDropEntityId() const { return m_pendingDropEntityId; }

	// called by script code upon reset to acquire a lip-sync extension from the entity's script table or to release an existing one if none is provided
	void AcquireOrReleaseLipSyncExtension();

protected:

	void GenerateBlendRagdollTags();

	void PhysicalizeLocalPlayerAdditionalParts();

	// SetActorModel helpers
	bool SetActorModelFromScript();
	bool SetActorModelInternal(const char* modelName = NULL);
	bool SetActorModelInternal(const SActorFileModelInfo &fileModelInfo);

	static IAttachment* GetOrCreateAttachment(IAttachmentManager *pAttachmentManager, const char *boneName, const char *attachmentName);

	// Helper to sync cloak to given attachment
	void CloakSyncAttachment(IAttachment* pAttachment, bool bFade);

	//movement
	virtual IActorMovementController * CreateMovementController() = 0;
	//

	virtual void InitGameParams(const SActorGameParams &gameParams, const bool reloadCharacterSounds);
	void RebindScript();

	void RegisterInAutoAimManager(const SAutoaimTargetRegisterParams &autoAimParams);

	void RegisterDBAGroups();
	void UnRegisterDBAGroups();

	CItem * StartRevive(int teamId);
	void FinishRevive(CItem * pItem);
	virtual void SetModelIndex(uint8 modelIndex) {}

	virtual bool ShouldRegisterAsAutoAimTarget() { return true; }
	void SelectWeaponWithAmmo(EntityId outOfAmmoId, bool keepHistory);

	void AttemptToRecycleAIActor( );

	bool GetRagdollContext( CProceduralContextRagdoll** ppRagdollContext ) const;
	void PhysicalizeBodyDamage();

	bool IsBodyDamageFlag(const HitInfo &hitInfo, EBodyDamagePIDFlags) const;

	void SetupLocalPlayer();

	void UpdateAutoDisablePhys(bool bRagdoll);

	void RequestServerResync()
	{
		if (!IsClient())
			GetGameObject()->RequestRemoteUpdate(eEA_Physics | eEA_GameClientDynamic | eEA_GameServerDynamic | eEA_GameClientStatic | eEA_GameServerStatic);
	}
	EntityId GetLeftHandObject() const;

	//
	typedef std::vector<SIKLimb> TIKLimbs;
	
	//
	virtual bool UpdateStance();
	void UpdateLegsColliders();
	void ReleaseLegsColliders();

	static SActorAnimationEvents s_animationEventsTable;

	EntityId m_lastNetItemId;
	bool	m_isClient;
	bool	m_isPlayer;
	bool	m_isMigrating;
	CHealth m_health;

	CActorPtr	m_pThis;
	//--------------------------------------
	//ранен персонаж или нет
	int m_wounded_pl;
	//проигрывается ли анимация у раненого персонажа или нет
	bool m_wounded_anim_playing;
	//Переменная текущей выносливости персонажа
	float m_stamina;
	//Переменная текущей максимальной выносливости персонажа
	int32 m_maxStamina;
	//Переменная воздуха в лёгких персонажа
	float m_breath;
	//Переменная текущей брони персонажа
	float m_armor;
	int32 m_maxArmor;
	//Переменная текущей маны персонажа
	float m_magicka;
	//Переменная текущей максимальной маны персонажа
	int32 m_maxMagicka;
	//Переменные текущих характеристик персонажа
	float m_strength;
	float m_agility;
	float m_willpower;
	float m_endurance;
	float m_personality;
	float m_intelligence;
	//уровень персонажа
	float m_level;
	//текущее количество очков опыта персонажа
	float m_levelxp;
	//количество очков опыта персонажа, необходимое для перехода на след. уровень
	float m_xptonextlevel;
	//идентификатор головы персонажа
	int m_plheadtype;
	//идентификатор волос персонажа
	int m_plhairtype;
	//идентификатор глаз персонажа
	int m_pleyestype;
	//текущее количество пунктов, необходимых для поднятия характеристик персонажа
	float m_leveluppointsstat;
	//пол персонажа 0-М / 1-Ж
	int m_gender;
	//количество денег у персонажа
	int m_gold;
	//стадия диалога с персонажем
	int m_dialogstage;
	//пути к моделям брони, одежды, вещей на персонаже, одетых в данный момент
	string m_armboots;
	string m_armbootsa;
	string m_armbootsb;
	string m_armbootsc;
	string m_armbootsd;
	string m_armbootsf;
	string m_armbootsj;
	//--------------------
	string m_armlegs;
	string m_armlegsa;
	string m_armlegsb;
	string m_armlegsc;
	string m_armlegsd;
	string m_armlegsf;
	string m_armlegsj;
	//--------------------
	string m_armchest;
	string m_armchesta;
	string m_armchestb;
	string m_armchestc;
	string m_armchestd;
	string m_armchestf;
	string m_armchestj;
	//-----------------
	string m_armarms;
	string m_armarmsa;
	string m_armarmsb;
	string m_armarmsc;
	string m_armarmsd;
	string m_armarmsf;
	string m_armarmsj;
	//-----------------
	string m_armhelmet;
	string m_armpaudron;
	string m_armshield;
	string m_armtorch;


	//путь к базовому скелету персонажа//не используется
	const char *m_chrmodel;
	//путь к базовому скелету персонажа
	string m_chrmodelstr;
	//путь к базовой одежде персонажа
	string m_underwear01;
	string m_underwear02;
	//вам лучше незнать к чему этот путь
	string m_pppp;
	//путь к модели головы персонажа
	string m_headmodel;
	//путь к модели левого глаза персонажа
	string m_eye_l;
	//путь к модели правого глаза персонажа
	string m_eye_r;
	//путь к модели волос персонажа
	string m_hairmodel;
	//путь к модели нижней части тела персонажа
	string m_lowerbody_model;
	//путь к модели верхней части тела персонажа
	string m_upperbody_model;
	//путь к модели рук персонажа
	string m_hands_model;
	//путь к модели ног персонажа
	string m_foots_model;
	//*************************************
	//идентификатор выбранного оружия на данный момент//слот
	EntityId m_wpnselecteditem;
	//идентификатор одетых ботинок на данный момент//слот
	EntityId m_wear_boots_id;
	//идентификатор одетых перчаток на данный момент//слот
	EntityId m_wear_arms_id;
	//идентификатор одетого шлема на данный момент//слот
	EntityId m_wear_helm_id;
	//идентификатор одетой верхней части брони на данный момент//слот
	EntityId m_wear_curr_id;
	//идентификатор одетой нижней части брони на данный момент//слот
	EntityId m_wear_pants_id;
	//идентификатор экипированного щита на данный момент//слот
	EntityId m_wear_shield_id;
	//идентификатор экипированного факела/осветительного устройства на данный момент//слот
	EntityId m_wear_torch_id;
	//идентификатор выбранных стрел на данный момент//слот
	EntityId m_wear_ammoarrows_id;
	//идентификатор выбранного оружия в левой руке//слот
	EntityId m_wpn_left_hand_id;
	//массив идентификаторов надетых украшений и прочего(что можно надеть на персонажа) 
	EntityId m_wear_acc[eAESlots_Number];
	//параметр занятости определённого слота
	bool m_equipmentswitchboots;
	bool m_equipmentswitchlegs;
	bool m_equipmentswitchchest;
	bool m_equipmentswitcharms;
	bool m_equipmentswitchhead;
	bool m_equipmentswitchshield;
	bool m_equipmentswitchtorch;
	//количество частей в броне/одежде(подмоделей) для слотов брони/одежды
	int m_boots_num_parts;
	int m_llegs_num_parts;
	int m_chest_num_parts;
	int m_arms_num_parts;
	//--------------------------------------

	//***********************************

	//Skills(combat, weapon, other)

	//WPNS
	float m_weaponry;
	float m_bows;
	float m_crossbows;
	float m_shortswords;
	float m_longswords;
	float m_twohandswords;
	float m_daggers;
	float m_shortaxes;
	float m_longaxes;
	float m_twohandaxes;
	float m_onehandblunt;
	float m_twohandblunt;
	float m_onehandpolearm;
	float m_twohandpolearm;
	float m_mgcstaffs;
	float m_throwwpn;
	float m_wristswpn;
	//Combat
	float m_lightarmor;
	float m_heavyarmor;
	float m_mediumarmor;
	float m_clothes;
	float m_supportmgc;
	float m_combatmgc;
	float m_blockingshield;
	float m_blockingnoshield;


	//Other
	float m_enchanting;
	float m_spellmaiking;
	float m_sneaking;
	float m_lockpicking;

	//*******************************
	//float m_now;
	//float m_healTime;
	float m_stmnTime;
	//float m_startedSprinting;
	//float m_breathRegenDelay;
	//float m_staminaRegenDelay;
	//float m_magickaRegenDelay;
	//float m_healthRegenDelay;
	//float m_healthAccError;
	//float m_healthRegenRate;
	//float m_breathRegenRate;
	float m_staminaRegenRate;
	//float m_magickaRegenRate;
	//*******************************

	//******************************* специальные переключатели
	bool additional_pack_added;
	bool additional_pack2_added;
	bool additional_pack_add_request;
	bool additional_pack2_add_request;
	bool additional_pack_equipped;
	bool additional_pack2_equipped;
	std::list<EntityId> m_items_to_equip_pack1;
	std::list<EntityId> m_items_to_equip_pack2;
	float equipping_timer;
	bool torch_anim_started;
	float delayed_physicalization_request;
	float delayed_add_packs_request;
	//////////////////////////////
	bool m_stealth_mode;
	bool m_relaxed_mode;
	//////////////////////////////
	EntityId item_on_back;
	//////////////////////////////
	float m_special_recoil_action[15];
	float m_special_action_anim_playing_time;
	int m_special_action_id;
	
	//*********************************************************

private:
	IInventory * m_pInventory;

protected:
	CCoherentValue<EStance> m_stance;
	EStance m_desiredStance;

	static SStanceInfo m_defaultStance;
	SStanceInfo m_stances[STANCE_LAST];

	SActorParams m_params;

	IAnimatedCharacter *m_pAnimatedCharacter;
	IActorMovementController * m_pMovementController;
	CDamageEffectController m_damageEffectController;
	CActorImpulseHandlerPtr	m_pImpulseHandler;

	static IItemSystem			*m_pItemSystem;
	static IGameFramework		*m_pGameFramework;
	static IGameplayRecorder*m_pGameplayRecorder;

	mutable SLinkStats m_linkStats;

	TIKLimbs m_IKLimbs;

	uint8 m_currentPhysProfile;

	float m_airControl;
	float m_airResistance;
	float m_inertia;
	float m_inertiaAccel;
	float m_timeImpulseRecover;

	EntityId m_netLastSelectablePickedUp;
	EntityId m_pendingDropEntityId;
	
	string m_currModel;
	string m_currShadowModel;

	// Lua cache
	SLuaCache_ActorPhysicsParamsPtr m_LuaCache_PhysicsParams;
	SLuaCache_ActorGameParamsPtr m_LuaCache_GameParams;
	SLuaCache_ActorPropertiesPtr m_LuaCache_Properties;

	float			m_lastUnCloakTime;
	float			m_spectateSwitchTime;
	float			m_fAwaitingServerUseResponse;

	int				m_teamId;
	bool			m_IsImmuneToForbiddenZone; 

	bool m_enableSwitchingItems;
	bool m_enableIronSights;
	bool m_enablePickupItems;
	bool m_cloakLayerActive;

	uint8		m_netPhysCounter;				 //	Physics counter, to enable us to throw away old updates

	bool		m_registeredInAutoAimMng;
	bool		m_registeredAnimationDBAs;

	bool		m_bAllowHitImpulses;

	bool		m_bAwaitingServerUseResponse;

	bool		m_shouldPlayHitReactions;

protected:
	int16 m_boneIDs[BONE_ID_NUM];
	QuatT m_boneTrans[BONE_ID_NUM];

	CActorTelemetry m_telemetry;
	SActorPhysics m_actorPhysics;
	SBlendRagdollParams m_blendRagdollParams;

#ifndef DEMO_BUILD_RPG_SS
	uint8 m_tryToChangeStanceCounter;
#endif

	IPhysicalEntity  *m_pLegsCollider[2],*m_pLegsFrame,*m_pLegsIgnoredCollider;
	int								m_iboneLeg[2];
	Vec3							m_ptSample[2];
	char							m_bLegActive[2];

private:

	//Body damage/destruction

	// This profile originates from the entity's properties/archetype.
	TBodyDamageProfileId m_DefaultBodyDamageProfileId;
	CBodyDestrutibilityInstance m_bodyDestructionInstance;

	// The override body damage profile ID that can be manipulating through 
	// scripting (INVALID_BODYDAMAGEPROFILEID if the default should be used).
	TBodyDamageProfileId m_OverrideBodyDamageProfileId;

	string m_sLipSyncExtensionType;		// represents the type of the lip-sync extension if one was acquired; equals "" if none was acquired
	//std::shared_ptr<CIKTorsoAim> m_poseModifier_aim1;
	IAnimationOperatorQueuePtr m_poseModifier_aim2;
	//**************************************************
	Vec3 fdir_pos_for_pl_rv_mode;
	//std::shared_ptr<CIKTorsoAim> m_poseModifier_aim3;
};

#endif //__Actor_H__
