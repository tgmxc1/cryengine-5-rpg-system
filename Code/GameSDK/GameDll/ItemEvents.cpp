// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
-------------------------------------------------------------------------
$Id$
$DateTime$

-------------------------------------------------------------------------
History:
- 5:9:2005   14:55 : Created by M�rcio Martins

*************************************************************************/
#include "StdAfx.h"
#include "Item.h"
#include "IActorSystem.h"
#include "Actor.h"
#include "Game.h"
#include "ItemSharedParams.h"
#include "EntityUtility/EntityEffects.h"

#include "ActorActionsNew.h"


//------------------------------------------------------------------------
void CItem::OnStartUsing()
{
	// Corpses have weapons with 'no save' flag set, restore it after start using
	GetEntity()->SetFlags( GetEntity()->GetFlags() & ~ENTITY_FLAG_NO_SAVE );
}

//------------------------------------------------------------------------
void CItem::OnStopUsing()
{
}

//------------------------------------------------------------------------
void CItem::OnSelect(bool select)
{
}

//------------------------------------------------------------------------
void CItem::OnSelected(bool selected)
{
	const int numAccessories = m_accessories.size();

	//Let accessories know about it
	for (int i = 0; i < numAccessories; i++)
	{
		if (IItem* pAccessory = m_pItemSystem->GetItem(m_accessories[i].accessoryId))
		{
			pAccessory->OnParentSelect(selected);
		}
	}

	bool renderAlways = (IsOwnerFP() && !IsMounted() && selected);
	RegisterFPWeaponForRenderingAlways(renderAlways);
	//------------OnSelect check for armed bow----------------------------
	if (this->GetWeaponType() == 9)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->ForcedDisableBow(GetEntityId());
		}
	}
	//-------------------------------------------------------------------
}

//------------------------------------------------------------------------
void CItem::OnReloaded()
{
	const int numAccessories = m_accessories.size();
	for (int i = 0; i < numAccessories; i++)
	{
		CItem* pAccessory = static_cast<CItem*>(m_pItemSystem->GetItem(m_accessories[i].accessoryId));
		if (pAccessory)
			pAccessory->OnParentReloaded();
	}
}

//------------------------------------------------------------------------
void CItem::OnEnterFirstPerson()
{
	//Prevent FP model to show up when activating AI/Physics in editor
	if(gEnv->IsEditor() && gEnv->IsEditing())
		return;

	EnableUpdate(true, eIUS_General);
	SetViewMode(eIVM_FirstPerson);

	const int numAccessories = m_accessories.size();

	//Inform accessories as well
	for (int i = 0; i < numAccessories; i++)
	{
		if (CItem* pAccessory = static_cast<CItem*>(m_pItemSystem->GetItem(m_accessories[i].accessoryId)))
		{
			pAccessory->OnEnterFirstPerson();
		}
	}
}

//------------------------------------------------------------------------
void CItem::OnEnterThirdPerson()
{
	if (GetOwnerActor() && !GetOwnerActor()->IsRemote())
		EnableUpdate(true, eIUS_General);

	SetViewMode(eIVM_ThirdPerson);

	const int numAccessories = m_accessories.size();

	//Inform accessories as well
	for (int i = 0; i < numAccessories; i++)
	{
		if (CItem* pAccessory = static_cast<CItem*>(m_pItemSystem->GetItem(m_accessories[i].accessoryId)))
		{
			pAccessory->OnEnterThirdPerson();
		}
	}
}

//------------------------------------------------------------------------
void CItem::OnReset()
{
	//Hidden entities must have physics disabled
	if(!GetEntity()->IsHidden())
		GetEntity()->EnablePhysics(true);

	DestroyedGeometry(false);  
	if (!m_sharedparams->params.item_get_hp_info_from_lua)
	{
		m_stats.health = m_sharedparams->params.item_hp;
		m_properties.hitpoints = (int)m_sharedparams->params.item_mhp;
	}
	else
		m_stats.health = (float)m_properties.hitpoints;
	//m_stats.health = (float)m_properties.hitpoints;

	UpdateDamageLevel();

	if(m_sharedparams->params.scopeAttachment)
		DrawSlot(eIGS_Aux1,false); //Hide secondary FP scope

	if (m_properties.mounted && m_sharedparams->params.mountable)
		MountAt(GetEntity()->GetWorldPos());
	else
		SetViewMode(eIVM_ThirdPerson);

	if (m_properties.pickable)
	{
		const bool hasOwner = (GetOwnerId() != 0);
		DeferPhysicalize(hasOwner ? false : (m_properties.physics!=eIPhys_NotPhysicalized), (m_properties.physics==eIPhys_PhysicalizedRigid));
		
		Pickalize(!hasOwner, false);
	}
	else
		DeferPhysicalize((m_properties.physics!=eIPhys_NotPhysicalized), (m_properties.physics==eIPhys_PhysicalizedRigid));

	// Added to remove detonator (left hand 'attachToOwner' item) after a checkpoint
	// reload when it's equipped.
	const int numAccessories = m_accessories.size();
	for (int i = 0; i < numAccessories; i++)
	{
		if (CItem* pItem = static_cast<CItem*>(m_pGameFramework->GetIItemSystem()->GetItem(m_accessories[i].accessoryId)))
		{
			const SAccessoryParams* params = GetAccessoryParams( m_accessories[i].pClass );

			ResetCharacterAttachment(eIGS_FirstPerson, params->attach_helper.c_str(), params->attachToOwner);
		}
	}

	GetEntity()->InvalidateTM();
}

//------------------------------------------------------------------------
void CItem::OnHit(float damage, int hitType)
{  
	if(!m_properties.hitpoints)
		return;

	if (g_pGameCVars->g_enable_items_damage_and_repair <= 0)
		return;

	if (GetOwnerActor() && GetOwnerActor()->IsDead())
		return;

	if (!GetOwnerActor() && g_pGameCVars->g_enable_items_damage_and_repair_if_item_no_owner <= 0)
		return;

	static int repairHitType = g_pGame->GetGameRules()->GetHitTypeId("repair");

	if (hitType && hitType == repairHitType)
	{
		if (m_stats.health < m_properties.hitpoints) //repair only to maximum 
		{
			bool destroyed = m_stats.health<=0.f;
			m_stats.health = min(float(m_properties.hitpoints),m_stats.health+damage);

			UpdateDamageLevel();

			if(destroyed && m_stats.health>0.f)
				OnRepaired();
		}
	}
	else
	{
		if (m_stats.health > 0.0f)
		{ 
			m_stats.health -= damage;

			UpdateDamageLevel();

			if (m_stats.health <= 0.0f)
			{
				m_stats.health = 0.0f;
				OnDestroyed();
				
				int n=(int)m_sharedparams->damageLevels.size();
				for (int i=0; i<n; ++i)
				{
					const SDamageLevel &level = m_sharedparams->damageLevels[i];
					if (level.min_health == 0 && level.max_health == 0)
					{
						int slot=(m_stats.viewmode&eIVM_FirstPerson)?eIGS_FirstPerson:eIGS_ThirdPerson;

						EntityEffects::SEffectSpawnParams spawnParams(ZERO, Vec3Constants<float>::fVec3_OneZ, level.scale, 0.f, false);
						EntityEffects::SpawnParticleWithEntity(GetEntity(), slot, level.effect.c_str(), level.helper.c_str(), spawnParams);
					}
				}
			}
			CheckDamageEvents(damage, hitType);
		}
	}
}

void CItem::CheckDamageEvents(float damage, int hitType)
{
	if (damage > 0.0f && m_stats.health > 0.0f)
	{
		static int meleeHitType = g_pGame->GetGameRules()->GetHitTypeId("melee");
		if (hitType && hitType != meleeHitType)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (this->GetEquipped() == 1)
			{
				CActor *pActor = GetOwnerActor();
				if (!pActor)
					return;

				if (nwAction)
					nwAction->TestHitOnActorEquippedItem(pActor->GetEntityId(), (int)damage);
			}
		}
	}
	else if (damage > 0.0f && m_stats.health <= 0.0f)
	{
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (this->GetEquipped() == 1)
		{
			CActor *pActor = GetOwnerActor();
			if (!pActor)
				return;

			if (nwAction)
				nwAction->BreakHitOnActorEquippedItem(pActor->GetEntityId(), this->GetEntityId());
		}
	}
}

//------------------------------------------------------------------------
void CItem::UpdateDamageLevel()
{
	if (m_properties.hitpoints<=0 || m_sharedparams->damageLevels.empty())
		return;

	int slot=(m_stats.viewmode&eIVM_FirstPerson)?eIGS_FirstPerson:eIGS_ThirdPerson;

	int n=(int)m_sharedparams->damageLevels.size();
	int health=(int)((100.0f*MAX(0.0f, m_stats.health))/m_properties.hitpoints);
	for (int i=0; i<n; ++i)
	{
		const SDamageLevel &level = m_sharedparams->damageLevels[i];
		if (level.min_health == 0 && level.max_health == 0)
			continue;

		if (level.min_health <= health && health < level.max_health)
		{
			if (m_damageLevelEffects[i] == -1)
				m_damageLevelEffects[i] = AttachEffect(slot, false, level.effect.c_str(), level.helper.c_str(), 
																									Vec3Constants<float>::fVec3_Zero, Vec3Constants<float>::fVec3_OneZ, 
																									level.scale, true);
		}
		else if (m_damageLevelEffects[i] != -1)
		{
			DetachEffect(m_damageLevelEffects[i]);
			m_damageLevelEffects[i] = -1;
		}
	}
}

//------------------------------------------------------------------------
void CItem::OnDestroyed()
{ 
  /* MR, 2007-02-09: shouldn't be needed 
	for (int i=0; i<eIGS_Last; i++)
	{
		ICharacterInstance *pCharacter = GetEntity()->GetCharacter(i);
		if (pCharacter)
			pCharacter->SetAnimationSpeed(0);
	}*/
	CryLogAlways("CItem::OnDestroyed");
	if (this->GetEquipped() == 1)
	{
		if (GetOwnerActor())
		{
			/*if (this->GetShield() == 1)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					nwAction->StopMeleeBlockAction(GetOwnerActor()->GetEntityId(), GetEntityId());
				}
			}*/
			if (GetOwnerActor()->m_blockactiveshield && this->GetShield() == 1)
			{
				CryLogAlways("CItem::OnDestroyed Shield is true item Equipped");
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					nwAction->StopMeleeBlockAction(GetOwnerActor()->GetEntityId(), GetOwnerActor()->GetCurrentItemId());
				}
				GetOwnerActor()->m_blockactiveshield = false;
				GetOwnerActor()->SetBlockActive(1, false);
			}
			else if (GetOwnerActor()->m_blockactivenoshield && this->IsSelected())
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					nwAction->StopMeleeBlockAction(GetOwnerActor()->GetEntityId(), GetEntityId());
				}
				GetOwnerActor()->m_blockactivenoshield = false;
				GetOwnerActor()->SetBlockActive(2, false);
			}
			else if (this->IsSelected() && this->GetWeaponType() == 9)
			{
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					nwAction->ForcedDisableBow(GetEntityId());
				}
			}
			CryLogAlways("CItem::OnDestroyed deequip request");
			GetOwnerActor()->EquipItem(this->GetEntityId());

			if (this->IsSelected())
			{
				GetOwnerActor()->DeSelectItem(this->GetEntityId(), true);
			}
		}
	}
	/*else
	{
		if (GetOwnerActor())
		{
			if (GetOwnerActor()->m_blockactiveshield && this->GetShield() == 1)
			{
				if (GetEntityId() == GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Shield))
				{
					CryLogAlways("CItem::OnDestroyed Shield is true item Equipped but item Equipped state not setup");
					CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
					if (nwAction)
					{
						nwAction->StopMeleeBlockAction(GetOwnerActor()->GetEntityId(), GetOwnerActor()->GetCurrentItemId());
					}
					GetOwnerActor()->m_blockactiveshield = false;
				}
			}
			else if (GetOwnerActor()->m_blockactivenoshield && this->IsSelected())
			{
				if (GetEntityId() == GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Weapon))
				{
					CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
					if (nwAction)
					{
						nwAction->StopMeleeBlockAction(GetOwnerActor()->GetEntityId(), GetEntityId());
					}
					GetOwnerActor()->m_blockactivenoshield = false;
				}
			}
			else if (this->IsSelected() && this->GetWeaponType() == 9)
			{
				if (GetEntityId() == GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Weapon))
				{
					CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
					if (nwAction)
					{
						nwAction->ForcedDisableBow(GetEntityId());
					}
				}
			}

			if (this->IsSelected())
			{
				GetOwnerActor()->DeSelectItem(this->GetEntityId(), true);
			}
			else
			{
				GetOwnerActor()->EquipItem(this->GetEntityId());
			}
		}
	}*/
	CryLogAlways("CItem::OnDestroyed DestroyedGeometry(true)");
	DestroyedGeometry(true);
	CryLogAlways("CItem::OnDestroyed play destr. action");
	if(!gEnv->pSystem->IsSerializingFile()) //don't replay destroy animations/effects
		PlayAction(GetFragmentIds().destroy);
	CryLogAlways("CItem::OnDestroyed disable item update");
	//EnableUpdate(false);
}

//------------------------------------------------------------------------
void CItem::OnRepaired()
{
	for (int i=0; i<eIGS_Last; i++)
	{
		ICharacterInstance *pCharacter = GetEntity()->GetCharacter(i);
		if (pCharacter)
			pCharacter->SetPlaybackScale(1.0f);
	}

	DestroyedGeometry(false);

	EnableUpdate(true);
}

//------------------------------------------------------------------------
void CItem::OnDropped(EntityId actorId, bool ownerWasAI)
{
	CActor *pActor = GetActor(actorId);
	if (pActor)
	{
		pActor->OnItemUPD(2);
	}
	UnRegisterAsUser();

	m_pItemSystem->RegisterForCollection(GetEntityId());
}

//------------------------------------------------------------------------
void CItem::OnPickedUp(EntityId actorId, bool destroyed)
{
	if(GetISystem()->IsSerializingFile() == 1)
		return;

	CActor *pActor=GetActor(actorId);
	if (!pActor)
		return;

	pActor->OnItemUPD(1);

	RegisterAsUser();

	if (!IsServer())
		return;

	// Corpses have weapons with 'no save' flag set, restore it after pick up
	GetEntity()->SetFlags( GetEntity()->GetFlags() & ~ENTITY_FLAG_NO_SAVE );

	//Once picked up, remove it from bound layer 
	//Prevents hide/unload with the level layer, when the player goes to a different part of the level
	bool removeEntityFromLayer = pActor->IsPlayer();
	if (removeEntityFromLayer)
	{
		gEnv->pEntitySystem->RemoveEntityFromLayers(GetEntityId());
	}

	m_pItemSystem->UnregisterForCollection(GetEntityId());
}

//------------------------------------------------------------------------
void CItem::OnBeginCutScene()
{
}

//------------------------------------------------------------------------
void CItem::OnEndCutScene()
{
}

//------------------------------------------------------------------------
void CItem::OnOwnerActivated()
{
	RegisterAsUser();
}

//------------------------------------------------------------------------
void CItem::OnOwnerDeactivated()
{
	UnRegisterAsUser();
}

void CItem::OnOwnerStanceChanged( const EStance stance )
{

}
