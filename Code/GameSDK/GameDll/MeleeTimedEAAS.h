/*************************************************************************
Crytek Source File.
Copyright (C), Crytek Studios, 2001-2006.
-------------------------------------------------------------------------
$Id$
$DateTime$
Description: Beam Fire Mode Implementation

-------------------------------------------------------------------------
History:
- 23:3:2006   13:02 : Created by M�rcio Martins

*************************************************************************/

#pragma once

#ifndef __MELEETIMEDEAAS_H__
#define __MELEETIMEDEAAS_H__

#include "IGameRulesSystem.h"
#include "GameParameters.h"
#include "MeleeCollisionHelper.h"
#include "FireModeParams.h"
#include "ICryMannequin.h"
#include "GameTypeInfo.h"

#include <CrySystem/ISystem.h>
#include <CrySystem/ICmdLine.h>
#include <CryGame/IGameFramework.h>
#include "MovementAction.h"
#include "Player.h"

class CWeapon;
class CActor;

class CMeleeTimedEAAS :
	public IMeleeCollisionHelperListener
{
	struct StartAttackingAction;
	struct StopAttackingAction;
	struct DelayedImpulse;

public:
	class CMeleeTimedEAASAction : public TAction<SAnimationContext>
	{
	public:
		DEFINE_ACTION("MeleeAction");

		typedef TAction<SAnimationContext> BaseClass;

		CMeleeTimedEAASAction(int priority, CWeapon* pWeapon, FragmentID fragmentID);

		virtual void Exit();
		void OnHitResult(CActor* pOwnerActor, bool hit);
		void NetEarlyExit();
		void StopAttackAction();

	private:
		EntityId m_weaponId;
		bool m_FinalStage;
		bool m_bCancelled;
	};

public:
	CRY_DECLARE_GTI_BASE(CMeleeTimedEAAS);

	CMeleeTimedEAAS();
	virtual ~CMeleeTimedEAAS();

	void Release();

	void InitMeleeMode(CWeapon* pWeapon, const SMeleeTimedModeParams* pParams);
	void InitFragmentData();

	void Update(float frameTime, uint32 frameId);
	void GetMemoryUsage(ICrySizer * s) const;

	void Activate(bool activate);

	bool CanAttack() const;
	void StartAttack();
	bool IsAttacking() const { return m_attacking || m_netAttacking; };

	void NetAttack();
	//----------------------
	void DoAttack();
	//----------------------
	bool GetRecoilSA();
	int GetDamage() const;
	float GetRange()  const;
	ILINE void SetDelayTimer(float timer) { m_delayTimer = timer; }

	//IMeleeCollisionHelperListener
	virtual void OnSuccesfulHit(const ray_hit& hitResult);
	virtual void OnFailedHit();

	virtual bool PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote, const Vec3 &orig = Vec3(ZERO));
	virtual bool PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual bool PerformRayTest3(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	void StartRay();
	void StartRay2(bool dc);
	void StopAttack();
	void PreStopAttack();
	void AdditiveRayTestStart();
	int GetAttackId();
	bool IsHoldCStarted();

	Vec3 GetAttackHelperPos(int helper_Id);
	Vec3 FindNearestPointInIntersectionLine(Vec3 nrPnt, int acc = 12);
	//~IMeleeCollisionHelperListener

	void CloseRangeAttack(bool closeRangeAttack);

	EntityId GetNearestTarget();
	ILINE static const char* GetWeaponComponentType() { return "MeleeTimedEAAS"; }
	void OnMeleeHitAnimationEvent();

	ILINE const SMeleeTimedParams& GetMeleeParams() const { return m_pMeleeParams->meleeparams; }
	ILINE const SMeleeTimedModeParams* GetMeleeModeParams() const { return m_pMeleeParams; }

	static void PlayHitMaterialEffect(const Vec3 &position, const Vec3 &normal, string hitFX, int surfaceIdx);
	ILINE static void SetMeleeDelay(float fDelay) { s_fNextAttack = fDelay; }
	//---------------------------------------------------------------------------------------------------------------
	void AttachDecalToWpn(const Vec3 &position, const Vec3 &normal, bool bBoostedMelee, int surfaceIdx, int blood_type = 0);
	bool CheckCollisionWD(Vec3 position, int surfaceIdx);
	void ForcedStopCurrentAttack();
	void ForcedResetAnyAttackState();
	//---------------------------------------------------------------------------------------------------------------

	float GetDuration() const;
	float GetDelay() const;

	ILINE bool IsMeleeWeapon() const { return m_pMeleeParams->meleeparams.is_melee_weapon; }

	float GetImpulseStrength();

#ifdef SERVER_CHECKS
	virtual float GetDamageAmountAtXMeters(float x);
#endif

private:

	float GetImpulseAiToPlayer() const;
	void RequestAlignmentToNearestTarget();
	void PerformMelee(const Vec3 &pos, const Vec3 &dir, bool remote);
	bool PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, bool remote);
	int Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, bool remote);
	void Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, int hitTypeID, int iPrim);

	void ApplyMeleeDamageHit(const SCollisionTestParams& collisionParams, const ray_hit& hitResult);
	void ApplyMeleeDamage(const Vec3& point, const Vec3& dir, const Vec3& normal, IPhysicalEntity* physicalEntity,
		EntityId entityID, int partId, int ipart, int surfaceIdx, bool remote, int iPrim);

	void ApplyMeleeEffects(bool hit);
	bool IsFriendlyHit(IEntity* pShooter, IEntity* pTarget);

	bool DoSlideMeleeAttack(CActor* pOwnerActor);
	float GetMeleeDamage() const;

	const ItemString &SelectMeleeAction() const;
	void GenerateAndQueueMeleeAction() const;
	void GenerateAndQueueMeleeActionForStatus(const SMeleeTimedTags::TTagParamsContainer& tagContainer) const;
	SMeleeTimedTags::SMeleeFragData GenerateFragmentData(const SMeleeTimedTags::TTagParamsContainer& tagContainer) const;

	bool IsMeleeFilteredOnEntity(const IEntity& targetEntity) const;

	bool SwitchToMeleeWeaponAndAttack();

	bool StartMultiAnimMeleeAttack(CActor* pActor);

	//------------------------------------------
	float GetOwnerStrength() const;
	bool GetWpnCrtAtk();
	//------------------------------------------

protected:

	enum EHitStatus
	{
		EHitStatus_Invalid,
		EHitStatus_ReceivedAnimEvent,
		EHitStatus_HaveHitResult
	};

	CWeapon* m_pWeapon;
	const SMeleeTimedModeParams* m_pMeleeParams;

	float		m_useMeleeWeaponDelay;
	float		m_delayTimer;
	float		m_attackTurnAmount; //Camera lock/turn progression value [0-1]
	float		m_attackTurnAmountSmoothRate;
	float		m_attackTime;	//How long the melee attack has been active for
	mutable int	m_hitTypeID;
	bool		m_attacked;
	bool		m_netAttacking;
	bool		m_attacking;
	bool		m_slideKick;
	bool		m_shortRangeAttack;

	//--------------------------------------------------
	float	m_meleeScale;
	float	m_hold_time_to_h;
	bool	m_hold_started_charge;
	int		num_mtl_eff_spwn;
	float	m_hold_timer;
	bool	m_atkn;
	bool	m_pulling;
	bool	m_atking;
	float	m_atk_time;
	bool	m_holdedf;
	bool	m_holdedr;
	bool	m_holdedl;
	bool	m_holdedb;
	bool    m_forceNextAtk;
	float	m_crt_hit_dmg_mult;
	int     num_hitedEntites;
	float	m_recoil_ca;
	float	m_updtimer;
	float	m_updtimer2;
	float	m_updtimer3 = 0.0f;
	float	m_durationTimer;
	std::vector<EntityId> m_hitedEntites;
	bool		m_gtcb;

	Vec3 pos_initial;
	Vec3 pos_start_old;
	Vec3 pos_end_old;
	Vec3 pos_start_nv;
	Vec3 pos_end_nv;
	Vec3 normalized_hit_direction;
	//--------------------------------------------------

	EHitStatus m_hitStatus;
	IActionController* m_piActionController;

	static float s_fNextAttack;

	static EntityId s_meleeSnapTargetId;
	static bool s_bMeleeSnapTargetCrouched;

	CMeleeCollisionHelper m_collisionHelper;

	ray_hit m_lastRayHit;
	SCollisionTestParams m_lastCollisionTest;

	CMeleeTimedEAASAction* m_pMeleeAction;
};

#endif //__MELEE_H__
