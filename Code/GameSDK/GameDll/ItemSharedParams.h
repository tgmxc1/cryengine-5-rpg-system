// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
-------------------------------------------------------------------------
$Id$
$DateTime$
Description: Stores all item parameters that don't mutate in instances...
						 Allows for some nice memory savings...

-------------------------------------------------------------------------
History:
- 3:4:2007   10:54 : Created by M�rcio Martins

*************************************************************************/

#pragma once

#ifndef __ITEMSHAREDPARAMS_H__
#define __ITEMSHAREDPARAMS_H__

#include "GameParameters.h"
#include "Item.h"
#include "ItemDefinitions.h"
#include "Audio/GameAudio.h"
#include <IForceFeedbackSystem.h>
#include <CryAnimation/ICryAnimation.h>
#include "WeaponStats.h"
#include "ItemParamsRegistration.h"

#define MAX_BOOK_PAGES_FT 300

class CItemResourceCache;
class CItemGeometryCache;
class CItemMaterialAndTextureCache;

struct SAimAnimsBlock
{
	ItemString anim[WeaponAimAnim::Total];
	
	void Reset();
	void Read( const XmlNodeRef& rootNode );
	void GetMemoryUsage( ICrySizer *s ) const
	{
		for (int i = 0; i < WeaponAimAnim::Total; ++i)
		{
			s->AddObject(anim[i]);
		}
	}	
};


struct SAimLookParameters
{
	SAimLookParameters();

	void Reset();
	void Read(const XmlNodeRef& rootNode);

#define AIMLOOK_PARAMS_MEMBERS(f) \
	f(float,	easeFactorInc) \
	f(float,	easeFactorDec) \
	f(float,	strafeScopeFactor) \
	f(float,	rotateScopeFactor) \
	f(float,	velocityInterpolationMultiplier) \
	f(float,	velocityLowPassFilter) \
	f(float,	accelerationSmoothing) \
	f(float,	accelerationFrontAugmentation) \
	f(float,	verticalVelocityScale) \
	f(bool,		sprintCameraAnimation) \

#define AIMLOOK_ARRAY_MEMBERS(f) \
	f(float, WeaponAimAnim::Total, blendFactors)

	REGISTER_STRUCT_WITH_ARRAYS(AIMLOOK_PARAMS_MEMBERS, AIMLOOK_ARRAY_MEMBERS, SAimLookParameters)
};


struct SParams
{
	SParams()
	{
		Reset();
	};

	void Reset()
	{
		selectable = true;
		droppable = true;
		pickable = true;
		mountable = true;
		usable = true;
		giveable = true;
		unique = true;
		mass = 3.5f;
		drop_impulse = 12.5f;
		select_override = 0.0f;
		auto_droppable = false;
		auto_pickable = true;
		has_first_select = false;
		select_delayed_grab_3P = false;
		attach_to_back = false;
		scopeAttachment = 0;
		attachment_gives_ammo = false;
		can_ledge_grab = true;
		can_rip_off = true;
		check_clip_size_after_drop = true;
		check_bonus_ammo_after_drop = true;
		remove_on_drop = true;
		usable_under_water = false;
		can_overcharge = false;
		fp_offset.Set(0.0f, 0.0f, 0.0f);
		fp_rot_offset.SetIdentity();
		hasAimAnims = false;
		ironsightAimAnimFactor = 1.f;
		fast_select = false;
		sprintToFireDelay = 0.0f;
		sprintToZoomDelay = 0.35f;
		sprintToMeleeDelay = 0.05f;
		runToSprintBlendTime = 0.2f;
		sprintToRunBlendTime = 0.2f;
		autoReloadDelay = 0.0f;
		zoomTimeMultiplier = 1.f;
		selectTimeMultiplier = 1.f;
		//*********************************************************************
		//������������� �������� ����� ����������
		tab_id = 0;
		armor_type = 0;
		armor_class = 0;
		quest_item = 0;
		item_impulse_in_combat_common = 11;
		gender_restruction = 0;
		other_uw_attack = 0;
		melee_wpn_notallowhelpers = 0;
		shield = 0;
		allow_underwater_attacks = 1;
		critical_hit_dmg_mod = 1.0f;
		critical_hit_base_chance = 0;
		equipped_i = 0;
		weapon_type = 0;
		for (int i = 0; i < 15; i++)
		{
			additional_effect_id[i] = -1;
			item_particle_attach_on_select[i] = false;
		}
		item_type = 0;
		num_armor_parts = 0;
		num_armor_parts_male = 0;
		potion_type = 0;
		magick_dmg_multipler = 1.0f;
		hprestoration = 0.0f;
		mprestoration = 0.0f;
		snrestoration = 0.0f;
		repair_force = 0;
		can_repair_items = 0;
		item_hp = 100.0f;
		item_mhp = 100.0f;
		dmg_current = 0.0f;
		dmg_throwing = 0.0f;
		dmg_added = 0.0f;
		armor_rating = 0.0f;
		book_num_pages = 0;
		item_quanity = 0;
		item_price = 0;
		combine_able = 0;
		item_get_hp_info_from_lua = false;
		two_handed_wpn = false;
		item_stackable = false;
		item_max_stack_size = 1;
		item_aim_alt_limits_horiz = 180.0f;
		item_aim_alt_limits_vert = 180.0f;
		item_view_limits_vert = 0.0f;
		item_view_limits_up = 0.0f;
		item_view_limits_down = 0.0f;
		use_trdps_item_aux_slot_as_phyproxy = false;
		physicalize_fp_slot_in_all_time = 0;
		physicalize_tp_slot_in_all_time = 0;
		//*******************************************************************

		tag = "";
		attachment[IItem::eIH_Right] = g_pItemStrings->right_item_attachment;
		attachment[IItem::eIH_Left] = g_pItemStrings->left_item_attachment;
		aiAttachment[IItem::eIH_Right] = g_pItemStrings->right_item_attachment;
		aiAttachment[IItem::eIH_Left] = g_pItemStrings->left_item_attachment;
		bone_attachment_01.clear();
		bone_attachment_02.clear();

		const int numStats = eWeaponStat_NumStats;
		for (int i = 0; i < numStats; i++)
		{
			weaponStats.stats[i] = 5;
		}

		aimAnims.Reset();
		mountedAimAnims.Reset();
		aimLookParams.Reset();

		for (int i = 0; i < MountedTPAimAnim::Total; ++i)
		{
			mountedTPAimAnims[i].clear();;
		}

		crosshairTexture = "";

		//������� ����� �������� ����� ��������������
		for (int i = 0; i < 15; i++)
		{
			armorpath[i].clear();
			armorpath_male[i].clear();
			itemdec[i].clear();
			item_particle_effect[i].clear();
			item_particle_effect_helper[i].clear();
			underwater_atk_anim[i].clear();
			basic_ammo_pickup[i].clear();
			arrow_quiver_path[i].clear();
		}
		for (int i = 0; i < 30; i++)
		{
			book_text[i].clear();
		}

		ammo_type.clear();
		base_item_to_stacking.clear();
		weapon_throw_action_def.clear();
		function_call_on_equip.clear();
		function_call_on_de_equip.clear();
		path_to_crosshair_texture.clear();
		idle_co_animation.clear();
		melee_fx_name.clear();
		icon.clear();
		//*******************************************
	}

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(tag);
		s->AddObject(display_name);
		s->AddObject(bone_attachment_01);
		s->AddObject(bone_attachment_02);

		//*******************************************************
		//���������� � ���������� ������ ���������� ����� ��������
		s->AddObject(icon);

		for (int i = 0; i < IItem::eIH_Last; i++)
		{
			s->AddObject(attachment[i]);
		}

		for (int i = 0; i < 15; i++)
		{
			s->AddObject(armorpath[i]);
			s->AddObject(armorpath_male[i]);
			s->AddObject(itemdec[i]);
			s->AddObject(item_particle_effect[i]);
			s->AddObject(item_particle_effect_helper[i]);
			s->AddObject(underwater_atk_anim[i]);
			s->AddObject(basic_ammo_pickup[i]);
			s->AddObject(arrow_quiver_path[i]);
		}
		for (int i = 0; i < 30; i++)
		{
			s->AddObject(book_text[i]);
		}

		s->AddObject(ammo_type);
		s->AddObject(base_item_to_stacking);
		s->AddObject(weapon_throw_action_def);
		s->AddObject(function_call_on_equip);
		s->AddObject(function_call_on_de_equip);
		s->AddObject(path_to_crosshair_texture);
		s->AddObject(idle_co_animation);
		s->AddObject(melee_fx_name);
		//******************************************************

		s->AddObject(aimAnims);
		s->AddObject(mountedAimAnims);
		
		for (int i = 0; i < MountedTPAimAnim::Total; i++)
		{
			s->AddObject(mountedTPAimAnims[i]);
		}
	}
	
	bool	selectable;
	bool	droppable;
	bool	pickable;
	bool	mountable;
	bool	usable;
	bool	giveable;
	bool	unique;
	bool	auto_droppable;
	bool	auto_pickable;
	bool	attachment_gives_ammo;
	bool	has_first_select;
	bool	fast_select;
	bool	select_delayed_grab_3P;
	bool	can_ledge_grab;
	bool	can_rip_off;
	bool	usable_under_water;
	bool	can_overcharge;
	bool  check_clip_size_after_drop;
	bool  check_bonus_ammo_after_drop;
	bool	remove_on_drop;
	float	sprintToFireDelay;
	float	sprintToZoomDelay;
	float	sprintToMeleeDelay;
	float	autoReloadDelay;
	float	runToSprintBlendTime;
	float	sprintToRunBlendTime;

	int   scopeAttachment;

	SWeaponStatsData weaponStats;

	float	mass;
	float	drop_impulse;
	float	select_override;
	float zoomTimeMultiplier;
	float selectTimeMultiplier;

	Vec3	fp_offset;
	Quat	fp_rot_offset;

	ItemString	tag;
	ItemString  itemClass;
	ItemString	aiAttachment[IItem::eIH_Last];
	ItemString	attachment[IItem::eIH_Last];
	ItemString	display_name;

	ItemString	adbFile;
	ItemString	soundAdbFile;
	ItemString  actionControllerFile;

	ItemString  bone_attachment_01;
	ItemString  bone_attachment_02;
	bool				attach_to_back;

	bool				hasAimAnims;
	float				ironsightAimAnimFactor;
	SAimAnimsBlock aimAnims;
	SAimAnimsBlock mountedAimAnims;
	SAimLookParameters aimLookParams;
	ItemString	mountedTPAimAnims[MountedTPAimAnim::Total];

	ItemString crosshairTexture;

	//����� ��������� ���������
	ItemString	icon;
	ItemString	armorpath[15];
	ItemString	armorpath_male[15];
	ItemString	itemdec[15];
	ItemString	item_particle_effect[15];
	ItemString	item_particle_effect_helper[15];
	bool	item_particle_attach_on_select[15];
	ItemString	book_text[MAX_BOOK_PAGES_FT];//for books only

	ItemString	underwater_atk_anim[15];

	ItemString	ammo_type;

	ItemString	basic_ammo_pickup[15];

	ItemString	arrow_quiver_path[15];

	ItemString	base_item_to_stacking;

	ItemString	weapon_throw_action_def;

	ItemString	function_call_on_equip;
	ItemString	function_call_on_de_equip;
	ItemString	path_to_crosshair_texture;
	ItemString	idle_co_animation;

	ItemString	melee_fx_name;

	short		tab_id;// 0 = default items(not show in inventory), 1 = weapons, 2 = armor, 3 = useful items, 4 = misc items, 5 = quest items
	int		armor_type;// 0 = not armor, 1 = boots, 2 = legs, 3 = chest, 4 = arms, 5 = head, 6 = acc..
	int		armor_class;// 0 = no class(cloth), 1 = light armor, 2 = medium armor, 3 = heavy armor
	short		quest_item;
	short		shield;
	int		item_impulse_in_combat_common;

	int		can_repair_items;
	int		repair_force;

	short		gender_restruction;
	short		other_uw_attack;
	short		melee_wpn_notallowhelpers;
	short		combine_able;

	int		weapon_type;// 0 = not weapon, 1 = one handed swords, 2 = one handed axes, 3 = one handed blunt, 4 = two handed swords, 5 = two handed axes, 6 = two handed blunt, 7 = polearm 1H, 8 = polearm 2H
	// 9 = bow, 10 = staff, 11 = crossbow, 12 = throw weapon, 13 = dagger, 14 = wrists
	int		additional_effect_id[15];
	int		item_type;
	int		num_armor_parts; // 0 - 7 not more!
	int		num_armor_parts_male; // 0 - 7 not more!
	float		magick_dmg_multipler;//for mage staff only
	float		hprestoration;
	float		mprestoration;
	float		snrestoration;
	float		armor_rating;
	float		dmg_current;
	float		dmg_throwing;
	float		dmg_added;
	int		potion_type;
	int		equipped_i;
	int		item_quanity;// number of useable items in inventory slot
	int		item_price;
	int		book_num_pages;//for book

	float		item_hp;
	float		item_mhp;

	short		allow_underwater_attacks;
	float		critical_hit_dmg_mod;
	int			critical_hit_base_chance;

	bool    item_get_hp_info_from_lua;
	bool    two_handed_wpn;
	bool    item_stackable;
	int    item_max_stack_size;
	float		item_aim_alt_limits_horiz;
	float		item_aim_alt_limits_vert;
	float		item_view_limits_vert;
	float		item_view_limits_up;
	float		item_view_limits_down;
	bool    use_trdps_item_aux_slot_as_phyproxy;
	int    physicalize_fp_slot_in_all_time;
	int    physicalize_tp_slot_in_all_time;
};


struct SMountParams
{
	SMountParams()
	{
		Reset();
	};

	void Reset()
	{
		body_distance = 0.55f;
		ground_distance = 0.0f;
		fpBody_offset.Set(0.0f,-1.0f,-0.1f);
		fpBody_offset_ironsight.Set(0.0f,-1.0f,-0.1f);

		pivot.clear();
		left_hand_helper.clear();
		right_hand_helper.clear();
		rotate_sound_fp.clear();
		rotate_sound_tp.clear();
	}

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(pivot);
		s->AddObject(left_hand_helper);
		s->AddObject(right_hand_helper);
	}

	float		body_distance;
	float		ground_distance;
	Vec3		fpBody_offset;
	Vec3		fpBody_offset_ironsight;
	ItemString	pivot;
	ItemString	left_hand_helper;
	ItemString	right_hand_helper;
	ItemString  rotate_sound_fp;
	ItemString  rotate_sound_tp;
};

struct SAnimation
{
	enum EFlags
	{
		ANIMATE_CAMERA						= BIT(0),
		USE_UNMODIFIED_BASEPOSE		= BIT(1),
		IDLEPOSE_ACTION						= BIT(2),
		CONSTANT_INTERPOLATION		= BIT(4),
		FULL_BODY									= BIT(5),
	};

	SAnimation()
		: flags(0)
		, layer(0)
		, speed(0.0f)
		, blend(0.0f)
		, blendBackToModifiedBasePoseTime(0.0f)
	{};

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(name);
	}

	ItemString	name;
	float				speed;
	float				blend;
	float				blendBackToModifiedBasePoseTime;
	uint8				layer;
	uint8				flags;
};

struct SForceFeedback
{
	SForceFeedback()
		: combatModeMultiplier(1), delay(0), fxId(InvalidForceFeedbackFxId) {}

	float combatModeMultiplier;
	float delay;
	ForceFeedbackFxId fxId;
};

struct SCameraShake
{
	SCameraShake()
		: time(0)
		, shift(0, 0, 0)
		, rotate(0, 0, 0) {}

	Vec3		shift;
	Vec3		rotate;
	float		time;
};

struct SAudio
{
	SAudio():	isstatic(false), sphere(0.0f), airadius(0.0f),issynched(false) {};

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(name);
	}

	ItemString		name;
	float			    airadius;
	float			    sphere;
	bool			    isstatic;
	bool          issynched;
};

struct SDamageLevel
{
	SDamageLevel()
		: max_health(100)
		, min_health(0)
		, scale(1.0f)
	{};

	int max_health;
	int min_health;
	float scale;
	ItemString effect;
	ItemString helper;

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(effect);
		s->AddObject(helper);
	}
};

struct SLaserParams
{   
	SLaserParams() 
	{ 
		laser_geometry_tp.clear();
		laser_dot[0].clear(); laser_dot[1].clear();
		laser_range[0] = laser_range[1] = 50.0f;
		laser_thickness[0] = laser_thickness[1] = 1.f;
		show_dot = true;
	}

	void CacheResources(CItemGeometryCache& geometryCache) const;

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(laser_dot[0]); 
		s->AddObject(laser_dot[1]);
		s->AddObject(laser_geometry_tp);
	}

	string		laser_geometry_tp;
	string		laser_dot[2];
	float			laser_range[2];
	float			laser_thickness[2];
	bool			show_dot;
};

struct SFlashLightParams
{   
	SFlashLightParams();

	void CacheResources(CItemMaterialAndTextureCache& textureCache) const;

	ItemString lightCookie;
	Vec3 color;
	float diffuseMult;
	float specularMult;
	float HDRDynamic;
	float distance;
	float fov;
	int		style;
	float	animSpeed;

	Vec3 fogVolumeColor;
	float fogVolumeRadius;
	float fogVolumeSize;
	float fogVolumeDensity;
};

enum EItemCategoryType
{
	eICT_None						= 0,
	eICT_Primary				= BIT(0),
	eICT_Secondary			= BIT(1),
	eICT_Heavy					= BIT(2),
	eICT_Explosive			= BIT(3),
	eICT_Grenade				= BIT(4),
	eICT_Medium					= BIT(5),
	eICT_Special				= BIT(6),
	eDF_All							= 0xFFFF
};

int GetItemCategoryType( const char* category );
int GetItemCategoryTypeByClass( IEntityClass* pClass );

struct SAccessoryParams
{
	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(attach_helper);
		s->AddObject(switchToFireMode);
		s->AddObject(secondaryFireMode);
		s->AddObject(zoommode);
		s->AddObject(zoommodeSecondary);
		s->AddObject(select);
		s->AddObject(select_empty);
		s->AddObject(category);
		
		s->AddContainer(firemodes);		
		s->AddContainer(disableFiremodes);		
	}

	IEntityClass* pAccessoryClass;

	ItemString		attach_helper;
	ItemString		show_helper;
	ItemString		switchToFireMode;
	ItemString		secondaryFireMode;
	ItemString		zoommode;
	ItemString		zoommodeSecondary;
	ItemString		select;
	ItemString		select_empty;
	ItemString		category;
	std::vector<ItemString>	firemodes;
	std::vector<ItemString>	disableFiremodes; 
	XmlNodeRef		tempAccessoryParams;

	SWeaponStatsData weaponStats;

	float reloadSpeedMultiplier;

	bool exclusive;
	bool selectable;		//Whether the attachment appears on the infiction weapon customization menu for selection (Currently only used in MP)
	bool defaultAccessory;
	bool client_only;
	bool attachToOwner;
	bool enableBaseModifier;
	bool alsoAttachDefault;
	bool extendsMagazine;
	bool beginsDetached; 
};

struct SGeometryDef
{
	SGeometryDef()
		: modelPath("")
		, pos(Vec3(ZERO))
		, angles(Ang3(ZERO))
		, scale(1.0f)
		, slot(eIGS_Last)
		, useCgfStreaming(false)
		, useParentMaterial(false)
	{};

	ItemString modelPath;
	ItemString material;
	Vec3 pos;
	Ang3 angles;
	float scale;
	int slot;
	bool useCgfStreaming;
	bool useParentMaterial;

	void GetMemoryUsage(ICrySizer * s) const
	{
		s->AddObject(modelPath);
		s->AddObject(material);
	}
};


struct SAttachmentHelper
{
	ItemString	name;
	ItemString	bone;
	int			slot;
	void GetMemoryUsage(ICrySizer * s) const
	{		
		s->AddObject(name);
		s->AddObject(bone);
	}
};

struct SCachedItemAnimation
{

	enum ECacheVariableFlags
	{
		eCVF_Hand		 = BIT(0),
		eCVF_Suffix	 = BIT(1)
	};

	SCachedItemAnimation( const uint32 hash, const int8 hand, const uint32 suffixHash, const uint32 modelHash)
		: m_stringHash(hash)
		, m_hand(hand)
		, m_suffixHash(suffixHash)
		, m_animationId(-1)
		, m_modelHash(modelHash)
		, m_variableFlags(0)
	{}

	inline uint32 GetChangeFlags(const SCachedItemAnimation &n) const
	{
		uint32 ret = 0;
		ret |= ((n.m_stringHash!=m_stringHash) << eCF_String);
		ret |= ((n.m_hand!=m_hand) << eCF_Hand);
		ret |= ((n.m_suffixHash!=m_suffixHash) << eCF_Suffix);

		return ret;
	}

	bool operator==( const SCachedItemAnimation &n ) const
	{
		return	(n.m_stringHash == m_stringHash) && 
				(n.m_modelHash == m_modelHash) &&
						((n.m_hand == m_hand) || IsVariableFlagNotSet(eCVF_Hand)) && 
						((n.m_suffixHash == m_suffixHash) || IsVariableFlagNotSet(eCVF_Suffix));
	}

	ILINE void SetAnimationId(int animationId) { m_animationId = animationId; }
	ILINE int GetAnimationId() const { return m_animationId; }
	void GetMemoryUsage( ICrySizer *pSizer ) const{}
	ILINE void SetVariableFlag(ECacheVariableFlags flag) { m_variableFlags |= flag; }
	ILINE bool IsVariableFlagNotSet(ECacheVariableFlags flag) const { return ((m_variableFlags&flag) == 0); } 

private:

	uint32				m_suffixHash;
	uint32				m_stringHash;
	uint32				m_modelHash;
	int					m_animationId;
	int8					m_hand;
	int8					m_variableFlags;
};

struct SAnimationPreCache
{
	ItemString DBAfile;
	bool			 thirdPerson;
};

class CItemSharedParams : public IGameSharedParameters
{
public:

	typedef std::vector<SDamageLevel>								TDamageLevelVector;
	typedef std::vector<SGeometryDef>								TGeometryDefVector;
	typedef std::vector<SAnimationPreCache>					TAnimationPrecacheVector;

	CItemSharedParams();
	virtual ~CItemSharedParams();

	//IGameSharedParameters
	virtual void GetMemoryUsage(ICrySizer *s) const;
	virtual const char* GetDataType() const { return "ItemParams"; }
	virtual void ReleaseLevelResources();
	//~IGameSharedParameters

	static const SLaserParams& GetDefaultLaserParameters();

	bool ReadItemParams(const XmlNodeRef& rootNode);
	void ReadOverrideItemParams(const XmlNodeRef& overrideParamsNode);

	void CacheResources(CItemResourceCache& itemResourceCache, const IEntityClass* pItemClass);
	void CacheResourcesForLevelStartMP(CItemResourceCache& itemResourceCache, const IEntityClass* pItemClass);

	const SGeometryDef* GetGeometryForSlot(eGeometrySlot geomSlot) const;
	void LoadGeometryForItem(CItem* pItem, eGeometrySlot skipSlot = eIGS_Last) const;

	TAccessoryParamsVector	accessoryparams;
	THelperVector						helpers;
	SParams									params;
	TDamageLevelVector			damageLevels;
	TDefaultAccessories			defaultAccessories;
	TInitialSetup						initialSetup;
	TAccessoryAmmoMap				bonusAccessoryAmmo;
	TAccessoryAmmoMap				accessoryAmmoCapacity;
	SLaserParams*						pLaserParams;
	SFlashLightParams*			pFlashLightParams;
	SMountParams*						pMountParams;
		
	ItemString	animationGroup;
	TAnimationPrecacheVector animationPrecache;

protected:
	bool ReadParams(const XmlNodeRef& paramsNode);
	bool ReadGeometry(const XmlNodeRef& paramsNode);
	bool ReadDamageLevels(const XmlNodeRef& paramsNode);
	bool ReadAccessories(const XmlNodeRef& paramsNode);
	bool ReadAccessoryParams(const XmlNodeRef& paramsNode, SAccessoryParams* params);
	bool ReadAccessoryAmmo(const XmlNodeRef& paramsNode);
	bool ReadLaserParams(const XmlNodeRef& paramsNode);
	bool ReadFlashLightParams(const XmlNodeRef& paramsNode);
	int	 TargetToSlot(const char* name);
	void PrefixPathIfFilename(const char* pPath, ItemString& filename);

	bool ItemClassUsesDefaultLaser(const IEntityClass* pItemClass) const;

	TGeometryDefVector			geometry;
	
};

#endif //__ITEMSHAREDPARAMS_H__
