#ifndef __SPELLPROJECTILE_H__
#define __SPELLPROJECTILE_H__

#if _MSC_VER > 1000
# pragma once
#endif

#include "Projectile.h"
#include "StickyProjectile.h"
#include "EntityUtility/EntityEffects.h"
//#include "LightningBolt.h"

class CSpellProjectile : public CProjectile
{
	//friend class CLightningBolt;
private:
	typedef CProjectile BaseClass;

public:
	CSpellProjectile();
	virtual ~CSpellProjectile();
	virtual void Update(SEntityUpdateContext &ctx, int updateSlot);
	virtual void HandleEvent(const SGameObjectEvent &event);
	virtual void Launch(const Vec3 &pos, const Vec3 &dir, const Vec3 &velocity, float speedScale);
	//virtual bool Init(IGameObject *pGameObject);
	virtual void Explode(const SExplodeDesc& explodeDesc);
	virtual void OnHit(const HitInfo& hit);
	virtual void ProcessHit(CGameRules& gameRules, const EventPhysCollision& collision, IEntity& target, float damage, int hitMatId, const Vec3& hitDir);
	virtual void GenerateArtificialCollision(IPhysicalEntity* pAttackerPhysEnt, IPhysicalEntity *pCollider, int colliderMatId, const Vec3 &position, const Vec3& normal, const Vec3 &vel, int partId, int surfaceIdx, int iPrim, const Vec3& impulse);
	virtual void SetSpellDamage(float dmg);
	virtual void TrailSpellEffect(bool enable, bool forced = false);
	virtual void RequestDelayedDestroy(bool destroy_immediatle);
	virtual void Destroy(bool forced = false);
	virtual void StopLightningChildEffect();
	virtual void SpawnMaterialEffectOnCollision(const EventPhysCollision* pCollision, IEntity* pHitTarget);
	virtual void ImpulseWave(float elapsed, Vec3 normal, float speed, float force, float amplitude, float decay, float rangeMin, float rangeMax, float duration);
	virtual void UpdateLightningEffect(float ftTime, bool explode = false, EntityId contact_ent = 0);
	virtual void UpdateProjectileDamageArea();
	virtual void UpdateProjectileDamageArea2();
	virtual void UpdateProjectileTargeting();
	string			fnc_param_on_hit[3];
	int			additive_effects_on_hit[15];
	float			additive_effects_times[15];
	float  damage;
	float  start_damage;
	float  spell_frc;
	float  wave_timer_val1;
	bool  trail_enable;
	bool  exploded;
	bool  is_lightning_child;
	EntityId  entity_to_ignore_hit;
	EntityId  m_targetId;
	int  enable_wave_phy_force;
	int  enable_lighting_bolt_params;
	int  enable_projectile_dmg_area;
	int  enable_projectile_targeting;
	Vec3  old_prj_pos;
	Vec3  old_prj_pos2;
	Vec3  old_prj_pos3;
	Vec3  m_destination;
	bool  m_isCruising;
	bool  m_isDescending;
	bool  m_have_target;
	std::vector<EntityId> m_entitiesLightningAffected;
protected:
	bool	  m_stuck_c;
	Vec3			m_launchLoc;
	float			m_safeExplosion;
	bool			m_skipWater; 
	CStickyProjectile	m_stickyProjectile;
	//CLightningBolt*	m_lightProjectile;
	EntityEffects::TAttachedEffectId	m_spell_trailEffectId;
	std::vector<EntityId> m_entitiesAffected;
	std::vector<EntityId> m_TargetEntities;
	float  time_to_destroy;
};


#endif 