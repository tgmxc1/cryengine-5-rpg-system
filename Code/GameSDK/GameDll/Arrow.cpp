#include "StdAfx.h"

#include "Arrow.h"
#include "Player.h"
#include "GameRules.h"
#include "Bullet.h"
#include "Game.h"
#include "Bow.h"
#include "CrossBow.h"

#include <CryEntitySystem/IEntitySystem.h>
#include <CryAction/IMaterialEffects.h>
#include <IGameObjectSystem.h>
#include <CryGame/GameUtils.h>
#include "GameCVars.h"
#include "UI/HUD/HUDCommon.h"


#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>


CArrow::CArrow():
m_stuck(false),
m_notStick(false),
m_can_do_damage(true),
m_nConstraints(0),
m_parentEntity(0),
m_stickyProjectile(true),
m_damageCap(65536.0f),
m_damageFallOffAmount(0.0f)
, m_damageFallOffStart(0.0f)
, m_damageFalloffMin(0.0f)
, m_pointBlankAmount(1.0f)
, m_pointBlankDistance(0.0f)
, m_pointBlankFalloffDistance(0.0f)
, m_Wpn_lcn_Pos(0.0f, 0.0f, 0.0f)
, m_velocity_mult(0.8f)
{
	m_localChildPos.Set(0.0f,0.0f,0.0f);
	m_localChildRot.SetIdentity();
}

//-------------------------------------------
CArrow::~CArrow()
{
	
}

//------------------------------------------
void CArrow::HandleEvent(const SGameObjectEvent &event)
{
	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);
	CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{
		EventPhysCollision *pCollision = (EventPhysCollision *)event.ptr;
		if (!pCollision)
			return;

		bool prsbl_stk = true;
		if (ISurfaceType* pSurf = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceTypeManager()->GetSurfaceType(pCollision->idmat[1]))
		{
			string surf_name = pSurf->GetName();
			string unpiercable_surf_names[6];
			unpiercable_surf_names[0] = "mat_rock";
			unpiercable_surf_names[1] = "mat_rock_dusty";
			unpiercable_surf_names[2] = "mat_metal";
			unpiercable_surf_names[3] = "mat_concrete";
			unpiercable_surf_names[4] = "mat_water";
			unpiercable_surf_names[5] = "mat_ladder";
			for (int i = 0; i < 6; i++)
			{
				if (surf_name == unpiercable_surf_names[i])
				{
					prsbl_stk = false;
					break;
				}
			}
		}

		if (this->GetEntity()/* && prsbl_stk*/)
		{
			//m_velocity_mult
			//pProjectileEntity->SetSlotFlags(0, pProjectileEntity->GetSlotFlags(0)&~ENTITY_SLOT_RENDER);
			//pProjectileEntity->SetSlotFlags(1, pProjectileEntity->GetSlotFlags(1)|ENTITY_SLOT_RENDER);
			//m_Wpn_lcn_Pos.GetDistance(pCollision->pt);
			float length_a_b = 0.0f;
			length_a_b = m_Wpn_lcn_Pos.GetDistance(pCollision->pt);
			float arrow_dst = -0.3f;
			if (m_velocity_mult > 3.0f)
				m_velocity_mult = 3.0f;

			float length_rel = length_a_b*0.05f;
			if (length_rel > 3.0f)
				length_rel = 3.0f;

			arrow_dst = ((arrow_dst + m_velocity_mult * 0.1f) - (length_rel * 0.1f));
			Ang3 angles(0, 0, 0);
			Vec3 position(0, 0, arrow_dst);
			Matrix34 tpLocalTM = Matrix34(Matrix33::CreateRotationXYZ(DEG2RAD(angles)));
			tpLocalTM.ScaleColumn(Vec3(1, 1, 1));
			tpLocalTM.SetTranslation(position);
			this->GetEntity()->SetSlotLocalTM(0, tpLocalTM);
		}
		//---------------------------------------------------------------------------------------------------------------------------------------
		IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
		//----------------------------------------------------------------------------------------------------------------------------------------
		if (gEnv->bServer && !m_stickyProjectile.IsStuck() && !m_stuck)
		{
			if (prsbl_stk)
			{
				//temp fix--------------------------------------------------------------------------------------------------------------------
				if (!gEnv->bMultiplayer)
				{
					m_stickyProjectile.Stick(CStickyProjectile::SStickParams(this, pCollision, CStickyProjectile::eO_AlignToVelocity));
				}

				if(gEnv->bMultiplayer)
					m_stickyProjectile.SetFlagsX(CStickyProjectile::eSF_HandleLocally);
			}

			m_stuck = true;
			if (pTarget)
			{
				CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if (!pActor_trg)
				{
					CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
					if (!pItem)
					{
						Vec3 hitDir(ZERO);
						if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
						{
							hitDir = pCollision->vloc[0].GetNormalized();
						}
						GenerateArtificialCollision(this->GetEntity()->GetPhysics(), pTarget->GetPhysics(), pCollision->idmat[0], pCollision->pt,
							pCollision->n, hitDir.GetNormalized() * 3.0f, pCollision->partid[1],
							pCollision->idmat[1], pCollision->iPrim[1], hitDir.GetNormalized() * 2.0f);

						this->SetVelocity(pCollision->pt, hitDir.GetNormalized(), Vec3(0, 0, 0), 0.0f);
					}
				}
				else
				{
					if (gEnv->bMultiplayer)
						m_stickyProjectile.SetFlagsX(CStickyProjectile::eSF_HandleLocally | CStickyProjectile::eSF_IsStuck);
				}
			}

			if (!m_stickyProjectile.IsStuck())
			{
				SetAspectProfile(eEA_Physics, ePT_Rigid);
				m_can_do_damage = false;
			}
		}
	}
	//CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{
		EventPhysCollision *pCollision = reinterpret_cast<EventPhysCollision *>(event.ptr);
		if (!pCollision)
			return;

		IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
		//Only process hits that have a target
		if (pTarget)
		{
			Vec3 dir(0, 0, 0);
			if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
				dir = pCollision->vloc[0].GetNormalized();

			float finalDamage = GetFinalDamage(pCollision->pt);
			const int hitMatId = pCollision->idmat[1];

			Vec3 hitDir(ZERO);
			if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
			{
				hitDir = pCollision->vloc[0].GetNormalized();
			}

			CGameRules *pGameRules = g_pGame->GetGameRules();
			IActor* pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId);
			CActor  *pActor_c = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
			bool ok = true;
			float m_crt_dmg_mult = 1.0f;
			CWeapon *pWpn = GetWeapon();
			if (pWpn)
			{
				int rnd_value1 = rand() % 100 + 1;
				if (rnd_value1 > 0 && rnd_value1 < pWpn->GetCriticalHitBaseChance()*pActor_c->crtchance_common_ml*pActor_c->crtchance_bows_ml)
					m_crt_dmg_mult = pWpn->GetCriticalHitDmgModiffer()*pActor_c->crtdmg_common_ml*pActor_c->crtdmg_bows_ml;
			
				finalDamage = finalDamage * pActor_c->bows_ml;
				finalDamage = ((finalDamage + pWpn->GetDmgAdded())*(m_crt_dmg_mult + (pActor_c->GetAgility()*0.01)));
			}

			if (m_can_do_damage)
				ProcessHit(*pGameRules, *pCollision, *pTarget, finalDamage, hitMatId, hitDir);


			//-------------------------------------------------------------------------------------------------------------------------
			/*CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
			if (!pActor_trg)
			{

				GenerateArtificialCollision(this->GetEntity()->GetPhysics(), pTarget->GetPhysics(), pCollision->idmat[0], pCollision->pt,
					pCollision->n, hitDir.GetNormalized() * 3.0f, pCollision->partid[1],
					pCollision->idmat[1], pCollision->iPrim[1], hitDir.GetNormalized() * 2.0f);

				CProjectile::SetVelocity(pCollision->pt, hitDir.GetNormalized(), Vec3(0,0,0),0.0f);
				if (ISurfaceType *pSurfaceType = gEnv->p3DEngine->GetMaterialManager()->GetSurfaceType(pCollision->idmat[1]))
				{
					if (pSurfaceType->GetBreakability() == 1)
					{
						CProjectile::GetGameObject()->SetAspectProfile(eEA_Physics, ePT_Static);
						GenerateArtificialCollision(this->GetEntity()->GetPhysics(), pTarget->GetPhysics(), pCollision->idmat[0], pCollision->pt,
							pCollision->n, hitDir.GetNormalized() * 3.0f, pCollision->partid[1],
							pCollision->idmat[1], pCollision->iPrim[1], hitDir.GetNormalized() * 2.0f);
						CProjectile::GetGameObject()->SetAspectProfile(eEA_Physics, ePT_Static);

					}
				}
			}*/
		}
		//temp fix--------------------------------------------------------------------------------------------------------------------
		if (gEnv->bMultiplayer)
		{
			GetEntity()->Hide(true);
			Destroy();
			return;
		}
		//--------------------------------------------------------------------------------------------------------------------------------
		if (m_stuck)
		{
			if (pTarget)
			{
				CActor *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if (pActor_trg)
					return;
			}

			IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
			if (!pItemSystem)
				return;

			EntityId item_prj = pItemSystem->CreateItemSimple(GetEntity()->GetWorldTM(), m_pAmmoParams->base_ammo_item_class.c_str());
			IEntity *pItemEnt = gEnv->pEntitySystem->GetEntity(item_prj);
			IEntity *pParentEnt = GetEntity()->GetParent();
			if (pParentEnt)
			{
				CActor *pActor_prt = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pParentEnt->GetId()));
				if (pActor_prt)
					return;
			}

			if (pItemEnt/* && pParentEnt*/)
			{
				CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pItemEnt->GetId()));
				if (pItem)
				{
					if (pParentEnt)
					{
						SEntityPhysicalizeParams params;
						params.type = PE_NONE;
						params.nSlot = -1;
						pItemEnt->Physicalize(params);
						SChildAttachParams attachParams(IEntity::ATTACHMENT_KEEP_TRANSFORMATION);
						pParentEnt->AttachChild(pItemEnt, attachParams);
						IItem *pItemc = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pParentEnt->GetId()));
						if (!pItemc)
						{
							params.type = PE_STATIC;
							params.nSlot = 11;
							pItemEnt->Physicalize(params);
						}
						pItem->DrawSlot(11, true);
					}
					pItemEnt->SetStatObj(GetEntity()->GetStatObj(0), 11, false);
					pItemEnt->SetSlotLocalTM(11, GetEntity()->GetSlotLocalTM(0, false));
					if (!pParentEnt)
					{
						if (m_can_do_damage)
						{
							pItem->Physicalize(true, false);
						}
						else
						{
							pItem->Physicalize(true, true);
							pItem->Impulse(pItemEnt->GetPos(), pCollision->n, 2.0f);
						}
					}
					pItem->Pickalize(true, true);
					pItem->DrawSlot(1, false);
					if (pTarget)
					{
						CItem* pItemXXCl = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
						if (pItemXXCl)
						{
							if (pItemXXCl->GetShield() == 1)
							{
								pItem->Physicalize(false, false);
								pItem->Pickalize(true, true);
								pItem->DrawSlot(1, false);
							}
						}
					}
					//CryLogAlways("CArrow Item entity pos now x- %f y- %f z- %f", pItemEnt->GetPos().x, pItemEnt->GetPos().y, pItemEnt->GetPos().z);
					Destroy();
				}
			}
		}
	}
}

//-------------------------------------------
void CArrow::Launch(const Vec3 &pos, const Vec3 &dir, const Vec3 &velocity, float speedScale)
{
	//CryLogAlways("CArrow::Launch called");
	Vec3 new_vel(0, 0, 0);
	CWeapon *pWpn = GetWeapon();
	if (pWpn)
	{
		if (pWpn->GetWeaponType() == 9)
		{
			CBow *pBow = static_cast<CBow*>(pWpn);
			if (pBow)
			{
				if (pWpn->GetOwnerActor())
					m_Wpn_lcn_Pos = pWpn->GetOwnerActor()->GetEntity()->GetWorldPos();

				m_velocity_mult = pBow->m_arrow_hold_time_old;
				if (m_velocity_mult > pBow->m_hold_time_max_to_vel)
					m_velocity_mult = pBow->m_hold_time_max_to_vel;

				new_vel = velocity;
				new_vel = new_vel*m_velocity_mult;
				//CryLogAlways("CArrow::Launch, vel mult = %f", m_velocity_mult);
				if (gEnv->bServer && gEnv->IsDedicated())
				{
					CProjectile::Launch(Vec3(ZERO), Vec3(0,0,-1), new_vel, m_velocity_mult);
					return;
				}
				CProjectile::Launch(pos, dir, new_vel, m_velocity_mult);
				return;
			}
		}
		else if (pWpn->GetWeaponType() == 11)
		{
			CCrossBow *pCrossBow = static_cast<CCrossBow*>(pWpn);
			if (pCrossBow)
			{
				if (pWpn->GetOwnerActor())
					m_Wpn_lcn_Pos = pWpn->GetOwnerActor()->GetEntity()->GetWorldPos();

				m_velocity_mult = pCrossBow->bolt_speed_mult;
				new_vel = velocity;
				new_vel = new_vel*m_velocity_mult;
				CProjectile::Launch(pos, dir, new_vel, m_velocity_mult);
				return;
			}
		}
	}
	else
	{
		m_Wpn_lcn_Pos = pos;
	}
	CProjectile::Launch(pos, dir, velocity, speedScale);
}

//-------------------------------------------
void CArrow::SetParams(const SProjectileDesc& projectileDesc)
{
	CProjectile::SetParams(projectileDesc);
}

//-------------------------------------------
void CArrow::Explode(const SExplodeDesc& explodeDesc)
{
	CProjectile::Explode(explodeDesc);
}

//-------------------------------------------
void CArrow::OnHit(const HitInfo& hit)
{
	CProjectile::OnHit(hit);
}

//-----------------------------------------------

//-------------------------------------------------------------
void CArrow::RefineCollision(EventPhysCollision *pCollision)
{
	static ray_hit hit;
	static const int objTypes = ent_all;    
	static const unsigned int flags = rwi_stop_at_pierceable|rwi_colltype_any;
	static const float c4Offset = 0.05f;

	Vec3 pos = pCollision->pt+(pCollision->n*0.2f);
	int hits = gEnv->pPhysicalWorld->RayWorldIntersection(pos,pCollision->n*-0.7f,objTypes,flags,&hit,1,&m_pPhysicalEntity,m_pPhysicalEntity?1:0);
	if(hits!=0)
	{
		
	}
}

void CArrow::ProcessHit(CGameRules& gameRules, const EventPhysCollision& collision, IEntity& target, float damage, int hitMatId, const Vec3& hitDir)
{
	if (damage > 0.0f)
	{
		m_hitTypeId = CActor::eADRT_Arrow;
		EntityId targetId = target.GetId();
		HitInfo hitInfo(m_ownerId ? m_ownerId : m_hostId, targetId, m_weaponId,
			damage, 0.0f, hitMatId, collision.partid[1],
			m_hitTypeId, collision.pt, hitDir, collision.n);

		hitInfo.remote = IsRemote();
		hitInfo.projectileId = GetEntityId();
		hitInfo.bulletType = m_pAmmoParams->bulletType;
		hitInfo.knocksDown = CheckAnyProjectileFlags(ePFlag_knocksTarget) && (damage > m_minDamageForKnockDown);
		hitInfo.knocksDownLeg = m_chanceToKnockDownLeg > 0 && damage > m_minDamageForKnockDownLeg && m_chanceToKnockDownLeg > cry_random(0, 99);
		hitInfo.penetrationCount = 0;
		//hitInfo.partId
		hitInfo.hitViaProxy = CheckAnyProjectileFlags(ePFlag_firedViaProxy);
		hitInfo.aimed = CheckAnyProjectileFlags(ePFlag_aimedShot);
		IPhysicalEntity* pPhysicalEntity = collision.pEntity[1];
		gameRules.ClientHit(hitInfo);
		ReportHit(targetId);
	}
}

void CArrow::GenerateArtificialCollision(IPhysicalEntity* pAttackerPhysEnt, IPhysicalEntity *pCollider, int colliderMatId, const Vec3 &position, const Vec3& normal, const Vec3 &vel, int partId, int surfaceIdx, int iPrim, const Vec3& impulse)
{
	//////////////////////////////////////////////////////////////////////////
	// Apply impulse to object.
	pe_action_impulse ai;
	ai.partid = partId;
	ai.point = position;
	ai.impulse = impulse;
	pCollider->Action(&ai);

	//////////////////////////////////////////////////////////////////////////
	// create a physical collision 
	pe_action_register_coll_event collision;

	collision.collMass = 0.005f; // this needs to be set to something.. but is seemingly ignored in calculations... (just doing what meleeCollisionHelper does here..)
	collision.partid[1] = partId;

	// collisions involving partId<-1 are to be ignored by game's damage calculations
	// usually created articially to make stuff break.
	collision.partid[0] = -2;
	collision.idmat[1] = surfaceIdx;
	collision.idmat[0] = colliderMatId;
	collision.n = normal;
	collision.pt = position;
	collision.vSelf = vel;
	collision.v = vel;//Vec3(0, 0, 0);
	collision.pCollider = pCollider;
	collision.iPrim[0] = -1;
	collision.iPrim[1] = iPrim;

	pAttackerPhysEnt->Action(&collision);
}

float CArrow::GetFinalDamage(const Vec3& hitPos) const
{
	float damage = (float)m_damage;

	if (m_damageFallOffAmount > 0.f)
	{
		float distTravelledSq = (m_initial_pos - hitPos).GetLengthSquared();
		bool fallof = distTravelledSq > (m_damageFallOffStart*m_damageFallOffStart);
		bool pointBlank = distTravelledSq < (m_pointBlankFalloffDistance*m_pointBlankFalloffDistance);
		float distTravelled = (fallof || pointBlank) ? sqrt_tpl(distTravelledSq) : 0.0f;

		if (fallof)
		{
			distTravelled -= m_damageFallOffStart;
			damage -= distTravelled * m_damageFallOffAmount;
		}

		if (pointBlank)
		{
			damage *= Projectile::GetPointBlankMultiplierAtRange(distTravelled, m_pointBlankDistance, m_pointBlankFalloffDistance, m_pointBlankAmount);
		}
	}

	damage = max(damage, m_damageFalloffMin);
	damage = damage*m_velocity_mult;

	return damage;
}