#include "StdAfx.h"
#include "PlayerPathFinding.h"

#include "Player.h"
#include "PlayerInput.h"
#include "PlayerMovementController.h"
#include <CryAISystem/IMovementSystem.h>
#include <CryAISystem/MovementRequest.h>
#include <CryAISystem/IAISystem.h>
#include <CryAISystem/IAgent.h>
#include <CryAISystem/MovementStyle.h>
#include <CryAISystem/IAIActorProxy.h>
#include <CryAISystem/IAIActor.h>

CPlayerPathFinding::CPlayerPathFinding()
	: m_movementRequestId(0)
	, m_pPathFollower(nullptr)
	, m_pFoundPath(nullptr)
{

}

CPlayerPathFinding::~CPlayerPathFinding()
{
	gEnv->pAISystem->GetMovementSystem()->UnregisterEntity(GetEntityId());
	m_pPathFollower.reset();
	SAFE_RELEASE(m_pFoundPath);
}

void CPlayerPathFinding::PostInit(IGameObject *pGameObject)
{
	m_pPlayer = static_cast<CPlayer *>(pGameObject->QueryExtension("Player"));
	if (!m_pPlayer)
		return;

	m_navigationAgentTypeId = gEnv->pAISystem->GetNavigationSystem()->GetAgentTypeID("MediumSizedCharacters");

	m_callbacks.queuePathRequestFunction = functor(*this, &CPlayerPathFinding::RequestPathTo);
	m_callbacks.checkOnPathfinderStateFunction = functor(*this, &CPlayerPathFinding::GetPathfinderState);
	m_callbacks.getPathFollowerFunction = functor(*this, &CPlayerPathFinding::GetPathFollower);
	m_callbacks.getPathFunction = functor(*this, &CPlayerPathFinding::GetINavPath);

	gEnv->pAISystem->GetMovementSystem()->RegisterEntity(GetEntityId(), m_callbacks, *this);
	if (m_pPathFollower == nullptr)
	{
		PathFollowerParams params;
		params.maxAccel = m_pPlayer->GetStanceMaxSpeed(m_pPlayer->GetStance());
		params.maxSpeed = params.maxAccel;
		params.minSpeed = 0.f;
		params.normalSpeed = params.maxSpeed;

		params.use2D = false;

		m_pPathFollower = gEnv->pAISystem->CreateAndReturnNewDefaultPathFollower(params, m_pathObstacles);
	}
	m_movementAbility.b3DMove = true;
}

void CPlayerPathFinding::RequestMoveTo(const Vec3 &position)
{
	if (m_pPathFollower == nullptr)
	{
		PathFollowerParams params;
		params.maxAccel = m_pPlayer->GetStanceMaxSpeed(m_pPlayer->GetStance());
		params.maxSpeed = params.maxAccel;
		params.minSpeed = 0.f;
		params.normalSpeed = params.maxSpeed;

		params.use2D = false;

		m_pPathFollower = gEnv->pAISystem->CreateAndReturnNewDefaultPathFollower(params, m_pathObstacles);
	}
	CRY_ASSERT_MESSAGE(m_movementRequestId.id == 0, "RequestMoveTo can not be called while another request is being handled!");

	MovementRequest movementRequest;
	movementRequest.entityID = m_pPlayer->GetEntityId();
	movementRequest.destination = position;
	movementRequest.callback = functor(*this, &CPlayerPathFinding::MovementRequestCallback);
	movementRequest.style.SetSpeed(MovementStyle::Run);

	movementRequest.type = MovementRequest::Type::MoveTo;

	m_state = Movement::StillFinding;

	m_movementRequestId = gEnv->pAISystem->GetMovementSystem()->QueueRequest(movementRequest);
}

void CPlayerPathFinding::RequestPathTo(MNMPathRequest &request)
{
	if (m_pPathFollower == nullptr)
	{
		PathFollowerParams params;
		params.maxAccel = m_pPlayer->GetStanceMaxSpeed(m_pPlayer->GetStance());
		params.maxSpeed = params.maxAccel;
		params.minSpeed = 0.f;
		params.normalSpeed = params.maxSpeed;

		params.use2D = false;

		m_pPathFollower = gEnv->pAISystem->CreateAndReturnNewDefaultPathFollower(params, m_pathObstacles);
	}
	m_state = Movement::StillFinding;

	request.resultCallback = functor(*this, &CPlayerPathFinding::OnMNMPathResult);
	m_pathFinderRequestId = gEnv->pAISystem->GetMNMPathfinder()->RequestPathTo(this, request);
}

void CPlayerPathFinding::CancelCurrentRequest()
{
	CRY_ASSERT(m_movementRequestId.id != 0);

	gEnv->pAISystem->GetMovementSystem()->CancelRequest(m_movementRequestId);
	m_movementRequestId = 0;

	if (m_pathFinderRequestId != 0)
	{
		gEnv->pAISystem->GetMNMPathfinder()->CancelPathRequest(m_pathFinderRequestId);

		m_pathFinderRequestId = 0;
	}
}

void CPlayerPathFinding::ForceSetMPlayer(CPlayer * pPlayer)
{
	if (pPlayer)
	{
		PostInit(pPlayer->GetGameObject());
	}
}

void CPlayerPathFinding::OnMNMPathResult(const MNM::QueuedPathID& requestId, MNMPathRequestResult& result)
{
	m_pathFinderRequestId = 0;

	if (result.HasPathBeenFound())
	{
		m_state = Movement::FoundPath;

		SAFE_DELETE(m_pFoundPath);
		m_pFoundPath = result.pPath->Clone();

		// Bump version
		m_pFoundPath->SetVersion(m_pFoundPath->GetVersion() + 1);

		m_pPathFollower->Reset();
		m_pPathFollower->AttachToPath(m_pFoundPath);
	}
	else
	{
		m_state = Movement::CouldNotFindPath;
	}
}

Vec3 CPlayerPathFinding::GetVelocity() const
{
	return m_pPlayer->GetActorPhysics().velocity;
}

void CPlayerPathFinding::SetMovementOutputValue(const PathFollowResult& result)
{
	float frameTime = gEnv->pTimer->GetFrameTime();
	float moveStep = m_pPlayer->GetStanceMaxSpeed(m_pPlayer->GetStance()) * frameTime;
	if (result.velocityOut.GetLength() > moveStep)
	{
		RequestMove(result.velocityOut.GetNormalized() * moveStep);
	}
	else
	{
		SetVelocity(result.velocityOut);
	}
}

void CPlayerPathFinding::ClearMovementState()
{
	SetVelocity(ZERO);
}

void CPlayerPathFinding::RequestMove(const Vec3 &direction)
{
	CryLogAlways("CPlayerPathFinding::RequestMove");
	if (auto *pPhysicalEntity = m_pPlayer->GetEntity()->GetPhysics())
	{
		pe_action_move moveAction;
		// Add dir to velocity
		moveAction.iJump = 2;
		moveAction.dir = direction;

		// Dispatch the movement request
		pPhysicalEntity->Action(&moveAction);
	}
}

void CPlayerPathFinding::SetVelocity(const Vec3 &velocity)
{
	CryLogAlways("CPlayerPathFinding::SetVelocity");
	if (auto *pPhysicalEntity = m_pPlayer->GetEntity()->GetPhysics())
	{
		pe_action_move moveAction;
		// Change velocity instantaneously
		moveAction.iJump = 1;

		moveAction.dir = velocity;

		// Dispatch the movement request
		pPhysicalEntity->Action(&moveAction);
	}
}

Vec3 CPlayerPathFinding::GetVelocity()
{
	if (m_pPlayer)
	{
		if (auto *pPhysicalEntity = m_pPlayer->GetEntity()->GetPhysics())
		{
			pe_status_living status;
			if (pPhysicalEntity->GetStatus(&status))
				return status.vel;
		}
	}

	return ZERO;
}

void CPlayerPathFinding::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	IEntity &entity = *m_pPlayer->GetEntity();
	IPhysicalEntity *pPhysicalEntity = entity.GetPhysics();
	if (pPhysicalEntity == nullptr)
		return;

	// Obtain stats from the living entity implementation
	GetLatestPhysicsStats(*pPhysicalEntity);
}

void CPlayerPathFinding::GetLatestPhysicsStats(IPhysicalEntity &physicalEntity)
{
	pe_status_living livingStatus;
	if (physicalEntity.GetStatus(&livingStatus) != 0)
	{
		m_bOnGround = !livingStatus.bFlying;

		// Store the ground normal in case it is needed
		// Note that users have to check if we're on ground before using, is considered invalid in air.
		m_groundNormal = livingStatus.groundSlope;
	}
}