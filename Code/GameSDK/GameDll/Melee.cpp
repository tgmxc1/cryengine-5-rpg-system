// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
-------------------------------------------------------------------------
$Id$
$DateTime$

-------------------------------------------------------------------------
History:
- 23:3:2006   13:05 : Created by M�rcio Martins

*************************************************************************/
#include "StdAfx.h"
#include "Melee.h"
#include "MeleeTimedEAAS.h"
#include "Game.h"
#include "Item.h"
#include "Weapon.h"
#include "GameRules.h"
#include "Player.h"
#include "IVehicleSystem.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <CryAction/IMaterialEffects.h>
#include "GameCVars.h"
#include "WeaponSharedParams.h"
#include "GameCodeCoverage/GameCodeCoverageTracker.h"
#include "WeaponMelee.h"

#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>	
#include "ScreenEffects.h"
#include "AutoAimManager.h"
#include <CryAISystem/ITargetTrackManager.h>
#include <CryAISystem/IAIActor.h>

#include "PlayerStateEvents.h"
#include "WeaponSystem.h"
#include "UI/HUD/HUDEventDispatcher.h"

#include "ItemAnimation.h"
#include "RecordingSystem.h"
#include "ICryMannequin.h"

#include <CrySystem/ITimer.h>

#include "ActorImpulseHandler.h"

#include "Utility/VectorMathFnc.h"
//#include <platform.h>
//#include <UnicodeFunctions.h>

//#include <windows.h>
//#include "windows.h"

#if 0 && (defined(USER_claire) || defined(USER_tombe) || defined(USER_johnmichael)|| defined(USER_evgeny)) 
#define MeleeDebugLog(...)				CryLogAlways("[MELEE DEBUG] " __VA_ARGS__)
#else
#define MeleeDebugLog(...)				(void)(0)
#endif

EntityId	CMelee::s_meleeSnapTargetId = 0;
float			CMelee::s_fNextAttack = 0.0f;
bool			CMelee::s_bMeleeSnapTargetCrouched = false;


CRY_IMPLEMENT_GTI_BASE(CMelee);


//------------------------------------------------------------------------
CMelee::CMelee()
: m_pWeapon(NULL)
, m_pMeleeParams(NULL)
, m_attacking(false)
, m_attacked(false)
, m_netAttacking(false)
, m_slideKick(false)
, dual_weld_master(false)
, dual_weld_slave(false)
, m_delayTimer(0.0f)
, m_useMeleeWeaponDelay(-1.0f)
, m_shortRangeAttack(false)
, m_hitTypeID(0)
, m_hitStatus(EHitStatus_Invalid)
, m_pMeleeAction(NULL)
, m_attackTurnAmount(0.f)
, m_attackTurnAmountSmoothRate(0.f)
, m_attackTime(0.f)
, m_piActionController(NULL)
//-------------------------
, m_nextatkTimer(0.0f)
, m_nextatk(0)
, num_mtl_eff_spwn(0)
, num_hitedEntites(1)
, m_crt_hit_dmg_mult(1.0f)
, m_updtimer(0.0f)
, m_updtimer2(0.0f)
, m_recoil_ca(0.0f)
, m_meleeScale(1.0f)
, m_durationTimer(0.0f)
, m_time_for_intersection_delay(0.00001f)
, m_timer_id(NULL)
, ets_timer(NULL)
, pos_initial(Vec3(ZERO))
, pos_start_old(Vec3(ZERO))
, pos_end_old(Vec3(ZERO))
, pos_start_nv(Vec3(ZERO))
, pos_end_nv(Vec3(ZERO))
, normalized_hit_direction(Vec3(ZERO))
, m_nextatk_counter(0)
//------------------------
{
	m_collisionHelper.SetUser(this);

	m_lastRayHit.pCollider = NULL;
}

void CMelee::Release()
{
	if (g_pGame)
	{
		g_pGame->GetWeaponSystem()->DestroyMeleeMode(this);
	}
	if( m_lastRayHit.pCollider )
	{
		m_lastRayHit.pCollider->Release();
	}
}

//------------------------------------------------------------------------
CMelee::~CMelee()
{
	m_pMeleeParams = NULL;
	SAFE_RELEASE(m_pMeleeAction);
	SAFE_RELEASE(m_lastRayHit.pCollider);
}

void CMelee::InitMeleeMode(CWeapon* pWeapon, const SMeleeModeParams* pParams)
{
	CRY_ASSERT_MESSAGE(pParams, "Melee Mode Params NULL! Have you set up the weapon xml correctly?");

	m_pWeapon = pWeapon;
	m_pMeleeParams = pParams;
	if (gEnv->p3DEngine && gEnv->p3DEngine->GetMaterialManager())
	{
		if (m_pMeleeParams)
		{
			for (int i = 0; i < 25; i++)
			{
				if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined") || m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].empty())
				{
					continue;
				}
				else
				{
					IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].c_str());
					if (!pMtl)
						pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].c_str(), false);

					IStatObj *pWpnFpObj = NULL;
					if (m_pWeapon && m_pWeapon->GetEntity())
					{
						pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(1);
						if (pWpnFpObj)
						{
							pMtl->PrecacheMaterial(1.0f, pWpnFpObj->GetRenderMesh(), false);
						}
						pWpnFpObj = NULL;
						pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
						if (pWpnFpObj)
						{
							pMtl->PrecacheMaterial(1.0f, pWpnFpObj->GetRenderMesh(), false);
						}
						pWpnFpObj = NULL;
					}
					pMtl = NULL;
				}
			}
		}
	}
}

void CMelee::InitFragmentData()
{
	if( m_pWeapon && m_pWeapon->GetOwnerActor() && m_pWeapon->GetOwnerActor()->GetAnimatedCharacter() )
	{
		m_piActionController = m_pWeapon->GetOwnerActor()->GetAnimatedCharacter()->GetActionController();

		assert( m_pWeapon->GetOwnerActor()->IsPlayer() ); // If this is not the case, we cannot use PlayerMannequin!

		const CTagDefinition* pTagDefinition = m_piActionController->GetTagDefinition( PlayerMannequin.fragmentIDs.melee_weapon );
		if ( pTagDefinition )
		{
			m_pMeleeParams->meleetags.Resolve( pTagDefinition, PlayerMannequin.fragmentIDs.melee_weapon, m_piActionController );
		}
		else
		{
			CryWarning( VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "CMelee: tagdefs for melee_weapon fragment missing for the player." );
		}
	}
}

//------------------------------------------------------------------------
void CMelee::Update(float frameTime, uint32 frameId)
{
	FUNCTION_PROFILER( GetISystem(), PROFILE_GAME );
	if (g_pGameCVars->g_enable_fl_limit_for_melee_system > 0)
	{
		if (m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 1)
		{
			if (frameTime > g_pGameCVars->g_ft_limit_val_for_melee)
				frameTime = g_pGameCVars->g_ft_limit_val_for_melee;
		}
	}
	bool remote = false;
	bool doMelee = false;
	bool requireUpdate = false;
	CActor* pOwnerAct1 = m_pWeapon->GetOwnerActor();
	if (!pOwnerAct1)
	{
		m_attacking = false;
		m_nextatkTimer = 0.0f;
		m_delayTimer = 0.0f;
		m_nextatk = 0;
		m_nextatk_counter = 0;
		num_hitedEntites = 1;
		m_crt_hit_dmg_mult = 1.0f;
		m_updtimer = 0.0f;
		m_updtimer2 = 0.0f;
		m_updtimer3 = 0.0f;
		impulse_to_water_time = 0.0f;
		m_recoil_ca = 0.0f;
		pos_initial = Vec3(ZERO);
		pos_start_old = Vec3(ZERO);
		pos_end_old = Vec3(ZERO);
		pos_start_nv = Vec3(ZERO);
		pos_end_nv = Vec3(ZERO);
		num_mtl_eff_spwn = 0;
		m_hitTypeID = CGameRules::EHitType::Melee;
		m_mat_spawned = 0;
		return;
	}

	if (pOwnerAct1->GetHealth() <= 0)
	{
		m_attacking = false;
		m_nextatkTimer = 0.0f;
		m_delayTimer = 0.0f;
		m_nextatk = 0;
		m_nextatk_counter = 0;
		num_hitedEntites = 1;
		m_crt_hit_dmg_mult = 1.0f;
		m_updtimer = 0.0f;
		m_updtimer2 = 0.0f;
		m_updtimer3 = 0.0f;
		impulse_to_water_time = 0.0f;
		m_recoil_ca = 0.0f;
		pos_initial = Vec3(ZERO);
		pos_start_old = Vec3(ZERO);
		pos_end_old = Vec3(ZERO);
		pos_start_nv = Vec3(ZERO);
		pos_end_nv = Vec3(ZERO);
		num_mtl_eff_spwn = 0;
		m_hitTypeID = CGameRules::EHitType::Melee;
		m_mat_spawned = 0;
		return;
	}

	if (m_recoil_ca > 0.0f)
	{
		requireUpdate = true;
		m_recoil_ca -= frameTime;
		if (m_recoil_ca <= 0.0f)
			m_recoil_ca = 0.0f;
	}

	if (m_updtimer > m_pMeleeParams->meleeparams.ray_test_end_delay[m_nextatk])
	{
		m_updtimer -= frameTime;
		if (m_updtimer <= m_pMeleeParams->meleeparams.ray_test_end_delay[m_nextatk])
		{
			CActor* pOwner = m_pWeapon->GetOwnerActor();
			m_updtimer = 0.0f;
			impulse_to_water_time = 0.0f;
			if (pOwner->IsPlayer())
			{
				m_pWeapon->Physicalize(false, false);
				m_pWeapon->GetEntity()->EnablePhysics(false);
				m_pWeapon->IgnoreCollision(false);
				if (m_timer_id != NULL)
					gEnv->pGame->GetIGameFramework()->RemoveTimer(m_timer_id);

				m_timer_id = NULL;
				//crtorvhd2_nt->FlagStop();
				//crtorvhd2_nt->m_threadRunning = false;
				pos_start_old = Vec3(ZERO);
				pos_end_old = Vec3(ZERO);
				pos_start_nv = Vec3(ZERO);
				pos_end_nv = Vec3(ZERO);
				num_mtl_eff_spwn = 0;
				pos_initial = Vec3(ZERO);
				m_updtimer3 = 0.0f;
				impulse_to_water_time = 0.0f;
				m_mat_spawned = 0;
			}
		}
	}

	if (m_updtimer > m_pMeleeParams->meleeparams.ray_test_end_delay[m_nextatk])
	{
		CActor* pOwner = m_pWeapon->GetOwnerActor();
		if (!pOwner)
			return;
		//����� ����� ������� ��� ������ �����
		if (m_pMeleeParams->meleeparams.is_new_melee && (m_updtimer <= (m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] - m_pMeleeParams->meleeparams.ray_test_start_delay[m_nextatk])))
		{
			pos_start_old = pos_start_nv;
			pos_end_old = pos_end_nv;

			if (pos_initial.IsZero())
				pos_initial = pos_start_old;

			if (m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 1)
			{
				if (pOwner)
				{
					if (!pOwner->IsPlayer() && m_pMeleeParams->meleeparams.is_ai_useable)
						StartRay();
					else if (pOwner->IsPlayer())
						StartRay();
				}
			}

			if(pOwner && pOwner->IsPlayer())
				StartHitedEntsDmgProcessing();

			//m_pWeapon->GetEntity()->EnablePhysics(true);
			//m_pWeapon->Physicalize(true,true);
			//m_pWeapon->IgnoreCollision(true);

			if (g_pGameCVars->g_melee_system_clear_hit_targets_after_time > 0)
			{
				m_updtimer3 += frameTime;
				if (m_updtimer3 >= g_pGameCVars->g_melee_system_time_to_clear_hit_targets)
				{
					if (m_pMeleeParams->meleeparams.hited_targets_clear_at[m_nextatk])
					{
						//num_mtl_eff_spwn = 0;
						m_hitedEntites.clear();
					}

					m_updtimer3 = 0.0f;
				}
			}
		}
		//--------------------------------------------------

		m_updtimer2 -= frameTime;
		if (m_updtimer2 <= 0.0f)
		{
			m_updtimer2 = m_pMeleeParams->meleeparams.ray_test_upd;
			if (m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 1)
			{
				if (!pOwner)
					return;

				if (!pOwner->IsPlayer() && m_pMeleeParams->meleeparams.is_ai_useable)
				{
					StartRay2(true);
					StartRay2(false);
				}
				else if (pOwner->IsPlayer())
				{
					StartRay2(true);
					StartRay2(false);
				}
			}
		}
	}

	if (m_durationTimer > 0.0f)
	{
		m_durationTimer -= frameTime;
		if (m_durationTimer <= 0.0f)
		{
			//m_attacking = false;
			m_durationTimer = 0.0f;
		}
		requireUpdate = true;
	}

	if (m_nextatkTimer>0.0f)
	{
		requireUpdate = true;
		m_nextatkTimer -= frameTime;
		if (m_nextatkTimer <= 0.0f)
		{
			m_nextatk = 0;
			m_nextatk_counter = 0;
			m_nextatkTimer = 0.0f;
		}
	}

	Vec3 dirToTarget(ZERO);
	if(m_pMeleeAction && s_meleeSnapTargetId && g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
	{
		m_attackTime += frameTime;

		CActor* pOwner = m_pWeapon->GetOwnerActor();
		IEntity* pTarget = gEnv->pEntitySystem->GetEntity(s_meleeSnapTargetId);
		if(pOwner && pTarget)
		{
			Vec3 targetPos = pTarget->GetWorldPos();

			if(s_bMeleeSnapTargetCrouched)
			{
				targetPos.z -= g_pGameCVars->pl_melee.mp_melee_system_camera_lock_crouch_height_offset;
			}

			dirToTarget = targetPos - pOwner->GetEntity()->GetWorldPos();
			dirToTarget.Normalize();

			static const float smooth_time = 0.1f;
			SmoothCD(m_attackTurnAmount, m_attackTurnAmountSmoothRate, frameTime, 1.f, smooth_time);
			Quat newViewRotation;
			newViewRotation.SetSlerp(pOwner->GetViewRotation(), Quat::CreateRotationVDir(dirToTarget), m_attackTurnAmount);
			Ang3 newAngles(newViewRotation);
			newAngles.y = 0.f; //No head tilting
			newViewRotation = Quat(newAngles);
			pOwner->SetViewRotation( newViewRotation );
		}

		if(m_attackTime >= g_pGameCVars->pl_melee.mp_melee_system_camera_lock_time && pOwner && pOwner->IsClient())
		{
			pOwner->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);
		}
	}

	if (m_updtimer > 0.0f)
		requireUpdate = true;

	///if (m_delayTimer <= 0.0f)
	//{
	//	m_attacking = false;
	//}

	if (m_attacking)
	{
		//MeleeDebugLog ("CMelee<%p> Update while attacking: m_attacked=%d, delay=%f", this, m_attacked, m_delayTimer);

		requireUpdate = true;
		if (m_delayTimer>0.0f)
		{
			RequestAlignmentToNearestTarget();
			m_delayTimer-=frameTime;
			if (m_delayTimer<=0.0f)
			{
				m_attacking = false;
				m_delayTimer=0.0f;
			}
		}
		else if (m_netAttacking)
		{
			remote = true;
			doMelee = true;
			m_attacking = false;
			m_slideKick = false;
			m_netAttacking = false;

			m_pWeapon->SetBusy(false);
		}	
		else if (!m_attacked)
		{
			doMelee = true;
			m_attacked = true;
		}

		if (m_pMeleeParams->meleeparams.is_new_melee)
		{
			if (m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] * 0.6f >= m_updtimer && !m_gtcb && m_nextatk != 0)
			{
				if (CActor *pActor = m_pWeapon->GetOwnerActor())
				{
					if (IMovementController * pMC = pActor->GetMovementController())
					{
						SMovementState info;
						pMC->GetMovementState(info);
						if (g_pGameCVars->g_enable_melee_center_st_rt > 0 || m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 0)
						{
							/*if (!dirToTarget.IsZeroFast())
							{
								pos_start_nv = info.weaponPosition;
								PerformMelee(info.weaponPosition, dirToTarget, remote);
								PerformRayTest3(info.weaponPosition, dirToTarget, 1, false);
							}
							else
							{*/
								pos_start_nv = info.weaponPosition;
								PerformMelee(info.weaponPosition, info.fireDirection, remote);
								PerformRayTest3(info.weaponPosition, info.fireDirection, 1, false);
							//}
						}
					}
				}
				m_gtcb = true;
			}
		}

		if ( !m_collisionHelper.IsBlocked() && doMelee)
		{
			if (CActor *pActor = m_pWeapon->GetOwnerActor())
			{
				if (IMovementController * pMC = pActor->GetMovementController())
				{
					SMovementState info;
					pMC->GetMovementState(info);

					if(!dirToTarget.IsZeroFast())
					{
						if (!m_pMeleeParams->meleeparams.is_new_melee)
							PerformMelee(info.weaponPosition, dirToTarget, remote);
					}
					else
					{
						if (!m_pMeleeParams->meleeparams.is_new_melee)
							PerformMelee(info.weaponPosition, info.fireDirection, remote);
					}
				}
			}
		}

		if (!m_pMeleeParams->meleeparams.is_new_melee)
		{
			if ((m_useMeleeWeaponDelay <= 0.0f && m_useMeleeWeaponDelay > -1.0f))
			{
				m_useMeleeWeaponDelay = -1.0f;

				// Switch to the MELEE WEAPON.
				SwitchToMeleeWeaponAndAttack();

				m_attacking = false;
			}
			m_useMeleeWeaponDelay -= frameTime;
		}
	}

	if (requireUpdate)
	{
		m_pWeapon->RequireUpdate(eIUS_FireMode);
		/*if (m_updtimer > 0.02f && m_timer_id == NULL)
		{
			m_timer_id = gEnv->pGame->GetIGameFramework()->AddTimer(m_time_for_intersection_delay, false, functor(*this, &CMelee::StartIntersection));
			//StartIntersection(NULL, m_timer_id);

			m_pWeapon->GetEntity()->SetTimer(21, 1);
		}*/

		if (g_pGameCVars->g_enable_melee_old_fps_compensation > 0)
		{
			if (m_pWeapon->GetOwnerActor() && m_pWeapon->GetOwnerActor()->IsPlayer() && m_pMeleeParams->meleeparams.is_new_melee)
			{
				//if (!crtorvhd2_nt->IsStarted())
				//	crtorvhd2_nt->Start(99952, "MeleeRtest");

				//crtorvhd2_nt->m_threadRunning = true;
				//crtorvhd2_nt->Run();
			}
		}
	}

	

}

//------------------------------------------------------------------------
void CMelee::Activate(bool activate)
{
	MeleeDebugLog ("CMelee<%p> Activate(%s)", this, activate ? "true" : "false");

	if( !IsMeleeWeapon() )
	{
		m_attacking = false;
		m_slideKick = false;
		m_shortRangeAttack = false;
		m_delayTimer = 0.0f;
	}

	if(!activate && m_pMeleeAction)
	{
		m_pMeleeAction->ForceFinish();
	}
}

//------------------------------------------------------------------------
bool CMelee::CanAttack() const
{
	if(!m_attacking && m_durationTimer <= 0.0f)
	{
		/*if (gEnv->bMultiplayer)
		{
			if(CPlayer * pPlayer = static_cast<CPlayer*>(m_pWeapon->GetOwnerActor()))
			{
				if(pPlayer->IsClient() && gEnv->pTimer->GetFrameStartTime().GetSeconds() > CMelee::s_fNextAttack && !pPlayer->GetStealthKill().IsBusy())
				{
					return true;
				}
			}
			
			return false; 
		}
		else
		{*/
			if (m_pWeapon->GetMeleeTimed())
			{
				if (!m_pWeapon->GetMeleeTimed()->GetRecoilSA())
					return true;
				else
					return false;
			}
			return true;
		//}
	}
	else
	{
		return false;
	}	
}

//------------------------------------------------------------------------
struct CMelee::StopAttackingAction
{
	CMelee *_this;
	StopAttackingAction(CMelee *melee): _this(melee) {};
	void execute(CItem *pItem)
	{
		_this->m_attacking = false;
		_this->m_slideKick = false;
		_this->m_netAttacking = false;

		_this->m_delayTimer = 0.0f;
		pItem->SetBusy(false);
		pItem->ForcePendingActions();
		MeleeDebugLog ("CMelee<%p> StopAttackingAction is being executed!", _this);

		CActor* pActor(NULL);
		if(!gEnv->bMultiplayer)
		{
			if (IEntity* owner = pItem->GetOwner())
				if (IAIObject* aiObject = owner->GetAI())
					if (IAIActor* aiActor = aiObject->CastToIAIActor())
						aiActor->SetSignal(0, "OnMeleePerformed");
		}
		else if( g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn && s_meleeSnapTargetId && (pActor = pItem->GetOwnerActor()) && pActor->IsClient() )
		{
			CActor* pOwnerActor = pItem->GetOwnerActor();
			pOwnerActor->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);

			s_meleeSnapTargetId = 0;
			CHANGED_NETWORK_STATE(pOwnerActor, CPlayer::ASPECT_SNAP_TARGET);
		}

		if(_this->m_pMeleeAction)
		{
			SAFE_RELEASE(_this->m_pMeleeAction);
		}
	}
};

SMeleeTags::SMeleeFragData CMelee::GenerateFragmentData( const SMeleeTags::TTagParamsContainer& tagContainer ) const
{
	const size_t numTags  = tagContainer.size();
	if( numTags > 0 )
	{
		const size_t tagIndex = cry_random((size_t)0, numTags - 1);
		const SMeleeTags::STagParams& meleeTags = tagContainer[tagIndex];

		SMeleeTags::SMeleeFragData fragData( tagIndex, meleeTags );

		return fragData;
	}

	return SMeleeTags::SMeleeFragData();
}

void CMelee::GenerateAndQueueMeleeAction() const
{
	const SMeleeTags& tags = m_pMeleeParams->meleetags;
	const CWeaponMelee::EMeleeStatus meleeStatus = static_cast<CWeaponMelee*> (m_pWeapon)->GetMeleeAttackAction();

	const char* pActionName = NULL;

	switch( meleeStatus )
	{
	case CWeaponMelee::EMeleeStatus_Left:
		GenerateAndQueueMeleeActionForStatus( tags.tag_params_combo_left );
		pActionName = "MeleeCombo";
		break;
	case CWeaponMelee::EMeleeStatus_Right:
		GenerateAndQueueMeleeActionForStatus( tags.tag_params_combo_right );
		pActionName = "MeleeCombo";
		break;
	case CWeaponMelee::EMeleeStatus_KillingBlow:
		GenerateAndQueueMeleeActionForStatus( tags.tag_params_combo_killingblow );
		pActionName = "MeleeKillingBlow";
		break;
	default:
		CryLog( "[Melee] Attempted to run a melee action when the status was unknown" );
		break;
	}
}

void CMelee::GenerateAndQueueMeleeActionForStatus( const SMeleeTags::TTagParamsContainer& tagContainer ) const
{
	SMeleeTags::SMeleeFragData fragData = GenerateFragmentData( tagContainer );
	if( fragData.m_pMeleeTags )
	{
		m_hitTypeID = fragData.m_pMeleeTags->hitType;

		CItemAction *pAction = new CItemAction(PP_PlayerAction, PlayerMannequin.fragmentIDs.melee_weapon, fragData.m_pMeleeTags->tagState);
		m_pWeapon->PlayFragment( pAction );
	}
}

const ItemString &CMelee::SelectMeleeAction() const
{
	CRY_ASSERT_MESSAGE( !IsMeleeWeapon(), "SelectMeleeAction is deprecated for the melee weapon!" );

	const SMeleeActions& actions = m_pMeleeParams->meleeactions;

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		if (m_pMeleeParams->meleeparams.is_new_melee)
		{
			m_hitTypeID = CGameRules::EHitType::Melee;
			if (!m_pMeleeParams->meleeparams.attack_type_on_hit[m_nextatk].empty())
			{
				CGameRules *pGameRules = g_pGame->GetGameRules();
				if (pGameRules)
				{
					int h_t_id = pGameRules->GetHitTypeId(m_pMeleeParams->meleeparams.attack_type_on_hit[m_nextatk].c_str());
					if (h_t_id > 0)
						m_hitTypeID = h_t_id;
				}
			}

			if (m_shortRangeAttack && !actions.attack_closeRange_new[m_nextatk].empty())
			{
				return actions.attack_closeRange_new[m_nextatk];
			}
			else
				return actions.attack_newc[m_nextatk];
		}


		if( !IsMeleeWeapon() )
		{
			m_hitTypeID = CGameRules::EHitType::Melee;

			if (m_shortRangeAttack && !actions.attack_closeRange.empty())
			{
				return actions.attack_closeRange;
			}
		}
	}

	return actions.attack;
}

void CMelee::StartAttack()
{
	bool canAttack = CanAttack();

	MeleeDebugLog("CMelee<%p> StartAttack canAttack=%s", this, canAttack ? "true" : "false");

	if (!canAttack)
		return;

	if (m_recoil_ca > 0.0f)
		return;

	num_hitedEntites = 1;
	m_hitedEntites.clear();
	m_pulsedEntites.clear();
	m_crt_hit_dmg_mult = 1.0f;
	m_Entites_hit_pos.clear();
	m_Entites_need_to_be_hited.clear();
	m_Entites_need_to_be_hited2.clear();
	s_hit_inf.clear();
	m_gtcb = false;
	num_mtl_eff_spwn = 0;

	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (!pOwner)
		return;

	if (pOwner->m_blockactiveshield || pOwner->m_blockactivenoshield)
		return;

	CPlayer *ownerPlayer = m_pWeapon->GetOwnerPlayer();

	m_attacking = true;
	m_slideKick = false;
	m_attacked = false;
	m_pWeapon->RequireUpdate(eIUS_FireMode);
	m_pWeapon->ExitZoom();

	bool isClient = pOwner ? pOwner->IsClient() : false;
	bool doingMultiMeleeAttack = true;

	if (!m_pMeleeParams->meleeparams.is_new_melee)
	{


		if (!DoSlideMeleeAttack(pOwner))
		{
			const float use_melee_weapon_delay = m_pMeleeParams->meleeparams.use_melee_weapon_delay;
			if (use_melee_weapon_delay >= 0.0f)
			{
				if (!ownerPlayer || ownerPlayer->IsSliding() || ownerPlayer->IsExitingSlide())
				{
					m_attacking = false;
					return;
				}

				if (use_melee_weapon_delay == 0.0f)
				{
					// instant switch: don't use the weapon's melee, send the event to the WeaponMelee weapon instead!
					if (SwitchToMeleeWeaponAndAttack())
					{
						m_useMeleeWeaponDelay = -1.0f;
						return;
					}
				}
			}

			if (IsMeleeWeapon())
			{
				doingMultiMeleeAttack = false;

				GenerateAndQueueMeleeAction();
			}
			else if (!gEnv->bMultiplayer || !g_pGameCVars->pl_melee.mp_melee_system || !ownerPlayer || !StartMultiAnimMeleeAttack(ownerPlayer))
			{
				const ItemString &meleeAction = SelectMeleeAction();

				FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
				m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
				//net sync---------------------------------------------------------------------
				CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
				if (nwAction)
				{
					nwAction->NetRequestPlayItemAction2(m_pWeapon->GetOwnerId(), meleeAction.c_str());
				}
				//-----------------------------------------------------------------------------

				doingMultiMeleeAttack = false;
			}

			m_delayTimer = GetDelay();
			m_attackTime = 0.f;

			if (ownerPlayer)
			{
				ownerPlayer->StateMachineHandleEventMovement(PLAYER_EVENT_FORCEEXITSLIDE);
			}
		}
		else
		{
			CRY_ASSERT(pOwner && (pOwner->GetActorClass() == CPlayer::GetActorClassType()));

			doingMultiMeleeAttack = false;

			m_hitTypeID = CGameRules::EHitType::Melee;

			// Dispatch the event and the delay timer is magically set!
			ownerPlayer->StateMachineHandleEventMovement(SStateEventSlideKick(this));

			m_slideKick = true;
		}

		m_pWeapon->SetBusy(true);

		if (!doingMultiMeleeAttack)
		{
			// Set up a timer to disable the melee attack at the end of the first-person animation...
			uint32 durationInMilliseconds = static_cast<uint32>(GetDuration() * 1000.f);
			MeleeDebugLog("CMelee<%p> Setting a timer to trigger StopAttackingAction (duration=%u)", this, durationInMilliseconds);

			// How much longer we need the animation to be than the damage delay, in seconds...
			const float k_requireAdditionalTime = 0.1f;

			if (durationInMilliseconds < (m_delayTimer + k_requireAdditionalTime) * 1000.0f)
			{
				durationInMilliseconds = (uint32)((m_delayTimer + k_requireAdditionalTime) * 1000.0f + 0.5f);	// Add 0.5f to round up when turning into a uint32
			}
			m_pWeapon->GetScheduler()->TimerAction(durationInMilliseconds, CSchedulerAction<StopAttackingAction>::Create(this), true);
		}

		m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());
		m_pWeapon->RequestStartMeleeAttack((m_pWeapon->GetMelee() == this), false);

		if (isClient)
		{
			s_fNextAttack = GetDuration() + gEnv->pTimer->GetFrameStartTime().GetSeconds();
			if (s_meleeSnapTargetId = GetNearestTarget())
			{
				IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(s_meleeSnapTargetId);
				s_bMeleeSnapTargetCrouched = pTargetActor && static_cast<CPlayer*>(pTargetActor)->GetStance() == STANCE_CROUCH;
			}
			CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

			if (ownerPlayer)
			{
				if (ownerPlayer->IsThirdPerson())
				{
					CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_3PSignalId, ownerPlayer->GetEntityId());
				}
				else
				{
					CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_FPSignalId, ownerPlayer->GetEntityId());
				}
			}

			CCCPOINT(Melee_LocalActorMelee);
		}
		else
		{
			CCCPOINT(Melee_NonLocalActorMelee);
		}
	}
	else
	{
		/*const float use_melee_weapon_delay = m_pMeleeParams->meleeparams.use_melee_weapon_delay;
		if (use_melee_weapon_delay >= 0.0f)
		{
			if (!ownerPlayer || ownerPlayer->IsSliding() || ownerPlayer->IsExitingSlide())
			{
				m_attacking = false;
				return;
			}

			if (use_melee_weapon_delay == 0.0f)
			{
				// instant switch: don't use the weapon's melee, send the event to the WeaponMelee weapon instead!
				if (SwitchToMeleeWeaponAndAttack())
				{
					m_useMeleeWeaponDelay = -1.0f;
					return;
				}
			}
		}*/

		if (ownerPlayer)
		{
			ownerPlayer->StateMachineHandleEventMovement(PLAYER_EVENT_FORCEEXITSLIDE);
		}

		if (!m_pMeleeParams->meleeparams.random_attacks_order)
		{
			if (m_pMeleeParams->meleeparams.number_combohits != m_nextatk)
			{
				m_nextatk += 1;
			}
			else
			{
				m_nextatk = 1;
			}
		}
		else
		{
			if (m_pMeleeParams->meleeparams.number_combohits > 1)
			{
				int nxt_tk = m_nextatk;
				nxt_tk = cry_random(1, int(m_pMeleeParams->meleeparams.number_combohits));
				if (nxt_tk == m_nextatk)
				{
					nxt_tk = cry_random(1, int(m_pMeleeParams->meleeparams.number_combohits));
					if (nxt_tk == m_nextatk)
					{
						nxt_tk = cry_random(1, int(m_pMeleeParams->meleeparams.number_combohits));
						if (nxt_tk == m_nextatk)
						{
							if (m_nextatk != 1)
							{
								nxt_tk = 1;
							}
							else if (m_nextatk == 1)
							{
								nxt_tk = 2;
							}
							else if (m_nextatk == m_pMeleeParams->meleeparams.number_combohits)
							{
								nxt_tk = m_pMeleeParams->meleeparams.number_combohits - 1;
							}
						}
					}
				}
				m_nextatk = nxt_tk;
			}
			else
			{
				m_nextatk = 1;
			}
		}

		if (m_nextatk_counter == m_pMeleeParams->meleeparams.number_combohits)
		{
			m_nextatk_counter = 0;
		}
		m_nextatk_counter += 1;

		if (m_pMeleeParams->meleeparams.attack_delay_after_combohits_counter_filled)
		{
			if (m_nextatk_counter == m_pMeleeParams->meleeparams.number_combohits)
			{
				m_recoil_ca += (m_pMeleeParams->meleeparams.delay_new[m_nextatk] + m_pMeleeParams->meleeparams.recoil_at_combo_end);
			}
		}

		if (m_nextatk >= 1)
		{
			if (m_pMeleeParams->meleeparams.is_attack_anim_ctrl[m_nextatk])
			{
				if (pOwner)
				{
					if (pOwner->GetEntity()->GetCharacter(0))
					{
						//CryLogAlways("try to SetAnimationDrivenMotion");
						if (pOwner->GetEntity()->GetCharacter(0)->GetISkeletonAnim())
						{
							//pOwner->GetEntity()->GetCharacter(0)->GetISkeletonAnim()->SetAnimationDrivenMotion(1);
							//CryLogAlways("SetAnimationDrivenMotion complete");
						}
					}
				}
			}
			else
			{
				if (pOwner)
				{
					if (pOwner->GetEntity()->GetCharacter(0))
					{
						if (pOwner->GetEntity()->GetCharacter(0)->GetISkeletonAnim())
						{
							//pOwner->GetEntity()->GetCharacter(0)->GetISkeletonAnim()->SetAnimationDrivenMotion(0);
						}
					}
				}
			}
			const ItemString &meleeAction = SelectMeleeAction();
			FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
			m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction2(m_pWeapon->GetOwnerId(), meleeAction.c_str());
			}
			//-----------------------------------------------------------------------------

			m_delayTimer = m_pMeleeParams->meleeparams.delay_new[m_nextatk];
			m_updtimer = m_pMeleeParams->meleeparams.ray_test_time[m_nextatk];
			m_durationTimer = m_pMeleeParams->meleeparams.duration_new[m_nextatk];
			m_nextatkTimer = m_pMeleeParams->meleeparams.duration_new[m_nextatk] * m_pMeleeParams->meleeparams.next_atk_timer_mult[m_nextatk];
		}

		m_attackTime = 0.f;

		m_pWeapon->SetBusy(true);
		if (pOwner && g_pGameCVars->g_melee_system_attacks_delay_depends_on_actor_stamina > 0)
		{
			float stamina_to_consume = pOwner->stamina_cons_atk_base;
			stamina_to_consume += m_pMeleeParams->meleeparams.stamina_on_atk_consumption[m_nextatk];
			stamina_to_consume *= pOwner->stamina_cons_atk_ml;
			if (stamina_to_consume > 0.0f)
			{
				if (pOwner->GetStamina() < stamina_to_consume)
				{
					m_delayTimer *= g_pGameCVars->g_melee_system_attack_delay_mult_in_low_stamina_mode;
				}
			}
		}

		if (g_pGameCVars->g_melee_system_attacks_delay_based_on_animation_length > 0)
		{
			// Set up a timer to disable the melee attack at the end of the first-person animation...
			uint32 durationInMilliseconds = static_cast<uint32>(m_durationTimer * 1000.f);

			const float k_requireAdditionalTime = 0.1f;

			if (durationInMilliseconds < (m_delayTimer + k_requireAdditionalTime) * 1000.0f)
			{
				durationInMilliseconds = (uint32)((m_delayTimer + k_requireAdditionalTime) * 1000.0f + 0.5f);	// Add 0.5f to round up when turning into a uint32
			}
			m_pWeapon->GetScheduler()->TimerAction(durationInMilliseconds, CSchedulerAction<StopAttackingAction>::Create(this), true);
		}
		else
		{
			uint32 durationInMilliseconds = static_cast<uint32>(m_delayTimer * 1000.f);
			m_pWeapon->GetScheduler()->TimerAction(durationInMilliseconds, CSchedulerAction<StopAttackingAction>::Create(this), true);
		}
		m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());
		m_pWeapon->RequestStartMeleeAttack((m_pWeapon->GetMelee() == this), false);

		if (isClient)
		{
			s_fNextAttack = m_pMeleeParams->meleeparams.duration_new[m_nextatk] + gEnv->pTimer->GetFrameStartTime().GetSeconds();
			if (s_meleeSnapTargetId = GetNearestTarget())
			{
				IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(s_meleeSnapTargetId);
				s_bMeleeSnapTargetCrouched = pTargetActor && static_cast<CPlayer*>(pTargetActor)->GetStance() == STANCE_CROUCH;
			}
			CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

			if (ownerPlayer)
			{
				if (ownerPlayer->IsThirdPerson())
				{
					CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_3PSignalId, ownerPlayer->GetEntityId());
				}
				else
				{
					CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_FPSignalId, ownerPlayer->GetEntityId());
				}
			}
			//CCCPOINT(Melee_LocalActorMelee);
		}
		else
		{
			//CCCPOINT(Melee_NonLocalActorMelee);
		}
	}
}

//------------------------------------------------------------------------
void CMelee::NetAttack()
{
	MeleeDebugLog ("CMelee<%p> NetAttack", this);

	const ItemString &meleeAction = SelectMeleeAction();

	m_pWeapon->OnMelee(m_pWeapon->GetOwnerId());

	CActor* pOwner = m_pWeapon->GetOwnerActor();

	if (!DoSlideMeleeAttack(pOwner))
	{
		if(!gEnv->bMultiplayer || !g_pGameCVars->pl_melee.mp_melee_system || !StartMultiAnimMeleeAttack(pOwner))
		{
			FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
			m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		}
	}
	else
	{
		CRY_ASSERT(pOwner && (pOwner->GetActorClass() == CPlayer::GetActorClassType()));

		if( pOwner->IsPlayer() )
		{
			static_cast<CPlayer*> (pOwner)->StateMachineHandleEventMovement( SStateEventSlideKick(this) );
		}
	}

	m_attacking = true;
	m_slideKick = false;
	m_netAttacking = true;
	m_delayTimer = GetDelay();

	m_pWeapon->SetBusy(true);
	m_pWeapon->RequireUpdate(eIUS_FireMode);

	CPlayer *pOwnerPlayer = m_pWeapon->GetOwnerPlayer();
	if (pOwnerPlayer)
	{
		if (pOwnerPlayer->IsThirdPerson())
		{
			CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_3PSignalId, pOwnerPlayer->GetEntityId());
		}
		else
		{
			CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_FPSignalId, pOwnerPlayer->GetEntityId());
		}
	}
}

//------------------------------------------------------------------------
int CMelee::GetDamage() const
{
	return m_pMeleeParams->meleeparams.damage;
}

//-----------------------------------------------------------------------
void CMelee::PerformMelee(const Vec3 &pos, const Vec3 &dir, bool remote)
{
	CCCPOINT_IF(! remote, Melee_PerformLocal);
	CCCPOINT_IF(remote, Melee_PerformRemote);

	MeleeDebugLog ("CMelee<%p> PerformMelee(remote=%s)", this, remote ? "true" : "false");

#if !defined(DEMO_BUILD_RPG_SS)
	if(g_pGameCVars->pl_melee.debug_gfx)
	{
		IPersistantDebug  *pDebug = g_pGame->GetIGameFramework()->GetIPersistantDebug();
		pDebug->Begin("CMelee::PerformMelee", false);
		pDebug->AddLine(pos, (pos + dir), ColorF(1.f,0.f,0.f,1.f), 15.f);
	}
#endif

	float range = GetRange();
	if(m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 0)
		range = m_pMeleeParams->meleeparams.range_new[m_nextatk];

	m_collisionHelper.DoCollisionTest(SCollisionTestParams(pos, dir, range, m_pWeapon->GetOwnerId(), 0, remote));
}

//------------------------------------------------------------------------
bool CMelee::PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, bool remote)
{
	MeleeDebugLog ("CMelee<%p> PerformCylinderTest(remote=%s)", this, remote ? "true" : "false");

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner?pOwner->GetPhysics():0;
	float range = GetRange();
	if (m_pMeleeParams->meleeparams.phy_test_type[m_nextatk] == 0)
		range = m_pMeleeParams->meleeparams.range_new[m_nextatk];

	primitives::cylinder cyl;
	cyl.r = 0.25f;
	cyl.axis = dir;
	cyl.hh = range *0.5f;
	cyl.center = pos + dir.normalized()*cyl.hh;

	geom_contact *contacts;
	intersection_params params;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = true;
	float n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO),
		ent_rigid|ent_sleeping_rigid|ent_independent|ent_static|ent_terrain|ent_water, &contacts, 0,
		geom_colltype_foliage|geom_colltype_player, &params, 0, 0, &pIgnore, pIgnore?1:0);

	int ret = (int)n;

	float closestdSq = 9999.0f;
	geom_contact *closestc = 0;
	geom_contact *currentc = contacts;

	for (int i=0; i<ret; i++)
	{
		geom_contact *contact = currentc;
		if (contact)
		{
			IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
			if (pCollider)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				if (pEntity)
				{
					if (pEntity == pOwner)
					{
						++currentc;
						continue;
					}
				}

				const float distSq = (pos-currentc->pt).len2();
				if (distSq < closestdSq)
				{
					closestdSq = distSq;
					closestc = contact;
				}
			}
		}
		++currentc;
	}

	if (closestc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

		ApplyMeleeDamage(closestc->pt, dir, -dir, pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], remote, closestc->iPrim[1]);
	}

	return closestc!=0;
}

//------------------------------------------------------------------------
int CMelee::Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, bool remote)
{
	MeleeDebugLog ("CMelee<%p> HitPointDirNormal(remote=%s)", this, remote ? "true" : "false");
	int hitTypeID = 0;

	IEntity *pTargetEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
	if (pTargetEnt)
	{

		std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
		std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
		int count = m_hitedEntites.size();

		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (pTargetEnt->GetId() == (*it))
			{
				return 1;
			}
		}
		GetWpnCrtAtk();
	}

	CActor *pOwnerActor = m_pWeapon->GetOwnerActor();

	if (pOwnerActor)
	{	
		IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
		IEntity* pTarget = pTargetActor ? pTargetActor->GetEntity() : gEnv->pEntitySystem->GetEntity(collidedEntityId);
		IEntity* pOwnerEntity = pOwnerActor->GetEntity();
		IAIObject* pOwnerAI = pOwnerEntity->GetAI();
		
		float damageScale = 1.0f;
		bool silentHit = false;
		bool effects_from_wpn_added = false;
		if(pTargetActor)
		{
			CActor *pActor = static_cast<CActor*>(pTargetActor);
			if (pActor)
			{
				if (pActor->GetHealth() <= 0)
				{
					
				}
			}
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				//add effect in its need
				effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), pt, normal);
				//check block
				if (!nwAction->TestHitBlock(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
				{
					if (pActor)
					{
						CItem* pItemCurrent = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pActor->GetCurrentItemId()));
						if (pItemCurrent)
						{
							if (!pItemCurrent->IsItemIgnoredByHud())
							{
								CWeapon* pWeapon = static_cast<CWeapon*>(pItemCurrent->GetIWeapon());
								if (pWeapon)
								{
									if (pWeapon->GetMelee())
									{
										//CryLogAlways("TestHitBlock, spawn block hit particle request, enemy weapon pos == x - %f, y - %f, z - %f", pWeapon->GetEntity()->GetWorldPos().x, pWeapon->GetEntity()->GetWorldPos().y, pWeapon->GetEntity()->GetWorldPos().z);
										Vec3 particle_ps = pWeapon->GetMelee()->FindNearestPointInIntersectionLine((GetAttackHelperPos(eMIHType_start) + GetAttackHelperPos(eMIHType_end)) * 0.5000f);
										EnemyBlockHited(particle_ps, normal);
									}
								}
							}
						}
					}
					return 1;
				}
			}
			IAnimatedCharacter* pTargetAC = pTargetActor->GetAnimatedCharacter();
			IAnimatedCharacter* pOwnerAC = pOwnerActor->GetAnimatedCharacter();

			if(pTargetAC && pOwnerAC)
			{
				Vec3 targetFacing(pTargetAC->GetAnimLocation().GetColumn1());
				Vec3 ownerFacing(pOwnerAC->GetAnimLocation().GetColumn1());
				float ownerFacingDot = ownerFacing.Dot(targetFacing);
				float fromBehindDot = cos_tpl(DEG2RAD(g_pGameCVars->pl_melee.angle_limit_from_behind));

				if(ownerFacingDot > fromBehindDot)
				{
#ifndef DEMO_BUILD_RPG_SS
					if (g_pGameCVars->g_LogDamage)
					{
						CryLog ("[DAMAGE] %s '%s' is%s meleeing %s '%s' from behind (because %f > %f)",
							pOwnerActor->GetEntity()->GetClass()->GetName(), pOwnerActor->GetEntity()->GetName(), silentHit ? " silently" : "",
							pTargetActor->GetEntity()->GetClass()->GetName(), pTargetActor->GetEntity()->GetName(),
							ownerFacingDot, fromBehindDot);
					}
#endif

					damageScale *= g_pGameCVars->pl_melee.damage_multiplier_from_behind;
				}
			}
		}
		else
		{
			CItem* pItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
			if (pItemTarget)
			{
				if (pItemTarget->GetOwnerActor())
				{
					CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
					if (nwAction)
					{
						CGameRules *pGameRules = g_pGame->GetGameRules();
						if (pGameRules)
						{
							float damage = GetMeleeDamage();
							if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
							{
								float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
								if (dist_xc > 2.0f)
									dist_xc = 2.0f;

								float dist_dmg_coof = damage * 0.05f;
								dist_xc *= 0.3f;
								//bigger distance == more damage;
								damage = damage - dist_dmg_coof / dist_xc;
							}

							if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
							{
								if (pOwnerActor)
								{
									float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
									if (vel_pwr > 5.0f)
										vel_pwr = 5.0f;

									float vel_dmg_coof = damage * 0.32f;
									vel_pwr *= 0.3f;
									//bigger velocity == more damage;
									damage = damage + (vel_dmg_coof * vel_pwr);
								}
							}

							if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
								damage = m_pMeleeParams->meleeparams.damage_ai;

							hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
							HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
								damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
							pGameRules->ClientHit(info);
							num_hitedEntites += 1;
							m_hitedEntites.push_back(pTarget->GetId());
						}
						//add effect in its need
						effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId());
						/*if (effects_from_wpn_added)
						{
							pItemTarget->GetOwnerActor()->SetLastDamagePosAndNormal(pt, normal);
						}*/
						//check block
						if (!nwAction->TestHitBlock(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
						{
							return 1;
						}
						else
						{
							if (pGameRules && pItemTarget->GetOwnerActor())
							{
								EntityId act_trg_id = pItemTarget->GetOwnerActor()->GetEntityId();
								float damage = GetMeleeDamage();
								if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
									damage = m_pMeleeParams->meleeparams.damage_ai;

								if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
								{
									float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
									if (dist_xc > 2.0f)
										dist_xc = 2.0f;

									float dist_dmg_coof = damage * 0.05f;
									dist_xc *= 0.3f;
									//bigger distance == more damage;
									damage = damage - dist_dmg_coof / dist_xc;
								}

								if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
								{
									if (pOwnerActor)
									{
										float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
										if (vel_pwr > 5.0f)
											vel_pwr = 5.0f;

										float vel_dmg_coof = damage * 0.32f;
										vel_pwr *= 0.3f;
										//bigger velocity == more damage;
										damage = damage + (vel_dmg_coof * vel_pwr);
									}
								}

								hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
								HitInfo info(m_pWeapon->GetOwnerId(), act_trg_id, m_pWeapon->GetEntityId(),
									damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
								pGameRules->ClientHit(info);
								num_hitedEntites += 1;
								m_hitedEntites.push_back(act_trg_id);
							}
							return 1;
						}
					}
				}
			}
		}


		// Send target stimuli
		if (!gEnv->bMultiplayer)
		{
			IAISystem *pAISystem = gEnv->pAISystem;
			ITargetTrackManager *pTargetTrackManager = pAISystem ? pAISystem->GetTargetTrackManager() : NULL;
			if (pTargetTrackManager && pOwnerAI)
			{
				IAIObject *pTargetAI = pTarget ? pTarget->GetAI() : NULL;
				if (pTargetAI)
				{
					const tAIObjectID aiOwnerId = pOwnerAI->GetAIObjectID();
					const tAIObjectID aiTargetId = pTargetAI->GetAIObjectID();

					TargetTrackHelpers::SStimulusEvent eventInfo;
					eventInfo.vPos = pt;
					eventInfo.eStimulusType = TargetTrackHelpers::eEST_Generic;
					eventInfo.eTargetThreat = AITHREAT_AGGRESSIVE;
					pTargetTrackManager->HandleStimulusEventForAgent(aiTargetId, aiOwnerId, "MeleeHit",eventInfo);
					pTargetTrackManager->HandleStimulusEventInRange(aiOwnerId, "MeleeHitNear", eventInfo, 5.0f);
				}
			}
		}

		//Check if is a friendly hit, in that case FX and Hit will be skipped
		bool isFriendlyHit = (pOwnerEntity && pTarget) ? IsFriendlyHit(pOwnerEntity, pTarget) : false;
		if (g_pGameCVars->i_enable_frenldly_hits > 0)
			isFriendlyHit = false;

		if(!isFriendlyHit)
		{
			CPlayer * pAttackerPlayer = pOwnerActor->IsPlayer() ? static_cast<CPlayer*>(pOwnerActor) : NULL;
			float damage = m_pMeleeParams->meleeparams.damage_ai;
			if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs > 0)
				damage = GetMeleeDamage();

			if(pOwnerActor->IsPlayer())
			{
				damage = m_slideKick ? m_pMeleeParams->meleeparams.slide_damage : GetMeleeDamage();
			}
			else
			{
				damage = GetMeleeDamage();
			}

#ifndef DEMO_BUILD_RPG_SS
			if (pTargetActor && g_pGameCVars->g_LogDamage)
			{
				CryLog ("[DAMAGE] %s '%s' is%s meleeing %s '%s' applying damage = %.3f x %.3f = %.3f",
					pOwnerActor->GetEntity()->GetClass()->GetName(), pOwnerActor->GetEntity()->GetName(), silentHit ? " silently" : "",
					pTargetActor->GetEntity()->GetClass()->GetName(), pTargetActor->GetEntity()->GetName(),
					damage, damageScale, damage * damageScale);
			}
#endif

			//Generate Hit
			if(pTarget)
			{
				int bl_tp = 0;
				SmartScriptTable props;
				IScriptTable* pScriptTable = pTarget->GetScriptTable();
				if (pScriptTable)
				{
					if (pScriptTable && pScriptTable->GetValue("Properties", props))
					{
						CScriptSetGetChain prop(props);
						prop.GetValue("blood_type", bl_tp);
					}
				}
				CGameRules *pGameRules = g_pGame->GetGameRules();
				CRY_ASSERT_MESSAGE(pGameRules, "No game rules! Melee can not apply hit damage");

				if (pGameRules)
				{
					if (damage <= 0.6f)
					{
						damage = GetMeleeDamage();
					}

					if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
						damage = m_pMeleeParams->meleeparams.damage_ai;

					if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
					{
						float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
						if (dist_xc > 2.0f)
							dist_xc = 2.0f;

						float dist_dmg_coof = damage * 0.05f;
						dist_xc *= 0.3f;
						//bigger distance == more damage;
						damage = damage - dist_dmg_coof / dist_xc;
					}

					if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
					{
						if (pOwnerActor)
						{
							float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
							if (vel_pwr > 5.0f)
								vel_pwr = 5.0f;

							float vel_dmg_coof = damage * 0.32f;
							vel_pwr *= 0.3f;
							//bigger velocity == more damage;
							damage = damage + (vel_dmg_coof * vel_pwr);
						}
					}

					hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
					HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
						damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);

					if (m_pMeleeParams->meleeparams.knockdown_chance>0 && cry_random(0, 99) < m_pMeleeParams->meleeparams.knockdown_chance)
						info.knocksDown = true;

					info.remote = remote;

					pGameRules->ClientHit(info);

					num_hitedEntites += 1;
					m_hitedEntites.push_back(pTarget->GetId());
					if (m_crt_hit_dmg_mult > 1.0f)
					{
						if (pTargetActor)
						{
							AttachDecalToWpn(pt, normal, false, surfaceIdx, bl_tp);
						}
					}
					else
					{
						int rnd_val_chs_1 = cry_random(0, 6);
						if (rnd_val_chs_1 <= 4 && pTargetActor)
						{
							AttachDecalToWpn(pt, normal, false, surfaceIdx, bl_tp);
						}
					}

					m_crt_hit_dmg_mult = 1.0f;

					//CryLogAlways(pCollidedEntity->GetName());
					/*string s1 = pTarget->GetName();
					string s2 = "pGameRules->ClientHit(((";
					string s3 = s2 + s1;
					CryLogAlways(s3.c_str());*/
					
				}

				if (pAttackerPlayer && pAttackerPlayer->IsClient())
				{
					CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
					if (nwAction_x)
					{
						nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, true);
					}
					const Vec3 posOffset = (pt - pTarget->GetWorldPos());
					SMeleeHitParams params;

					params.m_boostedMelee = false;
					params.m_hitNormal = normal;
					params.m_hitOffset = posOffset;
					params.m_surfaceIdx = surfaceIdx;
					params.m_targetId = pTarget->GetId();

					pAttackerPlayer->OnMeleeHit(params);
				}
				else
				{
					PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
				}
				CActor *pActor = static_cast<CActor*>(pTargetActor);
				if (pActor)
				{
					CPlayer * pTargetPlayer = pActor->IsPlayer() ? static_cast<CPlayer*>(pActor) : NULL;
					if (pTargetPlayer)
					{
						pTargetPlayer->OnMeleeDamage();
					}
				}
			}
			else
			{
				bool can_spwn_eff_mt = false;
				//Play Material FX
				if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] * 0.9f) && num_mtl_eff_spwn == 0)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] * 0.7f) && num_mtl_eff_spwn == 1)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] * 0.5f) && num_mtl_eff_spwn == 2)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[m_nextatk] * 0.4f) && num_mtl_eff_spwn == 3)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}

				if (can_spwn_eff_mt)
				{
					CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
					if (nwAction_x)
					{
						if (pAttackerPlayer && pAttackerPlayer->IsClient())
						{
							nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
						}
					}
					PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
				}

				if (g_pGameCVars->g_melee_sys_enable_close_waills_reaction > 0)
				{
					if (CheckCollisionWD(pt, surfaceIdx))
					{
						CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
						if (nwAction_x)
						{
							if (pAttackerPlayer && pAttackerPlayer->IsClient())
							{
								nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
							}
						}
						PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
						return 1;
					}
				}
				//AttachDecalToWpn(pt, normal, false, surfaceIdx);
			}
		}

		if (pTarget)
		{
			CActor *pCTargetActor = static_cast<CActor*>(pTargetActor);
			CPlayer* pTargetPlayer = (pTargetActor && pTargetActor->IsPlayer()) ? static_cast<CPlayer*>(pTargetActor) : NULL;

			if(pTargetPlayer && pTargetPlayer->IsClient())
			{
				if(m_pMeleeParams->meleeparams.trigger_client_reaction)
				{
					pTargetPlayer->TriggerMeleeReaction();
				}
			}
		}
	}

	return hitTypeID;
}

//------------------------------------------------------------------------
/* static */ void CMelee::PlayHitMaterialEffect(const Vec3 &position, const Vec3 &normal, string hitFX, int surfaceIdx)
{
	//if (m_pWeapon->m_sharedparams->params.idle_co_animation.empty())
	//Play Material FX
	const char* meleeFXType = "melee";
	if (/*!(hitFX.size()>2) || */!hitFX.empty())
		meleeFXType = hitFX.c_str();

	IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();
	Vec3 nv_normal = normal;
	if (surfaceIdx == 141)
	{
		if (nv_normal.z < 0.0f)
		{
			nv_normal.z = 0.2f;
		}
	}
	//CryLogAlways("CMelee::PlayHitMaterialEffect surfaceId = %d, normal z = %f", surfaceIdx, nv_normal.z);
	TMFXEffectId effectId = pMaterialEffects->GetEffectId(meleeFXType, surfaceIdx);
	if (effectId != InvalidEffectId)
	{
		SMFXRunTimeEffectParams params;
		params.pos = position;
		params.normal = nv_normal;
		params.playflags = eMFXPF_All | eMFXPF_Disable_Delay;
		//params.soundSemantic = eSoundSemantic_Player_Foley;
		pMaterialEffects->ExecuteEffect(effectId, params);
	}
}

void CMelee::AttachDecalToWpn(const Vec3 &position, const Vec3 &normal, bool bBoostedMelee, int surfaceIdx, int blood_type)
{

	//Function disabled for now
	/*float acc_inf_rps = 0.08f;
	float acc_inf_rps_pos = 0.18f;
	Vec3 dec_pos = position;
	Vec3 dec_nrm = normal;
	dec_nrm.z = cry_random(dec_nrm.z - acc_inf_rps, dec_nrm.z + acc_inf_rps);
	dec_nrm.x = cry_random(dec_nrm.x - acc_inf_rps, dec_nrm.x + acc_inf_rps);
	dec_nrm.y = cry_random(dec_nrm.y - acc_inf_rps, dec_nrm.y + acc_inf_rps);
	dec_pos.z = cry_random(dec_pos.z - acc_inf_rps_pos, dec_pos.z + acc_inf_rps_pos);
	dec_pos.x = cry_random(dec_pos.x - acc_inf_rps_pos, dec_pos.x + acc_inf_rps_pos);
	dec_pos.y = cry_random(dec_nrm.y - acc_inf_rps_pos, dec_pos.y + acc_inf_rps_pos);
	//---------------------decals on weapon----------------
	CryEngineDecalInfo decal;
	decal.vPos = position;
	decal.vNormal = normal;
	decal.fSize = cry_random(0.1f, 0.2f);
	decal.fLifeTime = cry_random(50.0f, 150.0f);
	decal.vHitDirection = -normal;
	decal.bSkipOverlappingTest = true;
	decal.fAngle = cry_random(0.0f, 50.0f);
	//decal.bForceEdge = true;
	decal.bForceSingleOwner = true;
	decal.bAssemble = false;
	decal.fGrowTimeAlpha = 0.4f;
	decal.fGrowTime = 0.4f;
	decal.preventDecalOnGround = true;
	decal.sortPrio = cry_random(1, 26);
	//decal.bDeferred = true;
	cry_strcpy(decal.szMaterialName, "materials/decals/blood/blood_splat1");
	if (m_pWeapon->GetEntity())
	{
		CActor *pActor = m_pWeapon->GetOwnerActor();
		if (pActor)
		{
			IEntityRenderProxy *pRenderProxy = (IEntityRenderProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
			if (pRenderProxy && pActor->IsThirdPerson())
			{
				CryLogAlways("Get Renderproxy for decal(3p)");
				decal.pIStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
				decal.ownerInfo.pRenderNode = pRenderProxy->GetRenderNode();
				decal.ownerInfo.nRenderNodeSlotId = 1;
				gEnv->p3DEngine->CreateDecal(decal);
			}
			else
			{
				IStatObj *pWpnFpObj = NULL;
				if (m_pWeapon->GetEntity()->GetStatObj(0))
				{
					CryLogAlways("CreatingRwndernode 1 seq");
					if (!pRenderProxy)
						return;

					IRenderNode *pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
					if (pNode)
					{
						pNode->CopyIRenderNodeData(pRenderProxy->GetRenderNode());
						pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
						pWpnFpObj->SetFlags(0);
						decal.pIStatObj = pWpnFpObj;
						pNode->SetEntityStatObj(0, pWpnFpObj);
						pNode->SetEntityStatObj(1, NULL);
						pNode->Hide(false);
						pNode->SetRndFlags(ERF_NO_DECALNODE_DECALS, false);
						CryLogAlways("CreatingRwndernode for decal(1p)");
						decal.ownerInfo.pRenderNode = pNode;
						decal.ownerInfo.nRenderNodeSlotId = 0;
						gEnv->p3DEngine->CreateDecal(decal);
					}
				}
				else
				{
					if (pActor->GetEntity()->GetCharacter(0))
					{
						IAttachmentManager* pAttachmentManager = pActor->GetEntity()->GetCharacter(0)->GetIAttachmentManager();
						if (pAttachmentManager)
						{
							IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName("weapon");
							if (pAttachment && pAttachment->GetIAttachmentObject())
							{
								CryLogAlways("CreatingRwndernode 1 seq");
								pWpnFpObj = pAttachment->GetIAttachmentObject()->GetIStatObj();
								if (pWpnFpObj)
								{
									CryLogAlways("CreatingRwndernode 2 seq");
									if (!pRenderProxy)
										return;
									
									IRenderNode *pNode = pRenderProxy->GetRenderNode();
									if (pNode)
									{
										Matrix34A mtx;
										pNode->GetEntityStatObj(0, 0, &mtx);
										CryLogAlways("CreatingRwndernode for decal(1p)");
										pNode->SetEntityStatObj(0, pWpnFpObj, &mtx);
										decal.ownerInfo.pRenderNode = pNode;
										decal.ownerInfo.nRenderNodeSlotId = 0;
										gEnv->p3DEngine->CreateDecal(decal);
									}
								}
							}
						}
					}
				}
			}
		}
	}*/
	//--------------set hit materials for wpn model. dynamic decals on weapon in 1p failure---------------------
	if (m_pWeapon && m_pWeapon->GetEntity())
	{
		CActor *pActor = m_pWeapon->GetOwnerActor();
		if (!pActor)
			return;

		//--------add b_stain_for weapon--------------------------------
		if (g_pGameCVars->g_melee_sys_enable_bl_drop_on_weapons > 0)
		{
			if (blood_type < 1)
				blood_type = 1;

			Vec3 prt_eff_pos(ZERO);
			prt_eff_pos = FindNearestPointInIntersectionLine(position);
			if(prt_eff_pos.IsZero())
				prt_eff_pos = position;

			m_pWeapon->RequestToEnableParticleEffectFromXML(blood_type, true, true, true, cry_random(2.0f*g_pGameCVars->g_melee_sys_time_mult_for_bl_emitter_on_weapons, 5.0f*g_pGameCVars->g_melee_sys_time_mult_for_bl_emitter_on_weapons), prt_eff_pos);
		}
		//--------------------------------------------------------------
		
		if (m_mat_spawned > 0)
			return;

		int slot = 1;
		IStatObj *pWpnFpObj = NULL;
		if (pActor->IsThirdPerson())
		{
			pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(1);
		}
		else
		{
			pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
			slot = 0;
		}

		if (pWpnFpObj)
		{
			int aval_materials_ah[25];
			int num_aval_mats = 0;
			string material_for_h_wp = "";
			for (int i = 0; i < 25; i++)
			{
				if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined") || m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].empty())
				{
					continue;
				}
				else
				{
					aval_materials_ah[num_aval_mats] = i;
					num_aval_mats += 1;
				}
			}

			if (num_aval_mats < 1)
				return;
			else if (num_aval_mats == 1)
				material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[0]].c_str();
			else if (num_aval_mats > 1)
			{
				int mtl_cur = 0;
				mtl_cur = cry_random(0, num_aval_mats - 1);
				material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[mtl_cur]].c_str();
			}

			if (material_for_h_wp.empty())
				return;

			IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(material_for_h_wp.c_str());
			if (!pMtl)
				pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(material_for_h_wp.c_str(), false);

			if (pMtl)
			{
				if (IEntityRenderProxy* renderProxy = static_cast<IEntityRenderProxy*>(m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER)))
				{
					if (IRenderNode* renderNode = renderProxy->GetRenderNode())
					{
						if (pMtl)
						{
							//renderNode->SetRndFlags(ERF_STATIC_INSTANCING, false);
							//renderNode->SetMaterial(pMtl);
							renderProxy->SetSlotMaterial(slot, pMtl);
							m_mat_spawned = 1;
						}

						if (pActor->IsPlayer() && !pActor->IsThirdPerson() && pMtl)
						{
							IAttachment* pAttachment = m_pWeapon->GetItemHandAttachment();
							if (pAttachment && pAttachment->GetType() != 4)
							{
								CCGFAttachment* pCGFAttachment = static_cast<CCGFAttachment*>(pAttachment->GetIAttachmentObject());
								if (pCGFAttachment)
								{
									pCGFAttachment->m_pReplacementMaterial = pMtl;
									m_mat_spawned = 1;
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if (m_pWeapon->GetEntity()->GetCharacter(slot))
			{
				int aval_materials_ah[25];
				int num_aval_mats = 0;
				string material_for_h_wp = "";
				for (int i = 0; i < 25; i++)
				{
					if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined"))
					{
						continue;
					}
					else
					{
						aval_materials_ah[num_aval_mats] = i;
						num_aval_mats += 1;
					}
				}

				if (num_aval_mats < 1)
					return;
				else if (num_aval_mats == 1)
					material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[0]].c_str();
				else if (num_aval_mats > 1)
				{
					int mtl_cur = 0;
					mtl_cur = cry_random(0, num_aval_mats - 1);
					material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[mtl_cur]].c_str();
				}

				if (material_for_h_wp.empty())
					return;

				IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(material_for_h_wp.c_str());
				if (!pMtl)
					pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(material_for_h_wp.c_str(), false);

				if (pMtl)
				{
					/*IEntityRenderProxy *pRenderProxy = (IEntityRenderProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
					if (pRenderProxy)
					{
					pRenderProxy->SetSlotMaterial(slot, pMtl);
					}*/
					m_pWeapon->GetEntity()->GetCharacter(slot)->SetIMaterial_Instance(pMtl);
					m_mat_spawned = 1;
				}
			}
		}
		pWpnFpObj = NULL;
	}
}

bool CMelee::CheckCollisionWD(Vec3 position, int surfaceIdx)
{
	if (surfaceIdx == 141)
	{
		return false;
	}
	float dist = pos_start_nv.GetDistance(position);
	if (m_pMeleeParams->meleeparams.enable_wail_hit_stop[m_nextatk])
	{
		float mx_val = m_pMeleeParams->meleeparams.wail_hit_stop_max_dist[m_nextatk];
		if (mx_val <= 0.0f)
			mx_val = g_pGameCVars->g_melee_sys_wail_max_dist;

		if (dist < mx_val && mx_val > 0.0f)
		{
			m_recoil_ca = m_pMeleeParams->meleeparams.wail_hit_stop_recoil_time[m_nextatk];
			m_nextatkTimer = 0.0f;
			m_delayTimer = 0.0f;
			m_nextatk = 0;
			m_nextatk_counter = 0;
			num_hitedEntites = 1;
			m_crt_hit_dmg_mult = 1.0f;
			m_updtimer = 0.0f;
			m_updtimer2 = 0.0f;
			impulse_to_water_time = 0.0f;
			pos_start_old = Vec3(ZERO);
			pos_end_old = Vec3(ZERO);
			pos_start_nv = Vec3(ZERO);
			pos_end_nv = Vec3(ZERO);
			m_attacking = false;
			const ItemString &meleeAction = "melee_attack_close_hit_on_wail";
			FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
			m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction2(m_pWeapon->GetOwnerId(), meleeAction.c_str());
			}
			//-----------------------------------------------------------------------------
			return true;
		}
	}
	return false;
}

void CMelee::ForcedStopCurrentAttack()
{
	m_nextatkTimer = 0.0f;
	m_delayTimer = 0.0f;
	m_nextatk = 0;
	m_nextatk_counter = 0;
	num_hitedEntites = 1;
	m_crt_hit_dmg_mult = 1.0f;
	m_updtimer = 0.0f;
	m_updtimer2 = 0.0f;
	impulse_to_water_time = 0.0f;
	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);
	const ItemString &meleeAction = "melee_attack_forced_stop";
	FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
	m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	//net sync---------------------------------------------------------------------
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (nwAction)
	{
		nwAction->NetRequestPlayItemAction2(m_pWeapon->GetOwnerId(), meleeAction.c_str());
	}
	//-----------------------------------------------------------------------------
}

void CMelee::EnemyBlockHited(const Vec3 &position, const Vec3 &normal)
{
	//CryLogAlways("CMelee::EnemyBlockHited called, position == x - %f, y - %f, z - %f", position.x, position.y, position.z);
	m_recoil_ca = 0.3f;
	m_nextatkTimer = 0.0f;
	m_delayTimer = 0.0f;
	m_nextatk = 0;
	m_nextatk_counter = 0;
	num_hitedEntites = 1;
	m_crt_hit_dmg_mult = 1.0f;
	m_updtimer = 0.0f;
	m_updtimer2 = 0.0f;
	impulse_to_water_time = 0.0f;
	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);
	m_attacking = false;

	IParticleEffect* pParticleEffect = gEnv->pParticleManager->FindEffect("melee.melee_block_hit.hit_nc_sparks");
	if (pParticleEffect)
	{
		SpawnParams sp;
		sp.bPrime = true;
		sp.bEnableAudio = true;
		sp.audioRtpc = "particlefx";
		sp.occlusionType = eAudioOcclusionType_Ignore;
		IParticleEmitter* pEmitter = pParticleEffect->Spawn(ParticleLoc(position, normal, 1.0f), &sp);
	}
}
//------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////
struct CMelee::DelayedImpulse
{
	DelayedImpulse(CMelee& melee, EntityId collidedEntityId, const Vec3& impulsePos, const Vec3& impulse, int partId, int ipart, int hitTypeID)
		: m_melee(melee)
		, m_collidedEntityId(collidedEntityId)
		, m_impulsePos(impulsePos)
		, m_impulse(impulse)
		, m_partId(partId)
		, m_ipart(ipart)
		, m_hitTypeID(hitTypeID)
	{

	};

	void execute(CItem *pItem)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(m_collidedEntityId);
		IPhysicalEntity* pPhysicalEntity = pEntity ? pEntity->GetPhysics() : NULL;

		if (pPhysicalEntity)
		{
			m_melee.m_collisionHelper.Impulse(pPhysicalEntity, m_impulsePos, m_impulse, m_partId, m_ipart, m_hitTypeID);
		}
	}

private:
	CMelee& m_melee;
	EntityId m_collidedEntityId;
	Vec3 m_impulsePos;
	Vec3 m_impulse;
	int m_partId;
	int m_ipart;
	int m_hitTypeID;
};
//////////////////////////////////////////////////////////////////////////

void CMelee::Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, int hitTypeID, int iPrim)
{
	if (pCollider && m_pMeleeParams->meleeparams.impulse>0.001f)
	{
		CActor* pOwnerActor = m_pWeapon->GetOwnerActor();
		const SPlayerMelee& meleeCVars = g_pGameCVars->pl_melee;
		const SMeleeParams& meleeParams = m_pMeleeParams->meleeparams;
		float impulse = meleeParams.impulse;
		bool aiShooter = pOwnerActor ? !pOwnerActor->IsPlayer() : true;
		bool delayImpulse = false;

		float impulseScale = 1.0f;

		//����� ��������, ������ �� ��� �������������� � �������
		IEntity *pTargetEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
		if (pTargetEnt)
		{
			bool is_brsh = false;
			if (IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC))
			{
				is_brsh = true;
			}

			if (!is_brsh)
			{
				std::vector<EntityId>::const_iterator it = m_pulsedEntites.begin();
				std::vector<EntityId>::const_iterator end = m_pulsedEntites.end();
				int count = m_pulsedEntites.size();

				for (int i = 0; i < count, it != end; i++, ++it)
				{
					if (pTargetEnt->GetId() == (*it))
					{
						return;
					}
				}
				//��������� ������ � ������
				m_pulsedEntites.push_back(pTargetEnt->GetId());
			}
		}
		////////////////////////////////////////////////////////////////
		
		//[kirill] add impulse to phys proxy - to make sure it's applied to cylinder as well (not only skeleton) - so that entity gets pushed
		// if no pEntity - do it old way
		IEntity * pEntity = gEnv->pEntitySystem->GetEntity(collidedEntityId);
		IGameFramework* pGameFramework = g_pGame->GetIGameFramework();
		CActor* pTargetActor = static_cast<CActor*>(pGameFramework->GetIActorSystem()->GetActor(collidedEntityId));
		if (pEntity && pTargetActor)
		{
			//If it's an entity, use the specific impulses if needed, and apply to physics proxy
			if ( meleeCVars.impulses_enable != SPlayerMelee::ei_Disabled )
			{
				impulse = meleeParams.impulse_actor;

				bool aiTarget = !pTargetActor->IsPlayer();

				if (aiShooter && !aiTarget)
				{
					float impulse_ai_to_player = GetImpulseAiToPlayer();
					if (impulse_ai_to_player != -1.f)
					{
						impulse = impulse_ai_to_player;
					}
				}

				//Delay a bit on death actors, when switching from alive to death, impulses don't apply
				//I schedule an impulse here, to get rid off the ugly .lua code which was calculating impulses on its own
				if (pTargetActor->IsDead())
				{
					if (meleeCVars.impulses_enable != SPlayerMelee::ei_OnlyToAlive)
					{
						delayImpulse = true;
						const float actorCustomScale = 1.0f;

						impulseScale *= actorCustomScale;
					}
					else
					{
						impulse = 0.0f;
					}
				}
				else if (meleeCVars.impulses_enable == SPlayerMelee::ei_OnlyToDead)
				{
					// Always allow impulses for melee from AI to local player
					// [*DavidR | 27/Oct/2010] Not sure about this
					if (!(aiShooter && !aiTarget))
						impulse = 0.0f;
				}
				else
				{
					impulse *= 0.01f;
					impulseScale = 1.0f;
				}
			}
			else if (pGameFramework->GetIVehicleSystem()->GetVehicle(collidedEntityId))
			{
				impulse = m_pMeleeParams->meleeparams.impulse_vehicle;
				impulseScale = 1.0f;
			}
		}
		float speed = sqrt_tpl(4000.0f / (80.0f*0.5f));
		if (surfaceIdx == 141)
		{
			impulse = 1.0f;
			impulseScale = 1.0f;
			speed = 0.3f;
		}

		const float fScaledImpulse = impulse * impulseScale;
		if (fScaledImpulse > 0.0f)
		{
			if (!delayImpulse)
			{
				m_collisionHelper.Impulse(pCollider, pt, dir * fScaledImpulse, partId, ipart, hitTypeID);
			}
			else
			{
				//Force up impulse, to make the enemy fly a bit
				Vec3 newDir = (dir.z < 0.0f) ? Vec3(dir.x, dir.y, 0.1f) : dir;
				newDir.Normalize();
				newDir.x *= fScaledImpulse;
				newDir.y *= fScaledImpulse;
				newDir.z *= impulse;

				if (pTargetActor)
				{
					pe_action_impulse imp;
					imp.iApplyTime = 0;
					imp.impulse = newDir;
					//imp.ipart = ipart;
					imp.partid = partId;
					imp.point = pt;
					pTargetActor->GetImpulseHander()->SetOnRagdollPhysicalizedImpulse( imp );
				}
				else
				{
					m_pWeapon->GetScheduler()->TimerAction(100, CSchedulerAction<DelayedImpulse>::Create(DelayedImpulse(*this, collidedEntityId, pt, newDir, partId, ipart, hitTypeID)), true);
				}
			}
		}

		// scar bullet
		// m = 0.0125
		// v = 800
		// energy: 4000
		// in this case the mass of the active collider is a player part
		// so we must solve for v given the same energy as a scar bullet

		if( IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC) )
		{
			speed = 0.003f;
		}

		
		m_collisionHelper.GenerateArtificialCollision(m_pWeapon->GetOwner(), pCollider, pt, normal, dir * speed, partId, ipart, surfaceIdx, iPrim);
	}
}

//-----------------------------------------------------------
bool CMelee::IsFriendlyHit(IEntity* pShooter, IEntity* pTarget)
{
	if(gEnv->bMultiplayer)
	{
		// Only count entity as friendly if friendly fire damage is off
		// and the player is on the same team. This will prevent blood effects.
		if (g_pGame->GetGameRules()->GetFriendlyFireRatio() <= 0.f)
		{
			CActor* pTargetActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
			if (pTargetActor)
			{
				return pTargetActor->IsFriendlyEntity(pShooter->GetId()) != 0;
			}
		}
	}
	else
	{
		IActor* pAITarget = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
		if(pAITarget && pTarget->GetAI() && !pTarget->GetAI()->IsHostile(pShooter->GetAI(),false))
		{
			return true;
		}

		return IsMeleeFilteredOnEntity(*pTarget);
	}

	return false;
}

//-----------------------------------------------------------------------
void CMelee::ApplyMeleeEffects(bool hit)
{
}

void CMelee::ApplyMeleeDamage(const Vec3& point, const Vec3& dir, const Vec3& normal, IPhysicalEntity* physicalEntity,
															EntityId entityID, int partId, int ipart, int surfaceIdx, bool remote, int iPrim)
{
	const float blend = m_slideKick ? 0.4f : SATURATE(m_pMeleeParams->meleeparams.impulse_up_percentage);
	Vec3 impulseDir = Vec3(0, 0, 1) * blend + dir * (1.0f - blend);
	impulseDir.normalize();

	int hitTypeID = Hit(point, dir, normal, physicalEntity, entityID, partId, ipart, surfaceIdx, remote);

	IActor* pActor = gEnv->bMultiplayer && entityID ? g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(entityID) : NULL;
	if(!gEnv->bMultiplayer || !pActor) //MP handles melee impulses to actors in GameRulesClientServer.cpp. See: ClApplyActorMeleeImpulse)
	{
		Impulse(point, impulseDir, normal, physicalEntity, entityID, partId, ipart, surfaceIdx, hitTypeID, iPrim);
	}
}

//---------------------------------------------------------------------
void CMelee::RequestAlignmentToNearestTarget()
{
	if (g_pGameCVars->g_melee_disable_aligment_to_nearest_target > 0)
		return;

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if(pOwner && pOwner->IsClient())
	{
		if (!s_meleeSnapTargetId)
		{
			// If we don't already have an auto-aim target, try and find one.
			if (s_meleeSnapTargetId = GetNearestTarget())
			{
				IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(s_meleeSnapTargetId);
				s_bMeleeSnapTargetCrouched = pTargetActor && static_cast<CPlayer*>(pTargetActor)->GetStance() == STANCE_CROUCH;

				CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);
			}
		}

		if(!s_meleeSnapTargetId || m_netAttacking)
			return;

		if(pOwner && pOwner->IsClient() && m_pMeleeAction && g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.SetViewLimit(pOwner->GetViewRotation().GetColumn1(), 0.01f, 0.01f, 0.01f, 0.01f, SViewLimitParams::eVLS_Item);
		}

		g_pGame->GetAutoAimManager().SetCloseCombatSnapTarget(s_meleeSnapTargetId, 
			g_pGameCVars->pl_melee.melee_snap_end_position_range, 
			g_pGameCVars->pl_melee.melee_snap_move_speed_multiplier);
	}
}

//--------------------------------------------------------
EntityId CMelee::GetNearestTarget()
{
	CActor* pOwnerActor = m_pWeapon->GetOwnerActor();
	if(pOwnerActor == NULL || (gEnv->bMultiplayer && m_slideKick))
		return 0;

	CRY_ASSERT(pOwnerActor->IsClient());

	IMovementController* pMovementController = pOwnerActor->GetMovementController();
	if (!pMovementController)
		return 0;

	SMovementState moveState;
	pMovementController->GetMovementState(moveState);

	const Vec3 playerDir = moveState.aimDirection;
	const Vec3 playerPos = moveState.eyePosition;
	const float range = g_pGameCVars->pl_melee.melee_snap_target_select_range;
	const float angleLimit = cos_tpl(DEG2RAD(g_pGameCVars->pl_melee.melee_snap_angle_limit));

	return m_collisionHelper.GetBestAutoAimTargetForUser(pOwnerActor->GetEntityId(), playerPos, playerDir, range, angleLimit);
}

//-----------------------------------------------------------------------
void CMelee::OnMeleeHitAnimationEvent()
{
	if( IsMeleeWeapon() && m_hitStatus == EHitStatus_HaveHitResult )
	{
		ApplyMeleeDamageHit( m_lastCollisionTest, m_lastRayHit );

		m_hitStatus = EHitStatus_Invalid;

		m_lastRayHit.pCollider->Release();
		m_lastRayHit.pCollider = NULL;
	}
	else
	{
		m_hitStatus= EHitStatus_ReceivedAnimEvent;
	}
}

//-----------------------------------------------------------------------
void CMelee::GetMemoryUsage( ICrySizer * s ) const
{
	s->AddObject(this, sizeof(*this));
}

void CMelee::ApplyMeleeDamageHit( const SCollisionTestParams& collisionParams, const ray_hit& hitResult )
{
	IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(hitResult.pCollider);
	EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

	ApplyMeleeDamage(hitResult.pt, collisionParams.m_dir, hitResult.n, hitResult.pCollider, collidedEntityId,
		hitResult.partid, hitResult.ipart, hitResult.surface_idx, collisionParams.m_remote, hitResult.iPrim);
}

void CMelee::OnSuccesfulHit( const ray_hit& hitResult )
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if( IsMeleeWeapon() && m_hitStatus != EHitStatus_ReceivedAnimEvent && hitResult.pCollider )
	{
		// we defer the MeleeDamage until the MeleeHitEvent is received!
		m_lastCollisionTest = m_collisionHelper.GetCollisionTestParams();
		m_lastRayHit = hitResult;

		m_lastRayHit.pCollider->AddRef();
		m_hitStatus = EHitStatus_HaveHitResult;
	}
	else
	{
		const SCollisionTestParams& collisionParams = m_collisionHelper.GetCollisionTestParams();
		
		ApplyMeleeDamageHit( collisionParams, hitResult );

		m_hitStatus = EHitStatus_Invalid;

		if(m_pMeleeAction)
		{
			if(pOwner)
			{
				m_pMeleeAction->OnHitResult(pOwner, true);
			}
			else
			{
				//Owner has dropped weapon (Likely from being killed) so we can stop and release the action
				m_pMeleeAction->ForceFinish();
				SAFE_RELEASE(m_pMeleeAction);
			}
		}
	}

	if(pOwner && pOwner->IsClient())
	{
		s_meleeSnapTargetId = 0;
		CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

		if(g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);
		}
	}
}

void CMelee::OnFailedHit()
{
	const SCollisionTestParams& collisionParams = m_collisionHelper.GetCollisionTestParams();

	bool collided = PerformCylinderTest(collisionParams.m_pos, collisionParams.m_dir, collisionParams.m_remote);

	CActor* pOwner = m_pWeapon->GetOwnerActor();

	if(pOwner && pOwner->IsClient())
	{
		if(!collided && s_meleeSnapTargetId)
		{
			Vec3 ownerpos = pOwner->GetEntity()->GetWorldPos();
			IEntity* pTarget = gEnv->pEntitySystem->GetEntity(s_meleeSnapTargetId);

			if(pTarget && ownerpos.GetSquaredDistance(pTarget->GetWorldPos()) < sqr(GetRange() * m_pMeleeParams->meleeparams.target_range_mult))
			{
				collided = m_collisionHelper.PerformMeleeOnAutoTarget(s_meleeSnapTargetId);
			}
		}

		s_meleeSnapTargetId = 0;
		CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

		if(g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);
		}
	}

	if(m_pMeleeAction)
	{
		if(pOwner)
		{
			m_pMeleeAction->OnHitResult(pOwner, collided);
		}
		else
		{
			//Owner has dropped weapon (Likely from being killed) so we can stop and release the action
			m_pMeleeAction->ForceFinish();
			SAFE_RELEASE(m_pMeleeAction);
		}
	}
	
	ApplyMeleeEffects(collided);
}

float CMelee::GetRange() const
{
	if (m_slideKick)
		return 2.75f;

	const SMeleeParams& meleeParams = m_pMeleeParams->meleeparams;
	return m_shortRangeAttack ? meleeParams.closeAttack.range : meleeParams.range;
}

bool CMelee::DoSlideMeleeAttack( CActor* pOwnerActor )
{
	if (pOwnerActor && pOwnerActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer* pOwnerPlayer = static_cast<CPlayer*>(pOwnerActor);
		const bool canDoKick = pOwnerPlayer->CanDoSlideKick();
		return canDoKick;
	}
	return false;
}

void CMelee::CloseRangeAttack(bool closeRangeAttack)
{
	m_shortRangeAttack = closeRangeAttack;
}

float CMelee::GetDuration() const
{
	if( IsMeleeWeapon() )
	{
		return m_pWeapon->GetCurrentAnimationTime( eIGS_Owner )*0.001f;
	}
	if (!m_shortRangeAttack)
	{
		return m_pMeleeParams->meleeparams.duration;
	}
	else
	{
		return m_pMeleeParams->meleeparams.closeAttack.duration;
	}
}

float CMelee::GetDelay() const
{
	if (!m_shortRangeAttack)
	{
		CActor* pOwner = m_pWeapon->GetOwnerActor();
		if (pOwner && pOwner->IsPlayer())
			return m_pMeleeParams->meleeparams.delay;
		else
			return m_pMeleeParams->meleeparams.aiDelay;
	}
	else
	{
		return m_pMeleeParams->meleeparams.closeAttack.delay;
	}
}

float CMelee::GetImpulseAiToPlayer() const
{
	return m_shortRangeAttack ? m_pMeleeParams->meleeparams.closeAttack.impulse_ai_to_player : m_pMeleeParams->meleeparams.impulse_ai_to_player;
}

bool CMelee::IsMeleeFilteredOnEntity( const IEntity& targetEntity ) const
{
	static IEntityClass* s_pAnimObjectClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("AnimObject");

	if (targetEntity.GetClass() == s_pAnimObjectClass)
	{
		if (IScriptTable* pEntityScript = targetEntity.GetScriptTable())
		{
			SmartScriptTable properties;
			if (pEntityScript->GetValue("Properties", properties))
			{
				SmartScriptTable physicProperties;
				if (properties->GetValue("Physics", physicProperties))
				{
					bool bulletCollisionEnabled = true;
					return (physicProperties->GetValue("bBulletCollisionEnabled", bulletCollisionEnabled) && !bulletCollisionEnabled);
				}
			}
		}
	}

	return false;
}

bool CMelee::SwitchToMeleeWeaponAndAttack()
{
	// Checking for the equipped MeleeWeapon for each melee is hateful, but there's nowhere to check the inventory when a weapon is equipped.
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if( pOwner && (pOwner->GetInventory()->GetItemByClass( CItem::sWeaponMeleeClass ) != 0) )
	{
		m_pWeapon->SetPlaySelectAction( false );
		pOwner->SelectItemByName( "WeaponMelee", true );

		// trigger the attack - should be good without ptr checks, we validated that the weapon exists in the inventory.
		pOwner->GetCurrentItem()->GetIWeapon()->MeleeAttack();

		return true;
	}

	return false;
}

float CMelee::GetMeleeDamage() const
{
	if (m_nextatk <= 50 && m_pMeleeParams->meleeparams.is_new_melee)
	{

		float dmg1nv = (float)m_pMeleeParams->meleeparams.damage_new[m_nextatk];
		float dmg2nv = (float)m_pMeleeParams->meleeparams.damage;

		if (num_hitedEntites == 0)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
		else if (num_hitedEntites == 1)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
		else if (num_hitedEntites == 2)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
		else if (num_hitedEntites == 3)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
		else if (num_hitedEntites == 4)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
		else if (num_hitedEntites > 4)
			return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;

		return ((dmg2nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
	}
	else
	{
		return m_pMeleeParams->meleeparams.damage;
	}

	return m_pMeleeParams->meleeparams.damage;
}

bool CMelee::StartMultiAnimMeleeAttack(CActor* pActor)
{
	if(pActor)
	{
		m_hitTypeID = CGameRules::EHitType::Melee;

		const IActionController* pActionController = pActor->GetAnimatedCharacter()->GetActionController();
		const CTagDefinition& fragmentIDs = pActionController->GetContext().controllerDef.m_fragmentIDs;
		const SMeleeActions& actions = m_pMeleeParams->meleeactions;

		if(m_pMeleeAction)
		{
			m_pMeleeAction->NetEarlyExit(); //Cancel the old one before we start the new one
		}

		int introID = fragmentIDs.Find(actions.attack_multipart.c_str());
		if(introID != FRAGMENT_ID_INVALID)
		{
			m_pMeleeAction = new CMeleeAction(PP_PlayerActionUrgent, m_pWeapon, introID);
			m_pMeleeAction->AddRef();
			pActor->GetAnimatedCharacter()->GetActionController()->Queue(*m_pMeleeAction);
			m_attackTurnAmount = 0.f;
			return true;
		}
	}

	return false; 
}

float CMelee::GetImpulseStrength()
{
	return m_pMeleeParams->meleeparams.impulse_actor;
}

bool CMelee::GetRecoilSA()
{
	return m_recoil_ca > 0.0f;
}

//////////////////////// CMeleeAction ////////////////////////

CMelee::CMeleeAction::CMeleeAction( int priority, CWeapon* pWeapon, FragmentID fragmentID)
	: BaseClass(priority, fragmentID, TAG_STATE_EMPTY, IAction::NoAutoBlendOut), m_weaponId(pWeapon->GetEntityId()), m_FinalStage(false), m_bCancelled(false)
{
	const SMannequinPlayerParams::Fragments::Smelee_multipart& meleeFragment = PlayerMannequin.fragments.melee_multipart;

	const CTagDefinition* pFragTagDef = meleeFragment.pTagDefinition;
	if(pFragTagDef)
	{
		CTagState fragTagState(*pFragTagDef, m_fragTags);

		pWeapon->SetFragmentTags(fragTagState);

		m_fragTags = fragTagState.GetMask();

		pFragTagDef->Set(m_fragTags, meleeFragment.fragmentTagIDs.into, true);
	}
}

void CMelee::CMeleeAction::Exit()
{
	if(!m_bCancelled)
	{
		StopAttackAction();
	}

	BaseClass::Exit();
}

void CMelee::CMeleeAction::OnHitResult( CActor* pOwnerActor, bool hit )
{
	CRY_ASSERT(pOwnerActor);

	const SMannequinPlayerParams::Fragments::Smelee_multipart& meleeFragment = PlayerMannequin.fragments.melee_multipart;

	const CTagDefinition* pFragTagDef = meleeFragment.pTagDefinition;
	if(pFragTagDef)
	{
		pFragTagDef->Set(m_fragTags, hit ? meleeFragment.fragmentTagIDs.hit : meleeFragment.fragmentTagIDs.miss, true);

		// In TP, force record the tag change so that the FP KillCam replay looks correct.
		if(pOwnerActor->IsThirdPerson() && GetStatus()==Installed)
		{
			if(CRecordingSystem* pRecSys = g_pGame->GetRecordingSystem())
			{
				uint32 optionIdx = GetOptionIdx();
				if (optionIdx == OPTION_IDX_RANDOM)
				{
					SetOptionIdx(GetContext().randGenerator.GenerateUint32());
				}
				pRecSys->OnMannequinRecordHistoryItem( SMannHistoryItem(GetForcedScopeMask(), PlayerMannequin.fragmentIDs.melee_multipart, m_fragTags, GetOptionIdx(), false), GetRootScope().GetActionController(), GetRootScope().GetEntityId() );
			}
		}
	}

	SetFragment(PlayerMannequin.fragmentIDs.melee_multipart, m_fragTags, m_optionIdx, m_userToken, false);

	m_FinalStage = true;
	m_flags &= ~IAction::NoAutoBlendOut;
}

void CMelee::CMeleeAction::NetEarlyExit()
{
	m_eStatus = IAction::Finished;
	m_flags &= ~(IAction::Interruptable | IAction::NoAutoBlendOut);

	StopAttackAction();
	m_bCancelled = true;
}

void CMelee::CMeleeAction::StopAttackAction()
{
	if( CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_weaponId)) )
	{
		CWeapon* pWeapon = static_cast<CWeapon*>(pItem->GetIWeapon());
		CMelee* pMelee = pWeapon->GetMelee();
		if(pMelee)
		{
			StopAttackingAction action(pMelee);
			action.execute(pItem);
		}
	}
}

bool CMelee::PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote, const Vec3 &orig)
{
	string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
	string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
	Vec3 position(0, 0, 0);
	Vec3 positionl(0, 0, 0);
	Vec3 pos323(0, 0, 0);
	IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
	if (pStatObj)
	{
		positionl = pStatObj->GetHelperPos(helperstart.c_str());
		positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
		position = pStatObj->GetHelperPos(helperend.c_str());
		position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
		pos323 = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
	}
	else
	{
		position = m_pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperend.c_str(), true);
		positionl = m_pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperstart.c_str(), true);
		pos323 = position;
	}
	
	if (!orig.IsZeroFast())
		pos323 = orig;

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
	IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200, 0, 0, 128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	if (pIgnore)
	{
		pEnts_to_ignore[0] = pIgnore;
		num_ents_to_ignore += 1;
	}
	
	if (m_pWeapon->GetEntity() && m_pWeapon->GetEntity()->GetPhysics())
	{
		pEnts_to_ignore[1] = m_pWeapon->GetEntity()->GetPhysics();
		num_ents_to_ignore += 1;
	}
	
	if (pOwneract)
	{
		if (pOwneract->GetEquippedState(eAESlot_Shield))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetEquippedState(eAESlot_Torch))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
	}
	num_ents_to_ignore = 3;
	CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwneract->GetEntityId()));
	CItem* pCurrentWeapon = static_cast<CItem*>(pOwneract->GetCurrentItem());
	if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwneract->IsPlayer())
	{
		position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
		positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
		pos323 = position;
	}

	if (pOwneract->GetHealth() <= 0)
		return false;

	ray_hit hit;
	int n;
	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[m_nextatk] / 2), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (g_pGameCVars->r_draw_debug_lines_of_new_melee > 0)
			pAuxGeom->DrawLine(pos, colNormal, pos323, colNormal, 5);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[m_nextatk] / 2), ent_living| ent_sleeping_rigid | ent_rigid | ent_independent,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (g_pGameCVars->r_draw_debug_lines_of_new_melee > 0)
			pAuxGeom->DrawLine(pos, colNormal, pos323, colNormal, 5);
	}

	//WriteLockCond lockColl(*params.plock, 0);
	//lockColl.SetActive(1);

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		if (pCollider)
		{
			IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
			EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
			if (pCollidedEntity)
			{
				if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
				{
					if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
						return false;
				}
				//if (pCollidedEntity->GetPhysics()->GetType() == PE_ROPE)
				//	CryLogAlways("Melee Physics hit to PE_ROPE");

				if (normalized_hit_direction.IsZeroFast())
					normalized_hit_direction = hit.n;

				int hitTypeID = Hit(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
				Impulse(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			}
		}
	}

	PerformRayTest2(pos, dir, 100.0f, false);

	return n>0;
}

bool CMelee::PerformRayTest3(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	if (pIgnore)
	{
		pEnts_to_ignore[0] = pIgnore;
		num_ents_to_ignore += 1;
	}
	
	if (m_pWeapon->GetEntity() && m_pWeapon->GetEntity()->GetPhysics())
	{
		pEnts_to_ignore[1] = m_pWeapon->GetEntity()->GetPhysics();
		num_ents_to_ignore += 1;
	}
	
	if (pOwneract)
	{
		if (pOwneract->GetEquippedState(eAESlot_Shield))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetEquippedState(eAESlot_Torch))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
	}
	num_ents_to_ignore = 3;
	ray_hit hit;
	int n;
	Vec3 nv_dir = dir;
	if (nv_dir.GetLength() > 1.0f)
		nv_dir = dir.normalized();

	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, nv_dir*(m_pMeleeParams->meleeparams.range_new[m_nextatk]), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent | ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable|rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, nv_dir*(m_pMeleeParams->meleeparams.range_new[m_nextatk]), ent_all | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
	}

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

		if (pCollidedEntity)
		{
			if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
			{
				if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
					return false;
			}

			if (normalized_hit_direction.IsZeroFast())
				normalized_hit_direction = hit.n;

			int hitTypeID = Hit(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			Impulse(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
		}
		else
		{
			Hit(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			Impulse(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, 1, hit.iPrim);
		}
	}
	return n>0;
}

void CMelee::StartRay()
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
		string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
		Vec3 position(0, 0, 0);
		Vec3 positionl(0, 0, 0);
		CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
		CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
		if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			//CryLogAlways(helperstart.c_str());
			//CryLogAlways(helperend.c_str());
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
			{
				if (pCurrentWeapon)
				{
					position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend, true);
					positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart, true);
				}
				PerformRayTest(positionl, (position - positionl), 1, false);
				pos_start = positionl;
				pos_end = position - positionl;
				pos_end_nv = position;
			}
			else
			{
				IEntity *pOwner = m_pWeapon->GetOwner();
				IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
				IEntity *pHeldObject = NULL;
				PerformRayTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), 1, false);
				//PerformCylinderTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), false);
				//PerformMelee(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), true);
				pos_start = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
				pos_end = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
				pos_end_nv = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			}

			if (!pos_start_old.IsZero() && !pos_end_nv.IsZero())
			{
				Vec3 nv_pos_center = (pos_end_nv + pos_start_nv) * 0.5000f;
				Vec3 nv_dir_xc_fa = (pos_start_old - nv_pos_center).normalized() * 0.1f;
				nv_pos_center = pos_start_old + (nv_dir_xc_fa * 1.5f);
				normalized_hit_direction = nv_pos_center - pos_start_old;
				normalized_hit_direction.normalize();
			}
			pos_start_nv = pos_start;
			AdditiveRayTestStart();
		}
		else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			int slot = 0;
			if (pAttackingPlayer->IsThirdPerson())
				slot = 1;

			position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
			positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
			PerformRayTest(positionl, (position - positionl), 1, false);
			pos_start = positionl;
			pos_end = position - positionl;
			pos_end_nv = position;
			if (!pos_end_old.IsZero() && !pos_end_nv.IsZero())
			{
				Vec3 nv_pos_center = (pos_end_nv + pos_start_nv) * 0.5000f;
				Vec3 nv_dir_xc_fa = (pos_start_old - nv_pos_center).normalized() * 0.1f;
				nv_pos_center = pos_start_old + (nv_dir_xc_fa * 1.1f);
				normalized_hit_direction = nv_pos_center - pos_start_old;
				normalized_hit_direction.normalize();
			}
			pos_start_nv = pos_start;
			AdditiveRayTestStart();
		}
		else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
		{

		}
	}
}

void CMelee::AdditiveRayTestStart()
{
	if (g_pGameCVars->g_melee_system_adtr_type <= 0)
		return;

	if (pos_start_nv != Vec3(ZERO) && pos_start_old != Vec3(ZERO))
	{
		//CryLogAlways("Additive ray test pre");
		if (pos_start_nv != pos_start_old)
		{
			//CryLogAlways("Additive ray test start");
			float dist_of_2points = 0.0f;
			float dist_of_2points_orig = 0.0f;
			dist_of_2points = pos_end_nv.GetDistance(pos_end_old);
			dist_of_2points_orig = dist_of_2points;
			dist_of_2points = dist_of_2points / g_pGameCVars->g_melee_system_rcdr_coof;
			int num_rays = (int)dist_of_2points;
			if (num_rays >= MAX_ADDITIVE_RAYS)
				num_rays = MAX_ADDITIVE_RAYS;

			if (g_pGameCVars->g_melee_system_adtr_type == 1)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + (pos_start_old - pos_start_nv) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + (pos_end_old - pos_end_nv) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 2)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalized() * dist_of_2points_orig) * mult_vv;	
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalized() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 3)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 4)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 5)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit3 = pos_start_old + (pos_end_old - pos_start_old) * mult_vv;
						Vec3 new_posit4 = pos_start_nv + (pos_end_nv - pos_start_nv) * mult_vv;
						PerformRayTest(new_posit3, (new_posit4 - new_posit3), 1, false, new_posit4);
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 6)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit3 = pos_start_old + (pos_end_old - pos_start_old) * mult_vv;
						Vec3 new_posit4 = pos_start_nv + (pos_end_nv - pos_start_nv) * mult_vv;
						PerformRayTest(new_posit3, (new_posit4 - new_posit3), 1, false, new_posit4);
						Vec3 new_posit1 = pos_start_nv + (pos_start_old - pos_start_nv) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + (pos_end_old - pos_end_nv) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
		}
	}
}

float CMelee::GetOwnerStrength() const
{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (!pActor)
		return 1.0f;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return 1.0f;

	float strength = 1.0f;
	float dmgmult = 0.1f;
	strength = dmgmult;
	strength = strength * (0.2f + 0.4f * 0.018f);
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	float str = 1.0f;
	propsStat->GetValue("strength", str);
	if (str > pActor->GetActorStrength())
	{
		strength *= str;
	}
	else if (str < pActor->GetActorStrength())
	{
		strength *= pActor->GetActorStrength();
	}
	else if (str == pActor->GetActorStrength())
	{
		strength *= pActor->GetActorStrength();
	}
	
	return (0.2f + (strength / 10));
}

bool CMelee::GetWpnCrtAtk()
{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (!pActor)
		return false;

	m_crt_hit_dmg_mult = 1.0f;
	int rnd_value1 = rand() % 100 + 1;

	if (m_pWeapon->GetWeaponType() == 1)
	{
		if (rnd_value1 > 0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_swords_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_swords_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 2)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_axes_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_axes_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 3)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 4)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thswords_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thswords_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 5)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thaxes_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thaxes_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 6)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 7)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_poles_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_poles_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 8)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thpoles_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thpoles_ml;
			return true;
		}
	}
	else
	{
		if (rnd_value1>0 && rnd_value1 < m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}

	return false;
}

bool CMelee::PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	if (!pOwneract)
		return false;

	ray_hit hit;
	int n;
	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[m_nextatk] / 2), ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, NULL, 0);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[m_nextatk] / 2), ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, NULL, 0);
	}
	int hitTypeID = CGameRules::EHitType::Melee;

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
		{
			if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
				return false;
		}

		if (normalized_hit_direction.IsZeroFast())
			normalized_hit_direction = hit.n;

		if (strength < 10)
		{
			if (remote)
			{
				Vec3 ac_dir = dir.normalized();
				Vec3 ac_dir2 = normalized_hit_direction;
				bool can_do_impulse = true;
				if (hit.surface_idx == 141)
				{
					can_do_impulse = false;
					if (m_updtimer >= impulse_to_water_time)
					{
						impulse_to_water_time += 0.2f;
						can_do_impulse = true;
					}
					ac_dir = Vec3(0, 0, -0.3f);
					ac_dir2 = ac_dir;
				}

				if (can_do_impulse)
					Impulse(hit.pt, ac_dir, ac_dir2, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			}
			else
			{
				Hit(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			}
		}
		else
		{
			ray_hit hit2 = hit;
			Hit(hit2.pt, dir, normalized_hit_direction, hit2.pCollider, collidedEntityId, hit2.partid, hit2.ipart, hit2.surface_idx, false);
			if (hit2.pCollider)
			{
				Vec3 ac_dir = dir.normalized();
				Vec3 ac_dir2 = normalized_hit_direction;
				bool can_do_impulse = true;
				if (hit.surface_idx == 141)
				{
					can_do_impulse = false;
					if (m_updtimer >= impulse_to_water_time)
					{
						impulse_to_water_time += 0.2f;
						can_do_impulse = true;
					}
					ac_dir = Vec3(0, 0, -0.3f);
					ac_dir2 = ac_dir;
				}

				if(can_do_impulse)
					Impulse(hit.pt, ac_dir, ac_dir2, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			}
		}
	}

	return n>0;
}

void CMelee::StartRay2(bool dc)
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
		string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
		Vec3 position(0, 0, 0);
		Vec3 positionl(0, 0, 0);
		CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
		CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
		if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			//CryLogAlways(helperstart.c_str());
			//CryLogAlways(helperend.c_str());
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
			{
				position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
				positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
				PerformRayTest2(positionl, (position - positionl), 1, dc);
			}
			else
			{
				IEntity *pOwner = m_pWeapon->GetOwner();
				IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
				IEntity *pHeldObject = NULL;
				PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), 1, dc);
			}
		}
		else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			int slot = 0;
			if (pAttackingPlayer->IsThirdPerson())
				slot = 1;

			position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
			positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
			PerformRayTest2(positionl, (position - positionl), 1, dc);
		}
		else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
		{

		}
	}
}

void CMelee::StartIntersection(void* pUserData, IGameFramework::TimerID handler)
{
	if (m_updtimer > 0.02f)
	{
		//CryLogAlways("start_resur");
		StartRay();
	}
}

void CMelee::StartHitedEntsDmgProcessing()
{
	std::vector<ray_hit>::iterator it_r = s_hit_inf.begin();
	std::vector<ray_hit>::iterator end_r = s_hit_inf.end();
	int count3 = s_hit_inf.size();
	for (int i = 0; i < count3, it_r != end_r; i++, ++it_r)
	{
		IPhysicalEntity *pCollider = it_r->pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (pCollidedEntity)
		{
			int hitTypeID = Hit(it_r->pt, pos_end, it_r->n, it_r->pCollider, collidedEntityId, it_r->partid, it_r->ipart, it_r->surface_idx, false);
			Impulse(it_r->pt, pos_end, it_r->n, it_r->pCollider, collidedEntityId, it_r->partid, it_r->ipart, it_r->surface_idx, hitTypeID, it_r->iPrim);
		}
	}
}

short CMelee::GetNextAttack()
{
	return m_nextatk;
}

Vec3 CMelee::GetAttackHelperPos(int helper_Id)
{
	Vec3 position(ZERO);
	if (!m_pWeapon || !m_pMeleeParams)
		return position;

	string helper = "";
	if(helper_Id == eMIHType_start)
		helper = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
	else if (helper_Id == eMIHType_end)
		helper = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
	else if (helper_Id == eMIHType_alternative)
		helper = m_pMeleeParams->meleeparams.helper_raytest_alt.c_str();

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	CPlayer* pAttackingPlayer = NULL;
	if(pOwner)
		pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));

	CItem* pCurrentWeapon = static_cast<CItem*>(m_pWeapon);
	if (pOwner)
	{
		pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
	}

	IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
	if (pStatObj)
	{
		position = pStatObj->GetHelperPos(helper.c_str());
		position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
		{
			if (pCurrentWeapon)
			{
				position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helper, true);
			}
		}
		else
		{
			position = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		}
	}
	else
	{
		int slot = 0;
		if (pAttackingPlayer->IsThirdPerson())
			slot = 1;

		position = pCurrentWeapon->GetSlotHelperPos(slot, helper.c_str(), true);
	}
	return position;
}

Vec3 CMelee::FindNearestPointInIntersectionLine(Vec3 nrPnt, int acc)
{
	Vec3 prt_eff_pos(ZERO);
	CVectorMathFncs *pVectFncs = g_pGame->GetVectorMathHelpfulFncs();
	if (pVectFncs)
	{
		prt_eff_pos = pVectFncs->FindNearestPointInLine(GetAttackHelperPos(eMIHType_start), GetAttackHelperPos(eMIHType_end), nrPnt, (float)acc);
	}
	return prt_eff_pos;
}

/*void CMelee::CRtest::Run()
{
	SetName("MeleeRtest");
	while (m_threadRunning)
	{
		if (!m_threadRunning)
		{
			break;
		}
		StartIntersect();
		CrySleep(5);
	}
	Stop();
}

void CMelee::CRtest::StartIntersect()
{
	if (!m_threadRunning)
		return; 

	CActor *pPla = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (pPla)
	{
		CPlayer* pAttackingPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (!pAttackingPlayer)
			return;

		if (CWeapon* pWeapon = static_cast<CWeapon*>(pPla->GetCurrentItem()))
		{
			CMelee* pMelee = pWeapon->GetMelee();
			if (pMelee && (pMelee->m_pMeleeParams->meleeparams.is_new_melee && (pMelee->m_updtimer <= (pMelee->m_pMeleeParams->meleeparams.ray_test_time[pMelee->m_nextatk] - pMelee->m_pMeleeParams->meleeparams.ray_test_start_delay[pMelee->m_nextatk]))))
			{
				if (pMelee->m_updtimer <= 0.0f)
					return;

				//pAttackingPlayer->GetEntity()->GetPhysics()->StartStep(0.001f);
				//pAttackingPlayer->GetEntity()->GetPhysics()->DoStep(0.001f);
				//Vec3 positionl2(0, 0, cry_random(0.0001f, -0.0001f));
				//SEntityUpdateContext ctx;
				//ctx.fCurrTime = gEnv->pTimer->GetCurrTime() + cry_random(0.0001f, -0.0001f);
				//ctx.fFrameTime = gEnv->pTimer->GetFrameTime() + cry_random(0.0001f, -0.0001f);

				//pAttackingPlayer->GetEntity()->SetPos(pAttackingPlayer->GetEntity()->GetPos() + positionl2, 5, true, true);
				//pAttackingPlayer->GetAnimatedCharacter()->GetAnimationGraphState()->Hurry();
				//pAttackingPlayer->GetAnimatedCharacter()->GetAnimationGraphState()->ForceTeleportToQueriedState();
				//pAttackingPlayer->GetAnimatedCharacter()->GetAnimationGraphState()->Update();
				//pAttackingPlayer->GetAnimatedCharacter()->Update(ctx, 0);
				//pAttackingPlayer->PrePhysicsUpdate();
				//pAttackingPlayer->GetEntity()->GetCharacter(0)->SetPlaybackScale(0.01f);
				//pAttackingPlayer->GetGameObject()->Pulse('bang');
				//pAttackingPlayer->GetGameObject()->ForceUpdate(true);
				Vec3 position(0, 0, 0);
				Vec3 positionl(0, 0, 0);
				string helperstart = pMelee->m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
				string helperend = pMelee->m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
				int slot_ft = 0;
				if (!pAttackingPlayer->IsThirdPerson() && pMelee->m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					position = pWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
					positionl = pWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
					slot_ft = eIGS_FirstPerson;
				}
				else if (pAttackingPlayer->IsThirdPerson() && pMelee->m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
				{
					position = pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperend.c_str(), false);
					positionl = pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperstart.c_str(), false);
					slot_ft = eIGS_ThirdPerson;
				}
				else if (pMelee->m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
				{

				}
				//pAttackingPlayer->GetEntity()->GetCharacter(0)->SetPlaybackScale(1.0f);
				if (!(pMelee->m_updtimer <= (pMelee->m_pMeleeParams->meleeparams.ray_test_time[pMelee->m_nextatk] - pMelee->m_pMeleeParams->meleeparams.ray_test_start_delay[pMelee->m_nextatk])))
					return;
				
				float time_compensation = 15.0f / gEnv->pTimer->GetFrameRate();
				float time_compensation2 = 1.0f / gEnv->pTimer->GetFrameRate();
				if (gEnv->pTimer->GetFrameRate() < 55.0f)
				{
					positionl.x = positionl.x + cry_random(-time_compensation2, time_compensation2) * 1.0f;
					positionl.y = positionl.y + cry_random(-time_compensation2, time_compensation2) * 1.0f;
					position.x = position.x + cry_random(-time_compensation, time_compensation) * 1.0f;
					position.y = position.y + cry_random(-time_compensation, time_compensation) * 1.0f;
				}
				if (slot_ft == eIGS_ThirdPerson)
				{
					position = pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
					positionl = pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
				}
				pMelee->pos_start = positionl;
				pMelee->pos_end = position - positionl;
				IEntity *pOwner = pMelee->m_pWeapon->GetOwner();
				IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
				ray_hit hit;
				int n;
				n = gEnv->pPhysicalWorld->RayWorldIntersection(pMelee->pos_start, pMelee->pos_end.normalized()*(pMelee->m_pMeleeParams->meleeparams.range_new[pMelee->m_nextatk] / 2), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent,
					rwi_colltype_any | rwi_stop_at_pierceable|rwi_ignore_back_faces, &hit, 1, &pIgnore, pIgnore ? 1 : 0);
				if (n > 0)
				{
					IPhysicalEntity *pCollider = hit.pCollider;
					IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
					EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
					if (pCollidedEntity && pCollidedEntity != pMelee->m_pWeapon->GetEntity())
					{
						std::vector<Vec3>::const_iterator it = pMelee->m_Entites_hit_pos.begin();
						std::vector<Vec3>::const_iterator end = pMelee->m_Entites_hit_pos.end();
						int count = pMelee->m_Entites_hit_pos.size();
						bool add_vector = true;
						bool add_entity = true;
						std::vector<EntityId>::const_iterator it_ent = pMelee->m_Entites_need_to_be_hited.begin();
						std::vector<EntityId>::const_iterator end_ent = pMelee->m_Entites_need_to_be_hited.end();
						int count2 = pMelee->m_Entites_need_to_be_hited.size();
						for (int i = 0; i < count2, it_ent != end_ent; i++, ++it_ent)
						{
							if (collidedEntityId == (*it_ent))
							{
								add_entity = false;
							}
						}

						for (int i = 0; i < count, it != end; i++, ++it)
						{
							if (hit.pt == (*it))
							{
								
							}
						}

						if (add_entity)
						{
							pMelee->s_hit_inf.push_back(hit);
							pMelee->m_Entites_need_to_be_hited.push_back(collidedEntityId);
							
						}
					}
				}
			}
			else
				m_threadRunning = false;
		}
	}
}*/

