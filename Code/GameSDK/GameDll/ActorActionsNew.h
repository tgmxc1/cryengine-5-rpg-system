#ifndef __ACTORACTIONSNEW_H__
#define __ACTORACTIONSNEW_H__

#include <IItemSystem.h>
#include <IWeapon.h>
#include <CryAISystem/IAgent.h>
#include <CryCore/Containers/VectorMap.h>
#include "Item.h"
#include "EntityUtility/EntityEffectsHeat.h"
#include "ScopeReticule.h"
#include "Utility/CryHash.h"
#include <CryAnimation/ICryAnimation.h>
#include "PlayerPlugin_Interaction.h"
#include "ShotDebug.h"
#include "UI/UITypes.h"

class CActorActionsNew
{
public:
	CActorActionsNew();
	virtual	~CActorActionsNew();

	enum ESpecialActions
	{
		eSpec_act_none = 0,
		eSpec_act_relaxed_mode_on,
		eSpec_act_relaxed_mode_off,
		eSpec_act_stealth_mode_on,
		eSpec_act_stealth_mode_off,
		eSpec_act_combat_block_no_shield_hited,
		eSpec_act_combat_block_is_broken_no_shield,
		eSpec_act_combat_hit_to_block_no_shield,
		eSpec_act_combat_block_safe_no_shield,
		eSpec_act_combat_block_with_shield_hited,
		eSpec_act_combat_block_is_broken_with_shield,
		eSpec_act_combat_hit_to_block_with_shield,
		eSpec_act_combat_block_safe_with_shield,
		eSpec_act_combat_push_recoil,
		eSpec_act_combat_recoil,
		eSpec_act_combat_falling,
		eSpec_act_combat_dodge_attack_rq,
		eSpec_act_combat_dodge_attack_lft,
		eSpec_act_combat_dodge_attack_rgt,
		eSpec_act_combat_dodge_attack_bwd,
		eSpec_act_combat_dodge_attack_fwd,
		eSpec_act_combat_push_prerequest,
		eSpec_act_combat_push_state
	};

	enum EPlayerInventorySelectionModes
	{
		ePISMode_none = 0,
		ePISMode_equip,
		ePISMode_drop,
		ePISMode_combine
	};

	enum EActionRecoilMods
	{
		eARMode_none = 0,
		eARMode_recoil_dodge,
		eARMode_recoil_push,
		eARMode_recoil_fly,
		eARMode_recoil_special
	};

	enum EAITargetVisualState
	{
		eAITVS_none = 0,
		eAITVS_have_actor_target,
		eAITVS_have_frendly_actor_target,
		eAITVS_have_simple_entity_target,
		eAITVS_have_danger_entity_target,
		eAITVS_have_obstructed_target,
		eAITVS_have_dead_actor_target,
		eAITVS_full
	};
	void Update(float frametime);
	void StartMeleeBlockAction(EntityId actorId, EntityId itemId);
	void StopMeleeBlockAction(EntityId actorId, EntityId itemId);
	void MeleeBlockReplayAnim(EntityId actorId, EntityId itemId);
	string GetSafeSkinAttachmentPth(EntityId actorId, int slot, string att_name);
	void SetupActorBodyModelPathPart(EntityId actorId, int slot, int part_id, bool clear_att_pth = true);
	void EquipArmorOrCloth(EntityId actorId, EntityId itemId);
	void DeEquipArmorOrCloth(EntityId actorId, EntityId itemId);
	void EquipDecorationItem(EntityId actorId, EntityId itemId, int slot);
	void DeEquipDecorationItem(EntityId actorId, EntityId itemId, int slot);
	void UseItemCommon(EntityId actorId, EntityId itemId);
	void NVcStartSpecialAction(EntityId actorId, EntityId itemId, ESpecialActions special_action, float sa_time = 0.0f);
	void StartFistsMeleeAction(EntityId actorId, EntityId itemId, const char *action, float delay = 0.5f, bool inp_block = true, float pwr = 0.0f, float rec = 0.2f, int num_rays = 5, float range = 0.0f);
	void StartBlockActionAttack(EntityId actorId, EntityId itemId, float delay = 0.5f, bool simple = true, float pwr = 0.0f, float rec = 0.2f, int num_rays = 5, bool attack_pref_left_item = true);
	//Ai Special actions for combat, can used by player or any character-----------------------------------------------
	void StartMeleeWeaponAttack(EntityId actorId, int spc_nv = 0);
	void StartWeaponAttackSimple(EntityId actorId);
	void StopWeaponAttackSimple(EntityId actorId);
	void StartSpellCastSimple(EntityId actorId, int sc_slot = 1, int sc_slot_forced = 1 );//default params
	void StopSpellCastSimple(EntityId actorId, int sc_slot = 1, bool forced = true);//default params
	void SelectAnySpellFromSbToSlotSimple(EntityId actorId, int sc_slot = 1);//default params
	void SwitchToWeaponInInventory(EntityId actorId, const char *item_name);
	bool SwitchToAnyMeleeWeapon(EntityId actorId);
	bool SwitchToAnyWeapon(EntityId actorId);
	bool SwitchToAnyRangeWeapon(EntityId actorId);
	bool SwitchToNoWpn(EntityId actorId);
	bool CheckCurrentWeaponAmmo(EntityId actorId);
	int  CheckCurrentWpnType(EntityId actorId);
	int  IsAITargetHaveAndVisibleOrObstr(EntityId actorId);
	EntityId  GetAITargetFT(EntityId actorId);
	int  GetItemsNumInInvByWpnType(EntityId actorId, bool melee);
	void EquipItemFromInventory(EntityId actorId, const char *item_name);
	void StartWeaponMeleeAttackComplex(EntityId actorId, float atk_time, float atk_delay, float atk_additive_damage, const char *m_action_name);
	void PlaySimpleAnimAction(EntityId actorId, const char *action_name);
	bool CanCastSpell(EntityId actorId, int sc_slot);
	//-------------------------------------------------------------------------------------------------------------
	void StartActionShieldEquipping(EntityId actorId, EntityId itemId);
	void StartActionShieldDeEquipping(EntityId actorId, EntityId itemId);
	void StartActionTorchEquipping(EntityId actorId, EntityId itemId);
	void StartActionTorchDeEquipping(EntityId actorId, EntityId itemId);
	void UpdateItemViewModeForced(EntityId itemId);
	bool TestHitBlock(EntityId T_actorId, EntityId H_actorId, int damage);
	bool TestHitOnActorEquippedItem(EntityId T_actorId, int damage);
	void BreakHitOnActorEquippedItem(EntityId T_actorId, EntityId itemId);
	bool AddSpecEffectFromWpnToTargetIfExist(EntityId T_actorId, EntityId H_actorId, Vec3 pos = Vec3(ZERO), Vec3 nrm = Vec3(ZERO));
	void ForcedDisableBow(EntityId itemId);
	void StartPreLaunchWeapon(EntityId actorId, EntityId itemId);
	void StopPreLaunchWeaponAndLaunchThis(EntityId actorId, EntityId itemId, float pwr = 1.0f);
	void UpdateQuiverAttachment(EntityId actorId, EntityId itemId);
	void SimpleAttachItemToHand(EntityId actorId, EntityId itemId, bool attach = true, int hand = 0);
	bool ActorCanDoAnyAgressiveActions(EntityId actorId, EntityId itemId = 0);
	typedef CryFixedArray<IPhysicalEntity*, 16> PhysIgnLst;
	int GetIgnoredPhyObjects(EntityId actorId, PhysIgnLst& skipList, EntityId itemId = 0);
	//-------------"TravelerNotes" Game functions new
	void ProcessDodge(EntityId actorId, EntityId itemId, float force, Vec3 direction, const char *dir_name_anim_add = "_fwd", bool start_attack = false);
	void ProcessFalling(EntityId actorId, EntityId itemId, float force, Vec3 direction, const char *dir_name_anim_add = "_fwd");
	void ForceStopFalling(EntityId actorId, EntityId itemId, bool start_attack = false);
	void ProcessFallingControlle(EntityId actorId, Vec3 direction);
	void AttackStartOnGroundAfterFalling(EntityId actorId, EntityId itemId);
	bool PreRequestPushToCameraDirection(EntityId actorId);
	bool RequestPushToCameraDirection(EntityId actorId, float force, float recoil, const char *push_action_name);
	void ActionPushToCameraDirectionEnd(EntityId actorId);
	void ActionPushToCameraDirectionCollisionEvent(EntityId actorId);
	void AttackStartAfterDodge(EntityId actorId, const char *dir_name_anim_add = "_none");
	bool ActionSpectacularKillRequest(EntityId actorId);
	//-----------------------------------------------
	//player only
	void OnMeleeHitToSurface(EntityId actorId, int surfaceId, bool isOnTarget);
	//--------------------------------------------------------------------------------
	void NetRequestPlayItemAction(EntityId actorId, int32 tag);
	//--------------------------------------------------------------------------------
	void NetRequestPlayItemAction2(EntityId actorId, string tag);
	//--------------------------------------------------------------------------------
	//player only
	void QuickSlotItemPressed(EntityId id, EntityId actorId);
	//player only
	void RequestReinitActorPhyMassVCItems(EntityId actorId);
	//player only
	void ContextMenuItemPressed(int id);
	//player only
	void InventoryItemPressed(EntityId id, bool wo_inv_update = false);
	//player only
	void InventoryItemProcessEquipItemInLeftSlot(EntityId id);
	//player only
	void InventorySelectItem(int id);
	//player only
	void InventoryDeselectPressed(int id);
	//player only
	void QuickMenuSelectSlot(int id);
	//player only
	void QuickMenuDeselectSlot(int id);
	//player only
	void DialogSysLinePressed(int id);
	//player only
	void UpdateDialog();
	//player only
	void StartDialog(EntityId actorId);
	//player only
	bool TryForDialogAbleTarget();
	//player only
	void LootSelectItem(int id);
	//player only
	void LootDeselectItem(int id);
	//player only
	void LootItemPressed(int id);
	//player only
	void LootTakeAllItems();
	//player only
	void LootLooterSelectItem(int id);
	//player only
	void LootLooterDeselectItem(int id);
	//player only
	void LootLooterItemPressed(int id);
	//player only
	void SellerSelectItem(int id);
	//player only
	void SellerDeselectItem(int id);
	//player only
	void SellerItemPressed(int id);
	//player only
	void BuyerSelectItem(int id);
	//player only
	void BuyerDeselectItem(int id);
	//player only
	void BuyerItemPressed(int id);
	//player only
	void SpellSelected(string name);
	//player only
	void SpellDeselected(string name);
	//player only
	void SpellPressed(string name);
	//player only
	void StartLooting(EntityId targetId);
	//player only
	void StartTraiding(EntityId traiderId);
	//player only
	bool TryForLootAbleTarget();
	//player only
	float GetDistanceToPlayerFromCamera();
	//player only
	float GetDistanceToPlayerFromCameraFCL();
	//player only
	void ReEanableDialogAfterTraidingEnd();
	//player only
	void UpdateInteractiveObjectInfo();
	//player only
	void AbilityPressed(int id);
	//player only
	void ProcessCombineItems(EntityId item_comb_Id_n1, EntityId item_comb_Id_n2);
	//player only
	void InventoryUpdatePlayerDoll(EntityId id);
	//player only
	void InventoryUpdateObjectAttachmentsOnPLdoll(EntityId id);
	//player only
	bool PlayerCanInterInThirdPerson(EntityId id);
	//player only
	bool UpdatePlayerShadowCharacter(EntityId id);
	//player only
	bool PlayerCanDoAnyAgressiveActions();
	//player only
	bool PlayerCanChangeWeapon();
	//player only
	void UpdatePlShadowChr(void* pUserData, IGameFramework::TimerID handler);
	//player only
	void DelayedUpdatePlShadowChr(float time = 0.01f);
	uint32 m_Timer_PL_Shadow_Update;
	//
	bool ActorCanChangeWeapon(EntityId actorId);
	void OnHDReaction(EntityId actorId);
	void ActorCancelAnyAttackAction(EntityId actorId);
	//player only
	bool IsAnyInteractiveUIOpened();
	//player only
	void CloseAnyInteractiveUIOpened();
	//player only
	void CancelAnyAttackAction();
	//player only
	bool SelectAimTarget(float range);
	//player only
	bool DeselectAimTarget();
	//player only
	void UpdateAimTarget();
	//player only
	void PauseAimTargetUpdating(bool pause);
	//player only
	EntityId CurrentInteractiveId;
	//player only
	int Current_inventory_selection_mode;
	//player only
	bool request_for_delayed_update_vis_attachments_on_player;
	//player only
	float frames_for_delayed_update_vis_attachments_on_player;
	//player only
	EntityId current_aim_target_id;
	//player only
	Vec3 current_aim_target_pos;
	//player only
	bool stop_aim_target_update;
	//player only
	EntityId current_player_id;
	//player only
	bool player_shadow_slot_updated;
	//player only
	int pl_view_shake_ids_x = 56;
	float pl_view_shake_timer_x = 0.0f;
	//player only
	bool ChekForInventoryCapacity(EntityId actorId);
	//player only
	int CalculateItemGoldCost(EntityId sellerId, EntityId buyerId, EntityId itemId);
	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};

#endif