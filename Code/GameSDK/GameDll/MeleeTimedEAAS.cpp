/*************************************************************************
Crytek Source File.
Copyright (C), Crytek Studios, 2001-2006.
-------------------------------------------------------------------------
$Id$
$DateTime$

-------------------------------------------------------------------------
History:
- 23:3:2006   13:05 : Created by M�rcio Martins

*************************************************************************/
#include "StdAfx.h"
#include "MeleeTimedEAAS.h"
#include "Melee.h"
#include "Game.h"
#include "Item.h"
#include "Weapon.h"
#include "GameRules.h"
#include "Player.h"
#include "IVehicleSystem.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <CryAction/IMaterialEffects.h>
#include "GameCVars.h"
#include "WeaponSharedParams.h"
#include "GameCodeCoverage/GameCodeCoverageTracker.h"
#include "WeaponMelee.h"

#include "ActorActionsNew.h"

#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>	
#include "ScreenEffects.h"
#include "AutoAimManager.h"
#include <CryAISystem/ITargetTrackManager.h>
#include <CryAISystem/IAIActor.h>

#include "PlayerStateEvents.h"
#include "WeaponSystem.h"
#include "UI/HUD/HUDEventDispatcher.h"

#include "ItemAnimation.h"
#include "RecordingSystem.h"
#include "ICryMannequin.h"

#include "ActorImpulseHandler.h"
#include "PlayerInput.h"

#include "Utility/VectorMathFnc.h"

#if 0 && (defined(USER_claire) || defined(USER_tombe) || defined(USER_johnmichael)|| defined(USER_evgeny)) 
#define MeleeDebugLog(...)				CryLogAlways("[MELEE DEBUG] " __VA_ARGS__)
#else
#define MeleeDebugLog(...)				(void)(0)
#endif

EntityId	CMeleeTimedEAAS::s_meleeSnapTargetId = 0;
float			CMeleeTimedEAAS::s_fNextAttack = 0.0f;
bool			CMeleeTimedEAAS::s_bMeleeSnapTargetCrouched = false;


CRY_IMPLEMENT_GTI_BASE(CMeleeTimedEAAS);


//------------------------------------------------------------------------
CMeleeTimedEAAS::CMeleeTimedEAAS()
: m_pWeapon(NULL)
, m_pMeleeParams(NULL)
, m_attacking(false)
, m_attacked(false)
, m_netAttacking(false)
, m_slideKick(false)
, m_delayTimer(0.0f)
, m_useMeleeWeaponDelay(-1.0f)
, m_shortRangeAttack(false)
, m_hitTypeID(0)
, m_hitStatus(EHitStatus_Invalid)
, m_pMeleeAction(NULL)
, m_attackTurnAmount(0.f)
, m_attackTurnAmountSmoothRate(0.f)
, m_attackTime(0.f)
, m_piActionController(NULL)

//-------------------------------------
, m_meleeScale(1.0f)
, m_hold_timer(0.0f)
, m_hold_time_to_h(0.0f)
, m_hold_started_charge(false)
, num_mtl_eff_spwn(0)
, m_atkn(false)
, m_pulling(false)
, m_atking(false)
, m_holdedf(false)
, m_holdedb(false)
, m_holdedl(false)
, m_holdedr(false)
, m_updtimer(0.0f)
, m_recoil_ca(0.0f)
, num_hitedEntites(1)
, m_crt_hit_dmg_mult(1.0f)
, m_updtimer2(0.0f)
, m_durationTimer(0.0f)
, pos_initial(Vec3(ZERO))
, pos_start_old(Vec3(ZERO))
, pos_end_old(Vec3(ZERO))
, pos_start_nv(Vec3(ZERO))
, pos_end_nv(Vec3(ZERO))
, normalized_hit_direction(Vec3(ZERO))
//----------------------------------------

{
	m_collisionHelper.SetUser(this);

	m_lastRayHit.pCollider = NULL;
}

void CMeleeTimedEAAS::Release()
{
	if (g_pGame)
	{
		//g_pGame->GetWeaponSystem()->DestroyMeleeMode(this);
	}
	if (m_lastRayHit.pCollider)
	{
		m_lastRayHit.pCollider->Release();
	}
}

//------------------------------------------------------------------------
CMeleeTimedEAAS::~CMeleeTimedEAAS()
{
	m_pMeleeParams = NULL;
	SAFE_RELEASE(m_pMeleeAction);
}

void CMeleeTimedEAAS::InitMeleeMode(CWeapon* pWeapon, const SMeleeTimedModeParams* pParams)
{
	CRY_ASSERT_MESSAGE(pParams, "Melee Timed Mode Params NULL! Have you set up the weapon xml correctly?");

	m_pWeapon = pWeapon;
	m_pMeleeParams = pParams;

	if (gEnv->p3DEngine && gEnv->p3DEngine->GetMaterialManager())
	{
		if (m_pMeleeParams)
		{
			for (int i = 0; i < 25; i++)
			{
				if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined") || m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].empty())
				{
					continue;
				}
				else
				{
					IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].c_str());
					if (!pMtl)
						pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i].c_str(), false);

					IStatObj *pWpnFpObj = NULL;
					if (m_pWeapon && m_pWeapon->GetEntity())
					{
						pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(1);
						if (pWpnFpObj)
						{
							pMtl->PrecacheMaterial(1.0f, pWpnFpObj->GetRenderMesh(), false);
						}
						pWpnFpObj = NULL;
						pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
						if (pWpnFpObj)
						{
							pMtl->PrecacheMaterial(1.0f, pWpnFpObj->GetRenderMesh(), false);
						}
						pWpnFpObj = NULL;
					}
					pMtl = NULL;
				}
			}
		}
	}
}

void CMeleeTimedEAAS::InitFragmentData()
{
	if (m_pWeapon && m_pWeapon->GetOwnerActor() && m_pWeapon->GetOwnerActor()->GetAnimatedCharacter())
	{
		m_piActionController = m_pWeapon->GetOwnerActor()->GetAnimatedCharacter()->GetActionController();

		assert(m_pWeapon->GetOwnerActor()->IsPlayer()); // If this is not the case, we cannot use PlayerMannequin!

		const CTagDefinition* pTagDefinition = m_piActionController->GetTagDefinition(PlayerMannequin.fragmentIDs.melee_weapon);
		if (pTagDefinition)
		{
			m_pMeleeParams->meleetags.Resolve(pTagDefinition, PlayerMannequin.fragmentIDs.melee_weapon, m_piActionController);
		}
		else
		{
			CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_WARNING, "CMeleeTimedEAAS: tagdefs for melee_weapon fragment missing for the player.");
		}
	}
}

//------------------------------------------------------------------------
void CMeleeTimedEAAS::Update(float frameTime, uint32 frameId)
{
	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);
	if (g_pGameCVars->g_enable_fl_limit_for_melee_system > 0)
	{
		if (frameTime > g_pGameCVars->g_ft_limit_val_for_melee)
			frameTime = g_pGameCVars->g_ft_limit_val_for_melee;
	}
	bool remote = false;
	bool requireUpdate = false;
	CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	if (!pOwner)
	{
		num_hitedEntites = 1;
		m_crt_hit_dmg_mult = 1.0f;
		m_updtimer = 0.0f;
		m_updtimer2 = 0.0f;
		m_updtimer3 = 0.0f;
		m_recoil_ca = 0.0f;
		pos_start_old = Vec3(ZERO);
		pos_end_old = Vec3(ZERO);
		pos_start_nv = Vec3(ZERO);
		pos_end_nv = Vec3(ZERO);
		m_hold_time_to_h = 0.0f;
		num_mtl_eff_spwn = 0;
		pos_initial = Vec3(ZERO);
		return;
	}

	if (pOwner->GetHealth() <= 0)
	{
		num_hitedEntites = 1;
		m_crt_hit_dmg_mult = 1.0f;
		m_updtimer = 0.0f;
		m_updtimer2 = 0.0f;
		m_updtimer3 = 0.0f;
		m_recoil_ca = 0.0f;
		pos_start_old = Vec3(ZERO);
		pos_end_old = Vec3(ZERO);
		pos_start_nv = Vec3(ZERO);
		pos_end_nv = Vec3(ZERO);
		m_hold_time_to_h = 0.0f;
		num_mtl_eff_spwn = 0;
		pos_initial = Vec3(ZERO);
		return;
	}

	if (m_recoil_ca > 0.0f)
	{
		requireUpdate = true;
		m_recoil_ca -= frameTime;
		if (m_recoil_ca <= 0.0f)
			m_recoil_ca = 0.0f;
	}

	if (!pOwner || pOwner->GetHealth() <= 0)
		return;

	CPlayer *pPlayer = (CPlayer *)pOwner;
	if (m_hold_started_charge)
	{
		if (m_hold_time_to_h < 2.5f)
			m_hold_time_to_h += frameTime;
	}

	if (m_updtimer>m_pMeleeParams->meleeparams.ray_test_end_delay[GetAttackId()])
	{
		m_updtimer -= frameTime;
		if (m_updtimer <= m_pMeleeParams->meleeparams.ray_test_end_delay[GetAttackId()])
		{
			m_updtimer = 0.0f;
			m_hold_time_to_h = 0.0f;
			m_hold_started_charge = false;
			num_mtl_eff_spwn = 0;
			m_updtimer3 = 0.0f;
		}
	}

	//CryLogAlways("start_upd1st");

	if (m_updtimer>m_pMeleeParams->meleeparams.ray_test_end_delay[GetAttackId()])
	{
		if ((m_updtimer <= (m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] - m_pMeleeParams->meleeparams.ray_test_start_delay[GetAttackId()])))
		{
			pos_start_old = pos_start_nv;
			pos_end_old = pos_end_nv;

			if (pos_initial.IsZero())
				pos_initial = pos_start_old;

			if(m_pMeleeParams->meleeparams.phy_test_type[GetAttackId()] == 1)
				StartRay();

			if (g_pGameCVars->g_melee_system_clear_hit_targets_after_time > 0)
			{
				m_updtimer3 += frameTime;
				if (m_updtimer3 >= g_pGameCVars->g_melee_system_time_to_clear_hit_targets)
				{
					if (m_pMeleeParams->meleeparams.hited_targets_clear_at[GetAttackId()])
					{
						m_hitedEntites.clear();
					}
					m_updtimer3 = 0.0f;
				}
			}

			m_updtimer2 -= frameTime;
			if (m_updtimer2 <= 0.0f)
			{
				m_updtimer2 = m_pMeleeParams->meleeparams.ray_test_upd;
				if (m_pMeleeParams->meleeparams.phy_test_type[GetAttackId()] == 1)
				{
					StartRay2(true);
					StartRay2(false);
				}
			}
			if (m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] * 0.6f >= m_updtimer && !m_gtcb)
			{
				if (CActor *pActor = m_pWeapon->GetOwnerActor())
				{
					if (IMovementController * pMC = pActor->GetMovementController())
					{
						SMovementState info;
						pMC->GetMovementState(info);
						if (g_pGameCVars->g_enable_melee_center_st_rt > 0 || m_pMeleeParams->meleeparams.phy_test_type[GetAttackId()] == 0)
						{
							pos_start_nv = info.weaponPosition;
							PerformMelee(info.weaponPosition, info.fireDirection, remote);
							PerformRayTest3(info.weaponPosition, info.fireDirection, 1, remote);
						}
					}
				}
				m_gtcb = true;
			}
		}

	}


	if (m_durationTimer>0)
	{

	}
	else
	{

	}


	if (m_delayTimer>0.07)
	{

	}
	else if (m_delayTimer >= 0.01f && m_delayTimer <= 0.07f)
	{

	}
	else if (m_delayTimer <= 0)
	{

	}

	if (m_attacking)
	{
		requireUpdate = true;

		if (m_hold_timer>0.0f)
		{
			m_hold_timer -= frameTime;
			if (m_hold_timer <= 0.0f)
				m_hold_timer = 0.0f;
		}

		if (/*!m_pulling && !m_atking && !m_atkn*/m_atkn)
		{
			

			if (m_hold_timer <= 0.0f && m_atkn)
			{
				StopAttack();
			}
		}

		

		if (m_delayTimer>0.0f)
		{
			m_delayTimer -= frameTime;
			if (m_delayTimer <= 0.0f)
				m_delayTimer = 0.0f;
		}
		else
		{
			if (!m_attacked)
			{


			}
		}
		if (m_atking && m_atk_time <= 0.0f)
		{

		}
		else if (m_atkn && m_atk_time <= 0.0f)
		{
			//m_pWeapon->SetBusy(false);
			/*m_attacking = false;
			m_atking = false;
			m_atkn = false;*/
		}

		m_atk_time -= frameTime;
		if (m_atk_time<0.0f)
			m_atk_time = 0.0f;
	}

	if (requireUpdate)
		m_pWeapon->RequireUpdate(eIUS_FireMode);
	
}

//------------------------------------------------------------------------
void CMeleeTimedEAAS::Activate(bool activate)
{
	MeleeDebugLog("CMeleeTimedEAAS<%p> Activate(%s)", this, activate ? "true" : "false");

	if (!IsMeleeWeapon())
	{
		m_attacking = false;
		m_slideKick = false;
		m_shortRangeAttack = false;
		m_delayTimer = 0.0f;
	}

	if (!activate && m_pMeleeAction)
	{
		m_pMeleeAction->ForceFinish();
	}

	//***************************
	m_hold_timer = 0.0f;

	m_atkn = false;
	m_pulling = false;
	m_atking = false;
	m_attacking = false;
	m_delayTimer = 0.0f;
	m_durationTimer = 0.0f;

	//******************************
}

//------------------------------------------------------------------------
bool CMeleeTimedEAAS::CanAttack() const
{
	if (!m_attacking && m_delayTimer <= 0 && m_durationTimer <= 0)
	{
		/*if (gEnv->bMultiplayer)
		{
			if (CPlayer * pPlayer = static_cast<CPlayer*>(m_pWeapon->GetOwnerActor()))
			{
				if (pPlayer->IsClient() && gEnv->pTimer->GetFrameStartTime().GetSeconds() > CMeleeTimedEAAS::s_fNextAttack && !pPlayer->GetStealthKill().IsBusy())
				{
					return true;
				}
			}

			return false;
		}
		else
		{*/
			if (m_pWeapon->GetMelee())
			{
				if (!m_pWeapon->GetMelee()->GetRecoilSA())
					return true;
				else
					return false;
			}
			return true;
		//}
	}
	else
	{
		return false;
	}
}

//------------------------------------------------------------------------
struct CMeleeTimedEAAS::StartAttackingAction
{
	StartAttackingAction(CMeleeTimedEAAS *_meleetimedeaas) : pmeleetimedeaas(_meleetimedeaas) {};
	CMeleeTimedEAAS *pmeleetimedeaas;

	void execute(CItem *_this)
	{
		pmeleetimedeaas->m_pulling = false;
		CActor* pOwner = static_cast<CActor *>(_this->GetOwnerActor());
		CPlayer *pPlayer = (CPlayer *)pOwner;
		CPlayerInput *pPlayerInput = reinterpret_cast<CPlayerInput *>(pPlayer->GetPlayerInput());
		if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Forward)
		{
			const ItemString &meleeAction = pmeleetimedeaas->m_pMeleeParams->meleeactions.attack_hold[1];
			FragmentID fragmentId = pmeleetimedeaas->m_pWeapon->GetFragmentID(meleeAction.c_str());
			pmeleetimedeaas->m_pWeapon->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(pmeleetimedeaas->m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------

			pmeleetimedeaas->m_holdedf = true;
			pmeleetimedeaas->m_holdedb = false;
			pmeleetimedeaas->m_holdedl = false;
			pmeleetimedeaas->m_holdedr = false;
		}
		else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Back)
		{
			const ItemString &meleeAction = pmeleetimedeaas->m_pMeleeParams->meleeactions.attack_hold[2];
			FragmentID fragmentId = pmeleetimedeaas->m_pWeapon->GetFragmentID(meleeAction.c_str());
			pmeleetimedeaas->m_pWeapon->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(pmeleetimedeaas->m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------

			pmeleetimedeaas->m_holdedf = false;
			pmeleetimedeaas->m_holdedb = true;
			pmeleetimedeaas->m_holdedl = false;
			pmeleetimedeaas->m_holdedr = false;
		}
		else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Left)
		{
			const ItemString &meleeAction = pmeleetimedeaas->m_pMeleeParams->meleeactions.attack_hold[3];
			FragmentID fragmentId = pmeleetimedeaas->m_pWeapon->GetFragmentID(meleeAction.c_str());
			pmeleetimedeaas->m_pWeapon->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(pmeleetimedeaas->m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------

			pmeleetimedeaas->m_holdedf = false;
			pmeleetimedeaas->m_holdedb = false;
			pmeleetimedeaas->m_holdedl = true;
			pmeleetimedeaas->m_holdedr = false;
		}
		else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Right)
		{
			const ItemString &meleeAction = pmeleetimedeaas->m_pMeleeParams->meleeactions.attack_hold[4];
			FragmentID fragmentId = pmeleetimedeaas->m_pWeapon->GetFragmentID(meleeAction.c_str());
			pmeleetimedeaas->m_pWeapon->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(pmeleetimedeaas->m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------

			pmeleetimedeaas->m_holdedf = false;
			pmeleetimedeaas->m_holdedb = false;
			pmeleetimedeaas->m_holdedl = false;
			pmeleetimedeaas->m_holdedr = true;
		}
		else
		{
			const ItemString &meleeAction = pmeleetimedeaas->m_pMeleeParams->meleeactions.attack_hold[5];
			FragmentID fragmentId = pmeleetimedeaas->m_pWeapon->GetFragmentID(meleeAction.c_str());
			pmeleetimedeaas->m_pWeapon->PlayAction(fragmentId, 0, true, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(pmeleetimedeaas->m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------

			pmeleetimedeaas->m_holdedf = false;
			pmeleetimedeaas->m_holdedb = false;
			pmeleetimedeaas->m_holdedl = false;
			pmeleetimedeaas->m_holdedr = false;
		}

//fix-----------------------------------------------------------------------------------------------
		/*if (pOwner)
		{
			pOwner->SetTag(PlayerMannequin.tagIDs.throwing, true);

			if (pOwner->IsClient())
			{
				pOwner->LockInteractor(pmeleetimedeaas->m_pWeapon->GetEntityId(), true);
			}
		}
		pmeleetimedeaas->m_pWeapon->SetItemFlag(CItem::eIF_BlockActions, true);*/
//--------------------------------------------------------------------------------------------------
		pmeleetimedeaas->m_hold_started_charge = true;
	}
};

//-------------------------------------------------------------------
struct CMeleeTimedEAAS::StopAttackingAction
{
	CMeleeTimedEAAS *_this;
	StopAttackingAction(CMeleeTimedEAAS *melee) : _this(melee) {};
	void execute(CItem *pItem)
	{
		_this->m_attacking = false;
		_this->m_slideKick = false;
		_this->m_netAttacking = false;

		_this->m_delayTimer = 0.0f;
		_this->m_atkn = false;
		_this->m_attacking = false;
		_this->m_attacked = false;
		_this->m_atking = false;

		_this->m_delayTimer = 0.0f;
		_this->m_durationTimer = 0.0f;
		_this->m_hold_timer = 0.0f;
		_this->m_atk_time = 0.0f;
		pItem->SetBusy(false);
		pItem->ForcePendingActions();
		MeleeDebugLog("CMeleeTimedEAAS<%p> StopAttackingAction is being executed!", _this);

		CActor* pActor(NULL);
		if (!gEnv->bMultiplayer)
		{
			if (IEntity* owner = pItem->GetOwner())
			if (IAIObject* aiObject = owner->GetAI())
			if (IAIActor* aiActor = aiObject->CastToIAIActor())
				aiActor->SetSignal(0, "OnMeleePerformed");
		}
		else if (g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn && s_meleeSnapTargetId && (pActor = pItem->GetOwnerActor()) && pActor->IsClient())
		{
			CActor* pOwnerActor = pItem->GetOwnerActor();
			pOwnerActor->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);

			s_meleeSnapTargetId = 0;
			CHANGED_NETWORK_STATE(pOwnerActor, CPlayer::ASPECT_SNAP_TARGET);
		}

		if (_this->m_pMeleeAction)
		{
			SAFE_RELEASE(_this->m_pMeleeAction);
		} 
		
		
	}
};

SMeleeTimedTags::SMeleeFragData CMeleeTimedEAAS::GenerateFragmentData(const SMeleeTimedTags::TTagParamsContainer& tagContainer) const
{
	const size_t numTags = tagContainer.size();
	if (numTags > 0)
	{
		const size_t tagIndex = cry_random((size_t)0, numTags - 1);
		const SMeleeTimedTags::STagParams& meleeTags = tagContainer[tagIndex];

		SMeleeTimedTags::SMeleeFragData fragData(tagIndex, meleeTags);

		return fragData;
	}

	return SMeleeTimedTags::SMeleeFragData();
}

void CMeleeTimedEAAS::GenerateAndQueueMeleeAction() const
{
	const SMeleeTimedTags& tags = m_pMeleeParams->meleetags;
	const CWeaponMelee::EMeleeStatus meleeStatus = static_cast<CWeaponMelee*> (m_pWeapon)->GetMeleeAttackAction();

	const char* pActionName = NULL;

	switch (meleeStatus)
	{
	case CWeaponMelee::EMeleeStatus_Left:
		GenerateAndQueueMeleeActionForStatus(tags.tag_params_combo_left);
		pActionName = "MeleeCombo";
		break;
	case CWeaponMelee::EMeleeStatus_Right:
		GenerateAndQueueMeleeActionForStatus(tags.tag_params_combo_right);
		pActionName = "MeleeCombo";
		break;
	case CWeaponMelee::EMeleeStatus_KillingBlow:
		GenerateAndQueueMeleeActionForStatus(tags.tag_params_combo_killingblow);
		pActionName = "MeleeKillingBlow";
		break;
	default:
		CryLog("[Melee] Attempted to run a melee action when the status was unknown");
		break;
	}
}

void CMeleeTimedEAAS::GenerateAndQueueMeleeActionForStatus(const SMeleeTimedTags::TTagParamsContainer& tagContainer) const
{
	SMeleeTimedTags::SMeleeFragData fragData = GenerateFragmentData(tagContainer);
	if (fragData.m_pMeleeTags)
	{
		m_hitTypeID = fragData.m_pMeleeTags->hitType;

		CItemAction *pAction = new CItemAction(PP_PlayerAction, PlayerMannequin.fragmentIDs.melee_weapon, fragData.m_pMeleeTags->tagState);
		m_pWeapon->PlayFragment(pAction);
	}
}

const ItemString &CMeleeTimedEAAS::SelectMeleeAction() const
{
	CRY_ASSERT_MESSAGE(!IsMeleeWeapon(), "SelectMeleeAction is deprecated for the melee weapon!");

	const SMeleeTimedActions& actions = m_pMeleeParams->meleeactions;

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		if (!IsMeleeWeapon())
		{
			m_hitTypeID = CGameRules::EHitType::Melee;

			if (m_shortRangeAttack && !actions.attack_closeRange.empty())
			{
				return actions.attack_closeRange;
			}
		}
	}

	return actions.attack;
}

void CMeleeTimedEAAS::StartAttack()
{
	bool canAttack = CanAttack();

	MeleeDebugLog("CMeleeTimedEAAS<%p> StartAttack canAttack=%s", this, canAttack ? "true" : "false");

	if (!canAttack)
		return;

	if (m_recoil_ca > 0.0f)
		return;

	if (!m_attacking && !m_atking && !m_pulling)
	{
		CActor* pOwner = m_pWeapon->GetOwnerActor();
		if (!pOwner)
			return;

		if (pOwner->m_blockactiveshield || pOwner->m_blockactivenoshield)
			return;

		m_attacking = true;
		m_pulling = true;
		m_atking = false;
		m_atkn = false;
		m_attacked = false;
		m_gtcb = false;
		m_pWeapon->SetBusy(true);
		pos_start_old = Vec3(ZERO);
		pos_end_old = Vec3(ZERO);
		pos_start_nv = Vec3(ZERO);
		pos_end_nv = Vec3(ZERO);
		m_hold_time_to_h = 0.0f;
		m_hold_started_charge = false;
		num_mtl_eff_spwn = 0;
		
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_hold[0];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------

		bool fp = false;
		if (pOwner && !pOwner->IsThirdPerson())
			fp = true;

		if (fp)
			m_pWeapon->GetScheduler()->TimerAction(0/*m_pWeapon->GetCurrentAnimationTime(eIGS_Owner)*/, CSchedulerAction<StartAttackingAction>::Create(this), false);
		else
			m_pWeapon->GetScheduler()->TimerAction(0, CSchedulerAction<StartAttackingAction>::Create(this), false);
		//m_pWeapon->GetScheduler()->ScheduleAction(CSchedulerAction<StartAttackingAction>::Create(this), true);
		//m_pWeapon->SetDefaultIdleAnimation(CItem::eIGS_FirstPerson, m_meleeactions.hold);
		m_hold_timer = m_pMeleeParams->meleeparams.hold_duration;
		m_pWeapon->RequestStartMeleeAttack(false,false);
		m_pWeapon->RequireUpdate(eIUS_FireMode);
	}
}

//------------------------------------------------------------------------
void CMeleeTimedEAAS::NetAttack()
{
	MeleeDebugLog("CMeleeTimedEAAS<%p> NetAttack", this);

	const ItemString &meleeAction = SelectMeleeAction();

	m_pWeapon->OnMeleeTimed(m_pWeapon->GetOwnerId());

	CActor* pOwner = m_pWeapon->GetOwnerActor();

	if (!DoSlideMeleeAttack(pOwner))
	{
		if (!gEnv->bMultiplayer || !g_pGameCVars->pl_melee.mp_melee_system || !StartMultiAnimMeleeAttack(pOwner))
		{
			FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
			m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		}
	}
	else
	{
		CRY_ASSERT(pOwner && (pOwner->GetActorClass() == CPlayer::GetActorClassType()));

		if (pOwner->IsPlayer())
		{
			//static_cast<CPlayer*> (pOwner)->StateMachineHandleEventMovement(SStateEventSlideKick(this));
		}
	}

	m_attacking = true;
	m_slideKick = false;
	m_netAttacking = true;
	m_delayTimer = GetDelay();

	m_pWeapon->SetBusy(true);
	m_pWeapon->RequireUpdate(eIUS_FireMode);

	CPlayer *pOwnerPlayer = m_pWeapon->GetOwnerPlayer();
	if (pOwnerPlayer)
	{
		if (pOwnerPlayer->IsThirdPerson())
		{
			CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_3PSignalId, pOwnerPlayer->GetEntityId());
		}
		else
		{
			CAudioSignalPlayer::JustPlay(m_pMeleeParams->meleeparams.m_FPSignalId, pOwnerPlayer->GetEntityId());
		}
	}
}

//------------------------------------------------------------------------
int CMeleeTimedEAAS::GetDamage() const
{

	return m_pMeleeParams->meleeparams.damage;
}

//-----------------------------------------------------------------------
void CMeleeTimedEAAS::PerformMelee(const Vec3 &pos, const Vec3 &dir, bool remote)
{
	CCCPOINT_IF(!remote, Melee_PerformLocal);
	CCCPOINT_IF(remote, Melee_PerformRemote);

	MeleeDebugLog("CMeleeTimedEAAS<%p> PerformMelee(remote=%s)", this, remote ? "true" : "false");

#if !defined(DEMO_BUILD_RPG_SS)
	if (g_pGameCVars->pl_melee.debug_gfx)
	{
		IPersistantDebug  *pDebug = g_pGame->GetIGameFramework()->GetIPersistantDebug();
		pDebug->Begin("CMeleeTimedEAAS::PerformMelee", false);
		pDebug->AddLine(pos, (pos + dir), ColorF(1.f, 0.f, 0.f, 1.f), 15.f);
	}
#endif

	m_collisionHelper.DoCollisionTest(SCollisionTestParams(pos, dir, GetRange(), m_pWeapon->GetOwnerId(), 0, remote));
}

//------------------------------------------------------------------------
bool CMeleeTimedEAAS::PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, bool remote)
{
	MeleeDebugLog("CMeleeTimedEAAS<%p> PerformCylinderTest(remote=%s)", this, remote ? "true" : "false");

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;

	primitives::cylinder cyl;
	cyl.r = 0.25f;
	cyl.axis = dir;
	cyl.hh = GetRange() *0.5f;
	cyl.center = pos + dir.normalized()*cyl.hh;

	float n = 0.0f;
	geom_contact *contacts;
	intersection_params params;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = true;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(primitives::cylinder::type, &cyl, Vec3(ZERO),
		ent_rigid | ent_sleeping_rigid | ent_independent | ent_static | ent_terrain | ent_water, &contacts, 0,
		geom_colltype_foliage | geom_colltype_player, &params, 0, 0, &pIgnore, pIgnore ? 1 : 0);

	int ret = (int)n;

	float closestdSq = 9999.0f;
	geom_contact *closestc = 0;
	geom_contact *currentc = contacts;

	for (int i = 0; i<ret; i++)
	{
		geom_contact *contact = currentc;
		if (contact)
		{
			IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
			if (pCollider)
			{
				IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
				if (pEntity)
				{
					if (pEntity == pOwner)
					{
						++currentc;
						continue;
					}
				}

				const float distSq = (pos - currentc->pt).len2();
				if (distSq < closestdSq)
				{
					closestdSq = distSq;
					closestc = contact;
				}
			}
		}
		++currentc;
	}

	if (closestc)
	{
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(closestc->iPrim[0]);
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

		ApplyMeleeDamage(closestc->pt, dir, -dir, pCollider, collidedEntityId, closestc->iPrim[1], 0, closestc->id[1], remote, closestc->iPrim[1]);
	}

	return closestc != 0;
}

//------------------------------------------------------------------------
int CMeleeTimedEAAS::Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, bool remote)
{
	MeleeDebugLog("CMelee<%p> HitPointDirNormal(remote=%s)", this, remote ? "true" : "false");
	int hitTypeID = 0;

	IEntity *pTargetEnt = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
	if (pTargetEnt)
	{

		std::vector<EntityId>::const_iterator it = m_hitedEntites.begin();
		std::vector<EntityId>::const_iterator end = m_hitedEntites.end();
		int count = m_hitedEntites.size();

		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (pTargetEnt->GetId() == (*it))
			{
				return 0;
			}
		}
	}

	GetWpnCrtAtk();

	CActor *pOwnerActor = m_pWeapon->GetOwnerActor();

	if (pOwnerActor)
	{
		IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(collidedEntityId);
		IEntity* pTarget = pTargetActor ? pTargetActor->GetEntity() : gEnv->pEntitySystem->GetEntity(collidedEntityId);
		IEntity* pOwnerEntity = pOwnerActor->GetEntity();
		IAIObject* pOwnerAI = pOwnerEntity->GetAI();

		float damageScale = 1.0f;
		if (m_hold_time_to_h > 1.0f)
			damageScale = m_hold_time_to_h;
		
		bool silentHit = false;
		bool effects_from_wpn_added = false;
		if (pTargetActor)
		{
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				//add effect in its need
				effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), pt, normal);
				//check block
				if (!nwAction->TestHitBlock(pTargetActor->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
					return 1;
			}
			IAnimatedCharacter* pTargetAC = pTargetActor->GetAnimatedCharacter();
			IAnimatedCharacter* pOwnerAC = pOwnerActor->GetAnimatedCharacter();


			//pTargetActor->SetHealth(-1.0f);

			if (pTargetAC && pOwnerAC)
			{
				Vec3 targetFacing(pTargetAC->GetAnimLocation().GetColumn1());
				Vec3 ownerFacing(pOwnerAC->GetAnimLocation().GetColumn1());
				float ownerFacingDot = ownerFacing.Dot(targetFacing);
				float fromBehindDot = cos_tpl(DEG2RAD(g_pGameCVars->pl_melee.angle_limit_from_behind));

				if (ownerFacingDot > fromBehindDot)
				{
#ifndef DEMO_BUILD_RPG_SS
					if (g_pGameCVars->g_LogDamage)
					{
						CryLog("[DAMAGE] %s '%s' is%s meleeing %s '%s' from behind (because %f > %f)",
							pOwnerActor->GetEntity()->GetClass()->GetName(), pOwnerActor->GetEntity()->GetName(), silentHit ? " silently" : "",
							pTargetActor->GetEntity()->GetClass()->GetName(), pTargetActor->GetEntity()->GetName(),
							ownerFacingDot, fromBehindDot);
					}
#endif

					damageScale *= g_pGameCVars->pl_melee.damage_multiplier_from_behind;
				}
			}
		}
		else
		{
			CItem* pItemTarget = static_cast<CItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(collidedEntityId));
			if (pItemTarget)
			{
				if (pItemTarget->GetOwnerActor())
				{
					CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
					if (nwAction)
					{
						CGameRules *pGameRules = g_pGame->GetGameRules();
						if (pGameRules)
						{
							float damage = GetMeleeDamage();
							if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
								damage = m_pMeleeParams->meleeparams.damage_ai;

							if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
							{
								float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
								if (dist_xc > 2.0f)
									dist_xc = 2.0f;

								float dist_dmg_coof = damage * 0.05f;
								dist_xc *= 0.3f;
								//bigger distance == more damage;
								damage = damage - dist_dmg_coof / dist_xc;
							}

							if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
							{
								if (pOwnerActor)
								{
									float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
									if (vel_pwr > 5.0f)
										vel_pwr = 5.0f;

									float vel_dmg_coof = damage * 0.32f;
									vel_pwr *= 0.3f;
									//bigger velocity == more damage;
									damage = damage + (vel_dmg_coof * vel_pwr);
								}
							}

							hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
							HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
								damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
							pGameRules->ClientHit(info);
							num_hitedEntites += 1;
							m_hitedEntites.push_back(pTarget->GetId());
						}
						//add effect in its need
						effects_from_wpn_added = nwAction->AddSpecEffectFromWpnToTargetIfExist(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId());
						//check block
						if (!nwAction->TestHitBlock(pItemTarget->GetOwnerActor()->GetEntityId(), pOwnerActor->GetEntityId(), (int)GetMeleeDamage()))
							return 1;
						else
						{
							if (pGameRules && pItemTarget->GetOwnerActor())
							{
								float damage = GetMeleeDamage();
								if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
									damage = m_pMeleeParams->meleeparams.damage_ai;

								if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
								{
									float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
									if (dist_xc > 2.0f)
										dist_xc = 2.0f;

									float dist_dmg_coof = damage * 0.05f;
									dist_xc *= 0.3f;
									//bigger distance == more damage;
									damage = damage - dist_dmg_coof / dist_xc;
								}

								if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
								{
									if (pOwnerActor)
									{
										float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
										if (vel_pwr > 5.0f)
											vel_pwr = 5.0f;

										float vel_dmg_coof = damage * 0.32f;
										vel_pwr *= 0.3f;
										//bigger velocity == more damage;
										damage = damage + (vel_dmg_coof * vel_pwr);
									}
								}

								hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
								HitInfo info(m_pWeapon->GetOwnerId(), pItemTarget->GetOwnerActor()->GetEntityId(), m_pWeapon->GetEntityId(),
									damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);
								pGameRules->ClientHit(info);
								num_hitedEntites += 1;
								m_hitedEntites.push_back(pItemTarget->GetOwnerActor()->GetEntityId());
							}
							return 1;
						}
					}
				}
			}
		}


		// Send target stimuli
		if (!gEnv->bMultiplayer)
		{
			IAISystem *pAISystem = gEnv->pAISystem;
			ITargetTrackManager *pTargetTrackManager = pAISystem ? pAISystem->GetTargetTrackManager() : NULL;
			if (pTargetTrackManager && pOwnerAI)
			{
				IAIObject *pTargetAI = pTarget ? pTarget->GetAI() : NULL;
				if (pTargetAI)
				{
					const tAIObjectID aiOwnerId = pOwnerAI->GetAIObjectID();
					const tAIObjectID aiTargetId = pTargetAI->GetAIObjectID();

					TargetTrackHelpers::SStimulusEvent eventInfo;
					eventInfo.vPos = pt;
					eventInfo.eStimulusType = TargetTrackHelpers::eEST_Generic;
					eventInfo.eTargetThreat = AITHREAT_AGGRESSIVE;
					pTargetTrackManager->HandleStimulusEventForAgent(aiTargetId, aiOwnerId, "MeleeHit", eventInfo);
					pTargetTrackManager->HandleStimulusEventInRange(aiOwnerId, "MeleeHitNear", eventInfo, 5.0f);
				}
			}
		}

		//Check if is a friendly hit, in that case FX and Hit will be skipped
		bool isFriendlyHit = (pOwnerEntity && pTarget) ? IsFriendlyHit(pOwnerEntity, pTarget) : false;
		if (g_pGameCVars->i_enable_frenldly_hits > 0)
			isFriendlyHit = false;

		if (!isFriendlyHit)
		{
			CPlayer * pAttackerPlayer = pOwnerActor->IsPlayer() ? static_cast<CPlayer*>(pOwnerActor) : NULL;
			float damage = m_pMeleeParams->meleeparams.damage_ai;
			if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs > 0)
				damage = GetMeleeDamage();

			if (pOwnerActor->IsPlayer())
			{
				damage = m_slideKick ? m_pMeleeParams->meleeparams.slide_damage : GetMeleeDamage();
			}

			if (g_pGameCVars->g_melee_system_damage_depends_on_distance > 0)
			{
				float dist_xc = pt.GetDistance(m_pWeapon->GetEntity()->GetWorldPos());
				if (dist_xc > 2.0f)
					dist_xc = 2.0f;

				float dist_dmg_coof = damage * 0.05f;
				dist_xc *= 0.3f;
				//bigger distance == more damage;
				damage = damage - dist_dmg_coof / dist_xc;
			}

			if (g_pGameCVars->g_melee_system_damage_depends_on_actor_physical_velocity > 0)
			{
				if (pOwnerActor)
				{
					float vel_pwr = pOwnerActor->GetActorPhysics().velocity.GetLengthFloat();
					if (vel_pwr > 5.0f)
						vel_pwr = 5.0f;

					float vel_dmg_coof = damage * 0.32f;
					vel_pwr *= 0.3f;
					//bigger velocity == more damage;
					damage = damage + (vel_dmg_coof * vel_pwr);
				}
			}

#ifndef DEMO_BUILD_RPG_SS
			if (pTargetActor && g_pGameCVars->g_LogDamage)
			{
				CryLog("[DAMAGE] %s '%s' is%s meleeing %s '%s' applying damage = %.3f x %.3f = %.3f",
					pOwnerActor->GetEntity()->GetClass()->GetName(), pOwnerActor->GetEntity()->GetName(), silentHit ? " silently" : "",
					pTargetActor->GetEntity()->GetClass()->GetName(), pTargetActor->GetEntity()->GetName(),
					damage, damageScale, damage * damageScale);
			}
#endif

			//Generate Hit
			if (pTarget)
			{
				int bl_tp = 0;
				SmartScriptTable props;
				IScriptTable* pScriptTable = pTarget->GetScriptTable();
				if (pScriptTable)
				{
					if (pScriptTable && pScriptTable->GetValue("Properties", props))
					{
						CScriptSetGetChain prop(props);
						prop.GetValue("blood_type", bl_tp);
					}
				}
				CGameRules *pGameRules = g_pGame->GetGameRules();
				CRY_ASSERT_MESSAGE(pGameRules, "No game rules! Melee can not apply hit damage");

				if (pGameRules)
				{
					if (g_pGameCVars->g_use_normal_damage_form_for_ai_chrs <= 0)
						damage = m_pMeleeParams->meleeparams.damage_ai;

					hitTypeID = silentHit ? CGameRules::EHitType::SilentMelee : m_hitTypeID;
					HitInfo info(m_pWeapon->GetOwnerId(), pTarget->GetId(), m_pWeapon->GetEntityId(),
						damage * damageScale, 0.0f, surfaceIdx, partId, hitTypeID, pt, dir, normal);

					if (m_pMeleeParams->meleeparams.knockdown_chance>0 && cry_random(0, 99) < m_pMeleeParams->meleeparams.knockdown_chance)
						info.knocksDown = true;

					info.remote = remote;

					pGameRules->ClientHit(info);

					num_hitedEntites += 1;
					m_hitedEntites.push_back(pTarget->GetId());
					if (m_crt_hit_dmg_mult > 1.0f)
					{
						if (pTargetActor)
						{
							AttachDecalToWpn(pt, normal, false, surfaceIdx, bl_tp);
						}
					}
					else
					{
						int rnd_val_chs_1 = cry_random(0, 6);
						if (rnd_val_chs_1 <= 4 && pTargetActor)
						{
							AttachDecalToWpn(pt, normal, false, surfaceIdx, bl_tp);
						}
					}
					m_crt_hit_dmg_mult = 1.0f;

					//CryLogAlways(pTarget->GetName());

				}

				if (pAttackerPlayer && pAttackerPlayer->IsClient())
				{
					CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
					if (nwAction_x)
					{
						nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, true);
					}
					const Vec3 posOffset = (pt - pTarget->GetWorldPos());
					SMeleeHitParams params;

					params.m_boostedMelee = false;
					params.m_hitNormal = normal;
					params.m_hitOffset = posOffset;
					params.m_surfaceIdx = surfaceIdx;
					params.m_targetId = pTarget->GetId();

					pAttackerPlayer->OnMeleeHit(params);
				}
				else
				{
					PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
				}
				CActor *pActor = static_cast<CActor*>(pTargetActor);
				if (pActor)
				{
					CPlayer * pTargetPlayer = pActor->IsPlayer() ? static_cast<CPlayer*>(pActor) : NULL;
					if (pTargetPlayer)
					{
						pTargetPlayer->OnMeleeDamage();
					}
				}
			}
			else
			{
				bool can_spwn_eff_mt = false;
				//Play Material FX
				if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] * 0.8f) && num_mtl_eff_spwn == 0)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] * 0.6f) && num_mtl_eff_spwn == 1)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] * 0.4f) && num_mtl_eff_spwn == 2)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}
				else if ((m_updtimer <= m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()] * 0.2f) && num_mtl_eff_spwn == 3)
				{
					can_spwn_eff_mt = true;
					num_mtl_eff_spwn += 1;
				}

				if (can_spwn_eff_mt)
				{
					CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
					if (nwAction_x)
					{
						if (pAttackerPlayer && pAttackerPlayer->IsClient())
						{
							nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
						}
					}
					PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
				}

				if (g_pGameCVars->g_melee_sys_enable_close_waills_reaction > 0)
				{
					if (CheckCollisionWD(pt, surfaceIdx))
					{
						CActorActionsNew* nwAction_x = g_pGame->GetActorActionsNew();
						if (nwAction_x)
						{
							if (pAttackerPlayer && pAttackerPlayer->IsClient())
							{
								nwAction_x->OnMeleeHitToSurface(pAttackerPlayer->GetEntityId(), surfaceIdx, false);
							}
						}
						PlayHitMaterialEffect(pt, normal, m_pWeapon->GetSharedItemParams()->params.melee_fx_name.c_str(), surfaceIdx);
						return 1;
					}
				}
			}
		}

		if (pTarget)
		{
			CActor *pCTargetActor = static_cast<CActor*>(pTargetActor);
			CPlayer* pTargetPlayer = (pTargetActor && pTargetActor->IsPlayer()) ? static_cast<CPlayer*>(pTargetActor) : NULL;

			if (pTargetPlayer && pTargetPlayer->IsClient())
			{
				if (m_pMeleeParams->meleeparams.trigger_client_reaction)
				{
					pTargetPlayer->TriggerMeleeReaction();
				}
			}
		}
	}

	return hitTypeID;
}

//------------------------------------------------------------------------
/* static */ void CMeleeTimedEAAS::PlayHitMaterialEffect(const Vec3 &position, const Vec3 &normal, string hitFX, int surfaceIdx)
{
	//Play Material FX
	const char* meleeFXType = "melee";
	if (!hitFX.empty())
		meleeFXType = hitFX.c_str();

	IMaterialEffects* pMaterialEffects = gEnv->pGame->GetIGameFramework()->GetIMaterialEffects();
	Vec3 nv_normal = normal;
	if (surfaceIdx == 141)
	{
		if (nv_normal.z < 0.0f)
		{
			nv_normal.z = 0.2f;
		}
	}
	TMFXEffectId effectId = pMaterialEffects->GetEffectId(meleeFXType, surfaceIdx);
	if (effectId != InvalidEffectId)
	{
		SMFXRunTimeEffectParams params;
		params.pos = position;
		params.normal = nv_normal;
		params.playflags = eMFXPF_All | eMFXPF_Disable_Delay;
		//params.soundSemantic = eSoundSemantic_Player_Foley;
		pMaterialEffects->ExecuteEffect(effectId, params);
	}
}

void CMeleeTimedEAAS::AttachDecalToWpn(const Vec3 &position, const Vec3 &normal, bool bBoostedMelee, int surfaceIdx, int blood_type)
{
	//Function disabled for now
	/*float acc_inf_rps = 0.08f;
	float acc_inf_rps_pos = 0.18f;
	Vec3 dec_pos = position;
	Vec3 dec_nrm = normal;
	dec_nrm.z = cry_random(dec_nrm.z - acc_inf_rps, dec_nrm.z + acc_inf_rps);
	dec_nrm.x = cry_random(dec_nrm.x - acc_inf_rps, dec_nrm.x + acc_inf_rps);
	dec_nrm.y = cry_random(dec_nrm.y - acc_inf_rps, dec_nrm.y + acc_inf_rps);
	dec_pos.z = cry_random(dec_pos.z - acc_inf_rps_pos, dec_pos.z + acc_inf_rps_pos);
	dec_pos.x = cry_random(dec_pos.x - acc_inf_rps_pos, dec_pos.x + acc_inf_rps_pos);
	dec_pos.y = cry_random(dec_nrm.y - acc_inf_rps_pos, dec_pos.y + acc_inf_rps_pos);
	//---------------------decals on weapon----------------
	CryEngineDecalInfo decal;
	decal.vPos = position;
	decal.vNormal = normal;
	decal.fSize = cry_random(0.1f, 0.2f);
	decal.fLifeTime = cry_random(50.0f, 150.0f);
	decal.vHitDirection = -normal;
	decal.bSkipOverlappingTest = true;
	decal.fAngle = cry_random(0.0f, 50.0f);
	//decal.bForceEdge = true;
	decal.bForceSingleOwner = true;
	decal.bAssemble = false;
	decal.fGrowTimeAlpha = 0.4f;
	decal.fGrowTime = 0.4f;
	decal.preventDecalOnGround = true;
	decal.sortPrio = cry_random(1, 26);
	//decal.bDeferred = true;
	cry_strcpy(decal.szMaterialName, "materials/decals/blood/blood_splat1");
	if (m_pWeapon->GetEntity())
	{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (pActor)
	{
	IEntityRenderProxy *pRenderProxy = (IEntityRenderProxy*)m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER);
	if (pRenderProxy && pActor->IsThirdPerson())
	{
	CryLogAlways("Get Renderproxy for decal(3p)");
	decal.pIStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
	decal.ownerInfo.pRenderNode = pRenderProxy->GetRenderNode();
	decal.ownerInfo.nRenderNodeSlotId = 1;
	gEnv->p3DEngine->CreateDecal(decal);
	}
	else
	{
	IStatObj *pWpnFpObj = NULL;
	if (m_pWeapon->GetEntity()->GetStatObj(0))
	{
	CryLogAlways("CreatingRwndernode 1 seq");
	if (!pRenderProxy)
	return;

	IRenderNode *pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
	if (pNode)
	{
	pNode->CopyIRenderNodeData(pRenderProxy->GetRenderNode());
	pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
	pWpnFpObj->SetFlags(0);
	decal.pIStatObj = pWpnFpObj;
	pNode->SetEntityStatObj(0, pWpnFpObj);
	pNode->SetEntityStatObj(1, NULL);
	pNode->Hide(false);
	pNode->SetRndFlags(ERF_NO_DECALNODE_DECALS, false);
	CryLogAlways("CreatingRwndernode for decal(1p)");
	decal.ownerInfo.pRenderNode = pNode;
	decal.ownerInfo.nRenderNodeSlotId = 0;
	gEnv->p3DEngine->CreateDecal(decal);
	}
	}
	else
	{
	if (pActor->GetEntity()->GetCharacter(0))
	{
	IAttachmentManager* pAttachmentManager = pActor->GetEntity()->GetCharacter(0)->GetIAttachmentManager();
	if (pAttachmentManager)
	{
	IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName("weapon");
	if (pAttachment && pAttachment->GetIAttachmentObject())
	{
	CryLogAlways("CreatingRwndernode 1 seq");
	pWpnFpObj = pAttachment->GetIAttachmentObject()->GetIStatObj();
	if (pWpnFpObj)
	{
	CryLogAlways("CreatingRwndernode 2 seq");
	if (!pRenderProxy)
	return;

	IRenderNode *pNode = pRenderProxy->GetRenderNode();
	if (pNode)
	{
	Matrix34A mtx;
	pNode->GetEntityStatObj(0, 0, &mtx);
	CryLogAlways("CreatingRwndernode for decal(1p)");
	pNode->SetEntityStatObj(0, pWpnFpObj, &mtx);
	decal.ownerInfo.pRenderNode = pNode;
	decal.ownerInfo.nRenderNodeSlotId = 0;
	gEnv->p3DEngine->CreateDecal(decal);
	}
	}
	}
	}
	}
	}
	}
	}
	}*/
	//--------------set hit materials for wpn model. dynamic decals on weapon in 1p failure---------------------
	if (m_pWeapon->GetEntity())
	{
		CActor *pActor = m_pWeapon->GetOwnerActor();
		if (!pActor)
			return;

		//--------add b_stain_for weapon--------------------------------
		if (g_pGameCVars->g_melee_sys_enable_bl_drop_on_weapons > 0)
		{
			if (blood_type < 1)
				blood_type = 1;

			Vec3 prt_eff_pos(ZERO);
			prt_eff_pos = FindNearestPointInIntersectionLine(position);
			if (prt_eff_pos.IsZero())
				prt_eff_pos = position;

			m_pWeapon->RequestToEnableParticleEffectFromXML(blood_type, true, true, true, cry_random(2.0f*g_pGameCVars->g_melee_sys_time_mult_for_bl_emitter_on_weapons, 5.0f*g_pGameCVars->g_melee_sys_time_mult_for_bl_emitter_on_weapons), prt_eff_pos);
		}
		//--------------------------------------------------------------
		int slot = 1;
		IStatObj *pWpnFpObj = NULL;
		if (pActor->IsThirdPerson())
		{
			pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(1);
		}
		else
		{
			pWpnFpObj = m_pWeapon->GetEntity()->GetStatObj(0);
			slot = 0;
		}

		if (pWpnFpObj)
		{
			int aval_materials_ah[25];
			int num_aval_mats = 0;
			string material_for_h_wp = "";
			for (int i = 0; i < 25; i++)
			{
				if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined"))
				{
					continue;
				}
				else
				{
					aval_materials_ah[num_aval_mats] = i;
					num_aval_mats += 1;
				}
			}

			if (num_aval_mats < 1)
				return;
			else if (num_aval_mats == 1)
				material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[0]].c_str();
			else if (num_aval_mats > 1)
			{
				int mtl_cur = 0;
				mtl_cur = cry_random(0, num_aval_mats - 1);
				material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[mtl_cur]].c_str();
			}

			if (material_for_h_wp.empty())
				return;

			IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(material_for_h_wp.c_str());
			if (!pMtl)
				pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(material_for_h_wp.c_str(), false);


			if (IEntityRenderProxy* renderProxy = static_cast<IEntityRenderProxy*>(m_pWeapon->GetEntity()->GetProxy(ENTITY_PROXY_RENDER)))
			{
				if (IRenderNode* renderNode = renderProxy->GetRenderNode())
				{
					if (pMtl)
					{
						//renderNode->SetRndFlags(ERF_STATIC_INSTANCING, false);
						//renderNode->SetMaterial(pMtl);
						renderProxy->SetSlotMaterial(slot, pMtl);
					}

					if (pActor->IsPlayer() && !pActor->IsThirdPerson() && pMtl)
					{
						IAttachment* pAttachment = m_pWeapon->GetItemHandAttachment();
						if (pAttachment && pAttachment->GetType() != 4)
						{
							CCGFAttachment* pCGFAttachment = static_cast<CCGFAttachment*>(pAttachment->GetIAttachmentObject());
							if (pCGFAttachment)
							{
								pCGFAttachment->m_pReplacementMaterial = pMtl;
							}
						}
					}
				}
			}
			//if (pMtl)
			//	pWpnFpObj->SetMaterial(pMtl);
		}
		else
		{
			if (m_pWeapon->GetEntity()->GetCharacter(slot))
			{
				int aval_materials_ah[25];
				int num_aval_mats = 0;
				string material_for_h_wp = "";
				for (int i = 0; i < 25; i++)
				{
					if (m_pMeleeParams->meleeparams.weapon_meterial_afterhit[i] == ItemString("undefined"))
					{
						continue;
					}
					else
					{
						aval_materials_ah[num_aval_mats] = i;
						num_aval_mats += 1;
					}
				}

				if (num_aval_mats < 1)
					return;
				else if (num_aval_mats == 1)
					material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[0]].c_str();
				else if (num_aval_mats > 1)
				{
					int mtl_cur = 0;
					mtl_cur = cry_random(0, num_aval_mats - 1);
					material_for_h_wp = m_pMeleeParams->meleeparams.weapon_meterial_afterhit[aval_materials_ah[mtl_cur]].c_str();
				}

				if (material_for_h_wp.empty())
					return;

				IMaterial *pMtl = gEnv->p3DEngine->GetMaterialManager()->FindMaterial(material_for_h_wp.c_str());
				if (!pMtl)
					pMtl = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(material_for_h_wp.c_str(), false);

				if (pMtl)
				{
					m_pWeapon->GetEntity()->GetCharacter(slot)->SetIMaterial_Instance(pMtl);
				}
			}
		}
	}
}

bool CMeleeTimedEAAS::CheckCollisionWD(Vec3 position, int surfaceIdx)
{
	if (surfaceIdx == 141)
	{
		return false;
	}
	float dist = pos_start_nv.GetDistance(position);
	if (m_pMeleeParams->meleeparams.enable_wail_hit_stop[GetAttackId()])
	{
		float mx_val = m_pMeleeParams->meleeparams.wail_hit_stop_max_dist[GetAttackId()];
		if (mx_val <= 0.0f)
			mx_val = g_pGameCVars->g_melee_sys_wail_max_dist;

		if (dist < mx_val && mx_val > 0.0f)
		{
			m_pWeapon->RequireUpdate(eIUS_FireMode);
			DoAttack();
			m_pWeapon->RequestStopFire();
			m_attacking = false;
			m_slideKick = false;
			m_netAttacking = false;

			m_delayTimer = 0.0f;
			m_atkn = false;
			m_attacking = false;
			m_attacked = false;
			m_atking = false;
			m_hold_started_charge = false;

			m_delayTimer = 0.0f;
			m_durationTimer = 0.0f;
			m_hold_timer = 0.0f;
			m_atk_time = 0.0f;
			m_pWeapon->SetBusy(false);
			//m_pWeapon->ForcePendingActions();
			num_hitedEntites = 1;
			m_crt_hit_dmg_mult = 1.0f;
			m_updtimer = 0.0f;
			m_updtimer2 = 0.0f;
			pos_start_old = Vec3(ZERO);
			pos_end_old = Vec3(ZERO);
			pos_start_nv = Vec3(ZERO);
			pos_end_nv = Vec3(ZERO);
			m_hold_time_to_h = 0.0f;
			m_recoil_ca = m_pMeleeParams->meleeparams.wail_hit_stop_recoil_time[GetAttackId()];
			num_hitedEntites = 1;
			m_crt_hit_dmg_mult = 1.0f;
			m_updtimer = 0.0f;
			m_updtimer2 = 0.0f;
			pos_start_old = Vec3(ZERO);
			pos_end_old = Vec3(ZERO);
			pos_start_nv = Vec3(ZERO);
			pos_end_nv = Vec3(ZERO);
			m_hold_time_to_h = 0.0f;
			const ItemString &meleeAction = "melee_eaas_attack_close_hit_on_wail";
			FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
			m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
			//net sync---------------------------------------------------------------------
			CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
			if (nwAction)
			{
				nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
			}
			//-----------------------------------------------------------------------------
			return true;
		}
	}
	return false;
}

void CMeleeTimedEAAS::ForcedStopCurrentAttack()
{
	m_pWeapon->RequireUpdate(eIUS_FireMode);
	DoAttack();
	m_pWeapon->RequestStopFire();
	m_attacking = false;
	m_slideKick = false;
	m_netAttacking = false;

	m_delayTimer = 0.0f;
	m_atkn = false;
	m_attacking = false;
	m_attacked = false;
	m_atking = false;
	m_hold_started_charge = false;

	m_delayTimer = 0.0f;
	m_durationTimer = 0.0f;
	m_hold_timer = 0.0f;
	m_atk_time = 0.0f;
	m_pWeapon->SetBusy(false);
	//m_pWeapon->ForcePendingActions();
	num_hitedEntites = 1;
	m_crt_hit_dmg_mult = 1.0f;
	m_updtimer = 0.0f;
	m_updtimer2 = 0.0f;
	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);
	m_hold_time_to_h = 0.0f;
	const ItemString &meleeAction = "melee_eaas_attack_forced_stop";
	FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
	m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	//net sync---------------------------------------------------------------------
	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (nwAction)
	{
		nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
	}
	//-----------------------------------------------------------------------------
}
void CMeleeTimedEAAS::ForcedResetAnyAttackState()
{
	m_attacking = false;
	m_slideKick = false;
	m_netAttacking = false;

	m_delayTimer = 0.0f;
	m_atkn = false;
	m_attacking = false;
	m_attacked = false;
	m_atking = false;
	m_hold_started_charge = false;
	m_recoil_ca = 0.0f;
	m_delayTimer = 0.0f;
	m_durationTimer = 0.0f;
	m_hold_timer = 0.0f;
	m_atk_time = 0.0f;
	m_pWeapon->SetBusy(false);
	num_hitedEntites = 1;
	m_crt_hit_dmg_mult = 1.0f;
	m_updtimer = 0.0f;
	m_updtimer2 = 0.0f;
	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);
	m_hold_time_to_h = 0.0f;
	num_hitedEntites = 1;
	m_crt_hit_dmg_mult = 1.0f;
	m_updtimer = 0.0f;
	m_updtimer2 = 0.0f;
	pos_start_old = Vec3(ZERO);
	pos_end_old = Vec3(ZERO);
	pos_start_nv = Vec3(ZERO);
	pos_end_nv = Vec3(ZERO);
	m_hold_time_to_h = 0.0f;
	m_delayTimer = 0.0f;
	m_durationTimer = 0.0f;
}
//------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////
struct CMeleeTimedEAAS::DelayedImpulse
{
	DelayedImpulse(CMeleeTimedEAAS& melee, EntityId collidedEntityId, const Vec3& impulsePos, const Vec3& impulse, int partId, int ipart, int hitTypeID)
	: m_melee(melee)
	, m_collidedEntityId(collidedEntityId)
	, m_impulsePos(impulsePos)
	, m_impulse(impulse)
	, m_partId(partId)
	, m_ipart(ipart)
	, m_hitTypeID(hitTypeID)
	{

	};

	void execute(CItem *pItem)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(m_collidedEntityId);
		IPhysicalEntity* pPhysicalEntity = pEntity ? pEntity->GetPhysics() : NULL;

		if (pPhysicalEntity)
		{
			m_melee.m_collisionHelper.Impulse(pPhysicalEntity, m_impulsePos, m_impulse, m_partId, m_ipart, m_hitTypeID);
		}
	}

private:
	CMeleeTimedEAAS& m_melee;
	EntityId m_collidedEntityId;
	Vec3 m_impulsePos;
	Vec3 m_impulse;
	int m_partId;
	int m_ipart;
	int m_hitTypeID;
};
//////////////////////////////////////////////////////////////////////////

void CMeleeTimedEAAS::Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, EntityId collidedEntityId, int partId, int ipart, int surfaceIdx, int hitTypeID, int iPrim)
{
	if (pCollider && m_pMeleeParams->meleeparams.impulse>0.001f)
	{
		CActor* pOwnerActor = m_pWeapon->GetOwnerActor();
		const SPlayerMelee& meleeCVars = g_pGameCVars->pl_melee;
		const SMeleeTimedParams& meleeParams = m_pMeleeParams->meleeparams;
		float impulse = meleeParams.impulse;
		bool aiShooter = pOwnerActor ? !pOwnerActor->IsPlayer() : true;
		bool delayImpulse = false;

		float impulseScale = 1.0f;

		//[kirill] add impulse to phys proxy - to make sure it's applied to cylinder as well (not only skeleton) - so that entity gets pushed
		// if no pEntity - do it old way
		IEntity * pEntity = gEnv->pEntitySystem->GetEntity(collidedEntityId);
		IGameFramework* pGameFramework = g_pGame->GetIGameFramework();
		CActor* pTargetActor = static_cast<CActor*>(pGameFramework->GetIActorSystem()->GetActor(collidedEntityId));
		if (pEntity && pTargetActor)
		{
			//If it's an entity, use the specific impulses if needed, and apply to physics proxy
			if (meleeCVars.impulses_enable != SPlayerMelee::ei_Disabled)
			{
				impulse = meleeParams.impulse_actor;

				bool aiTarget = !pTargetActor->IsPlayer();

				if (aiShooter && !aiTarget)
				{
					float impulse_ai_to_player = GetImpulseAiToPlayer();
					if (impulse_ai_to_player != -1.f)
					{
						impulse = impulse_ai_to_player;
					}
				}

				//Delay a bit on death actors, when switching from alive to death, impulses don't apply
				//I schedule an impulse here, to get rid off the ugly .lua code which was calculating impulses on its own
				if (pTargetActor->IsDead())
				{
					if (meleeCVars.impulses_enable != SPlayerMelee::ei_OnlyToAlive)
					{
						delayImpulse = true;
						const float actorCustomScale = 1.0f;

						impulseScale *= actorCustomScale;
					}
					else
					{
						impulse = 0.0f;
					}
				}
				else if (meleeCVars.impulses_enable == SPlayerMelee::ei_OnlyToDead)
				{
					// Always allow impulses for melee from AI to local player
					// [*DavidR | 27/Oct/2010] Not sure about this
					if (!(aiShooter && !aiTarget))
						impulse = 0.0f;
				}
				else
				{
					impulse *= 0.01f;
					impulseScale = 1.0f;
				}
			}
			else if (pGameFramework->GetIVehicleSystem()->GetVehicle(collidedEntityId))
			{
				impulse = m_pMeleeParams->meleeparams.impulse_vehicle;
				impulseScale = 1.0f;
			}
		}

		float speed = sqrt_tpl(4000.0f / (80.0f*0.5f));
		if (surfaceIdx == 141)
		{
			impulse = 1.0f;
			impulseScale = 1.0f;
			speed = 0.3f;
		}

		const float fScaledImpulse = impulse * impulseScale;
		if (fScaledImpulse > 0.0f)
		{
			if (!delayImpulse)
			{
				m_collisionHelper.Impulse(pCollider, pt, dir * fScaledImpulse, partId, ipart, hitTypeID);
			}
			else
			{
				//Force up impulse, to make the enemy fly a bit
				Vec3 newDir = (dir.z < 0.0f) ? Vec3(dir.x, dir.y, 0.1f) : dir;
				newDir.Normalize();
				newDir.x *= fScaledImpulse;
				newDir.y *= fScaledImpulse;
				newDir.z *= impulse;

				if (pTargetActor)
				{
					pe_action_impulse imp;
					imp.iApplyTime = 0;
					imp.impulse = newDir;
					//imp.ipart = ipart;
					imp.partid = partId;
					imp.point = pt;
					pTargetActor->GetImpulseHander()->SetOnRagdollPhysicalizedImpulse(imp);
				}
				else
				{
					m_pWeapon->GetScheduler()->TimerAction(100, CSchedulerAction<DelayedImpulse>::Create(DelayedImpulse(*this, collidedEntityId, pt, newDir, partId, ipart, hitTypeID)), true);
				}
			}
		}

		// scar bullet
		// m = 0.0125
		// v = 800
		// energy: 4000
		// in this case the mass of the active collider is a player part
		// so we must solve for v given the same energy as a scar bullet

		if (IRenderNode *pBrush = (IRenderNode*)pCollider->GetForeignData(PHYS_FOREIGN_ID_STATIC))
		{
			speed = 0.003f;
		}

		m_collisionHelper.GenerateArtificialCollision(m_pWeapon->GetOwner(), pCollider, pt, normal, dir * speed, partId, ipart, surfaceIdx, iPrim);
	}
}

//-----------------------------------------------------------
bool CMeleeTimedEAAS::IsFriendlyHit(IEntity* pShooter, IEntity* pTarget)
{
	if (gEnv->bMultiplayer)
	{
		// Only count entity as friendly if friendly fire damage is off
		// and the player is on the same team. This will prevent blood effects.
		if (g_pGame->GetGameRules()->GetFriendlyFireRatio() <= 0.f)
		{
			CActor* pTargetActor = (CActor*)g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
			if (pTargetActor)
			{
				return pTargetActor->IsFriendlyEntity(pShooter->GetId()) != 0;
			}
		}
	}
	else
	{
		IActor* pAITarget = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId());
		if (pAITarget && pTarget->GetAI() && !pTarget->GetAI()->IsHostile(pShooter->GetAI(), false))
		{
			return true;
		}

		return IsMeleeFilteredOnEntity(*pTarget);
	}

	return false;
}

//-----------------------------------------------------------------------
void CMeleeTimedEAAS::ApplyMeleeEffects(bool hit)
{
}

void CMeleeTimedEAAS::ApplyMeleeDamage(const Vec3& point, const Vec3& dir, const Vec3& normal, IPhysicalEntity* physicalEntity,
	EntityId entityID, int partId, int ipart, int surfaceIdx, bool remote, int iPrim)
{
	const float blend = m_slideKick ? 0.4f : SATURATE(m_pMeleeParams->meleeparams.impulse_up_percentage);
	Vec3 impulseDir = Vec3(0, 0, 1) * blend + dir * (1.0f - blend);
	impulseDir.normalize();

	int hitTypeID = Hit(point, dir, normal, physicalEntity, entityID, partId, ipart, surfaceIdx, remote);

	IActor* pActor = gEnv->bMultiplayer && entityID ? g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(entityID) : NULL;
	if (!gEnv->bMultiplayer || !pActor) //MP handles melee impulses to actors in GameRulesClientServer.cpp. See: ClApplyActorMeleeImpulse)
	{
		Impulse(point, impulseDir, normal, physicalEntity, entityID, partId, ipart, surfaceIdx, hitTypeID, iPrim);
	}
}

//---------------------------------------------------------------------
void CMeleeTimedEAAS::RequestAlignmentToNearestTarget()
{
	if (g_pGameCVars->g_melee_disable_aligment_to_nearest_target > 0)
		return;

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner && pOwner->IsClient())
	{
		if (!s_meleeSnapTargetId)
		{
			// If we don't already have an auto-aim target, try and find one.
			if (s_meleeSnapTargetId = GetNearestTarget())
			{
				IActor* pTargetActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(s_meleeSnapTargetId);
				s_bMeleeSnapTargetCrouched = pTargetActor && static_cast<CPlayer*>(pTargetActor)->GetStance() == STANCE_CROUCH;

				CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);
			}
		}

		if (!s_meleeSnapTargetId || m_netAttacking)
			return;

		if (pOwner && pOwner->IsClient() && m_pMeleeAction && g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.SetViewLimit(pOwner->GetViewRotation().GetColumn1(), 0.01f, 0.01f, 0.01f, 0.01f, SViewLimitParams::eVLS_Item);
		}

		g_pGame->GetAutoAimManager().SetCloseCombatSnapTarget(s_meleeSnapTargetId,
			g_pGameCVars->pl_melee.melee_snap_end_position_range,
			g_pGameCVars->pl_melee.melee_snap_move_speed_multiplier);
	}
}

//--------------------------------------------------------
EntityId CMeleeTimedEAAS::GetNearestTarget()
{
	CActor* pOwnerActor = m_pWeapon->GetOwnerActor();
	if (pOwnerActor == NULL || (gEnv->bMultiplayer && m_slideKick))
		return 0;

	CRY_ASSERT(pOwnerActor->IsClient());

	IMovementController* pMovementController = pOwnerActor->GetMovementController();
	if (!pMovementController)
		return 0;

	SMovementState moveState;
	pMovementController->GetMovementState(moveState);

	const Vec3 playerDir = moveState.aimDirection;
	const Vec3 playerPos = moveState.eyePosition;
	const float range = g_pGameCVars->pl_melee.melee_snap_target_select_range;
	const float angleLimit = cos_tpl(DEG2RAD(g_pGameCVars->pl_melee.melee_snap_angle_limit));

	return m_collisionHelper.GetBestAutoAimTargetForUser(pOwnerActor->GetEntityId(), playerPos, playerDir, range, angleLimit);
}

//-----------------------------------------------------------------------
void CMeleeTimedEAAS::OnMeleeHitAnimationEvent()
{
	if (IsMeleeWeapon() && m_hitStatus == EHitStatus_HaveHitResult)
	{
		ApplyMeleeDamageHit(m_lastCollisionTest, m_lastRayHit);

		m_hitStatus = EHitStatus_Invalid;

		m_lastRayHit.pCollider->Release();
		m_lastRayHit.pCollider = NULL;
	}
	else
	{
		m_hitStatus = EHitStatus_ReceivedAnimEvent;
	}
}

//-----------------------------------------------------------------------
void CMeleeTimedEAAS::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}

void CMeleeTimedEAAS::ApplyMeleeDamageHit(const SCollisionTestParams& collisionParams, const ray_hit& hitResult)
{
	IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(hitResult.pCollider);
	EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

	ApplyMeleeDamage(hitResult.pt, collisionParams.m_dir, hitResult.n, hitResult.pCollider, collidedEntityId,
		hitResult.partid, hitResult.ipart, hitResult.surface_idx, collisionParams.m_remote, hitResult.iPrim);
}

void CMeleeTimedEAAS::OnSuccesfulHit(const ray_hit& hitResult)
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (IsMeleeWeapon() && m_hitStatus != EHitStatus_ReceivedAnimEvent && hitResult.pCollider)
	{
		// we defer the MeleeDamage until the MeleeHitEvent is received!
		m_lastCollisionTest = m_collisionHelper.GetCollisionTestParams();
		m_lastRayHit = hitResult;

		m_lastRayHit.pCollider->AddRef();
		m_hitStatus = EHitStatus_HaveHitResult;
	}
	else
	{
		const SCollisionTestParams& collisionParams = m_collisionHelper.GetCollisionTestParams();

		ApplyMeleeDamageHit(collisionParams, hitResult);

		m_hitStatus = EHitStatus_Invalid;

		if (m_pMeleeAction)
		{
			if (pOwner)
			{
				m_pMeleeAction->OnHitResult(pOwner, true);
			}
			else
			{
				//Owner has dropped weapon (Likely from being killed) so we can stop and release the action
				m_pMeleeAction->ForceFinish();
				SAFE_RELEASE(m_pMeleeAction);
			}
		}
	}

	if (pOwner && pOwner->IsClient())
	{
		s_meleeSnapTargetId = 0;
		CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

		if (g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);
		}
	}
}

void CMeleeTimedEAAS::OnFailedHit()
{
	const SCollisionTestParams& collisionParams = m_collisionHelper.GetCollisionTestParams();

	bool collided = PerformCylinderTest(collisionParams.m_pos, collisionParams.m_dir, collisionParams.m_remote);

	CActor* pOwner = m_pWeapon->GetOwnerActor();

	if (pOwner && pOwner->IsClient())
	{
		if (!collided && s_meleeSnapTargetId)
		{
			Vec3 ownerpos = pOwner->GetEntity()->GetWorldPos();
			IEntity* pTarget = gEnv->pEntitySystem->GetEntity(s_meleeSnapTargetId);

			if (pTarget && ownerpos.GetSquaredDistance(pTarget->GetWorldPos()) < sqr(GetRange() * m_pMeleeParams->meleeparams.target_range_mult))
			{
				collided = m_collisionHelper.PerformMeleeOnAutoTarget(s_meleeSnapTargetId);
			}
		}

		s_meleeSnapTargetId = 0;
		CHANGED_NETWORK_STATE(pOwner, CPlayer::ASPECT_SNAP_TARGET);

		if (g_pGameCVars->pl_melee.mp_melee_system_camera_lock_and_turn)
		{
			pOwner->GetActorParams().viewLimits.ClearViewLimit(SViewLimitParams::eVLS_Item);
		}
	}

	if (m_pMeleeAction)
	{
		if (pOwner)
		{
			m_pMeleeAction->OnHitResult(pOwner, collided);
		}
		else
		{
			//Owner has dropped weapon (Likely from being killed) so we can stop and release the action
			m_pMeleeAction->ForceFinish();
			SAFE_RELEASE(m_pMeleeAction);
		}
	}

	ApplyMeleeEffects(collided);
}

float CMeleeTimedEAAS::GetRange() const
{
	if (m_slideKick)
		return 2.75f;

	const SMeleeTimedParams& meleeParams = m_pMeleeParams->meleeparams;
	return m_shortRangeAttack ? meleeParams.closeAttack.range : meleeParams.range;
}

bool CMeleeTimedEAAS::DoSlideMeleeAttack(CActor* pOwnerActor)
{
	if (pOwnerActor && pOwnerActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer* pOwnerPlayer = static_cast<CPlayer*>(pOwnerActor);
		const bool canDoKick = pOwnerPlayer->CanDoSlideKick();
		return canDoKick;
	}
	return false;
}

void CMeleeTimedEAAS::CloseRangeAttack(bool closeRangeAttack)
{
	m_shortRangeAttack = closeRangeAttack;
}

float CMeleeTimedEAAS::GetDuration() const
{
	if (IsMeleeWeapon())
	{
		return m_pWeapon->GetCurrentAnimationTime(eIGS_Owner)*0.001f;
	}
	if (!m_shortRangeAttack)
	{
		return m_pMeleeParams->meleeparams.duration;
	}
	else
	{
		return m_pMeleeParams->meleeparams.closeAttack.duration;
	}
}

float CMeleeTimedEAAS::GetDelay() const
{
	if (!m_shortRangeAttack)
	{
		CActor* pOwner = m_pWeapon->GetOwnerActor();
		if (pOwner && pOwner->IsPlayer())
			return m_pMeleeParams->meleeparams.delay;
		else
			return m_pMeleeParams->meleeparams.aiDelay;
	}
	else
	{
		return m_pMeleeParams->meleeparams.closeAttack.delay;
	}
}

float CMeleeTimedEAAS::GetImpulseAiToPlayer() const
{
	return m_shortRangeAttack ? m_pMeleeParams->meleeparams.closeAttack.impulse_ai_to_player : m_pMeleeParams->meleeparams.impulse_ai_to_player;
}

bool CMeleeTimedEAAS::IsMeleeFilteredOnEntity(const IEntity& targetEntity) const
{
	static IEntityClass* s_pAnimObjectClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("AnimObject");

	if (targetEntity.GetClass() == s_pAnimObjectClass)
	{
		if (IScriptTable* pEntityScript = targetEntity.GetScriptTable())
		{
			SmartScriptTable properties;
			if (pEntityScript->GetValue("Properties", properties))
			{
				SmartScriptTable physicProperties;
				if (properties->GetValue("Physics", physicProperties))
				{
					bool bulletCollisionEnabled = true;
					return (physicProperties->GetValue("bBulletCollisionEnabled", bulletCollisionEnabled) && !bulletCollisionEnabled);
				}
			}
		}
	}

	return false;
}

bool CMeleeTimedEAAS::SwitchToMeleeWeaponAndAttack()
{
	// Checking for the equipped MeleeWeapon for each melee is hateful, but there's nowhere to check the inventory when a weapon is equipped.
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner && (pOwner->GetInventory()->GetItemByClass(CItem::sWeaponMeleeClass) != 0))
	{
		m_pWeapon->SetPlaySelectAction(false);
		pOwner->SelectItemByName("WeaponMelee", true);

		// trigger the attack - should be good without ptr checks, we validated that the weapon exists in the inventory.
		pOwner->GetCurrentItem()->GetIWeapon()->MeleeAttack();

		return true;
	}

	return false;
}

float CMeleeTimedEAAS::GetMeleeDamage() const
{
	int cnga = 0;
	if (m_holdedf)
		cnga = 1;
	else if (m_holdedb)
		cnga = 2;
	else if (m_holdedl)
		cnga = 3;
	else if (m_holdedr)
		cnga = 4;

	float dmg1nv = (float)m_pMeleeParams->meleeparams.damage_new[cnga];
	float dmg2nv = (float)m_pMeleeParams->meleeparams.damage;

	if (num_hitedEntites == 0)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	else if (num_hitedEntites == 1)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.first_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	else if (num_hitedEntites == 2)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.second_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	else if (num_hitedEntites == 3)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.third_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	else if (num_hitedEntites == 4)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.fourth_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;
	else if (num_hitedEntites > 4)
		return (((dmg1nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_pMeleeParams->meleeparams.other_hited_ent_dmg_mlt)*m_crt_hit_dmg_mult;

	return ((dmg2nv + m_pWeapon->GetDmgAdded())*GetOwnerStrength()*m_meleeScale)*m_crt_hit_dmg_mult;
}

#ifdef SERVER_CHECKS
float CMeleeTimedEAAS::GetDamageAmountAtXMeters(float x)
{
	return max(m_pMeleeParams->meleeparams.damage, m_pMeleeParams->meleeparams.slide_damage);
}

#endif

bool CMeleeTimedEAAS::StartMultiAnimMeleeAttack(CActor* pActor)
{
	if (pActor)
	{
		m_hitTypeID = CGameRules::EHitType::Melee;

		const IActionController* pActionController = pActor->GetAnimatedCharacter()->GetActionController();
		const CTagDefinition& fragmentIDs = pActionController->GetContext().controllerDef.m_fragmentIDs;
		const SMeleeTimedActions& actions = m_pMeleeParams->meleeactions;

		if (m_pMeleeAction)
		{
			m_pMeleeAction->NetEarlyExit(); //Cancel the old one before we start the new one
		}

		int introID = fragmentIDs.Find(actions.attack_multipart.c_str());
		if (introID != FRAGMENT_ID_INVALID)
		{
			m_pMeleeAction = new CMeleeTimedEAASAction(PP_PlayerActionUrgent, m_pWeapon, introID);
			m_pMeleeAction->AddRef();
			pActor->GetAnimatedCharacter()->GetActionController()->Queue(*m_pMeleeAction);
			m_attackTurnAmount = 0.f;
			return true;
		}
	}

	return false;
}

float CMeleeTimedEAAS::GetImpulseStrength()
{
	return m_pMeleeParams->meleeparams.impulse_actor;
}

//////////////////////// CMeleeTimedEAASAction ////////////////////////

CMeleeTimedEAAS::CMeleeTimedEAASAction::CMeleeTimedEAASAction(int priority, CWeapon* pWeapon, FragmentID fragmentID)
: BaseClass(priority, fragmentID, TAG_STATE_EMPTY, IAction::NoAutoBlendOut), m_weaponId(pWeapon->GetEntityId()), m_FinalStage(false), m_bCancelled(false)
{
	const SMannequinPlayerParams::Fragments::Smelee_multipart& meleeFragment = PlayerMannequin.fragments.melee_multipart;

	const CTagDefinition* pFragTagDef = meleeFragment.pTagDefinition;
	if (pFragTagDef)
	{
		CTagState fragTagState(*pFragTagDef, m_fragTags);

		pWeapon->SetFragmentTags(fragTagState);

		m_fragTags = fragTagState.GetMask();

		pFragTagDef->Set(m_fragTags, meleeFragment.fragmentTagIDs.into, true);
	}
}

void CMeleeTimedEAAS::CMeleeTimedEAASAction::Exit()
{
	if (!m_bCancelled)
	{
		StopAttackAction();
	}

	BaseClass::Exit();
}

void CMeleeTimedEAAS::CMeleeTimedEAASAction::OnHitResult(CActor* pOwnerActor, bool hit)
{
	CRY_ASSERT(pOwnerActor);

	const SMannequinPlayerParams::Fragments::Smelee_multipart& meleeFragment = PlayerMannequin.fragments.melee_multipart;

	const CTagDefinition* pFragTagDef = meleeFragment.pTagDefinition;
	if (pFragTagDef)
	{
		pFragTagDef->Set(m_fragTags, hit ? meleeFragment.fragmentTagIDs.hit : meleeFragment.fragmentTagIDs.miss, true);

		// In TP, force record the tag change so that the FP KillCam replay looks correct.
		if (pOwnerActor->IsThirdPerson() && GetStatus() == Installed)
		{
			if (CRecordingSystem* pRecSys = g_pGame->GetRecordingSystem())
			{
				uint32 optionIdx = GetOptionIdx();
				if (optionIdx == OPTION_IDX_RANDOM)
				{
					SetOptionIdx(GetContext().randGenerator.GenerateUint32());
				}
				pRecSys->OnMannequinRecordHistoryItem(SMannHistoryItem(GetForcedScopeMask(), PlayerMannequin.fragmentIDs.melee_multipart, m_fragTags, GetOptionIdx(), false), GetRootScope().GetActionController(), GetRootScope().GetEntityId());
			}
		}
	}

	SetFragment(PlayerMannequin.fragmentIDs.melee_multipart, m_fragTags, m_optionIdx, m_userToken, false);

	m_FinalStage = true;
	m_flags &= ~IAction::NoAutoBlendOut;
}

void CMeleeTimedEAAS::CMeleeTimedEAASAction::NetEarlyExit()
{
	m_eStatus = IAction::Finished;
	m_flags &= ~(IAction::Interruptable | IAction::NoAutoBlendOut);

	StopAttackAction();
	m_bCancelled = true;
}

void CMeleeTimedEAAS::CMeleeTimedEAASAction::StopAttackAction()
{
	if (CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_weaponId)))
	{
		CWeapon* pWeapon = static_cast<CWeapon*>(pItem->GetIWeapon());
		CMeleeTimedEAAS* pMelee = pWeapon->GetMeleeTimed();
		if (pMelee)
		{
			//pMelee->StopAttack();
			//StopAttackingAction action(pMelee);
			//action.execute(pItem);
			
		}
	}
}

bool CMeleeTimedEAAS::PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote, const Vec3 &orig)
{
	string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
	string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
	Vec3 position(0, 0, 0);
	Vec3 positionl(0, 0, 0);
	Vec3 pos323(0, 0, 0);
	IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
	if (pStatObj)
	{
		positionl = pStatObj->GetHelperPos(helperstart.c_str());
		positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
		position = pStatObj->GetHelperPos(helperend.c_str());
		position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
		pos323 = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
	}
	else
	{
		position = m_pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperend.c_str(), true);
		positionl = m_pWeapon->GetSlotHelperPos(eIGS_ThirdPerson, helperstart.c_str(), true);
		pos323 = position;
	}

	if (!orig.IsZeroFast())
		pos323 = orig;

	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;

	IRenderer* pRenderer = gEnv->pRenderer;
	IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
	pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
	ColorB colNormal(200, 0, 0, 128);

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	if (pIgnore)
	{
		pEnts_to_ignore[0] = pIgnore;
		num_ents_to_ignore += 1;
	}
	
	if (m_pWeapon->GetEntity() && m_pWeapon->GetEntity()->GetPhysics())
	{
		pEnts_to_ignore[1] = m_pWeapon->GetEntity()->GetPhysics();
		num_ents_to_ignore += 1;
	}
	
	if (pOwneract)
	{
		if (pOwneract->GetEquippedState(eAESlot_Shield))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetEquippedState(eAESlot_Torch))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
	}
	num_ents_to_ignore = 3;
	CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwneract->GetEntityId()));
	CItem* pCurrentWeapon = static_cast<CItem*>(pOwneract->GetCurrentItem());
	if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwneract->IsPlayer())
	{
		position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
		positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
		pos323 = position;
	}

	ray_hit hit;
	int n;
	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[GetAttackId()] / 2), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent,
			rwi_colltype_any | rwi_stop_at_pierceable|rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (g_pGameCVars->r_draw_debug_lines_of_new_melee > 0)
			pAuxGeom->DrawLine(pos, colNormal, pos323, colNormal, 5);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[GetAttackId()] / 2), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent,
			rwi_colltype_any | rwi_stop_at_pierceable|rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);

		if (g_pGameCVars->r_draw_debug_lines_of_new_melee > 0)
			pAuxGeom->DrawLine(pos, colNormal, pos323, colNormal, 5);
	}

	//WriteLockCond lockColl(*params.plock, 0);
	//lockColl.SetActive(1);

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
		{
			if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
				return false;
		}

		if (normalized_hit_direction.IsZeroFast())
			normalized_hit_direction = hit.n;

		int hitTypeID = Hit(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
		Impulse(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
	}

	PerformRayTest2(pos, dir, 100.0f, false);

	return n>0;
}

void CMeleeTimedEAAS::StartRay()
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
		string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
		Vec3 position(0, 0, 0);
		Vec3 positionl(0, 0, 0);
		CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
		CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
		if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			//CryLogAlways(helperstart.c_str());
			//CryLogAlways(helperend.c_str());
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			//CryLogAlways(helperstart.c_str());
			if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
			{
				position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
				positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
				PerformRayTest(positionl, (position - positionl), 1, false);
				pos_end_nv = position;
				pos_start_nv = positionl;
			}
			else
			{
				IEntity *pOwner = m_pWeapon->GetOwner();
				IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
				IEntity *pHeldObject = NULL;
				PerformRayTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), 1, false);
				//PerformCylinderTest(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), false);
				//PerformMelee(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), true);
				pos_end_nv = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
				pos_start_nv = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			}

			if (!pos_end_old.IsZero() && !pos_end_nv.IsZero())
			{
				Vec3 nv_pos_center = (pos_end_nv + pos_start_nv) * 0.5000f;
				Vec3 nv_dir_xc_fa = (pos_start_old - nv_pos_center).normalized() * 0.1f;
				nv_pos_center = pos_start_old + (nv_dir_xc_fa * 1.5f);
				normalized_hit_direction = nv_pos_center - pos_start_old;
				normalized_hit_direction.normalize();
			}
			AdditiveRayTestStart();
		}
		else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			int slot = 0;
			if (pAttackingPlayer->IsThirdPerson())
				slot = 1;

			position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
			positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
			PerformRayTest(positionl, (position - positionl), 1, false);
			pos_end_nv = position;
			pos_start_nv = positionl;
			if (!pos_end_old.IsZero() && !pos_end_nv.IsZero())
			{
				Vec3 nv_pos_center = (pos_end_nv + pos_start_nv) * 0.5000f;
				Vec3 nv_dir_xc_fa = (pos_start_old - nv_pos_center).normalized() * 0.1f;
				nv_pos_center = pos_start_old + (nv_dir_xc_fa * 1.5f);
				normalized_hit_direction = nv_pos_center - pos_start_old;
				normalized_hit_direction.normalize();
			}
			AdditiveRayTestStart();
		}
		else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
		{

		}
	}
}

void CMeleeTimedEAAS::AdditiveRayTestStart()
{
	if (g_pGameCVars->g_melee_system_adtr_type <= 0)
		return;

	if (pos_start_nv != Vec3(ZERO) && pos_start_old != Vec3(ZERO))
	{
		//CryLogAlways("Additive ray test pre");
		if (pos_start_nv != pos_start_old)
		{
			//CryLogAlways("Additive ray test start");
			float dist_of_2points = 0.0f;
			float dist_of_2points_orig = 0.0f;
			dist_of_2points = pos_end_nv.GetDistance(pos_end_old);
			dist_of_2points_orig = dist_of_2points;
			dist_of_2points = dist_of_2points / g_pGameCVars->g_melee_system_rcdr_coof;
			int num_rays = (int)dist_of_2points;
			if (num_rays >= MAX_ADDITIVE_RAYS)
				num_rays = MAX_ADDITIVE_RAYS;

			if (g_pGameCVars->g_melee_system_adtr_type == 1)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + (pos_start_old - pos_start_nv) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + (pos_end_old - pos_end_nv) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 2)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalized() * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalized() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 3)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedSafe(Vec3(ZERO)) * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 4)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 5)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit3 = pos_start_old + (pos_end_old - pos_start_old) * mult_vv;
						Vec3 new_posit4 = pos_start_nv + (pos_end_nv - pos_start_nv) * mult_vv;
						PerformRayTest(new_posit3, (new_posit4 - new_posit3), 1, false, new_posit4);
						Vec3 new_posit1 = pos_start_nv + ((pos_start_old - pos_start_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + ((pos_end_old - pos_end_nv).GetNormalizedFast() * dist_of_2points_orig) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
			else if (g_pGameCVars->g_melee_system_adtr_type == 6)
			{
				float coof = 1.0f / num_rays;
				for (int i = 0; i < num_rays; i++)
				{
					if (i > 0 && i != num_rays)
					{
						float mult_vv = i * coof;
						Vec3 new_posit3 = pos_start_old + (pos_end_old - pos_start_old) * mult_vv;
						Vec3 new_posit4 = pos_start_nv + (pos_end_nv - pos_start_nv) * mult_vv;
						PerformRayTest(new_posit3, (new_posit4 - new_posit3), 1, false, new_posit4);
						Vec3 new_posit1 = pos_start_nv + (pos_start_old - pos_start_nv) * mult_vv;
						Vec3 new_posit2 = pos_end_nv + (pos_end_old - pos_end_nv) * mult_vv;
						PerformRayTest(new_posit1, (new_posit2 - new_posit1), 1, false, new_posit2);
					}
				}
			}
		}
	}
}

float CMeleeTimedEAAS::GetOwnerStrength() const
{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (!pActor)
		return 1.0f;

	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return 1.0f;

	float strength = 1.0f;
	float dmgmult = 0.1f;
	strength = dmgmult;
	strength = strength * (0.2f + 0.4f * 0.018f);
	IEntity *pEntity = pActor->GetEntity();
	SmartScriptTable props;
	SmartScriptTable propsStat;
	IScriptTable* pScriptTable = pEntity->GetScriptTable();
	pScriptTable && pScriptTable->GetValue("Properties", props);
	props->GetValue("Stat", propsStat);
	float str = 1.0f;
	propsStat->GetValue("strength", str);
	if (str > pActor->GetActorStrength())
	{
		strength *= str;
	}
	else if (str < pActor->GetActorStrength())
	{
		strength *= pActor->GetActorStrength();
	}
	else if (str == pActor->GetActorStrength())
	{
		strength *= pActor->GetActorStrength();
	}

	return (0.2f + (strength / 10));
}

bool CMeleeTimedEAAS::GetWpnCrtAtk()
{
	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (!pActor)
		return false;

	m_crt_hit_dmg_mult = 1.0f;

	int rnd_value1 = rand() % 100 + 1;

	if (m_pWeapon->GetWeaponType() == 1)
	{
		if (rnd_value1 > 0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_swords_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_swords_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 2)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_axes_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_axes_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 3)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 4)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thswords_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thswords_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 5)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thaxes_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thaxes_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 6)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 7)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_poles_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_poles_ml;
			return true;
		}
	}
	else if (m_pWeapon->GetWeaponType() == 8)
	{
		if (rnd_value1>0 && rnd_value1<m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml*pActor->crtchance_thpoles_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml*pActor->crtdmg_thpoles_ml;
			return true;
		}
	}
	else
	{
		if (rnd_value1>0 && rnd_value1 < m_pWeapon->GetCriticalHitBaseChance()*pActor->crtchance_common_ml)
		{
			m_crt_hit_dmg_mult = m_pWeapon->GetCriticalHitDmgModiffer()*pActor->crtdmg_common_ml;
			return true;
		}
	}

	return false;
}

bool CMeleeTimedEAAS::PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;

	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());

	ray_hit hit;
	int n;
	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[GetAttackId()] / 2), ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, NULL, 0);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, dir.normalized()*(m_pMeleeParams->meleeparams.range_new[GetAttackId()] / 2), ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, NULL, 0);
	}
	int hitTypeID = 1;

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;
		if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
		{
			if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
				return false;
		}

		if (normalized_hit_direction.IsZeroFast())
			normalized_hit_direction = hit.n;

		if (strength < 10)
		{
			if (remote)
			{
				Impulse(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			}
			else
			{
				Hit(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			}
		}
		else
		{
			ray_hit hit2 = hit;
			Hit(hit2.pt, dir, normalized_hit_direction, hit2.pCollider, collidedEntityId, hit2.partid, hit2.ipart, hit2.surface_idx, false);
			if (hit2.pCollider)
			{
				Impulse(hit.pt, dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
			}
		}
	}

	return n>0;
}

bool CMeleeTimedEAAS::PerformRayTest3(const Vec3 &pos, const Vec3 &dir, float strength, bool remote)
{
	IEntity *pOwner = m_pWeapon->GetOwner();
	IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
	CActor* pOwneract = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	IPhysicalEntity *pEnts_to_ignore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	if (pIgnore)
	{
		pEnts_to_ignore[0] = pIgnore;
		num_ents_to_ignore += 1;
	}

	if (m_pWeapon->GetEntity() && m_pWeapon->GetEntity()->GetPhysics())
	{
		pEnts_to_ignore[1] = m_pWeapon->GetEntity()->GetPhysics();
		num_ents_to_ignore += 1;
	}

	if (pOwneract)
	{
		if (pOwneract->GetEquippedState(eAESlot_Shield))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetEquippedState(eAESlot_Torch))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pEnts_to_ignore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
	}
	num_ents_to_ignore = 3;
	ray_hit hit;
	int n;
	Vec3 nv_dir = dir;
	if (nv_dir.GetLength() > 1.0f)
		nv_dir = dir.normalized();

	if (pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, nv_dir*(m_pMeleeParams->meleeparams.range_new[GetAttackId()]), ent_living | ent_sleeping_rigid | ent_rigid | ent_independent | ent_static | ent_terrain | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
	}
	else if (!pOwneract->IsPlayer())
	{
		n = gEnv->pPhysicalWorld->RayWorldIntersection(pos, nv_dir*(m_pMeleeParams->meleeparams.range_new[GetAttackId()]), ent_all | ent_water,
			rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, pEnts_to_ignore, num_ents_to_ignore);
	}

	if (n>0)
	{
		IPhysicalEntity *pCollider = hit.pCollider;
		IEntity* pCollidedEntity = pCollider ? gEnv->pEntitySystem->GetEntityFromPhysics(pCollider) : NULL;
		EntityId collidedEntityId = pCollidedEntity ? pCollidedEntity->GetId() : 0;

		if (pCollidedEntity)
		{
			if (CItem* pItem_own = pOwneract->GetItem(collidedEntityId))
			{
				if (pItem_own->GetOwnerId() == m_pWeapon->GetOwnerId())
					return false;
			}

			if (normalized_hit_direction.IsZeroFast())
				normalized_hit_direction = hit.n;

			int hitTypeID = Hit(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			Impulse(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, hitTypeID, hit.iPrim);
		}
		else
		{
			Hit(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, false);
			Impulse(hit.pt, nv_dir, normalized_hit_direction, hit.pCollider, collidedEntityId, hit.partid, hit.ipart, hit.surface_idx, 1, hit.iPrim);
		}
	}
	return n>0;
}

void CMeleeTimedEAAS::StartRay2(bool dc)
{
	CActor* pOwner = m_pWeapon->GetOwnerActor();
	if (pOwner)
	{
		string helperstart = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
		string helperend = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
		Vec3 position(0, 0, 0);
		Vec3 positionl(0, 0, 0);
		CPlayer* pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));
		CItem* pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
		IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
		if (pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			//CryLogAlways(helperstart.c_str());
			//CryLogAlways(helperend.c_str());
			positionl = pStatObj->GetHelperPos(helperstart.c_str());
			positionl = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(positionl);
			position = pStatObj->GetHelperPos(helperend.c_str());
			position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
			m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl);
			if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
			{
				position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperend.c_str(), true);
				positionl = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helperstart.c_str(), true);
				PerformRayTest2(positionl, (position - positionl), 1, dc);
			}
			else
			{
				IEntity *pOwner = m_pWeapon->GetOwner();
				IPhysicalEntity *pIgnore = pOwner ? pOwner->GetPhysics() : 0;
				IEntity *pHeldObject = NULL;
				PerformRayTest2(m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl), (m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position) - m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(positionl)), 1, dc);
			}
		}
		else if (!pStatObj && m_pWeapon->GetMeleeWpnNotAllowhelpers() == 0)
		{
			int slot = 0;
			if (pAttackingPlayer->IsThirdPerson())
				slot = 1;

			position = pCurrentWeapon->GetSlotHelperPos(slot, helperend.c_str(), true);
			positionl = pCurrentWeapon->GetSlotHelperPos(slot, helperstart.c_str(), true);
			PerformRayTest2(positionl, (position - positionl), 1, dc);
		}
		else if (m_pWeapon->GetMeleeWpnNotAllowhelpers() == 1)
		{

		}
	}
}

void CMeleeTimedEAAS::DoAttack()
{
	m_atk_time = m_pMeleeParams->meleeparams.delay;
	CActor* pOwner = static_cast<CActor *>(m_pWeapon->GetOwnerActor());
	m_atking = true;
	m_atkn = false;
	bool isClient = pOwner ? pOwner->IsClient() : false;
	CPlayer *pPlayer = (CPlayer *)pOwner;
	CPlayerInput *pPlayerInput = reinterpret_cast<CPlayerInput *>(pPlayer->GetPlayerInput());

	/*if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Forward || m_holdedf)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[1];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}
	else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Back || m_holdedb)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[2];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}
	else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Left || m_holdedl)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[3];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}
	else if (pPlayerInput->GetMoveButtonsState()&CPlayerInput::eMBM_Right || m_holdedr)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[4];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}
	else
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[5];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	}*/
//---------------------------fix2----------------------------------------------
	/*if (pOwner)
	{
		pOwner->SetTag(PlayerMannequin.tagIDs.throwing, false);

		if (pOwner->IsClient())
		{
			pOwner->LockInteractor(m_pWeapon->GetEntityId(), false);
		}
	}*/
//------------------------------------------------------------------------------

	if (m_holdedf)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[1];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------
	}
	else if (m_holdedb)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[2];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------
	}
	else if (m_holdedl)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[3];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------
	}
	else if (m_holdedr)
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[4];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------
	}
	else
	{
		const ItemString &meleeAction = m_pMeleeParams->meleeactions.attack_newf[5];
		FragmentID fragmentId = m_pWeapon->GetFragmentID(meleeAction.c_str());
		m_pWeapon->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		//net sync---------------------------------------------------------------------
		CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
		if (nwAction)
		{
			nwAction->NetRequestPlayItemAction(m_pWeapon->GetOwnerId(), fragmentId);
		}
		//-----------------------------------------------------------------------------
	}

	//m_pWeapon->SetItemFlag(CItem::eIF_BlockActions, false);
	m_hitTypeID = CGameRules::EHitType::Melee;
	if (!m_pMeleeParams->meleeparams.attack_type_on_hit[GetAttackId()].empty())
	{
		CGameRules *pGameRules = g_pGame->GetGameRules();
		if (pGameRules)
		{
			int h_t_id = pGameRules->GetHitTypeId(m_pMeleeParams->meleeparams.attack_type_on_hit[GetAttackId()].c_str());
			if (h_t_id > 0)
				m_hitTypeID = h_t_id;
			else
				m_hitTypeID = CGameRules::EHitType::Melee;
		}
	}

	/*uint32 durationInMilliseconds = static_cast<uint32>(GetDuration() * 1000.f);
	MeleeDebugLog("CMeleeTimedEAAS<%p> Setting a timer to trigger StopAttackingAction (duration=%u)", this, durationInMilliseconds);
	// How much longer we need the animation to be than the damage delay, in seconds...
	const float k_requireAdditionalTime = 0.1f;
	if (durationInMilliseconds < (m_delayTimer + k_requireAdditionalTime) * 1000.0f)
	{
		if (!gEnv->IsDedicated())
		{
			CRY_ASSERT_MESSAGE(false, string().Format("CMeleeTimedEAAS<%p> Warning! Melee attack timeout (%f seconds) needs to be substantially longer than the damage delay (%f seconds)! Increasing...", this, durationInMilliseconds / 1000.f, m_delayTimer));
		}
		durationInMilliseconds = (uint32)((m_delayTimer + k_requireAdditionalTime) * 1000.0f + 0.5f);	// Add 0.5f to round up when turning into a uint32
	}*/
	m_pWeapon->GetScheduler()->TimerAction(m_pWeapon->GetCurrentAnimationTime(eIGS_Owner), CSchedulerAction<StopAttackingAction>::Create(this), true);
	//m_pWeapon->GetScheduler()->TimerAction(m_pWeapon->GetCurrentAnimationTime(CItem::eIGS_FirstPerson), CSchedulerAction<StopAttackingAction>::Create(this), true);
	
	m_delayTimer = m_pMeleeParams->meleeparams.delay;
	m_durationTimer = m_pMeleeParams->meleeparams.duration;
	m_attacked = true;

	m_meleeScale = 1.0f;

	CActor *pActor = m_pWeapon->GetOwnerActor();
	if (!pActor)
		return;

	Vec3 pos(ZERO);
	Vec3 dir(ZERO);
	IMovementController * pMC = pActor->GetMovementController();
	if (!pMC)
		return;

	SMovementState info;
	pMC->GetMovementState(info);
	pos = info.eyePosition;
	dir = info.eyeDirection;

	m_pWeapon->RequestMeleeAttack(false, pos, dir);

	m_pWeapon->OnMeleeTimed(m_pWeapon->GetOwnerId());
	//m_pWeapon->SetDefaultIdleAnimation(CItem::eIGS_FirstPerson, g_pItemStrings->idle);
}

bool CMeleeTimedEAAS::GetRecoilSA()
{
	return m_recoil_ca > 0.0f;
}

void CMeleeTimedEAAS::StopAttack()
{
	if (m_attacking && !m_atking)
	{
		num_hitedEntites = 1;
		m_hitedEntites.clear();
		m_updtimer = m_pMeleeParams->meleeparams.ray_test_time[GetAttackId()];
		m_pWeapon->RequireUpdate(eIUS_FireMode);
		DoAttack();
		m_pWeapon->RequestStopFire();
		
		m_crt_hit_dmg_mult = 1.0f;
	}
}

void CMeleeTimedEAAS::PreStopAttack()
{
	m_atkn = true;
	m_pWeapon->RequireUpdate(eIUS_FireMode);
}

int CMeleeTimedEAAS::GetAttackId()
{
	int cnga = 0;
	if (m_holdedf)
		cnga = 1;
	else if (m_holdedb)
		cnga = 2;
	else if (m_holdedl)
		cnga = 3;
	else if (m_holdedr)
		cnga = 4;

	return cnga;
}

bool CMeleeTimedEAAS::IsHoldCStarted()
{
	return m_hold_started_charge;
}

Vec3 CMeleeTimedEAAS::GetAttackHelperPos(int helper_Id)
{
	Vec3 position(ZERO);
	if (!m_pWeapon || !m_pMeleeParams)
		return position;

	string helper = "";
	if (helper_Id == 1)
		helper = m_pMeleeParams->meleeparams.helper_raytest_st.c_str();
	else if (helper_Id == 2)
		helper = m_pMeleeParams->meleeparams.helper_raytest_end.c_str();
	else if (helper_Id == 3)
		helper = m_pMeleeParams->meleeparams.helper_raytest_alt.c_str();

	CActor* pOwner = m_pWeapon->GetOwnerActor();
	CPlayer* pAttackingPlayer = NULL;
	if (pOwner)
		pAttackingPlayer = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pOwner->GetEntityId()));

	CItem* pCurrentWeapon = static_cast<CItem*>(m_pWeapon);
	if (pOwner)
	{
		pCurrentWeapon = static_cast<CItem*>(pOwner->GetCurrentItem());
	}

	IStatObj *pStatObj = m_pWeapon->GetEntity()->GetStatObj(1);
	if (pStatObj)
	{
		position = pStatObj->GetHelperPos(helper.c_str());
		position = m_pWeapon->GetEntity()->GetSlotLocalTM(1, false).TransformPoint(position);
		m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		if (pAttackingPlayer && !pAttackingPlayer->IsThirdPerson() && pOwner->IsPlayer())
		{
			if (pCurrentWeapon)
			{
				position = pCurrentWeapon->GetSlotHelperPos(eIGS_FirstPerson, helper, true);
			}
		}
		else
		{
			position = m_pWeapon->GetEntity()->GetWorldTM().TransformPoint(position);
		}
	}
	else
	{
		int slot = 0;
		if (pAttackingPlayer->IsThirdPerson())
			slot = 1;

		position = pCurrentWeapon->GetSlotHelperPos(slot, helper.c_str(), true);
	}
	return position;
}

Vec3 CMeleeTimedEAAS::FindNearestPointInIntersectionLine(Vec3 nrPnt, int acc)
{
	Vec3 prt_eff_pos(ZERO);
	CVectorMathFncs *pVectFncs = g_pGame->GetVectorMathHelpfulFncs();
	if (pVectFncs)
	{
		prt_eff_pos = pVectFncs->FindNearestPointInLine(GetAttackHelperPos(CMelee::eMIHType_start), GetAttackHelperPos(CMelee::eMIHType_end), nrPnt, (float)acc);
	}
	return prt_eff_pos;
}
