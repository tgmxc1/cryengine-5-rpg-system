// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#include "StdAfx.h"
#include "NoWeapon.h"
#include "GameInputActionHandlers.h"
#include "GameActions.h"
#include "Actor.h"
#include "WeaponFPAiming.h"
#include "FireMode.h"
#include "ActorActionsNew.h"

CNoWeapon::CNoWeapon()
: CWeapon()
{
	left_butt_pressed = false;
	right_butt_pressed = false;
	RegisterActions();
}

//////////////////////////////////////////////////////////////////////////
CNoWeapon::~CNoWeapon()
{
}

//////////////////////////////////////////////////////////////////////////
void CNoWeapon::RegisterActions()
{
	CGameInputActionHandlers::TNoWeaponActionHandler& noWeaponActionHandler = g_pGame->GetGameInputActionHandlers().GetCNoWeaponActionHandler();

	if (noWeaponActionHandler.GetNumHandlers() == 0)
	{
		const CGameActions& actions = g_pGame->Actions();

		//if(gEnv->bMultiplayer)
		//{
		noWeaponActionHandler.AddHandler(actions.special, &CNoWeapon::OnActionMelee);
		noWeaponActionHandler.AddHandler(actions.wpn_block_melee, &CNoWeapon::OnActionBlockAtk);
		noWeaponActionHandler.AddHandler(actions.attack1, &CNoWeapon::OnActionAttackPrim);
		noWeaponActionHandler.AddHandler(actions.attack2_xi, &CNoWeapon::OnActionAttackSecond);
		//}
	}
}

//////////////////////////////////////////////////////////////////////////
void CNoWeapon::Select(bool select)
{
	BaseClass::Select(select);

	SetBusy(!select);
}

//////////////////////////////////////////////////////////////////////////
void CNoWeapon::OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	CGameInputActionHandlers::TNoWeaponActionHandler& noWeaponActionHandler = g_pGame->GetGameInputActionHandlers().GetCNoWeaponActionHandler();

	noWeaponActionHandler.Dispatch(this,actorId,actionId,activationMode,value);
}

//////////////////////////////////////////////////////////////////////////
bool CNoWeapon::OnActionMelee( EntityId actorId, const ActionId& actionId, int activationMode, float value )
{
	if(activationMode == eAAM_OnPress)
	{
		//If we are swimming and can fire under water we might want to do a melee attack
		CActor* pOwnerActor = GetOwnerActor();
		if(pOwnerActor && pOwnerActor->IsSwimming() && CanFireUnderWater())
		{
			CGameInputActionHandlers::TWeaponActionHandler& weaponActionHandler = g_pGame->GetGameInputActionHandlers().GetCWeaponActionHandler();
			weaponActionHandler.Dispatch(this,actorId,actionId,activationMode,value);
		}
	}

	return true;
}

bool CNoWeapon::OnActionAttackPrim(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	if (g_pGameCVars->g_combat_enable_hth_attacks_if_nwpn_selected == 0)
		return false;
	//CryLogAlways("CNoWeapon::OnActionAttackPrim");
	CActor* pOwnerActor = GetOwnerActor();
	if (!pOwnerActor)
		return false;

	if (pOwnerActor->GetRelaxedMod())
		return false;

	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return false;

	if (pOwnerActor->IsPlayer())
	{
		if (!nwAction->PlayerCanDoAnyAgressiveActions())
			return false;
	}

	if (!nwAction->ActorCanDoAnyAgressiveActions(pOwnerActor->GetEntityId(), GetEntityId()))
		return false;

	if (pOwnerActor->m_rgt_hand_blc)
		return false;

	if (activationMode == eAAM_OnPress)
	{
		left_butt_pressed = true;
		if (right_butt_pressed)
		{
			if (spec_melee_attack_simple_delay == 0.0f && spec_melee_attack_simple_recoil == 0.0f)
				nwAction->StartFistsMeleeAction(pOwnerActor->GetEntityId(), GetEntityId(), "melee_fists_attack_rgt_upperc", 0.2f, true, 6.0f, 0.5f, 7);
		}
	}

	if (activationMode == eAAM_OnRelease)
	{
		left_butt_pressed = false;

		if (spec_melee_attack_simple_delay == 0.0f && spec_melee_attack_simple_recoil == 0.0f)
			nwAction->StartFistsMeleeAction(pOwnerActor->GetEntityId(), GetEntityId(), "melee_fists_attack_rgt", 0.2f);
	}

	return true;
}

bool CNoWeapon::OnActionAttackSecond(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	if (g_pGameCVars->g_combat_enable_hth_attacks_if_nwpn_selected == 0)
		return false;

	if (g_pGameCVars->g_combat_enable_hth_attacks_if_nwpn_selected == 2)
		return false;

	CActor* pOwnerActor = GetOwnerActor();
	if (!pOwnerActor)
		return false;

	if (pOwnerActor->GetRelaxedMod())
		return false;

	CActorActionsNew* nwAction = g_pGame->GetActorActionsNew();
	if (!nwAction)
		return false;

	if (pOwnerActor->IsPlayer())
	{
		if (!nwAction->PlayerCanDoAnyAgressiveActions())
			return false;
	}

	if (!nwAction->ActorCanDoAnyAgressiveActions(pOwnerActor->GetEntityId(), GetEntityId()))
		return false;

	if(pOwnerActor->m_lft_hand_blc)
		return false;

	if (activationMode == eAAM_OnPress)
	{
		right_butt_pressed = true;
		if (left_butt_pressed)
		{
			if (spec_melee_attack_simple_delay == 0.0f && spec_melee_attack_simple_recoil == 0.0f)
				nwAction->StartFistsMeleeAction(pOwnerActor->GetEntityId(), GetEntityId(), "melee_fists_attack_lgt_upperc", 0.2f, true, 4.0f, 0.3f, 7);
		}
	}

	if (activationMode == eAAM_OnRelease)
	{
		right_butt_pressed  = false;

		if (spec_melee_attack_simple_delay == 0.0f && spec_melee_attack_simple_recoil == 0.0f)
			nwAction->StartFistsMeleeAction(pOwnerActor->GetEntityId(), GetEntityId(), "melee_fists_attack_lgt", 0.2f);
	}

	return true;
}

bool CNoWeapon::OnActionBlockAtk(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	CActor* pOwnerActor = GetOwnerActor();
	if (pOwnerActor)
	{
		CGameInputActionHandlers::TWeaponActionHandler& weaponActionHandler = g_pGame->GetGameInputActionHandlers().GetCWeaponActionHandler();
		weaponActionHandler.Dispatch(this, actorId, actionId, activationMode, value);
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
bool CNoWeapon::UpdateAimAnims(SParams_WeaponFPAiming &aimAnimParams)
{
	if (m_sharedparams->params.hasAimAnims)
	{
		IFireMode* pFireMode = GetFireMode(GetCurrentFireMode());
		aimAnimParams.shoulderLookParams = 
			pFireMode ?
			&static_cast<CFireMode*>(pFireMode)->GetShared()->aimLookParams :
		&m_sharedparams->params.aimLookParams;

		return true;
	}

	return false;
}

bool CNoWeapon::ShouldDoPostSerializeReset() const
{
	return false;
}
