#ifndef __DIALOGSYSTEMEVENTS_H__
#define __DIALOGSYSTEMEVENTS_H__

# pragma once

class CDialogSystemEvents
{
public:
#define MaxQuestions 125
	CDialogSystemEvents();
	virtual void ExecEvent(int eventid, int eventid1, int eventid2, int eventid3, int eventid4, int eventid5, int questionid, string m_currentdialogpath);
	virtual void ExecEventNew(int eventid, int questionid, string m_currentdialogpath, int event_line);
	virtual void AltUpdate();
	virtual void SetPressedId(int id, bool state=true);
	virtual bool GetPressedId(int id);
	virtual bool IsAnyQuestionPressedAtThisTime();
	virtual void ResetPressedAtThisTime();
	virtual void SetOldDs(int ds);
	virtual int GetOldDs();
	bool question_is_pressed[MaxQuestions];
	float f_time;
	int old_ds;
	bool is_any_question_be_pressed;
	// 0 = no event, 1 = add quest, 2 = del quest, 3 = add item or item pack, 4 = del item,
	// 5 = add ally, 6 = del ally, 7 = NPS start follow to player, 8 = NPS stop follow to player, 9 = add friend,
	// 10 = del friend, 11 = complete quest, 12 = complete quest stage, 13 = fail quest, 14 = fail quest stage,
	// 15 = progress or update quest, 16 = add enemy, 17 = del enemy, 18 = create object, 19 = remove object,
	// 20 = activate object, 21 = deactivate object, 22 = quot from dialog, 23 = AI start follow on path, 24 = AI stop follow on path
	// 25 = start trade, 26 = play nps anim, 27 = play player anim, 28 = play nps and player anim, 29 = execute flowgraf structure,
	// 30 = execute script structure, 31 = add sub quest, 32 = update sub quest, 33 = set other npc dialog stage,
	enum EDialogEventTypes
	{
		eDET_no_Event = 0,
		eDET_add_Quest,
		eDET_del_Quest,
		eDET_add_Item,
		eDET_del_item,
		eDET_add_NPC_Ally,
		eDET_del_NPC_Ally,
		eDET_NPC_start_FollowToPlayer,
		eDET_NPC_stop_FollowToPlayer,
		eDET_add_NPC_Friend,
		eDET_del_NPC_Friend,
		eDET_complete_Quest,
		eDET_complete_QuestStage,
		eDET_fail_Quest,
		eDET_fail_QuestStage,
		eDET_update_Quest,
		eDET_add_NPC_Enemy,
		eDET_del_NPC_Enemy,
		eDET_create_Object,
		eDET_remove_Object,
		eDET_activate_Object,
		eDET_deactivate_Object,
		eDET_quot_from_Dialog,
		eDET_NPC_start_FollowOnPath,
		eDET_NPC_stop_FollowOnPath,
		eDET_NPC_start_Traiding,
		eDET_play_NPC_Anim,
		eDET_play_Player_Anim,
		eDET_play_Player_and_NPC_Anim,
		eDET_exec_Flowgraf_Struct,
		eDET_exec_Console_Struct,
		eDET_exec_Script_Struct,
		eDET_add_SubQuest,
		eDET_update_SubQuest,
		eDET_set_NPC_DialogStage,
		eDET_add_gold_to_player
	};

	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};
#endif 