#ifndef __GAMEXMLSETTINGANDGLOBALS_H__
#define __GAMEXMLSETTINGANDGLOBALS_H__

#include <CrySystem/XML/IXml.h>
#include "IGameObject.h"
#include "Actor.h"
#include "Item.h"
#include "Environment/Chest.h"

#define XMLEXTENSION ".xml"

struct SActorBuff
{
	SActorBuff()
	{
		buff_description = "";
		buff_icon_path = "";
		buff_id = 0;
		buff_effect_id = 0;
		buff_type = 0;
		start_time = 0.0f;
		for (int i = 0; i < 10; i++)
		{
			r_val[i] = "";
			r_val_c[i] = "";
		}
	};
	XmlString buff_description;
	XmlString buff_icon_path;
	int buff_id;
	int buff_effect_id;
	int buff_type;
	float start_time;
	XmlString r_val[10];
	XmlString r_val_c[10];
};

class CGameXMLSettingAndGlobals
{
public:
	CGameXMLSettingAndGlobals();
	virtual	~CGameXMLSettingAndGlobals();

	void Init();
	void Serialize(TSerialize ser);
	void Update(float frametime);

	void InitCharacterCustomization();
	void InitPlayerChrAttachmentsBase();
	void InitActorAnimActionDefs();
	void InitCharacterBuffs();
	void InitCharacterSpells(bool preload_all_static_spells_in_mem = true);
	void InitCharacterPerks();
	void InitCharacterSkills();

	void InitCustomGameVars();

	bool IsCharacterCreatingVis();

	void SetDialogMainNpc(EntityId main_npc);
	EntityId GetDialogMainNpc();

	string character_customization_path;
	string character_spells_path;
	string character_buffs_path;
	string character_perks_path;
	string character_skills_path;
	string character_actions_anim_path;
	string items_combinations_def;


	XmlNodeRef CharacterCustomization_node;
	XmlNodeRef CharacterSpells_node;
	XmlNodeRef CharacterBuffs_node;
	XmlNodeRef CharacterPerks_node;
	XmlNodeRef CharacterSkills_node;
	XmlNodeRef CharacterActionsAnims_node;

	int days_left;
	float old_time_od;

	bool enable_character_creating_menu_at_game_start;

	struct SActorSpell
	{
		SActorSpell()
		{
			spell_id = -1;
			for (int i = 0; i != 3; i++)
			{
				//if (i < 3)
				//{
					spell_description[i] = "";
					spell_icon_path[i] = "";
					spell_cast_helper[i] = "";
					spell_cast_bone[i] = "";
					spell_act_scr_func[i] = "";
					spell_act_scr_func_arg[i] = "";
				//}
			}
			start_time = 0.0f;
			spell_damage = 0.0f;
			spell_chargeable = false;
			spell_effect_id = 0;
			spell_type = "";
			for (int i = 0; i < 3; i++)
			{
				for (int ic = 0; ic < 5; ic++)
				{
					spell_particles[i][ic] = "";
					spell_helpers[i][ic] = "";
					spell_lights[i][ic] = 0;
					spell_particles_fp[i][ic] = "";
					spell_helpers_fp[i][ic] = "";
				}
			}
			
			for (int i = 0; i < 15; i++)
			{
				spell_projectles[i] = "";
				spell_projectles_pos_offset[i] = Vec3(ZERO);
				spell_projectles_dir_offset[i] = Vec3(ZERO);
				for (int ic = 0; ic < 15; ic++)
				{
					spell_additive_effects_ids[i][ic] = -1;
				}
			}
			spell_mana_cost = 0.0f;
			spell_hp_cost = 0.0f;
			spell_stmn_cost = 0.0f;
			spell_sub_effect_id = 0;
			spell_force = 0.0f;
			spell_sub_type = 0;
			spell_cast_delay = 0.0f;
			spell_cast_time_to_can_cast = 0.0f;
			spell_cast_max_time_to_charge = 0.0f;
			spell_charge_max_damege_mult = 0.0f;
			spell_cast_recoil = 0.0f;
			spell_charge_spec_effect = 0;
			cast_after_charge_perm = false;
			spell_projectles_dir_offset_randomize = false;
			can_cast_w_weapon = 0;
			apply_weapon_mgc_fcr_mult = false;
			can_move_while_cast = 0;
			spell_cast_action_startcast[0] = "";
			spell_cast_action_chargecast[0] = "";
			spell_cast_action_pre_endcast[0] = "";
			spell_cast_action_endcast[0] = "";
			spell_cast_action_startcast[1] = "";
			spell_cast_action_chargecast[1] = "";
			spell_cast_action_pre_endcast[1] = "";
			spell_cast_action_endcast[1] = "";
			spell_cast_action_startcast[2] = "";
			spell_cast_action_chargecast[2] = "";
			spell_cast_action_pre_endcast[2] = "";
			spell_cast_action_endcast[2] = "";
			spell_name = "";
			two_handed_spell = false;
		}
		int spell_id;
		XmlString spell_description[3];
		XmlString spell_icon_path[3];
		float start_time;
		float spell_damage;
		bool spell_chargeable;
		int spell_effect_id;
		XmlString spell_type;
		XmlString spell_particles[3][5];
		XmlString spell_helpers[3][5];
		XmlString spell_particles_fp[3][5];
		XmlString spell_helpers_fp[3][5];
		XmlString spell_act_scr_func[3];
		XmlString spell_act_scr_func_arg[3];
		XmlString spell_projectles[15];
		Vec3 spell_projectles_pos_offset[15];
		Vec3 spell_projectles_dir_offset[15];
		bool spell_projectles_dir_offset_randomize;
		int spell_lights[3][5];
		float spell_mana_cost;
		float spell_hp_cost;
		float spell_stmn_cost;
		int spell_sub_effect_id;
		int spell_additive_effects_ids[15] [15];
		float spell_force;
		int spell_sub_type;
		float spell_cast_delay;
		float spell_cast_time_to_can_cast;
		float spell_cast_max_time_to_charge;
		float spell_charge_max_damege_mult;
		float spell_cast_recoil;
		int spell_charge_spec_effect;
		bool cast_after_charge_perm;
		int can_cast_w_weapon;
		bool apply_weapon_mgc_fcr_mult;
		bool two_handed_spell;
		int can_move_while_cast;
		XmlString spell_cast_action_startcast[3];
		XmlString spell_cast_action_chargecast[3];
		XmlString spell_cast_action_pre_endcast[3];
		XmlString spell_cast_action_endcast[3];
		XmlString spell_cast_helper[3];
		XmlString spell_cast_bone[3];
		XmlString spell_name;
	};
	std::vector<SActorSpell> s_ActorSpells;
	struct SCharacterCustomizationData
	{
		SCharacterCustomizationData();
		string character_fem_hair[255];
		string character_fem_hair_info[255];
		string character_fem_face[255];
		string character_fem_face_info[255];
		string character_fem_eye_lft[255];
		string character_fem_eye_rgt[255];
		string character_fem_eye_info[255];
		string character_man_hair[255];
		string character_man_hair_info[255];
		string character_man_face[255];
		string character_man_face_info[255];
		string character_man_eye_lft[255];
		string character_man_eye_rgt[255];
		string character_man_eye_info[255];
		string character_base_attachment_foots[3];
		string character_base_attachment_legs[3];
		string character_base_attachment_torso[3];
		string character_base_attachment_hands[3];
		string character_base_attachment_head[3];
		string character_base_attachment_eyes[3];
		string character_fem_skeletion[5];
		string character_man_skeletion[5];
	};

	SCharacterCustomizationData	   customization_data;

	EntityId current_dialog_NPC;
	string current_dialog_path;
	EntityId current_opened_Chest;
	EntityId current_traider;
	EntityId current_traider_chest;
	EntityId current_selected_Item;
	EntityId current_item_to_combine;
	string current_selected_spell;
	int current_selected_spell_ids;

	struct SPlayerQuest
	{
		SPlayerQuest();
		string m_questname;
		string m_questpath;
		string m_sdec;
		string m_ldec;
		int m_queststat;
		int m_lastcn;
		int m_questflag;
		int m_questid;
		float m_complete_percent;
		EntityId m_quest_target_ent_id;
		string m_quest_target_ent_name;
		Vec3 m_quest_target_point;
	};
	std::vector<SPlayerQuest> s_pl_Quests;

	struct SPlayerSubQuest
	{
		SPlayerSubQuest();
		string m_parentquestname;
		string m_questname;
		string m_sdec;
		string m_ldec;
		int m_queststat;
		int m_lastcn;
		int m_questflag;
		int m_questid;
		string m_questpath;
		EntityId m_quest_target_ent_id;
		string m_quest_target_ent_name;
		Vec3 m_quest_target_point;
	};
	std::vector<SPlayerSubQuest> s_pl_SubQuests;
	//quests and subquests operations, .xml quest file required-----------------------------------------------------------------------------------------
	void AddQuest(const char* questpath, int id, int stage, int flag, int last, int quest_lc_st = 1, float compl_percent = 0.0f);
	void DelQuest(const char* questpath, int id);
	void UpdateQuest(const char* questpath, int id, int stage, int flag, int last, int old_std, int old_flg, int old_lst, int quest_lc_st = 1, float compl_percent = 0.0f);

	void AddSubQuest(const char* quest, int id, int stage, int last, int flag = 1, int quest_lc_st = 1);
	void DelSubQuest(const char* quest, int id);
	void UpdateSubQuest(const char* quest, int id, int stage, int flag, int last, int old_std, int old_flg, int old_lst, int quest_lc_st = 1);
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//quests and subquests operations, no .xml quest file required--------------------------------------------------------------------------------------
	void AddQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st = 1, float compl_percent = 0.0f);
	void DelQuestNoXML(string quest_name);
	void UpdateQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st = 1, float compl_percent = 0.0f);

	void AddSubQuestNoXML(string parent_quest_name, string quest_name, string quest_dec[2], int id, int stage, int last, int flag = 1, int quest_lc_st = 1);
	void DelSubQuestNoXML(string quest_name);
	void UpdateSubQuestNoXML(string quest_name, string quest_dec[2], int id, int stage, int flag, int last, int quest_lc_st = 1);
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	float GetQuestCompletePercent(string quest_name);
	void SetQuestCompletePercent(string quest_name, float percent);
	float GetQuestCompletePercentById(int quest_id);
	void SetQuestCompletePercentById(int quest_id, float percent);

	bool IsSubQuestInJournal(string quest_name);
	bool IsQuestInJournal(string quest_name);
	string GetFullQuestPath(const char* quest);

	string GetPerkScrFuncOnUp(int realId);
	string GetPerkScrFuncOnDown(int realId);

	CActor* GetThisActor(EntityId act_id);
	CItem* GetThisItem(EntityId itm_id);

	SActorBuff* GetCharacterBuffInfo(int id);
	SActorSpell GetCharacterSpellInfo(int id, bool from_xml);
	Vec3 GetCurrentPlayerPos();
	Quat GetCurrentPlayerRot();
	CChest *GetThisContainer(EntityId container_id);

	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};

#endif