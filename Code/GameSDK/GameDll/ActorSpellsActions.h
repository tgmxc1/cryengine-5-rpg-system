#ifndef __ACTORSPELLSACTIONS_H__
#define __ACTORSPELLSACTIONS_H__

/*#if _MSC_VER > 1000
# pragma once
#endif*/

#include "Utility/CryHash.h"
#include <SharedParams/ISharedParams.h>
#include "Actor.h"

#define MAX_PER_CAST_SLOTS 3
class CActorSpellsActions
{
public:
	CActorSpellsActions();
	~CActorSpellsActions();
	enum ESpellTypes
	{
		eSpell_type_none = 0,
		eSpell_type_cast_on_self,
		eSpell_type_cast_on_target,
		eSpell_type_cast_combined,
		eSpell_type_cast_on_area,
		eSpell_type_cast_continuosly_on_target,
		eSpell_type_cast_continuosly_on_self
	};
	void Update(float frametime);
	void StartCast(int spell_slot, int forced_cast_slot = 0);
	void CastPerm(int cast_slot);
	void ProcessAOESpell(int cast_slot);
	void AOESpellCastEnd(int cast_slot);
	void UpdateProjectedParticleAttachments(int cast_slot);
	void DeleteProjectedParticleEmitters(int cast_slot);
	void PlayCastAction(const char *action, int cast_slot, EntityId itemId, bool end_of_cast = false);
	void RequestToPreEndCast(int cast_slot, bool forced = false);
	void RequestToEndCurrentCast(int cast_slot, bool all_slots, bool simple_reset, bool with_end_action);
	bool IsCurrentSlotBusy(int cast_slot);
	bool CanCastSpellFromSlot(int cast_slot);
	void OnStartCastSpell(int cast_slot);
	void OnCastSpell(int cast_slot);
	void OnEndCastSpell(int cast_slot);
	void SetUser(CActor* pActor);
	//vars
	int current_cast_slots[MAX_PER_CAST_SLOTS];
	float current_cast_slots_timers[MAX_PER_CAST_SLOTS];
	float current_cast_slots_charge_timers[MAX_PER_CAST_SLOTS];
	float spell_cast_slots_delays[MAX_PER_CAST_SLOTS];
	bool spell_cast_slots_requred_to_cast[MAX_PER_CAST_SLOTS];
	float spell_cast_recoils[MAX_PER_CAST_SLOTS];
	bool is_cast_real_started;
	float current_cast_slots_timers_rts[MAX_PER_CAST_SLOTS];
	int current_cast_aoe_spell[MAX_PER_CAST_SLOTS];
	CActor* pActorUser;
	string particle_attachments_on_user_actor[MAX_PER_CAST_SLOTS];
	IParticleEmitter* lc_emm[MAX_PER_CAST_SLOTS];
	IParticleEmitter* Aoe_emm[MAX_PER_CAST_SLOTS];
	Vec3 Aoe_pos[MAX_PER_CAST_SLOTS];
	std::vector<IParticleEmitter*>emitters_to_delete;
	CItemAction *m_CastSpellActions[MAX_PER_CAST_SLOTS];

	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};

#endif