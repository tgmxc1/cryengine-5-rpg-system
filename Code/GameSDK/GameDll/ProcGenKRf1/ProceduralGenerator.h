#pragma once

#ifndef PROCEDURAL_GENERATOR_H
#define PROCEDURAL_GENERATOR_H


#include <IGameObject.h>
#include <CryGame/IGameVolumes.h>
#define MAX_NUM_GEN_OBJ_TYPE_VAR 5
class CProceduralGenerator : public CGameObjectExtensionHelper<CProceduralGenerator, IGameObjectExtension>, public IInputEventListener
{
public:
	CProceduralGenerator();
	~CProceduralGenerator();

	struct SQuedGenObject
	{
		SQuedGenObject()
		{
			object_pos = Vec3(ZERO);
			object_rot = Quat(IDENTITY);
			object_scale = Vec3(ZERO);
			object_type = 0;
			object_slot = 0;
			for (int i = 0; i < 256; i++)
			{
				object_mdl[i] = 0;
			}
			physics_enable = false;
			stream_sector_id = 0;
			vDistRate[0] = 50;
			vDistRate[1] = 50;
		}

		~SQuedGenObject()
		{

		}

		Vec3 object_pos;
		Quat object_rot;
		Vec3 object_scale;
		uint8 object_type;
		uint8 object_slot;
		char object_mdl[256];
		bool physics_enable;
		uint32 stream_sector_id;
		uint8 vDistRate[2];
	};

	struct SGenerationSector
	{
	//public:
		SGenerationSector()
		{
			sector_start_point = Vec2i(ZERO);
			sector_center_point = Vec2(ZERO);
			sector_size = 128;
			sector_gen_oblects.clear();
			sector_succesfully_gen_oblects.clear();
			sector_succesfully_gen_entites.clear();
			generated_complete = false;
			number_of_loaded_objects_in_sector = 0;
			loading_xml_complete = false;
		}

		~SGenerationSector()
		{
			sector_gen_oblects.clear();
			sector_succesfully_gen_oblects.clear();
			sector_succesfully_gen_entites.clear();
			sector_gen_oblects.resize(0);
			sector_succesfully_gen_oblects.resize(0);
			sector_succesfully_gen_entites.resize(0);
		}

		void SetSectorSize(const int &size) { sector_size = size; }
		void SetSectorStartPoint(const Vec2i &point) { sector_start_point = point; }
		void SetSectorCenterPoint(const Vec2 &point) { sector_center_point = point; }
		int GetSectorId() { return sector_id; }
		bool IsGenerationComplete() { return generated_complete; }
		void SetGenerationComplete(const bool &compLete) { generated_complete = compLete; }
		void AddSectorObjectToGeneration(const SQuedGenObject &object) { sector_gen_oblects.push_back(object); }
		void SetNumberOfLoadedObjects(const int &numBer) { number_of_loaded_objects_in_sector = numBer; }
		uint32 GetNumberOfLoadedObjects() { return number_of_loaded_objects_in_sector; }
		uint32 GetNumberOfGeneratedObjects() { return sector_gen_oblects.size(); }
		std::vector<SQuedGenObject> sector_gen_oblects;
		std::vector<IRenderNode *> sector_succesfully_gen_oblects;
		std::vector<EntityId> sector_succesfully_gen_entites;
	//private:
		Vec2i sector_start_point;
		Vec2 sector_center_point;
		int sector_size;
		int sector_id;
		bool generated_complete;
		bool loading_xml_complete;
		uint32 number_of_loaded_objects_in_sector;
	};

	struct SLoadedSector
	{
		SLoadedSector()
		{
			sec_id = 0;
		}

		~SLoadedSector()
		{

		}
		void SetSectorId(int id) { sec_id = id; }
		int GetSectorId() { return sec_id; }
		int sec_id;
	};

	struct SGenSettingData
	{
		SGenSettingData()
		{
			for (int i = 0; i < MAX_NUM_GEN_OBJ_TYPE_VAR; i++)
			{
				decals[i] = "";
				brushes[i] = "";
				arc_entites[i] = "";
				vegetations[i] = "";
				surf_ids[i] = 0;
				veg_inds[i] = 0;
				brush_params[i] = 0;
			}

			for (int i = 0; i < 2; i++)
			{
				obj_view_params[i] = 0;
			}

			decal_gen = false;
			brushes_gen = false;
			arc_ents_gen = false;
			terrain_surf_ids_gen = false;
			vegetation_gen = false;
			vegetations_gen_coof = 0;
			vegetations_base_scale = 0;
			vegetations_rnd_scale = 0;
			vegetations_gen_up_ht = 0;
			vegetations_gen_rnd_pwr = 0;
			gen_vegetations_floating = false;
			gen_vegetations_rnd_rot_full = false;
			decals_gen_coof = 0;
			decals_aval_num = 0;
			decals_base_scale = 0;
			decals_rnd_scale = 0;
			decals_gen_up_ht = 0;
			decals_gen_rnd_pwr = 0;
			gen_updwn = 0;
			gen_up_from_nrm = 0;
			gen_arr_dist_points_for_all = false;
			gen_brushes_floating = false;
			gen_brushes_rnd_rot_full = false;
			gen_brushes_base_scale = 0.0f;
			gen_brushes_rnd_scale = 0.0f;
			gen_brushes_up_hgt = 0.0f;
			gen_brushes_rnd_pwr = 0.0f;
			gen_brushes_coof = 0;
			gen_arc_ent_coof = 0;
			gen_surf_coof = 0;
			gen_arc_ent_floating = false;
			gen_arc_ent_rnd_rot_full = false;
			gen_arc_ent_base_scale = 0.0f;
			gen_arc_ent_rnd_scale = 0.0f;
			gen_arc_ent_up_hgt = 0.0f;
			gen_arc_ent_rnd_pwr = 0.0f;
			gen_surf_rnd_pwr = 0.0f;
			fMinHeight = 0.0f;
			fMaxHeight = 11111.0f;
			fMinSlope = 0.0;
			fMaxSlope = 180.0;
			veg_atm = false;
			veg_bending = 0.0f;
			z_gen_rot_rand_val = 0.0f;
			obj_view_params[2];
			gen_olly_on_terrain = false;
			gen_on_stat_objs = 1;
			min_dist_gen_old = 0.0f;
			min_dist_gen_old_rnd = 0.0f;
			normal_rot_mult_rnd = 0.0f;
			normal_rot_mult_x = 1.0f;
		}

		~SGenSettingData()
		{

		}

		string decals[MAX_NUM_GEN_OBJ_TYPE_VAR];
		string brushes[MAX_NUM_GEN_OBJ_TYPE_VAR];
		string arc_entites[MAX_NUM_GEN_OBJ_TYPE_VAR];
		string vegetations[MAX_NUM_GEN_OBJ_TYPE_VAR];
		int surf_ids[MAX_NUM_GEN_OBJ_TYPE_VAR];
		int veg_inds[MAX_NUM_GEN_OBJ_TYPE_VAR];
		int brush_params[MAX_NUM_GEN_OBJ_TYPE_VAR];
		bool decal_gen;
		bool brushes_gen;
		bool arc_ents_gen;
		bool terrain_surf_ids_gen;
		bool vegetation_gen;
		float vegetations_gen_coof;
		float vegetations_base_scale;
		float vegetations_rnd_scale;
		float vegetations_gen_up_ht;
		float vegetations_gen_rnd_pwr;
		bool gen_vegetations_floating;
		bool gen_vegetations_rnd_rot_full;
		float decals_gen_coof;
		int decals_aval_num;
		float decals_base_scale;
		float decals_rnd_scale;
		float decals_gen_up_ht;
		float decals_gen_rnd_pwr;
		int gen_updwn;
		float gen_up_from_nrm;
		bool gen_arr_dist_points_for_all;
		bool gen_brushes_floating;
		bool gen_brushes_rnd_rot_full;
		float gen_brushes_base_scale;
		float gen_brushes_rnd_scale;
		float gen_brushes_up_hgt;
		float gen_brushes_rnd_pwr;
		float gen_brushes_coof;
		float gen_arc_ent_coof;
		float gen_surf_coof;
		bool gen_arc_ent_floating;
		bool gen_arc_ent_rnd_rot_full;
		float gen_arc_ent_base_scale;
		float gen_arc_ent_rnd_scale;
		float gen_arc_ent_up_hgt;
		float gen_arc_ent_rnd_pwr;
		float gen_surf_rnd_pwr;
		float fMinHeight;
		float fMaxHeight;
		float fMinSlope;
		float fMaxSlope;
		bool veg_atm;
		float veg_bending;
		float z_gen_rot_rand_val;
		int obj_view_params[2];
		bool gen_olly_on_terrain;	
		int gen_on_stat_objs;
		float min_dist_gen_old;
		float min_dist_gen_old_rnd;
		float normal_rot_mult_rnd;
		float normal_rot_mult_x;
	};

	// IGameObjectExtension
	virtual bool Init(IGameObject * pGameObject);
	virtual void InitClient(int channelId) {}
	virtual void PostInit(IGameObject * pGameObject);
	virtual void PostInitClient(int channelId) {}
	virtual bool ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params);
	bool GetVolumeInfoForEntityGen(EntityId entityId, IGameVolumes::VolumeInfo * volumeInfo);
	virtual void PostReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params) {}
	virtual bool GetEntityPoolSignature(TSerialize signature) { return false; }
	virtual void Release();
	virtual void FullSerialize(TSerialize ser) {};
	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) { return false; }
	virtual void PostSerialize() {}
	virtual void SerializeSpawnInfo(TSerialize ser) {}
	virtual ISerializableInfoPtr GetSpawnInfo() { return nullptr; }
	virtual void Update(SEntityUpdateContext& ctx, int slot);
	//----------Input Listner---------------------------------------
	void RegisterInputListner(bool bRegister);
	virtual bool OnInputEvent(const SInputEvent& event);
	//----------~Input Listner---------------------------------------
	virtual void HandleEvent(const SGameObjectEvent& gameObjectEvent) {}
	virtual void ProcessEvent(SEntityEvent& entityEvent) {}
	virtual void SetChannelId(uint16 id) {}
	virtual void SetAuthority(bool auth) {}
	virtual void PostUpdate(float frameTime);
	virtual void PostRemoteSpawn() {}
	virtual void GetMemoryUsage(ICrySizer *pSizer) const;
	// ~IGameObjectExtension

	uint32 m_TimerGen = 0;
	void DelayedGenObjects(void* pUserData, IGameFramework::TimerID handler);

	void GenObjects();
	void GenObjectsAtTerrainPoint(Vec2 point,float genPower, float cellScale);
	void GenObjectsOnSpline();
	void GenObjectsOnShapeBorder();

	void SaveObjectsInXML();

	void SaveStreamingSectorsInXML(const string &path);
	void SaveStreamingSectorsSlicedInXMLFiles(const string &path);

	string GenerateRNDGUID();
	AABB GetRandomAABBFromTriagleInShape(const std::vector<Vec3>& polygon);
	Vec3 GetRandomPointInShape(const std::vector<Vec3>& polygon, float minz, float maxz);

	Vec3 GetRandomPointInSphere(Vec3 point, float radius);

	void TriangulatePolygon2D(const Vec2* pPolygon, const int numPolyPts, PodArray<Vec2>* pVertices, PodArray<uint8>* pIndices);
	bool PointInTriangle2D(const Vec2& pt, const Vec2& a, const Vec2& b, const Vec2& c);
	bool TriangleIsConvex2D(const Vec2& a, const Vec2& b, const Vec2& c);

	string GenerateNodeXName(string fts_nm);

	void UpdateGenDirection();

	void UpdateStreamingProcess();

	void DoPaint(Vec3 point, float radius);

	void DoEraseObjectsAtPont(Vec3 point, float radius);

	bool IsValidSlopeAndHeight(const Vec3 &point, const Vec3 &normal, const float &slopeMin, const float &slopeMax, const float &heightMin, const float &heightMax);

	void ReconstructSplineFromShape();

	void ClearGeneratedRenderNodes();
	void ClearGeneratedRenderNodesInSector(const int &sectorIds, const bool &delayed = false);
	void ClearGeneratedRenderNodesInAllSectors();
	bool IsGeneratedRenderNodeAreadyInArray(IRenderNode *pNode);
	Quat GenerateObjOrientation(const Matrix34 &orig_mtx, const Vec3 &normal);
	bool IsPointNotTooCloseToOldObjs(const Vec3 &point, const float &dist);
	bool FCVPointCLCheck(const Vec3 &point, const Vec3 & point2, const float &dist);
	bool DeleteRenderNodeFromArray(IRenderNode *pNode);
	bool DeleteEntityNodeFromArray(EntityId ids);

	void GenerateObjectsFromXMLFile(string path);

	void LoadXMLStreamFileInSector(SGenerationSector *pSgen_sect);
	void UnloadXMLStreamFileFromSector(SGenerationSector *pSgen_sect);

	void GreateObjectFromQue(const SQuedGenObject &object_q, int sectorIds = -1);

	void AddObjectToSectorQue(const SQuedGenObject &object_q, const int &sectorIds);
	void GenerateObjectsInSector(const int &sectorIds);
	void GenerateObjectInSector(SGenerationSector *pSgen_sect, const uint32 &objectIds);
	void DeleteObjectFromSector(SGenerationSector *pSgen_sect, const uint32 &objectIds);
	SGenerationSector* GetStreamingSectorPointer(const int &sectorIds);

	sint16 GetVegetationGrpId(const string &vgName);

	void SetGeneratedState(bool state);
	Vec3 GenerateVecRandomValue(const Vec3 &min, const Vec3 &max);
	void AddObjToGenQue(SQuedGenObject object);

	void GetGenShapeSpline(std::vector<Vec3>& spline);
	bool IsSplineBased() { return (is_SplineGen || is_ShapeGen); }

	int GetNearestStreamingSector(const Vec2 & point, const int & sectorSize);
	bool IsGenerationManagerThreadUsed() { return use_generation_manager_thread; }
	void ProcessCurrentSectors();
	void RemoveUnloadedSectorFromProcessing(const int &idSx);
	bool IsSectorInUnloadedSectors(const int &idSx);
	void RefillGenerationSettingData();
	string GetStreamSectorFilepath(const int &sectorIds);
	uint32 GetSectorsNTNumber();
	uint32 GetSectorsCurrentlyNumber() {return generated_sectors.size();}
private:
	std::vector<IRenderNode *> succesfully_gen_oblects;
	std::vector<EntityId> succesfully_gen_entites;
	float upd_timer_c1;
	bool request_regenerate;
	bool old_upd_prm_refr;
	bool already_gen;
	int veg_grps[5];
	Vec3 gen_direction;
	bool is_SplineGen;
	bool is_ShapeGen;
	int bmp_loaded_wh[2];
	std::vector<Vec3> xrf_gen_shape;
	Vec3 current_mouse_world_pos;
	Vec3 current_mouse_world_pos2;
	Vec3 current_mouse_world_pos_projected;
	bool is_paint_started;
	bool is_erase_started;
	bool is_que_enabled;
	int que_perframe_limit;
	bool is_streaming_sectors_enabled;
	bool use_generation_manager_thread;
	bool generation_setting_data_refilled;
	bool load_objects_in_streamFromStreamingGenFiles;
	int old_nearest_steaming_segment;
	int streaming_cover_size;
	int sector_size;
	uint64 generated_objects_in_stream_rd;
	Vec3 paint_pos[2];
	Vec3 center_point;
	Vec2 generatorWorldPoint;
	std::queue<SQuedGenObject> gen_objects_queue;
	std::vector<SGenerationSector*> generated_sectors;
	std::vector<int> loaded_sectors_ids;
	std::vector<int> unloading_sectors_ids;
	SGenSettingData setting_data;
};



#endif
