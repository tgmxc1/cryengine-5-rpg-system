#include "GenerationManager.h"

CGenerationManager::CGenerationManager()
{
	ProcGen_Thread = nullptr;
	already_initialised = false;
	m_cn_p_Generators.clear();
	m_cn_p_Generators.resize(0);
}

CGenerationManager::~CGenerationManager()
{
	if (ProcGen_Thread)
	{
		ProcGen_Thread->SignalStopWork();
		ProcGen_Thread->SetGenerationMN(NULL);
		if (gEnv->pThreadManager && gEnv->pThreadManager->JoinThread(ProcGen_Thread, eJM_Join))
		{
			SAFE_DELETE(ProcGen_Thread);
		}
		ProcGen_Thread = nullptr;
	}
	m_cn_p_Generators.clear();
	m_cn_p_Generators.resize(0);
}

void CGenerationManager::InitManager()
{
	if (already_initialised)
		return;

	if (ProcGen_Thread != nullptr)
		return;

	ProcGen_Thread = new CGenerationThread();
	if (!gEnv->pThreadManager->SpawnThread(ProcGen_Thread, "GenerationThread"))
	{
		CryFatalError("Error spawning \"GenerationThread\" thread.");
	}

	if (ProcGen_Thread != nullptr)
	{
		ProcGen_Thread->SignalStartWork();
		ProcGen_Thread->SetGenerationMN(this);
	}
	already_initialised = true;
}

void CGenerationManager::UpdateManager()
{
	if (ProcGen_Thread == nullptr)
		return;

	ProcGen_Thread->ResetUpdatesCounter();
}

void CGenerationManager::UpdateGenerators()
{
	std::vector<CProceduralGenerator *>::const_iterator it = m_cn_p_Generators.begin();
	std::vector<CProceduralGenerator *>::const_iterator end = m_cn_p_Generators.end();
	uint32 count = m_cn_p_Generators.size();
	if (count <= 0)
		return;

	for (int i = 0; i < count, it != end; i++, ++it)
	{
		if ((*it) != nullptr)
		{
			if (!(*it)->IsGenerationManagerThreadUsed())
				continue;

			if ((*it)->GetSectorsCurrentlyNumber() > 0 && (*it)->GetSectorsCurrentlyNumber() == (*it)->GetSectorsNTNumber())
			{
				(*it)->UpdateStreamingProcess();
			}
			//(*it)->ProcessCurrentSectors();
		}
	}
}

bool CGenerationManager::IsGeneratorAlreadyInManager(CProceduralGenerator * prcGenerator)
{
	std::vector<CProceduralGenerator *>::const_iterator it = m_cn_p_Generators.begin();
	std::vector<CProceduralGenerator *>::const_iterator end = m_cn_p_Generators.end();
	int count = m_cn_p_Generators.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (prcGenerator == (*it))
			{
				return true;
			}
		}
	}
	return false;
}

void CGenerationManager::AddGeneratorToManager(CProceduralGenerator * prcGenerator)
{
	if(!IsGeneratorAlreadyInManager(prcGenerator))
		m_cn_p_Generators.push_back(prcGenerator);
}

void CGenerationManager::DelGeneratorFromManager(CProceduralGenerator * prcGenerator)
{
	std::vector<CProceduralGenerator *>::const_iterator it = m_cn_p_Generators.begin();
	std::vector<CProceduralGenerator *>::const_iterator end = m_cn_p_Generators.end();
	uint32 count = m_cn_p_Generators.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			CProceduralGenerator *pLsc1 = (*it);
			if (pLsc1 == prcGenerator)
			{
				m_cn_p_Generators.erase(it);
				m_cn_p_Generators.resize(count - 1);
			}
		}
	}
}

void CGenerationManager::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
	s->AddContainer(m_cn_p_Generators);
}

void CGenerationManager::CGenerationThread::ThreadEntry()
{
	while (m_threadRunning)
	{
		if (pGMN == NULL)
		{
			CrySleep(111);
			continue;
		}

		if (!gEnv->pGame || !gEnv->pGame->GetIGameFramework() || !gEnv->pGame->GetIGameFramework()->IsGameStarted())
		{
			CrySleep(20);
			continue;
		}

		if (per_frame_thread_updates < 32001)
			++per_frame_thread_updates;

		if (pGMN->IsManagerIntialized())
			pGMN->UpdateGenerators();

		CrySleep(20);
	}
}
