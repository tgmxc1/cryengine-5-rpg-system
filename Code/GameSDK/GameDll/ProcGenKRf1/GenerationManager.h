#ifndef __GENERATION_MANAGER_H__
#define __GENERATION_MANAGER_H__

#include <CryMemory/CrySizer.h>
#include <CrySystem/ISystem.h>
#include <CrySystem/ICmdLine.h>

#include <CryGame/IGameFramework.h>
#include <CryThreading/IThreadManager.h>
#include "ProcGenKRf1/ProceduralGenerator.h"
//GenerationManager singletion class
class CGenerationManager
{
	class CGenerationThread : public IThread
	{
	public:
		CGenerationThread()
		{
			m_threadRunning = true;
			per_frame_thread_updates = 0;
			pGMN = NULL;
		}

		~CGenerationThread()
		{
			m_threadRunning = false;
		}

		// Start accepting work on thread
		virtual void ThreadEntry();

		// Signals the thread that it should not accept anymore work and exit
		void SignalStopWork()
		{
			m_threadRunning = false;

		}

		void SignalStartWork()
		{
			m_threadRunning = true;
		}

		void ResetUpdatesCounter()
		{
			per_frame_thread_updates = 0;
		}

		int GetUpdatesCounter()
		{
			return per_frame_thread_updates;
		}

		void SetGenerationMN(CGenerationManager*gmn)
		{
			pGMN = gmn;
		}

	private:
		volatile bool m_threadRunning;
		int per_frame_thread_updates;
		CGenerationManager*pGMN;
	};
public:
	CGenerationManager();
	~CGenerationManager();

	void InitManager();
	void UpdateManager();
	void UpdateGenerators();
	bool IsManagerIntialized() {return already_initialised;}
	bool IsGeneratorAlreadyInManager(CProceduralGenerator* prcGenerator);
	void AddGeneratorToManager(CProceduralGenerator* prcGenerator);
	void DelGeneratorFromManager(CProceduralGenerator* prcGenerator);
	CGenerationThread* ProcGen_Thread;
	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
private:
	std::vector<CProceduralGenerator*> m_cn_p_Generators;
	bool already_initialised;
};

#endif