#include "StdAfx.h"
#include "SpellProjectile.h"
#include "Game.h"
#include "Projectile.h"
#include "Bullet.h"
#include "NetInputChainDebug.h"
#include "Chaff.h"
#include <CryAISystem/IAIActor.h>
#include "AI/GameAISystem.h"


//------------------------------------------------------------------------
CSpellProjectile::CSpellProjectile() :
m_stuck_c(false),
m_stickyProjectile(true),
m_safeExplosion(0),
m_skipWater(false),
time_to_destroy(0.0f)
{
	m_launchLoc=Vec3(0, 0, 0);
	
	for (int i = 0; i < 15; i++)
	{
		additive_effects_on_hit[i] = -1;
		additive_effects_times[i] = 0.0f;
	};

	fnc_param_on_hit[1] = "";
	fnc_param_on_hit[2] = "";
	damage = 0.0f;
	start_damage = 0.0f;
	trail_enable = false;
	exploded = false;
	is_lightning_child = false;
	enable_wave_phy_force = 0;
	enable_lighting_bolt_params = 0;
	enable_projectile_dmg_area = 0;
	enable_projectile_targeting = 0;
	spell_frc = 1.0f;
	entity_to_ignore_hit = 0;
	m_targetId = 0;
	wave_timer_val1 = 0.0f;
	old_prj_pos = Vec3(0, 0, 0);
	old_prj_pos2 = Vec3(0, 0, 0);
	old_prj_pos3 = Vec3(0, 0, 0);
	m_destination = Vec3(0, 0, 0);
	m_isCruising = false;
	m_isDescending = false;
	m_have_target = false;
	//m_lightProjectile = NULL;
	//m_stuck = false;
}

//------------------------------------------------------------------------
CSpellProjectile::~CSpellProjectile()
{
	//if (m_lightProjectile)
	//	delete m_lightProjectile;
}

//------------------------------------------------------------------------
/*bool CSpellProjectile::Init(IGameObject *pGameObject)
{
	if (CProjectile::Init(pGameObject))
	{
		//m_skipWater = GetParam("ignoreWater", m_skipWater);
		return true;
	}

	return false;
}*/

void CSpellProjectile::Update(SEntityUpdateContext &ctx, int updateSlot)
{
	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);
	BaseClass::Update(ctx, updateSlot);
	if (!trail_enable)
	{
		//TrailEffect(true);
		TrailSpellEffect(true);
		TrailSound(true);
		trail_enable = true;
	}

	if (time_to_destroy > 0)
	{
		time_to_destroy -= ctx.fFrameTime;
		if (time_to_destroy <= 0.0f)
		{
			time_to_destroy = -1.0f;
			RequestDelayedDestroy(true);
		}
		return;
	}

	if (!m_exploded)
	{
		if (!gEnv->pGame->GetIGameFramework()->IsGameStarted())
			return;

		if (!m_pAmmoParams)
			return;

		if (enable_projectile_dmg_area == 0)
		{
			if (m_pAmmoParams->pSpellParams)
			{
				for (int i = 0; i < 15; i++)
				{
					if (additive_effects_on_hit[i] == 788880)
					{
						enable_projectile_dmg_area = 1;
					}
					int sp_ef = m_pAmmoParams->pSpellParams->projectile_spec_effects_on_start[i];
					if (sp_ef == 788880)
					{
						enable_projectile_dmg_area = 1;
					}
				}

				if (enable_projectile_dmg_area == 0)
					enable_projectile_dmg_area = 3;
			}
		}

		if (enable_projectile_targeting == 0)
		{
			if (m_pAmmoParams->pSpellParams)
			{
				for (int i = 0; i < 15; i++)
				{
					if (additive_effects_on_hit[i] == 788881)
					{
						enable_projectile_targeting = 1;
					}
					int sp_ef = m_pAmmoParams->pSpellParams->projectile_spec_effects_on_start[i];
					if (sp_ef == 788881)
					{
						enable_projectile_targeting = 1;
					}
				}

				if (enable_projectile_targeting == 0)
					enable_projectile_targeting = 3;
			}
		}

		if (enable_projectile_dmg_area == 1)
			UpdateProjectileDamageArea();

		if (enable_projectile_targeting == 1)
			UpdateProjectileTargeting();
	}

	if (enable_wave_phy_force == 0)
	{
		bool pressure_effect_enable = false;
		for (int i = 0; i < 15; i++)
		{
			//special physical effect wave pressure id = 788876
			if (additive_effects_on_hit[i] == 788876)
			{
				pressure_effect_enable = true;
				//CryLogAlways("additive_effects_on_hit[i] == 788876");
			}

			if (m_pAmmoParams->pSpellParams)
			{
				int sp_ef = m_pAmmoParams->pSpellParams->projectile_spec_effects_on_start[i];
				if (sp_ef == 788876)
				{
					pressure_effect_enable = true;
				}
			}
		}

		if (pressure_effect_enable)
		{
			enable_wave_phy_force = 1;
			wave_timer_val1 = 3.0f * spell_frc;
		}
		else
		{
			enable_wave_phy_force = 2;
		}
	}
	else if (enable_wave_phy_force == 1)
	{
		if(GetEntity())
			old_prj_pos = GetEntity()->GetPos();

		if (wave_timer_val1 > 0.0f)
		{
			wave_timer_val1 -= ctx.fFrameTime;
			if (wave_timer_val1 > 0.0f)
			{
				if (m_pAmmoParams && m_pAmmoParams->pSpellForceWaveProjectileParams)
				{
					float speed = m_pAmmoParams->pSpellForceWaveProjectileParams->speed * spell_frc;
					float force = m_pAmmoParams->pSpellForceWaveProjectileParams->force * spell_frc;
					float amplitude = m_pAmmoParams->pSpellForceWaveProjectileParams->amplitude * spell_frc;
					float rangeMin = m_pAmmoParams->pSpellForceWaveProjectileParams->rangeMin * spell_frc;
					float rangeMax = m_pAmmoParams->pSpellForceWaveProjectileParams->rangeMax * spell_frc;
					float decay = m_pAmmoParams->pSpellForceWaveProjectileParams->decay * spell_frc;
					float duration = m_pAmmoParams->pSpellForceWaveProjectileParams->duration * spell_frc;
					Vec3 normal = old_prj_pos - m_launchLoc;
					normal.normalize();
					ImpulseWave(wave_timer_val1, normal, speed, force, amplitude, decay, rangeMin, rangeMax, duration);
				}
			}
		}
	}

	if (enable_lighting_bolt_params == 0)
	{
		bool lighting_effect_enable = false;
		for (int i = 0; i < 15; i++)
		{
			//special lighting bolt effect id = 788877
			if (additive_effects_on_hit[i] == 788877)
			{
				lighting_effect_enable = true;
			}

			if (additive_effects_on_hit[i] == 788878)
			{
				enable_lighting_bolt_params = 3;
			}

			if (m_pAmmoParams->pSpellParams)
			{
				int sp_ef = m_pAmmoParams->pSpellParams->projectile_spec_effects_on_start[i];
				if (sp_ef == 788877)
				{
					lighting_effect_enable = true;
				}
				else if (sp_ef == 788878)
				{
					enable_lighting_bolt_params = 3;
				}
			}
		}

		if (enable_lighting_bolt_params != 3)
		{
			if (lighting_effect_enable)
			{
				enable_lighting_bolt_params = 1;
				//if (!m_lightProjectile)
				//	m_lightProjectile = new CLightningBolt;
			}
			else
			{
				enable_lighting_bolt_params = 2;
			}
		}
	}
	else if (enable_lighting_bolt_params == 1)
	{
		if (is_lightning_child)
		{
			if (damage < 0.0f)
			{
				TrailSpellEffect(false);
				StopLightningChildEffect();
			}
			return;
		}

		if (damage > 0.0f)
			UpdateLightningEffect(ctx.fFrameTime);
		else
		{
			TrailSpellEffect(false);
			Destroy();
		}
	}
	else if (enable_lighting_bolt_params == 3)
	{
		if (is_lightning_child)
		{
			if (damage < 0.0f)
			{
				TrailSpellEffect(false);
				StopLightningChildEffect();
			}
			return;
		}

		if (damage < 0.0f)
		{
			TrailSpellEffect(false);
			Destroy();
		}
		return;
	}
}
//------------------------------------------------------------------------
void CSpellProjectile::HandleEvent(const SGameObjectEvent &event)
{
	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);
	/*if (enable_lighting_bolt_params == 1)
	{
		if (m_lightProjectile)
			m_lightProjectile->HandleEvent(event);
	}
	else*/
		BaseClass::HandleEvent(event);

	if (event.event == eGFE_OnCollision)
	{
		//CryLogAlways("CSpellProjectile::HandleEvent");
		EventPhysCollision *pCollision = (EventPhysCollision *)event.ptr;
		if (!pCollision)
			return;

		IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
		IEntity *pOwner = gEnv->pEntitySystem->GetEntity(GetOwnerId());
		CActor* pOwneract = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(GetOwnerId()));
		IPhysicalEntity *pIgnore[3] = { 0, 0, 0 };
		int num_ents_to_ignore = 0;
		pIgnore[0] = pOwner ? pOwner->GetPhysics() : 0;
		num_ents_to_ignore += 1;
		pIgnore[1] = GetEntity()->GetPhysics();
		num_ents_to_ignore += 1;
		if (pOwneract)
		{
			if (pOwneract->GetEquippedState(eAESlot_Shield))
			{
				CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
				if (pItem_to_ignore)
				{
					if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
					{
						pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
						num_ents_to_ignore += 1;
					}
				}
			}
			else if (pOwneract->GetEquippedState(eAESlot_Torch))
			{
				CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
				if (pItem_to_ignore)
				{
					if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
					{
						pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
						num_ents_to_ignore += 1;
					}
				}
			}
			else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
			{
				CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
				if (pItem_to_ignore)
				{
					if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
					{
						pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
						num_ents_to_ignore += 1;
					}
				}
			}
		}

		if (pTarget && pTarget->GetPhysics())
		{
			for (int i = 0; i < num_ents_to_ignore; i++)
			{
				if (pIgnore[i] == pTarget->GetPhysics())
					return;
			}
		}

		if (gEnv->bServer && !m_stickyProjectile.IsStuck() && !m_stuck_c)
		{
			if (GetStickable() > 0)
			{
				m_stickyProjectile.Stick(CStickyProjectile::SStickParams(this, pCollision, CStickyProjectile::eO_AlignToVelocity));
				m_stuck_c = true;
			}

			if (pTarget)
			{
				CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if (!pActor_trg)
				{
					CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
					if (!pItem)
					{
						Vec3 hitDir(ZERO);
						if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
						{
							hitDir = pCollision->vloc[0].GetNormalized();
						}

						if (damage > 0.0f)
						{
							GenerateArtificialCollision(this->GetEntity()->GetPhysics(), pTarget->GetPhysics(), pCollision->idmat[0], pCollision->pt,
								pCollision->n, hitDir.GetNormalized() * 3.0f, pCollision->partid[1],
								pCollision->idmat[1], pCollision->iPrim[1], hitDir.GetNormalized() * 2.0f);
						}
					}
					else
					{
						if (pItem->GetItemHealth() <= 0.0f)
						{
							if (GetProjectleRemovingType() > 0)
							{
								if (GetExplodable() > 1 && !exploded)
								{
									CProjectile::SExplodeDesc explodeDesc(true);
									explodeDesc.impact = true;
									explodeDesc.pos = pCollision->pt;
									explodeDesc.normal = pCollision->n;
									explodeDesc.vel = pCollision->vloc[0];
									explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
									Explode(explodeDesc);
								}
								else
								{
									GetEntity()->SetPos(pCollision->pt);
									TrailSpellEffect(false);
									Destroy();
								}
							}
							return;
						}
					}
				}
			}
			//SpawnMaterialEffectOnCollision(pCollision, pTarget);
		}

		

		/*const SCollisionEffectParams* pCollisionFxParams = m_pAmmoParams->pCollision ? m_pAmmoParams->pCollision->pEffectParams : NULL;
		if (pCollisionFxParams)
		{
			if (!pCollisionFxParams->effect.empty())
			{
				CItemParticleEffectCache& particleCache = g_pGame->GetGameSharedParametersStorage()->GetItemResourceCache().GetParticleEffectCache();
				IParticleEffect* pEffect = particleCache.GetCachedParticle(pCollisionFxParams->effect);

				if (pEffect)
				{
					pEffect->Spawn(true, IParticleEffect::ParticleLoc(pCollision->pt, pCollision->n, pCollisionFxParams->scale));
				}
			}
		}*/
	}

	if (event.event == eGFE_OnCollision)
	{
		EventPhysCollision *pCollision = (EventPhysCollision *)event.ptr;
		if (!pCollision)
			return;

		if (pCollision)
		{
			IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
			//Only process hits that have a target
			if (pTarget)
			{
				if (pTarget->GetId() == GetOwnerId())
				{
					return;
				}

				

				Vec3 dir(0, 0, 0);
				if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
					dir = pCollision->vloc[0].GetNormalized();

				float finalDamage = damage + GetPrgDmg();
				const int hitMatId = pCollision->idmat[1];

				Vec3 hitDir(ZERO);
				if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
				{
					hitDir = pCollision->vloc[0].GetNormalized();
				}

				CGameRules *pGameRules = g_pGame->GetGameRules();
				CActor  *pActor_c = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				CActor  *pActor_own = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
				if (pActor_c)
				{
					for (int i = 0; i < 15; i++)
					{
						if (additive_effects_on_hit[i] > -1 && additive_effects_on_hit[i] < 65535)
						{
							pActor_c->AddBuff(additive_effects_on_hit[i], additive_effects_times[i]);
						}
					}

					if (!fnc_param_on_hit[1].empty())
						pActor_c->RequestToRunLuaFncFromActorScriptInstance(fnc_param_on_hit[1].c_str(), fnc_param_on_hit[2].c_str());
				}
				bool ok = true;
				float m_crt_dmg_mult = 1.0f;
				if (pActor_own)
				{
					finalDamage = (finalDamage*(m_crt_dmg_mult + (pActor_own->GetIntelligence()*0.01)));
				}
				ProcessHit(*pGameRules, *pCollision, *pTarget, finalDamage, hitMatId, hitDir);
				if(!pTarget->GetPhysics())
					return;

				if (pTarget->GetPhysics() && pTarget->GetPhysics()->GetType() != PE_PARTICLE)
				{
					if (is_lightning_child)
					{
						m_entitiesLightningAffected.push_back(pTarget->GetId());
						UpdateLightningEffect(0.0f, false, pTarget->GetId());
						TrailSpellEffect(false);
						StopLightningChildEffect();
						return;
					}
					else if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
					{
						m_entitiesLightningAffected.push_back(pTarget->GetId());
						UpdateLightningEffect(0.0f, true, pTarget->GetId());
						TrailSpellEffect(false);
						Destroy();
						return;
					}
				}
				

				if (GetProjectleRemovingType() > 0)
				{
					if (pTarget->GetPhysics() && pTarget->GetPhysics()->GetType() == PE_PARTICLE)
					{
						CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
						CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pProjectile);
						if (pSpellPhy)
						{
							if (pSpellPhy->GetOwnerId() == GetOwnerId())
							{
								return;
							}
							else
							{
								//CProjectile::HandleEvent(event);
								if (GetExplodable() > 1 && !exploded)
								{
									CProjectile::SExplodeDesc explodeDesc(true);
									explodeDesc.impact = true;
									explodeDesc.pos = pCollision->pt;
									explodeDesc.normal = pCollision->n;
									explodeDesc.vel = pCollision->vloc[0];
									explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
									Explode(explodeDesc);
								}
								else
								{
									GetEntity()->SetPos(pCollision->pt);
									TrailSpellEffect(false);
									Destroy();
								}
								return;
							}
						}
					}
					else
					{
						//CProjectile::HandleEvent(event);
						if (GetExplodable() > 1 && !exploded)
						{
							CProjectile::SExplodeDesc explodeDesc(true);
							explodeDesc.impact = true;
							explodeDesc.pos = pCollision->pt;
							explodeDesc.normal = pCollision->n;
							explodeDesc.vel = pCollision->vloc[0];
							explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
							Explode(explodeDesc);
						}
						else
						{
							GetEntity()->SetPos(pCollision->pt);
							TrailSpellEffect(false);
							Destroy();
						}
						return;
					}
				}
			}
			else
			{
				if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
				{
					UpdateLightningEffect(0.0f, true);
					TrailSpellEffect(false);
					Destroy();
					return;
				}

				if (GetProjectleRemovingType() > 0 && GetExplodable() == 0)
				{
					//CProjectile::HandleEvent(event);
					GetEntity()->SetPos(pCollision->pt);
					TrailSpellEffect(false);
					Destroy();
					return;
				}
			}

			if (GetExplodable() > 1 && !exploded)
			{
				if (pTarget && pTarget->GetPhysics() && pTarget->GetPhysics()->GetType() == PE_PARTICLE)
				{
					CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
					if (pProjectile->GetOwnerId() == GetOwnerId())
					{
						return;
					}
				}
				CProjectile::SExplodeDesc explodeDesc(true);
				explodeDesc.impact = true;
				explodeDesc.pos = pCollision->pt;
				explodeDesc.normal = pCollision->n;
				explodeDesc.vel = pCollision->vloc[0];
				explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
				exploded = true;
				//CryLogAlways("CSpellProjectile::Explode called");
				Explode(explodeDesc);
				//return;
			}
		}
	}
}

//------------------------------------------------------------------------
void CSpellProjectile::Launch(const Vec3 &pos, const Vec3 &dir, const Vec3 &velocity, float speedScale)
{
	//CryLogAlways("CSpellProjectile::Launch");
	BaseClass::Launch(pos, dir, velocity, speedScale);
	m_launchLoc=pos;
	if (!trail_enable)
	{
		//TrailEffect(true);
		TrailSpellEffect(true);
		TrailSound(true);
		trail_enable = true;
	}
	m_hitTypeId = GetProjectleDamageType();
	if (m_pAmmoParams->pParticleParams)
	{
		m_pAmmoParams->pParticleParams->surface_idx = m_pAmmoParams->surfaceTypeId;
	}
	//m_safeExplosion = GetParam("safeexplosion", m_safeExplosion);
}

void CSpellProjectile::TrailSpellEffect(bool enable, bool forced)
{
	if (enable)
	{
		const STrailParams* pTrail = m_pAmmoParams->pTrail;
		if (!pTrail)
			return;

		const char* effectName = pTrail->effect.c_str();
		EntityEffects::SEffectAttachParams attachParams(ZERO, FORWARD_DIRECTION, pTrail->scale, pTrail->prime, 1);
		IParticleEffect *pParticle = gEnv->pParticleManager->FindEffect(effectName);
		//pParticle->
		m_spell_trailEffectId = m_projectileEffects.AttachParticleEffect(pParticle, attachParams);
	}
	else
	{
		if (GetProjectleRemovingType() == 2 && !forced)
		{
			return;
		}

		if (!is_lightning_child)
		{
			m_projectileEffects.DetachEffect(m_spell_trailEffectId);
			m_spell_trailEffectId = 0;
		}
	}

}

void CSpellProjectile::RequestDelayedDestroy(bool destroy_immediatle)
{
	if (destroy_immediatle)
	{
		TrailSpellEffect(false, true);
		Destroy(true);
	}
	else
	{
		SetVelocity(GetEntity()->GetPos(), Vec3(0, 0, 0), Vec3(0, 0, 0), 0);
		GetEntity()->SetPos(GetEntity()->GetWorldPos());
		time_to_destroy = 0.01f;
	}
}

void CSpellProjectile::Destroy(bool forced)
{
	if (GetProjectleRemovingType() == 2 && !forced)
	{
		return;
	}
	CProjectile::Destroy();
}

void CSpellProjectile::StopLightningChildEffect()
{
	if (!is_lightning_child)
		return;

	SetVelocity(GetEntity()->GetPos(), Vec3(0, 0, 0), Vec3(0, 0, 0), 0);
	GetEntity()->SetPos(GetEntity()->GetWorldPos());
}

void CSpellProjectile::UpdateLightningEffect(float ftTime, bool explode, EntityId contact_ent)
{
	float dimc = cry_random(0.1f * spell_frc, 1.0f * spell_frc) + damage * 0.01;
	if (explode)
	{
		const SExplosionParams* pExplosionParams = m_pAmmoParams->pExplosion;
		if (pExplosionParams)
		{
			dimc = pExplosionParams->maxRadius;
		}
	}

	if (damage <= 0.0f)
		return;

	Vec3 boxDim = Vec3(dimc, dimc, dimc);
	Vec3 m_effectCenter = GetEntity()->GetWorldPos();
	Vec3 ptmin = m_effectCenter - boxDim;
	Vec3 ptmax = m_effectCenter + boxDim;
	IPhysicalEntity** pEntityList = NULL;
	static const int iObjTypes = ent_rigid | ent_sleeping_rigid | ent_living;// | ent_allocate_list;
	int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pEntityList, iObjTypes);
	AABB bounds;
	for (int i = 0; i<numEntities; ++i)
	{
		IPhysicalEntity* pPhysicalEntity = pEntityList[i];
		IEntity* pEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));

		if (pPhysicalEntity->GetType() == PE_PARTICLE)
			continue;
		// Has the entity already been affected?
		if (pEntity)
		{
			if (pEntity->GetId() == GetOwnerId())
				continue;

			if (pEntity->GetId() == GetEntityId())
				continue;

			if (pEntity->GetId() == contact_ent)
				continue;

			if(stl::find(m_entitiesLightningAffected, pEntity->GetId()))
				continue;

			if (damage <= 0.0f)
			{
				if (!explode || GetExplodable() == 0)
				{
					StopLightningChildEffect();
					if (!is_lightning_child)
						Destroy();
				}
				break;
			}

			IEntityPhysicalProxy* pPhysicalProxy = static_cast<IEntityPhysicalProxy*>(pEntity->GetProxy(ENTITY_PROXY_PHYSICS));
			if (pPhysicalProxy)
			{
				pPhysicalProxy->GetWorldBounds(bounds);
				Vec3 p = bounds.GetCenter();
				Vec3 v = p - m_effectCenter;
				CProjectile *pSpellProjecle = g_pGame->GetWeaponSystem()->SpawnAmmo(gEnv->pEntitySystem->GetClassRegistry()->FindClass("spelllightingsub"), false);
				if (pSpellProjecle)
				{
					float corrected_damage = start_damage / (spell_frc*3.0f);
					damage = damage - corrected_damage;
					if (corrected_damage < 5.0f)
						damage = 0.0f;

					if (damage <= 0.0f)
					{
						StopLightningChildEffect();
						Destroy();
						break;
					}

					CProjectile::SProjectileDesc projectileDesc(GetOwnerId(), 0, 0, corrected_damage, 0, 0, 0, pSpellProjecle->GetHitType(), 0, false);
					pSpellProjecle->SetParams(projectileDesc);
					CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pSpellProjecle);
					if (pSpellPhy)
					{
						float speed_scale = p.GetDistance(m_effectCenter);
						if (speed_scale < 0.3f)
							speed_scale = 0.3f;

						speed_scale = speed_scale / 33.0f;
						Vec3 spell_spwn_pos_npv = m_effectCenter;
						Vec3 spell_spwn_dir_npv = v.normalized();
						Vec3 spell_spwn_vel = spell_spwn_dir_npv * 6.0f;
						pSpellPhy->SetSpellDamage(corrected_damage);
						pSpellPhy->spell_frc = spell_frc;
						pSpellPhy->Launch(spell_spwn_pos_npv, spell_spwn_dir_npv, spell_spwn_vel, speed_scale);
						for (int ic = 0; ic < 15; ic++)
						{																		//exclude wave effect for perfomance
							if (additive_effects_on_hit[ic] > 0 && additive_effects_on_hit[ic] != 788876)
							{
								pSpellPhy->additive_effects_on_hit[ic] = additive_effects_on_hit[ic];
								if (additive_effects_times[ic] > 0.0f)
									pSpellPhy->additive_effects_times[ic] = additive_effects_times[ic];
								else
									pSpellPhy->additive_effects_times[ic] = 0.0f;
							}
						}
						pSpellPhy->fnc_param_on_hit[1] = fnc_param_on_hit[1];
						pSpellPhy->fnc_param_on_hit[2] = fnc_param_on_hit[2];
						pSpellPhy->is_lightning_child = true;
						m_entitiesLightningAffected.push_back(pEntity->GetId());
						pSpellPhy->m_entitiesLightningAffected = m_entitiesLightningAffected;
					}

					if (damage <= 0.0f)
					{
						if (!explode || GetExplodable() == 0)
						{
							StopLightningChildEffect();
							if (!is_lightning_child)
								Destroy();
						}
						break;
						//return;
					}
				}
			}
		}
	}
}

void CSpellProjectile::Explode(const SExplodeDesc& explodeDesc)
{
	TrailEffect(false);
	TrailSound(false);
	const SExplosionParams* pExplosionParams = m_pAmmoParams->pExplosion;
	if (pExplosionParams)
	{
		//CryLogAlways("CSpellProjectile::Explode pExplosionParams is have");
		IPhysicalEntity** ppList = NULL;
		IPhysicalWorld* pWorld = gEnv->pPhysicalWorld;
		Vec3 pt_min(ZERO);
		Vec3 pt_max(ZERO);
		Vec3 pt_pos = GetEntity()->GetPos();//explodeDesc.pos;
		//CryLogAlways("CSpellProjectile::Explode maxExplRad = %f", pExplosionParams->maxRadius);
		pt_min = pt_pos - Vec3(pExplosionParams->maxRadius, pExplosionParams->maxRadius, pExplosionParams->maxRadius);
		pt_max = pt_pos + Vec3(pExplosionParams->maxRadius, pExplosionParams->maxRadius, pExplosionParams->maxRadius);
		/*IRenderer* pRenderer = gEnv->pRenderer;
		if (pRenderer)
		{
			IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
			pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
			ColorB colNormal(200, 0, 0, 128);
			AABB aabb(pt_min, pt_max);
			pAuxGeom->DrawAABB(aabb, false, colNormal, eBBD_Faceted);
		}*/

		const int entityCount = pWorld->GetEntitiesInBox(pt_min, pt_max, ppList, ent_sleeping_rigid | ent_rigid | ent_living | ent_independent);
		for (int i = 0; i < entityCount; ++i)
		{
			IEntity * pEntity = (IEntity*)ppList[i]->GetForeignData(PHYS_FOREIGN_ID_ENTITY);
			if (pEntity)
			{
				//CryLogAlways("CSpellProjectile::Explode ent %s", pEntity->GetName());
				if (pEntity->GetId() != m_ownerId)
				{
					CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
					if (pActor)
					{
						for (int i = 0; i < 15; i++)
						{
							if (additive_effects_on_hit[i] > -1 && additive_effects_on_hit[i] < 65535)
							{
								pActor->AddBuff(additive_effects_on_hit[i], additive_effects_times[i]);
							}
						}

						if (!fnc_param_on_hit[1].empty())
							pActor->RequestToRunLuaFncFromActorScriptInstance(fnc_param_on_hit[1].c_str(), fnc_param_on_hit[2].c_str());
					}
				}
			}
		}
	}

	if (enable_lighting_bolt_params == 1 || enable_lighting_bolt_params == 3)
	{
		UpdateLightningEffect(0.0f, true);
	}

	CProjectile::Explode(explodeDesc);
}

void CSpellProjectile::OnHit(const HitInfo& hit)
{
	BaseClass::OnHit(hit);
}

void CSpellProjectile::GenerateArtificialCollision(IPhysicalEntity* pAttackerPhysEnt, IPhysicalEntity *pCollider, int colliderMatId, const Vec3 &position, const Vec3& normal, const Vec3 &vel, int partId, int surfaceIdx, int iPrim, const Vec3& impulse)
{
	//////////////////////////////////////////////////////////////////////////
	// Apply impulse to object.
	pe_action_impulse ai;
	ai.partid = partId;
	ai.point = position;
	ai.impulse = impulse;
	pCollider->Action(&ai);

	//////////////////////////////////////////////////////////////////////////
	// create a physical collision 
	pe_action_register_coll_event collision;

	collision.collMass = 0.005f; // this needs to be set to something.. but is seemingly ignored in calculations... (just doing what meleeCollisionHelper does here..)
	collision.partid[1] = partId;

	// collisions involving partId<-1 are to be ignored by game's damage calculations
	// usually created articially to make stuff break.
	collision.partid[0] = -2;
	collision.idmat[1] = surfaceIdx;
	collision.idmat[0] = colliderMatId;
	collision.n = normal;
	collision.pt = position;
	collision.vSelf = vel;
	collision.v = Vec3(0, 0, 0);
	collision.pCollider = pCollider;
	collision.iPrim[0] = -1;
	collision.iPrim[1] = iPrim;

	pAttackerPhysEnt->Action(&collision);
}

void CSpellProjectile::ProcessHit(CGameRules& gameRules, const EventPhysCollision& collision, IEntity& target, float damage, int hitMatId, const Vec3& hitDir)
{
	if (damage > 0.f)
	{
		m_hitTypeId = GetProjectleDamageType();
		EntityId targetId = target.GetId();
		HitInfo hitInfo(m_ownerId ? m_ownerId : m_hostId, targetId, m_weaponId,
			damage, 0.0f, hitMatId, collision.partid[1],
			m_hitTypeId, collision.pt, hitDir, collision.n);

		hitInfo.remote = IsRemote();
		hitInfo.projectileId = GetEntityId();
		hitInfo.bulletType = m_pAmmoParams->bulletType;
		hitInfo.knocksDown = false;
		hitInfo.knocksDownLeg = false;
		hitInfo.penetrationCount = 1;
		//hitInfo.partId
		hitInfo.hitViaProxy = CheckAnyProjectileFlags(ePFlag_firedViaProxy);
		hitInfo.aimed = CheckAnyProjectileFlags(ePFlag_aimedShot);
		IPhysicalEntity* pPhysicalEntity = collision.pEntity[1];
		gameRules.ClientHit(hitInfo);
		ReportHit(targetId);
		/*if (is_lightning_child)
		{
			UpdateLightningEffect(0.0f, false, target.GetId());
			//m_entitiesLightningAffected.push_back(pEntity->GetId());
		}*/
	}
}

void CSpellProjectile::ImpulseWave(float elapsed, Vec3 normal, float speed, float force, float amplitude, float decay, float rangeMin, float rangeMax, float duration)
{
	//CryLogAlways("CSpellProjectile::ImpulseWave");
	float maxTime = duration;
	float percent = maxTime > FLT_EPSILON ? elapsed / maxTime : 1.0f;
	if (percent >= 1.0f)
	{
		return;
	}
	Vec3 N = normal;
	const float range = rangeMax - rangeMin;
	Vec3 boxDim(rangeMax, rangeMax, rangeMax);
	Vec3 m_effectCenter = GetEntity()->GetPos();
	Vec3 ptmin = m_effectCenter - boxDim;
	Vec3 ptmax = m_effectCenter + boxDim;

	float waveFront = elapsed * speed;
	if (decay > FLT_EPSILON) decay = 1.0f / decay;
	force = pow_tpl(force * (1 - percent), decay);
	amplitude = pow_tpl(amplitude * (1 - percent), decay);
	IPhysicalEntity** pEntityList = NULL;
	static const int iObjTypes = ent_rigid | ent_sleeping_rigid | ent_living;// | ent_allocate_list;
	int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pEntityList, iObjTypes);
	AABB bounds;
	for (int i = 0; i<numEntities; ++i)
	{
		IPhysicalEntity* pPhysicalEntity = pEntityList[i];
		IEntity* pEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));

		// Has the entity already been affected?
		if (pEntity)
		{
			if (pEntity->GetId() == GetOwnerId())
				continue;

			if (pEntity->GetId() == GetEntityId())
				continue;

			bool affected = stl::find(m_entitiesAffected, pEntity->GetId());
			if (!affected)
			{
				IEntityPhysicalProxy* pPhysicalProxy = static_cast<IEntityPhysicalProxy*>(pEntity->GetProxy(ENTITY_PROXY_PHYSICS));
				if (pPhysicalProxy)
				{
					pPhysicalProxy->GetWorldBounds(bounds);
					Vec3 p = bounds.GetCenter();
					Vec3 v = p - m_effectCenter;
					float distFromCenter = v.GetLength() + FLT_EPSILON;
					if (distFromCenter < rangeMax)
					{
						if (waveFront > distFromCenter) // has the wavefront passed the entity?
						{
							//pPhysicalEntity->GetStatus(&dyn);

							// normalize v, cheaper than another sqrt
							v /= distFromCenter;

							Vec3 dir = N + v * force;
							static bool usePos = false;
							float impulse = 1.0f - (max(0.0f, distFromCenter - rangeMin) / range);
							impulse = impulse * amplitude;// / dyn.mass;
							if (impulse > FLT_EPSILON)
							{
								if (pPhysicalEntity)
								{
									pe_params_part ent_params_c1;
									if (impulse > pPhysicalEntity->GetParams(&ent_params_c1))
									{
										if (ent_params_c1.mass < (impulse * force)|| impulse > g_pGameCVars->g_spellprojectle_min_forcewave_frc_to_knockdown)
										{
											CActor* pActor = (CActor *)gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId());
											if (pActor)
											{
												if (!pActor->GetActorStats()->isRagDoll)
												{
													HitInfo hitInfo;
													hitInfo.pos = p;
													hitInfo.dir = dir * impulse;
													hitInfo.partId = -1;
													if (g_pGameCVars->g_spellprojectle_forcewave_can_knockdown > 0)
													{
														if (pActor->IsPlayer() && g_pGameCVars->g_spellprojectle_forcewave_can_knockdown_players > 0)
														{
															pActor->Fall(hitInfo);
														}
														else if (!pActor->IsPlayer())
														{
															pActor->Fall(hitInfo);
														}
													}

													IAnimatedCharacter* pAnimChar = pActor->GetAnimatedCharacter();
													if (pAnimChar)
													{
														SCharacterMoveRequest request;
														request.type = eCMT_Fly;
														request.rotation = pActor->GetEntity()->GetRotation();
														request.velocity = dir * impulse;
														request.jumping = false;
														pAnimChar->AddMovement(request);
													}

													if(amplitude > g_pGameCVars->g_spellprojectle_max_forcewave_impulse_to_knockdownned_enemy)
														pActor->AddLocalHitImpulse(SHitImpulse(-1, -1, p, dir.normalized() * g_pGameCVars->g_spellprojectle_max_forcewave_impulse_to_knockdownned_enemy, 1.0f));
													else
														pActor->AddLocalHitImpulse(SHitImpulse(-1, -1, p, dir.normalized() * amplitude, 1.0f));
													//m_entitiesAffected.push_back(pEntity->GetId());
													continue;
												}
												else
												{
													if (amplitude > g_pGameCVars->g_spellprojectle_max_forcewave_impulse_to_knockdownned_enemy)
														pPhysicalProxy->AddImpulse(-1, p, dir.normalized() * g_pGameCVars->g_spellprojectle_max_forcewave_impulse_to_knockdownned_enemy, usePos, 1.0f);
													else
														pPhysicalProxy->AddImpulse(-1, p, dir.normalized() * amplitude, usePos, 1.0f);

													m_entitiesAffected.push_back(pEntity->GetId());
													continue;
												}
											}
										}
									}
								}
								pPhysicalProxy->AddImpulse(-1, p, dir * impulse, usePos, 1.0f);
								m_entitiesAffected.push_back(pEntity->GetId());
							}
						}
					}
				}
			}
		}
	}
}

void CSpellProjectile::UpdateProjectileDamageArea()
{
	UpdateProjectileDamageArea2();
	if (!GetEntity())
		return;

	float rng_max = 0.25f;
	if (m_pAmmoParams->pParticleParams)
		rng_max = m_pAmmoParams->pParticleParams->size;

	Vec3 m_effectCenter = GetEntity()->GetPos();
	IEntity *pOwner = gEnv->pEntitySystem->GetEntity(GetOwnerId());
	/*IPhysicalEntity *pIgnore[2];// = pOwner ? pOwner->GetPhysics() : 0;
	pIgnore[0] = pOwner ? pOwner->GetPhysics() : 0;
	pIgnore[1] = GetEntity()->GetPhysics();*/
	CActor* pOwneract = static_cast<CActor *>(gEnv->pGame->GetIGameFramework()->GetIActorSystem()->GetActor(GetOwnerId()));
	IPhysicalEntity *pIgnore[3] = { 0, 0, 0 };
	int num_ents_to_ignore = 0;
	pIgnore[0] = pOwner ? pOwner->GetPhysics() : 0;
	num_ents_to_ignore += 1;
	pIgnore[1] = GetEntity()->GetPhysics();
	num_ents_to_ignore += 1;
	if (pOwneract)
	{
		if (pOwneract->GetEquippedState(eAESlot_Shield))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Shield));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetEquippedState(eAESlot_Torch))
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Torch));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
		else if (pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left) > 0)
		{
			CItem* pItem_to_ignore = pOwneract->GetItem(pOwneract->GetCurrentEquippedItemId(eAESlot_Weapon_Left));
			if (pItem_to_ignore)
			{
				if (pItem_to_ignore->GetEntity() && pItem_to_ignore->GetEntity()->GetPhysics())
				{
					pIgnore[2] = pItem_to_ignore->GetEntity()->GetPhysics();
					num_ents_to_ignore += 1;
				}
			}
		}
	}
	num_ents_to_ignore = 3;
	primitives::sphere sphere;
	sphere.r = rng_max;
	sphere.center = m_effectCenter;

	float n = 0.0f;
	geom_contact *contacts = NULL;
	intersection_params params;
	params.bStopAtFirstTri = false;
	params.bNoBorder = true;
	params.bNoAreaContacts = true;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(sphere.type, &sphere, Vec3(ZERO),
		ent_rigid | ent_sleeping_rigid | ent_independent | ent_living | ent_water, &contacts, 0,
		(rwi_colltype_any | rwi_stop_at_pierceable), &params, 0, 0, pIgnore, num_ents_to_ignore);

	geom_contact *contact = contacts;
	if (contact)
	{
		if (n > (rng_max / 2))
			return;
		//CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea contact finded");
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
		if (pCollider)
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
			if (pEntity)
			{
				//CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea entity finded");
				if (pEntity->GetId() == GetEntityId())
					return;

				if (pEntity->GetId() == m_ownerId)
					return;

				if (GetEntity()->GetPhysics())
				{
					if (pEntity->GetPhysics()->GetType() == GetEntity()->GetPhysics()->GetType())
						return;
				}
				/*CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea start HEvent sending");
				SGameObjectEvent evt = SGameObjectEvent(eGFE_OnCollision, eGOEF_LoggedPhysicsEvent);
				EventPhysCollision epc;
				epc.pEntity[1] = pCollider;
				epc.iForeignData[1] = PHYS_FOREIGN_ID_ENTITY;
				epc.pt = contact->pt; epc.n = contact->n;
				epc.iPrim[0] = contact->iPrim[0]; epc.iPrim[1] = contact->iPrim[1];
				epc.pForeignData[1] = &pEntity;
				evt.ptr = &epc;*/
				//HandleEvent(evt);
				if (gEnv->bServer && !m_stickyProjectile.IsStuck() && !m_stuck_c)
				{
					if (GetStickable() > 0)
					{
						//m_stickyProjectile.Stick(CStickyProjectile::SStickParams(this, pCollision, CStickyProjectile::eO_AlignToVelocity));
						m_stuck_c = true;
					}
					IEntity *pTarget = pEntity;
					if (pTarget)
					{
						CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
						if (!pActor_trg)
						{
							CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
							if (!pItem)
							{
								
							}
							else
							{
								if (pItem->GetItemHealth() <= 0.0f)
								{
									if (GetProjectleRemovingType() > 0)
									{
										if (GetExplodable() > 1 && !exploded)
										{
											CProjectile::SExplodeDesc explodeDesc(true);
											explodeDesc.impact = true;
											explodeDesc.pos = contact->pt;
											explodeDesc.normal = contact->n;
											explodeDesc.vel = contact->dir;
											explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
											Explode(explodeDesc);
										}
										else
										{
											GetEntity()->SetPos(contact->pt);
											TrailSpellEffect(false);
											Destroy();
										}
									}
									return;
								}
							}
						}
					}

					if (pTarget)
					{
						if (pTarget->GetId() == GetOwnerId())
						{
							return;
						}
						Vec3 dir(0, 0, 0);
						float finalDamage = damage + GetPrgDmg();
						const int hitMatId = 0;

						Vec3 hitDir(ZERO);
						if (contact->dir.GetLengthSquared() > 1e-6f)
						{
							hitDir = contact->dir.GetNormalized();
						}

						CGameRules *pGameRules = g_pGame->GetGameRules();
						CActor  *pActor_c = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
						CActor  *pActor_own = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
						if (pActor_c)
						{
							for (int i = 0; i < 15; i++)
							{
								if (additive_effects_on_hit[i] > -1 && additive_effects_on_hit[i] < 65535)
								{
									pActor_c->AddBuff(additive_effects_on_hit[i], additive_effects_times[i]);
								}
							}

							if (!fnc_param_on_hit[1].empty())
								pActor_c->RequestToRunLuaFncFromActorScriptInstance(fnc_param_on_hit[1].c_str(), fnc_param_on_hit[2].c_str());
						}
						bool ok = true;
						float m_crt_dmg_mult = 1.0f;
						if (pActor_own)
						{
							finalDamage = (finalDamage*(m_crt_dmg_mult + (pActor_own->GetIntelligence()*0.01)));
						}
						EventPhysCollision pCollision;
						pCollision.pEntity[1] = pCollider;
						pCollision.iForeignData[1] = PHYS_FOREIGN_ID_ENTITY;
						pCollision.pt = contact->pt; pCollision.n = contact->n;
						pCollision.iPrim[0] = contact->iPrim[0]; pCollision.iPrim[1] = contact->iPrim[1];
						pCollision.pForeignData[1] = &pEntity;
						ProcessHit(*pGameRules, pCollision, *pTarget, finalDamage, hitMatId, hitDir);
						if(!pTarget->GetPhysics())
							return;

						if (pTarget->GetPhysics()->GetType() != PE_PARTICLE)
						{
							if (is_lightning_child)
							{
								m_entitiesLightningAffected.push_back(pTarget->GetId());
								UpdateLightningEffect(0.0f, false, pTarget->GetId());
								TrailSpellEffect(false);
								StopLightningChildEffect();
								return;
							}
							else if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
							{
								m_entitiesLightningAffected.push_back(pTarget->GetId());
								UpdateLightningEffect(0.0f, true, pTarget->GetId());
								TrailSpellEffect(false);
								Destroy();
								return;
							}
						}


						if (GetProjectleRemovingType() > 0)
						{
							if (!pTarget->GetPhysics())
								return;

							if (pTarget->GetPhysics()->GetType() == PE_PARTICLE)
							{
								CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
								CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pProjectile);
								if (pSpellPhy)
								{
									if (pSpellPhy->GetOwnerId() == GetOwnerId())
									{
										return;
									}
									else
									{
										//CProjectile::HandleEvent(event);
										if (GetExplodable() > 1 && !exploded)
										{
											CProjectile::SExplodeDesc explodeDesc(true);
											explodeDesc.impact = true;
											explodeDesc.pos = contact->pt;
											explodeDesc.normal = contact->n;
											explodeDesc.vel = contact->dir;
											explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
											Explode(explodeDesc);
										}
										else
										{
											GetEntity()->SetPos(contact->pt);
											TrailSpellEffect(false);
											Destroy();
										}
										return;
									}
								}
							}
							else
							{
								//CProjectile::HandleEvent(event);
								if (GetExplodable() > 1 && !exploded)
								{
									CProjectile::SExplodeDesc explodeDesc(true);
									explodeDesc.impact = true;
									explodeDesc.pos = contact->pt;
									explodeDesc.normal = contact->n;
									explodeDesc.vel = contact->dir;
									explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
									Explode(explodeDesc);
								}
								else
								{
									GetEntity()->SetPos(contact->pt);
									TrailSpellEffect(false);
									Destroy();
								}
								return;
							}
						}
					}
					else
					{
						//contact->pt.GetDistance();
						if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
						{
							UpdateLightningEffect(0.0f, true);
							TrailSpellEffect(false);
							Destroy();
							return;
						}

						if (GetProjectleRemovingType() > 0 && GetExplodable() == 0)
						{
							//CProjectile::HandleEvent(event);
							GetEntity()->SetPos(contact->pt);
							TrailSpellEffect(false);
							Destroy();
							return;
						}
					}

					if (GetExplodable() > 1 && !exploded)
					{
						if (pTarget && pTarget->GetPhysics() && pTarget->GetPhysics()->GetType() == PE_PARTICLE)
						{
							CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
							if (pProjectile->GetOwnerId() == GetOwnerId())
							{
								return;
							}
						}
						CProjectile::SExplodeDesc explodeDesc(true);
						explodeDesc.impact = true;
						explodeDesc.pos = contact->pt;
						explodeDesc.normal = contact->n;
						explodeDesc.vel = contact->dir;
						explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
						exploded = true;
						Explode(explodeDesc);
					}
					//SpawnMaterialEffectOnCollision(pCollision, pTarget);
				}
				return;
				/*CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea end HEvent sending");
				if (GetExplodable() > 0 || GetProjectleRemovingType() > 0)
				{
					return;
				}*/
			}
		}
	}
	old_prj_pos2 = m_effectCenter;
}

void CSpellProjectile::UpdateProjectileDamageArea2()
{
	if (old_prj_pos2.IsZero())
		return;

	if (!GetEntity())
		return;

	float rng_max = 0.25f;
	if (m_pAmmoParams->pParticleParams)
		rng_max = m_pAmmoParams->pParticleParams->size;

	Vec3 m_effectCenter = GetEntity()->GetPos();
	IEntity *pOwner = gEnv->pEntitySystem->GetEntity(GetOwnerId());
	IPhysicalEntity *pIgnore[2];// = pOwner ? pOwner->GetPhysics() : 0;
	pIgnore[0] = pOwner ? pOwner->GetPhysics() : 0;
	pIgnore[1] = GetEntity()->GetPhysics();
	primitives::cylinder sphere;
	sphere.r = rng_max;
	sphere.center = (m_effectCenter + old_prj_pos2) *0.5f;
	sphere.hh = sphere.center.GetDistance(m_effectCenter);
	sphere.axis = m_effectCenter - sphere.center;

	float n = 0.0f;
	geom_contact *contacts = NULL;
	intersection_params params;
	params.bNoAreaContacts = true;
	n = gEnv->pPhysicalWorld->PrimitiveWorldIntersection(sphere.type, &sphere, Vec3(ZERO),
		ent_rigid | ent_sleeping_rigid | ent_independent | ent_living | ent_water, &contacts, 0,
		(rwi_colltype_any | rwi_stop_at_pierceable), &params, 0, 0, pIgnore, 2);

	geom_contact *contact = contacts;
	if (contact)
	{
		//CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea contact finded");
		IPhysicalEntity *pCollider = gEnv->pPhysicalWorld->GetPhysicalEntityById(contact->iPrim[0]);
		if (pCollider)
		{
			IEntity *pEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
			if (pEntity)
			{
				//CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea entity finded");
				if (pEntity->GetId() == GetEntityId())
					return;

				if (pEntity->GetId() == m_ownerId)
					return;

				if (GetEntity()->GetPhysics())
				{
					if (pEntity->GetPhysics()->GetType() == GetEntity()->GetPhysics()->GetType())
						return;
				}
				/*CryLogAlways("CSpellProjectile::UpdateProjectileDamageArea start HEvent sending");
				SGameObjectEvent evt = SGameObjectEvent(eGFE_OnCollision, eGOEF_LoggedPhysicsEvent);
				EventPhysCollision epc;
				epc.pEntity[1] = pCollider;
				epc.iForeignData[1] = PHYS_FOREIGN_ID_ENTITY;
				epc.pt = contact->pt; epc.n = contact->n;
				epc.iPrim[0] = contact->iPrim[0]; epc.iPrim[1] = contact->iPrim[1];
				epc.pForeignData[1] = &pEntity;
				evt.ptr = &epc;*/
				//HandleEvent(evt);
				if (gEnv->bServer && !m_stickyProjectile.IsStuck() && !m_stuck_c)
				{
					if (GetStickable() > 0)
					{
						//m_stickyProjectile.Stick(CStickyProjectile::SStickParams(this, pCollision, CStickyProjectile::eO_AlignToVelocity));
						m_stuck_c = true;
					}
					IEntity *pTarget = pEntity;
					if (pTarget)
					{
						CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
						if (!pActor_trg)
						{
							CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
							if (!pItem)
							{

							}
							else
							{
								if (pItem->GetItemHealth() <= 0.0f)
								{
									if (GetProjectleRemovingType() > 0)
									{
										if (GetExplodable() > 1 && !exploded)
										{
											CProjectile::SExplodeDesc explodeDesc(true);
											explodeDesc.impact = true;
											explodeDesc.pos = contact->pt;
											explodeDesc.normal = contact->n;
											explodeDesc.vel = contact->dir;
											explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
											Explode(explodeDesc);
										}
										else
										{
											GetEntity()->SetPos(contact->pt);
											TrailSpellEffect(false);
											Destroy();
										}
									}
									return;
								}
							}
						}
					}

					if (pTarget)
					{
						if (pTarget->GetId() == GetOwnerId())
						{
							return;
						}
						Vec3 dir(0, 0, 0);
						float finalDamage = damage + GetPrgDmg();
						const int hitMatId = 0;

						Vec3 hitDir(ZERO);
						if (contact->dir.GetLengthSquared() > 1e-6f)
						{
							hitDir = contact->dir.GetNormalized();
						}

						CGameRules *pGameRules = g_pGame->GetGameRules();
						CActor  *pActor_c = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
						CActor  *pActor_own = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
						if (pActor_c)
						{
							for (int i = 0; i < 15; i++)
							{
								if (additive_effects_on_hit[i] > -1 && additive_effects_on_hit[i] < 65535)
								{
									pActor_c->AddBuff(additive_effects_on_hit[i], additive_effects_times[i]);
								}
							}

							if (!fnc_param_on_hit[1].empty())
								pActor_c->RequestToRunLuaFncFromActorScriptInstance(fnc_param_on_hit[1].c_str(), fnc_param_on_hit[2].c_str());
						}
						bool ok = true;
						float m_crt_dmg_mult = 1.0f;
						if (pActor_own)
						{
							finalDamage = (finalDamage*(m_crt_dmg_mult + (pActor_own->GetIntelligence()*0.01)));
						}
						EventPhysCollision pCollision;
						pCollision.pEntity[1] = pCollider;
						pCollision.iForeignData[1] = PHYS_FOREIGN_ID_ENTITY;
						pCollision.pt = contact->pt; pCollision.n = contact->n;
						pCollision.iPrim[0] = contact->iPrim[0]; pCollision.iPrim[1] = contact->iPrim[1];
						pCollision.pForeignData[1] = &pEntity;
						ProcessHit(*pGameRules, pCollision, *pTarget, finalDamage, hitMatId, hitDir);
						if (!pTarget->GetPhysics())
							return;

						if (pTarget->GetPhysics()->GetType() != PE_PARTICLE)
						{
							if (is_lightning_child)
							{
								m_entitiesLightningAffected.push_back(pTarget->GetId());
								UpdateLightningEffect(0.0f, false, pTarget->GetId());
								TrailSpellEffect(false);
								StopLightningChildEffect();
								return;
							}
							else if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
							{
								m_entitiesLightningAffected.push_back(pTarget->GetId());
								UpdateLightningEffect(0.0f, true, pTarget->GetId());
								TrailSpellEffect(false);
								Destroy();
								return;
							}
						}


						if (GetProjectleRemovingType() > 0)
						{
							if (!pTarget->GetPhysics())
								return;

							if (pTarget->GetPhysics()->GetType() == PE_PARTICLE)
							{
								CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
								CSpellProjectile* pSpellPhy = static_cast<CSpellProjectile*>(pProjectile);
								if (pSpellPhy)
								{
									if (pSpellPhy->GetOwnerId() == GetOwnerId())
									{
										return;
									}
									else
									{
										//CProjectile::HandleEvent(event);
										if (GetExplodable() > 1 && !exploded)
										{
											CProjectile::SExplodeDesc explodeDesc(true);
											explodeDesc.impact = true;
											explodeDesc.pos = contact->pt;
											explodeDesc.normal = contact->n;
											explodeDesc.vel = contact->dir;
											explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
											Explode(explodeDesc);
										}
										else
										{
											GetEntity()->SetPos(contact->pt);
											TrailSpellEffect(false);
											Destroy();
										}
										return;
									}
								}
							}
							else
							{
								//CProjectile::HandleEvent(event);
								if (GetExplodable() > 1 && !exploded)
								{
									CProjectile::SExplodeDesc explodeDesc(true);
									explodeDesc.impact = true;
									explodeDesc.pos = contact->pt;
									explodeDesc.normal = contact->n;
									explodeDesc.vel = contact->dir;
									explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
									Explode(explodeDesc);
								}
								else
								{
									GetEntity()->SetPos(contact->pt);
									TrailSpellEffect(false);
									Destroy();
								}
								return;
							}
						}
					}
					else
					{
						if (!is_lightning_child && enable_lighting_bolt_params == 3 && GetExplodable() == 0)
						{
							UpdateLightningEffect(0.0f, true);
							TrailSpellEffect(false);
							Destroy();
							return;
						}

						if (GetProjectleRemovingType() > 0 && GetExplodable() == 0)
						{
							//CProjectile::HandleEvent(event);
							GetEntity()->SetPos(contact->pt);
							TrailSpellEffect(false);
							Destroy();
							return;
						}
					}

					if (GetExplodable() > 1 && !exploded)
					{
						if (pTarget && pTarget->GetPhysics() && pTarget->GetPhysics()->GetType() == PE_PARTICLE)
						{
							CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(pTarget->GetId());
							if (pProjectile->GetOwnerId() == GetOwnerId())
							{
								return;
							}
						}
						CProjectile::SExplodeDesc explodeDesc(true);
						explodeDesc.impact = true;
						explodeDesc.pos = contact->pt;
						explodeDesc.normal = contact->n;
						explodeDesc.vel = contact->dir;
						explodeDesc.targetId = pTarget ? pTarget->GetId() : 0;
						exploded = true;
						Explode(explodeDesc);
					}
					//SpawnMaterialEffectOnCollision(pCollision, pTarget);
				}
				return;
			}
		}
	}
	//old_prj_pos2 = m_effectCenter;
}

void CSpellProjectile::UpdateProjectileTargeting()
{
	float frameTime = gEnv->pTimer->GetFrameTime();
	if (!GetEntity())
		return;

	if (!GetEntity()->GetPhysics())
		return;

	CActor* pOwnerActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
	pe_status_dynamics status;
	if (!GetEntity()->GetPhysics()->GetStatus(&status))
		return;

	pe_status_pos pos;
	if (!GetEntity()->GetPhysics()->GetStatus(&pos))
		return;

	const SHomingMissileParams& homingParams = *(m_pAmmoParams->pHomingParams);
	if (homingParams.m_cruise)
	{
		if (!m_have_target && !old_prj_pos3.IsZero())
		{
			m_TargetEntities.clear();
			float old_target_dist = 10000.0f;
			float rdng = homingParams.m_maxTargetDistance*0.1f;
			Vec3 dir_prj = GetEntity()->GetPos() - old_prj_pos3;
			Vec3 m_defe3 = GetEntity()->GetPos() + dir_prj * rdng;
			Vec3 boxDim(rdng, rdng, rdng);
			Vec3 m_effectCenter = (m_defe3 + GetEntity()->GetPos()) *0.5f;//GetEntity()->GetPos();
			Vec3 ptmin = m_effectCenter - boxDim;
			Vec3 ptmax = m_effectCenter + boxDim;
			IPhysicalEntity** pEntityList = NULL;
			static const int iObjTypes = ent_living;
			int numEntities = gEnv->pPhysicalWorld->GetEntitiesInBox(ptmin, ptmax, pEntityList, iObjTypes);
			for (int i = 0; i < numEntities; ++i)
			{
				IPhysicalEntity* pPhysicalEntity = pEntityList[i];
				IEntity* pEntity = static_cast<IEntity*>(pPhysicalEntity->GetForeignData(PHYS_FOREIGN_ID_ENTITY));
				if (pEntity)
				{
					if (pEntity->GetId() == GetEntityId())
						continue;

					if (pEntity->GetId() == m_ownerId)
						continue;

					if (GetEntity()->GetPhysics())
					{
						if (pEntity->GetPhysics()->GetType() == GetEntity()->GetPhysics()->GetType())
							continue;
					}
					CActor* pTargetActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
					if (pTargetActor)
					{
						if (pTargetActor->IsDead())
							continue;

						if (pOwnerActor)
						{
							uint8	m_localPlayerFaction;
							uint8	m_mpActorFaction;
							IAIObject* pAIObjectMpActor = pTargetActor->GetEntity()->GetAI();
							m_mpActorFaction = pAIObjectMpActor ? pAIObjectMpActor->GetFactionID() : IFactionMap::InvalidFactionID;
							IAIObject* pAIObjectLocPl = pOwnerActor->GetEntity()->GetAI();
							m_localPlayerFaction = pAIObjectLocPl ? pAIObjectLocPl->GetFactionID() : IFactionMap::InvalidFactionID;
							if (gEnv->pAISystem->GetFactionMap().GetReaction(m_localPlayerFaction, m_mpActorFaction) == IFactionMap::Hostile)
							{

							}
							else
							{
								continue;
							}
						}
					}
					ray_hit hit;
					int n = 0;
					IPhysicalEntity *pEnts_to_ignore = NULL;
					pEnts_to_ignore = GetEntity()->GetPhysics();
					Vec3 Ent_pos_emn = pEntity->GetPos();
					Ent_pos_emn.z += 0.8f;
					Vec3 dir_to_rtest = Ent_pos_emn - GetEntity()->GetPos();
					n = gEnv->pPhysicalWorld->RayWorldIntersection(GetEntity()->GetPos(), dir_to_rtest.normalized() * rdng, ent_static | ent_living | ent_terrain | ent_independent,
						rwi_colltype_any | rwi_stop_at_pierceable, &hit, 1, &pEnts_to_ignore, 1);
					if (n > 0)
					{
						IPhysicalEntity *pCollider = hit.pCollider;
						if (pCollider)
						{
							IEntity* pCollidedEntity = gEnv->pEntitySystem->GetEntityFromPhysics(pCollider);
							if (pCollidedEntity)
							{
								if (pCollidedEntity->GetId() == pEntity->GetId())
								{
									m_TargetEntities.push_back(pEntity->GetId());
									m_targetId = pEntity->GetId();
								}
							}
						}
					}
				}
			}
			std::vector<EntityId>::const_iterator it = m_TargetEntities.begin();
			std::vector<EntityId>::const_iterator end = m_TargetEntities.end();
			int count = m_TargetEntities.size();
			if (count > 0)
			{
				for (int i = 0; i < count, it != end; i++, ++it)
				{
					IEntity* pEntity = gEnv->pEntitySystem->GetEntity(*it);
					if (pEntity)
					{
						float dist_c = pEntity->GetPos().GetDistance(GetEntity()->GetPos());
						if (dist_c < old_target_dist)
						{
							//CryLogAlways("CSpellProjectile::UpdateProjectileTargeting dist_c = %f", dist_c);
							old_target_dist = dist_c;
							m_targetId = pEntity->GetId();

						}
						else
						{
							continue;
						}
					}
				}

				if (m_targetId > 0)
					m_have_target = true;
			}
		}
	}
	else
	{
		if (pOwnerActor)
		{
			IMovementController * pMC = pOwnerActor->GetMovementController();
			if (pMC)
			{
				SMovementState info;
				pMC->GetMovementState(info);
				if (pOwnerActor->IsPlayer())
				{
					m_destination = info.eyePosition + (info.fireDirection * 6550.0f);
					//CryLogAlways("CSpellProjectile m_destination x=%f y=%f z=%f", m_destination.x, m_destination.y, m_destination.z);
				}
				else
				{
					if (!pOwnerActor->Current_target_dir.IsZero())
						m_destination = info.eyePosition + (pOwnerActor->Current_target_dir.GetNormalized() * 6550.0f);
					else
						m_destination = info.eyePosition + (info.fireDirection * 6550.0f);
				}
				m_targetId = 0;
			}
		}
	}

	if (m_targetId > 0)
	{
		IEntity* pTarget = gEnv->pEntitySystem->GetEntity(m_targetId);
		if (pTarget)
		{
			AABB box;
			pTarget->GetWorldBounds(box);
			m_destination = box.GetCenter();
		}
	}
	else
	{

	}
	float currentSpeed = status.v.len();
	Vec3 currentPos = GetEntity()->GetWorldPos();
	Vec3 goalDir(ZERO);
	float m_controlLostTimer = -0.1f;
	currentSpeed = status.v.len();
	if (currentSpeed > 0.001f)
	{
		Vec3 currentVel = status.v;
		currentPos = pos.pos;
		goalDir = Vec3(ZERO);
		//Turn more slowly...
		currentVel.Normalize();

		if (m_controlLostTimer <= 0.0f)
		{
			goalDir = m_destination - currentPos;
			goalDir.Normalize();
		}
		else
			goalDir = currentVel;

		if (homingParams.m_bTracksChaff)
		{
			const float k_trackFov = cosf(DEG2RAD(30.0f));
			Vec3 trackDir;
			float dot;
			trackDir = currentVel*0.001f*(1.0f - k_trackFov);

			dot = max(goalDir.Dot(currentVel) - k_trackFov, 0.0f);
			trackDir += dot*goalDir;

			for (size_t i = 0; i<CChaff::s_chaffs.size(); ++i)
			{
				CChaff *c = CChaff::s_chaffs[i];
				Vec3 chaffPos = c->GetPosition();
				Vec3 chaffDir = chaffPos - currentPos;
				if (chaffDir.len2()>0.01f)
				{
					chaffDir.Normalize();
					dot = max(chaffDir.Dot(currentVel) - k_trackFov, 0.0f);
					trackDir = dot*chaffDir;
				}
			}
			goalDir = trackDir.normalized();
		}

		float cosine = currentVel.Dot(goalDir);
		cosine = CLAMP(cosine, -1.0f, 1.0f);
		float totalAngle = RAD2DEG(acos_tpl(cosine));
		if (cosine<0.99)
		{
			float maxAngle = homingParams.m_turnSpeed*frameTime;
			if (maxAngle>totalAngle)
				maxAngle = totalAngle;
			float t = (maxAngle / totalAngle)*homingParams.m_lazyness;
			goalDir = Vec3::CreateSlerp(currentVel, goalDir, t);
			goalDir.Normalize();
		}
		currentSpeed = min(homingParams.m_maxSpeed, currentSpeed);
	}
	float desiredSpeed = currentSpeed;
	if (currentSpeed < homingParams.m_maxSpeed - 0.1f)
	{
		desiredSpeed = min(homingParams.m_maxSpeed, desiredSpeed + homingParams.m_accel*frameTime);
	}
	Vec3 currentDir = status.v.GetNormalizedSafe(FORWARD_DIRECTION);
	Vec3 dir = currentDir;
	if (!goalDir.IsZero())
	{
		float cosine = max(min(currentDir.Dot(goalDir), 0.999f), -0.999f);
		float goalAngle = RAD2DEG(acos_tpl(cosine));
		float maxAngle = homingParams.m_turnSpeed * frameTime;
		if (goalAngle > maxAngle + 0.05f)
			dir = (Vec3::CreateSlerp(currentDir, goalDir, maxAngle / goalAngle)).normalize();
		else //if (goalAngle < 0.005f)
			dir = goalDir;
	}
	old_prj_pos3 = GetEntity()->GetPos();
	if (m_destination.IsZero())
		return;

	pe_action_set_velocity action;
	action.v = dir * desiredSpeed;
	//CryLogAlways("CSpellProjectile set veloc x=%f y=%f z=%f", action.v.x, action.v.y, action.v.z );
	GetEntity()->GetPhysics()->Action(&action);
}

void CSpellProjectile::SetSpellDamage(float dmg)
{
	damage = dmg;
	start_damage = damage;
}

void CSpellProjectile::SpawnMaterialEffectOnCollision(const EventPhysCollision* pCollision, IEntity* pHitTarget)
{
	//Spawn effect
	//CryLogAlways("CSpellProjectile::SpawnMaterialEffectOnCollision start");
	IMaterialEffects* pMaterialEffects = g_pGame->GetIGameFramework()->GetIMaterialEffects();
	TMFXEffectId effectId = pMaterialEffects->GetEffectId(GetEntity()->GetClass(), pCollision->idmat[1]);
	//CryLogAlways("CSpellProjectile::SpawnMaterialEffectOnCollision pCollision->idmat[1] = %i", pCollision->idmat[1]);
	//CryLogAlways("CSpellProjectile::SpawnMaterialEffectOnCollision effectId = %i", effectId);
	if (effectId != InvalidEffectId)
	{
		//CryLogAlways("CSpellProjectile::SpawnMaterialEffectOnCollision end");
		SMFXRunTimeEffectParams params;
		params.src = GetEntityId();
		params.trg = pHitTarget ? pHitTarget->GetId() : 0;
		params.srcSurfaceId = m_pAmmoParams->pParticleParams->surface_idx;
		params.trgSurfaceId = pCollision->idmat[1];
		params.srcRenderNode = (pCollision->iForeignData[0] == PHYS_FOREIGN_ID_STATIC) ? (IRenderNode*)pCollision->pForeignData[0] : NULL;
		params.trgRenderNode = (pCollision->iForeignData[1] == PHYS_FOREIGN_ID_STATIC) ? (IRenderNode*)pCollision->pForeignData[1] : NULL;
		params.pos = GetEntity()->GetPos();
		params.normal = pCollision->n; 
		params.partID = pCollision->partid[1];
		params.dir[0] = -pCollision->n;
		params.playflags = eMFXPF_All&(~eMFXPF_Audio); //Do not play the sound on backface
		params.playflags &= ~eMFXPF_Decal; //We disable also decals, since hit.pt is not refined with render mesh
		params.fDecalPlacementTestMaxSize = pCollision->fDecalPlacementTestMaxSize;
		pMaterialEffects->ExecuteEffect(effectId, params);
	}
}
