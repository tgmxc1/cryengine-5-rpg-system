// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
 -------------------------------------------------------------------------
  $Id$
  $DateTime$
  
 -------------------------------------------------------------------------
  History:
  - Created by Marco Corbetta

*************************************************************************/

#include "StdAfx.h"
#include <CryRenderer/IRenderAuxGeom.h>
#include "WorldBuilder.h"
#include "Game.h"
#include "UI/HUD/HUDCommon.h"
#include "GameCVars.h"
#include "LevelTolevelSerialization.h"
#include "PlayerCharacterCreatingSys.h"
#include "GameXMLSettingAndGlobals.h"
#include "Player.h"

//////////////////////////////////////////////////////////////////////////
CWorldBuilder::CWorldBuilder()
{
	if(gEnv->pGame && gEnv->pGame->GetIGameFramework())
	{
		CRY_ASSERT_MESSAGE(gEnv->pGame->GetIGameFramework()->GetILevelSystem(), "Unable to register as levelsystem listener!");
		if(gEnv->pGame->GetIGameFramework()->GetILevelSystem())
		{
			gEnv->pGame->GetIGameFramework()->GetILevelSystem()->AddListener(this);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CWorldBuilder::~CWorldBuilder()
{		
	if(gEnv->pGame && gEnv->pGame->GetIGameFramework())
	{
		if(gEnv->pGame->GetIGameFramework()->GetILevelSystem())
			gEnv->pGame->GetIGameFramework()->GetILevelSystem()->RemoveListener( this );
	}
}

//////////////////////////////////////////////////////////////////////////
void CWorldBuilder::OnLoadingStart(ILevelInfo *pLevel)
{
	// reset existing prefabs before loading the level
	m_PrefabManager.Clear(false);
	// reset player UI before loading the level
	if (g_pGameCVars->g_pl_main_hud_activation_from_code > 0)
	{
		if (g_pGame && g_pGame->GetHUDCommon())
			g_pGame->GetHUDCommon()->Reset();
	}
}

//////////////////////////////////////////////////////////////////////////
void CWorldBuilder::OnLoadingComplete(ILevelInfo* pLevel)
{	
	int nSeed=0;
	ICVar *pCvar=gEnv->pConsole->GetCVar("g_SessionSeed");
	if (pCvar)	
		nSeed=pCvar->GetIVal();	

	// spawn prefabs	
	{				
		IEntityItPtr i = gEnv->pEntitySystem->GetEntityIterator();
		while (!i->IsEnd())
		{
			IEntity* pEnt = i->Next();
			if (strncmp(pEnt->GetClass()->GetName(), "ProceduralObject", 16) == 0)
			{
				const Vec3& vPos = pEnt->GetPos();

				//m_PrefabManager.SetPrefabGroup(sTileThemeDesc);			
				m_PrefabManager.CallOnSpawn(pEnt, nSeed + VecHash(vPos));
			}
		}
	}

	CLevelTolevelserialization* ltl = g_pGame->GetLevelTolevelserialization();
	if (!ltl)
		return;

	ltl->upd_val1 = 0.2f;

	if (g_pGame && g_pGame->GetHUDCommon() && !gEnv->IsEditor() && !gEnv->bMultiplayer)
	{
		g_pGame->GetHUDCommon()->post_initialization_timer = 8.0f;
		if (!g_pGame->GetHUDCommon()->m_PostInitScreenVisible)
			g_pGame->GetHUDCommon()->ShowPostInitialization(true);
	}

	if (gEnv->bMultiplayer)
	{
		CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (pPlayer)
		{
			pPlayer->ClearTfSpawned();
			pPlayer->ResetActorVars();
			pPlayer->m_player_name = "";
			pPlayer->SetPlayerModelStr("");
			pPlayer->InvalidateCurrentModelName();
			pPlayer->reequip_timer_cn1 = 0.0f;
			pPlayer->rload_pl_model_timer_after_ps = 0.0f;
			pPlayer->serialization_reinit_customization_setting_on_character = 0.0f;
			CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
			if (p_PlCrSys)
			{
				p_PlCrSys->net_selected_character_name = "";
				if (g_pGame->GetGameXMLSettingAndGlobals())
					g_pGame->GetGameXMLSettingAndGlobals()->enable_character_creating_menu_at_game_start = true;

				pPlayer->SetActorModel("", false);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void CWorldBuilder::OnUnloadComplete(ILevelInfo* pLevel)
{
	if (gEnv->bMultiplayer)
	{
		CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
		if (pPlayer)
		{
			pPlayer->ClearTfSpawned();
			pPlayer->ResetActorVars();
			pPlayer->m_player_name = "";
			pPlayer->SetPlayerModelStr("");
			pPlayer->InvalidateCurrentModelName();
			pPlayer->reequip_timer_cn1 = 0.0f;
			pPlayer->rload_pl_model_timer_after_ps = 0.0f;
			pPlayer->serialization_reinit_customization_setting_on_character = 0.0f;
			CPlayerCharacterCreatingSys * p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
			if (p_PlCrSys)
			{
				p_PlCrSys->net_selected_character_name = "";
				if (g_pGame->GetGameXMLSettingAndGlobals())
					g_pGame->GetGameXMLSettingAndGlobals()->enable_character_creating_menu_at_game_start = true;
			}
		}
	}
	return;
}

//////////////////////////////////////////////////////////////////////////
void CWorldBuilder::DrawDebugInfo()
{
	// show prefab debug info
	char szDebugInfo[256];
	float colors[4]={1,1,1,1};
	//float colorsYellow[4]={1,1,0,1};
	//float colorsRed[4]={1,0,0,1};

	IEntityItPtr i = gEnv->pEntitySystem->GetEntityIterator();
	while (!i->IsEnd())
	{
		IEntity* pEnt = i->Next();
		if (strncmp(pEnt->GetClass()->GetName(), "ProceduralObject", 16) != 0)
			continue;
			
		Vec3 wp = pEnt->GetWorldTM().GetTranslation();				

		// check if we have the info we need from the script 
		IScriptTable *pScriptTable(pEnt->GetScriptTable());
		if (pScriptTable)
		{
			ScriptAnyValue value;
			if (pScriptTable->GetValueAny("PrefabSourceName",value))
			{
				char *szPrefabName=NULL;
				if (value.CopyTo(szPrefabName))
				{
					cry_sprintf( szDebugInfo, "%s", szPrefabName );
					wp.z-= 0.1f;
					gEnv->pRenderer->DrawLabelEx(wp, 1.1f, colors, true, true, "%s", szDebugInfo);
				}
			}
		}	
	}	//i
}

