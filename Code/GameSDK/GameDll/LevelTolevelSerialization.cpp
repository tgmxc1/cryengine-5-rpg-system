#include "StdAfx.h"
#include "LevelTolevelSerialization.h"
#include <CryEntitySystem/IEntitySystem.h>
#include <CryNetwork/ISerialize.h>
#include <CryGame/GameUtils.h>
#include <CryGame/IGameFramework.h>
#include "Player.h"
#include "Game.h"
#include "GameCVars.h"
#include "GameActions.h"
#include <Cry3DEngine/ITimeOfDay.h>
#include "PlayerCharacterCreatingSys.h"


CLevelTolevelserialization::CLevelTolevelserialization()
{
	upd_val1=0.0f;
	upd_val2 = 0.0f;
	pl_pos = Vec3(ZERO);
	pl_pos_spec = Vec3(ZERO);
	pl_usedmarkspell = false;
	next_spawn_point_id = 0;
	m_spells.clear();
	m_spells_in_active_slots.clear();
	m_player_items.clear();
	m_levels_to_loading.clear();
	m_actor_perks.clear();
	m_actor_activeEffects.clear();
	ltl_Quests.clear();
	ltl_SubQuests.clear();
	m_char_name = "";	m_player_model_path = "";	m_pl_gender = 0;	m_pl_gold = 9;	m_pl_level = 1;	m_pl_hp = 1; 	m_pl_stmn = 1;	m_pl_mp = 1;	m_pl_hp_n = 1;
	m_pl_stmn_n = 1;	m_pl_mp_n = 1;	m_levelxp = 1;	m_xptonextlevel = 2;	m_plheadtype = 1;	m_plhairtype = 1;	m_pleyestype = 1;	m_strength = 1;
	m_willpower = 1;	m_endurance = 1;	m_personality = 1;	m_intelligence = 1;	m_agility = 1;	m_breath = 1;
	m_stealth_mode = false;
	m_relaxed_mode = false;
	item_on_back = 0;
	//---------------------------
	tod_days_left = 0;
	tod_time = 0.0f;
	//---------------------------
	load_process_ltlinfo_at_level_loading_end = false; load_process_tod_time = false; load_process_player_ltl = false; load_process_player_inventory_ltl = false;
	load_process_player_special_pos_ltl = false; load_process_player_quests_ltl = false; load_process_player_spells_ltl = false;
	load_ent_info = false; load_player_inventory_ended_sucessful = false;
}

CLevelTolevelserialization::~CLevelTolevelserialization()
{

}


void CLevelTolevelserialization::Serialize(TSerialize ser)
{
	//if (ser.BeginOptionalGroup("LevelTolevelserialization", true))
	if (ser.GetSerializationTarget() == eST_SaveGame)
	{
		ser.BeginGroup("LevelTolevelserialization");
		std::list<string> m_ltl_levels;
		std::vector<SLevelSerializedLTL>::iterator it = m_levels_to_loading.begin();
		std::vector<SLevelSerializedLTL>::iterator end = m_levels_to_loading.end();
		int count2 = m_levels_to_loading.size();
		int count = 0;
		SLevelSerializedLTL level_to_LTLS;
		ser.BeginGroup("LTLLevels");
		if (ser.IsWriting())
		{
			m_ltl_levels.clear();
			for (int i = 0; i < count2, it != end; i++, ++it)
			{
				level_to_LTLS = *it;
				if (level_to_LTLS.level_name.empty())
				{
					continue;
				}
				m_ltl_levels.push_back(level_to_LTLS.level_name);
				ser.BeginGroup(level_to_LTLS.level_name.c_str());//ln start
				SSerializedLtlObject pd_obj;
				std::vector<SSerializedLtlObject>::iterator it_obj = level_to_LTLS.m_level_objects.begin();
				std::vector<SSerializedLtlObject>::iterator end_obj = level_to_LTLS.m_level_objects.end();
				count = level_to_LTLS.m_level_objects.size();
				int r_objects_num = 0;
				ser.BeginGroup("LevelObjects");//lo
				for (int io = 0; io < count, it_obj != end_obj; io++, ++it_obj)
				{
					pd_obj = *it_obj;
					if (pd_obj.object_id > 0)
					{
						char B_valstr[17];
						itoa(r_objects_num, B_valstr, 10);
						string C_valstr = B_valstr;
						string A1_valstr = string("L_object_id_") + C_valstr;
						ser.BeginGroup(A1_valstr.c_str());
						ser.Value("id", pd_obj.object_id);
						ser.Value("pos_vec", pd_obj.oblect_position.GetTranslation());
						ser.Value("pos_quat", Quat(pd_obj.oblect_position));
						ser.Value("pos_scale", Vec3(1.0, 1.0, 1.0));
						r_objects_num += 1;
						ser.EndGroup();
					}
				}
				ser.Value("objects_num", r_objects_num);
				ser.EndGroup();//lo end
				SSerializedLtlActor pd_actor;
				std::vector<SSerializedLtlActor>::iterator it_actors = level_to_LTLS.m_level_actors.begin();
				std::vector<SSerializedLtlActor>::iterator end_actors = level_to_LTLS.m_level_actors.end();
				count = level_to_LTLS.m_level_actors.size();
				int r_actor_num = 0;
				ser.BeginGroup("LevelActors");//act
				for (int ia = 0; ia < count, it_actors != end_actors; ia++, ++it_actors)
				{
					pd_actor = *it_actors;
					if (!pd_actor.actorName.empty())
					{
						char B_valstr[17];
						itoa(r_actor_num, B_valstr, 10);
						string C_valstr = B_valstr;
						string A1_valstr = string("L_actor_id_") + C_valstr;
						ser.BeginGroup(A1_valstr.c_str());
						ser.Value("name", pd_actor.actorName);
						ser.Value("id", pd_actor.actorId);
						ser.Value("pos_vec", pd_actor.actor_position.GetTranslation());
						ser.Value("pos_quat", Quat(pd_actor.actor_position));
						ser.Value("pos_scale", Vec3(1.0, 1.0, 1.0));
						ser.Value("dialog_stg", pd_actor.dialog_stage);
						ser.Value("health", pd_actor.health);
						r_actor_num += 1;
						ser.EndGroup();
					}
				}
				ser.Value("actors_num", r_actor_num);
				ser.EndGroup();//act end
				SSerializedLtlItem pd_item;
				std::vector<SSerializedLtlItem>::iterator it_items = level_to_LTLS.m_level_items.begin();
				std::vector<SSerializedLtlItem>::iterator end_items = level_to_LTLS.m_level_items.end();
				count = level_to_LTLS.m_level_items.size();
				int r_items_num = 0;
				ser.BeginGroup("LevelItems");//itm
				for (int ii = 0; ii < count, it_items != end_items; ii++, ++it_items)
				{
					pd_item = *it_items;
					if (!pd_item.item_class_name.empty())
					{
						char B_valstr[17];
						itoa(r_items_num, B_valstr, 10);
						string C_valstr = B_valstr;
						string A1_valstr = string("L_item_id_") + C_valstr;
						ser.BeginGroup(A1_valstr.c_str());
						ser.Value("name", pd_item.item_class_name);
						ser.Value("id", pd_item.item_id);
						ser.Value("pos_vec", pd_item.item_position.GetTranslation());
						ser.Value("pos_quat", Quat(pd_item.item_position));
						ser.Value("pos_scale", Vec3(1.0, 1.0, 1.0));
						ser.Value("owner_id", pd_item.actorOwnerId);
						ser.Value("equ_stat", pd_item.equipped_state);
						ser.Value("quan_stat", pd_item.quantity_state);
						ser.Value("nodraw_type", pd_item.is_nodraw_type);
						r_items_num += 1;
						ser.EndGroup();
					}
				}
				ser.Value("items_num", r_items_num);
				ser.EndGroup();//itm end
				ser.EndGroup();//ln end
			}
			ser.Value("ltl_levels_nmn", m_ltl_levels);
		}
		else
		{
			m_levels_to_loading.clear();
			m_levels_to_loading.resize(0);
			ser.Value("ltl_levels_nmn", m_ltl_levels);
			std::list<string>::iterator it = m_ltl_levels.begin();
			std::list<string>::iterator end = m_ltl_levels.end();
			int levels_count = m_ltl_levels.size();
			for (int i = 0; i < levels_count, it != end; i++, ++it)
			{
				string lvl_nmn = *it;
				if (lvl_nmn.empty())
					continue;

				level_to_LTLS.level_name = lvl_nmn;
				ser.BeginGroup(lvl_nmn.c_str());//1
				ser.BeginGroup("LevelObjects");//obj
				int r_objects_num = 0;
				ser.Value("objects_num", r_objects_num);
				SSerializedLtlObject pd_obj;
				for (int io = 0; io < r_objects_num; io++)
				{
					char B_valstr[17];
					itoa(io, B_valstr, 10);
					string C_valstr = B_valstr;
					string A1_valstr = string("L_object_id_") + C_valstr;
					ser.BeginGroup(A1_valstr.c_str());
					ser.Value("id", pd_obj.object_id);
					Vec3 posit_obj(ZERO);
					Quat rot_obj(IDENTITY);
					Vec3 scale(ZERO);
					ser.Value("pos_vec", posit_obj);
					ser.Value("pos_quat", rot_obj);
					ser.Value("pos_scale", scale);
					pd_obj.oblect_position.Set(scale, rot_obj, posit_obj);
					ser.EndGroup();
					level_to_LTLS.m_level_objects.push_back(pd_obj);
				}
				ser.EndGroup();//obj
				ser.BeginGroup("LevelActors");//act
				int r_actor_num = 0;
				ser.Value("actors_num", r_actor_num);
				SSerializedLtlActor pd_actor;
				for (int ia = 0; ia < r_actor_num; ia++)
				{
					char B_valstr[17];
					itoa(ia, B_valstr, 10);
					string C_valstr = B_valstr;
					string A1_valstr = string("L_actor_id_") + C_valstr;
					ser.BeginGroup(A1_valstr.c_str());
					ser.Value("name", pd_actor.actorName);
					ser.Value("id", pd_actor.actorId);
					Vec3 posit_obj(ZERO);
					Quat rot_obj(IDENTITY);
					Vec3 scale(ZERO);
					ser.Value("pos_vec", posit_obj);
					ser.Value("pos_quat", rot_obj);
					ser.Value("pos_scale", scale);
					pd_actor.actor_position.Set(scale, rot_obj, posit_obj);
					ser.Value("dialog_stg", pd_actor.dialog_stage);
					ser.Value("health", pd_actor.health);
					ser.EndGroup();
					level_to_LTLS.m_level_actors.push_back(pd_actor);
				}
				ser.EndGroup();//act
				ser.BeginGroup("LevelItems");//item
				int r_items_num = 0;
				ser.Value("items_num", r_items_num);
				SSerializedLtlItem pd_item;
				for (int ii = 0; ii < r_items_num; ii++)
				{
					char B_valstr[17];
					itoa(ii, B_valstr, 10);
					string C_valstr = B_valstr;
					string A1_valstr = string("L_item_id_") + C_valstr;
					ser.BeginGroup(A1_valstr.c_str());
					ser.Value("name", pd_item.item_class_name);
					ser.Value("id", pd_item.item_id);
					Vec3 posit_obj(ZERO);
					Quat rot_obj(IDENTITY);
					Vec3 scale(ZERO);
					ser.Value("pos_vec", posit_obj);
					ser.Value("pos_quat", rot_obj);
					ser.Value("pos_scale", scale);
					pd_item.item_position.Set(scale, rot_obj, posit_obj);
					ser.Value("owner_id", pd_item.actorOwnerId);
					ser.Value("equ_stat", pd_item.equipped_state);
					ser.Value("quan_stat", pd_item.quantity_state);
					ser.Value("nodraw_type", pd_item.is_nodraw_type);
					ser.EndGroup();
					level_to_LTLS.m_level_items.push_back(pd_item);
				}
				ser.EndGroup();//item
				ser.EndGroup();//1
				m_levels_to_loading.push_back(level_to_LTLS);
			}
		}
		ser.EndGroup();
		ser.BeginGroup("LTLTimeOfDay");
		ser.Value("ltl_tod_time", tod_time);
		ser.Value("ltl_days_left", tod_days_left);
		ser.EndGroup();
		ser.BeginGroup("PlayerCharacter");
		ser.BeginGroup("Spells");
		ser.Value("ltl_spells", m_spells);
		ser.EndGroup();
		ser.BeginGroup("MarkedLevel");
		ser.Value("ltl_pl_pos", pl_pos);
		ser.Value("ltl_pl_pos_spec", pl_pos_spec);
		ser.Value("ltl_char_name", m_char_name);
		ser.EndGroup();
		ser.EndGroup();

		//---------------------------------------M
		ser.EndGroup();
	}
}
/////////////////////////////////////////////////////
void CLevelTolevelserialization::SavePlayerInventoryState()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	IInventory *pInventory = pPlayer->GetInventory();
	if (!pInventory)
		return;

	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	if (!pItemSystem)
		return;

	m_player_items.clear();
	for (int i = 0; i < pInventory->GetCount(); i++)
	{
		CItem *curItem = static_cast<CItem*>(pItemSystem->GetItem(pInventory->GetItem(i)));
		if (curItem)
		{
			SSerializedLtlItem pd_item;
			pd_item.equipped_state = curItem->GetEquipped();
			pd_item.item_class_name = curItem->GetEntity()->GetClass()->GetName();
			pd_item.quantity_state = curItem->GetItemQuanity();
			pd_item.is_nodraw_type = curItem->IsItemIgnoredByHud();
			m_player_items.push_back(pd_item);
		}
	}
}

void CLevelTolevelserialization::LoadPlayerInventoryState()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	IInventory *pInventory = pPlayer->GetInventory();
	if (!pInventory)
		return;

	IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
	if (!pItemSystem)
		return;

	SSerializedLtlItem pd_item;
	std::vector<SSerializedLtlItem>::iterator it_items = m_player_items.begin();
	std::vector<SSerializedLtlItem>::iterator end_items = m_player_items.end();
	int count = m_player_items.size();
	if (count > 1 && !load_player_inventory_ended_sucessful)
		load_player_inventory_ended_sucessful = false;
	else
		load_player_inventory_ended_sucessful = true;

	for (int ii = 0; ii < count, it_items != end_items; ii++, ++it_items)
	{
		pd_item = *it_items;
		if (pd_item.item_class_name.empty())
			continue;

		if (pd_item.is_nodraw_type)
		{
			if (pd_item.equipped_state > 0)
			{
				pPlayer->ScheduleItemSwitch(pPlayer->GetNoWeaponId(), true);
			}
			continue;
		}

		EntityId item_id = pItemSystem->GiveItem(gEnv->pGame->GetIGameFramework()->GetClientActor(), pd_item.item_class_name.c_str(),false,false,false);
		CItem *curItem = static_cast<CItem*>(pItemSystem->GetItem(item_id));
		if (curItem && pd_item.quantity_state > -1)
		{
			curItem->SetItemQuanity(pd_item.quantity_state);
			if (pd_item.equipped_state > 0)
			{
				bool equipable = true;
				equipable = pPlayer->EquipItem(item_id);
				if (curItem->CanSelect() && equipable)
				{
					if (!pPlayer->GetRelaxedMod())
						pPlayer->ScheduleItemSwitch(item_id, true);
					else
						pPlayer->SetEquippedItemId(eAESlot_Weapon, curItem->GetEntityId());
				}
			}
		}

		if (g_pGameCVars->g_LTL_system_delayed_restore_player_equipment > 0)
		{
			m_player_items.erase(it_items);
			m_player_items.resize(count - 1);
			break;
		}
	}

	bool bHasNoWeapon = pInventory->GetCountOfClass("NoWeapon") > 0;
	bool bHasPcarWeapon = pInventory->GetCountOfClass("PickAndThrowWeapon") > 0;
	if (!bHasNoWeapon)
	{
		pItemSystem->GiveItem((IActor*)pPlayer, "NoWeapon", false, false, false);
	}
	if (!bHasPcarWeapon)
	{
		pItemSystem->GiveItem((IActor*)pPlayer, "PickAndThrowWeapon", false, false, false);
	}

	if (g_pGameCVars->g_LTL_system_delayed_restore_player_equipment <= 0)
	{
		load_player_inventory_ended_sucessful = true;
	}
}

void CLevelTolevelserialization::SavePlayerParams()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if(!pPlayer)
		return;

	m_pl_gender = pPlayer->GetGender();
	m_pl_gold = pPlayer->GetGold();
	m_strength = pPlayer->GetStrength();
	m_pl_level = pPlayer->GetLevel();
	m_pl_hp = pPlayer->GetMaxHealth();
	m_pl_stmn = pPlayer->GetMaxStamina();
	m_pl_mp = pPlayer->GetMaxMagicka();
	m_levelxp = pPlayer->GetLevelXp();
	m_xptonextlevel = pPlayer->GetXpToNextLevel();
	m_plheadtype = pPlayer->GetHeadType();
	m_plhairtype = pPlayer->GetHairType();
	m_pleyestype = pPlayer->GetEyesType();
	m_willpower = pPlayer->GetWillpower();
	m_endurance = pPlayer->GetEndurance();
	m_personality = pPlayer->GetPersonality();
	m_intelligence = pPlayer->GetIntelligence();
	m_agility = pPlayer->GetAgility();
	m_char_name = pPlayer->m_player_name;
	m_stealth_mode = pPlayer->GetStealthMod();
	m_relaxed_mode = pPlayer->GetRelaxedMod();
	item_on_back = pPlayer->GetItemOnBackId();
	m_pl_hp_n = pPlayer->GetHealth();
	m_pl_stmn_n = pPlayer->GetStamina();
	m_pl_mp_n = pPlayer->GetMagicka();
	m_player_model_path = pPlayer->GetPlayerModelStr();

	m_actor_perks.clear();
	m_actor_activeEffects.clear();
	m_actor_perks = pPlayer->m_passive_effects;
	m_actor_activeEffects = pPlayer->m_timed_effects_buffs;
}

void CLevelTolevelserialization::LoadPlayerParams()
{
	CPlayer *pPlayer = static_cast<CPlayer*>(gEnv->pGame->GetIGameFramework()->GetClientActor());
	if (!pPlayer)
		return;

	if (!m_actor_activeEffects.empty())
	{
		std::vector<CActor::SActorTimedEffectsFtg>::iterator it = m_actor_activeEffects.begin();
		std::vector<CActor::SActorTimedEffectsFtg>::iterator end = m_actor_activeEffects.end();
		int buffs_count = m_actor_activeEffects.size();
		for (int i = 0; i < buffs_count, it != end; i++, ++it)
		{
			CActor::SActorTimedEffectsFtg effect = *it;
			if (effect.buff_id > 0)
			{
				pPlayer->AddBuff(effect.buff_id, effect.buff_time, effect.effect_id);
			}
		}
	}

	if (!m_actor_perks.empty())
	{
		std::vector<CActor::SActorPassiveEffectRK>::iterator it = m_actor_perks.begin();
		std::vector<CActor::SActorPassiveEffectRK>::iterator end = m_actor_perks.end();
		int perks_count = m_actor_perks.size();
		for (int i = 0; i < perks_count, it != end; i++, ++it)
		{
			CActor::SActorPassiveEffectRK effect = *it;
			if (effect.perk_id > -1 && effect.perk_lvl > 0)
			{
				for (int ic = 1; ic < effect.perk_lvl + 1; ic++)
				{
					pPlayer->ActivatePerk(effect.perk_id, true, ic);
				}
			}
		}
	}
	pPlayer->SetGender(m_pl_gender);
	pPlayer->SetGold(m_pl_gold);
	pPlayer->SetStrength(m_strength);
	pPlayer->SetLevel(m_pl_level);
	pPlayer->SetMaxHealth(m_pl_hp);
	pPlayer->SetMaxStamina(m_pl_stmn);
	pPlayer->SetMaxMagicka(m_pl_mp);
	pPlayer->SetLevelXp(m_levelxp);
	pPlayer->SetXpToNextLevel(m_xptonextlevel);
	pPlayer->SetHeadType(m_plheadtype);
	pPlayer->SetHairType(m_plhairtype);
	pPlayer->SetEyesType(m_pleyestype);
	pPlayer->SetWillpower(m_willpower);
	pPlayer->SetEndurance(m_endurance);
	pPlayer->SetPersonality(m_personality);
	pPlayer->SetIntelligence(m_intelligence);
	pPlayer->SetAgility(m_agility);
	pPlayer->m_player_name = m_char_name;
	pPlayer->SetStealthMod(m_stealth_mode);
	pPlayer->SetRelaxedMod(m_relaxed_mode);
	pPlayer->SetItemOnBackId(item_on_back);
	pPlayer->SetHealth(m_pl_hp_n);
	pPlayer->SetStamina(m_pl_stmn_n);
	pPlayer->SetMagicka(m_pl_mp_n);
	pPlayer->SetPlayerModelStr(m_player_model_path.c_str());
}

void CLevelTolevelserialization::SaveQuests()
{
	CGameXMLSettingAndGlobals* pGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGlobals)
		return;

	ltl_Quests.clear();
	ltl_Quests = pGlobals->s_pl_Quests;
	ltl_SubQuests.clear();
	ltl_SubQuests = pGlobals->s_pl_SubQuests;
}

void CLevelTolevelserialization::LoadQuests()
{
	CGameXMLSettingAndGlobals* pGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGlobals)
		return;

	pGlobals->s_pl_Quests = ltl_Quests;
	pGlobals->s_pl_SubQuests = ltl_SubQuests;
}

void CLevelTolevelserialization::SavePlayerSpellBook()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	m_spells.clear();
	m_spells.resize(0);
	m_spells_in_active_slots.clear();
	CGameXMLSettingAndGlobals::SActorSpell act_spell;
	act_spell.spell_id = -1;
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator it = pClient->m_actor_spells_book.begin();
	std::vector<CGameXMLSettingAndGlobals::SActorSpell>::iterator end = pClient->m_actor_spells_book.end();
	int count = pClient->m_actor_spells_book.size();
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		act_spell = *it;
		if (act_spell.spell_id > -1)
		{
			m_spells.push_back(act_spell.spell_id);
		}
	}

	for (int i = 0; i < MAX_USABLE_SPELLS_SLOTS; i++)
	{
		if (pClient->GetSpellIdFromCurrentSpellsForUseSlot(i) > 0)
			m_spells_in_active_slots.insert(std::make_pair(pClient->GetSpellIdFromCurrentSpellsForUseSlot(i), i));
	}
}

void CLevelTolevelserialization::LoadPlayerSpellBook()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	pClient->ClearActorSB(true);
	std::vector<int>::iterator it = m_spells.begin();
	std::vector<int>::iterator end = m_spells.end();
	int count = m_spells.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (*it > -1)
			{
				pClient->AddSpellToActorSB(*it);
			}
		}
	}

	std::map<int, int>::iterator it_slots = m_spells_in_active_slots.begin();
	std::map<int, int>::iterator end_slots = m_spells_in_active_slots.end();
	int count_slots = m_spells_in_active_slots.size();
	if (count_slots > 0)
	{
		for (int i = 0; i < count_slots, it_slots != end_slots; i++, ++it_slots)
		{
			pClient->AddSpellToCurrentSpellsForUseSlot((*it_slots).first, (*it_slots).second);
		}
	}
}

void CLevelTolevelserialization::SaveTimeOfDay()
{
	ITimeOfDay* pTod = gEnv->p3DEngine->GetTimeOfDay();
	if (pTod)
	{
		tod_time = pTod->GetTime();
	}

	CGameXMLSettingAndGlobals* pGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGlobals)
		return;

	tod_days_left = pGlobals->days_left;
}

void CLevelTolevelserialization::LoadTimeOfDay()
{
	ITimeOfDay* pTod = gEnv->p3DEngine->GetTimeOfDay();
	if(pTod)
		pTod->SetTime(tod_time,true);

	CGameXMLSettingAndGlobals* pGlobals = g_pGame->GetGameXMLSettingAndGlobals();
	if (!pGlobals)
		return;

	pGlobals->days_left = tod_days_left;
}

void CLevelTolevelserialization::UpdateLtl()
{
	if (upd_val1 > 0.0f)
	{
		upd_val1 -= gEnv->pTimer->GetFrameTime();
		if (gEnv->pTimer->GetFrameTime() > 0.02f)
			upd_val1 -= 0.02f;

		if (load_process_ltlinfo_at_level_loading_end)
		{
			ICVar* saveLoadEnabled = gEnv->pConsole->GetCVar("g_EnableLoadSave");
			if (saveLoadEnabled)
			{
				if (saveLoadEnabled->GetIVal() == 1)
				{
					saveLoadEnabled->Set(0);
				}
			}

			if (load_process_tod_time)
			{
				LoadTimeOfDay();
				load_process_tod_time = false;
			}

			if (load_process_player_ltl)
			{
				LoadPlayerParams();
				LoadPlayerChToLevel();
				load_process_player_ltl = false;
				upd_val1 = 0.1f;
				return;

			}

			if (load_ent_info)
			{
				LoadAllEntitesToLevel();
				ClearEntitesInList();
				load_ent_info = false;
			}

			if (load_process_player_quests_ltl)
			{
				LoadQuests();
				load_process_player_quests_ltl = false;
			}

			if (load_process_player_special_pos_ltl)
			{
				LoadSpecialPlayerPos();
				load_process_player_special_pos_ltl = false;
			}

			if (load_process_player_spells_ltl)
			{
				LoadPlayerSpellBook();
				load_process_player_spells_ltl = false;
			}
		}

		if (upd_val1 <= 0.0f)
		{
			load_player_inventory_ended_sucessful = false;
			if (load_process_player_inventory_ltl)
			{
				LoadPlayerInventoryState();
				upd_val1 = 0.03f;
				//load_process_player_inventory_ltl = false;
			}

			if(load_player_inventory_ended_sucessful)
				load_process_player_inventory_ltl = false;

			if (!load_process_player_inventory_ltl)
			{
				upd_val1 = 0.0f;
				load_process_ltlinfo_at_level_loading_end = false;
				upd_val2 = 0.5f;
			}
		}
	}

	if (upd_val2 > 0.0f)
	{
		upd_val2 -= gEnv->pTimer->GetFrameTime();
		if (gEnv->pTimer->GetFrameTime() > 0.02f)
			upd_val2 -= 0.02f;

		if (upd_val2 <= 0.0f)
		{
			ICVar* saveLoadEnabled = gEnv->pConsole->GetCVar("g_EnableLoadSave");
			if (saveLoadEnabled)
			{
				if (saveLoadEnabled->GetIVal() == 0)
				{
					saveLoadEnabled->Set(1);
				}
			}
			gEnv->pConsole->ExecuteString("save", true);
			upd_val2 = 0.0f;
		}
	}
}

EntityId CLevelTolevelserialization::GetSpawnPointIdFromName(string name)
{
	if (!gEnv->pEntitySystem)
		return 0;

	IEntity *pEntity = gEnv->pEntitySystem->FindEntityByName(name.c_str());
	if (pEntity)
	{
		return pEntity->GetId();
	}
	return 0;
}

bool CLevelTolevelserialization::ClearEntitesInList()
{
	//CryLogAlways("CLevelTolevelserialization::ClearEntitesInList called");
	const char *levelName = g_pGame->GetIGameFramework()->GetLevelName();
	string mappedName = g_pGame->GetMappedLevelName(levelName);
	std::vector<SLevelSerializedLTL>::iterator it = m_levels_to_loading.begin();
	std::vector<SLevelSerializedLTL>::iterator end = m_levels_to_loading.end();
	int count = m_levels_to_loading.size();
	SLevelSerializedLTL level_to_LTLS2;
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		level_to_LTLS2 = *it;
		if (level_to_LTLS2.level_name == mappedName && !level_to_LTLS2.level_name.empty())
		{
			(*it).level_name.clear();
			(*it).m_level_actors.clear();
			(*it).m_level_actors.resize(0);
			(*it).m_level_items.clear();
			(*it).m_level_items.resize(0);
			(*it).m_level_objects.clear();
			(*it).m_level_objects.resize(0);
			m_levels_to_loading.erase(it);
			m_levels_to_loading.resize(count - 1);
			break;
		}
	}
	//CryLogAlways("CLevelTolevelserialization::ClearEntitesInList end");
	return true;
}

void CLevelTolevelserialization::SaveAllEntitesOnLevel()
{
	//CryLogAlways("CLevelTolevelserialization::SaveAllEntitesOnLevel called");
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	SLevelSerializedLTL level_to_LTLS;
	const char *levelName = g_pGame->GetIGameFramework()->GetLevelName();
	string mappedName = g_pGame->GetMappedLevelName(levelName);
	level_to_LTLS.level_name = mappedName;
	IEntityItPtr pIt = gEnv->pEntitySystem->GetEntityIterator();
	pIt->MoveFirst();
	for (int i = 0; !pIt->IsEnd(); ++i)
	{
		IEntity *pEntity = pIt->Next();
		if (!pEntity)
			continue;

		if(pClient->GetEntityId() == pEntity->GetId())
			continue;

		CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
		CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pEntity->GetId()));
		if (pActor)
		{
			SSerializedLtlActor act_this;
			act_this.actorId = pEntity->GetId();
			act_this.actor_position = pEntity->GetWorldTM();
			act_this.actorName = pEntity->GetName();
			act_this.dialog_stage = pActor->GetDialogStage();
			act_this.health = pActor->GetHealth();
			level_to_LTLS.m_level_actors.push_back(act_this);
		}
		else if (pItem)
		{
			if(pClient->GetEntityId() == pItem->GetOwnerId())
				continue;

			SSerializedLtlItem item_this;
			item_this.item_id = pEntity->GetId();
			item_this.item_position = pEntity->GetWorldTM();
			if(pEntity->GetClass())
				item_this.item_class_name = pEntity->GetClass()->GetName();

			item_this.quantity_state = pItem->GetItemQuanity();
			item_this.equipped_state = pItem->GetEquipped();
			item_this.actorOwnerId = pItem->GetOwnerId();
			item_this.is_nodraw_type = pItem->IsItemIgnoredByHud();
			level_to_LTLS.m_level_items.push_back(item_this);
		}
		else
		{
			IScriptTable *pEntityTable = pEntity->GetScriptTable();
			SmartScriptTable pProperties;
			bool ltl_pos_and_rot = false;
			if (pEntityTable && pEntityTable->GetValue("Properties", pProperties))
			{
				pProperties->GetValue("ltl_pos_and_rot_serialization", ltl_pos_and_rot);
			}
			SSerializedLtlObject obj_this;
			obj_this.object_id = pEntity->GetId();
			obj_this.oblect_position = pEntity->GetWorldTM();
			if (ltl_pos_and_rot)
				obj_this.oblect_position = pEntity->GetWorldTM();
			else
				obj_this.oblect_position.SetTranslation(Vec3(ZERO));

			level_to_LTLS.m_level_objects.push_back(obj_this);
		}
	}
	std::vector<SLevelSerializedLTL>::iterator it = m_levels_to_loading.begin();
	std::vector<SLevelSerializedLTL>::iterator end = m_levels_to_loading.end();
	int count = m_levels_to_loading.size();
	SLevelSerializedLTL level_to_LTLS2;
	for (int i = 0; i < count, it != end; i++, ++it)
	{
		level_to_LTLS2 = *it;
		if (level_to_LTLS2.level_name == mappedName)
		{
			(*it).level_name.clear();
			(*it).m_level_actors.clear();
			(*it).m_level_items.clear();
			(*it).m_level_objects.clear();
			m_levels_to_loading.erase(it);
			m_levels_to_loading.resize(count - 1);
			break;
		}
	}
	m_levels_to_loading.push_back(level_to_LTLS);
}

void CLevelTolevelserialization::LoadAllEntitesToLevel()
{
	//CryLogAlways("CLevelTolevelserialization::LoadAllEntitesToLevel called");
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	const char *levelName = g_pGame->GetIGameFramework()->GetLevelName();
	string mappedName = g_pGame->GetMappedLevelName(levelName);
	std::vector<EntityId> ents_to_not_delete;
	std::vector<SLevelSerializedLTL>::iterator it = m_levels_to_loading.begin();
	std::vector<SLevelSerializedLTL>::iterator end = m_levels_to_loading.end();
	int count2 = m_levels_to_loading.size();
	int count = 0;
	SLevelSerializedLTL level_to_LTLS;
	for (int i = 0; i < count2, it != end; i++, ++it)
	{
		level_to_LTLS = *it;
		if (level_to_LTLS.level_name == mappedName)
		{
			break;
		}
	}

	if(level_to_LTLS.level_name != mappedName)
		return;

	SSerializedLtlObject pd_obj;
	std::vector<SSerializedLtlObject>::iterator it_obj = level_to_LTLS.m_level_objects.begin();
	std::vector<SSerializedLtlObject>::iterator end_obj = level_to_LTLS.m_level_objects.end();
	count = level_to_LTLS.m_level_objects.size();
	for (int i = 0; i < count, it_obj != end_obj; i++, ++it_obj)
	{
		pd_obj = *it_obj;
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pd_obj.object_id);
		if (!pEntity)
			continue;

		if (pClient->GetEntityId() == pEntity->GetId())
			continue;

		if (!pd_obj.oblect_position.GetTranslation().IsZero())
		{
			pEntity->SetPos(pd_obj.oblect_position.GetTranslation());
			pEntity->SetRotation(Quat(pd_obj.oblect_position));
		}
		//pEntity->SetWorldTM(pd_obj.oblect_position);
		ents_to_not_delete.push_back(pd_obj.object_id);
	}
	SSerializedLtlActor pd_actor;
	std::vector<SSerializedLtlActor>::iterator it_actors = level_to_LTLS.m_level_actors.begin();
	std::vector<SSerializedLtlActor>::iterator end_actors = level_to_LTLS.m_level_actors.end();
	count = level_to_LTLS.m_level_actors.size();
	for (int i = 0; i < count, it_actors != end_actors; i++, ++it_actors)
	{
		pd_actor = *it_actors;
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pd_actor.actorId);
		if (!pEntity)
			continue;

		if (pClient->GetEntityId() == pEntity->GetId())
			continue;

		CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pEntity->GetId()));
		if(!pActor)
			continue;

		pActor->SetDialogStage(pd_actor.dialog_stage);
		pEntity->SetWorldTM(pd_actor.actor_position);
		if (pd_actor.health <= 0.0f && !pd_actor.actor_position.IsZero())
		{
			pActor->Fall();
		}
		pActor->SetHealth(pd_actor.health);
		ents_to_not_delete.push_back(pd_actor.actorId);
	}
	SSerializedLtlItem pd_item;
	std::vector<SSerializedLtlItem>::iterator it_items = level_to_LTLS.m_level_items.begin();
	std::vector<SSerializedLtlItem>::iterator end_items = level_to_LTLS.m_level_items.end();
	count = level_to_LTLS.m_level_items.size();
	for (int i = 0; i < count, it_items != end_items; i++, ++it_items)
	{
		pd_item = *it_items;
		IEntity *pEntity = gEnv->pEntitySystem->GetEntity(pd_item.item_id);
		if (!pEntity)
			continue;

		if (pClient->GetEntityId() == pEntity->GetId())
			continue;

		CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pEntity->GetId()));
		if (!pItem)
			continue;

		if (pd_item.quantity_state < 0)
			continue;

		pItem->SetItemQuanity(pd_item.quantity_state);
		if (pItem->GetOwnerId() != pd_item.actorOwnerId && pItem->GetOwner() && pd_item.actorOwnerId != 0)
		{
			CActor* pActorOwn2 = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pItem->GetOwnerId()));
			CActor* pActorOwn1 = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pd_item.actorOwnerId));
			if (pActorOwn1 && pActorOwn2)
			{
				pActorOwn2->DropItem(pEntity->GetId());
				pActorOwn1->PickUpItem(pEntity->GetId(), false, false);
			}


			if (pActorOwn2)
			{

			}
		}
		if (pItem->GetOwnerId() != pd_item.actorOwnerId && pItem->GetOwner() && pd_item.actorOwnerId <= 0)
		{
			CActor* pActorOwn2 = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pItem->GetOwnerId()));
			if (pActorOwn2)
			{
				pActorOwn2->DropItem(pEntity->GetId());
			}
		}
		if (pItem->GetEquipped() > 0 && pd_item.equipped_state == 0)
		{
			CActor* pActor = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pItem->GetOwnerId()));
			if (pActor)
			{
				if (pItem->IsSelected())
				{
					pActor->SelectItem(0, false, true);
				}
			}
		}
		pEntity->SetWorldTM(pd_item.item_position);
		ents_to_not_delete.push_back(pd_item.item_id);
	}
	IEntityItPtr pIt = gEnv->pEntitySystem->GetEntityIterator();
	pIt->MoveFirst();
	for (int i = 0; !pIt->IsEnd(); ++i)
	{
		IEntity *pEntity = pIt->Next();
		if (!pEntity)
			continue;

		if (pClient->GetEntityId() == pEntity->GetId())
			continue;

		if(!stl::find(ents_to_not_delete, pEntity->GetId()))
			gEnv->pEntitySystem->RemoveEntity(pEntity->GetId());
	}
}

void CLevelTolevelserialization::LoadPlayerChToLevel()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	pClient->RequestToReloadActorModelOrReinitAttachments(false, true);
	pClient->serialization_reinit_customization_setting_on_character = 0.01f;
	pClient->rload_pl_model_timer_after_ps = 0.0f;
	pClient->reequip_timer_cn1 = 0.0f;
	/*CPlayerCharacterCreatingSys* p_PlCrSys = g_pGame->GetPlayerCharacterCreatingSys();
	if (!p_PlCrSys)
		return;*/
}

void CLevelTolevelserialization::SetNextSpawnPoint(EntityId point_id)
{
	next_spawn_point_id = point_id;
}

void CLevelTolevelserialization::LoadSpecialPlayerPos()
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	if(!pl_pos_spec.IsZero())
		pClient->GetEntity()->SetPos(pl_pos_spec);
}

void CLevelTolevelserialization::SpecialFunctionProcessing(ELTLSpecFunctions fnc_id)
{
	CPlayer *pClient = static_cast<CPlayer*>(g_pGame->GetIGameFramework()->GetClientActor());
	if (!pClient)
		return;

	switch (fnc_id)
	{
	case eLTLSF_RequestLoadLevelFTSpellMark:
	{
		SaveAllPlInfoForLTL();
		SaveAllEntitesOnLevel();
		load_process_ltlinfo_at_level_loading_end = true; load_process_tod_time = true; load_process_player_ltl = true; load_process_player_inventory_ltl = true;
		load_process_player_special_pos_ltl = true; load_process_player_quests_ltl = true; load_process_player_spells_ltl = true;
		pl_pos_spec = pl_pos;
		string stringToExecute = "";
		stringToExecute.Format("map %s", pl_marked_lv.c_str());
		gEnv->pConsole->ExecuteString(stringToExecute.c_str(), true, true);
		break;
	}
	break;
	}
}

void CLevelTolevelserialization::SaveAllPlInfoForLTL()
{
	SavePlayerInventoryState();
	SavePlayerSpellBook();
	SaveQuests();
	SavePlayerParams();
}

void CLevelTolevelserialization::GetMemoryUsage(ICrySizer * s) const
{
	s->AddObject(this, sizeof(*this));
}
