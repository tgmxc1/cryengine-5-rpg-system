/*#ifndef __MELEEALT_H__
#define __MELEEALT_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include "Weapon.h"
//#include "ItemParamReader.h"
#include "IGameRulesSystem.h"
#include "GameXmlParamReader.h"


#define ResetValue(name, defaultValue) if (defaultInit) name=defaultValue; reader.Read(#name, name)
#define ResetValueEx(name, var, defaultValue) if (defaultInit) var=defaultValue; reader.Read(name, var)

class CMeleeAlt :
	public IFireMode
{
	struct StartAttackingAction;
	struct StopAttackingAction;

	typedef struct SMeleeAltParams
	{
		SMeleeAltParams() { Reset(); };
		void Reset(const IItemParamsNode *params=0, bool defaultInit=true)
		{
			CGameXmlParamReader reader(params);
			ResetValue(helper,				"");
			ResetValue(range,					1.75f);
			ResetValue(animspdmult,					1.0f);
			ResetValue(ray_test_time,					1.0f);
			ResetValue(damage,				32);
			ResetValue(damageAI,			10);
			ResetValue(crosshair,			"default");
			ResetValue(hit_type,			"melee");
			ResetValue(impulse,				50.0f);
			ResetValue(delay,					0.5f);
			ResetValue(duration,			0.5f);
			ResetValue(hold_duration, 1.0f);
			ResetValue(hold_min_scale,1.0f);
			ResetValue(hold_max_scale,1.0f);
			ResetValue(first_hited_ent_dmg_mlt,					1.0f);
			ResetValue(second_hited_ent_dmg_mlt,					1.0f);
			ResetValue(third_hited_ent_dmg_mlt,					1.0f);
			ResetValue(fourth_hited_ent_dmg_mlt,					1.0f);
			ResetValue(other_hited_ent_dmg_mlt,					1.0f);
		}

		void GetMemoryStatistics(ICrySizer * s)
		{
			s->Add(crosshair);
			s->Add(hit_type);
		}

		ItemString	helper;
		float				range;
		float				animspdmult;

		short				damage;
		short				damageAI;

		float first_hited_ent_dmg_mlt;
		float second_hited_ent_dmg_mlt;
		float third_hited_ent_dmg_mlt;
		float fourth_hited_ent_dmg_mlt;
		float other_hited_ent_dmg_mlt;

		ItemString	crosshair;
		ItemString	hit_type;
		float				ray_test_time;

		float		impulse;

		float		delay;
		float		duration;

		float hold_duration;
		float hold_min_scale;
		float hold_max_scale;

	} SMeleeAltParams;

	typedef struct SMeleeAltActions
	{
		SMeleeAltActions() { Reset(); };
		void Reset(const IItemParamsNode *params=0, bool defaultInit=true)
		{
			CItemParamReader reader(params);
			ResetValue(attack,			"attack");
			ResetValue(hold,		"hold");
			ResetValue(attackf,			"attackf");
			ResetValue(holdf,		"holdf");
			ResetValue(attackl,			"attackl");
			ResetValue(holdl,		"holdl");
			ResetValue(attackr,			"attackr");
			ResetValue(holdr,		"holdr");
			ResetValue(attackb,			"attackb");
			ResetValue(holdb,		"holdb");
			ResetValue(pull,		"pull");
			ResetValue(hit,					"hit");
		}

		void GetMemoryStatistics(ICrySizer * s)
		{
			s->Add(attack);
			s->Add(hold);
			s->Add(attackf);
			s->Add(holdf);
			s->Add(attackl);
			s->Add(holdl);
			s->Add(attackr);
			s->Add(holdr);
			s->Add(attackb);
			s->Add(holdb);
			s->Add(pull);
			s->Add(hit);
		}

		ItemString	attack;
		ItemString hold;
		ItemString	attackf;
		ItemString holdf;
		ItemString	attackl;
		ItemString holdl;
		ItemString	attackr;
		ItemString holdr;
		ItemString	attackb;
		ItemString holdb;
		ItemString pull;
		ItemString	hit;
	} SMeleeAltActions;

public:
	CMeleeAlt();
	virtual ~CMeleeAlt();

	//IFireMode
	virtual void Init(IWeapon *pWeapon, const struct IItemParamsNode *params);
	virtual void Update(float frameTime, uint frameId);
	virtual void PostUpdate(float frameTime) {};
	virtual void UpdateFPView(float frameTime) {};
	virtual void Release();
	virtual void GetMemoryStatistics(ICrySizer * s);

	virtual void ResetParams(const struct IItemParamsNode *params);
	virtual void PatchParams(const struct IItemParamsNode *patch);

	virtual void Activate(bool activate);

	virtual int GetAmmoCount() const { return 0; };
	virtual int GetClipSize() const { return 0; };

	virtual bool OutOfAmmo() const { return false; };
	virtual bool CanReload() const { return false; };
	virtual void Reload(int zoomed) {};
	virtual bool IsReloading() { return false; };
	virtual void CancelReload() {};
	virtual bool CanCancelReload() { return false; };

	virtual bool AllowZoom() const { return true; };
	virtual void Cancel() {};

	virtual float GetRecoil() const { return 0.0f; };
	virtual float GetSpread() const { return 0.0f; };
	virtual float GetMinSpread() const { return 0.0f; }
	virtual float GetMaxSpread() const { return 0.0f; }
	virtual const char *GetCrosshair() const { return ""; };

	virtual bool CanFire(bool considerAmmo=true) const;
	virtual void StartFire();
	virtual void StopFire();
	virtual bool IsFiring() const { return m_attacking; };
	virtual float GetHeat() const { return 0.0f; };
	virtual bool	CanOverheat() const {return false;};

	virtual void NetShoot(const Vec3 &hit, int ph);
	virtual void NetShootEx(const Vec3 &pos, const Vec3 &dir, const Vec3 &vel, const Vec3 &hit, float extra, int ph);
	virtual void NetEndReload() {};

	virtual void NetStartFire();
	virtual void NetStopFire();

	virtual EntityId GetProjectileId() const { return 0; };
	virtual EntityId RemoveProjectileId() { return 0; };
	virtual void SetProjectileId(EntityId id) {};

	virtual const char *GetType() const;
	virtual IEntityClass* GetAmmoType() const { return 0; };
	virtual int GetDamage(float distance=0.0f) const;
	virtual const char *GetDamageType() const;

	virtual float GetSpinUpTime() const { return 0.0f; };
	virtual float GetSpinDownTime() const { return 0.0f; };
  virtual float GetNextShotTime() const { return 0.0f; };
	virtual void SetNextShotTime(float time) {};
  virtual float GetFireRate() const { return 0.0f; };

	virtual void Enable(bool enable) { m_enabled = enable; };
	virtual bool IsEnabled() const { return m_enabled; };

	virtual Vec3 GetFiringPos(const Vec3 &probableHit) const {return ZERO;}
	virtual Vec3 GetFiringDir(const Vec3 &probableHit, const Vec3& firingPos) const {return ZERO;}
	virtual void SetName(const char *name) { m_name = name; };
	virtual const char *GetName() { return m_name.empty()?0:m_name.c_str();};

  virtual bool HasFireHelper() const { return false; }
  virtual Vec3 GetFireHelperPos() const { return Vec3(ZERO); }
  virtual Vec3 GetFireHelperDir() const { return FORWARD_DIRECTION; }

  virtual int GetCurrentBarrel() const { return 0; }
	virtual void Serialize(TSerialize ser) {};
	virtual void PostSerialize(){};

	virtual void SetRecoilMultiplier(float recoilMult) { }
	virtual float GetRecoilMultiplier() const { return 1.0f; }

	virtual void PatchSpreadMod(SSpreadModParams &sSMP){};
	virtual void ResetSpreadMod(){};

	virtual void PatchRecoilMod(SRecoilModParams &sRMP){};
	virtual void ResetRecoilMod(){};

	virtual void ResetLock() {};
	virtual void StartLocking(EntityId targetId, int partId) {};
	virtual void Lock(EntityId targetId, int partId) {};
	virtual void Unlock() {};

	virtual void SetAmmoType(const char* type) { };

	//~IFireMode

	virtual bool PerformRayTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual bool PerformRayTest2(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual void Hit2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote);
	virtual bool PerformCylinderTest(const Vec3 &pos, const Vec3 &dir, float strength, bool remote);
	virtual void Hit(ray_hit *hit, const Vec3 &dir, float damageScale, bool remote);
	virtual void Hit(geom_contact *contact, const Vec3 &dir, float damageScale, bool remote);
	virtual void Hit(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float damageScale, bool remote);
	virtual void Impulse(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale);
	virtual void Impulse2(const Vec3 &pt, const Vec3 &dir, const Vec3 &normal, IPhysicalEntity *pCollider, int partId, int ipart, int surfaceIdx, float impulseScale);
	
	virtual void ApplyCameraShake(bool hit);

	//Special case when performing melee with an object (offHand)
	//It must ignore also the held entity!
	virtual void IgnoreEntity(EntityId entityId) { m_ignoredEntity = entityId; }
	virtual void MeleeScale(float scale) { m_meleeScale = scale; }
	virtual float GetOwnerStrength() const;


protected:
	CWeapon *m_pWeapon;
	bool		m_enabled;

	bool		m_attacking;
	Vec3		m_last_pos;

	float		m_delayTimer;
	float		m_durationTimer;
	string	m_name;
	Vec3		m_beginPos;
	bool		m_attacked;

	float	m_hold_timer;

	EntityId	m_ignoredEntity;
	float			m_meleeScale;

	SMeleeAltParams	m_meleeparams;
	SMeleeAltActions	m_meleeactions;

	bool			m_noImpulse;
	virtual void DoAttack();
	bool	m_atkn;
	bool	m_pulling;
	bool	m_atking;
	float	m_atk_time;
	bool	m_holdedf;
	bool	m_holdedr;
	bool	m_holdedl;
	bool	m_holdedb;
	bool  m_forceNextAtk;

	float			m_crt_hit_dmg_mult;
	int num_hitedEntites;
	
	float		m_updtimer;
	float		m_updtimer2;
	std::vector<EntityId> m_hitedEntites;

	bool			m_animspeed_returned;

};



#endif*/ //__MELEE_H__