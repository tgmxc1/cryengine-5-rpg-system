// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
 -------------------------------------------------------------------------
  $Id$
  $DateTime$
  Description: Exposes actor functionality to LUA
  
 -------------------------------------------------------------------------
  History:
  - 7:10:2004   14:19 : Created by Márcio Martins

*************************************************************************/
#ifndef __SCRIPTBIND_ACTOR_H__
#define __SCRIPTBIND_ACTOR_H__

#if _MSC_VER > 1000
# pragma once
#endif


#include <CryScriptSystem/IScriptSystem.h>
#include <CryScriptSystem/ScriptHelpers.h>


struct IGameFramework;
struct IActor;


// <title Actor>
// Syntax: Actor
class CScriptBind_Actor :
	public CScriptableBase
{
public:
	CScriptBind_Actor(ISystem *pSystem);
	virtual ~CScriptBind_Actor();

	void AttachTo(IActor *pActor);

	//------------------------------------------------------------------------
	int DumpActorInfo(IFunctionHandler *pH);

	//New Equip
	virtual int Add_additional_equip(IFunctionHandler *pH);
	virtual int Add_additional_equip2(IFunctionHandler *pH);
	//new actoins
	virtual int StartMeleeAttack(IFunctionHandler *pH);
	virtual int StartMeleeAttackComplexAction(IFunctionHandler *pH, float time, float delay, float damage, const char *actionName);
	virtual int StartBlockAction(IFunctionHandler *pH);
	virtual int StopBlockAction(IFunctionHandler *pH);
	virtual int StartSimpleAttackAction(IFunctionHandler *pH);
	virtual int StopSimpleAttackAction(IFunctionHandler *pH);
	virtual int RequestSwitchToWeaponInInventory(IFunctionHandler *pH, const char *itemName);
	virtual int RequestEquipItemFromInventory(IFunctionHandler *pH, const char *itemName);
	int GetCurrentSelectedWeaponType(IFunctionHandler *pH);
	virtual int RequestStartWpnThrowing(IFunctionHandler *pH);
	virtual int RequestToDoWpnThrowing(IFunctionHandler *pH, float force);
	int SwitchToAnyMeleeWeapon(IFunctionHandler *pH);
	int SwitchToAnyWeapon(IFunctionHandler *pH);
	int SwitchToAnyRangeWeapon(IFunctionHandler *pH);
	int SwitchToNoWpn(IFunctionHandler *pH);
	int CheckCurrentWeaponAmmo(IFunctionHandler *pH);
	int IsCurrentSelectedWeaponMeleeSupport(IFunctionHandler *pH);
	int IsAITargetHaveAndVisibleOrObstr(IFunctionHandler *pH);
	int GetAITargetFT(IFunctionHandler *pH);
	int GetEquippedBowAmmo(IFunctionHandler *pH);
	int ResetVars(IFunctionHandler *pH);
	int DeequipAllEquippedItems(IFunctionHandler *pH);
	int FullReloadActorModel(IFunctionHandler *pH);
	int GetBuffEffectActive(IFunctionHandler *pH, int id);
	int GetActorResistForDamageType(IFunctionHandler *pH, int id);
	int AddUsableSpell(IFunctionHandler *pH, int id, bool fromXml);
	int AddSpellToCastSlot(IFunctionHandler *pH, int id, int slot);
	int IsSpellInUsableSpells(IFunctionHandler *pH, int id);
	int CanCastSpellFromSlot(IFunctionHandler *pH, int slot);
	int GetLeftHandItemId(IFunctionHandler *pH);
	int SetNumberOfAbleInventorySlots(IFunctionHandler *pH, int slotsNum);
	int SetActorRelaxedState(IFunctionHandler *pH, bool relaxed);
	int SetActorStealthState(IFunctionHandler *pH, bool stealth);
	int ActorJumpRequest(IFunctionHandler *pH);
	int ActivateBuEffectOnActor(IFunctionHandler *pH, int id, float time, int effId);
	int ActivateSpecPassivePerkOnActor(IFunctionHandler *pH, int id, bool active, int level);
	int TryToEquipUnSelectableItem(IFunctionHandler *pH, EntityId itemId);
	int GetCurrentEquippedSlotItemId(IFunctionHandler *pH, int slotId);
	int CreateParticleEmitterOnActor(IFunctionHandler *pH, int characterSlot, const char *attachmentName, const char *boneName, const char *particleEffectName, bool setDefPose);
	int CreateSimpleLightOnActor(IFunctionHandler *pH, const char *attachmentName, const char *boneName, bool setDefPose, float colR, float colG, float colB, float colA, float rad, float intense, bool shadows);
	int DeleteParticleEmitterOrLigthFromActor(IFunctionHandler *pH, int characterSlot, const char *attachmentName, bool clearOnly);
	int SimulateDamageToActor(IFunctionHandler *pH, float damage, int hitType);
	int GetBuffEffectTime(IFunctionHandler *pH, int id);
	int AddBuffEffectTime(IFunctionHandler *pH, int id, float time);
	int PlayAnimAction(IFunctionHandler *pH, const char *actionName);
	int CreateBloodLooseEffect(IFunctionHandler *pH);
	int ClearAllBloodLooseEffects(IFunctionHandler *pH);
	int SetActorResistForDamageType(IFunctionHandler *pH, int id, float rsvalue);
	int GetActorResistForDamageTypeByName(IFunctionHandler *pH, const char *name);
	int SetActorResistForDamageTypeByName(IFunctionHandler *pH, const char *name, float rsvalue);
	int AddActorDamageResistValueByResName(IFunctionHandler *pH, const char *name, float rsvalue);
	int DelActorDamageResistValueByResName(IFunctionHandler *pH, const char *name, float rsvalue);
	int AddActorDamageResistValueByResId(IFunctionHandler *pH, int id, float rsvalue);
	int DelActorDamageResistValueByResId(IFunctionHandler *pH, int id, float rsvalue);
	int ActorStartCastSpellSimple(IFunctionHandler *pH);
	int ActorStopCastSpellSimple(IFunctionHandler *pH);
	int ActorSelectAnySpellSimple(IFunctionHandler *pH);

	int GetActorHealthPercents(IFunctionHandler *pH);

	int Revive(IFunctionHandler *pH);
	int Kill(IFunctionHandler *pH);
	int ShutDown(IFunctionHandler *pH);
	int SetParams(IFunctionHandler *pH);
	int GetHeadDir(IFunctionHandler *pH);
	int GetAimDir(IFunctionHandler *pH);

	int PostPhysicalize(IFunctionHandler *pH);
	int GetChannel(IFunctionHandler *pH);
	int IsPlayer(IFunctionHandler *pH);
	int IsLocalClient(IFunctionHandler *pH);
	int GetLinkedVehicleId(IFunctionHandler *pH);

	int LinkToEntity(IFunctionHandler *pH);
	int SetAngles(IFunctionHandler *pH,Ang3 vAngles );
	int PlayerSetViewAngles( IFunctionHandler *pH,Ang3 vAngles );
	int GetAngles(IFunctionHandler *pH);

	int SetMovementTarget(IFunctionHandler *pH, Vec3 pos, Vec3 target, Vec3 up, float speed);
	int CameraShake(IFunctionHandler *pH,float amount,float duration,float frequency,Vec3 pos);
	int SetViewShake(IFunctionHandler *pH, Ang3 shakeAngle, Vec3 shakeShift, float duration, float frequency, float randomness);

	int SetExtensionParams(IFunctionHandler* pH, const char *extension, SmartScriptTable params);

	int SvRefillAllAmmo(IFunctionHandler* pH, const char* refillType, bool refillAll, int grenadeCount, bool bRefillCurrentGrenadeType);
	int ClRefillAmmoResult(IFunctionHandler* pH, bool ammoRefilled);

	int SvGiveAmmoClips(IFunctionHandler* pH, int numClips);

	int IsImmuneToForbiddenArea(IFunctionHandler *pH);

	int SetHealth(IFunctionHandler *pH, float health);
	int SetMaxHealth(IFunctionHandler *pH, float health);
	int GetHealth(IFunctionHandler *pH);
	int GetMaxHealth(IFunctionHandler *pH);
	int DamageInfo(IFunctionHandler *pH, ScriptHandle shooter, ScriptHandle target, ScriptHandle weapon, ScriptHandle projectile, float damage, int damageType, Vec3 hitDirection);
	int GetLowHealthThreshold(IFunctionHandler *pH);

	int AddLVLxp(IFunctionHandler *pH, float xp);

	int SetStamina(IFunctionHandler *pH, float stamina);
	int SetMaxStamina(IFunctionHandler *pH, float stamina);
	int SetMagicka(IFunctionHandler *pH, float magicka);
	int SetMaxMagicka(IFunctionHandler *pH, float magicka);
	int SetStrength(IFunctionHandler *pH, float strength);
	int SetAgility(IFunctionHandler *pH, float agility);
	int SetWillpower(IFunctionHandler *pH, float willpower);
	int SetEndurance(IFunctionHandler *pH, float endurance);
	int SetPersonality(IFunctionHandler *pH, float personality);
	int SetIntelligence(IFunctionHandler *pH, float intelligence);
	int SetLevel(IFunctionHandler *pH, float level);
	int SetGender(IFunctionHandler *pH, float gender);
	int SetGold(IFunctionHandler *pH, float gold);
	int SetArmor(IFunctionHandler *pH, float armor);
	int GetStrength(IFunctionHandler *pH);
	int GetArmor(IFunctionHandler *pH);
	int GetMaxArmor(IFunctionHandler *pH);
	int GetStamina(IFunctionHandler *pH);
	int GetMaxStamina(IFunctionHandler *pH);
	int GetGender(IFunctionHandler *pH);
	int GetLevel(IFunctionHandler *pH);
	int GetMagicka(IFunctionHandler *pH);
	int GetMaxMagicka(IFunctionHandler *pH);
	int GetGold(IFunctionHandler *pH);
	int GetAgility(IFunctionHandler *pH);
	int GetWillpower(IFunctionHandler *pH);
	int GetEndurance(IFunctionHandler *pH);
	int GetPersonality(IFunctionHandler *pH);
	int GetIntelligence(IFunctionHandler *pH);



	int SetPhysicalizationProfile(IFunctionHandler *pH, const char *profile);
	int GetPhysicalizationProfile(IFunctionHandler *pH);

	int QueueAnimationState(IFunctionHandler *pH, const char *animationState);

	int CreateCodeEvent(IFunctionHandler *pH,SmartScriptTable params);

	int PauseAnimationGraph(IFunctionHandler* pH);
	int ResumeAnimationGraph(IFunctionHandler* pH);
	int HurryAnimationGraph(IFunctionHandler* pH);
	int SetTurnAnimationParams(IFunctionHandler* pH, const float turnThresholdAngle, const float turnThresholdTime);

	int SetSpectatorMode(IFunctionHandler *pH, int mode, ScriptHandle targetId);
	int GetSpectatorMode(IFunctionHandler *pH);
	int GetSpectatorState(IFunctionHandler *pH);
	int GetSpectatorTarget(IFunctionHandler* pH);

	int Fall(IFunctionHandler *pH, Vec3 hitPos);

	int StandUp(IFunctionHandler *pH);

	int GetExtraHitLocationInfo(IFunctionHandler *pH, int slot, int partId);

	int SetForcedLookDir(IFunctionHandler *pH, CScriptVector dir);
	int ClearForcedLookDir(IFunctionHandler *pH);
	int SetForcedLookObjectId(IFunctionHandler *pH, ScriptHandle objectId);
	int ClearForcedLookObjectId(IFunctionHandler *pH);

	int CanSpectacularKillOn(IFunctionHandler *pH, ScriptHandle targetId);
	int StartSpectacularKill(IFunctionHandler *pH, ScriptHandle targetId);
	int StartSpectacularKillWId(IFunctionHandler *pH, int targetId);
	int RegisterInAutoAimManager(IFunctionHandler *pH);
	int CheckBodyDamagePartFlags(IFunctionHandler *pH, int partID, int materialID, uint32 bodyPartFlagsMask);
	int GetBodyDamageProfileID(IFunctionHandler* pH, const char* bodyDamageFileName, const char* bodyDamagePartsFileName);
	int OverrideBodyDamageProfileID(IFunctionHandler* pH, const int bodyDamageProfileID);
	int IsGod(IFunctionHandler* pH);

	//------------------------------------------------------------------------
	// ITEM STUFF
	//------------------------------------------------------------------------
	int HolsterItem(IFunctionHandler *pH, bool holster);
	int DropItem(IFunctionHandler *pH, ScriptHandle itemId);
	int PickUpItem(IFunctionHandler *pH, ScriptHandle itemId);
	int IsCurrentItemHeavy( IFunctionHandler* pH );
	int PickUpPickableAmmo(IFunctionHandler *pH, const char *ammoName, int count);

	int SelectItemByName(IFunctionHandler *pH, const char *name);
	int SelectItem(IFunctionHandler *pH, ScriptHandle itemId, bool forceSelect);
	int SelectLastItem(IFunctionHandler *pH);
	int SelectNextItem(IFunctionHandler *pH, int direction, bool keepHistory, const char *category);
	int SimpleFindItemIdInCategory(IFunctionHandler *pH, const char *category);

	int DisableHitReaction(IFunctionHandler *pH);
	int EnableHitReaction(IFunctionHandler *pH);

	int CreateIKLimb( IFunctionHandler *pH, int slot, const char *limbName, const char *rootBone, const char *midBone, const char *endBone, int flags);

	int PlayAction(IFunctionHandler *pH, const char* action);

	//misc
	int RefreshPickAndThrowObjectPhysics( IFunctionHandler *pH );

	int AcquireOrReleaseLipSyncExtension(IFunctionHandler *pH);

protected:
	class CActor *GetActor(IFunctionHandler *pH);

	bool IsGrenadeClass(const IEntityClass* pEntityClass) const;
	bool RefillOrGiveGrenades(CActor& actor, IInventory& inventory, IEntityClass* pGrenadeClass, int grenadeCount);

	ISystem					*m_pSystem;
	IGameFramework	*m_pGameFW;
};

#endif //__SCRIPTBIND_ACTOR_H__
