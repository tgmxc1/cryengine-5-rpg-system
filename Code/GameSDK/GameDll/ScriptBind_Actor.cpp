// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

/*************************************************************************
 -------------------------------------------------------------------------
  $Id$
  $DateTime$
  
 -------------------------------------------------------------------------
  History:
  - 7:10:2004   14:19 : Created by Márcio Martins

*************************************************************************/
#include "StdAfx.h"
#include "ScriptBind_Actor.h"
#include "IMovementController.h"
#include "Item.h"
#include "Game.h"
#include "Player.h"
#include "GameCVars.h"
#include "Utility/StringUtils.h"
#include "WeaponSystem.h"
#include "FireMode.h"
#include "Projectile.h"

#include <CryGame/IGameFramework.h>
#include <IVehicleSystem.h>
#include <IGameObject.h>
#include <CryMath/Cry_Geo.h>
#include <CryMath/Cry_GeoDistance.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <IViewSystem.h>
#include <CryAISystem/IAIActor.h>
#include "GameRules.h"
#include "PickAndThrowWeapon.h"

#include "UI/HUD/HUDEventDispatcher.h"
#include "PersistantStats.h"

#include "ActorActionsNew.h"

#if CRY_PLATFORM_WINDOWS && CRY_PLATFORM_64BIT
	#pragma warning(disable: 4244)
#endif

#if !defined(DEMO_BUILD_RPG_SS) && !defined(PERFORMANCE_BUILD)
	#define GOD_MODE_ENABLED
#endif

//------------------------------------------------------------------------
CScriptBind_Actor::CScriptBind_Actor(ISystem *pSystem)
: m_pSystem(pSystem),
	m_pGameFW(pSystem->GetIGame()->GetIGameFramework())
{
	Init(pSystem->GetIScriptSystem(), pSystem, 1);

	//////////////////////////////////////////////////////////////////////////
	// Init tables.
	//////////////////////////////////////////////////////////////////////////
#undef SCRIPT_REG_CLASSNAME
#define SCRIPT_REG_CLASSNAME &CScriptBind_Actor::

  SCRIPT_REG_FUNC(DumpActorInfo);
	SCRIPT_REG_FUNC(Revive);
	SCRIPT_REG_FUNC(Kill);
	SCRIPT_REG_FUNC(ShutDown);
	SCRIPT_REG_FUNC(SetParams);
	SCRIPT_REG_FUNC(GetHeadDir);
	SCRIPT_REG_FUNC(GetAimDir);
	SCRIPT_REG_FUNC(PostPhysicalize);
	SCRIPT_REG_FUNC(GetChannel);
	SCRIPT_REG_FUNC(IsPlayer);
	SCRIPT_REG_FUNC(IsLocalClient);
	SCRIPT_REG_FUNC(LinkToEntity);
	//New Equip
	SCRIPT_REG_FUNC(Add_additional_equip);
	SCRIPT_REG_FUNC(Add_additional_equip2);
	//new actions
	SCRIPT_REG_FUNC(StartMeleeAttack);
	SCRIPT_REG_TEMPLFUNC(StartMeleeAttackComplexAction, "time, delay, damage, actionName");
	SCRIPT_REG_FUNC(StartBlockAction);
	SCRIPT_REG_FUNC(StopBlockAction);
	SCRIPT_REG_FUNC(StartSimpleAttackAction);
	SCRIPT_REG_FUNC(StopSimpleAttackAction);
	SCRIPT_REG_TEMPLFUNC(RequestSwitchToWeaponInInventory, "itemName");
	SCRIPT_REG_TEMPLFUNC(RequestEquipItemFromInventory, "itemName");
	SCRIPT_REG_FUNC(GetCurrentSelectedWeaponType);
	SCRIPT_REG_FUNC(RequestStartWpnThrowing);
	SCRIPT_REG_TEMPLFUNC(RequestToDoWpnThrowing, "force");
	SCRIPT_REG_FUNC(SwitchToAnyMeleeWeapon);
	SCRIPT_REG_FUNC(SwitchToAnyWeapon);
	SCRIPT_REG_FUNC(SwitchToAnyRangeWeapon);
	SCRIPT_REG_FUNC(SwitchToNoWpn);
	SCRIPT_REG_FUNC(CheckCurrentWeaponAmmo);
	SCRIPT_REG_FUNC(IsCurrentSelectedWeaponMeleeSupport);
	SCRIPT_REG_FUNC(IsAITargetHaveAndVisibleOrObstr);
	SCRIPT_REG_FUNC(GetAITargetFT);
	SCRIPT_REG_FUNC(GetEquippedBowAmmo);
	SCRIPT_REG_FUNC(ResetVars);
	SCRIPT_REG_FUNC(DeequipAllEquippedItems);
	SCRIPT_REG_FUNC(FullReloadActorModel);
	SCRIPT_REG_TEMPLFUNC(GetBuffEffectActive, "id");
	SCRIPT_REG_TEMPLFUNC(GetActorResistForDamageType, "id");
	SCRIPT_REG_TEMPLFUNC(AddUsableSpell, "id, fromXml");
	SCRIPT_REG_TEMPLFUNC(AddSpellToCastSlot, "id, slot");
	SCRIPT_REG_TEMPLFUNC(IsSpellInUsableSpells, "id");
	SCRIPT_REG_TEMPLFUNC(CanCastSpellFromSlot, "slot");
	SCRIPT_REG_FUNC(GetLeftHandItemId);
	SCRIPT_REG_TEMPLFUNC(SetNumberOfAbleInventorySlots, "slotsNum");
	SCRIPT_REG_TEMPLFUNC(SetActorRelaxedState, "relaxed");
	SCRIPT_REG_TEMPLFUNC(SetActorStealthState, "stealth");
	SCRIPT_REG_FUNC(ActorJumpRequest);
	SCRIPT_REG_TEMPLFUNC(ActivateBuEffectOnActor, "id, time, effId");
	SCRIPT_REG_TEMPLFUNC(ActivateSpecPassivePerkOnActor, "id, active, level");
	SCRIPT_REG_TEMPLFUNC(TryToEquipUnSelectableItem, "itemId");
	SCRIPT_REG_TEMPLFUNC(GetCurrentEquippedSlotItemId, "slotId");
	SCRIPT_REG_TEMPLFUNC(CreateParticleEmitterOnActor, "characterSlot, attachmentName, boneName, particleEffectName, setDefPose");
	SCRIPT_REG_TEMPLFUNC(CreateSimpleLightOnActor, "attachmentName, boneName, setDefPose, colR, colG, colB, colA, rad, intense, shadows");
	SCRIPT_REG_TEMPLFUNC(DeleteParticleEmitterOrLigthFromActor, "characterSlot, attachmentName, clearOnly");
	SCRIPT_REG_TEMPLFUNC(SimulateDamageToActor, "damage, hitType");
	SCRIPT_REG_TEMPLFUNC(GetBuffEffectTime, "id");
	SCRIPT_REG_TEMPLFUNC(AddBuffEffectTime, "id, time");
	SCRIPT_REG_TEMPLFUNC(PlayAnimAction, "actionName");
	SCRIPT_REG_FUNC(CreateBloodLooseEffect);
	SCRIPT_REG_FUNC(ClearAllBloodLooseEffects);
	SCRIPT_REG_TEMPLFUNC(SetActorResistForDamageType, "id, rsvalue");
	SCRIPT_REG_TEMPLFUNC(GetActorResistForDamageTypeByName, "name");
	SCRIPT_REG_TEMPLFUNC(SetActorResistForDamageTypeByName, "name, rsvalue");
	SCRIPT_REG_TEMPLFUNC(AddActorDamageResistValueByResName, "name, rsvalue");
	SCRIPT_REG_TEMPLFUNC(DelActorDamageResistValueByResName, "name, rsvalue");
	SCRIPT_REG_TEMPLFUNC(AddActorDamageResistValueByResId, "id, rsvalue");
	SCRIPT_REG_TEMPLFUNC(DelActorDamageResistValueByResId, "id, rsvalue");
	SCRIPT_REG_FUNC(ActorStartCastSpellSimple);
	SCRIPT_REG_FUNC(ActorStopCastSpellSimple);
	SCRIPT_REG_FUNC(ActorSelectAnySpellSimple);
	//-----------

	SCRIPT_REG_TEMPLFUNC(GetLinkedVehicleId, "");
	SCRIPT_REG_TEMPLFUNC(SetAngles,"vAngles");
	SCRIPT_REG_FUNC(GetAngles);
	SCRIPT_REG_TEMPLFUNC(SetMovementTarget,"pos,target,up,speed");
	SCRIPT_REG_TEMPLFUNC(CameraShake,"amount,duration,frequency,pos");
	SCRIPT_REG_TEMPLFUNC(SetViewShake,"shakeAngle, shakeShift, duration, frequency, randomness");

	SCRIPT_REG_TEMPLFUNC(SetExtensionParams,"extension,params");

	SCRIPT_REG_TEMPLFUNC(SvRefillAllAmmo, "refillType, refillAll, fragGrenadeCount, refillCurrentGrenadeType");
	SCRIPT_REG_TEMPLFUNC(ClRefillAmmoResult, "ammoRefilled");
	SCRIPT_REG_TEMPLFUNC(SvGiveAmmoClips, "numClips");

	SCRIPT_REG_TEMPLFUNC(SetHealth,"health");
	SCRIPT_REG_TEMPLFUNC(DamageInfo,"shooterID, targetID, weaponID, projectileID, damage, damageType, hitDirection");
	SCRIPT_REG_TEMPLFUNC(GetLowHealthThreshold, "");
	SCRIPT_REG_TEMPLFUNC(SetMaxHealth,"health");
	SCRIPT_REG_FUNC(GetHealth);
	SCRIPT_REG_FUNC(GetMaxHealth);
	SCRIPT_REG_FUNC(IsImmuneToForbiddenArea);

	//-------------------------------------------
	SCRIPT_REG_TEMPLFUNC(SetStamina, "stamina");
	SCRIPT_REG_TEMPLFUNC(SetMaxStamina, "stamina");
	SCRIPT_REG_TEMPLFUNC(SetMagicka, "magicka");
	SCRIPT_REG_TEMPLFUNC(SetMaxMagicka, "magicka");
	SCRIPT_REG_TEMPLFUNC(SetStrength, "strength");
	SCRIPT_REG_TEMPLFUNC(SetAgility, "agility");
	SCRIPT_REG_TEMPLFUNC(SetWillpower, "willpower");
	SCRIPT_REG_TEMPLFUNC(SetEndurance, "endurance");
	SCRIPT_REG_TEMPLFUNC(SetPersonality, "personality");
	SCRIPT_REG_TEMPLFUNC(SetIntelligence, "intelligence");
	SCRIPT_REG_TEMPLFUNC(SetLevel, "level");
	SCRIPT_REG_TEMPLFUNC(SetArmor, "armor");
	SCRIPT_REG_TEMPLFUNC(SetGender, "gender");
	SCRIPT_REG_TEMPLFUNC(SetGold, "gold");
	SCRIPT_REG_TEMPLFUNC(AddLVLxp, "levelXP");
	//SCRIPT_REG_FUNC(GetHealth);
	SCRIPT_REG_FUNC(GetStrength);
	//SCRIPT_REG_FUNC(GetMaxHealth);
	SCRIPT_REG_FUNC(GetArmor);
	SCRIPT_REG_FUNC(GetMaxArmor);
	SCRIPT_REG_FUNC(GetStamina);
	SCRIPT_REG_FUNC(GetMaxStamina);
	SCRIPT_REG_FUNC(GetGender);
	SCRIPT_REG_FUNC(GetLevel);
	SCRIPT_REG_FUNC(GetMagicka);
	SCRIPT_REG_FUNC(GetMaxMagicka);
	SCRIPT_REG_FUNC(GetGold);
	SCRIPT_REG_FUNC(GetAgility);
	SCRIPT_REG_FUNC(GetWillpower);
	SCRIPT_REG_FUNC(GetEndurance);
	SCRIPT_REG_FUNC(GetPersonality);
	SCRIPT_REG_FUNC(GetIntelligence);
	//-------------------------------------------

	SCRIPT_REG_TEMPLFUNC(SetPhysicalizationProfile, "profile");
	SCRIPT_REG_TEMPLFUNC(GetPhysicalizationProfile, "");

	SCRIPT_REG_TEMPLFUNC(QueueAnimationState,"animationState");
	SCRIPT_REG_TEMPLFUNC(CreateCodeEvent,"params");
	SCRIPT_REG_FUNC(PauseAnimationGraph);
	SCRIPT_REG_FUNC(ResumeAnimationGraph);
	SCRIPT_REG_FUNC(HurryAnimationGraph);
	SCRIPT_REG_TEMPLFUNC(SetTurnAnimationParams, "turnThresholdAngle, turnThresholdTime");

	SCRIPT_REG_TEMPLFUNC(SetSpectatorMode,"mode, target");
	SCRIPT_REG_TEMPLFUNC(GetSpectatorMode,"");
	SCRIPT_REG_TEMPLFUNC(GetSpectatorState,"");
	SCRIPT_REG_TEMPLFUNC(GetSpectatorTarget, "");

	SCRIPT_REG_TEMPLFUNC(Fall,"hitPosX, hitPosY, hitPosZ");
	
	SCRIPT_REG_TEMPLFUNC(GetExtraHitLocationInfo, "slot, partId");
	SCRIPT_REG_TEMPLFUNC(StandUp,"");

	SCRIPT_REG_TEMPLFUNC(SetForcedLookDir, "dir");
	SCRIPT_REG_TEMPLFUNC(ClearForcedLookDir, "");
	SCRIPT_REG_TEMPLFUNC(SetForcedLookObjectId, "objectId");
	SCRIPT_REG_TEMPLFUNC(ClearForcedLookObjectId, "");

	SCRIPT_REG_TEMPLFUNC(PlayAction, "action");
	SCRIPT_REG_TEMPLFUNC(PlayerSetViewAngles,"vAngles");

	SCRIPT_REG_TEMPLFUNC(CanSpectacularKillOn, "targetId");
	SCRIPT_REG_TEMPLFUNC(StartSpectacularKill, "targetId");
	SCRIPT_REG_TEMPLFUNC(StartSpectacularKillWId, "targetId");
	SCRIPT_REG_TEMPLFUNC(RegisterInAutoAimManager,"RegisterInAutoAimManager");
	SCRIPT_REG_TEMPLFUNC(CheckBodyDamagePartFlags, "partID, materialID, bodyPartFlagsMask");
	SCRIPT_REG_TEMPLFUNC(GetBodyDamageProfileID, "bodyDamageFileName, bodyDamagePartsFileName");
	SCRIPT_REG_TEMPLFUNC(OverrideBodyDamageProfileID, "bodyDamageProfileID");
	SCRIPT_REG_FUNC(IsGod);
	SCRIPT_REG_FUNC(AcquireOrReleaseLipSyncExtension);


	//------------------------------------------------------------------------
	// NETWORK
	//------------------------------------------------------------------------
	SCRIPT_REG_TEMPLFUNC(HolsterItem, "holster");
	SCRIPT_REG_TEMPLFUNC(DropItem, "itemId");
	SCRIPT_REG_TEMPLFUNC(PickUpItem, "itemId");
	SCRIPT_REG_TEMPLFUNC(IsCurrentItemHeavy, "");
	SCRIPT_REG_TEMPLFUNC(SelectNextItem, "direction, keepHistory, category");
	SCRIPT_REG_TEMPLFUNC(SimpleFindItemIdInCategory, "category");
	SCRIPT_REG_TEMPLFUNC(PickUpPickableAmmo, "ammoName, count");

	SCRIPT_REG_TEMPLFUNC(SelectItemByName, "name, [forceFastSelect]");
	SCRIPT_REG_TEMPLFUNC(SelectItem, "itemId, forceSelect");
	SCRIPT_REG_TEMPLFUNC(SelectLastItem, "");

	//------------------------------------------------------------------------

	//------------------------------------------------------------------------
	// HIT REACTION
	//------------------------------------------------------------------------
	SCRIPT_REG_TEMPLFUNC(EnableHitReaction, "");
	SCRIPT_REG_TEMPLFUNC(DisableHitReaction, "");
	
	SCRIPT_REG_TEMPLFUNC(CreateIKLimb,"slot,limbName,rootBone,midBone,endBone,flags");

	SCRIPT_REG_TEMPLFUNC(RefreshPickAndThrowObjectPhysics, "");

	m_pSS->SetGlobalValue("ZEROG_AREA_ID", ZEROG_AREA_ID);

	m_pSS->SetGlobalValue("IKLIMB_LEFTHAND", IKLIMB_LEFTHAND);
	m_pSS->SetGlobalValue("IKLIMB_RIGHTHAND", IKLIMB_RIGHTHAND);

	m_pSS->SetGlobalValue("INVALID_BODYDAMAGEPROFILEID", (int)INVALID_BODYDAMAGEPROFILEID);

	for (uint32 i=0; i<BONE_ID_NUM; i++)
	{
		m_pSS->SetGlobalValue(s_BONE_ID_NAME[i], i);
	}

	SCRIPT_REG_GLOBAL(eBodyDamage_PID_None);
	SCRIPT_REG_GLOBAL(eBodyDamage_PID_Headshot);
	SCRIPT_REG_GLOBAL(eBodyDamage_PID_Foot);
	SCRIPT_REG_GLOBAL(eBodyDamage_PID_Groin);
	SCRIPT_REG_GLOBAL(eBodyDamage_PID_Knee);
	SCRIPT_REG_GLOBAL(eBodyDamage_PID_WeakSpot);
}

//------------------------------------------------------------------------
CScriptBind_Actor::~CScriptBind_Actor()
{
}


//////////////////////////////////////////////////////////////////////////
int CScriptBind_Actor::PlayerSetViewAngles(IFunctionHandler *pH,Ang3 vAngles)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		pPlayer->SetViewRotation( Quat( vAngles ) );
	}

	return pH->EndFunction();
}


//------------------------------------------------------------------------
void CScriptBind_Actor::AttachTo(IActor *pActor)
{
	IScriptTable *pScriptTable = pActor->GetEntity()->GetScriptTable();

	if (pScriptTable)
	{
		SmartScriptTable thisTable(m_pSS);

		thisTable->SetValue("__this", ScriptHandle(pActor->GetEntityId()));
		thisTable->Delegate(GetMethodsTable());

		pScriptTable->SetValue("actor", thisTable);
	}
}

//------------------------------------------------------------------------
CActor *CScriptBind_Actor::GetActor(IFunctionHandler *pH)
{
	void *pThis = pH->GetThis();

	if (pThis)
	{
		IActor *pActor = m_pGameFW->GetIActorSystem()->GetActor((EntityId)(UINT_PTR)pThis);
		if (pActor)
			return static_cast<CActor *>(pActor);
	}

	return 0;
}


//------------------------------------------------------------------------
int CScriptBind_Actor::DumpActorInfo(IFunctionHandler *pH)
{
  CActor *pActor = GetActor(pH);
  if (!pActor)
    return pH->EndFunction();

  pActor->DumpActorInfo();
  
  return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::Revive(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->Revive(CActor::kRFR_ScriptBind);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::Kill(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->Kill();

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::ShutDown(IFunctionHandler *pH)
{
	CActor* pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->ShutDown();

	return pH->EndFunction();
}

//------------------------------------------------------------------------
//set the actor params, pass the params table to the actor
int CScriptBind_Actor::SetParams(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		SmartScriptTable params;

		if (pH->GetParamType(1) != svtNull && pH->GetParam(1, params))
			pActor->SetParamsFromLua(params);
	}
	
	return pH->EndFunction();
}

// has to be changed! (maybe bone position)
//------------------------------------------------------------------------
int CScriptBind_Actor::GetHeadDir(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	Vec3 headDir = FORWARD_DIRECTION;

	if (IMovementController * pMC = pActor->GetMovementController())
	{
		SMovementState ms;
		pMC->GetMovementState( ms );
		headDir = ms.eyeDirection;
	}

	return pH->EndFunction(Script::SetCachedVector( headDir, pH, 1 ));
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetAimDir(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	Vec3 aimDir = FORWARD_DIRECTION;

	if (IMovementController * pMC = pActor->GetMovementController())
	{
		SMovementState ms;
		pMC->GetMovementState( ms );
		aimDir = ms.aimDirection;
	}

	return pH->EndFunction(Script::SetCachedVector( aimDir, pH, 1 ));
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetChannel(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction( (int)pActor->GetChannelId() );
}

//------------------------------------------------------------------------
int CScriptBind_Actor::IsPlayer(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor && pActor->IsPlayer())
		return pH->EndFunction(1);
	else
		return pH->EndFunction();
} 

//------------------------------------------------------------------------
int CScriptBind_Actor::IsLocalClient(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor && pActor->IsClient())
		return pH->EndFunction(1);
	else
		return pH->EndFunction();
} 

//------------------------------------------------------------------------
int CScriptBind_Actor::PostPhysicalize(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->PostPhysicalize();

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetLinkedVehicleId(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		ScriptHandle vehicleHandle;

		if (IVehicle* pVehicle = pActor->GetLinkedVehicle())
		{
			vehicleHandle.n = pVehicle->GetEntityId();
			return pH->EndFunction(vehicleHandle);
		}
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::LinkToEntity(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		IEntity *pEntity(0);
		ScriptHandle entityId;

		entityId.n = 0;
		if (pH->GetParamType(1) != svtNull)
			pH->GetParam(1, entityId);

		pActor->LinkToEntity((EntityId)entityId.n);
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
//TOFIX:rendundant with CScriptBind_Entity::SetAngles
int CScriptBind_Actor::SetAngles(IFunctionHandler *pH,Ang3 vAngles)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetAngles(vAngles);

	return pH->EndFunction();
}

int CScriptBind_Actor::GetAngles(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	Ang3 angles(0,0,0);

	if (pActor)
		angles = pActor->GetAngles();

	return pH->EndFunction( Script::SetCachedVector( (Vec3)angles, pH, 1 ) );
}

int CScriptBind_Actor::SetMovementTarget(IFunctionHandler *pH, Vec3 pos, Vec3 target, Vec3 up, float speed )
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetMovementTarget(pos,target,up,speed);
		
	return pH->EndFunction();
}

int CScriptBind_Actor::CameraShake(IFunctionHandler *pH,float amount,float duration,float frequency,Vec3 pos)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

  const char* source = "";
  if (pH->GetParamType(5) != svtNull)
    pH->GetParam(5, source);
    
	pActor->CameraShake(amount,0,duration,frequency,pos,0,source);
		
	return pH->EndFunction();
}

int CScriptBind_Actor::SetViewShake(IFunctionHandler *pH, Ang3 shakeAngle, Vec3 shakeShift, float duration, float frequency, float randomness)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	EntityId actorId = pActor->GetEntityId();
	IView* pView = m_pGameFW->GetIViewSystem()->GetViewByEntityId(actorId);
	if (pView)
	{
		const int SCRIPT_SHAKE_ID = 42;
		pView->SetViewShake(shakeAngle, shakeShift, duration, frequency, randomness, SCRIPT_SHAKE_ID);
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetExtensionParams(IFunctionHandler* pH, const char *extension, SmartScriptTable params)
{
	CActor * pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	bool ok = false;
	if (pActor)
		ok = pActor->GetGameObject()->SetExtensionParams(extension, params);
	if (!ok)
		pH->GetIScriptSystem()->RaiseError("Failed to set params for extension %s", extension);
	return pH->EndFunction();
}

//------------------------------------------------------------------------
//If refillCurrentGrenadeType is true then give grenadeCount of the users inventory grenade type rather than frag grenades (If false defaults to frags)
int CScriptBind_Actor::SvRefillAllAmmo(IFunctionHandler* pH, const char* refillType, bool refillAll, int grenadeCount, bool refillCurrentGrenadeType)
{
	CRY_ASSERT(gEnv->bServer);

	if (!gEnv->bServer)
		return pH->EndFunction(0);

	CActor * pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction(0);

	IInventory *pInventory=pActor->GetInventory();
	if (!pInventory)
		return pH->EndFunction(0);

	bool ammoCollected = false;

	IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
	CRY_ASSERT(pItemSystem);

	//1.- First, Refill ammo for all weapons we have, except explosives/grenades
	if(refillAll || (refillType && strlen(refillType) > 0))
	{
		const int itemCount = pInventory->GetCount();
		for (int i = 0; i < itemCount; ++i)
		{
			IItem* pItem = pItemSystem->GetItem(pInventory->GetItem(i));

			CWeapon* pWeapon = pItem ? static_cast<CWeapon*>(pItem->GetIWeapon()) : NULL;
			if (pWeapon && pWeapon->CanPickUpAutomatically() && !IsGrenadeClass(pWeapon->GetEntity()->GetClass()))
				ammoCollected |= pWeapon->RefillAllAmmo(refillType, refillAll);
		}
	}

	//2. Next, handle explosives/grenades
	if (grenadeCount > 0)
	{
		if(refillCurrentGrenadeType)
		{
			const int grenadeCategories = eICT_Explosive|eICT_Grenade;
			const int numItems = pInventory->GetCount();

			for(int i = 0; i < numItems; i++)
			{
				EntityId itemId = pInventory->GetItem(i);

				IItem* pItem = pItemSystem->GetItem(itemId);

				if(pItem)
				{
					IEntityClass* pItemClass = pItem->GetEntity()->GetClass();
					const char* category = pItemSystem->GetItemCategory(pItemClass->GetName());
					const int categoryType = GetItemCategoryType(category);	

					if(categoryType & grenadeCategories)
					{
						ammoCollected |= RefillOrGiveGrenades(*pActor, *pInventory, pItemClass, grenadeCount);
						break;
					}
				}
			}
		}
		else
		{
			ammoCollected |= RefillOrGiveGrenades(*pActor, *pInventory, CItem::sFragHandGrenadesClass, grenadeCount);
		}
	}

	return pH->EndFunction(ammoCollected ? 1 : 0);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::ClRefillAmmoResult(IFunctionHandler* pH, bool ammoRefilled)
{
	CActor * pActor = GetActor(pH);
	if (pActor)
	{
		if (ammoRefilled)
		{
			CAudioSignalPlayer::JustPlay("Player_GrabAmmo", pActor->GetEntityId());
			if (pActor->GetActorClass() == CPlayer::GetActorClassType())
			{
				static_cast<CPlayer*>(pActor)->RefillAmmo();
			}
		}
		else
		{
			CAudioSignalPlayer::JustPlay("Player_AmmoFull", pActor->GetEntityId());
			SHUDEvent event(eHUDEvent_OnAmmoPickUp);
			event.AddData(SHUDEventData((void*)0));
			event.AddData(SHUDEventData((int)0));
			event.AddData(SHUDEventData((void*)0));
			CHUDEventDispatcher::CallEvent(event);
		}
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetHealth(IFunctionHandler *pH, float health)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetHealth(health);

	return pH->EndFunction(pActor->GetHealth());
}
//------------------------------------------------------------------------
int CScriptBind_Actor::DamageInfo(IFunctionHandler *pH, ScriptHandle shooter, ScriptHandle target, ScriptHandle weapon, ScriptHandle projectile, float damage, int damageType, Vec3 hitDirection)
{
	EntityId shooterID = (EntityId)shooter.n;
	EntityId targetID = (EntityId)target.n;
	EntityId weaponID = (EntityId)weapon.n;
	EntityId projectileID = (EntityId)projectile.n;
	CProjectile *pProjectile = g_pGame->GetWeaponSystem()->GetProjectile(projectileID);
	IEntityClass *pProjectileClass = (pProjectile ? pProjectile->GetEntity()->GetClass() : 0);
	CActor *pActor = GetActor(pH);
	if (pActor)
	{
		pActor->DamageInfo(shooterID, weaponID, pProjectileClass, damage, damageType, hitDirection);
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::GetLowHealthThreshold(IFunctionHandler *pH)
{
	return pH->EndFunction(g_pGameCVars->g_playerLowHealthThreshold);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetMaxHealth(IFunctionHandler *pH, float health)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetMaxHealth(health);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetHealth(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetHealth());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::IsImmuneToForbiddenArea(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
	{
		return pH->EndFunction();
	}

	return pH->EndFunction(pActor->ImmuneToForbiddenZone());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetMaxHealth(IFunctionHandler *pH)
{
  CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetMaxHealth());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::QueueAnimationState(IFunctionHandler *pH, const char *animationState)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->QueueAnimationState(animationState);
	
	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::CreateCodeEvent(IFunctionHandler *pH,SmartScriptTable params)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return (pActor->CreateCodeEvent(params));
}

//------------------------------------------------------------------------
int CScriptBind_Actor::PauseAnimationGraph(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		pActor->GetAnimationGraphState()->Pause(true, eAGP_PlayAnimationNode);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::ResumeAnimationGraph(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		pActor->GetAnimationGraphState()->Pause(false, eAGP_PlayAnimationNode);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::HurryAnimationGraph(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		pActor->GetAnimationGraphState()->Hurry();

	return pH->EndFunction();
}


// ===========================================================================
//  Set turn animation parameters.
//
//  In:     The function handler.
//  In:     Threshold angle, in degrees, that is needed before turning is 
//          even considered (>= 0.0f will not modify).
//  In:     The current angle deviation needs to be over the turnThresholdAngle 
//          for longer than this time before the character turns (>= 0.0f will 
//          not modify).
//
//	Returns:	A default exit code (in Lua: void).
//
int CScriptBind_Actor::SetTurnAnimationParams(
	IFunctionHandler* pH, const float turnThresholdAngle, const float turnThresholdTime)
{
	bool result = false;

	if ( (turnThresholdAngle < 0.0f) || (turnThresholdTime < 0.0f) )
	{
		CryLog("SetTurnAnimationParams(): Invalid parameter(s)!");
		assert(false);
	}
	else
	{
		CActor* actor = GetActor(pH);
		if (actor != NULL)
		{
			actor->SetTurnAnimationParams(turnThresholdAngle, turnThresholdTime);
		}
		else
		{
			CryLog("SetTurnAnimationParams(): Unable to obtain actor!");
		}
	}

	return pH->EndFunction();
}


//------------------------------------------------------------------------
int CScriptBind_Actor::SetSpectatorMode(IFunctionHandler *pH, int mode, ScriptHandle targetId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetSpectatorModeAndOtherEntId(mode, (EntityId)targetId.n);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetSpectatorMode(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	return pH->EndFunction(pActor->GetSpectatorMode());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetSpectatorState(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	return pH->EndFunction(pActor->GetSpectatorState());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetSpectatorTarget(IFunctionHandler* pH)
{
	CActor* pActor = GetActor(pH);
	if(!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetSpectatorTarget());
}

//------------------------------------------------------------------------
int CScriptBind_Actor::Fall(IFunctionHandler *pH, Vec3 hitPos)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	// [Mikko] 11.10.2007 - Moved the check here, since it was causing too much trouble in CActor.Fall().
	// The point of this filtering is to mostly mask out self-induced collision damage on friendly NPCs
	// which are playing special animations.

	bool bForce = false;
	if (pH->GetParamType(2) != svtNull)
		pH->GetParam(2, bForce);

	if(!g_pGameCVars->g_enableFriendlyFallAndPlay && !bForce)
	{
		if (const IAnimatedCharacter* pAC = pActor->GetAnimatedCharacter())
		{
			if ((pAC->GetPhysicalColliderMode() == eColliderMode_NonPushable) ||
				(pAC->GetPhysicalColliderMode() == eColliderMode_PushesPlayersOnly))
			{
				// Only mask for player friendly NPCs.
				if (pActor->GetEntity() && pActor->GetEntity()->GetAI())
				{
					IAIObject* pAI = pActor->GetEntity()->GetAI();
					IAIObject* playerAI = 0;
					if (IActor* pPlayerActor = m_pGameFW->GetClientActor())
						playerAI = pPlayerActor->GetEntity()->GetAI();

					if (playerAI && gEnv->pAISystem
						&& gEnv->pAISystem->GetFactionMap().GetReaction(playerAI->GetFactionID(), pAI->GetFactionID()) > IFactionMap::Hostile)
						return pH->EndFunction();
				}
			}
		}
	}

	if( static_cast<CPlayer*> (pActor)->CanFall() )
	{
		pActor->Fall(hitPos);
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetExtraHitLocationInfo(IFunctionHandler *pH, int slot, int partId)
{
	CActor* pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	IEntity* pEntity = pActor->GetEntity();
	if (!pEntity)
		return pH->EndFunction();

	ICharacterInstance* pCharacter = pActor->GetEntity()->GetCharacter(slot);
	if (!pCharacter)
		return pH->EndFunction();

	IPhysicalEntity* pPE = pCharacter->GetISkeletonPose()->GetCharacterPhysics();
	if (!pPE)
		return pH->EndFunction();

	pe_status_pos posStatus;
	pe_params_part part;
	part.partid = partId;

	if (!pPE->GetParams(&part) || !pPE->GetStatus(&posStatus))
		return pH->EndFunction();

	SmartScriptTable result = Script::GetCachedTable(pH, 3);

	Matrix34 worldTM = Matrix34::Create(Vec3(posStatus.scale, posStatus.scale, posStatus.scale), posStatus.q, posStatus.pos);
	Matrix34 partWorldTM = worldTM * Matrix34::Create(Vec3(part.scale, part.scale, part.scale), part.q, part.pos);

	result->SetValue("partId", partId);
	result->SetValue("ipart", part.ipart);
	result->SetValue("mass", part.mass);
	result->SetValue("scale", part.scale);
	result->SetValue("pos", partWorldTM.GetTranslation());
	result->SetValue("dir", partWorldTM.GetColumn1());

	IDefaultSkeleton& rIDefaultSkeleton = pCharacter->GetIDefaultSkeleton();
	ISkeletonPose* pSkeletonPose = pCharacter->GetISkeletonPose();
	if (!pSkeletonPose)
		return pH->EndFunction();

	result->SetToNull("attachmentName");
	result->SetToNull("boneName");

	// 1000 is a magic number in CryAnimation
	// see CAttachmentManager::PhysicalizeAttachment
	const int firstAttachmentPartId = 1000;
	
	if (partId >= firstAttachmentPartId)
	{
		IAttachmentManager *pAttchmentManager = pCharacter->GetIAttachmentManager();
		if (IAttachment* pAttachment = pAttchmentManager->GetInterfaceByIndex(partId-firstAttachmentPartId))
			result->SetValue("attachmentName", pAttachment->GetName());
	}
	else
	{
		if (const char* boneName = rIDefaultSkeleton.GetJointNameByID(pSkeletonPose->getBonePhysParentOrSelfIndex(partId)))
			result->SetValue("boneName", boneName);
	}

	return pH->EndFunction(result);
}


//------------------------------------------------------------------------
int CScriptBind_Actor::StandUp(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->StandUp();

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetForcedLookDir(IFunctionHandler *pH, CScriptVector dir)
{
	CPlayer* pPlayer = static_cast<CPlayer*>(GetActor(pH));
	if (pPlayer)
	{
		pPlayer->SetForcedLookDir(dir.Get());
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::ClearForcedLookDir(IFunctionHandler *pH)
{
	CPlayer* pPlayer = static_cast<CPlayer*>(GetActor(pH));
	if (pPlayer)
	{
		pPlayer->ClearForcedLookDir();
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetForcedLookObjectId(IFunctionHandler *pH, ScriptHandle objectId)
{
	CPlayer* pPlayer = static_cast<CPlayer*>(GetActor(pH));
	if (pPlayer)
	{
		pPlayer->SetForcedLookObjectId((EntityId)objectId.n);
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::ClearForcedLookObjectId(IFunctionHandler *pH)
{
	CPlayer* pPlayer = static_cast<CPlayer*>(GetActor(pH));
	if (pPlayer)
	{
		pPlayer->ClearForcedLookObjectId();
	}

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetPhysicalizationProfile(IFunctionHandler *pH, const char *profile)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	
	uint32 p = 0;
	if (!stricmp(profile, "alive"))
		p = eAP_Alive;
	else if (!stricmp(profile, "unragdoll"))
		p = eAP_NotPhysicalized;
	else if (!stricmp(profile, "ragdoll"))
	{
		if (!pActor->GetLinkedVehicle())
			p = eAP_Ragdoll;
		else
			p = eAP_Alive;
	}
	else if (!stricmp(profile, "spectator"))
		p = eAP_Spectator;
	else if (!stricmp(profile, "sleep"))
		p = eAP_Sleep;
	else
		return pH->EndFunction();

	//Don't turn ragdoll while grabbed
	if(p==eAP_Ragdoll && !pActor->CanRagDollize())
		return pH->EndFunction();

	pActor->GetGameObject()->SetAspectProfile(eEA_Physics, p);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetPhysicalizationProfile(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	uint8 profile=pActor->GetGameObject()->GetAspectProfile(eEA_Physics);
	const char *profileName;
	if (profile == eAP_Alive)
		profileName="alive";
	else if (profile == eAP_NotPhysicalized)
		profileName = "unragdoll";
	else if (profile == eAP_Ragdoll)
		profileName = "ragdoll";
	else if (profile == eAP_Sleep)
		profileName = "sleep";
	else if (profile == eAP_Spectator)
		profileName = "spectator";
	else
		return pH->EndFunction();

	return pH->EndFunction(profileName);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::DisableHitReaction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
	{
		pActor->DisableHitReactions();
	}	
	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::EnableHitReaction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
	{
		pActor->EnableHitReactions();
	}	
	return pH->EndFunction();
}
//------------------------------------------------------------------------
int CScriptBind_Actor::SetMaxStamina(IFunctionHandler *pH, float stamina)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetMaxStamina((float)stamina);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SetMaxMagicka(IFunctionHandler *pH, float magicka)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetMaxMagicka((float)magicka);

	return pH->EndFunction();
}

int CScriptBind_Actor::GetStrength(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		return pH->EndFunction(pActor->GetStrength());

	return pH->EndFunction();
}

//------------------------------------------------------------------------

//------------------------------------------------------------------------
int CScriptBind_Actor::GetArmor(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetArmor());

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetMaxArmor(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetMaxArmor());

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetStamina(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetStamina());

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetMaxStamina(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetMaxStamina());

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::GetGender(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetGender());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetLevel(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetLevel());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetMagicka(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetMagicka());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetMaxMagicka(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		return pH->EndFunction(pActor->GetMaxMagicka());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetGold(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		return pH->EndFunction(pActor->GetGold());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetAgility(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		return pH->EndFunction(pActor->GetAgility());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetWillpower(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		return pH->EndFunction(pActor->GetWillpower());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetEndurance(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		return pH->EndFunction(pActor->GetEndurance());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetPersonality(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		return pH->EndFunction(pActor->GetPersonality());

	return pH->EndFunction();
}

int CScriptBind_Actor::GetIntelligence(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (pActor)
		return pH->EndFunction(pActor->GetIntelligence());

	return pH->EndFunction();
}


int CScriptBind_Actor::SetStamina(IFunctionHandler *pH, float stamina)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetStamina((float)stamina);

	return pH->EndFunction();
}

int CScriptBind_Actor::AddLVLxp(IFunctionHandler *pH, float xp)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->AddXperiencePoints((float)xp);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetMagicka(IFunctionHandler *pH, float magicka)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetMagicka((float)magicka);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetStrength(IFunctionHandler *pH, float strength)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		pActor->SetStrength(strength);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetAgility(IFunctionHandler *pH, float agility)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		pActor->SetAgility((int)agility);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetWillpower(IFunctionHandler *pH, float willpower)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetWillpower((int)willpower);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetEndurance(IFunctionHandler *pH, float endurance)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetEndurance((int)endurance);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetPersonality(IFunctionHandler *pH, float personality)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetPersonality((int)personality);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetIntelligence(IFunctionHandler *pH, float intelligence)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetIntelligence((int)intelligence);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetLevel(IFunctionHandler *pH, float level)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetLevel((int)level);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetGender(IFunctionHandler *pH, float gender)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetGender((int)gender);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetGold(IFunctionHandler *pH, float gold)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetGold((int)gold);

	return pH->EndFunction();
}

int CScriptBind_Actor::SetArmor(IFunctionHandler *pH, float armor)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->SetArmor((int)armor);

	return pH->EndFunction();
}

int CScriptBind_Actor::Add_additional_equip(IFunctionHandler *pH)
{
	//CryLogAlways("CScriptBind_Actor::Add_additional_equip");
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		pActor->AddAdditionalEquip();

		/*if (gEnv->bMultiplayer)
		{
			if (gEnv->bServer)
				pActor->GetGameObject()->InvokeRMI(CActor::ClAddAdditionalEquip(), CActor::NoParams(), eRMI_ToRemoteClients);
			else
			{
				pActor->GetGameObject()->InvokeRMI(CActor::SvAddAdditionalEquip(), CActor::NoParams(), eRMI_ToServer);
			}
		}*/
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::Add_additional_equip2(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		pActor->AddAdditionalEquip2();

		/*if (gEnv->bMultiplayer)
		{
			if (gEnv->bServer)
				pActor->GetGameObject()->InvokeRMI(CActor::ClAddAdditionalEquip2(), CActor::NoParams(), eRMI_ToRemoteClients);
			else
			{
				pActor->GetGameObject()->InvokeRMI(CActor::SvAddAdditionalEquip2(), CActor::NoParams(), eRMI_ToServer);
			}
		}*/
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::StartMeleeAttack(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StartMeleeWeaponAttack(pActor->GetEntityId());

	return pH->EndFunction();
}

int CScriptBind_Actor::StartMeleeAttackComplexAction(IFunctionHandler *pH, float time, float delay, float damage, const char *actionName)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	//CryLogAlways("try to start StartMeleeAttackComplexAction from lua");
	//CryLogAlways(actionName);

	pActorActionsNew->StartWeaponMeleeAttackComplex(pActor->GetEntityId(), time, delay, damage, actionName);

	return pH->EndFunction();
}

int CScriptBind_Actor::StartBlockAction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	if (!pActor->m_blockactiveshield && !pActor->m_blockactivenoshield)
		pActorActionsNew->StartMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());

	return pH->EndFunction();
}

int CScriptBind_Actor::StopBlockAction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	
	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	if (pActor->m_blockactiveshield || pActor->m_blockactivenoshield)
		pActorActionsNew->StopMeleeBlockAction(pActor->GetEntityId(), pActor->GetCurrentItemId());

	return pH->EndFunction();
}

int CScriptBind_Actor::StartSimpleAttackAction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StartWeaponAttackSimple(pActor->GetEntityId());

	return pH->EndFunction();
}

int CScriptBind_Actor::StopSimpleAttackAction(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StopWeaponAttackSimple(pActor->GetEntityId());

	return pH->EndFunction();
}

int CScriptBind_Actor::RequestSwitchToWeaponInInventory(IFunctionHandler *pH, const char *itemName)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->SwitchToWeaponInInventory(pActor->GetEntityId(), itemName);

	return pH->EndFunction();
}

int CScriptBind_Actor::RequestEquipItemFromInventory(IFunctionHandler *pH, const char *itemName)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->EquipItemFromInventory(pActor->GetEntityId(), itemName);

	return pH->EndFunction();
}

int CScriptBind_Actor::GetCurrentSelectedWeaponType(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		if (pActor->GetCurrentItem())
		{
			CItem *pItem = static_cast<CItem*>(pActor->GetCurrentItem());
			if (pItem)
			{
				return pH->EndFunction(pItem->GetWeaponType());
			}
		}
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::RequestStartWpnThrowing(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StartPreLaunchWeapon(pActor->GetEntityId(), pActor->GetCurrentItemId());

	return pH->EndFunction();
}

int CScriptBind_Actor::RequestToDoWpnThrowing(IFunctionHandler *pH, float force)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StopPreLaunchWeaponAndLaunchThis(pActor->GetEntityId(), pActor->GetCurrentItemId(), force);

	return pH->EndFunction();
}

int CScriptBind_Actor::SwitchToAnyMeleeWeapon(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->SwitchToAnyMeleeWeapon(pActor->GetEntityId()));
}

int CScriptBind_Actor::SwitchToAnyWeapon(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->SwitchToAnyWeapon(pActor->GetEntityId()));
}

int CScriptBind_Actor::SwitchToAnyRangeWeapon(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->SwitchToAnyRangeWeapon(pActor->GetEntityId()));
}

int CScriptBind_Actor::SwitchToNoWpn(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->SwitchToNoWpn(pActor->GetEntityId()));
}

int CScriptBind_Actor::CheckCurrentWeaponAmmo(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->CheckCurrentWeaponAmmo(pActor->GetEntityId()));
}

int CScriptBind_Actor::IsCurrentSelectedWeaponMeleeSupport(IFunctionHandler * pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	bool wpn_melee = false;
	if (pActorActionsNew->CheckCurrentWpnType(pActor->GetEntityId()) < 9 && pActorActionsNew->CheckCurrentWpnType(pActor->GetEntityId()) > 0)
		wpn_melee = true;

	return pH->EndFunction(wpn_melee);
}

int CScriptBind_Actor::IsAITargetHaveAndVisibleOrObstr(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	int out_value = pActorActionsNew->IsAITargetHaveAndVisibleOrObstr(pActor->GetEntityId());
	//CryLogAlways("CScriptBind_Actor::IsAITargetHaveAndVisibleOrObstr called, out value = %i", out_value);
	return pH->EndFunction(out_value);
}

int CScriptBind_Actor::GetAITargetFT(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	return pH->EndFunction(pActorActionsNew->GetAITargetFT(pActor->GetEntityId()));
}

int CScriptBind_Actor::GetEquippedBowAmmo(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		EntityId idc = pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows);
		return pH->EndFunction(idc > 0);
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::ResetVars(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		pActor->ResetActorVars();
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::DeequipAllEquippedItems(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		pActor->RequestToDeequipAllItemsFromEquipPacks();
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::FullReloadActorModel(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		pActor->RequestToReloadActorModelOrReinitAttachments(true);
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::GetBuffEffectActive(IFunctionHandler *pH, int id)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		return pH->EndFunction(pActor->GetBuffActive(id));
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::GetActorResistForDamageType(IFunctionHandler *pH, int id)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
	{
		return pH->EndFunction(pActor->GetResistForDamageType(id));
	}

	return pH->EndFunction();
}

int CScriptBind_Actor::AddUsableSpell(IFunctionHandler *pH, int id, bool fromXml)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddSpellToActorSB(id, fromXml);
	return pH->EndFunction();
}

int CScriptBind_Actor::AddSpellToCastSlot(IFunctionHandler *pH, int id, int slot)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddSpellToCurrentSpellsForUseSlot(id, slot);
	return pH->EndFunction();
}

int CScriptBind_Actor::IsSpellInUsableSpells(IFunctionHandler *pH, int id)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->IsSpellInActorSB(id));
}

int CScriptBind_Actor::CanCastSpellFromSlot(IFunctionHandler * pH, int slot)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction(false);

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction(false);

	return pH->EndFunction(pActorActionsNew->CanCastSpell(pActor->GetEntityId(), slot));
}

int CScriptBind_Actor::GetLeftHandItemId(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetAnyItemInLeftHand());
}

int CScriptBind_Actor::SetNumberOfAbleInventorySlots(IFunctionHandler *pH, int slotsNum)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetMaxInvSlots(slotsNum);
	return pH->EndFunction();
}

int CScriptBind_Actor::SetActorRelaxedState(IFunctionHandler *pH, bool relaxed)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetRelaxedMod(relaxed);
	return pH->EndFunction();
}

int CScriptBind_Actor::SetActorStealthState(IFunctionHandler *pH, bool stealth)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetStealthMod(stealth);
	return pH->EndFunction();
}

int CScriptBind_Actor::ActorJumpRequest(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor->ActCanJump() && pActor->GetMovementController())
	{
		CMovementRequest mr;
		mr.SetJump();
		pActor->ActJump();
		pActor->GetMovementController()->RequestMovement(mr);
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::ActivateBuEffectOnActor(IFunctionHandler *pH, int id, float time, int effId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddBuff(id, time, effId);
	return pH->EndFunction();
}

int CScriptBind_Actor::ActivateSpecPassivePerkOnActor(IFunctionHandler *pH, int id, bool active, int level)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->ActivatePerk(id, active, level);
	return pH->EndFunction();
}

int CScriptBind_Actor::TryToEquipUnSelectableItem(IFunctionHandler *pH, EntityId itemId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->EquipItem(itemId);
	return pH->EndFunction();
}

int CScriptBind_Actor::GetCurrentEquippedSlotItemId(IFunctionHandler *pH, int slotId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetCurrentEquippedItemId(slotId));
}

int CScriptBind_Actor::CreateParticleEmitterOnActor(IFunctionHandler *pH, int characterSlot, const char *attachmentName, const char *boneName, const char *particleEffectName, bool setDefPose)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	//CryLogAlways("CScriptBind_Actor::CreateParticleEmitterOnActor called, used bone - %s , used efect - %s", boneName, particleEffectName);
	pActor->CreateParticleAttachment(characterSlot, attachmentName, boneName, particleEffectName, setDefPose);
	return pH->EndFunction();
}

int CScriptBind_Actor::CreateSimpleLightOnActor(IFunctionHandler *pH, const char *attachmentName, const char *boneName, bool setDefPose, float colR, float colG, float colB, float colA, float rad, float intense, bool shadows)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	ColorF col_lgt(colR, colG, colB, colA);
	pActor->CreateLightAttachment(0, attachmentName, boneName, setDefPose, col_lgt, rad, intense, shadows);
	return pH->EndFunction();
}

int CScriptBind_Actor::DeleteParticleEmitterOrLigthFromActor(IFunctionHandler *pH, int characterSlot, const char *attachmentName, bool clearOnly)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->DeleteParticleAttachment(characterSlot, attachmentName, clearOnly);
	return pH->EndFunction();
}

int CScriptBind_Actor::SimulateDamageToActor(IFunctionHandler *pH, float damage, int hitType)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CGameRules* pGameRules = g_pGame->GetGameRules();
	if (pGameRules)
	{
		HitInfo hitInfo(pActor->GetEntityId(), pActor->GetEntityId(), 0,
			damage, 0.0f, 0, -1,
			hitType, Vec3(ZERO), Vec3(ZERO), Vec3(ZERO));
		pGameRules->ClientHit(hitInfo);
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::GetBuffEffectTime(IFunctionHandler *pH, int id)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	return pH->EndFunction(pActor->GetBuffTime(id));
}

int CScriptBind_Actor::AddBuffEffectTime(IFunctionHandler *pH, int id, float time)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddBuffTime(id, time);
	return pH->EndFunction();
}

int CScriptBind_Actor::PlayAnimAction(IFunctionHandler *pH, const char *actionName)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->PlaySimpleAnimAction(pActor->GetEntityId(), actionName);
	return pH->EndFunction();
}

int CScriptBind_Actor::CreateBloodLooseEffect(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->CreateBloodLooseEffect();
	return pH->EndFunction();
}

int CScriptBind_Actor::ClearAllBloodLooseEffects(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->ClearBloodLooseEffects();
	return pH->EndFunction();
}

int CScriptBind_Actor::SetActorResistForDamageType(IFunctionHandler *pH, int id, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetResistForDamageType(id, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::GetActorResistForDamageTypeByName(IFunctionHandler *pH, const char *name)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();
	return pH->EndFunction(pActor->GetResistForDamageTypeByName(name));
}

int CScriptBind_Actor::SetActorResistForDamageTypeByName(IFunctionHandler *pH, const char *name, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SetResistForDamageTypeByName(name, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::AddActorDamageResistValueByResName(IFunctionHandler *pH, const char *name, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddResistForDamageTypeByName(name, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::DelActorDamageResistValueByResName(IFunctionHandler *pH, const char *name, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->DelResistForDamageTypeByName(name, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::AddActorDamageResistValueByResId(IFunctionHandler *pH, int id, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->AddResistForDamageType(id, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::DelActorDamageResistValueByResId(IFunctionHandler *pH, int id, float rsvalue)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->DelResistForDamageType(id, rsvalue);
	return pH->EndFunction();
}

int CScriptBind_Actor::ActorStartCastSpellSimple(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StartSpellCastSimple(pActor->GetEntityId());
	return pH->EndFunction();
}

int CScriptBind_Actor::ActorStopCastSpellSimple(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->StopSpellCastSimple(pActor->GetEntityId());
	return pH->EndFunction();
}

int CScriptBind_Actor::ActorSelectAnySpellSimple(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
	if (!pActorActionsNew)
		return pH->EndFunction();

	pActorActionsNew->SelectAnySpellFromSbToSlotSimple(pActor->GetEntityId());
	return pH->EndFunction();
}

int CScriptBind_Actor::GetActorHealthPercents(IFunctionHandler * pH)
{
	return 0;
}

//------------------------------------------------------------------------
int CScriptBind_Actor::CreateIKLimb( IFunctionHandler *pH, int slot, const char *limbName, const char *rootBone, const char *midBone, const char *endBone, int flags)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	if (pActor)
		pActor->CreateIKLimb(SActorIKLimbInfo(slot,limbName,rootBone,midBone,endBone,flags));

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::HolsterItem(IFunctionHandler *pH, bool holster)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->HolsterItem(holster);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::DropItem(IFunctionHandler *pH, ScriptHandle itemId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	float impulse=1.0f;
	bool bydeath=false;

	if (pH->GetParamCount()>1 && pH->GetParamType(2)==svtNumber)
		pH->GetParam(2, impulse);

	if (pH->GetParamCount()>2 && pH->GetParamType(3)==svtNumber||pH->GetParamType(2)==svtBool)
		pH->GetParam(3, bydeath);

	pActor->DropItem((EntityId)itemId.n, impulse, true, bydeath);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::PickUpItem(IFunctionHandler *pH, ScriptHandle itemId)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	bool select=true;
	if (pH->GetParamCount() > 1)
	{
		pH->GetParam(2, select);
	}

	pActor->PickUpItem((EntityId)itemId.n, true, select);

	return pH->EndFunction();
}

int CScriptBind_Actor::IsCurrentItemHeavy( IFunctionHandler* pH )
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	bool result = static_cast<CPlayer*> (pActor)->HasHeavyWeaponEquipped();

	return pH->EndFunction( result );
}


//------------------------------------------------------------------------
int CScriptBind_Actor::PickUpPickableAmmo(IFunctionHandler *pH, const char *ammoName, int count)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	IInventory *pInventory = pActor->GetInventory();
	if (!pInventory)
		return pH->EndFunction();

	IEntityClass* pAmmoClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(ammoName);
	if (!pAmmoClass)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR, "Ammo class %s not found!", ammoName);
		return pH->EndFunction();
	}

	int currAmmo = pInventory->GetAmmoCount( pAmmoClass );
	int finalAmmo = currAmmo + count;
	pInventory->SetAmmoCount( pAmmoClass, finalAmmo );
	pInventory->RMIReqToServer_SetAmmoCount( pAmmoClass->GetName(), finalAmmo );
	if (pActor->IsPlayer())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		pPlayer->OnPickedUpPickableAmmo( pAmmoClass, count );
	}

	return pH->EndFunction();
}


//------------------------------------------------------------------------
int CScriptBind_Actor::SelectLastItem(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SelectLastItem(true);

	return pH->EndFunction();
}


//------------------------------------------------------------------------
int CScriptBind_Actor::SelectItemByName(IFunctionHandler *pH, const char *name)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	bool forceFastSelect = false;
	if (pH->GetParamType(2) == svtBool)
		pH->GetParam(2, forceFastSelect);

	pActor->SelectItemByName(name, true, forceFastSelect);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SelectItem(IFunctionHandler *pH, ScriptHandle itemId, bool forceSelect)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->SelectItem((EntityId)itemId.n, true, forceSelect);

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SelectNextItem(IFunctionHandler *pH, int direction, bool keepHistory, const char *category)
{
	CActor *pActor = GetActor(pH);
	bool found = pActor && (pActor->GetInventory()->GetCountOfCategory(category) > 0);
	if (found)
	{
		int categoryType = GetItemCategoryType(category);
		pActor->SelectNextItem(direction, keepHistory, categoryType);
	}

	return pH->EndFunction(found);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::SimpleFindItemIdInCategory(IFunctionHandler *pH, const char *category)
{
	CActor *pActor = GetActor(pH);
	ScriptHandle result;

	if (pActor)
	{
		EntityId foundEntityId = pActor->SimpleFindItemIdInCategory(category);
		result.n=foundEntityId;
	}

	return pH->EndFunction(result);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::PlayAction(IFunctionHandler *pH, const char* action)
{
	CActor *pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction();

	pActor->PlayAction(action, "");

	return pH->EndFunction();
}

//------------------------------------------------------------------------
int CScriptBind_Actor::CanSpectacularKillOn(IFunctionHandler *pH, ScriptHandle targetId)
{
	bool bResult = false;

	CActor* pActor = GetActor(pH);
	if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
	{
		CPlayer* pKiller = static_cast<CPlayer*>(pActor);
		bResult = pKiller->GetSpectacularKill().CanSpectacularKillOn((EntityId)targetId.n);
	}

	return pH->EndFunction(bResult);
}

//------------------------------------------------------------------------
int CScriptBind_Actor::StartSpectacularKill(IFunctionHandler *pH, ScriptHandle targetId)
{
	bool bResult = false;

	CActor* pActor = GetActor(pH);
	if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
	{
		CPlayer* pKiller = static_cast<CPlayer*>(pActor);
		bResult = pKiller->GetSpectacularKill().StartOnTarget((EntityId)targetId.n);
	}

	return pH->EndFunction(bResult);
}

int CScriptBind_Actor::StartSpectacularKillWId(IFunctionHandler * pH, int targetId)
{
	bool bResult = false;

	CActor* pActor = GetActor(pH);
	if (pActor && (pActor->GetActorClass() == CPlayer::GetActorClassType()))
	{
		CPlayer* pKiller = static_cast<CPlayer*>(pActor);
		bResult = true;
		pKiller->RequestSpectacularKillNv((EntityId)targetId);
	}

	return pH->EndFunction(bResult);
}

bool CScriptBind_Actor::IsGrenadeClass( const IEntityClass* pEntityClass ) const
{
	return pEntityClass == CItem::sFragHandGrenadesClass;
}

//------------------------------------------------------------------------
int CScriptBind_Actor::RegisterInAutoAimManager(IFunctionHandler *pH)
{
	CActor *pActor = GetActor(pH);

	if (pActor)
		pActor->RegisterInAutoAimManager();

	return pH->EndFunction();
}


// ===========================================================================
//	Check certain flags on a body damage part have been set.
//
//	In:		The function handler
//	In:		The part ID (often obtained from hit information).
//	In:		The material ID (often obtained from hit information).
//	In:		The flag masks (must be a combination of any of the 
//			eBodyDamage_PID_xxx values).
//
//	Returns:	A default exit code (in Lua: True if all of the flags in the 
//				mask match, otherwise false).
//
int CScriptBind_Actor::CheckBodyDamagePartFlags(
	IFunctionHandler *pH, int partID, int materialID, uint32 bodyPartFlagsMask)
{
	bool result = false;

	CActor* actor = GetActor(pH);
	if (actor != NULL)
	{
		const uint32 flags = actor->GetBodyDamagePartFlags(partID, materialID);
		result = ((flags & bodyPartFlagsMask) == bodyPartFlagsMask);
	}

	return pH->EndFunction(result);
}


// ===========================================================================
//	Get a body damage profile ID for a combination of body damage file names.
//
//	In:		The function handler
//	In:		The body damage file name (empty string will ignore).
//	In:		The body damage parts file name (empty string will ignore).
//
//	Returns:	A default exit code (in Lua: A body damage profile ID or
//				-1 on error).
//
int CScriptBind_Actor::GetBodyDamageProfileID(
	IFunctionHandler* pH, 
	const char* bodyDamageFileName, const char* bodyDamagePartsFileName)
{
	CActor* actor = GetActor(pH);
	if (actor != NULL)
	{
		const TBodyDamageProfileId profileID = actor->GetBodyDamageProfileID(
			bodyDamageFileName, bodyDamagePartsFileName);
		if (profileID == INVALID_BODYDAMAGEPROFILEID)
		{
			CryLog("GetBodyDamageProfileID(): Unable to obtain profile ID for '%s' and '%s'.",
				bodyDamageFileName, bodyDamagePartsFileName);
		}
		return pH->EndFunction(profileID);
	}

	return pH->EndFunction();
}


// ===========================================================================
//	Override a damage profile ID.
//
//	In:		The function handler
//	In:		The damage profile ID (INVALID_BODYDAMAGEPROFILEID will revert to
//			the default). Also see LookUpBodyDamageProfileID().
//
//	Returns:	A default exit code (in Lua: void).
//
int CScriptBind_Actor::OverrideBodyDamageProfileID(
	IFunctionHandler* pH, 
	const int bodyDamageProfileID)
{
	CActor* actor = GetActor(pH);
	if (actor != NULL)
	{		
		actor->OverrideBodyDamageProfileID((TBodyDamageProfileId)bodyDamageProfileID);
	}

	return pH->EndFunction();
}


bool CScriptBind_Actor::RefillOrGiveGrenades( CActor& actor, IInventory& inventory, IEntityClass* pGrenadeClass, int grenadeCount )
{
	CRY_ASSERT(pGrenadeClass);

	IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();
	CRY_ASSERT(pItemSystem);
	
	bool grenadesRefilled = false;
	bool hadGrenadesBefore = true;

	IItem* pGrenadeItem = pItemSystem->GetItem(inventory.GetItemByClass(pGrenadeClass));
	if (!pGrenadeItem && pGrenadeClass)
	{
		//We don't have grenades yet, give it to the player (this is SP only anyways, but can be only done in the server)
		if (gEnv->bServer)
		{
			pItemSystem->GiveItem(&actor, pGrenadeClass->GetName(), false, false, false);

			//Try to get the item again...
			pGrenadeItem = pItemSystem->GetItem(inventory.GetItemByClass(pGrenadeClass));
			
			hadGrenadesBefore = false;
		}
	}

	if (pGrenadeItem)
	{
		CWeapon* pGrenadeWeapon = static_cast<CWeapon*>(pGrenadeItem->GetIWeapon());
		if (pGrenadeWeapon)
		{
			const int fireModeCount = pGrenadeWeapon->GetNumOfFireModes();
			for (int j = 0; j < fireModeCount; ++j)
			{
				IFireMode* pFireMode = pGrenadeWeapon->GetFireMode(j);
				CRY_ASSERT(pFireMode);

				IEntityClass* pAmmoTypeClass = pFireMode->GetAmmoType();
				if (pAmmoTypeClass)
				{
					const int ammoTypeCount = hadGrenadesBefore ? inventory.GetAmmoCount(pAmmoTypeClass) : 0;
					const int ammoTypeCapacity = inventory.GetAmmoCapacity(pAmmoTypeClass);

					if (ammoTypeCount < ammoTypeCapacity)
					{
						pGrenadeWeapon->SetInventoryAmmoCount(pAmmoTypeClass, min(ammoTypeCount + grenadeCount, ammoTypeCapacity));
						grenadesRefilled = true;
					}
				}

			}
		}
	}

	return grenadesRefilled;
}


int CScriptBind_Actor::RefreshPickAndThrowObjectPhysics( IFunctionHandler *pH )
{
	CActor* pActor = GetActor(pH);
	if(pActor && pActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		static IEntityClass* pPickAndThrowWeaponClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass("PickAndThrowWeapon");
		IItem* pItem																  = pPlayer->GetCurrentItem(false); 
		if(pItem)
		{
			if (pItem->GetEntity()->GetClass() == pPickAndThrowWeaponClass)
			{
				CPickAndThrowWeapon* pPickAndThrowWeapon = static_cast<CPickAndThrowWeapon*>(pItem);
				pPickAndThrowWeapon->OnObjectPhysicsPropertiesChanged();
			}
		}
	}
	return pH->EndFunction();
}

int CScriptBind_Actor::SvGiveAmmoClips( IFunctionHandler* pH, int numClips )
{
	CRY_ASSERT(gEnv->bServer);

	if (!gEnv->bServer)
		return pH->EndFunction(0);

	CActor * pActor = GetActor(pH);
	if (!pActor)
		return pH->EndFunction(0);

	IInventory *pInventory=pActor->GetInventory();
	if (!pInventory)
		return pH->EndFunction(0);

	int ammoCollected = 0;

	IItemSystem* pItemSystem = g_pGame->GetIGameFramework()->GetIItemSystem();

	int itemCount = pInventory->GetCount();
	for (int i = 0; i < itemCount; ++i)
	{
		IItem* pItem = pItemSystem->GetItem(pInventory->GetItem(i));

		CWeapon* pWeapon = pItem ? static_cast<CWeapon*>(pItem->GetIWeapon()) : NULL;
		if (pWeapon && pWeapon->CanPickUpAutomatically() && !IsGrenadeClass(pWeapon->GetEntity()->GetClass()))
		{
			int fireModeCount = pWeapon->GetNumOfFireModes();
			if (fireModeCount > 0)
			{
				IFireMode* pFireMode = pWeapon->GetFireMode(0);			// Only give ammo for the primary fire mode
				IEntityClass* pAmmoTypeClass = pFireMode->GetAmmoType();
				if (pAmmoTypeClass)
				{
					int ammoTypeCount = pInventory->GetAmmoCount(pAmmoTypeClass);
					int ammoTypeCapacity = pInventory->GetAmmoCapacity(pAmmoTypeClass);

					if (ammoTypeCount < ammoTypeCapacity)
					{
						int clipSize = pFireMode->GetClipSize();
						int finalAmmoCount = ammoTypeCount + (clipSize * numClips);
						finalAmmoCount = MIN(finalAmmoCount, ammoTypeCapacity);

						pWeapon->SetInventoryAmmoCount(pAmmoTypeClass, finalAmmoCount);
						ammoCollected = 1;
					}
				}
			}
		}
	}

	return pH->EndFunction(ammoCollected);
}


int CScriptBind_Actor::IsGod( IFunctionHandler *pH )
{
#ifdef GOD_MODE_ENABLED
	CActor* pActor = GetActor(pH);
	if(pActor && pActor->GetActorClass() == CPlayer::GetActorClassType())
	{
		CPlayer* pPlayer = static_cast<CPlayer*>(pActor);
		return pH->EndFunction(pPlayer->IsGod());
	}
#endif

	return pH->EndFunction(false);
}

int CScriptBind_Actor::AcquireOrReleaseLipSyncExtension(IFunctionHandler *pH)
{
	if (CActor *pActor = GetActor(pH))
	{
		pActor->AcquireOrReleaseLipSyncExtension();
	}
	return pH->EndFunction();
}
