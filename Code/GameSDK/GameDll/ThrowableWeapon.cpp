#include "StdAfx.h"
#include <CryAnimation/ICryAnimation.h>
//#include "CryCharMorphParams.h"
#include "ThrowableWeapon.h"
#include "Game.h"
#include "GameCVars.h"
#include "Actor.h"
#include "Throw.h"
#include "GameActions.h"
#include "Item.h"
#include "WeaponSystem.h"
#include <CryEntitySystem/IEntitySystem.h>
#include "ActorActionsNew.h"

CThrowableWeapon::CThrowableWeapon(void)
{

}

CThrowableWeapon::~CThrowableWeapon(void)
{

}

bool CThrowableWeapon::Init(IGameObject * pGameObject)
{
	if (!CWeapon::Init(pGameObject))
		return false;

	return true;
}

void CThrowableWeapon::Update(SEntityUpdateContext &ctx, int slot)
{

}

void CThrowableWeapon::OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{

}

void CThrowableWeapon::StartFire()
{

}

void CThrowableWeapon::StartFire(const SProjectileLaunchParams& launchParams)
{

}

void CThrowableWeapon::StopFire()
{

}