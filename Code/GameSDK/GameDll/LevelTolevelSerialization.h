#ifndef __LEVELTOLEVELSERIALIZATION_H__
#define __LEVELTOLEVELSERIALIZATION_H__

//#include "GameXMLSettingAndGlobals.h"
#include "Actor.h"

# pragma once

class CLevelTolevelserialization
{
public:
	CLevelTolevelserialization();
	virtual	~CLevelTolevelserialization();

	void Serialize(TSerialize ser);
	EntityId next_spawn_point_id;
	string next_spawn_point_name;
	void UpdateLtl();
	EntityId GetSpawnPointIdFromName(string name);

	enum ELTLSpecFunctions
	{
		eLTLSF_None = 0,
		eLTLSF_RequestLoadLevelFTSpellMark
	};

	virtual void SavePlayerParams();
	virtual void LoadPlayerParams();
	virtual void SavePlayerInventoryState();
	virtual void LoadPlayerInventoryState();
	virtual void SaveQuests();
	virtual void LoadQuests();
	virtual void SavePlayerSpellBook();
	virtual void LoadPlayerSpellBook();
	virtual void SaveTimeOfDay();
	virtual void LoadTimeOfDay();
	bool ClearEntitesInList();
	virtual void SaveAllEntitesOnLevel();
	virtual void LoadAllEntitesToLevel();
	virtual void LoadPlayerChToLevel();
	virtual void SetNextSpawnPoint(EntityId point_id);
	virtual void LoadSpecialPlayerPos();
	virtual void SpecialFunctionProcessing(ELTLSpecFunctions fnc_id);
	Vec3 pl_pos;
	Vec3 pl_pos_spec;
	string pl_marked_lv;
	bool pl_usedmarkspell;
	float upd_val1;
	float upd_val2;
	int tod_days_left;
	float tod_time;
	bool load_process_ltlinfo_at_level_loading_end;
	bool load_process_tod_time;
	bool load_process_player_ltl;
	bool load_process_player_inventory_ltl;
	bool load_process_player_special_pos_ltl;
	bool load_process_player_quests_ltl;
	bool load_process_player_spells_ltl;
	bool load_ent_info;

	bool load_player_inventory_ended_sucessful;


	struct SSerializedLtlObject
	{
		SSerializedLtlObject()
		{
			object_id = 0;
			oblect_position = Matrix34::CreateIdentity();
		}
		EntityId object_id;
		Matrix34 oblect_position;
	};

	struct SSerializedLtlItem
	{
		SSerializedLtlItem()
		{
			item_id = 0;
			item_class_name = "";
			equipped_state = -1;
			quantity_state = -1;
			actorOwnerId = 0;
			item_position = Matrix34::CreateIdentity();
			is_nodraw_type = false;
		}
		EntityId item_id;
		string item_class_name;
		int equipped_state;
		int quantity_state;
		bool is_nodraw_type;
		EntityId actorOwnerId;
		Matrix34 item_position;
	};

	struct SSerializedLtlActor
	{
		SSerializedLtlActor()
		{
			actorId = 0;
			actorName = "";
			dialog_stage = -1;
			health = -1;
			actor_position = Matrix34::CreateIdentity();
		}
		EntityId actorId;
		string actorName;
		int dialog_stage;
		int health;
		Matrix34 actor_position;
	};

	struct SLevelSerializedLTL
	{
		SLevelSerializedLTL()
		{
			level_name = "";
			m_level_actors.clear();
			m_level_items.clear();
			m_level_objects.clear();
		}
		string level_name;
		std::vector<SSerializedLtlActor> m_level_actors;
		std::vector<SSerializedLtlItem> m_level_items;
		std::vector<SSerializedLtlObject> m_level_objects;
	};
	std::vector<SSerializedLtlItem> m_player_items;
	std::vector<SLevelSerializedLTL> m_levels_to_loading;
	std::vector<int> m_spells;
	std::map<int, int> m_spells_in_active_slots;
	std::vector<CGameXMLSettingAndGlobals::SPlayerQuest> ltl_Quests;
	std::vector<CGameXMLSettingAndGlobals::SPlayerSubQuest> ltl_SubQuests;
	virtual void SaveAllPlInfoForLTL();
	string m_char_name;
	string m_player_model_path;
	int m_pl_gender;
	int m_pl_gold;
	int m_pl_level;
	int m_pl_hp;
	int m_pl_stmn;
	int m_pl_mp;
	int m_pl_hp_n;
	int m_pl_stmn_n;
	int m_pl_mp_n;
	float m_levelxp;
	float m_xptonextlevel;
	int m_plheadtype;
	int m_plhairtype;
	int m_pleyestype;
	float m_strength;
	float m_willpower;
	float m_endurance;
	float m_personality;
	float m_intelligence;
	float m_agility;
	float m_breath;
	std::vector<CActor::SActorPassiveEffectRK> m_actor_perks;
	std::vector<CActor::SActorTimedEffectsFtg> m_actor_activeEffects;
	bool m_stealth_mode;
	bool m_relaxed_mode;
	EntityId item_on_back;

	//mem stat
	void GetMemoryUsage(ICrySizer * s) const;
};
#endif 