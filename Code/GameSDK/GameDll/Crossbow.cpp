#include "StdAfx.h"
#include <CryAnimation/ICryAnimation.h>
//#include "CryCharMorphParams.h"
#include "Crossbow.h"
#include "Game.h"
#include "GameCVars.h"
#include "Actor.h"
#include "Throw.h"
#include "GameActions.h"
#include "Item.h"
#include "WeaponSystem.h"
#include <CryEntitySystem/IEntitySystem.h>
#include "ActorActionsNew.h"
#include "Player.h"

CCrossBow::CCrossBow(void)
{
	max_number_of_loaded_bolts_in_crossbow = 2;
	reload_time_for_one_bolt = 0.5f;
	reload_timer = 0.0f;
	recoil_after_shot_time = 0.2f;
	recoil_after_reload_time = 0.6f;
	can_shot = false;
	process_reload_started = false;
	current_loaded_bolts_number = 0;
	recoil_timer = 0.0f;
	reload_type = 1;//1-normal by one bolt, 2-magazine reload
	reload_time_sequence1 = 0.4f;
	reload_time_sequence2 = 0.8f;
	bolt_attached_to_hand = false;
	for (int i = 0; i < MAX_BOLTS_LOADED; i++)
	{
		bolt_attached_to_crossbow[i] = false;
		bolts_models_saved[i] = "";
		bolts_ammo[i] = "";
	}
	bolt_damage_mult = 1.0f;
	bolt_speed_mult = 1.0f;
}

CCrossBow::~CCrossBow(void)
{
	SetFiringLocator(NULL);
}

bool CCrossBow::Init(IGameObject * pGameObject)
{
	bool init_cmpl = true;
	if (!CWeapon::Init(pGameObject))
		init_cmpl = false;

	if (!GetIWeapon())
		return true;

	int idr = GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		CSingle *fm = (CSingle *)pFmd;
		if (!fm)
			return true;

		pCrossbowParams = fm->GetCrossbowParams();
		max_number_of_loaded_bolts_in_crossbow = pCrossbowParams.maxLoadedBolts;
		reload_type = pCrossbowParams.ammoLoadType;
		bolt_damage_mult = pCrossbowParams.damageMult;
		bolt_speed_mult = pCrossbowParams.boltsSpeedMult;
	}
	return init_cmpl;
}

void CCrossBow::Update(SEntityUpdateContext &ctx, int slot)
{
	if (recoil_timer >= 0.0f)
	{
		recoil_timer -= ctx.fFrameTime;
		if (recoil_timer <= 0.0f)
			CryLogAlways("CCrossBow::Update recoil_timer is 0");
	}

	if (!GetOwnerActor())
		return;

	if (process_reload_started)
	{
		reload_timer += ctx.fFrameTime;
		if (reload_type == 1)
		{
			if (reload_timer >= reload_time_sequence1)
			{
				if (!bolt_attached_to_hand)
				{
					AttachBoltToHand();
				}
			}

			if (reload_timer >= reload_time_sequence2)
			{
				if (!bolt_attached_to_crossbow[current_loaded_bolts_number + 1])
				{
					CreateBoltAttachmentOnCrossbow(current_loaded_bolts_number + 1);
				}

				if (bolt_attached_to_hand)
				{
					DetachBoltFromHand();
				}

				process_reload_started = false;
				current_loaded_bolts_number += 1;
				this->SetItemFlag(CItem::eIF_BlockActions, false);
				recoil_timer = recoil_after_reload_time;
			}
		}
		else if (reload_type == 2)
		{

		}
	}
	BaseClass::Update(ctx, slot);
}

void CCrossBow::OnAction(EntityId actorId, const ActionId& actionId, int activationMode, float value)
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pBoltsItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if(!pBoltsItem)
		pBoltsItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (actionId == "attack1")
	{
		if(activationMode == eAAM_OnPress || activationMode == eAAM_OnRelease)
			CryLogAlways("bolts_ammo[current_loaded_bolts_number] == %s", bolts_ammo[current_loaded_bolts_number].c_str());

		if(recoil_timer > 0.0f)
			return;

		if (!pActor)
			return;

		if (IsBusy())
		{
			//CWeapon::OnAction(actorId, actionId, activationMode, value);
			//SetBusy(false);
			return;
		}

		if (CActorActionsNew* nwAction = g_pGame->GetActorActionsNew())
		{
			if (pActor->IsPlayer())
			{
				if(!nwAction->PlayerCanDoAnyAgressiveActions())
					return;
			}
			//CryLogAlways("CCrossBow::OnAction actionId == attack1 seq0");
			if (!nwAction->ActorCanDoAnyAgressiveActions(pActor->GetEntityId(), GetEntityId()))
				return;
		}

		//CryLogAlways("CCrossBow::OnAction actionId == attack1 seq1");

		if (activationMode == eAAM_OnPress || activationMode == eAAM_OnRelease)
		{
			if (IsNoAmmoType() || !IsAmmoTypeCorrected())
			{
				ReinitFiremode();
			}
		}

		if (current_loaded_bolts_number > 0 && !process_reload_started)
		{
			if (activationMode == eAAM_OnHold)
				return;

			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}
			}
			this->SetItemFlag(CItem::eIF_BlockActions, false);
			activationMode = eAAM_OnPress;
			CWeapon::OnAction(actorId, actionId, activationMode, value);
			Shot(current_loaded_bolts_number);
			return;
		}

		if (!process_reload_started && current_loaded_bolts_number <= 0 && activationMode == eAAM_OnPress)
		{
			if (pActor->IsPlayer())
			{
				CPlayer *pPlayer = static_cast<CPlayer *>(pActor);
				if (pPlayer)
				{
					if (!pPlayer->CanFire())
						return;
				}
			}
			ProcessReloadBolt();
			return;
		}

		if(process_reload_started)
			return;

		if (current_loaded_bolts_number <= 0)
			return;

		//Shot(current_loaded_bolts_number);
	}
	else if (actionId == "reload")
	{
		if(!pBoltsItem)
			return;

		if (m_zm && m_zm->IsZoomed())
			return;

		if (recoil_timer >= 0)
			return;

		if (process_reload_started)
		{
			return;
		}

		if (current_loaded_bolts_number < max_number_of_loaded_bolts_in_crossbow)
		{
			ProcessReloadBolt();
		}
		return;
	}
	else if (actionId == "attack2_xi")
	{
		if (!pActor)
			return;

		if (m_zm && m_zm->IsZoomed())
			return;

		if (CActorActionsNew* nwAction = g_pGame->GetActorActionsNew())
		{
			if (pActor->IsPlayer())
			{
				if (!nwAction->PlayerCanDoAnyAgressiveActions())
					return;
			}

			if (!nwAction->ActorCanDoAnyAgressiveActions(pActor->GetEntityId(), GetEntityId()))
				return;
		}

		if (process_reload_started || recoil_timer > 0.0f)
		{
			return;
		}

		if (pActor->GetStamina() < (pActor->stamina_cons_atk_base * pActor->stamina_cons_atk_ml))
		{
			if (g_pGameCVars->g_stamina_sys_can_melee_attack_if_low_stmn_val == 0)
				return;
		}

		if (AreAnyItemFlagsSet(CItem::eIF_BlockActions))
		{
			return;
		}

		RequireUpdate(eIUS_General);
		SpecialMeleeRequestDelayedSimpleAttack(0.3f, (11.0f*pCrossbowParams.meleeCombatHitDmgMult) + (pActor->GetActorStrength() * 0.1f), 0.4, 9);
		const ItemString &meleeAction = "crossbow_melee_attack";
		FragmentID fragmentId = GetFragmentID(meleeAction.c_str());
		PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
		SetItemFlag(CItem::eIF_BlockActions, true);
		recoil_timer = 0.7f;
		OnMeleeNw(pActor->GetEntityId());
		return;
	}
	else if (actionId == "wpn_block_melee")
	{
		if (m_zm && m_zm->IsZoomed())
			return;

		RequireUpdate(eIUS_General);
		if ((process_reload_started || recoil_timer > 0.0f) && (activationMode == eAAM_OnPress))
		{
			return;
		}
		recoil_timer = 0.5f;
		CWeapon::OnAction(actorId, actionId, activationMode, value);
		return;
	}
	CWeapon::OnAction(actorId, actionId, activationMode, value);
}

void CCrossBow::StartFire()
{
	CWeapon::StartFire();
}

void CCrossBow::StartFire(const SProjectileLaunchParams& launchParams)
{
	CWeapon::StartFire(launchParams);
}

void CCrossBow::StopFire()
{
	CWeapon::StopFire();
}

void CCrossBow::OnSelected(bool selected)
{
	CWeapon::OnSelected(selected);

	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 0;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_REPEAT_LAST_KEY;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
		if (current_loaded_bolts_number > 0)
		{
			for (int i = 0; i < current_loaded_bolts_number; i++)
			{
				aparams.m_nLayerID = i + 1;
				aparams.m_fTransTime = 0;
				string anim_ct = "";
				anim_ct.Format("crossbow_armed_idle_only_up_seq_%i", i + 1);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation(anim_ct.c_str(), aparams);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(i + 1, 1);
			}
		}
		else
		{
			for (int i = 0; i < max_number_of_loaded_bolts_in_crossbow; i++)
			{
				if (this->GetEntity()->GetCharacter(slot_v))
				{
					//this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(i, 0.03);
				}
			}
		}
	}
	SetBusy(false);
	SetItemFlag(CItem::eIF_BlockActions, false);
	UpdateLoadedBolts();
}

void CCrossBow::OnEnterFirstPerson()
{
	CWeapon::OnEnterFirstPerson();
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	int slot_old = eIGS_ThirdPerson;
	if(tp_v)
		slot_old = eIGS_FirstPerson;

	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 0;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_REPEAT_LAST_KEY;
	aparams.m_fPlaybackWeight = 1;
	if (current_loaded_bolts_number > 0)
	{
		if (this->GetEntity()->GetCharacter(slot_old))
		{
			this->GetEntity()->GetCharacter(slot_old)->GetISkeletonAnim()->StopAnimationsAllLayers();
		}

		if (this->GetEntity()->GetCharacter(slot_v))
		{
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationsAllLayers();
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
			for (int i = 0; i < current_loaded_bolts_number; i++)
			{
				aparams.m_nLayerID = i + 1;
				aparams.m_fTransTime = 0;
				string anim_ct = "";
				anim_ct.Format("crossbow_armed_idle_only_up_seq_%i", i + 1);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation(anim_ct.c_str(), aparams);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(i+1, 1);
			}
		}
	}
	else
	{
		if (this->GetEntity()->GetCharacter(slot_old))
		{
			this->GetEntity()->GetCharacter(slot_old)->GetISkeletonAnim()->StopAnimationsAllLayers();
		}

		if (this->GetEntity()->GetCharacter(slot_v))
		{
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationsAllLayers();
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
			for (int i = 0; i < max_number_of_loaded_bolts_in_crossbow; i++)
			{
				if (this->GetEntity()->GetCharacter(slot_v))
				{
					//this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(i, 0.03);
				}
			}
		}
	}
	UpdateLoadedBolts();
}

void CCrossBow::OnEnterThirdPerson()
{
	CWeapon::OnEnterThirdPerson();
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	int slot_old = eIGS_ThirdPerson;
	if (tp_v)
		slot_old = eIGS_FirstPerson;

	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 0;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_REPEAT_LAST_KEY;
	aparams.m_fPlaybackWeight = 1;
	if (current_loaded_bolts_number > 0)
	{
		if (this->GetEntity()->GetCharacter(slot_old))
		{
			this->GetEntity()->GetCharacter(slot_old)->GetISkeletonAnim()->StopAnimationsAllLayers();
		}

		if (this->GetEntity()->GetCharacter(slot_v))
		{
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationsAllLayers();
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
			for (int i = 0; i < current_loaded_bolts_number; i++)
			{
				aparams.m_nLayerID = i + 1;
				aparams.m_fTransTime = 0;
				string anim_ct = "";
				anim_ct.Format("crossbow_armed_idle_only_up_seq_%i", i + 1);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation(anim_ct.c_str(), aparams);
				this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(i + 1, 1);
			}
		}
	}
	else
	{
		if (this->GetEntity()->GetCharacter(slot_old))
		{
			this->GetEntity()->GetCharacter(slot_old)->GetISkeletonAnim()->StopAnimationsAllLayers();
		}

		if (this->GetEntity()->GetCharacter(slot_v))
		{
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationsAllLayers();
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
			this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
			for (int i = 0; i < max_number_of_loaded_bolts_in_crossbow; i++)
			{
				if (this->GetEntity()->GetCharacter(slot_v))
				{
					//this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(i, 0.03);
				}
			}
		}
	}
	UpdateLoadedBolts();
}

void CCrossBow::OnReset()
{
	CWeapon::OnReset();
	SetFiringLocator(this);
}

bool CCrossBow::GetProbableHit(EntityId weaponId, const IFireMode* pFireMode, Vec3& hit)
{
	return false;
}



bool CCrossBow::GetFiringPos(EntityId weaponId, const IFireMode* pFireMode, Vec3& pos)
{
	CryLogAlways("CCrossBow::GetFiringPos");
	string bolt_ct = "";
	bolt_ct.Format("bolt_reject_pos_%i", current_loaded_bolts_number);
	int slot = eIGS_ThirdPerson;
	CActor* pOwnerActor = GetOwnerActor();
	if (pOwnerActor && !pOwnerActor->IsThirdPerson())
		slot = eIGS_FirstPerson;

	ICharacterInstance *pCharacter = GetEntity()->GetCharacter(slot);
	if (!pCharacter || !(pCharacter->IsCharacterVisible() && slot == eIGS_ThirdPerson))
		return false;

	Matrix34 fireHelperLocation = GetCharacterAttachmentWorldTM(slot, bolt_ct.c_str());
	pos = fireHelperLocation.TransformPoint(Vec3(ZERO));
	return true;
}



bool CCrossBow::GetFiringDir(EntityId weaponId, const IFireMode* pFireMode, Vec3& dir, const Vec3& probableHit, const Vec3& firingPos)
{
	return false;
}



bool CCrossBow::GetActualWeaponDir(EntityId weaponId, const IFireMode* pFireMode, Vec3& dir, const Vec3& probableHit, const Vec3& firingPos)
{
	return false;
}



bool CCrossBow::GetFiringVelocity(EntityId weaponId, const IFireMode* pFireMode, Vec3& vel, const Vec3& firingDir)
{
	return false;
}



void CCrossBow::WeaponReleased()
{
}

void CCrossBow::ConsumptAmmoItem()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if(!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (pItem && pItem->GetItemQuanity()>0)
	{
		pItem->SetItemQuanity(pItem->GetItemQuanity() - 1);
		if (pItem->GetItemQuanity() < 1)
		{
			int idr = this->GetIWeapon()->GetCurrentFireMode();
			IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
			if (pFmd)
			{
				CActorActionsNew *pActorActionsNew = g_pGame->GetActorActionsNew();
				if (pItem->GetEntityId() == pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts))
					pActor->SetEquippedItemId(eAESlot_Ammo_Bolts, 0);
				else
					pActor->SetEquippedItemId(eAESlot_Ammo_Arrows, 0);

				pItem->SetEquipped(0);
				if (pActorActionsNew)
					pActorActionsNew->DeEquipArmorOrCloth(pActor->GetEntityId(), pItem->GetEntityId());
				//pItem->DeEquip(true);
				pItem->Select(false);
				pItem->HideItem(true);
				pItem->AttachToHand(false, true);
				pItem->Drop(1, false, false);
				pItem->GetEntity()->Hide(true);
			}
		}
	}
}

void CCrossBow::ProcessReloadBolt()
{
	//CryLogAlways("TryToStart bow reload1");
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (!pItem)
		return;

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType && pItem && pItem->GetItemQuanity() <= 0)
		{
			return;
		}

		if (!pItem)
		{
			pFmd->SetAmmoType("noarrow");
			return;
		}

	}

	/*if (current_loaded_bolts_number > 0)
	{
		if (pItem->GetItemQuanity() <= current_loaded_bolts_number)
			return;
	}*/
	//CryLogAlways("TryToStart bow reload2");
	if (current_loaded_bolts_number >= max_number_of_loaded_bolts_in_crossbow)
		return;
	//CryLogAlways("TryToStart bow reload3");
	if (process_reload_started)
		return;
	//CryLogAlways("TryToStart bow reload4");
	reload_timer = 0.0f;
	process_reload_started = true;
	const ItemString &bowAction = "crossbow_reloading";
	FragmentID fragmentId = this->GetFragmentID(bowAction.c_str());
	this->PlayAction(fragmentId, 0, false, CItem::eIPAF_Default);
	this->SetItemFlag(CItem::eIF_BlockActions, true);
	EnableLoadedAnim(current_loaded_bolts_number+1);
}

void CCrossBow::UpdateLoadedBolts()
{
	for (int i = 0; i < current_loaded_bolts_number; i++)
	{
		CreateBoltAttachmentOnCrossbowFromSaved(i+1);
	}
}


void CCrossBow::AttachBoltToHand()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		//if (string(pAmmoType->GetName()) == string("noarrow"))
		//	pAmmoType = NULL;

		if (pAmmoType && pItem && pItem->GetItemQuanity() > 0)
		{
			
			if (pActor->IsPlayer())
			{
				pItem->SetHand(1);
				pItem->HideItem(false);
				pItem->AttachToHand(true);
			}
			else
			{
				pItem->SetHand(1);
				pItem->HideItem(false);
				pItem->AttachToHand(true);
			}

			int mode_v = 1;
			if (pActor->IsThirdPerson())
				mode_v = 2;
		}
	}
	bolt_attached_to_hand = true;
	GetOrPutBoltToQuiver(true);
}

void CCrossBow::DetachBoltFromHand()
{
	CActor *pActor = CWeapon::GetOwnerActor();
	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (pItem)
	{
		if (pActor->IsPlayer())
		{
			pItem->HideItem(true);
			pItem->SetHand(1);
			pItem->AttachToHand(false);
		}
		else
		{
			pItem->HideItem(true);
			pItem->SetHand(1);
			pItem->AttachToHand(false);
		}

		//if (pItem && pItem->GetItemQuanity() > 1)
		//{
		//	ConsumptAmmoItem();
			//GetOrPutBoltToQuiver(true);
		//}
		//else
		//	GetOrPutBoltToQuiver(true);

		ConsumptAmmoItem();
		////if (pItem->GetItemQuanity() == 1)
		////	ConsumptAmmoItem();
	}
	bolt_attached_to_hand = false;
}

void CCrossBow::CreateBoltAttachmentOnCrossbow(int bolt_id)
{
	if (!this->GetOwnerActor())
		return;

	string bolt_ct = "";
	bolt_ct.Format("bolt_att_h_%i", bolt_id);
	string bone_ct = "";
	bone_ct.Format("ctr_bone_%i", bolt_id);

	ICharacterInstance *pCharacter = NULL;
	if (!this->GetOwnerActor()->IsThirdPerson())
		pCharacter = GetEntity()->GetCharacter(eIGS_FirstPerson);
	else
		pCharacter = GetEntity()->GetCharacter(eIGS_ThirdPerson);

	if (!pCharacter)
	{
		CryLogAlways("Error, no crossbow character to bolt attach");
		return;
	}

	int idr = this->GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = this->GetIWeapon()->GetFireMode(idr);
	if (!pFmd)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(this->GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(this->GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (pItem)
	{
		bolt_attached_to_crossbow[bolt_id] = true;
		bolts_models_saved[bolt_id] = pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str();
		bolts_ammo[bolt_id] = pItem->GetItemAmmoType();
		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(bolt_ct.c_str());
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(bolt_ct.c_str(), CA_BONE, bone_ct.c_str(), true);
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
		else if (pIAttachment && pIAttachment->GetIAttachmentObject() || pIAttachment->GetIAttachmentSkin())
		{
			pIAttachment->HideAttachment(false);
		}
		else
		{
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pItem->GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				//pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
		return;
	}
	IEntityClass* pAmmoType = pFmd->GetAmmoType();
	IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
	IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(bolt_ct.c_str());
	const SAmmoParams *pAmmoParams = g_pGame->GetWeaponSystem()->GetAmmoParams(pAmmoType);
	if (!pIAttachment)
	{
		pIAttachment = pIAttachmentManager->CreateAttachment(bolt_ct.c_str(), CA_BONE, bone_ct.c_str(), true);
		if (!pAmmoParams->fpGeometryName.empty())
		{
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAmmoParams->fpGeometryName.c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
	}
	else if (pIAttachment && pIAttachment->GetIAttachmentObject() || pIAttachment->GetIAttachmentSkin())
	{
		pIAttachment->HideAttachment(false);
	}
	else
	{
		if (!pAmmoParams->fpGeometryName.empty())
		{
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(pAmmoParams->fpGeometryName.c_str()))
			{
				pIAttachment->ClearBinding();
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				//pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
	}
	bolt_attached_to_crossbow[bolt_id] = true;
	if (pAmmoParams)
	{
		bolts_models_saved[bolt_id] = pAmmoParams->fpGeometryName.c_str();
		if(pAmmoType)
			bolts_ammo[bolt_id] = pAmmoType->GetName();
	}
}

void CCrossBow::CreateBoltAttachmentOnCrossbowFromSaved(int bolt_id)
{
	if (!this->GetOwnerActor())
		return;

	string bolt_ct = "";
	bolt_ct.Format("bolt_att_h_%i", bolt_id);
	string bone_ct = "";
	bone_ct.Format("ctr_bone_%i", bolt_id);

	ICharacterInstance *pCharacter = NULL;
	if (!this->GetOwnerActor()->IsThirdPerson())
		pCharacter = GetEntity()->GetCharacter(eIGS_FirstPerson);
	else
		pCharacter = GetEntity()->GetCharacter(eIGS_ThirdPerson);

	if (!pCharacter)
	{
		CryLogAlways("Error, no crossbow character to bolt attach");
		return;
	}

	if (!bolts_models_saved[bolt_id].empty())
	{
		bolt_attached_to_crossbow[bolt_id] = true;
		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		IAttachment* pIAttachment = pIAttachmentManager->GetInterfaceByName(bolt_ct.c_str());
		if (!pIAttachment)
		{
			pIAttachment = pIAttachmentManager->CreateAttachment(bolt_ct.c_str(), CA_BONE, bone_ct.c_str(), true);
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(bolts_models_saved[bolt_id].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
		else if (pIAttachment && pIAttachment->GetIAttachmentObject() || pIAttachment->GetIAttachmentSkin())
		{
			pIAttachment->HideAttachment(false);
		}
		else
		{
			if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(bolts_models_saved[bolt_id].c_str()))
			{
				CCGFAttachment *pCGFAttachment = new CCGFAttachment();
				pCGFAttachment->pObj = pStatObj;
				pIAttachment->AddBinding(pCGFAttachment);
				//pIAttachment->AlignJointAttachment();
				pIAttachment->HideAttachment(false);
			}
		}
	}
}

void CCrossBow::DeleteBoltAttachmentFromCrossbow(int bolt_id)
{
	string bolt_ct = "";
	bolt_ct.Format("bolt_att_h_%i", bolt_id);
	ICharacterInstance *pCharacter = this->GetEntity()->GetCharacter(eIGS_FirstPerson);
	if (pCharacter)
	{
		IAttachmentManager* pIAttachmentManager = pCharacter->GetIAttachmentManager();
		IAttachment *pAttachmentt = pIAttachmentManager->GetInterfaceByName(bolt_ct.c_str());
		if (pAttachmentt)
		{
			pAttachmentt->ClearBinding();
		}
	}
	ICharacterInstance *pCharacter2 = this->GetEntity()->GetCharacter(eIGS_ThirdPerson);
	if (pCharacter2)
	{
		IAttachmentManager* pIAttachmentManager = pCharacter2->GetIAttachmentManager();
		IAttachment *pAttachmentt = pIAttachmentManager->GetInterfaceByName(bolt_ct.c_str());
		if (pAttachmentt)
		{
			pAttachmentt->ClearBinding();
		}
	}
	bolt_attached_to_crossbow[bolt_id] = false;
	bolts_models_saved[bolt_id] = "";
	bolts_ammo[bolt_id] = "";
}

void CCrossBow::EnableLoadedAnim(int bolt_id)
{
	string animation_ct = "";
	animation_ct.Format("bolt_loaded_%i", bolt_id);
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(bolt_id, 0.03);
	}
	CryCharAnimationParams aparams;
	aparams.m_nLayerID = bolt_id;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_REPEAT_LAST_KEY;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_v))
	{
		bool anim_started_crossbow = this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation(animation_ct.c_str(), aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(bolt_id, 1);
	}
	else
	{
		CryLogAlways("Error, no crossbow character to start anim");
	}
}

void CCrossBow::PlayWeaponShotAnim(int bolt_id)
{
	string animation_ct = "";
	animation_ct.Format("bolt_shot_%i", bolt_id);
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		//this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationInLayer(bolt_id, 0.5);
	}
	CryCharAnimationParams aparams;
	aparams.m_nLayerID = bolt_id;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0.1;
	aparams.m_fPlaybackSpeed = 2.5f;
	aparams.m_nFlags = 0;
	aparams.m_fKeyTime = 0.05;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_v))
	{
		bool anim_started_bow = this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation(animation_ct.c_str(), aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(bolt_id, 1);
	}
}

void CCrossBow::Shot(int bolt_id)
{
	PlayWeaponShotAnim(bolt_id);
	if (bolt_attached_to_crossbow[bolt_id] == true)
		DeleteBoltAttachmentFromCrossbow(bolt_id);

	if (current_loaded_bolts_number > 0)
	{
		//ConsumptAmmoItem();
		current_loaded_bolts_number -= 1;
	}
	recoil_timer = recoil_after_shot_time;
	//CWeapon::StopFire();
}

bool CCrossBow::IsNoAmmoType()
{
	if(!GetIWeapon())
		return false;

	int idr = GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType)
		{
			if (string(pAmmoType->GetName()) == string("noarrow"))
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

bool CCrossBow::IsAmmoTypeCorrected()
{
	if (!GetIWeapon())
		return false;

	int idr = GetIWeapon()->GetCurrentFireMode();
	IFireMode *pFmd = GetIWeapon()->GetFireMode(idr);
	if (pFmd)
	{
		IEntityClass* pAmmoType = pFmd->GetAmmoType();
		if (pAmmoType)
		{
			if (current_loaded_bolts_number > 0)
			{
				if(bolts_ammo[current_loaded_bolts_number].empty())
					return false;

				if (string(pAmmoType->GetName()) == bolts_ammo[current_loaded_bolts_number])
				{
					return true;
				}
			}
			else
				return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

void CCrossBow::ResetLoadedState()
{
	bool tp_v = false;
	if (this->GetOwnerActor() && this->GetOwnerActor()->IsThirdPerson())
		tp_v = true;

	int slot_v = eIGS_FirstPerson;
	if (tp_v)
		slot_v = eIGS_ThirdPerson;

	int slot_old = eIGS_ThirdPerson;
	if (tp_v)
		slot_old = eIGS_FirstPerson;

	CryCharAnimationParams aparams;
	aparams.m_nLayerID = 0;
	aparams.m_fAllowMultilayerAnim = 1;
	aparams.m_fTransTime = 0;
	aparams.m_fPlaybackSpeed = 1.0f;
	aparams.m_nFlags |= CA_REPEAT_LAST_KEY;
	aparams.m_fPlaybackWeight = 1;
	if (this->GetEntity()->GetCharacter(slot_old))
	{
		this->GetEntity()->GetCharacter(slot_old)->GetISkeletonAnim()->StopAnimationsAllLayers();
	}

	if (this->GetEntity()->GetCharacter(slot_v))
	{
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StopAnimationsAllLayers();
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->StartAnimation("crossbow_idle01", aparams);
		this->GetEntity()->GetCharacter(slot_v)->GetISkeletonAnim()->SetLayerPlaybackScale(0, 1);
	}

	for (int i = 0; i < current_loaded_bolts_number; i++)
	{
		DeleteBoltAttachmentFromCrossbow(i+1);
	}

	bolt_attached_to_hand = false;
	for (int i = 0; i < MAX_BOLTS_LOADED; i++)
	{
		bolt_attached_to_crossbow[i] = false;
		bolts_models_saved[i] = "";
		bolts_ammo[i] = "";
	}
	reload_timer = 0.0f;
	can_shot = false;
	process_reload_started = false;
	current_loaded_bolts_number = 0;
	recoil_timer = 0.0f;
}

void CCrossBow::OnHitDeathReaction()
{
	if (!GetOwnerActor())
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if(!pItem)
		return;

	if (process_reload_started)
	{
		if (bolt_attached_to_hand)
		{
			Matrix34 mtx_rv = Matrix34::CreateIdentity();
			IAttachmentManager* pAttachmentManager = GetOwnerAttachmentManager();
			if (pAttachmentManager)
			{
				IAttachment *pAttachment = pAttachmentManager->GetInterfaceByName(m_sharedparams->params.attachment[m_stats.hand].c_str());
				if (pAttachment)
				{
					mtx_rv = Matrix34(pAttachment->GetAttWorldAbsolute());
				}
			}
			EntityId nv_it_id = m_pItemSystem->CreateItemSimple(mtx_rv, pItem->GetBaseItemToStack());
			CItem *pItem_dropped = static_cast<CItem *>(m_pItemSystem->GetItem(nv_it_id));
			if (pItem_dropped)
			{
				pItem_dropped->Physicalize(true, true);
				pItem_dropped->Impulse(pItem_dropped->GetEntity()->GetPos(), Vec3(0,0,0.2), 2.0f);
			}
			DetachBoltFromHand();
			//pItem->SetItemQuanity(pItem->GetItemQuanity() - 1);
		}

		reload_timer = 0.0f;
		process_reload_started = false;
	}
}

void CCrossBow::GetOrPutBoltToQuiver(bool get)
{
	CActor *pActor = CWeapon::GetOwnerActor();
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(GetOwnerActor()->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (pItem)
	{
		if (pItem->GetItemQuanity() > 10)
			return;

		string attachment_name = "arrow_quiver";
		if (pItem->GetItemType() == 10)
			attachment_name = "bolts_quiver";

		if (ICharacterInstance *pAttachmentObjectChar = pActor->GetAttachmentObjectCharacterInstance(0, attachment_name.c_str()))
		{
			int number_arrows = pItem->GetItemQuanity();
			if (pAttachmentObjectChar->GetIAttachmentManager())
			{
				if (get)
				{
					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", number_arrows - 1);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						pAttachment_quiver_arrow->ClearBinding();
					}
				}
				else
				{

					string arrow_ft = "";
					arrow_ft.Format("arrow_ft_%i", number_arrows);
					IAttachment *pAttachment_quiver_arrow = pAttachmentObjectChar->GetIAttachmentManager()->GetInterfaceByName(arrow_ft.c_str());
					if (pAttachment_quiver_arrow && !pAttachment_quiver_arrow->GetIAttachmentObject())
					{
						if (IStatObj *pStatObj = gEnv->p3DEngine->LoadStatObj(GetSharedItemParams()->params.arrow_quiver_path[1].c_str()))
						{
							CCGFAttachment *pCGFAttachment = new CCGFAttachment();
							pCGFAttachment->pObj = pStatObj;
							pAttachment_quiver_arrow->AddBinding(pCGFAttachment);
						}
					}
				}
			}
		}
	}
}

void CCrossBow::ReinitFiremode()
{
	CryLogAlways("CCrossBow::ReinitFiremode()");
	CActor *pActor = GetOwnerActor();
	if (!pActor)
		return;

	CItem *pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Bolts)));
	if (!pItem)
		pItem = static_cast<CItem *>(m_pItemSystem->GetItem(pActor->GetCurrentEquippedItemId(eAESlot_Ammo_Arrows)));

	if (pActor->GetCurrentItem())
	{
		CWeapon* pWeapon = pActor->GetWeapon(pActor->GetCurrentItemId());
		if (pWeapon)
		{
			int idr = 0;
			idr = pWeapon->GetCurrentFireMode();
			IFireMode *pFmd = pWeapon->GetFireMode(idr);
			if (pFmd)
			{
				CSingle *fm = (CSingle *)pFmd;
				if (fm)
				{
					//CryLogAlways("CCrossBow::ReinitFiremode() CSingle EXIST");
					pCrossbowParams = fm->GetCrossbowParams();
					max_number_of_loaded_bolts_in_crossbow = pCrossbowParams.maxLoadedBolts;
					reload_type = pCrossbowParams.ammoLoadType;
					bolt_damage_mult = pCrossbowParams.damageMult;
					bolt_speed_mult = pCrossbowParams.boltsSpeedMult;
					if (bolt_speed_mult > 2.0f)
					{
						//CryLogAlways("bolt_speed_mult > 2.0f");
					}
				}

				if (pItem && current_loaded_bolts_number == 0)
				{
					IEntityClass* pAmmoType = pFmd->GetAmmoType();
					if (pAmmoType)
					{
						pFmd->SetAmmoType(pItem->GetItemAmmoType());
					}
					else if (!pAmmoType)
					{
						pFmd->SetAmmoType(pItem->GetItemAmmoType());
					}
					pFmd->Activate(false);
					pFmd->Activate(true);
				}
				else
				{
					if (!bolts_ammo[current_loaded_bolts_number].empty())
					{
						IEntityClass* pAmmoType = pFmd->GetAmmoType();
						if (pAmmoType)
						{
							pFmd->SetAmmoType(bolts_ammo[current_loaded_bolts_number]);
						}
						else if (!pAmmoType)
						{
							pFmd->SetAmmoType(bolts_ammo[current_loaded_bolts_number]);
						}
						pFmd->Activate(false);
						pFmd->Activate(true);
					}
					else
					{
						if (pItem)
						{
							IEntityClass* pAmmoType = pFmd->GetAmmoType();
							if (pAmmoType)
							{
								pFmd->SetAmmoType(pItem->GetItemAmmoType());
							}
							else if (!pAmmoType)
							{
								pFmd->SetAmmoType(pItem->GetItemAmmoType());
							}
							pFmd->Activate(false);
							pFmd->Activate(true);
						}
					}
				}
			}
		}
	}
}
