#include "StdAfx.h"

#include "Bolt.h"
#include "Player.h"
#include "GameRules.h"
#include "Bullet.h"
#include "Game.h"
#include "CrossBow.h"

#include <CryEntitySystem/IEntitySystem.h>
#include <CryAction/IMaterialEffects.h>
#include <IGameObjectSystem.h>
#include <CryGame/GameUtils.h>
#include "GameCVars.h"
#include "UI/HUD/HUDCommon.h"


#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>


CBolt::CBolt():
m_stuck(false),
m_notStick(false),
m_can_do_damage(true),
m_nConstraints(0),
m_parentEntity(0),
m_stickyProjectile(true),
m_damageCap(65536.0f),
m_damageFallOffAmount(0.0f)
, m_damageFallOffStart(0.0f)
, m_damageFalloffMin(0.0f)
, m_pointBlankAmount(1.0f)
, m_pointBlankDistance(0.0f)
, m_pointBlankFalloffDistance(0.0f)
, m_Wpn_lcn_Pos(0.0f, 0.0f, 0.0f)
, m_velocity_mult(0.8f)
, m_damage_mult(1.0f)
{
	m_localChildPos.Set(0.0f,0.0f,0.0f);
	m_localChildRot.SetIdentity();
}

//-------------------------------------------
CBolt::~CBolt()
{
	
}

//------------------------------------------
void CBolt::HandleEvent(const SGameObjectEvent &event)
{
	FUNCTION_PROFILER(GetISystem(), PROFILE_GAME);
	CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{
		EventPhysCollision *pCollision = (EventPhysCollision *)event.ptr;
		if (!pCollision)
			return;

		if (this->GetEntity())
		{
			float length_a_b = 0.0f;
			length_a_b = m_Wpn_lcn_Pos.GetDistance(pCollision->pt);
			float Bolt_dst = -0.3f;
			if (m_velocity_mult > 3.0f)
				m_velocity_mult = 3.0f;

			float length_rel = length_a_b*0.05f;
			if (length_rel > 3.0f)
				length_rel = 3.0f;

			Bolt_dst = ((Bolt_dst + m_velocity_mult * 0.1f) - (length_rel * 0.1f));
			Ang3 angles(0, 0, 0);
			Vec3 position(0, 0, Bolt_dst);
			Matrix34 tpLocalTM = Matrix34(Matrix33::CreateRotationXYZ(DEG2RAD(angles)));
			tpLocalTM.ScaleColumn(Vec3(1, 1, 1));
			tpLocalTM.SetTranslation(position);
			this->GetEntity()->SetSlotLocalTM(0, tpLocalTM);
		}
		//---------------------------------------------------------------------------------------------------------------------------------------
		IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
		//----------------------------------------------------------------------------------------------------------------------------------------
		if (gEnv->bServer && !m_stickyProjectile.IsStuck() && !m_stuck)
		{
			m_stickyProjectile.Stick(CStickyProjectile::SStickParams(this, pCollision, CStickyProjectile::eO_AlignToVelocity));
			m_stuck = true;
			if (pTarget)
			{
				CActor  *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if (!pActor_trg)
				{
					CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pTarget->GetId()));
					if (!pItem)
					{
						Vec3 hitDir(ZERO);
						if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
						{
							hitDir = pCollision->vloc[0].GetNormalized();
						}
						GenerateArtificialCollision(this->GetEntity()->GetPhysics(), pTarget->GetPhysics(), pCollision->idmat[0], pCollision->pt,
							pCollision->n, hitDir.GetNormalized() * 3.0f, pCollision->partid[1],
							pCollision->idmat[1], pCollision->iPrim[1], hitDir.GetNormalized() * 2.0f);

						this->SetVelocity(pCollision->pt, hitDir.GetNormalized(), Vec3(0, 0, 0), 0.0f);
					}
				}
			}

			if (!m_stickyProjectile.IsStuck())
			{
				SetAspectProfile(eEA_Physics, ePT_Rigid);
				m_can_do_damage = false;
			}
		}
	}
	//CProjectile::HandleEvent(event);
	if (event.event == eGFE_OnCollision)
	{
		EventPhysCollision *pCollision = reinterpret_cast<EventPhysCollision *>(event.ptr);
		if (!pCollision)
			return;

		IEntity *pTarget = pCollision->iForeignData[1] == PHYS_FOREIGN_ID_ENTITY ? (IEntity*)pCollision->pForeignData[1] : 0;
		//Only process hits that have a target
		if (pTarget)
		{
			Vec3 dir(0, 0, 0);
			if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
				dir = pCollision->vloc[0].GetNormalized();

			float finalDamage = GetFinalDamage(pCollision->pt);
			const int hitMatId = pCollision->idmat[1];

			Vec3 hitDir(ZERO);
			if (pCollision->vloc[0].GetLengthSquared() > 1e-6f)
			{
				hitDir = pCollision->vloc[0].GetNormalized();
			}

			CGameRules *pGameRules = g_pGame->GetGameRules();
			IActor* pActor = g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId);
			CActor  *pActor_c = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(m_ownerId));
			bool ok = true;
			float m_crt_dmg_mult = 1.0f;
			CWeapon *pWpn = GetWeapon();
			if (pWpn)
			{
				int rnd_value1 = rand() % 100 + 1;
				if (rnd_value1 > 0 && rnd_value1 < pWpn->GetCriticalHitBaseChance()*pActor_c->crtchance_common_ml*pActor_c->crtchance_crossbows_ml)
					m_crt_dmg_mult = pWpn->GetCriticalHitDmgModiffer()*pActor_c->crtdmg_common_ml*pActor_c->crtdmg_crossbows_ml;
			
				finalDamage = finalDamage * pActor_c->crossbows_ml;
				finalDamage = ((finalDamage + pWpn->GetDmgAdded())*(m_crt_dmg_mult + (pActor_c->GetAgility()*0.01)));
			}

			if (m_can_do_damage)
				ProcessHit(*pGameRules, *pCollision, *pTarget, finalDamage*m_damage_mult, hitMatId, hitDir);
		}

		if (m_stuck)
		{
			if (pTarget)
			{
				CActor *pActor_trg = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pTarget->GetId()));
				if (pActor_trg)
					return;
			}

			IItemSystem	*pItemSystem = static_cast<IItemSystem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem());
			if (!pItemSystem)
				return;

			EntityId item_prj = pItemSystem->CreateItemSimple(GetEntity()->GetWorldTM(), m_pAmmoParams->base_ammo_item_class.c_str());
			IEntity *pItemEnt = gEnv->pEntitySystem->GetEntity(item_prj);
			IEntity *pParentEnt = GetEntity()->GetParent();
			if (pParentEnt)
			{
				CActor *pActor_prt = static_cast<CActor*>(g_pGame->GetIGameFramework()->GetIActorSystem()->GetActor(pParentEnt->GetId()));
				if (pActor_prt)
					return;
			}

			if (pItemEnt)
			{
				CItem* pItem = static_cast<CItem*>(g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pItemEnt->GetId()));
				if (pItem)
				{
					if (pParentEnt)
					{
						SEntityPhysicalizeParams params;
						params.type = PE_NONE;
						params.nSlot = -1;
						pItemEnt->Physicalize(params);
						SChildAttachParams attachParams(IEntity::ATTACHMENT_KEEP_TRANSFORMATION);
						pParentEnt->AttachChild(pItemEnt, attachParams);
						IItem *pItemc = static_cast<IItem*>(gEnv->pGame->GetIGameFramework()->GetIItemSystem()->GetItem(pParentEnt->GetId()));
						if (!pItemc)
						{
							params.type = PE_STATIC;
							params.nSlot = 11;
							pItemEnt->Physicalize(params);
						}
						pItem->DrawSlot(11, true);
					}
					pItemEnt->SetStatObj(GetEntity()->GetStatObj(0), 11, false);
					pItemEnt->SetSlotLocalTM(11, GetEntity()->GetSlotLocalTM(0, false));
					if (!pParentEnt)
					{
						if (m_can_do_damage)
						{
							pItem->Physicalize(true, false);
						}
						else
						{
							pItem->Physicalize(true, true);
							pItem->Impulse(pItemEnt->GetPos(), pCollision->n, 2.0f);
						}
					}
					pItem->Pickalize(true, true);
					pItem->DrawSlot(1, false);
					//CryLogAlways("CBolt Item entity pos now x- %f y- %f z- %f", pItemEnt->GetPos().x, pItemEnt->GetPos().y, pItemEnt->GetPos().z);
					Destroy();
				}
			}
		}
	}
}

//-------------------------------------------
void CBolt::Launch(const Vec3 &pos, const Vec3 &dir, const Vec3 &velocity, float speedScale)
{
	//CryLogAlways("CBolt::Launch called");
	Vec3 new_vel(0, 0, 0);
	CWeapon *pWpn = GetWeapon();
	if (pWpn)
	{
		if (pWpn->GetWeaponType() == 11)
		{
			CCrossBow *pCrossBow = static_cast<CCrossBow*>(pWpn);
			if (pCrossBow)
			{
				if (pWpn->GetOwnerActor())
					m_Wpn_lcn_Pos = pWpn->GetOwnerActor()->GetEntity()->GetWorldPos();

				m_damage_mult = pCrossBow->bolt_damage_mult;
				m_velocity_mult = pCrossBow->bolt_speed_mult;
				new_vel = velocity;
				new_vel = new_vel*m_velocity_mult;
				CProjectile::Launch(pos, dir, new_vel, m_velocity_mult);
				return;
			}
		}
	}
	else
	{
		m_Wpn_lcn_Pos = pos;
	}
	CProjectile::Launch(pos, dir, velocity, speedScale);
}

//-------------------------------------------
void CBolt::SetParams(const SProjectileDesc& projectileDesc)
{
	CProjectile::SetParams(projectileDesc);
}

//-------------------------------------------
void CBolt::Explode(const SExplodeDesc& explodeDesc)
{
	CProjectile::Explode(explodeDesc);
}

//-------------------------------------------
void CBolt::OnHit(const HitInfo& hit)
{
	CProjectile::OnHit(hit);
}

//-----------------------------------------------

//-------------------------------------------------------------
void CBolt::RefineCollision(EventPhysCollision *pCollision)
{
	static ray_hit hit;
	static const int objTypes = ent_all;    
	static const unsigned int flags = rwi_stop_at_pierceable|rwi_colltype_any;
	static const float c4Offset = 0.05f;

	Vec3 pos = pCollision->pt+(pCollision->n*0.2f);
	int hits = gEnv->pPhysicalWorld->RayWorldIntersection(pos,pCollision->n*-0.7f,objTypes,flags,&hit,1,&m_pPhysicalEntity,m_pPhysicalEntity?1:0);
	if(hits!=0)
	{
		
	}
}

void CBolt::ProcessHit(CGameRules& gameRules, const EventPhysCollision& collision, IEntity& target, float damage, int hitMatId, const Vec3& hitDir)
{
	if (damage > 0.0f)
	{
		m_hitTypeId = CActor::eADRT_Bolt;
		EntityId targetId = target.GetId();
		HitInfo hitInfo(m_ownerId ? m_ownerId : m_hostId, targetId, m_weaponId,
			damage, 0.0f, hitMatId, collision.partid[1],
			m_hitTypeId, collision.pt, hitDir, collision.n);

		hitInfo.remote = IsRemote();
		hitInfo.projectileId = GetEntityId();
		hitInfo.bulletType = m_pAmmoParams->bulletType;
		hitInfo.knocksDown = CheckAnyProjectileFlags(ePFlag_knocksTarget) && (damage > m_minDamageForKnockDown);
		hitInfo.knocksDownLeg = m_chanceToKnockDownLeg > 0 && damage > m_minDamageForKnockDownLeg && m_chanceToKnockDownLeg > cry_random(0, 99);
		hitInfo.penetrationCount = 0;
		//hitInfo.partId
		hitInfo.hitViaProxy = CheckAnyProjectileFlags(ePFlag_firedViaProxy);
		hitInfo.aimed = CheckAnyProjectileFlags(ePFlag_aimedShot);
		IPhysicalEntity* pPhysicalEntity = collision.pEntity[1];
		gameRules.ClientHit(hitInfo);
		ReportHit(targetId);
	}
}

void CBolt::GenerateArtificialCollision(IPhysicalEntity* pAttackerPhysEnt, IPhysicalEntity *pCollider, int colliderMatId, const Vec3 &position, const Vec3& normal, const Vec3 &vel, int partId, int surfaceIdx, int iPrim, const Vec3& impulse)
{
	//////////////////////////////////////////////////////////////////////////
	// Apply impulse to object.
	pe_action_impulse ai;
	ai.partid = partId;
	ai.point = position;
	ai.impulse = impulse;
	pCollider->Action(&ai);

	//////////////////////////////////////////////////////////////////////////
	// create a physical collision 
	pe_action_register_coll_event collision;

	collision.collMass = 0.005f; // this needs to be set to something.. but is seemingly ignored in calculations... (just doing what meleeCollisionHelper does here..)
	collision.partid[1] = partId;

	// collisions involving partId<-1 are to be ignored by game's damage calculations
	// usually created articially to make stuff break.
	collision.partid[0] = -2;
	collision.idmat[1] = surfaceIdx;
	collision.idmat[0] = colliderMatId;
	collision.n = normal;
	collision.pt = position;
	collision.vSelf = vel;
	collision.v = vel;//Vec3(0, 0, 0);
	collision.pCollider = pCollider;
	collision.iPrim[0] = -1;
	collision.iPrim[1] = iPrim;

	pAttackerPhysEnt->Action(&collision);
}

float CBolt::GetFinalDamage(const Vec3& hitPos) const
{
	float damage = (float)m_damage;

	if (m_damageFallOffAmount > 0.f)
	{
		float distTravelledSq = (m_initial_pos - hitPos).GetLengthSquared();
		bool fallof = distTravelledSq > (m_damageFallOffStart*m_damageFallOffStart);
		bool pointBlank = distTravelledSq < (m_pointBlankFalloffDistance*m_pointBlankFalloffDistance);
		float distTravelled = (fallof || pointBlank) ? sqrt_tpl(distTravelledSq) : 0.0f;

		if (fallof)
		{
			distTravelled -= m_damageFallOffStart;
			damage -= distTravelled * m_damageFallOffAmount;
		}

		if (pointBlank)
		{
			damage *= Projectile::GetPointBlankMultiplierAtRange(distTravelled, m_pointBlankDistance, m_pointBlankFalloffDistance, m_pointBlankAmount);
		}
	}

	damage = max(damage, m_damageFalloffMin);
	damage = damage*m_velocity_mult;

	return damage;
}