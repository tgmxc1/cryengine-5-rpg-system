#include "VectorMathFnc.h"

CVectorMathFncs::CVectorMathFncs()
{

}

CVectorMathFncs::~CVectorMathFncs()
{

}

Vec3 CVectorMathFncs::FindNearestPointInLine(Vec3 start_line_point, Vec3 end_line_point, Vec3 st_find_point, float num_slcs)
{
	Vec3 last_nearest_point(ZERO);
	Vec3 line_direction = end_line_point - start_line_point;
	Vec3 line_direction_n = line_direction.GetNormalized();
	float last_nearest_point_distance = 10000.0f;
	int num_seq = (int)num_slcs;
	float coof = 1.0f / num_seq;
	for (int i = 0; i < num_seq; i++)
	{
		if (i > 0 && i != num_seq)
		{
			float mult_vv = i * coof;
			Vec3 lpn = start_line_point + (line_direction_n * mult_vv);
			float dist = lpn.GetDistance(st_find_point);
			if (dist < last_nearest_point_distance)
			{
				last_nearest_point_distance = dist;
				last_nearest_point = lpn;
			}
		}
	}
	float dist = start_line_point.GetDistance(st_find_point);
	if (dist < last_nearest_point_distance)
	{
		last_nearest_point_distance = dist;
		last_nearest_point = start_line_point;
	}
	dist = end_line_point.GetDistance(st_find_point);
	if (dist < last_nearest_point_distance)
	{
		last_nearest_point_distance = dist;
		last_nearest_point = end_line_point;
	}

	return last_nearest_point;
}

Vec3 CVectorMathFncs::FindNearestPointInLine(Vec3 start_line_point, Vec3 line_direction, float line_length, Vec3 st_find_point, float num_slcs)
{
	Vec3 last_nearest_point(ZERO);
	Vec3 end_line_point = start_line_point + (line_direction * line_length);
	float last_nearest_point_distance = 10000.0f;
	int num_seq = (int)num_slcs;
	float coof = 1.0f / num_seq;
	for (int i = 0; i < num_seq; i++)
	{
		if (i > 0 && i != num_seq)
		{
			float mult_vv = i * coof;
			Vec3 lpn = start_line_point + (line_direction * mult_vv);
			float dist = lpn.GetDistance(st_find_point);
			if (dist < last_nearest_point_distance)
			{
				last_nearest_point_distance = dist;
				last_nearest_point = lpn;
			}
		}
	}
	float dist = start_line_point.GetDistance(st_find_point);
	if (dist < last_nearest_point_distance)
	{
		last_nearest_point_distance = dist;
		last_nearest_point = start_line_point;
	}
	dist = end_line_point.GetDistance(st_find_point);
	if (dist < last_nearest_point_distance)
	{
		last_nearest_point_distance = dist;
		last_nearest_point = end_line_point;
	}

	return last_nearest_point;
}
