#ifndef __VECTOR_MATH_FNC__H__
#define __VECTOR_MATH_FNC__H__

//#include "IGameObject.h"
#include "StdAfx.h"

class CVectorMathFncs
{

public:

	CVectorMathFncs();
	~CVectorMathFncs();

	struct S3DLinePointed
	{
		Vec3 first_line_point;
		Vec3 second_line_point;
	};

	struct S3DLineDirectional
	{
		Vec3 first_line_point;
		Vec3 line_direction;
		float line_length;
	};

	Vec3 FindNearestPointInLine(Vec3 start_line_point, Vec3 end_line_point, Vec3 st_find_point, float num_slcs);
	Vec3 FindNearestPointInLine(Vec3 start_line_point, Vec3 line_direction, float line_length, Vec3 st_find_point, float num_slcs);
};


#endif