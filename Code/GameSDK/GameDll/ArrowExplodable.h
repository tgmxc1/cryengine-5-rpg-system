#ifndef __ARROWEXPLODABLE_H__
#define __ARROWEXPLODABLE_H__

#if _MSC_VER > 1000
# pragma once
#endif

#include "Projectile.h"
#include "StickyProjectile.h"

class CArrowExplodable : public CProjectile
{
private:
	typedef CProjectile BaseClass;

public:
	CArrowExplodable();
	virtual ~CArrowExplodable();
	virtual void HandleEvent(const SGameObjectEvent &event);
	virtual void Launch(const Vec3 &pos, const Vec3 &dir, const Vec3 &velocity, float speedScale);
	virtual void Explode(const SExplodeDesc& explodeDesc);
	virtual void OnHit(const HitInfo& hit);
	void RefineCollision(EventPhysCollision *pCollision);
	virtual void SetParams(const SProjectileDesc& projectileDesc);
	void ProcessHit(CGameRules& gameRules, const EventPhysCollision& collision, IEntity& target, float damage, int hitMatId, const Vec3& hitDir);
	void GenerateArtificialCollision(IPhysicalEntity* pAttackerPhysEnt, IPhysicalEntity *pCollider, int colliderMatId, const Vec3 &position, const Vec3& normal, const Vec3 &vel, int partId, int surfaceIdx, int iPrim, const Vec3& impulse);
	float GetFinalDamage(const Vec3& hitPos) const;
	bool	  m_stuck;
	bool      m_notStick;
	int       m_nConstraints;
	int       m_constraintIds[3];
	CStickyProjectile	m_stickyProjectile;
	EntityId	m_parentEntity;
	Vec3			m_localChildPos;
	Quat			m_localChildRot;
	float m_damageCap;
	float m_damageFallOffStart;
	float m_damageFallOffAmount;
	float m_damageFalloffMin;
	float m_pointBlankAmount;
	float m_pointBlankDistance;
	float m_pointBlankFalloffDistance;
	float m_velocity_mult;
	Vec3  m_Wpn_lcn_Pos;
};

#endif