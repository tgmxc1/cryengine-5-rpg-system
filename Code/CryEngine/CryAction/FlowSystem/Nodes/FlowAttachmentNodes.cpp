// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#include "StdAfx.h"
#include "FlowBaseNode.h"
#include "FlowEntityNode.h"

class CFlowNode_Attachment : public CFlowEntityNodeBase
{
private:
	enum ECharacterSlot { eCharacterSlot_Invalid = -1 };

protected:
	int    m_characterSlot;
	string m_boneName;

public:
	CFlowNode_Attachment(SActivationInfo* pActInfo)
		: m_characterSlot(eCharacterSlot_Invalid)
	{
		m_event = ENTITY_EVENT_RESET;
	}

	enum EInPorts
	{
		eIP_EntityId,
		eIP_BoneName,
		eIP_CharacterSlot,
		eIP_EnableOffset,
		eIP_Attach,
		eIP_Detach,
		eIP_Hide,
		eIP_UnHide,
		eIP_RotationOffset,
		eIP_TranslationOffset
	};

	enum EOutPorts
	{
		eOP_Attached,
		eOP_Detached
	};

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig in_config[] = {
			InputPortConfig<EntityId>("Item",          _HELP("Entity to be linked")),
			InputPortConfig<string>("BoneName",        _HELP("Attachment bone")),
			InputPortConfig<int>("CharacterSlot",      0, _HELP("Host character slot")),
			InputPortConfig<bool>("EnableAttacmentOffset", false, _HELP("Apply Offset params")),
			InputPortConfig<SFlowSystemVoid>("Attach", _HELP("Attach")),
			InputPortConfig<SFlowSystemVoid>("Detach", _HELP("Detach any entity currently attached to the given bone")),
			InputPortConfig_Void("Hide",               _HELP("Hides attachment")),
			InputPortConfig_Void("UnHide",             _HELP("Unhides attachment")),
			InputPortConfig<Vec3>("RotationOffset",    _HELP("Rotation offset")),
			InputPortConfig<Vec3>("TranslationOffset", _HELP("Translation offset")),
			{ 0 }
		};
		static const SOutputPortConfig out_config[] = {
			OutputPortConfig<SFlowSystemVoid>("Attached"),
			OutputPortConfig<SFlowSystemVoid>("Detached"),
			{ 0 }
		};
		config.sDescription = _HELP("Attach/Detach an entity to another one.");
		config.pInputPorts = in_config;
		config.pOutputPorts = out_config;
		config.SetCategory(EFLN_APPROVED | EFLN_TARGET_ENTITY);
	}

	virtual IFlowNodePtr Clone(SActivationInfo* pActInfo) { return new CFlowNode_Attachment(pActInfo); }

	virtual void         Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo* pActInfo)
	{
		CFlowEntityNodeBase::ProcessEvent(event, pActInfo);
		switch (event)
		{
		case eFE_Activate:
			{
				if (IsPortActive(pActInfo, eIP_Attach))
				{
					AttachObject(pActInfo);
				}
				if (IsPortActive(pActInfo, eIP_Detach))
				{
					DetachObject(pActInfo);
				}

				if (IsPortActive(pActInfo, eIP_Hide))
				{
					HideAttachment(pActInfo);
				}
				else if (IsPortActive(pActInfo, eIP_UnHide))
				{
					UnHideAttachment(pActInfo);
				}

				if (IsPortActive(pActInfo, eIP_RotationOffset) || IsPortActive(pActInfo, eIP_TranslationOffset))
				{
					UpdateOffset(pActInfo);
				}
				break;
			}
		case eFE_Initialize:
			{
				m_characterSlot = GetPortInt(pActInfo, eIP_CharacterSlot);
				m_boneName = GetPortString(pActInfo, eIP_BoneName);

				if (gEnv->IsEditor() && m_entityId)
				{
					UnregisterEvent(m_event);
					RegisterEvent(m_event);
				}
				break;
			}
		}
	}

	virtual void GetMemoryUsage(ICrySizer* s) const
	{
		s->Add(*this);
	}

	IAttachment* GetAttachment(IEntity* pEntity)
	{
		IAttachment* pAttachment = nullptr;
		if (pEntity && !m_boneName.empty() && m_characterSlot > eCharacterSlot_Invalid)
		{
			if (ICharacterInstance* pCharacterInstance = pEntity->GetCharacter(m_characterSlot))
			{
				if (IAttachmentManager* pAttachmentManager = pCharacterInstance->GetIAttachmentManager())
				{
					pAttachment = pAttachmentManager->GetInterfaceByName(m_boneName);
				}
			}
		}
		return pAttachment;
	}

	void AttachObject(SActivationInfo* pActInfo)
	{
		if (pActInfo->pEntity)
		{
			EntityId entityId = GetPortEntityId(pActInfo, eIP_EntityId);
			IEntity* pEntity = gEnv->pEntitySystem->GetEntity(entityId);
			if (pEntity)
			{
				IAttachment* pAttachment = GetAttachment(pActInfo->pEntity);
				if (pAttachment)
				{
					CEntityAttachment* pEntityAttachment = new CEntityAttachment;
					pEntityAttachment->SetEntityId(entityId);
					pAttachment->AddBinding(pEntityAttachment);

					UpdateOffset(pActInfo);
					ActivateOutput(pActInfo, eOP_Attached, 0);
				}
			}
		}
	}

	void DetachObject(SActivationInfo* pActInfo)
	{
		IAttachment* pAttachment = GetAttachment(pActInfo->pEntity);
		if (pAttachment)
		{
			pAttachment->ClearBinding();
			ActivateOutput(pActInfo, eOP_Detached, 0);
		}
	}

	void HideAttachment(SActivationInfo* pActInfo)
	{
		IAttachment* pAttachment = GetAttachment(pActInfo->pEntity);
		if (pAttachment)
		{
			pAttachment->HideAttachment(1);

			IAttachmentObject* pAttachmentObject = pAttachment->GetIAttachmentObject();
			if ((pAttachmentObject != NULL) && (pAttachmentObject->GetAttachmentType() == IAttachmentObject::eAttachment_Entity))
			{
				IEntity* pAttachedEntity = gEnv->pEntitySystem->GetEntity(static_cast<CEntityAttachment*>(pAttachmentObject)->GetEntityId());
				if (pAttachedEntity)
				{
					pAttachedEntity->Hide(true);
				}
			}
		}
	}

	void UnHideAttachment(SActivationInfo* pActInfo)
	{
		IAttachment* pAttachment = GetAttachment(pActInfo->pEntity);
		if (pAttachment)
		{
			pAttachment->HideAttachment(0);

			IAttachmentObject* pAttachmentObject = pAttachment->GetIAttachmentObject();
			if ((pAttachmentObject != NULL) && (pAttachmentObject->GetAttachmentType() == IAttachmentObject::eAttachment_Entity))
			{
				IEntity* pAttachedEntity = gEnv->pEntitySystem->GetEntity(static_cast<CEntityAttachment*>(pAttachmentObject)->GetEntityId());
				if (pAttachedEntity)
				{
					pAttachedEntity->Hide(false);
				}
			}
		}
	}

	void UpdateOffset(SActivationInfo* pActInfo)
	{
		if (GetPortBool(pActInfo, eIP_EnableOffset) == true)
		{
			IAttachment* pAttachment = GetAttachment(pActInfo->pEntity);
			if (pAttachment)
			{
				const Vec3& rotationOffsetEuler = GetPortVec3(pActInfo, eIP_RotationOffset);
				QuatT offset = QuatT::CreateRotationXYZ(Ang3(DEG2RAD(rotationOffsetEuler)));
				offset.t = GetPortVec3(pActInfo, eIP_TranslationOffset);
				pAttachment->SetAttRelativeDefault(offset);
			}
		}
	}

	void OnEntityEvent(IEntity* pEntity, SEntityEvent& event)
	{
		if (event.event == m_event)
		{
			if (IAttachment* pAttachment = GetAttachment(pEntity))
			{
				pAttachment->ClearBinding();
			}
		}
	}
};

class CFlowGetObjectHelperPosNode : public CFlowBaseNode<eNCT_Singleton>
{
private:
	enum EInputPorts
	{
		IN_GET = 0,
		IN_Slot,
		IN_HelperName,
		IN_COORDSYS
	};

	enum EOutputPorts
	{
		OUT_POS = 0
	};

	enum ECoordSys
	{
		CS_PARENT = 0,
		CS_WORLD,
	};

public:
	CFlowGetObjectHelperPosNode(SActivationInfo * pActInfo)
	{
	}

	virtual ~CFlowGetObjectHelperPosNode()
	{
	}

	

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Get",      _HELP("Trigger to get current values")),
			InputPortConfig<int>("ObjSlot", 0, _HELP("Slot to find object helper")),
			InputPortConfig<string>("Helper", "", _HELP("Helper name")),
			InputPortConfig<int>("CoordSys", 1,                                      _HELP("In which coordinate system the values are expressed"),_HELP("CoordSys"), _UICONFIG("enum_int:Object=0,World=1")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig<Vec3>("Pos",      _HELP("Entity helper position vector")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("Get Entity Helper Position");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		if (event != eFE_Activate || IsPortActive(pActInfo, IN_GET) == false)
			return;
		// only if IN_GET is activated.

		IEntity* pEntity = pActInfo->pEntity;
		if (pEntity == 0)
			return;

		ECoordSys coorSys = (ECoordSys)GetPortInt(pActInfo, IN_COORDSYS);

		switch (coorSys)
		{
		case CS_WORLD:
		{
			Vec3 pos = GetEntityHelperPos(pEntity->GetId(), GetPortInt(pActInfo, IN_Slot), GetPortString(pActInfo, IN_HelperName), false);
			ActivateOutput(pActInfo, OUT_POS, pos);
			break;
		}

		case CS_PARENT:
		{
			Vec3 pos = GetEntityHelperPos(pEntity->GetId(), GetPortInt(pActInfo, IN_Slot), GetPortString(pActInfo, IN_HelperName), true);
			ActivateOutput(pActInfo, OUT_POS, pos);
			break;
		}

		}
	}

	Vec3 GetEntityHelperPos(EntityId id, int slot, const char* helperName, bool objectSpace)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(id);
		if (!pEntity)
			return Vec3(ZERO);

		IStatObj* pObject = pEntity->GetStatObj(slot);
		ICharacterInstance* pCharacter = pEntity->GetCharacter(slot);

		Vec3 position(0, 0, 0);

		if (pObject)
		{
			if (pObject->GetGeoName())
			{
				_smart_ptr<IStatObj> pRootObj = gEnv->p3DEngine->LoadStatObj(pObject->GetFilePath());

				if (pRootObj)
				{
					position = pRootObj->GetHelperPos(helperName);
				}
			}
			else
			{
				position = pObject->GetHelperPos(helperName);
			}

			if (!objectSpace)
			{
				position = pEntity->GetSlotWorldTM(slot).TransformPoint(position);
			}
			else
			{
				position = pEntity->GetSlotLocalTM(slot, false).TransformPoint(position);
			}
		}
		else if (pCharacter)
		{
			int16 id = pCharacter->GetIDefaultSkeleton().GetJointIDByName(helperName);
			if (id >= 0)
				position = pCharacter->GetISkeletonPose()->GetAbsJointByID(id).t;
			else
				position.Set(0, 0, 0);
			//position = pCharacter->GetISkeleton()->GetHelperPos(helperName);

			if (!objectSpace)
			{
				position = pEntity->GetSlotWorldTM(slot).TransformPoint(position);
			}
			else
			{
				position = pEntity->GetSlotLocalTM(slot, false).TransformPoint(position);
			}
		}

		return position;
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
	}
};

REGISTER_FLOW_NODE("Entity:Attachment", CFlowNode_Attachment);
REGISTER_FLOW_NODE("Entity:GetHelperPos", CFlowGetObjectHelperPosNode);
